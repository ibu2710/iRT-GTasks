package com.todoroo.gtasks;

import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

public interface ConnectionManager {
    JSONObject get() throws IOException, GoogleLoginException, JSONException;

    JSONObject post(JSONObject jSONObject) throws IOException, GoogleLoginException, JSONException;
}
