package com.todoroo.gtasks;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.annotation.NotThreadSafe;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@NotThreadSafe
public class GoogleConnectionManager implements ConnectionManager {
    private static final String CLIENT_LOGIN = "https://www.google.com/accounts/ClientLogin";
    private static final String TASKS_GET = "https://mail.google.com/tasks/ig";
    private static final String TASKS_POST = "https://mail.google.com/tasks/r/ig";
    private static final Pattern setupJson = Pattern.compile("_setup\\((.*)\\)");
    private String authenticationToken;
    private boolean forceGoogleAccount;
    private DefaultHttpClient httpClient;
    private String password;
    private String username;

    private static class PostData {
        private List<NameValuePair> nvps;

        private PostData() {
            this.nvps = new ArrayList();
        }

        public PostData add(String name, String value) {
            this.nvps.add(new BasicNameValuePair(name, value));
            return this;
        }

        public HttpPost build(String url) throws UnsupportedEncodingException {
            UrlEncodedFormEntity ent = new UrlEncodedFormEntity(this.nvps, "UTF-8");
            HttpPost post = new HttpPost(url);
            post.setEntity(ent);
            return post;
        }
    }

    public GoogleConnectionManager(String username, String password, boolean forceGoogleAccount) {
        this.authenticationToken = null;
        this.forceGoogleAccount = false;
        this.httpClient = new DefaultHttpClient();
        this.username = username;
        this.password = password;
        this.forceGoogleAccount = forceGoogleAccount;
    }

    public GoogleConnectionManager(String username, String password) {
        this(username, password, false);
    }

    public GoogleConnectionManager(String authenticationToken) {
        this.authenticationToken = null;
        this.forceGoogleAccount = false;
        this.httpClient = new DefaultHttpClient();
        this.authenticationToken = authenticationToken;
    }

    public void authenticate(boolean force) throws GoogleLoginException, IOException {
        if (this.authenticationToken == null || force) {
            try {
                for (String line : execute(new PostData().add("Email", this.username).add("Passwd", this.password).add("service", "goanna_mobile").add("source", "todoroo-astrid-1").add("accountType", this.forceGoogleAccount ? "GOOGLE" : "HOSTED_OR_GOOGLE").build(CLIENT_LOGIN)).split("\\r?\\n")) {
                    if (line.startsWith("Auth=")) {
                        this.authenticationToken = line.replace("Auth=", "auth=");
                        return;
                    }
                }
            } catch (Throwable e) {
                throw new GoogleLoginException(e);
            }
        }
    }

    public JSONObject get() throws IOException, GoogleLoginException, JSONException {
        authenticate(false);
        String response = execute(new HttpGet(TASKS_GET));
        Matcher matcher = setupJson.matcher(response);
        if (matcher.find()) {
            return new JSONObject(matcher.group(1));
        }
        throw new GoogleLoginException("Could not parse response for JSON. Response: " + response);
    }

    public JSONObject post(JSONObject post) throws IOException, GoogleLoginException, JSONException {
        authenticate(false);
        HttpPost request = new PostData().add("r", post.toString()).build(TASKS_POST);
        request.setHeader("AT", "1.0");
        return new JSONObject(execute(request));
    }

    protected String execute(HttpUriRequest request) throws IOException, GoogleLoginException {
        if (this.authenticationToken != null) {
            request.setHeader("Authorization", "GoogleLogin " + this.authenticationToken);
        }
        HttpResponse response = this.httpClient.execute(request);
        String ret = EntityUtils.toString(response.getEntity(), "UTF-8");
        if (response.getStatusLine().getStatusCode() == 200) {
            return ret;
        }
        throw new ConnectException(new StringBuilder(String.valueOf(response.getStatusLine().toString())).append(" from ").append(request.getURI()).append(".\n\n").append(ret).toString());
    }

    public String getToken() {
        return this.authenticationToken;
    }
}
