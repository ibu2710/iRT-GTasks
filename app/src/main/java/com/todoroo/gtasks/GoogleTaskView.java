package com.todoroo.gtasks;

public class GoogleTaskView {
    private final GoogleTaskList activeTaskList;
    private final GoogleTaskListInfo[] otherLists;

    public GoogleTaskView(GoogleTaskList activeTaskList, GoogleTaskListInfo[] otherLists) {
        this.activeTaskList = activeTaskList;
        this.otherLists = otherLists;
    }

    public GoogleTaskList getActiveTaskList() {
        return this.activeTaskList;
    }

    public GoogleTaskListInfo[] getAllLists() {
        return this.otherLists;
    }

    public int getNextListIndex() {
        int max = 0;
        for (GoogleTaskListInfo otherList : this.otherLists) {
            int index = getIndex(otherList.getId());
            if (index > max) {
                max = index;
            }
        }
        return max + 1;
    }

    private int getIndex(String listId) {
        int firstColon = listId.indexOf(58);
        return Integer.parseInt(listId.substring(firstColon + 1, listId.indexOf(58, firstColon + 1)));
    }
}
