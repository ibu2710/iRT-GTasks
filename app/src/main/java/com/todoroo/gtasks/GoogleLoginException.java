package com.todoroo.gtasks;

public class GoogleLoginException extends GoogleTasksException {
    private static final long serialVersionUID = 1;

    public GoogleLoginException(String message) {
        super(message);
    }

    public GoogleLoginException(String message, Throwable cause) {
        super(message, cause);
    }

    public GoogleLoginException(Throwable cause) {
        super(cause);
    }
}
