package com.todoroo.gtasks;

import com.irt.tasks.app.dataaccess.adapter.GoogleTaskContentProvider;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GoogleTaskListParser {
    private static final String[] EMPTY_STRING_ARRAY = new String[0];
    private static final SimpleDateFormat taskDateParser = new SimpleDateFormat("yyyyMMdd");

    public GoogleTaskView parseGoogleTaskView(JSONObject jsonObject) throws JSONException {
        JSONArray array = jsonObject.getJSONArray("lists");
        GoogleTaskListInfo[] list = new GoogleTaskListInfo[array.length()];
        GoogleTaskListInfo active = null;
        for (int i = 0; i < array.length(); i++) {
            JSONObject jsonTaskList = array.getJSONObject(i);
            list[i] = parseGoogleTaskListInfo(jsonTaskList);
            if (jsonTaskList.has("child_entity")) {
                active = list[i];
            }
        }
        return new GoogleTaskView(new GoogleTaskList(active, parseGoogleTaskTasks(jsonObject.getJSONArray("tasks"))), list);
    }

    public GoogleTaskListInfo parseGoogleTaskListInfo(JSONObject jsonTaskListInfo) {
        return new GoogleTaskListInfo(jsonTaskListInfo.optString("id"), jsonTaskListInfo.optString("name"));
    }

    public GoogleTaskTask[] parseGoogleTaskTasks(JSONArray jsonTasks) throws JSONException {
        GoogleTaskTask[] tasks = new GoogleTaskTask[jsonTasks.length()];
        for (int i = 0; i < tasks.length; i++) {
            tasks[i] = parseGoogleTaskTask(jsonTasks.getJSONObject(i));
        }
        return tasks;
    }

    public GoogleTaskTask parseGoogleTaskTask(JSONObject jsonTask) throws JSONException {
        String[] child_ids;
        String id = jsonTask.optString("id");
        String name = jsonTask.optString("name");
        boolean completed = jsonTask.optBoolean("completed");
        boolean deleted = jsonTask.optBoolean("deleted");
        long completed_date = jsonTask.optLong("completed_date");
        long last_modified = jsonTask.optLong("last_modified");
        JSONArray child_id = jsonTask.optJSONArray("child_id");
        if (child_id == null || child_id.length() == 0) {
            child_ids = EMPTY_STRING_ARRAY;
        } else {
            child_ids = new String[child_id.length()];
            for (int i = 0; i < child_ids.length; i++) {
                child_ids[i] = child_id.getString(i);
            }
        }
        String taskDateString = jsonTask.optString("task_date");
        long task_date = 0;
        if (taskDateString.length() > 0) {
            try {
                task_date = taskDateParser.parse(taskDateString).getTime();
            } catch (ParseException e) {
            }
        }
        return new GoogleTaskTask(id, name, completed, deleted, completed_date, last_modified, child_ids, task_date, jsonTask.optString(GoogleTaskContentProvider.NOTES), jsonTask.getJSONArray("list_id").getString(0));
    }
}
