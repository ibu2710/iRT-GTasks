package com.todoroo.gtasks;

import com.irt.tasks.app.common.ElapsedTime;
import com.todoroo.gtasks.GoogleTaskService.ConvenientTaskCreator;
import com.todoroo.gtasks.actions.Action;
import com.todoroo.gtasks.actions.Actions;
import com.todoroo.gtasks.actions.GetTasksAction;
import com.todoroo.gtasks.actions.ListAction;
import com.todoroo.gtasks.actions.ListActions;
import com.todoroo.gtasks.actions.ListActions.TaskCreator;
import com.todoroo.gtasks.actions.ListActions.TaskModifier;
import com.todoroo.gtasks.actions.ListCreationAction;
import com.todoroo.gtasks.actions.TaskCreationListAction;
import java.util.List;

public class Example {
    private static final Actions f79a = new Actions();
    private static final ListActions f80l = new ListActions();

    public void example() throws Throwable {
        GoogleTaskService service = new GoogleTaskService("username@gmail.com", "password");
        GoogleTaskView taskView = service.getTaskView();
        System.out.println(new Tree(taskView.getActiveTaskList().getTasks()).prettyPrint());
        String listId = taskView.getAllLists()[0].getId();
        service.executeActions(f79a.getTasks(listId, false));
        List<GoogleTaskTask> tasks = service.getTasks(listId);
        GetTasksAction getTasksAction = f79a.getTasks(listId, false);
        Action deleteAction = f79a.deleteList(taskView.getAllLists()[1].getId());
        ListCreationAction listCreationAction = f79a.createList(taskView.getNextListIndex(), "mynewlist");
        service.executeActions(getTasksAction, deleteAction);
        tasks = getTasksAction.getGoogleTasks();
        String newListId = listCreationAction.getNewId();
        TaskCreationListAction createTaskAction = ((TaskCreator) ((TaskCreator) f80l.createTask("mynewtask").completed(false)).deleted(false)).done();
        ListAction modificationAction = ((TaskModifier) f80l.modifyTask(((GoogleTaskTask) tasks.get(0)).getId()).taskDate(System.currentTimeMillis() + ElapsedTime.DAY_IN_MILLIS)).done();
        ListAction listRenameAction = f80l.renameList("mynewlistname");
        service.executeListActions(listId, createTaskAction, modificationAction, listRenameAction);
        String newTaskId = createTaskAction.getNewId();
        String secondTaskId = ((ConvenientTaskCreator) service.createTask(listId, "anothernewtask").completed(false)).go();
    }
}
