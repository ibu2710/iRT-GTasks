package com.todoroo.gtasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Tree {
    public final List<Tree> children;
    private boolean isTop;
    public final GoogleTaskTask task;

    private Tree(GoogleTaskTask task) {
        this.children = new ArrayList();
        this.isTop = true;
        this.task = task;
    }

    public Tree(GoogleTaskTask[] tasks) {
        this.children = new ArrayList();
        this.isTop = true;
        this.task = null;
        this.children.addAll(buildForest(tasks));
    }

    public static List<Tree> buildForest(GoogleTaskTask[] tasks) {
        int i = 0;
        Map<String, Tree> taskMap = new HashMap();
        for (GoogleTaskTask task : tasks) {
            taskMap.put(task.getId(), new Tree(task));
        }
        int top = tasks.length;
        for (Tree tree2 : taskMap.values()) {
            for (String childId : tree2.task.getChild_ids()) {
                Tree task2 = (Tree) taskMap.get(childId);
                task2.isTop = false;
                top--;
                tree2.children.add(task2);
            }
        }
        List<Tree> ret = new ArrayList(top);
        int length = tasks.length;
        while (i < length) {
            Tree tree2 = (Tree) taskMap.get(tasks[i].getId());
            if (tree2.isTop) {
                ret.add(tree2);
            }
            i++;
        }
        return ret;
    }

    public List<Integer> findIndex(String taskId) {
        ArrayList<Integer> index = new ArrayList();
        return findIndex(taskId, index) ? index : null;
    }

    private boolean findIndex(String taskId, List<Integer> route) {
        if (this.task != null && this.task.getId().equals(taskId)) {
            return true;
        }
        for (int i = 0; i < this.children.size(); i++) {
            if (((Tree) this.children.get(i)).findIndex(taskId, route)) {
                route.add(Integer.valueOf(i));
                return true;
            }
        }
        return false;
    }

    public String prettyPrint() {
        StringBuilder sb = new StringBuilder();
        sb.append('\n');
        prettyPrint(0, sb);
        return sb.toString();
    }

    private void prettyPrint(int indent, StringBuilder builder) {
        if (this.task != null) {
            indent(indent, builder);
            builder.append(this.task.getName()).append('\n');
        }
        for (Tree child : this.children) {
            child.prettyPrint(this.task == null ? 0 : indent + 2, builder);
        }
    }

    private void indent(int indent, StringBuilder builder) {
        for (int i = 0; i < indent; i++) {
            builder.append(' ');
        }
    }
}
