package com.todoroo.gtasks;

public final class GoogleTaskTask {
    private final String[] child_ids;
    private final boolean completed;
    private final long completed_date;
    private final boolean deleted;
    private final String id;
    private final long last_modified;
    private final String list_id;
    private final String name;
    private final String notes;
    private final long task_date;

    public GoogleTaskTask(String id, String name, boolean completed, boolean deleted, long completed_date, long last_modified, String[] child_ids, long task_date, String notes, String list_id) {
        this.id = id;
        this.name = name;
        this.completed = completed;
        this.deleted = deleted;
        this.completed_date = completed_date;
        this.last_modified = last_modified;
        this.child_ids = child_ids;
        this.task_date = task_date;
        this.notes = notes;
        this.list_id = list_id;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public boolean isCompleted() {
        return this.completed;
    }

    public boolean isDeleted() {
        return this.deleted;
    }

    public long getCompleted_date() {
        return this.completed_date;
    }

    public long getLast_modified() {
        return this.last_modified;
    }

    public String[] getChild_ids() {
        return this.child_ids;
    }

    public long getTask_date() {
        return this.task_date;
    }

    public String getNotes() {
        return this.notes;
    }

    public String getList_id() {
        return this.list_id;
    }
}
