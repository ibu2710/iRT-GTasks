package com.todoroo.gtasks.actions;

import com.irt.tasks.app.dataaccess.adapter.GoogleTaskContentProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ListActions {

    public static abstract class TaskBuilder<Self> {
        private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
        protected final JSONObject entityDelta = new JSONObject();
        protected final JSONObject json = new JSONObject();

        public Self taskDate(long millis) throws JSONException {
            if (millis == 0) {
                this.entityDelta.put("task_date", "");
            } else {
                this.entityDelta.put("task_date", dateFormatter.format(new Date(millis)));
            }
            return (Self) this;
        }

        public Self notes(String notes) throws JSONException {
            this.entityDelta.put(GoogleTaskContentProvider.NOTES, notes);
            return (Self) this;
        }

        public Self deleted(boolean deleted) throws JSONException {
            this.entityDelta.put("deleted", deleted);
            return (Self) this;
        }

        public Self completed(boolean completed) throws JSONException {
            this.entityDelta.put("completed", completed);
            return (Self) this;
        }
    }

    public static abstract class AbstractTaskCreator<Self> extends TaskBuilder<Self> {
        public AbstractTaskCreator(String name) throws JSONException {
            this.json.put("index", 0);
            this.json.put("dest_parent_type", "GROUP");
            this.entityDelta.put("name", name);
            this.entityDelta.put("creator_id", JSONObject.NULL);
            this.entityDelta.put("entity_type", "TASK");
        }

        public Self parentId(String parentId) throws JSONException {
            this.json.put("parent_id", parentId);
            return (Self) this;
        }

        protected TaskCreationListAction done() throws JSONException {
            this.json.put("entity_delta", this.entityDelta);
            return new TaskCreationListAction(this.json, this.json.has("parent_id") ? new String[]{"list_id"} : new String[]{"list_id", "parent_id"});
        }
    }

    public static class TaskCreator extends AbstractTaskCreator<TaskCreator> {
        public TaskCreator(String name) throws JSONException {
            super(name);
        }

        public TaskCreationListAction done() throws JSONException {
            return super.done();
        }
    }

    public static class TaskModifier extends TaskBuilder<TaskModifier> {
        public TaskModifier(String id) throws JSONException {
            this.json.put("id", id);
        }

        public TaskModifier name(String name) throws JSONException {
            this.entityDelta.put("name", name);
            return this;
        }

        public ListAction done() throws JSONException {
            this.json.put("entity_delta", this.entityDelta);
            return new ListAction("update", this.json, null);
        }
    }

    public ListAction renameList(String newName) throws JSONException {
        JSONObject ret = new JSONObject();
        JSONObject entityDelta = new JSONObject();
        entityDelta.put("name", newName);
        ret.put("entity_delta", entityDelta);
        return new ListAction("update", ret, "id");
    }

    public TaskCreator createTask(String name) throws JSONException {
        return new TaskCreator(name);
    }

    public ListAction move(String id, String parentId, String priorSiblingId) throws JSONException {
        JSONObject ret = new JSONObject();
        ret.put("id", id);
        if (parentId != null) {
            ret.put("dest_parent", parentId);
        }
        if (priorSiblingId != null) {
            ret.put("prior_sibling_id", priorSiblingId);
        }
        return new ListAction("move", ret, parentId == null ? new String[]{"source_list", "dest_parent"} : new String[]{"source_list"});
    }

    public ListAction clearCompletedTasks() throws JSONException {
        JSONObject ret = new JSONObject();
        ret.put("clear_list_ids", true);
        return new ListAction("update_user", ret, null);
    }

    public TaskModifier modifyTask(String id) throws JSONException {
        return new TaskModifier(id);
    }
}
