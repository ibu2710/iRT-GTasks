package com.todoroo.gtasks.actions;

import org.json.JSONArray;
import org.json.JSONObject;

public class AbstractAction {
    protected final JSONObject jsonObject;
    private JSONObject result = null;
    private JSONArray tasks = null;

    public AbstractAction(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public void setResult(JSONObject result) {
        this.result = result;
    }

    public void setTasks(JSONArray tasks) {
        this.tasks = tasks;
    }

    protected JSONObject getResult() {
        if (this.result != null) {
            return this.result;
        }
        throw new IllegalStateException("Result has not yet been set. You must first pass this object to GoogleTaskService.executeListActions or GoogleTaskService.executeActions");
    }

    protected JSONArray getTasks() {
        if (this.tasks != null) {
            return this.tasks;
        }
        throw new IllegalStateException("Tasks has not yet been set. You must first pass this object to GoogleTaskService.executeListActions or GoogleTaskService.executeActions.");
    }
}
