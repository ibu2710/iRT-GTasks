package com.todoroo.gtasks.actions;

import com.todoroo.gtasks.GoogleTaskListParser;
import com.todoroo.gtasks.GoogleTaskTask;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class GetTasksAction extends Action {
    private static final GoogleTaskListParser parser = new GoogleTaskListParser();
    private final String listId;

    public GetTasksAction(String listId, boolean includeDeleted) throws JSONException {
        super("get_all", new JSONObject().put("list_id", listId).put("get_deleted", includeDeleted));
        this.listId = listId;
    }

    public List<GoogleTaskTask> getGoogleTasks() throws JSONException {
        GoogleTaskTask[] allTasks = parser.parseGoogleTaskTasks(getTasks());
        List<GoogleTaskTask> ret = new ArrayList();
        for (GoogleTaskTask task : allTasks) {
            if (task.getList_id().equals(this.listId)) {
                ret.add(task);
            }
        }
        return ret;
    }
}
