package com.todoroo.gtasks.actions;

import org.json.JSONException;
import org.json.JSONObject;

public class Actions {
    public ListCreationAction createList(int index, String name) throws JSONException {
        return new ListCreationAction(index, name);
    }

    public GetTasksAction getTasks(String listId, boolean includeDeleted) throws JSONException {
        return new GetTasksAction(listId, includeDeleted);
    }

    public Action moveTask(String id, String sourceList, String destList, String destParent) throws JSONException {
        JSONObject ret = new JSONObject();
        ret.put("id", id);
        ret.put("source_list", sourceList);
        ret.put("dest_list", destList);
        String str = "dest_parent";
        if (destParent != null) {
            destList = destParent;
        }
        ret.put(str, destList);
        return new Action("move", ret);
    }

    public Action deleteList(String listId) throws JSONException {
        JSONObject ret = new JSONObject();
        ret.put("id", listId);
        ret.put("entity_delta", new JSONObject().put("deleted", true));
        return new Action("update", ret);
    }
}
