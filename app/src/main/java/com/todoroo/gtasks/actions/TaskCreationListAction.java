package com.todoroo.gtasks.actions;

import org.json.JSONException;
import org.json.JSONObject;

public class TaskCreationListAction extends ListAction {
    public TaskCreationListAction(JSONObject jsonObject, String... listIdJsonKey) throws JSONException {
        super("create", jsonObject, listIdJsonKey);
    }

    public String getNewId() throws JSONException {
        return getResult().getString("new_id");
    }
}
