package com.todoroo.gtasks.actions;

import org.json.JSONException;
import org.json.JSONObject;

public class ListCreationAction extends Action {
    public ListCreationAction(int index, String name) throws JSONException {
        super("create", new JSONObject().put("index", index).put("entity_delta", new JSONObject().put("name", name).put("creator_id", JSONObject.NULL).put("entity_type", "GROUP")));
    }

    public String getNewId() throws JSONException {
        return getResult().getString("new_id");
    }
}
