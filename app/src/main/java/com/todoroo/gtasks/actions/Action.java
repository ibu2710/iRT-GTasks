package com.todoroo.gtasks.actions;

import org.json.JSONException;
import org.json.JSONObject;

public class Action extends AbstractAction {
    public Action(String actionType, JSONObject jsonObject) throws JSONException {
        super(jsonObject);
        jsonObject.put("action_type", actionType);
    }

    public JSONObject toJson() throws JSONException {
        return this.jsonObject;
    }
}
