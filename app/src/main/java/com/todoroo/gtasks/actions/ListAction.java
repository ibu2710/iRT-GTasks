package com.todoroo.gtasks.actions;

import org.json.JSONException;
import org.json.JSONObject;

public class ListAction extends AbstractAction {
    private final String[] listIdJsonKey;

    public ListAction(String actionType, JSONObject jsonObject, String... listIdJsonKey) throws JSONException {
        super(jsonObject);
        this.listIdJsonKey = listIdJsonKey;
        jsonObject.put("action_type", actionType);
    }

    public JSONObject toJson(String listId) throws JSONException {
        if (this.listIdJsonKey != null) {
            for (String key : this.listIdJsonKey) {
                this.jsonObject.put(key, listId);
            }
        }
        return this.jsonObject;
    }
}
