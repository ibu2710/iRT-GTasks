package com.todoroo.gtasks;

import com.todoroo.gtasks.actions.AbstractAction;
import com.todoroo.gtasks.actions.Action;
import com.todoroo.gtasks.actions.GetTasksAction;
import com.todoroo.gtasks.actions.ListAction;
import com.todoroo.gtasks.actions.ListActions.AbstractTaskCreator;
import com.todoroo.gtasks.actions.TaskCreationListAction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GoogleTaskService {
    private static final String LATEST_SYNC_POINT = "latest_sync_point";
    private int actionId;
    private final ConnectionManager connectionManager;
    private long latestSyncPoint;
    private final GoogleTaskListParser parser;

    private interface ToJson<T> {
        public static final ToJson<Action> ACTION_TO_JSON = new C20161();

        class C20161 implements ToJson<Action> {
            C20161() {
            }

            public JSONObject toJson(Action action) throws JSONException {
                return action.toJson();
            }
        }

        JSONObject toJson(T t) throws JSONException;
    }

    public class ConvenientTaskCreator extends AbstractTaskCreator<ConvenientTaskCreator> {
        private final String listId;

        private ConvenientTaskCreator(String listId, String name) throws JSONException {
            super(name);
            this.listId = listId;
        }

        public String go() throws JSONException, IOException, GoogleLoginException {
            TaskCreationListAction listAction = done();
            GoogleTaskService.this.executeListActions(this.listId, listAction);
            return listAction.getNewId();
        }
    }

    public GoogleTaskService(String username, String password) {
        this(new GoogleConnectionManager(username, password));
    }

    public GoogleTaskService(ConnectionManager connectionManager) {
        this.parser = new GoogleTaskListParser();
        this.actionId = 1;
        this.connectionManager = connectionManager;
    }

    public synchronized GoogleTaskView getTaskView() throws JSONException, IOException, GoogleLoginException {
        JSONObject initialData;
        initialData = this.connectionManager.get().getJSONObject("t");
        this.latestSyncPoint = initialData.getLong(LATEST_SYNC_POINT);
        return this.parser.parseGoogleTaskView(initialData);
    }

    public synchronized List<GoogleTaskTask> getTasks(String listId) throws JSONException, IOException, GoogleLoginException {
        executeActions(new GetTasksAction(listId, false));
        return new GetTasksAction(listId,false).getGoogleTasks();
    }

    public synchronized void executeListActions(final String listId, ListAction... actions) throws JSONException, IOException, GoogleLoginException {
        executeAbstractActions(new ToJson<ListAction>() {
            public JSONObject toJson(ListAction action) throws JSONException {
                return action.toJson(listId);
            }
        }, new JSONObject().put("current_list_id", listId), actions);
    }

    public synchronized void executeActions(Action... actions) throws JSONException, IOException, GoogleLoginException {
        executeAbstractActions(ToJson.ACTION_TO_JSON, new JSONObject(), actions);
    }

    private synchronized <T extends AbstractAction> void executeAbstractActions(ToJson<T> toJson, JSONObject jsonRequest, T... actions) throws JSONException, IOException, GoogleLoginException {
        Map<Integer, T> actionMap = new HashMap(actions.length);
        JSONArray actionList = new JSONArray();
        for (T action : actions) {
            int thisActionId = this.actionId;
            this.actionId = thisActionId + 1;
            actionMap.put(Integer.valueOf(thisActionId), action);
            JSONObject jsonAction = toJson.toJson(action);
            jsonAction.put("action_id", thisActionId);
            actionList.put(jsonAction);
        }
        jsonRequest.put("action_list", actionList);
        jsonRequest.put("client_version", 12743913);
        jsonRequest.put(LATEST_SYNC_POINT, this.latestSyncPoint);
        JSONObject jsonObject = this.connectionManager.post(jsonRequest);
        if (jsonObject.has(LATEST_SYNC_POINT)) {
            this.latestSyncPoint = jsonObject.getLong(LATEST_SYNC_POINT);
        }
        JSONArray results = jsonObject.getJSONArray("results");
        for (int i = 0; i < results.length(); i++) {
            JSONObject result = results.getJSONObject(i);
            ((AbstractAction) actionMap.get(Integer.valueOf(result.getInt("action_id")))).setResult(result);
        }
        if (jsonObject.has("tasks")) {
            JSONArray tasks = jsonObject.getJSONArray("tasks");
            for (T action2 : actions) {
                action2.setTasks(tasks);
            }
        }
    }

    public ConvenientTaskCreator createTask(String listId, String name) throws JSONException {
        return new ConvenientTaskCreator(listId, name);
    }
}
