package com.todoroo.gtasks;

public final class GoogleTaskList {
    private final GoogleTaskListInfo info;
    private final GoogleTaskTask[] tasks;

    public GoogleTaskList(GoogleTaskListInfo info, GoogleTaskTask[] tasks) {
        this.info = info;
        this.tasks = tasks;
    }

    public GoogleTaskListInfo getInfo() {
        return this.info;
    }

    public GoogleTaskTask[] getTasks() {
        return this.tasks;
    }
}
