package com.irt.tasks.ui.task;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Email;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessprocess.EmailBp;
import com.irt.tasks.app.businessprocess.EmailBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.CSVWriter;
import com.irt.tasks.app.common.DateUtil;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.sync.DropboxSyncHelper;

import java.util.Calendar;
import java.util.Date;

public class ReminderEditActivity extends AbstractActivity {
    static final int TIME_DIALOG_ID = 0;
    ConfigManager config;
    EmailBp emailBp = new EmailBpImpl();
    Spinner mAutoSnoozeDurationSpinner;
    TextView mAutoSnoozeDurationTextView;
    Button mCancelButton;
    DaysEarlier[] mDaysEarlierArray = new DaysEarlier[]{new DaysEarlier(0, "On due date"), new DaysEarlier(1, "1 day"), new DaysEarlier(2, "2 days"), new DaysEarlier(3, "3 days"), new DaysEarlier(4, "4 days"), new DaysEarlier(5, "5 days"), new DaysEarlier(6, "6 days"), new DaysEarlier(7, "1 week"), new DaysEarlier(8, "8 days"), new DaysEarlier(9, "9 days"), new DaysEarlier(10, "10 days"), new DaysEarlier(11, "11 days"), new DaysEarlier(12, "12 days"), new DaysEarlier(13, "13 days"), new DaysEarlier(14, "2 weeks"), new DaysEarlier(15, "15 days"), new DaysEarlier(16, "16 days"), new DaysEarlier(17, "17 days"), new DaysEarlier(18, "18 days"), new DaysEarlier(19, "19 days"), new DaysEarlier(20, "20 days"), new DaysEarlier(21, "3 weeks"), new DaysEarlier(28, "4 weeks"), new DaysEarlier(30, "30 days"), new DaysEarlier(40, "40 days")};
    Spinner mDaysEarlierSpinner;
    TextView mDueDateDescriptionTextView;
    TextView mEmailNotifciationNoteTextView;
    CheckBox mEnableReminder;
    EditText mExtraEmailsEditText;
    boolean mIsDaysEarlierRendered = false;
    TextView mNextAlarmDate;
    TextView mNextAlarmDateLabelTextView;
    CheckBox mNotifyByEmailCheckBox;
    CheckBox mNotifyExtraEmailsCheckBox;
    Button mOkButton;
    Calendar mReminderDateTime = Calendar.getInstance();
    Button mReminderTime;
    CheckBox mRepeatRromCompletionDateCheckbox;
    Button mResetButton;
    ImageButton mSnoozeRemoveImabeButton;
    Task mTask;
    TextView mTaskNameTextView;

    private class DaysEarlier {
        public int days = 0;
        public String name = "";

        public DaysEarlier(int _days, String _name) {
            this.days = _days;
            this.name = _name;
        }

        public String toString() {
            return this.name;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.config = ConfigManager.getInstance();
        setContentView(R.layout.reminder_edit);
        AppUtil.turnOffAutoKeyboardPopUp(this);
        Object obj = ArchitectureContext.getObject();
        if (obj != null && (obj instanceof Task)) {
            this.mTask = (Task) ArchitectureContext.getObject();
        }
        initView();
        setCancelButtonListener();
        setOkButtonListener();
        setResetButtonListener();
        setReminderTimeListener();
        setDaysEarlierSpinnerListener();
        setEnableReminderListener();
        initAutoSnoozeDurationSpinner();
        setSnoozeRemoveListener();
        setDueDateDescription();
    }

    private void setDueDateDescription() {
        this.mDueDateDescriptionTextView.setText(AppUtil.getDueDateDescription(this.mTask, "EEE, MMM d, yyyy", false));
    }

    private void setEnableReminderListener() {
        this.mEnableReminder.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ReminderEditActivity.this.clearSnooze();
            }
        });
    }

    private void initView() {
        this.mTaskNameTextView = (TextView) findViewById(R.id.task_name_label);
        this.mEnableReminder = (CheckBox) findViewById(R.id.enable_reminder_checkbox);
        this.mDaysEarlierSpinner = (Spinner) findViewById(R.id.days_ealier_spinner);
        this.mReminderTime = (Button) findViewById(R.id.reminder_time);
        this.mRepeatRromCompletionDateCheckbox = (CheckBox) findViewById(R.id.repeat_from_completion_date_checkbox);
        this.mResetButton = (Button) findViewById(R.id.reset_button);
        this.mOkButton = (Button) findViewById(R.id.ok_button);
        this.mCancelButton = (Button) findViewById(R.id.cancel_button);
        this.mNextAlarmDate = (TextView) findViewById(R.id.next_alarm);
        this.mEmailNotifciationNoteTextView = (TextView) findViewById(R.id.email_notification_label);
        this.mNotifyByEmailCheckBox = (CheckBox) findViewById(R.id.notify_by_email_checkbox);
        this.mNotifyExtraEmailsCheckBox = (CheckBox) findViewById(R.id.notify_extra_emails_checkbox);
        this.mExtraEmailsEditText = (EditText) findViewById(R.id.extra_emails);
        DropboxSyncHelper.addDelayedIdleToEditText(this.mExtraEmailsEditText);
        this.mNextAlarmDateLabelTextView = (TextView) findViewById(R.id.next_alarm_label);
        this.mSnoozeRemoveImabeButton = (ImageButton) findViewById(R.id.snooze_remove);
        this.mAutoSnoozeDurationSpinner = (Spinner) findViewById(R.id.auto_snooze_duration_spinner);
        this.mAutoSnoozeDurationTextView = (TextView) findViewById(R.id.auto_snooze_auto_duration_label);
        this.mDueDateDescriptionTextView = (TextView) findViewById(R.id.due_date_description);
        initViewValues();
    }

    private void initViewValues() {
        this.mEnableReminder.setChecked(this.mTask.isReminderEnabled().booleanValue());
        this.mDaysEarlierSpinner.setAdapter(getDaysEarlierArrayAdapter());
        this.mDaysEarlierSpinner.setSelection(getDaysEarlierPosition());
        this.mReminderDateTime.setTimeInMillis(this.mTask.getReminderDateTime().longValue());
        updateReminderTime();
        updateNextAlarmDateTime();
        this.mNotifyByEmailCheckBox.setChecked(this.mTask.isNotifyByEmail().booleanValue());
        this.mNotifyExtraEmailsCheckBox.setChecked(this.mTask.isNotifyExtraEmails().booleanValue());
        this.mExtraEmailsEditText.setText(this.mTask.getExtraEmails());
        this.mNotifyByEmailCheckBox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ReminderEditActivity.this.setEmailControls();
                ReminderEditActivity.this.mEnableReminder.setChecked(true);
            }
        });
        this.mNotifyExtraEmailsCheckBox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ReminderEditActivity.this.mNotifyExtraEmailsCheckBox.isChecked()) {
                    ReminderEditActivity.this.setExtraMailsContent();
                }
                ReminderEditActivity.this.setEmailControls();
                ReminderEditActivity.this.mEnableReminder.setChecked(true);
            }
        });
        setEmailControls();
    }

    private void setExtraMailsContent() {
        if (StringUtil.isNullOrEmpty(this.mExtraEmailsEditText.getText().toString())) {
            Email email = this.emailBp.get();
            if (email != null) {
                this.mExtraEmailsEditText.setText(email.getExtraEmail());
            }
        }
    }

    private void setEmailControls() {
        boolean z;
        boolean z2 = true;
        if (isApplicationEmailNotificationEnabled()) {
            this.mEmailNotifciationNoteTextView.setVisibility(View.GONE);
        } else {
            this.mEmailNotifciationNoteTextView.setVisibility(View.VISIBLE);
        }
        this.mNotifyByEmailCheckBox.setEnabled(isApplicationEmailNotificationEnabled());
        CheckBox checkBox = this.mNotifyExtraEmailsCheckBox;
        if (isApplicationEmailNotificationEnabled() && this.mNotifyByEmailCheckBox.isChecked()) {
            z = true;
        } else {
            z = false;
        }
        checkBox.setEnabled(z);
        EditText editText = this.mExtraEmailsEditText;
        if (!(isApplicationEmailNotificationEnabled() && this.mNotifyByEmailCheckBox.isChecked() && this.mNotifyExtraEmailsCheckBox.isChecked())) {
            z2 = false;
        }
        editText.setEnabled(z2);
    }

    private boolean isApplicationEmailNotificationEnabled() {
        return this.config.isReminderEmailValidated() && this.config.isEnableReminderEmail();
    }

    private ArrayAdapter<DaysEarlier> getDaysEarlierArrayAdapter() {
        ArrayAdapter<DaysEarlier> spinnerArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, this.mDaysEarlierArray);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return spinnerArrayAdapter;
    }

    private void setReminderTimeListener() {
        this.mReminderTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ReminderEditActivity.this.showDialog(0);
            }
        });
    }

    private void setSnoozeRemoveListener() {
        this.mSnoozeRemoveImabeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ReminderEditActivity.this.clearSnooze();
            }
        });
    }

    private void clearSnooze() {
        this.mTask.setSnoozeDateTime(Long.valueOf(0));
        updateNextAlarmDateTime();
    }

    protected void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case 0:
                ((TimePickerDialog) dialog).updateTime(this.mReminderDateTime.get(Calendar.HOUR_OF_DAY), this.mReminderDateTime.get(12));
                return;
            default:
                return;
        }
    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return new TimePickerDialog(this, new OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        ReminderEditActivity.this.mEnableReminder.setChecked(true);
                        ReminderEditActivity.this.mReminderDateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        ReminderEditActivity.this.mReminderDateTime.set(Calendar.MINUTE, minute);
                        ReminderEditActivity.this.updateReminderTime();
                        ReminderEditActivity.this.updateNextAlarmDateTime();
                    }
                }, this.mReminderDateTime.get(Calendar.HOUR_OF_DAY), this.mReminderDateTime.get(Calendar.MINUTE), false);
            default:
                return null;
        }
    }

    private void updateReminderTime() {
        this.mReminderTime.setText(DateUtil.format(this.mReminderDateTime, AppUtil.getReminderTimeFormat()));
    }

    private void updateNextAlarmDateTime() {
        long reminderDateTimeMillis = this.mReminderDateTime.getTimeInMillis();
        Date reminderSnoozeDateTime = this.mReminderDateTime.getTime();
        SpannableStringBuilder nextAlarmString = new SpannableStringBuilder();
        long todayDateTimeMillis = AppUtil.getTodayDateTime().getTime();
        if (this.mTask.getSnoozeDateTime().longValue() > 0 && this.mReminderDateTime.getTimeInMillis() > todayDateTimeMillis) {
            this.mTask.setSnoozeDateTime(Long.valueOf(0));
        }
        if (this.mTask.getSnoozeDateTime().longValue() > 0) {
            this.mNextAlarmDateLabelTextView.setText("Snooze:");
            this.mSnoozeRemoveImabeButton.setVisibility(0);
            reminderDateTimeMillis = this.mTask.getSnoozeDateTime().longValue();
            reminderSnoozeDateTime = new Date(reminderDateTimeMillis);
            nextAlarmString.append(new SpannableString(" In " + AppUtil.formatTimeBeforeNextAlarm((((double) (reminderDateTimeMillis - todayDateTimeMillis)) / 1000.0d) / 60.0d)) + CSVWriter.DEFAULT_LINE_END);
            nextAlarmString.append("   " + DateUtil.format(reminderSnoozeDateTime, AppUtil.getNextAlarmFormat(true)));
        } else {
            this.mNextAlarmDateLabelTextView.setText("Next alarm:");
            this.mSnoozeRemoveImabeButton.setVisibility(8);
            boolean isReminderInThePastOrDone = reminderDateTimeMillis < todayDateTimeMillis || this.mTask.isCompleted().booleanValue();
            if (!isReminderInThePastOrDone) {
                nextAlarmString.append(new SpannableString(" In " + AppUtil.formatTimeBeforeNextAlarm((((double) (reminderDateTimeMillis - todayDateTimeMillis)) / 1000.0d) / 60.0d)) + CSVWriter.DEFAULT_LINE_END);
            }
            nextAlarmString.append("   " + DateUtil.format(reminderSnoozeDateTime, AppUtil.getNextAlarmFormat(!isReminderInThePastOrDone)));
            if (isReminderInThePastOrDone) {
                SpannableString past = new SpannableString(this.mTask.isCompleted().booleanValue() ? " (Done)" : " (Past)");
                past.setSpan(new ForegroundColorSpan(-65536), 0, past.length(), 0);
                nextAlarmString.append(past);
            }
        }
        this.mNextAlarmDate.setText(nextAlarmString);
    }

    private void setCancelButtonListener() {
        this.mCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ReminderEditActivity.this.finish();
            }
        });
    }

    private void setResetButtonListener() {
        this.mResetButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ReminderEditActivity.this.clearSnooze();
                ReminderEditActivity.this.mEnableReminder.setChecked(true);
                ReminderEditActivity.this.mDaysEarlierSpinner.setSelection(0);
                ReminderEditActivity.this.mNotifyByEmailCheckBox.setChecked(false);
                ReminderEditActivity.this.mNotifyExtraEmailsCheckBox.setChecked(false);
                AppUtil.calculateReminderDateTime(ReminderEditActivity.this.mTask.getDueDate(), ReminderEditActivity.this.mTask, true);
                ReminderEditActivity.this.mReminderDateTime.setTimeInMillis(ReminderEditActivity.this.mTask.getReminderDateTime().longValue());
                ReminderEditActivity.this.mAutoSnoozeDurationSpinner.setSelection(1);
                ReminderEditActivity.this.setEmailControls();
                ReminderEditActivity.this.updateReminderTime();
                ReminderEditActivity.this.updateNextAlarmDateTime();
                ReminderEditActivity.this.setAutoSnoozeDurationTextView();
            }
        });
    }

    private void initAutoSnoozeDurationSpinner() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.task_auto_snooze_duration));
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.mAutoSnoozeDurationSpinner.setAdapter(spinnerArrayAdapter);
        this.mAutoSnoozeDurationSpinner.setSelection(AppUtil.getPositionFromArrayValue(this, R.array.task_auto_snooze_duration_values, this.mTask.getAutoSnoozeDuration().intValue()));
        this.mAutoSnoozeDurationSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (ReminderEditActivity.this.mAutoSnoozeDurationSpinner.getSelectedItemId() == 1) {
                    ReminderEditActivity.this.setAutoSnoozeDurationTextView();
                } else {
                    ReminderEditActivity.this.mAutoSnoozeDurationTextView.setVisibility(8);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setAutoSnoozeDurationTextView() {
        this.mAutoSnoozeDurationTextView.setVisibility(0);
        this.mAutoSnoozeDurationTextView.setText(AppUtil.getAutoSnoozeDurationDescription(this));
    }

    private void setOkButtonListener() {
        this.mOkButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (validateExtraEmails()) {
                    ReminderEditActivity.this.mTask.setReminderEnabled(Boolean.valueOf(ReminderEditActivity.this.mEnableReminder.isChecked()));
                    ReminderEditActivity.this.mTask.setReminderDateTime(Long.valueOf(ReminderEditActivity.this.mReminderDateTime.getTimeInMillis()));
                    ReminderEditActivity.this.mTask.setReminderDaysEarlier(Integer.valueOf(ReminderEditActivity.this.getDaysEarlierValue()));
                    ReminderEditActivity.this.mTask.setNotifyByEmail(Boolean.valueOf(ReminderEditActivity.this.mNotifyByEmailCheckBox.isChecked()));
                    ReminderEditActivity.this.mTask.setNotifyExtraEmails(Boolean.valueOf(ReminderEditActivity.this.mNotifyExtraEmailsCheckBox.isChecked()));
                    ReminderEditActivity.this.mTask.setExtraEmails(ReminderEditActivity.this.mExtraEmailsEditText.getText().toString());
                    ReminderEditActivity.this.mTask.setAutoSnoozeDuration(new Integer(ReminderEditActivity.this.getResources().getStringArray(R.array.task_auto_snooze_duration_values)[ReminderEditActivity.this.mAutoSnoozeDurationSpinner.getSelectedItemPosition()]));
                    ReminderEditActivity.this.finish();
                }
            }

            private boolean validateExtraEmails() {
                String message = null;
                String extraEmails = ReminderEditActivity.this.mExtraEmailsEditText.getText().toString();
                if (StringUtil.isNullOrEmpty(extraEmails)) {
                    ReminderEditActivity.this.mNotifyExtraEmailsCheckBox.setChecked(false);
                } else if (!AppUtil.validateExtraEmails(ReminderEditActivity.this.mExtraEmailsEditText, extraEmails)) {
                    message = "Extra emails invalid format";
                }
                boolean isValid = StringUtil.isNullOrEmpty(message);
                if (!isValid) {
                    AppUtil.showGenericWarningMessage(ReminderEditActivity.this, "Extra emails validation", message);
                }
                return isValid;
            }
        });
    }

    private int getDaysEarlierValue() {
        return ((DaysEarlier) this.mDaysEarlierSpinner.getSelectedItem()).days;
    }

    private int getDaysEarlierPosition() {
        for (int i = 0; i < this.mDaysEarlierArray.length; i++) {
            if (this.mTask.getReminderDaysEarlier().intValue() == this.mDaysEarlierArray[i].days) {
                return i;
            }
        }
        return 0;
    }

    private void setDaysEarlierSpinnerListener() {
        this.mDaysEarlierSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                if (ReminderEditActivity.this.mIsDaysEarlierRendered) {
                    ReminderEditActivity.this.mEnableReminder.setChecked(true);
                }
                ReminderEditActivity.this.mIsDaysEarlierRendered = true;
                EditTaskHelper.calculateReminderDate(((DaysEarlier) ReminderEditActivity.this.mDaysEarlierSpinner.getSelectedItem()).days, ReminderEditActivity.this.mTask, ReminderEditActivity.this.mReminderDateTime);
                ReminderEditActivity.this.updateNextAlarmDateTime();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }
}
