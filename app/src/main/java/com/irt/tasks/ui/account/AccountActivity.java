package com.irt.tasks.ui.account;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.tasks.Tasks;
import com.google.api.services.tasks.TasksScopes;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessprocess.AccountBp;
import com.irt.tasks.app.businessprocess.AccountBpImpl;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.ui.AbstractListActivity;
import com.irt.tasks.ui.dragndrop.DragListener;
import com.irt.tasks.ui.dragndrop.DragNDropListView;
import com.irt.tasks.ui.dragndrop.DropListener;
import com.irt.tasks.ui.dragndrop.RemoveListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AccountActivity extends AbstractListActivity {
    private static final String PREF_ACCOUNT_NAME = "accountName";
    protected static final int REQUEST_ACCOUNT_PICKER = 2;
    protected static final int REQUEST_AUTHORIZATION = 1;
    protected static final int REQUEST_GOOGLE_PLAY_SERVICES = 0;
    AccountBp accountBp = new AccountBpImpl();
    final HttpTransport httpTransport = AndroidHttp.newCompatibleTransport();
    final JsonFactory jsonFactory = GsonFactory.getDefaultInstance();
    Account mAccount;
    List<Account> mAccounts;
    Button mAddAccount;
    protected GoogleAccountCredential mCredential;
    boolean mIsSaveSortedAccounts;
    Tasks mService;
    int numAsyncTasks;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_dragndroplistview);
        ArrayList<Account> content = initAccounts();
        setListeners(setListView(content));
        this.mAddAccount = (Button) findViewById(R.id.add_account_button);
        if (content.size() > 1) {
            this.mAddAccount.setText("Edit or Add Account");
        } else {
            this.mAddAccount.setText("Add Account");
        }
        this.mAddAccount.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new Builder(AccountActivity.this).setTitle("BETA: Google Tasks Sync").setMessage("Google tasks sync is still on BETA stage, your tasks lists might not sync properly during this beta period. It is encouraged to use the Offline Tasks account for serious lists and use Google tasks list for simple non critical lists.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AccountActivity.this.mAccount = null;
                        AccountActivity.this.chooseAccount();
                    }
                }).show();
            }
        });
        setGoogleAccountPicker();
    }

    protected void onResume() {
        super.onResume();
        onCreate(null);
    }

    private ListView setListView(ArrayList<Account> content) {
        setListAdapter(new AccountAdapter(this, content));
        return getListView();
    }

    private ArrayList<Account> initAccounts() {
        this.mAccounts = this.accountBp.getAll();
        ArrayList<Account> content = new ArrayList(this.mAccounts.size());
        for (int i = 0; i < this.mAccounts.size(); i++) {
            content.add(this.mAccounts.get(i));
        }
        return content;
    }

    private void setListeners(ListView listView) {
        if (listView instanceof DragNDropListView) {
            ((DragNDropListView) listView).enableDragMode(true);
            ((DragNDropListView) listView).setDropListener(new DropListener() {
                @Override
                public void onDrop(int from, int to) {
                    ListAdapter adapter = AccountActivity.this.getListAdapter();
                    if (adapter instanceof AccountAdapter) {
                        ((AccountAdapter) adapter).onDrop(from, to);
                        AccountActivity.this.getListView().invalidateViews();
                    }
                }
            });
            ((DragNDropListView) listView).setRemoveListener(new RemoveListener() {
                @Override
                public void onRemove(int i) {

                }
            });
            ((DragNDropListView) listView).setDragListener(new DragListener() {
                int defaultBackgroundColor;
                public void onDrag(int x, int y, ListView listView, int startPostion) {
                }

                public void setDisplacedListItem(Object displacedItem, View itemView) {
                    AccountAdapterHelper.getView((Account) displacedItem, itemView, null);
                }

                public void blankOutDraggedItemListItem(View itemView) {
                    ((TextView) itemView.findViewById(R.id.TextView01)).setText("");
                    ((TextView) itemView.findViewById(R.id.auto_sync)).setText("");
                    ((TextView) itemView.findViewById(R.id.enabled)).setText("");
                }

                public void onStartDrag(View itemView, int position) {
                    itemView.setVisibility(View.INVISIBLE);
                    this.defaultBackgroundColor = itemView.getDrawingCacheBackgroundColor();
                    ImageView iv = (ImageView) itemView.findViewById(R.id.ImageView01);
                    if (iv != null) {
                        iv.setVisibility(View.INVISIBLE);
                    }
                }

                public void onStopDrag(View itemView) {
                    if (itemView != null) {
                        itemView.setVisibility(View.VISIBLE);
                        itemView.setBackgroundColor(this.defaultBackgroundColor);
                        ImageView iv = (ImageView) itemView.findViewById(R.id.ImageView01);
                        if (iv != null) {
                            iv.setVisibility(View.VISIBLE);
                        }
                    }
                    AccountActivity.this.mIsSaveSortedAccounts = true;
                }

                public ListAdapter getMyListAdapter() {
                    return AccountActivity.this.getListAdapter();
                }
            });
        }
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AccountActivity.this.saveSortedAccounts();
                AccountActivity.this.mAccount = (Account) AccountActivity.this.mAccounts.get(position);
                if (DatabaseHelper.OFFLINE_ACCOUNT_ID == AccountActivity.this.mAccount.getId() || AccountActivity.this.mAccount.isNewGoogleId().booleanValue()) {
                    ArchitectureContext.setObject(AccountActivity.this.mAccount);
                    AccountActivity.this.editActivity();
                    return;
                }
                new Builder(AccountActivity.this).setTitle("Old Google Sync ID").setMessage(R.string.delete_this_account_due_to_old_sync_id).setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ArchitectureContext.setObject(AccountActivity.this.mAccount);
                        AccountActivity.this.editActivity();
                    }
                }).show();
            }
        });
    }

    protected void editActivity() {
        startActivity(new Intent(this, AccountEditActivity.class));
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        try {
            saveSortedAccounts();
            finish();
            return super.onKeyDown(keyCode, event);
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return true;
        }
    }

    private void saveSortedAccounts() {
        if (this.mIsSaveSortedAccounts) {
            for (Account account : ((AccountAdapter) getListAdapter()).getSortedAccounts()) {
                this.accountBp.saveOrUpdate(account);
            }
            this.mIsSaveSortedAccounts = false;
        }
    }

    protected void setGoogleAccountPicker() {
        this.mCredential = GoogleAccountCredential.usingOAuth2(this, Collections.singleton(TasksScopes.TASKS));
        this.mService = new Tasks.Builder(this.httpTransport, this.jsonFactory, this.mCredential).setApplicationName("Google-TasksAndroidSample/1.0").build();
    }

    void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        runOnUiThread(new Runnable() {
            public void run() {
                GooglePlayServicesUtil.getErrorDialog(connectionStatusCode, AccountActivity.this, 0).show();
            }
        });
    }

    void refreshView() {
        onResume();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String accountName;
        switch (requestCode) {
            case 0:
                if (resultCode == -1) {
                    chooseAccount();
                    return;
                } else {
                    checkGooglePlayServicesAvailable();
                    return;
                }
            case 1:
                if (resultCode == -1) {
                    accountName = data.getExtras().getString("authAccount");
                    if (accountName != null) {
                        setAccountCredential(accountName);
                        AsyncListLoadTasks.run(this);
                        return;
                    }
                    return;
                }
                chooseAccount();
                return;
            case 2:
                if (resultCode == -1 && data != null && data.getExtras() != null) {
                    accountName = data.getExtras().getString("authAccount");
                    if (accountName != null) {
                        setAccountCredential(accountName);
                        Editor editor = getPreferences(0).edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.commit();
                        AsyncListLoadTasks.run(this);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void setAccountCredential(String accountName) {
        for (Account account : this.mAccounts) {
            if (account.getEmail().equals(accountName)) {
                this.mAccount = account;
                ArchitectureContext.setObject(this.mAccount);
            }
        }
        if (this.mAccount == null) {
            Account newAccount = new Account();
            newAccount.setSortPosition(Integer.valueOf(this.mAccounts.size()));
            newAccount.setEmail(accountName);
            ArchitectureContext.setObject(newAccount);
        }
        this.mCredential.setSelectedAccountName(accountName);
    }

    private boolean checkGooglePlayServicesAvailable() {
        int connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (!GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            return true;
        }
        showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        return false;
    }

    protected void chooseAccount() {
        startActivityForResult(this.mCredential.newChooseAccountIntent(), 2);
    }
}
