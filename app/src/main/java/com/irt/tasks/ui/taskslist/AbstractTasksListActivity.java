package com.irt.tasks.ui.taskslist;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources.Theme;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.AccountBpImpl;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.businessprocess.TasksListBp;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.EmailUtility;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.importexport.ImportExportBonsaiActivity;
import com.irt.tasks.ui.taskslist.OptionsMenuHelper.MonitorSync;
import com.irt.tasks.ui.taskslist.OptionsMenuHelper.SyncWithGoogleTasks;
import com.irt.tasks.ui.taskslist.extras.ListColorActivity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTasksListActivity extends AbstractMainListActivity {
    int mFirstVisibleTask;
    int mSelectedTaskPosition = 0;
    int mSortScope = 0;
    public Task mTaskDecorator = new Task();
    List<Task> mTasks = new ArrayList();
    TasksList mTasksList;
    TaskBp taskBp = new TaskBpImpl();

    class C19231 implements OnClickListener {
        C19231() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
        }
    }

    class C19242 implements OnClickListener {
        C19242() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            new SyncWithGoogleTasks(AbstractTasksListActivity.this, AbstractTasksListActivity.this.mTasksList).execute(new Void[0]);
            new MonitorSync().execute(new Void[0]);
        }
    }

    class C19253 implements OnClickListener {
        C19253() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
        }
    }

    class C19264 implements OnClickListener {
        C19264() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
        }
    }

    class C19275 implements OnClickListener {
        C19275() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
        }
    }

    class C19286 implements OnClickListener {
        C19286() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
        }
    }

    class C19308 implements OnClickListener {
        C19308() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            if (ConfigManager.getInstance().isSyncing()) {
                Toast.makeText(AbstractTasksListActivity.this, "Sync is on going, try again later...", Toast.LENGTH_LONG).show();
                return;
            }
            new ProcessTaskAction(AbstractTasksListActivity.this, 12, AbstractTasksListActivity.this.mSortScope).execute(new TasksList[]{AbstractTasksListActivity.this.mTasksList});
        }
    }

    class C19319 implements OnClickListener {
        C19319() {
        }

        public void onClick(DialogInterface dialog, int sortScope) {
            AbstractTasksListActivity.this.mSortScope = sortScope;
        }
    }

    class ProcessTaskAction extends AsyncTask<TasksList, Void, Boolean> {
        int mAction;
        Context mContext;
        int mSortScope;
        private ProgressDialog progressDialog;
        TaskBp taskBp = new TaskBpImpl();

        public ProcessTaskAction(Context context, int action, int sortScope) {
            this.mContext = context;
            this.mAction = action;
            this.mSortScope = sortScope;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = ProgressDialog.show(this.mContext, null, "Processing, please wait...", true, true);
        }

        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            AbstractTasksListActivity.this.refreshDisplay(null);
            this.progressDialog.dismiss();
            Toast.makeText(this.mContext, "Completed processing action.", Toast.LENGTH_LONG).show();
        }

        protected Boolean doInBackground(TasksList... tasksList) {
            switch (this.mAction) {
                case 0:
                    this.taskBp.deleteCompleted(tasksList[0]);
                    break;
                case 7:
                    AbstractTasksListActivity.this.tasksListBp.overwriteLocalTasks(tasksList[0]);
                    break;
                case 12:
                    this.taskBp.sortPermanent(tasksList[0], this.mSortScope);
                    break;
                case 14:
                    this.taskBp.uncheckCompleted(tasksList[0]);
                    break;
            }
            return Boolean.valueOf(true);
        }
    }

    public class ProcessTaskActionProgressBar extends AsyncTask<TasksList, Void, Long> {
        int mAction;
        Context mContext;
        String mMessage;
        private ProgressDialog progressDialog;
        TasksListBp tasksListBp = new TasksListBpImpl();

        public ProcessTaskActionProgressBar(Context context, int action, String message) {
            this.mContext = context;
            this.mAction = action;
            this.mMessage = message;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = ProgressDialog.show(AbstractTasksListActivity.this, null, this.mMessage, true, true);
        }

        protected void onPostExecute(Long result) {
            super.onPostExecute(result);
            this.progressDialog.dismiss();
            Toast.makeText(this.mContext, "Completed processing action.", Toast.LENGTH_LONG).show();
            AppUtil.refreshDisplay();
        }

        protected Long doInBackground(TasksList... tasksList) {
            Long valueOf;
            synchronized (this) {
                if (!ConfigManager.getInstance().isSyncing()) {
                    switch (this.mAction) {
                        case 6:
                            this.tasksListBp.overwriteGoogleTasks(tasksList[0]);
                            break;
                    }
                }
                valueOf = Long.valueOf(0);
            }
            return valueOf;
        }
    }

    public abstract String getEmailVisibleTasksBody();

    public abstract void refreshDisplay(Long l);

    public abstract void updateListItem(int i, boolean z);

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    void setTaskPosition(int position) {
        this.mSelectedTaskPosition = position;
    }

    void performShowHideNumbers() {
        ConfigManager config = ConfigManager.getInstance();
        if (config.isShowTaskNumbers()) {
            config.setShowTaskNumbers(false);
        } else {
            config.setShowTaskNumbers(true);
        }
        onResume();
    }

    public void performAction(int whichAction) {
        ConfigManager config = ConfigManager.getInstance();
        boolean isAllTasksList = DatabaseHelper.ALL_TASKS_LIST_ID.equals(this.mTasksList.getId());
        boolean isAccountOffline = DatabaseHelper.OFFLINE_ACCOUNT_ID.equals(this.mTasksList.getAccountId());
        Intent intent;
        Account account;
        switch (whichAction) {
            case 0:
                new ProcessTaskAction(this, 0, 0).execute(new TasksList[]{this.mTasksList});
                return;
            case 1:
                if (config.getRightTapAction().intValue() == 30) {
                    config.setRightTapAction(Integer.valueOf(65));
                    return;
                } else {
                    config.setRightTapAction(Integer.valueOf(30));
                    return;
                }
            case 2:
                this.mFirstVisibleTask = 0;
                setTaskPosition(0);
                refreshDisplay(null);
                return;
            case 3:
                this.mFirstVisibleTask = this.mTasks.size();
                setTaskPosition(this.mTasks.size());
                refreshDisplay(null);
                return;
            case 4:
                if (isAllTasksList) {
                    AppUtil.showGenericMessage(this, "Single list support only", "Import/Export is not for ALL TASKS LIST. Please try it on specific list by going back and select a list.");
                    return;
                }
                ArchitectureContext.setObject(this.mTasksList);
                intent = new Intent(this, ImportExportBonsaiActivity.class);
                intent.putExtra("taskListId", this.mTasksList.getId());
                startActivity(intent);
                return;
            case 5:
                if (isAllTasksList) {
                    AppUtil.showGenericMessage(this, "Single list support only", "Level Color is not for ALL TASKS LIST. Please try it on specific list by going back and select a list.");
                    return;
                }
                intent = new Intent(this, ListColorActivity.class);
                intent.putExtra(Constants.TASK_LIST, this.mTasksList.getId());
                startActivity(intent);
                return;
            case 6:
                if (isAllTasksList) {
                    AppUtil.showGenericMessage(this, "Single list support only", "Overwrite Google changes is not for ALL TASKS LIST.  Please try it on specific list by going back and select a list.");
                    return;
                } else if (isAccountOffline) {
                    AppUtil.showGenericMessage(this, "Online Account only", "Overwrite Google changes is NOT for Offline Account.");
                    return;
                } else if (this.mTasksList.istActiveTasksList().booleanValue()) {
                    AppUtil.showGenericMessage(this, "Google Default Task List", "Overwrite Google changes is NOT for Google Default Task List. To overwrite Google Default Task List requires some steps: copy the local list as another list then 'Clear local/Google tasks', copy the list back and sync.");
                    return;
                } else {
                    account = new AccountBpImpl().get(this.mTasksList.getAccountId());
                    if (account.isNewGoogleId().booleanValue() || account.getId() == DatabaseHelper.OFFLINE_ACCOUNT_ID) {
                        showOverwriteGoogleChanges();
                        return;
                    } else {
                        new Builder(this).setTitle("Old Google Sync ID").setMessage("Unable to overwrite Google changes because the local account uses old sync ID.\n\nPlease check Preferences -> Account Manager for details. ").setPositiveButton(R.string.alert_dialog_ok, new C19275()).show();
                        return;
                    }
                }
            case 7:
                if (isAllTasksList) {
                    AppUtil.showGenericMessage(this, "Single list support only", "Overwrite local changes is not for ALL TASKS LIST. Please try it on specific list by going back and select a list.");
                    return;
                } else if (isAccountOffline) {
                    AppUtil.showGenericMessage(this, "Online Account only", "Overwrite local changes is NOT for Offline Account.");
                    return;
                } else {
                    account = new AccountBpImpl().get(this.mTasksList.getAccountId());
                    if (account.isNewGoogleId().booleanValue() || account.getId() == DatabaseHelper.OFFLINE_ACCOUNT_ID) {
                        showOverwriteLocalChanges();
                        return;
                    } else {
                        new Builder(this).setTitle("Old Google Sync ID").setMessage("Unable to overwrite local changes because the account uses old sync ID.\n\nPlease check Preferences -> Account Manager for details. ").setPositiveButton(R.string.alert_dialog_ok, new C19264()).show();
                        return;
                    }
                }
            case 8:
                refreshDisplay(null);
                return;
            case 9:
                sendEmail(getEmailAllTasksBody(), true);
                return;
            case 10:
                sendEmail(getEmailVisibleTasksBody(), false);
                return;
            case 11:
                performShowHideNumbers();
                return;
            case 12:
                if (isAllTasksList) {
                    AppUtil.showGenericMessage(this, "Single list support only", "Sort permanent is not for ALL TASKS LIST. Please try it on specific list by going back and select a list.");
                    return;
                } else {
                    showSortPermanent();
                    return;
                }
            case 13:
                if (isAccountOffline) {
                    AppUtil.showGenericMessage(this, "Online Account only", "Sync list is NOT for Offline Account.");
                    return;
                } else if (13 != whichAction) {
                    return;
                } else {
                    if (config.isSyncing()) {
                        Toast.makeText(this, "Sync is on going...", Toast.LENGTH_LONG).show();
                        return;
                    } else if (this.mTasksList.getId() == DatabaseHelper.ALL_TASKS_LIST_ID) {
                        List<Account> accounts = new AccountBpImpl().getAll();
                        StringBuffer accountWithOldGoogleIds = new StringBuffer();
                        for (Account account2 : accounts) {
                            if (!(account2.isNewGoogleId().booleanValue() || account2.getId() == DatabaseHelper.OFFLINE_ACCOUNT_ID)) {
                                accountWithOldGoogleIds.append("\n  - " + account2.getEmail());
                            }
                        }
                        if (accountWithOldGoogleIds.length() > 1) {
                            new Builder(this).setTitle("Old Google Sync ID's").setMessage("The following Google accounts will not be synced because it uses old sync ID's." + accountWithOldGoogleIds + "\n\nPlease check Preferences -> Account Manager for details. " + "\n\nContinue with sync? ").setPositiveButton(R.string.alert_dialog_yes, new C19242()).setNegativeButton(R.string.alert_dialog_no, new C19231()).show();
                            return;
                        }
                        new SyncWithGoogleTasks(this, this.mTasksList).execute(new Void[0]);
                        new MonitorSync().execute(new Void[0]);
                        return;
                    } else {
                        Account account2 = new AccountBpImpl().get(this.mTasksList.getAccountId());
                        if (account2.isNewGoogleId().booleanValue() || account2.getId() == DatabaseHelper.OFFLINE_ACCOUNT_ID) {
                            new SyncWithGoogleTasks(this, this.mTasksList).execute(new Void[0]);
                            new MonitorSync().execute(new Void[0]);
                            return;
                        }
                        new Builder(this).setTitle("Old Google Sync ID").setMessage("Unable to sync because account uses old sync ID.\n\nPlease check Preferences -> Account Manager for details. ").setPositiveButton(R.string.alert_dialog_ok, new C19253()).show();
                        return;
                    }
                }
            case 14:
                new ProcessTaskAction(this, 14, 0).execute(new TasksList[]{this.mTasksList});
                return;
            default:
                AppUtil.showGenericMessage(this, "TODO Message", "Sorry, this action is still a TODO.");
                return;
        }
    }

    private void sendEmail(String emailBody, boolean isAllTasks) {
        if (StringUtil.isNullOrEmpty(ConfigManager.getInstance().getDefaultEmailRecipient())) {
            promptForEmailAddress(emailBody, isAllTasks);
        } else {
            performEmailSend(emailBody, isAllTasks);
        }
    }

    private void performEmailSend(String emailBody, boolean isAllTasks) {
        String[] recipients = new String[]{ConfigManager.getInstance().getDefaultEmailRecipient()};
        String subject = this.mTasksList.getName() + "  (Visible tasks)";
        if (isAllTasks) {
            subject = this.mTasksList.getName() + "  (All tasks)";
        }
        EmailUtility.sendEmail(recipients, subject, emailBody, this);
    }

    private void promptForEmailAddress(final String emailBody, final boolean isAllTasks) {
        View textEntryView = LayoutInflater.from(this).inflate(R.layout.email_entry_dialog, null);
        final EditText emailAddressEditView = (EditText) textEntryView.findViewById(R.id.email_address_edit_view);
        new Builder(this).setTitle("Email Address").setView(textEntryView).setPositiveButton(R.string.alert_dialog_ok, new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ConfigManager.getInstance().setDefaultEmailRecipient(emailAddressEditView.getText().toString());
                AbstractTasksListActivity.this.performEmailSend(emailBody, isAllTasks);
            }
        }).setNegativeButton(R.string.alert_dialog_cancel, new C19286()).show();
    }

    private void showSortPermanent() {
        this.mSortScope = 0;
        new Builder(this).setTitle("Sort [" + this.mTasksList.getName() + "] by task name permanently").setSingleChoiceItems(R.array.sort_scope, 0, new C19319()).setPositiveButton(R.string.alert_dialog_ok, new C19308()).setNegativeButton(R.string.alert_dialog_cancel, null).show();
    }

    private void showOverwriteGoogleChanges() {
        new Builder(this).setTitle("Overwrite Google changes").setMessage("Overwrite [" + this.mTasksList.getName() + "] Google Tasks changes?").setPositiveButton(R.string.alert_dialog_ok, new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichTaksList) {
                dialog.dismiss();
                if (ConfigManager.getInstance().isSyncing()) {
                    Toast.makeText(AbstractTasksListActivity.this, "Sync is on going...", Toast.LENGTH_LONG).show();
                    return;
                }
                new ProcessTaskActionProgressBar(AbstractTasksListActivity.this, 6, "Please wait overwriting Google Tasks...").execute(new TasksList[]{AbstractTasksListActivity.this.mTasksList});
            }
        }).setNegativeButton(R.string.alert_dialog_cancel, null).show();
    }

    private void showOverwriteLocalChanges() {
        new Builder(this).setTitle("Overwrite Local changes").setMessage("Overwrite [" + this.mTasksList.getName() + "] local changes. Decorators will not be overwritten.").setPositiveButton(R.string.alert_dialog_ok, new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichTaksList) {
                dialog.dismiss();
                if (ConfigManager.getInstance().isSyncing()) {
                    Toast.makeText(AbstractTasksListActivity.this, "Sync is on going...", Toast.LENGTH_LONG).show();
                    return;
                }
                new ProcessTaskAction(AbstractTasksListActivity.this, 7, 0).execute(new TasksList[]{AbstractTasksListActivity.this.mTasksList});
            }
        }).setNegativeButton(R.string.alert_dialog_cancel, null).show();
    }

    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
    }

    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
    }

    public void onContentChanged() {
        super.onContentChanged();
    }

    public void setListAdapter(ListAdapter adapter) {
        super.setListAdapter(adapter);
    }

    public void setSelection(int position) {
        super.setSelection(position);
    }

    public int getSelectedItemPosition() {
        return super.getSelectedItemPosition();
    }

    public long getSelectedItemId() {
        return super.getSelectedItemId();
    }

    public ListView getListView() {
        return super.getListView();
    }

    public ListAdapter getListAdapter() {
        return super.getListAdapter();
    }

    protected void finalize() throws Throwable {
        super.finalize();
    }

    public Intent getIntent() {
        return super.getIntent();
    }

    public void setIntent(Intent newIntent) {
        super.setIntent(newIntent);
    }

    public WindowManager getWindowManager() {
        return super.getWindowManager();
    }

    public Window getWindow() {
        return super.getWindow();
    }

    public View getCurrentFocus() {
        return super.getCurrentFocus();
    }

    public int getWallpaperDesiredMinimumWidth() {
        return super.getWallpaperDesiredMinimumWidth();
    }

    public int getWallpaperDesiredMinimumHeight() {
        return super.getWallpaperDesiredMinimumHeight();
    }

    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    protected void onStart() {
        super.onStart();
    }

    protected void onRestart() {
        super.onRestart();
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onPostResume() {
        super.onPostResume();
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    protected void onPause() {
        super.onPause();
    }

    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
    }

    public boolean onCreateThumbnail(Bitmap outBitmap, Canvas canvas) {
        return super.onCreateThumbnail(outBitmap, canvas);
    }

    public CharSequence onCreateDescription() {
        return super.onCreateDescription();
    }

    protected void onStop() {
        super.onStop();
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public int getChangingConfigurations() {
        return super.getChangingConfigurations();
    }

    public Object getLastNonConfigurationInstance() {
        return super.getLastNonConfigurationInstance();
    }

    public Object onRetainNonConfigurationInstance() {
        return super.onRetainNonConfigurationInstance();
    }

    public void onLowMemory() {
        super.onLowMemory();
    }

    public void startManagingCursor(Cursor c) {
        super.startManagingCursor(c);
    }

    public void stopManagingCursor(Cursor c) {
        super.stopManagingCursor(c);
    }

    public View findViewById(int id) {
        return super.findViewById(id);
    }

    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
    }

    public void setContentView(View view) {
        super.setContentView(view);
    }

    public void setContentView(View view, LayoutParams params) {
        super.setContentView(view, params);
    }

    public void addContentView(View view, LayoutParams params) {
        super.addContentView(view, params);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return super.onKeyUp(keyCode, event);
    }

    public boolean onKeyMultiple(int keyCode, int repeatCount, KeyEvent event) {
        return super.onKeyMultiple(keyCode, repeatCount, event);
    }

    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    public boolean onTrackballEvent(MotionEvent event) {
        return super.onTrackballEvent(event);
    }

    public void onUserInteraction() {
        super.onUserInteraction();
    }

    public void onWindowAttributesChanged(WindowManager.LayoutParams params) {
        super.onWindowAttributesChanged(params);
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    public boolean hasWindowFocus() {
        return super.hasWindowFocus();
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        return super.dispatchKeyEvent(event);
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }

    public boolean dispatchTrackballEvent(MotionEvent ev) {
        return super.dispatchTrackballEvent(ev);
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent event) {
        return super.dispatchPopulateAccessibilityEvent(event);
    }

    public View onCreatePanelView(int featureId) {
        return super.onCreatePanelView(featureId);
    }

    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        return super.onCreatePanelMenu(featureId, menu);
    }

    public boolean onPreparePanel(int featureId, View view, Menu menu) {
        return super.onPreparePanel(featureId, view, menu);
    }

    public boolean onMenuOpened(int featureId, Menu menu) {
        return super.onMenuOpened(featureId, menu);
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        return super.onMenuItemSelected(featureId, item);
    }

    public void onPanelClosed(int featureId, Menu menu) {
        super.onPanelClosed(featureId, menu);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
    }

    public void openOptionsMenu() {
        super.openOptionsMenu();
    }

    public void closeOptionsMenu() {
        super.closeOptionsMenu();
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    public void registerForContextMenu(View view) {
        super.registerForContextMenu(view);
    }

    public void unregisterForContextMenu(View view) {
        super.unregisterForContextMenu(view);
    }

    public void openContextMenu(View view) {
        super.openContextMenu(view);
    }

    public void closeContextMenu() {
        super.closeContextMenu();
    }

    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }

    public void onContextMenuClosed(Menu menu) {
        super.onContextMenuClosed(menu);
    }

    protected Dialog onCreateDialog(int id) {
        return super.onCreateDialog(id);
    }

    protected void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
    }

    public boolean onSearchRequested() {
        return super.onSearchRequested();
    }

    public void startSearch(String initialQuery, boolean selectInitialQuery, Bundle appSearchData, boolean globalSearch) {
        super.startSearch(initialQuery, selectInitialQuery, appSearchData, globalSearch);
    }

    public void takeKeyEvents(boolean get) {
        super.takeKeyEvents(get);
    }

    public LayoutInflater getLayoutInflater() {
        return super.getLayoutInflater();
    }

    public MenuInflater getMenuInflater() {
        return super.getMenuInflater();
    }

    protected void onApplyThemeResource(Theme theme, int resid, boolean first) {
        super.onApplyThemeResource(theme, resid, first);
    }

    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
    }

    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    public boolean startActivityIfNeeded(Intent intent, int requestCode) {
        return super.startActivityIfNeeded(intent, requestCode);
    }

    public boolean startNextMatchingActivity(Intent intent) {
        return super.startNextMatchingActivity(intent);
    }

    public void startActivityFromChild(Activity child, Intent intent, int requestCode) {
        super.startActivityFromChild(child, intent, requestCode);
    }

    public String getCallingPackage() {
        return super.getCallingPackage();
    }

    public ComponentName getCallingActivity() {
        return super.getCallingActivity();
    }

    public void setVisible(boolean visible) {
        super.setVisible(visible);
    }

    public boolean isFinishing() {
        return super.isFinishing();
    }

    public void finish() {
        super.finish();
    }

    public void finishFromChild(Activity child) {
        super.finishFromChild(child);
    }

    public void finishActivity(int requestCode) {
        super.finishActivity(requestCode);
    }

    public void finishActivityFromChild(Activity child, int requestCode) {
        super.finishActivityFromChild(child, requestCode);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public PendingIntent createPendingResult(int requestCode, Intent data, int flags) {
        return super.createPendingResult(requestCode, data, flags);
    }

    public void setRequestedOrientation(int requestedOrientation) {
        super.setRequestedOrientation(requestedOrientation);
    }

    public int getRequestedOrientation() {
        return super.getRequestedOrientation();
    }

    public int getTaskId() {
        return super.getTaskId();
    }

    public boolean isTaskRoot() {
        return super.isTaskRoot();
    }

    public boolean moveTaskToBack(boolean nonRoot) {
        return super.moveTaskToBack(nonRoot);
    }

    public String getLocalClassName() {
        return super.getLocalClassName();
    }

    public ComponentName getComponentName() {
        return super.getComponentName();
    }

    public SharedPreferences getPreferences(int mode) {
        return super.getPreferences(mode);
    }

    public Object getSystemService(String name) {
        return super.getSystemService(name);
    }

    public void setTitle(CharSequence title) {
        super.setTitle(title);
    }

    public void setTitle(int titleId) {
        super.setTitle(titleId);
    }

    public void setTitleColor(int textColor) {
        super.setTitleColor(textColor);
    }

    protected void onTitleChanged(CharSequence title, int color) {
        super.onTitleChanged(title, color);
    }

    protected void onChildTitleChanged(Activity childActivity, CharSequence title) {
        super.onChildTitleChanged(childActivity, title);
    }

    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);
    }
}
