package com.irt.tasks.ui;

import android.app.Activity;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.sync.DropboxSyncHelper;

public class AbstractActivity extends Activity {
    protected ConfigManager config;

    protected void onResume() {
        super.onResume();
        this.config = ConfigManager.getInstance();
        DropboxSyncHelper.uiRequestSyncWithDelay();
    }

    public void onUserInteraction() {
        super.onUserInteraction();
        DropboxSyncHelper.uiRequestSyncWithDelay();
    }
}
