package com.irt.tasks.ui.dragndrop;

public interface DropListener {
    void onDrop(int i, int i2);
}
