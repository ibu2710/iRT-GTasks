package com.irt.tasks.ui.task;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;

import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.ContextTask;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.ui.AbstractListActivity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractEditListActivity extends AbstractListActivity {
    RadioButton mAndOperatorRadioButton;
    RadioGroup mAndOrRadioGroup;
    RadioButton mAnyRadioButton;
    Button mCancelButton;
    List<ContextTask> mContextTasksOriginal = null;
    boolean mIsHideAndOr;
    boolean mIsShowAny;
    Button mNewButton;
    RadioButton mNoneRadioButton;
    RadioButton mOrOperatorRadioButton;
    TableLayout mRadioGroupsTableLayout;
    Button mSaveButton;
    Task mTask;

    protected abstract void performDeleteSelectedListItem(int i);

    protected abstract void performEditSelectedListItem(int i);

    protected abstract void performListOnItemClick(AdapterView<?> adapterView, View view, int i, long j);

    protected abstract void performNew();

    protected abstract void performOnCreate(Bundle bundle);

    protected abstract void performSave();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_edit_generic);
        this.mIsShowAny = getIntent().getBooleanExtra(Constants.INTENT_IS_SHOW_ANY, false);
        this.mIsHideAndOr = getIntent().getBooleanExtra(Constants.INTENT_IS_HIDE_AND_OR, false);
        initViewControls();
        initCancelButton();
        initNewButton();
        initSaveButton();
        if (ArchitectureContext.getObject() instanceof Task) {
            this.mTask = (Task) ArchitectureContext.getObject();
            if (this.mTask.getContextTasks() != null) {
                this.mContextTasksOriginal = new ArrayList<ContextTask>(this.mTask.getContextTasks());
            }
        }
        if (this.mIsShowAny) {
            initAnyAnyTableLayout();
            initAnyRadioButton();
            initNoneRadioButton();
            if (this.mIsHideAndOr) {
                this.mAndOrRadioGroup.setVisibility(View.GONE);
            } else {
                initAndOrRadioGroup();
            }
        }
        setListListeners();
        performOnCreate(savedInstanceState);
    }

    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.context_menu_filter_edit:
                performEditSelectedListItem(info.position);
                return true;
            case R.id.context_menu_filter_delete:
                performDeleteSelectedListItem(info.position);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (isShowContextMenu(((AdapterContextMenuInfo) menuInfo).position)) {
            getMenuInflater().inflate(R.menu.context_menu_filter, menu);
        }
    }

    private void initAndOrRadioGroup() {
        if (this.mTask.isAndOperation()) {
            this.mAndOperatorRadioButton.setChecked(true);
        } else {
            this.mOrOperatorRadioButton.setChecked(true);
        }
    }

    protected void setRadioButtons(boolean isItemSelected) {
        if (!this.mRadioGroupsTableLayout.isShown()) {
            return;
        }
        if (isItemSelected) {
            this.mAnyRadioButton.setChecked(false);
            this.mNoneRadioButton.setChecked(false);
            this.mTask.setAnySelected(false);
            this.mTask.setNoneSelected(false);
            return;
        }
        performSetRadioButtons();
    }

    protected void setTaskForSelectedItem(int position) {
        performSetTaskForSelectedItem(position);
    }

    protected void performSetTaskForSelectedItem(int position) {
    }

    protected boolean isShowContextMenu(int position) {
        return true;
    }

    protected void performSetRadioButtons() {
    }

    protected void performOnClickAnyRadioButton() {
        this.mTask.setAnySelected(true);
        this.mTask.setNoneSelected(false);
        resetContextTagProject();
        finish();
    }

    protected void performOnClickNoneRadioButton() {
        this.mTask.setAnySelected(false);
        this.mTask.setNoneSelected(true);
        resetContextTagProject();
        finish();
    }

    private void resetContextTagProject() {
        this.mTask.setContextTasks(null);
        this.mTask.setTagTasks(null);
        this.mTask.setProjectId(0L);
    }

    protected CharSequence getAnyRadioButtonText() {
        return null;
    }

    protected CharSequence getNoneRadioButtonText() {
        return null;
    }

    private void initAnyAnyTableLayout() {
        this.mRadioGroupsTableLayout.setVisibility(View.VISIBLE);
    }

    private void initAnyRadioButton() {
        this.mAnyRadioButton.setText(getAnyRadioButtonText());
        this.mAnyRadioButton.setChecked(this.mTask.isAnySelected());
        this.mAnyRadioButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AbstractEditListActivity.this.performOnClickAnyRadioButton();
            }
        });
    }

    private void initNoneRadioButton() {
        this.mNoneRadioButton.setText(getNoneRadioButtonText());
        this.mNoneRadioButton.setChecked(this.mTask.isNoneSelected());
        this.mNoneRadioButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AbstractEditListActivity.this.performOnClickNoneRadioButton();
            }
        });
    }

    private void setListListeners() {
        getListView().setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AbstractEditListActivity.this.performListOnItemClick(parent, view, position, id);
            }
        });
        registerForContextMenu(getListView());
    }

    private void initViewControls() {
        this.mNewButton = (Button) findViewById(R.id.new_button);
        this.mSaveButton = (Button) findViewById(R.id.save_button);
        this.mCancelButton = (Button) findViewById(R.id.cancel_button);
        this.mRadioGroupsTableLayout = (TableLayout) findViewById(R.id.any_table_layout);
        this.mAnyRadioButton = (RadioButton) findViewById(R.id.any);
        this.mNoneRadioButton = (RadioButton) findViewById(R.id.none);
        this.mAndOrRadioGroup = (RadioGroup) findViewById(R.id.and_or_options);
        this.mAndOperatorRadioButton = (RadioButton) findViewById(R.id.and_operation);
        this.mOrOperatorRadioButton = (RadioButton) findViewById(R.id.or_operation);
    }

    private void initNewButton() {
        this.mNewButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AbstractEditListActivity.this.performNew();
            }
        });
    }

    private void initSaveButton() {
        this.mSaveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AbstractEditListActivity.this.performSave();
                AbstractEditListActivity.this.finish();
            }
        });
    }

    private void initCancelButton() {
        this.mCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AbstractEditListActivity.this.performCancel();
                AbstractEditListActivity.this.finish();
            }
        });
    }

    void performCancel() {
    }
}
