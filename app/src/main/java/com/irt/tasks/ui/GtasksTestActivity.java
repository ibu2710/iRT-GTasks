package com.irt.tasks.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.AccountBp;
import com.irt.tasks.app.businessprocess.AccountBpImpl;
import com.irt.tasks.app.businessprocess.SyncBp;
import com.irt.tasks.app.businessprocess.SyncBpImpl;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.DateUtil;
import com.irt.tasks.app.common.ElapsedTime;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.GtaskDao;
import com.irt.tasks.app.dataaccess.GtaskDaoImpl;
import com.irt.tasks.app.dataaccess.GtasksListDao;
import com.irt.tasks.app.dataaccess.GtasksListDaoImpl;
import com.irt.tasks.app.dataaccess.TasksListDao;
import com.irt.tasks.app.dataaccess.TasksListDaoImpl;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.security.AndroidCrypto;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GtasksTestActivity extends Activity {
    static final String APPLICATION_CODE_PARAM = "application_code";
    static final int APPLICATION_ID_IRT_CALENDAR_PRO = 1;
    static final String DEVICE_ID_PARAM = "device_id";
    public static final String GOOGLE_TASKS = "googleTasks";
    public static final String GOOGLE_TASKS_LIST = "googleTasksList";
    public static final String LOCAL_TASKS = "localTasks";
    public static final String LOCAL_TASKS_LIST = "localTasksList";
    AccountBp accountBp = new AccountBpImpl();
    GtaskDao gtasksDao = new GtaskDaoImpl();
    GtasksListDao gtasksListDao = new GtasksListDaoImpl();
    SyncBp syncBp = new SyncBpImpl();
    TaskBp taskBp = new TaskBpImpl();
    TasksListDao tasksListDao = new TasksListDaoImpl();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void syncWithGoogleTasks() {
        this.syncBp.syncWithGoogleTasks();
    }

    public Map<String, Object> syncWithGoogleTasksTest(Long tasksListId) {
        TasksList tasksList = this.tasksListDao.get(tasksListId);
        this.syncBp.syncWithGoogleTasks(tasksList);
        System.out.println("*** syncWithGoogleTasksTest -- done with syncWithgoogleTasks... ***" + tasksList.getId() + Globals.FORWARDSLASH + tasksList.getName());
        ArchitectureContext.setAccount(this.accountBp.get(tasksList.getAccountId()));
        TasksList googleTasksList = this.gtasksListDao.getTasksList(tasksList.getGtasksListId());
        TasksList localTasksList = this.tasksListDao.get(tasksListId);
        List<Task> googleTasks = this.gtasksDao.getAllTasksByTasksListId(tasksList.getGtasksListId());
        List<Task> localTasks = this.taskBp.getAllTasksWithCompletedAndSupportForAllTasks(localTasksList, false);
        Map<String, Object> testResultMap = new HashMap();
        testResultMap.put(GOOGLE_TASKS_LIST, googleTasksList);
        testResultMap.put(LOCAL_TASKS_LIST, localTasksList);
        testResultMap.put(GOOGLE_TASKS, googleTasks);
        testResultMap.put(LOCAL_TASKS, localTasks);
        return testResultMap;
    }

    public List<Task> getAllTasksByTasksListId(String tasksListId) {
        return this.gtasksDao.getAllTasksByTasksListId(tasksListId);
    }

    public void moveTaskWithinSameList(Task task, Task parentTask, Task priorSiblingTask) {
        this.gtasksDao.moveTaskWithinSameList(task);
    }

    public void moveTaskFromListToList(Task task, TasksList sourceList, TasksList destList, Task destParent) {
        this.gtasksDao.moveTaskFromListToList(task, sourceList, destList, destParent);
    }

    public List<TasksList> getAllGTasksLists() {
        return this.gtasksListDao.getAllTasksLists();
    }

    public void deleteTasksList(TasksList tasksList) {
        this.gtasksListDao.deleteTasksList(tasksList);
    }

    public void insertTasksList(TasksList tasksList) {
        this.gtasksListDao.insertTasksList(tasksList);
    }

    public void renameTasksList(TasksList tasksList) {
        this.gtasksListDao.renameTasksList(tasksList);
    }

    public List<Account> getAllAccountsFromSystem() {
        return this.accountBp.getAllAccountsFromSystem(this);
    }

    public List<Account> getAllAccounts() {
        return this.accountBp.getAll();
    }

    public String encrypt(String masterpassword, String clearText) {
        String crypto = null;
        try {
            crypto = AndroidCrypto.encrypt(masterpassword, clearText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return crypto;
    }

    public String decrypt(String masterpassword, String crypto) {
        String clearText = null;
        try {
            clearText = AndroidCrypto.decrypt(masterpassword, crypto, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clearText;
    }

    public Date getExpirationDate() {
        String result = "";
        ArrayList<NameValuePair> nameValuePairs = new ArrayList();
        String device_id = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
        if (TextUtils.isEmpty(device_id)) {
            throw new RuntimeException("Could not register phone");
        }
        nameValuePairs.add(new BasicNameValuePair(APPLICATION_CODE_PARAM, "1.0"));
        nameValuePairs.add(new BasicNameValuePair(DEVICE_ID_PARAM, device_id));
        InputStream inputStream = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://www.homeschoolskedtrack.com/getExpirationDate.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            inputStream = httpclient.execute(httppost).getEntity().getContent();
        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
        }
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String line = bufferedReader.readLine();
                if (line == null) {
                    break;
                }
                stringBuilder.append(line + "n");
            }
            inputStream.close();
            result = stringBuilder.toString();
        } catch (Exception e2) {
            Log.e("log_tag", "Error converting result " + e2.toString());
        }
        int trialDays = 0;
        try {
            String registrationDateStr = "";
            String thisDeviceId = "";
            JSONArray jArray = new JSONArray(result);
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject json_data = jArray.getJSONObject(i);
                Log.i("log_tag", "device_id: " + json_data.getString(DEVICE_ID_PARAM) + ", trial_days: " + json_data.getInt("trial_days") + ", registration_date: " + json_data.getString("registration_date"));
                trialDays = json_data.getInt("trial_days");
                registrationDateStr = json_data.getString("registration_date");
                thisDeviceId = json_data.getString(DEVICE_ID_PARAM);
            }
            if (trialDays != 0) {
                if (!(StringUtil.isNullOrEmpty(registrationDateStr) || StringUtil.isNullOrEmpty(thisDeviceId) || !thisDeviceId.equals(device_id))) {
                    Date registrationDate = DateUtil.parseDate(registrationDateStr, Constants.DATE_FORMAT_YYYY_MM_DD);
                    long trialDaysInMilles = ElapsedTime.DAY_IN_MILLIS * ((long) trialDays);
                    if (AppUtil.getTodayDateOnly().getTime() - registrationDate.getTime() >= trialDaysInMilles) {
                        return new Date(registrationDate.getTime() + trialDaysInMilles);
                    }
                    return new Date(registrationDate.getTime() + trialDaysInMilles);
                }
            }
            return AppUtil.getTodayDateOnly();
        } catch (JSONException e3) {
            Log.e("log_tag", "Error parsing data " + e3.toString());
            return AppUtil.getTodayDateOnly();
        }
    }
}
