package com.irt.tasks.ui.task;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Project;
import java.util.List;

public class ProjectInteractiveArrayAdapter extends ArrayAdapter<Project> {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private int mResourceId = 0;
    private int mSelectedPosition = -1;
    private RadioButton mSelectedRB;

    public static class ViewHolder {
        TextView name;
        RadioButton radioBtn;
    }

    public ProjectInteractiveArrayAdapter(Context context, int resource, int textViewResourceId, List<Project> objects) {
        super(context, resource, textViewResourceId, objects);
        this.mContext = context;
        this.mResourceId = resource;
        this.mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        View view = convertView;
        if (view == null) {
            view = this.mLayoutInflater.inflate(this.mResourceId, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) view.findViewById(R.id.text);
            holder.radioBtn = (RadioButton) view.findViewById(R.id.radioButton1);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.radioBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (!(position == ProjectInteractiveArrayAdapter.this.mSelectedPosition || ProjectInteractiveArrayAdapter.this.mSelectedRB == null)) {
                    ProjectInteractiveArrayAdapter.this.mSelectedRB.setChecked(false);
                }
                ProjectInteractiveArrayAdapter.this.mSelectedPosition = position;
                ProjectInteractiveArrayAdapter.this.mSelectedRB = (RadioButton) v;
                if (position >= 0) {
                    AbstractEditListActivity activity = (AbstractEditListActivity) ProjectInteractiveArrayAdapter.this.mContext;
                    activity.setTaskForSelectedItem(position);
                    activity.setRadioButtons(true);
                }
            }
        });
        if (((Project) getItem(position)).isSelected()) {
            this.mSelectedPosition = position;
        }
        if (this.mSelectedPosition != position) {
            holder.radioBtn.setChecked(false);
        } else {
            holder.radioBtn.setChecked(true);
            this.mSelectedRB = holder.radioBtn;
        }
        holder.name.setText(((Project) getItem(position)).getTitle());
        return view;
    }
}
