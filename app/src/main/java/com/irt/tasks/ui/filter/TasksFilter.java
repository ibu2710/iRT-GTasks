package com.irt.tasks.ui.filter;

import java.util.List;

public class TasksFilter {
    private List<Long> contextIds = null;
    private boolean isContextAnd = false;
    boolean isContextNone = false;
    boolean isProjectNone = false;
    private boolean isTagAnd = false;
    boolean isTagNone = false;
    private Long projectId = Long.valueOf(0);
    private Integer status = Integer.valueOf(6);
    private List<Long> tagIds = null;

    public Long getProjectId() {
        return this.projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Long> getContextIds() {
        return this.contextIds;
    }

    public void setContextIds(List<Long> contextIds) {
        this.contextIds = contextIds;
    }

    public List<Long> getTagIds() {
        return this.tagIds;
    }

    public void setTagIds(List<Long> tagIds) {
        this.tagIds = tagIds;
    }

    public boolean isContextNone() {
        return this.isContextNone;
    }

    public void setContextNone(boolean isContextNone) {
        this.isContextNone = isContextNone;
    }

    public boolean isContextAnd() {
        return this.isContextAnd;
    }

    public void setContextAnd(boolean isAnd) {
        this.isContextAnd = isAnd;
    }

    public boolean isProjectNone() {
        return this.isProjectNone;
    }

    public void setProjectNone(boolean isProjectNone) {
        this.isProjectNone = isProjectNone;
    }

    public boolean isTagNone() {
        return this.isTagNone;
    }

    public void setTagNone(boolean isTagNone) {
        this.isTagNone = isTagNone;
    }

    public boolean isTagAnd() {
        return this.isTagAnd;
    }

    public void setTagAnd(boolean isTagAnd) {
        this.isTagAnd = isTagAnd;
    }
}
