package com.irt.tasks.ui.task;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import com.irt.tasks.app.businessobjects.ContextBase;
import com.irt.tasks.app.businessobjects.ContextTask;
import com.irt.tasks.app.businessprocess.ContextBp;
import com.irt.tasks.app.businessprocess.ContextBpImpl;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.ui.task.ContextBaseInteractiveArrayAdapter.ViewHolder;
import java.util.ArrayList;
import java.util.List;

public class ContextSelectionListActivity extends AbstractEditListActivity {
    ContextBp contextBp = new ContextBpImpl();
    List<ContextBase> mContexts;

    protected void performOnCreate(Bundle savedInstanceState) {
    }

    private void refereshList() {
        setListAdapter(new ContextBaseInteractiveArrayAdapter(this, getContexts()));
    }

    protected List<ContextBase> getContexts() {
        this.mContexts = this.contextBp.getAllContexts();
        if (this.mContexts.size() > 0) {
            List<ContextTask> contextTasks = this.mTask.getContextTasks();
            if (contextTasks != null) {
                for (ContextTask contextTask : contextTasks) {
                    setSelectedContext(contextTask.getContextId());
                }
            }
        }
        return this.mContexts;
    }

    private void setSelectedContext(Long contextId) {
        for (ContextBase contextBase : this.mContexts) {
            if (contextBase.getId().longValue() == contextId.longValue()) {
                contextBase.setSelected(true);
            }
        }
    }

    protected void performSetRadioButtons() {
        for (ContextBase contextBase : this.mContexts) {
            if (contextBase.isSelected()) {
                return;
            }
        }
        this.mAnyRadioButton.setChecked(true);
        this.mTask.setAnySelected(true);
    }

    protected CharSequence getAnyRadioButtonText() {
        return "Any context";
    }

    protected CharSequence getNoneRadioButtonText() {
        return "No context";
    }

    protected void performCancel() {
        this.mTask.setContextTasks(this.mContextTasksOriginal);
    }

    protected void onResume() {
        super.onResume();
        refereshList();
    }

    protected void performNew() {
        editContext(new ContextBase());
    }

    private void editContext(ContextBase contextBase) {
        performSave();
        ArchitectureContext.setObject(contextBase);
        startActivity(new Intent(this, ContextEditActivity.class));
    }

    protected void performSave() {
        List<ContextTask> contextTasks = new ArrayList<ContextTask>();
        for (ContextBase contextBase : this.mContexts) {
            if (contextBase.isSelected()) {
                contextTasks.add(new ContextTask(contextBase.getId(), this.mTask.getId()));
            }
        }
        this.mTask.setContextTasks(contextTasks);
        this.mTask.setAndOperation(this.mAndOperatorRadioButton.isChecked());
    }

    protected void performListOnItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        CheckBox checkbox = ((ViewHolder) view.getTag()).checkbox;
        if (checkbox.isChecked()) {
            checkbox.setChecked(false);
        } else {
            checkbox.setChecked(true);
        }
    }

    protected void performDeleteSelectedListItem(final int position) {
        new Builder(this).setTitle("Delete context").setMessage("Context [" + ((ContextBase) this.mContexts.get(position)).getName() + "] will be deleted?").setPositiveButton("OK", new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ContextSelectionListActivity.this.contextBp.deleteContext((ContextBase) ContextSelectionListActivity.this.mContexts.get(position));
                ContextSelectionListActivity.this.contextBp.deleteAllContextIncludesByContextBaseId(((ContextBase) ContextSelectionListActivity.this.mContexts.get(position)).getId());
                ContextSelectionListActivity.this.contextBp.deleteAllConextTasksbyContextBaseId(((ContextBase) ContextSelectionListActivity.this.mContexts.get(position)).getId());
                ContextSelectionListActivity.this.onResume();
            }
        }).setNegativeButton("Cancel", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }

    protected void performEditSelectedListItem(int position) {
        editContext((ContextBase) this.mContexts.get(position));
    }
}
