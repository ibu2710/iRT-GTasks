package com.irt.tasks.ui.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.framework.config.ConfigManager;

public class DropboxSyncHelper {
    public static final String ACCOUNT = "iRT GTasks Outliner Sync";
    public static final String ACCOUNT_TYPE = "example.com";
    public static final String AUTHORITY = "com.example.android.datasync.provider";
    private static final int BACKEND_FILE_LISTENING_DELAY_SECONDS = 20;
    private static final int BACKEND_RESET_FIRST_TIME_RUN_DELAY_SECONDS = 180;
    private static final int BACKEND_SET_WIFI_STATUS_DELAY_SECONDS = 5;
    public static final String DROPBOX_URI_STRING = "content://com.irt.tasks.ui.sync.dummyprovider";
    static Handler _backendDelayFileListeningHandler = new Handler();
    static Runnable _backendDelayFileListeningRunnable = new C18384();
    static Handler _backendRequestSyncWithDelayHandler = new Handler();
    static Runnable _backendRequestSyncWithDelayRunnable = new C18373();
    static Handler _backendResetFirstTimeRunWithDelayHandler = new Handler();
    static Runnable _backendResetFirstTimeRunWithDelayRunnable = new C18395();
    static Handler _backendSetWifiStatusDelayHandler = new Handler();
    static Runnable _backendSetWifiStatusDelayRunnable = new C18406();
    static Handler _uiRequestSyncWithDelayHandler = new Handler();
    static Runnable _uiRequestSyncWithDelayRunnable = new C18362();
    public static boolean isFirstTimeRun = true;

    static class C18362 implements Runnable {
        ConfigManager config = ConfigManager.getInstance();

        C18362() {
        }

        public void run() {
            if (this.config.isDataEditInitiated()) {
                this.config.setDataEditInitiated(Boolean.valueOf(false));
                DropboxSyncHelper.requestSync();
            }
        }
    }

    static class C18373 implements Runnable {
        C18373() {
        }

        public void run() {
            AppUtil.logDebug("Requesting sync from backend...");
            DropboxSyncHelper.requestSync();
        }
    }

    static class C18384 implements Runnable {
        ConfigManager config = ConfigManager.getInstance();

        C18384() {
        }

        public void run() {
            AppUtil.logDebug("Start listening for incoming files...");
            this.config.setDropboxFileSentFromThisDevice(Boolean.valueOf(false));
        }
    }

    static class C18395 implements Runnable {
        ConfigManager config = ConfigManager.getInstance();

        C18395() {
        }

        public void run() {
            AppUtil.logDebug("Set first time run to true...");
            DropboxSyncHelper.isFirstTimeRun = true;
        }
    }

    static class C18406 implements Runnable {
        ConfigManager config = ConfigManager.getInstance();

        C18406() {
        }

        public void run() {
            AppUtil.logDebug("Set Wifi status, is connected: " + AppUtil.isWifiConnected());
            this.config.setWifiConnected(Boolean.valueOf(AppUtil.isWifiConnected()));
            if (this.config.isDropboxAutoSync()) {
                DropboxSyncHelper.backendRequestSyncWithDelay(1, true);
                DropboxSyncHelper.broadCastSyncStatus(this.config.getDropboxSyncStatus());
            }
        }
    }

    public static Account CreateSyncAccount(Context context) {
        Account newAccount = new Account(ACCOUNT, ACCOUNT_TYPE);
        return ((AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE)).addAccountExplicitly(newAccount, null, null) ? newAccount : newAccount;
    }

    public static void stopSync() {
        ContentResolver.setIsSyncable(CreateSyncAccount(ConfigManager.getInstance().getContext()), AUTHORITY, 0);
    }

    public static void requestSync() {
        requestSync(false);
    }

    public static void requestSync(boolean isForceRefresh) {
        ConfigManager config = ConfigManager.getInstance();
        if (config.isDropboxAutoSync()) {
            Account androidAcount = CreateSyncAccount(config.getContext());
            Bundle bundle = new Bundle();
            bundle.putBoolean("expedited", true);
            bundle.putBoolean("force", true);
            bundle.putBoolean("force", true);
            bundle.putBoolean(DropboxSyncAdapter.SYNC_EXTRAS_FORCE_REFRESH, isForceRefresh);
            ContentResolver.requestSync(androidAcount, AUTHORITY, bundle);
        }
    }

    public static void uiRequestSyncWithDelay() {
        ConfigManager config = ConfigManager.getInstance();
        if (config.isDataEditInitiated() && isProcessSyncRelatedRequest(config)) {
            _uiRequestSyncWithDelayHandler.removeCallbacks(_uiRequestSyncWithDelayRunnable);
            _uiRequestSyncWithDelayHandler.postDelayed(_uiRequestSyncWithDelayRunnable, (long) (config.getDropboxSyncAfterLastEditIdleSeconds().intValue() * 1000));
        }
    }

    public static void backendRequestSyncWithDelay(int delaySeconds) {
        backendRequestSyncWithDelay(delaySeconds, false);
    }

    public static void backendRequestSyncWithDelay(int delaySeconds, boolean isWifiOverride) {
        if (isProcessSyncRelatedRequest(ConfigManager.getInstance()) || isWifiOverride) {
            AppUtil.logDebug("Backend requesting for sync with delay...");
            _backendRequestSyncWithDelayHandler.removeCallbacks(_backendRequestSyncWithDelayRunnable);
            if (isFirstTimeRun) {
                isFirstTimeRun = false;
                _backendRequestSyncWithDelayHandler.postDelayed(_backendRequestSyncWithDelayRunnable, 1000);
                return;
            }
            _backendRequestSyncWithDelayHandler.postDelayed(_backendRequestSyncWithDelayRunnable, (long) (delaySeconds * 1000));
        }
    }

    private static boolean isProcessSyncRelatedRequest(ConfigManager config) {
        return !config.isDropboxAutoSyncOnWifiOnly() || (config.isDropboxAutoSyncOnWifiOnly() && config.isWifiConnected());
    }

    public static void cancelBackendSyncRequest() {
        if (isProcessSyncRelatedRequest(ConfigManager.getInstance())) {
            AppUtil.logDebug("Canceling backend sync request...");
            _backendRequestSyncWithDelayHandler.removeCallbacks(_backendRequestSyncWithDelayRunnable);
        }
    }

    public static void backendDelayFileListening() {
        if (isProcessSyncRelatedRequest(ConfigManager.getInstance())) {
            AppUtil.logDebug("Delay backend file listening...");
            _backendDelayFileListeningHandler.removeCallbacks(_backendDelayFileListeningRunnable);
            _backendDelayFileListeningHandler.postDelayed(_backendDelayFileListeningRunnable, 20000);
        }
    }

    public static void backendResetFirstTimeRunWithDelay() {
        if (isProcessSyncRelatedRequest(ConfigManager.getInstance())) {
            AppUtil.logDebug("Delay resetting first time run...");
            _backendResetFirstTimeRunWithDelayHandler.removeCallbacks(_backendResetFirstTimeRunWithDelayRunnable);
            _backendResetFirstTimeRunWithDelayHandler.postDelayed(_backendResetFirstTimeRunWithDelayRunnable, 180000);
        }
    }

    public static void backendSetWifiStatusDelay() {
        ConfigManager config = ConfigManager.getInstance();
        AppUtil.logDebug("Setting Wifi status with delay...");
        _backendSetWifiStatusDelayHandler.removeCallbacks(_backendSetWifiStatusDelayRunnable);
        _backendSetWifiStatusDelayHandler.postDelayed(_backendSetWifiStatusDelayRunnable, 5000);
    }

    public static void addDelayedIdleToEditText(EditText editText) {
        if (ConfigManager.getInstance().isDropboxAutoSync()) {
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    DropboxSyncHelper.uiRequestSyncWithDelay();
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }
    }

    public static void broadCastSyncStatus(String syncStatus) {
        ConfigManager config = ConfigManager.getInstance();
        config.setDropboxSyncStatus(syncStatus);
        config.getContext().getContentResolver().notifyChange(Uri.parse(DROPBOX_URI_STRING), null, false);
    }
}
