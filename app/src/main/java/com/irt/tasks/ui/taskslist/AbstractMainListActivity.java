package com.irt.tasks.ui.taskslist;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.irt.tasks.R;
import com.irt.tasks.app.businessprocess.TasksListBp;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.AbstractListActivity;
import com.irt.tasks.ui.sync.DropboxSyncAdapter;
import com.irt.tasks.ui.sync.DropboxSyncHelper;
import java.util.Date;

public abstract class AbstractMainListActivity extends AbstractListActivity {
    private static final int SYNC_ICON_TYPE_CIRCLE_WITH_LETTER = 2;
    private static final int SYNC_ICON_TYPE_CLOUD = 0;
    private static final int SYNC_ICON_TYPE_SYNC_CLASSIC = 1;
    ConfigManager config;
    Bitmap mCloudGreenBitmap;
    Bitmap mCloudGreenNoWifiBitmap;
    Bitmap mCloudOffBitmap;
    Bitmap mCloudOrangeBitmap;
    Bitmap mCloudRedBitmap;
    Bitmap mCloudYellowBitmap;
    Bitmap mCloudYellowNoWifiBitmap;
    ImageView mDropboxSyncStatus;
    private ContentObserver mObserver = null;
    public TextView mTitle;
    TasksListBp tasksListBp = new TasksListBpImpl();

    class C19212 implements OnClickListener {

        class C19191 implements DialogInterface.OnClickListener {
            C19191() {
            }

            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }

        class C19202 implements DialogInterface.OnClickListener {
            C19202() {
            }

            public void onClick(DialogInterface dialog, int whichButton) {
                AbstractMainListActivity.this.config.setDataEditInitiated(Boolean.valueOf(false));
                AbstractMainListActivity.this.config.setDropboxSyncThisTimeOnly(Boolean.valueOf(true));
                DropboxSyncHelper.requestSync(true);
            }
        }

        C19212() {
        }

        public void onClick(View v) {
            String syncMessage = AbstractMainListActivity.this.getLastSyncDescription() + "\nDelay sending changes: " + AbstractMainListActivity.this.formatSyncPeriod(AbstractMainListActivity.this.config.getDropboxSyncAfterLastEditIdleSeconds().intValue()) + "\nDelay receiving changes: " + AbstractMainListActivity.this.formatSyncPeriod(AbstractMainListActivity.this.config.getDropboxDelayInReceivingFilesFromOtherDevicesSeconds().intValue()) + "\n\nSync now?";
            String title = "Dropbox Auto Sync";
            if (AbstractMainListActivity.this.config.isDropboxAutoSyncOnWifiOnly() && !AbstractMainListActivity.this.config.isWifiConnected()) {
                syncMessage = "Auto sync is set to WIFI only. Syncing now could use your data minutes.\n" + AbstractMainListActivity.this.getLastSyncDescription() + "\nDo you want to sync now?";
                title = "Dropbox No WIFI";
            }
            new Builder(AbstractMainListActivity.this).setTitle(title).setMessage(syncMessage).setPositiveButton(R.string.alert_dialog_ok, new C19202()).setNegativeButton(R.string.alert_dialog_cancel, new C19191()).show();
        }
    }

    class C19223 implements OnClickListener {
        C19223() {
        }

        public void onClick(View v) {
            AppUtil.showMessage(AbstractMainListActivity.this, R.drawable.dropbox_sync_icon, "Dropbox Sync", "Dropbox sync is disabled. When enabled it will sync all devices running iRT GTasks Outliner. The sync is an ENTIRE DATABASE sync, the device with the latest database modification wins during syncing. To enable: Preferences -> Sync Preferences -> Dropbox Sync Preferences -> Auto Sync Dropbox.");
        }
    }

    public abstract String getEmailAllTasksBody();

    public abstract void refreshDisplay();

    public abstract void showCopyList();

    public abstract void showCopyListAsBranch();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.config = ConfigManager.getInstance();
    }

    private void setBitmaps() {
        switch (this.config.getDropboxSyncIconType().intValue()) {
            case 0:
                this.mCloudGreenBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cloud_green);
                this.mCloudYellowBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cloud_yellow);
                this.mCloudRedBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cloud_red);
                this.mCloudOffBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cloud_off);
                this.mCloudOrangeBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cloud_orange);
                this.mCloudGreenNoWifiBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cloud_green_no_wifi);
                this.mCloudYellowNoWifiBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cloud_yellow_no_wifi);
                return;
            case 1:
                this.mCloudGreenBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.sync_white);
                this.mCloudYellowBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.sync_yellow);
                this.mCloudRedBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.sync_red);
                this.mCloudOffBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cloud_off);
                this.mCloudOrangeBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.sync_orange);
                this.mCloudGreenNoWifiBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.sync_white_no_wifi);
                this.mCloudYellowNoWifiBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.sync_yellow_no_wifi);
                return;
            case 2:
                this.mCloudGreenBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.circle_green);
                this.mCloudYellowBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.circle_yellow);
                this.mCloudRedBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.circle_red);
                this.mCloudOffBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cloud_off);
                this.mCloudOrangeBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.circle_orange);
                this.mCloudGreenNoWifiBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.circle_green_no_wifi);
                this.mCloudYellowNoWifiBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.circle_yellow_no_wifi);
                return;
            default:
                return;
        }
    }

    protected void onResume() {
        super.onResume();
        setContentObserver();
        setBitmaps();
        setDropboxFileStatusImageView();
    }

    private void setContentObserver() {
        if (this.config.isDropboxAutoSync() && this.mObserver == null) {
            Uri taskUri = Uri.parse(DropboxSyncHelper.DROPBOX_URI_STRING);
            this.mObserver = new ContentObserver(new Handler(Looper.getMainLooper())) {
                public void onChange(boolean selfChange) {
                    AbstractMainListActivity.this.setDropboxCloudSyncStatus(AbstractMainListActivity.this.config.getDropboxSyncStatus(), true);
                }
            };
            getContentResolver().registerContentObserver(taskUri, true, this.mObserver);
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.mObserver != null) {
            getContentResolver().unregisterContentObserver(this.mObserver);
        }
    }

    private void setDropboxCloudSyncStatus(String syncStatus, boolean isRefreshDisplay) {
        if (DropboxSyncAdapter.SYNCING_STATUS_SOURCE_DATA_MODIFIED.equals(syncStatus)) {
            if (!this.config.isDropboxAutoSyncOnWifiOnly() || this.config.isWifiConnected()) {
                this.mDropboxSyncStatus.setImageBitmap(this.mCloudYellowBitmap);
            } else {
                this.mDropboxSyncStatus.setImageBitmap(this.mCloudYellowNoWifiBitmap);
            }
        } else if (DropboxSyncAdapter.SYNCING_STATUS_STARTING_FROM_FILE_LISTENER.equals(syncStatus)) {
            this.mDropboxSyncStatus.setImageBitmap(this.mCloudOrangeBitmap);
        } else if (DropboxSyncAdapter.SYNCING_STATUS_STARTING.equals(syncStatus)) {
            this.mDropboxSyncStatus.setImageBitmap(this.mCloudRedBitmap);
        } else if (DropboxSyncAdapter.SYNCING_STATUS_COMPLETE.equals(syncStatus)) {
            if (!this.config.isDropboxAutoSyncOnWifiOnly() || this.config.isWifiConnected()) {
                this.mDropboxSyncStatus.setImageBitmap(this.mCloudGreenBitmap);
            } else {
                this.mDropboxSyncStatus.setImageBitmap(this.mCloudGreenNoWifiBitmap);
            }
        } else if (DropboxSyncAdapter.SYNCING_STATUS_COMPLETE_WITH_DATABASE_UPDATE.equals(syncStatus)) {
            if (!this.config.isDropboxAutoSyncOnWifiOnly() || this.config.isWifiConnected()) {
                this.mDropboxSyncStatus.setImageBitmap(this.mCloudGreenBitmap);
            } else {
                this.mDropboxSyncStatus.setImageBitmap(this.mCloudGreenNoWifiBitmap);
            }
            if (isRefreshDisplay) {
                AppUtil.refreshDisplay();
            }
        } else {
            this.mDropboxSyncStatus.setImageBitmap(this.mCloudRedBitmap);
        }
    }

    private String formatSyncPeriod(int timePeriod) {
        if (timePeriod < 60) {
            return timePeriod + " sec";
        }
        if (timePeriod < 3600) {
            return (timePeriod / 60) + " min";
        } else if (timePeriod >= 43200) {
            return "1 day";
        } else {
            int timePeriodHour = (timePeriod / 60) / 60;
            return timePeriodHour + (timePeriodHour > 1 ? " hours" : " hour");
        }
    }

    private String getLastSyncDescription() {
        String lastSyncDescription = AppUtil.formatTimeBeforeNextAlarm((double) (((new Date().getTime() - this.config.getDropboxSyncDateMillis().longValue()) / 1000) / 60));
        if (StringUtil.isNullOrEmpty(lastSyncDescription)) {
            lastSyncDescription = " just now";
        }
        return "Last sync: " + lastSyncDescription;
    }

    private void setDropboxFileStatusImageView() {
        this.mDropboxSyncStatus = (ImageView) findViewById(R.id.dropbox_file_status);
        if (this.config.isDropboxAutoSync()) {
            setDropboxCloudSyncStatus(this.config.getDropboxSyncStatus(), false);
            this.mDropboxSyncStatus.setOnClickListener(new C19212());
            return;
        }
        this.mDropboxSyncStatus.setImageBitmap(this.mCloudOffBitmap);
        this.mDropboxSyncStatus.setOnClickListener(new C19223());
    }
}
