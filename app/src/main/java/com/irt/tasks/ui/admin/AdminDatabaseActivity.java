package com.irt.tasks.ui.admin;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;
import com.dropbox.sync.android.DbxAccountManager;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.DatabaseMetadata;
import com.irt.tasks.app.businessprocess.TasksListBp;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.DropboxDaoImpl;
import com.irt.tasks.app.dataaccess.FileDao;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.filter.TasksFilterManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

public class AdminDatabaseActivity extends AbstractActivity {
    public static final String APPLICATION_DIRECTORY = ("/data/" + AppUtil.getPackageName() + Globals.FORWARDSLASH);
    public static final String APPLICATION_FILES_DIRECTORY = (APPLICATION_DIRECTORY + "files/");
    private static final int BACKUP_ACTION = 0;
    public static final String BACKUP_DIRECTORY = "/irt_gtasks_outliner/backup/";
    public static final String BACKUP_FILES_DIRECTORY = "/irt_gtasks_outliner/backup/files/";
    private static final int BACKUP_WARNING_DIALOG = 2;
    private static final int CHECK_DATE_ACTION = 4;
    public static final String DATABASES_DIRECTORY = (APPLICATION_DIRECTORY + "databases/");
    public static final String DATABASE_FILE_PATH = (DATABASES_DIRECTORY + "irttasks");
    public static final String DATABASE_METADATA = "irttasks_metadata";
    public static final String DATABASE_NAME = "irttasks";
    static final int REQUEST_LINK_TO_DBX = 0;
    private static final int RESTORE_ACTION = 1;
    private static final int RESTORE_DIALOG = 1;
    private static final int RESTORE_SYNC_ACTION = 3;
    private static final int RESTORE_SYNC_WARNING_DIALOG = 3;
    Button backupButton;
    private ConfigManager config;
    FileDao fileDao = null;
    boolean isDropboxFileDateInitialized = false;
    private DbxAccountManager mAccountManager;
    TextView mDeviceLastBackup;
    CheckBox mDropboxMode;
    TextView mLastDataModification;
    TextView mLastDataModificationLabel;
    TextView mRecommendation;
    Button restoreButton;

    public class ProcessBackupOrRestore extends AsyncTask<String, Void, String> {
        int mBackupOrRestore;
        Context mContext;
        String mPostExecuteMessage;
        String mPreExecuteMessage;
        private ProgressDialog progressDialog;
        TasksListBp tasksListBp = new TasksListBpImpl();

        public ProcessBackupOrRestore(Context context, int backupOrRestore, String preExecuteMessage, String postExecuteMessage) {
            this.mContext = context;
            this.mBackupOrRestore = backupOrRestore;
            this.mPreExecuteMessage = preExecuteMessage;
            this.mPostExecuteMessage = postExecuteMessage;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = ProgressDialog.show(AdminDatabaseActivity.this, null, this.mPreExecuteMessage, true, true);
        }

        protected void onPostExecute(String message) {
            super.onPostExecute(message);
            this.progressDialog.dismiss();
            if (StringUtil.isNullOrEmpty(message)) {
                switch (this.mBackupOrRestore) {
                    case 1:
                    case 3:
                        AppUtil.scheduleAllAlarms(this.mContext);
                        break;
                }
                AdminDatabaseActivity.this.setupDeviceLastBackup();
                message = this.mPostExecuteMessage;
            }
            if (!StringUtil.isNullOrEmpty(message)) {
                Toast.makeText(this.mContext, message, Toast.LENGTH_LONG).show();
            }
        }

        protected String doInBackground(String... toOrFromFilePath) {
            long lastModifiedDate = 0;
            DatabaseMetadata localDatabaseMetadata;
            switch (this.mBackupOrRestore) {
                case 0:
                    localDatabaseMetadata = new DatabaseMetadata();
                    AppUtil.copyLocalDatabaseAndMetadataToDropbox(AdminDatabaseActivity.this.fileDao, localDatabaseMetadata, AppUtil.setLocalDatabaseMetadata(localDatabaseMetadata), Constants.DROPBOX_DATABASE_RELATIVE_FILE_PATH, Constants.DROPBOX_DATABASE_META_DATA_RELATIVE_FILE_PATH);
                    AdminDatabaseActivity.this.config.setDropboxLastBackupDateFromThisDeviceMillis(localDatabaseMetadata.getLastModifiedDateMillis());
                    AdminDatabaseActivity.this.config.setBackupInitializedAfterInstall(Boolean.valueOf(true));
                    return null;
                case 1:
                    break;
                case 3:
                    lastModifiedDate = new Date().getTime();
                    localDatabaseMetadata = new DatabaseMetadata();
                    File localDatabaseFile = AppUtil.setLocalDatabaseMetadata(localDatabaseMetadata);
                    localDatabaseMetadata.setLastModifiedDateMillis(Long.valueOf(lastModifiedDate));
                    AppUtil.copyLocalMetadataToDropbox(AdminDatabaseActivity.this.fileDao, localDatabaseMetadata, localDatabaseFile, Constants.DROPBOX_DATABASE_META_DATA_RELATIVE_FILE_PATH);
                    break;
                case 4:
                    AdminDatabaseActivity.this.config.setDropboxLastBackupDateFromThisDeviceMillis(AdminDatabaseActivity.this.getDropboxDatabaseLastModifiedDateMillis());
                    return null;
                default:
                    return null;
            }
            try {
                Log.d(Constants.TAG, "******/irttasks.db-" + toOrFromFilePath[0] + "-" + "irttasks" + "******");
                AppUtil.forceCopyFromDropBox(Constants.DROPBOX_DATABASE_RELATIVE_FILE_PATH, toOrFromFilePath[0], "irttasks");
                if (lastModifiedDate == 0) {
                    lastModifiedDate = AdminDatabaseActivity.this.getDropboxDatabaseLastModifiedDateMillis().longValue();
                }
                AdminDatabaseActivity.this.config.setDropboxLastBackupDateFromThisDeviceMillis(Long.valueOf(lastModifiedDate));
                AppUtil.setLastApplicationModifiedDate(lastModifiedDate);
                AdminDatabaseActivity.this.config.setBackupInitializedAfterInstall(Boolean.valueOf(true));
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return e.getMessage();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                return throwable.getMessage();
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.config = ConfigManager.getInstance();
        setContentView(R.layout.admin_database);
        this.config.setPreferenceAccessed(true);
    }

    protected void onResume() {
        super.onResume();
        this.isDropboxFileDateInitialized = false;
        initializeViewsAndListeners();
        initSDCardBackupFileDate();
    }

    private void initSDCardBackupFileDate() {
        this.config.setSDCardLastBackupDateFromThisDeviceMillis(getLocalDatabaseLastModifiedDateMillis());
    }

    private void initializedDropboxFileDate() {
        if (!this.isDropboxFileDateInitialized && this.config.isDropboxMode()) {
            this.isDropboxFileDateInitialized = true;
            this.fileDao = new DropboxDaoImpl();
            new ProcessBackupOrRestore(this, 4, "Getting Dropbox file date....", null).execute(new String[0]);
        }
    }

    private void initializeViewsAndListeners() {
        this.mDeviceLastBackup = (TextView) findViewById(R.id.device_last_backup);
        this.mLastDataModification = (TextView) findViewById(R.id.last_modified);
        this.mLastDataModificationLabel = (TextView) findViewById(R.id.last_modified_label);
        this.mRecommendation = (TextView) findViewById(R.id.recommendation);
        this.backupButton = (Button) findViewById(R.id.backup);
        this.restoreButton = (Button) findViewById(R.id.restore);
        this.mDropboxMode = (CheckBox) findViewById(R.id.dropbox_mode);
        this.mDropboxMode.setChecked(this.config.isDropboxMode());
        setBackupMode();
        this.mDropboxMode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AdminDatabaseActivity.this.setBackupMode();
            }
        });
        setupDeviceLastBackup();
        this.backupButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AdminDatabaseActivity.this.config.getLastAppModificationDateMillis().longValue() < AdminDatabaseActivity.this.getBackupFileDate()) {
                    AdminDatabaseActivity.this.showDialog(2);
                } else {
                    AdminDatabaseActivity.this.backupDatabaseToSDCardOrDropbox();
                }
            }
        });
        this.restoreButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AdminDatabaseActivity.this.config.isDropboxAutoSync() || AdminDatabaseActivity.this.getBackupFileDate() >= AdminDatabaseActivity.this.config.getLastAppModificationDateMillis().longValue()) {
                    AdminDatabaseActivity.this.showDialog(1);
                } else {
                    AdminDatabaseActivity.this.showDialog(3);
                }
            }
        });
    }

    private void setupDeviceLastBackup() {
        long deviceLastBackupMillis = getBackupFileDate();
        if (deviceLastBackupMillis > 0) {
            this.mDeviceLastBackup.setText(AppUtil.formatLastDeviceUpdateDate(deviceLastBackupMillis));
        } else {
            this.mDeviceLastBackup.setText("None");
        }
        setupLastDataModification(deviceLastBackupMillis);
    }

    private long getBackupFileDate() {
        long deviceLastBackupMillis = this.config.getDropboxLastBackupDateFromThisDeviceMillis().longValue();
        if (this.mDropboxMode.isChecked()) {
            return deviceLastBackupMillis;
        }
        return this.config.getSDCardLastBackupDateFromThisDeviceMillis().longValue();
    }

    private void setupLastDataModification(long backupDateMillis) {
        long lastAppModificationDateMillis = this.config.getLastAppModificationDateMillis().longValue();
        if (lastAppModificationDateMillis > backupDateMillis) {
            colorRecommendation(Color.parseColor("#006A35"), " Do a Backup ");
            this.mLastDataModificationLabel.setText("Last modification LATER than backup");
        } else if (lastAppModificationDateMillis == backupDateMillis) {
            this.mLastDataModificationLabel.setText("Last modification SAME as backup");
            colorRecommendation(-16777216, " Do nothing ");
        } else {
            colorRecommendation(-65536, " Do a Restore ");
            this.mLastDataModificationLabel.setText("Last modification EARLIER than backup");
        }
        if (lastAppModificationDateMillis > 0) {
            this.mLastDataModification.setText(AppUtil.formatLastDeviceUpdateDate(lastAppModificationDateMillis));
        } else {
            this.mLastDataModification.setText("None");
        }
    }

    private void colorRecommendation(int bgColor, String statusMsg) {
        SpannableStringBuilder label;
        SpannableString recommendation;
        if (this.config.isBackupInitializedAfterInstall()) {
            label = new SpannableStringBuilder("Recommendation: ");
            recommendation = new SpannableString(statusMsg);
            recommendation.setSpan(new ForegroundColorSpan(-1), 0, recommendation.length(), 0);
            recommendation.setSpan(new BackgroundColorSpan(bgColor), 0, recommendation.length(), 0);
            label.append(recommendation);
            this.mRecommendation.setText(label);
            return;
        }
        label = new SpannableStringBuilder("Fresh install: ");
        recommendation = new SpannableString(" Backup/Restore with caution ");
        //recommendation.setSpan(new ForegroundColorSpan(-16777216), 0, recommendation.length(), 0);
        recommendation.setSpan(new BackgroundColorSpan(-256), 0, recommendation.length(), 0);
        label.append(recommendation);
        this.mRecommendation.setText(label);
    }

    private void setBackupMode() {
        if (this.mDropboxMode.isChecked()) {
            this.mAccountManager = DbxAccountManager.getInstance(getApplicationContext(), DropboxDaoImpl.APP_KEY, DropboxDaoImpl.SECRET_KEY);
            if (this.mAccountManager.hasLinkedAccount()) {
                setDropboxMode();
                initializedDropboxFileDate();
            } else {
                this.mAccountManager.startLink((Activity) this, 0);
            }
        } else {
            setSdCardMode();
        }
        setupDeviceLastBackup();
    }

    private void setDropboxMode() {
        this.config.setDropboxMode(true);
        SpannableStringBuilder backupButtonText = new SpannableStringBuilder("Backup to Dropbox");
        //backupButtonText.setSpan(new ForegroundColorSpan(-65536), 0, backupButtonText.length(), 0);
        backupButtonText.setSpan(new StyleSpan(1), 0, backupButtonText.length(), 0);
        this.backupButton.setText(backupButtonText);
        SpannableStringBuilder restoreButtonText = new SpannableStringBuilder("Restore from Dropbox");
        //restoreButtonText.setSpan(new ForegroundColorSpan(-65536), 0, restoreButtonText.length(), 0);
        restoreButtonText.setSpan(new StyleSpan(1), 0, restoreButtonText.length(), 0);
        this.restoreButton.setText(restoreButtonText);
    }

    private void setSdCardMode() {
        this.config.setDropboxMode(false);
        this.backupButton.setText("Backup to SD Card");
        this.restoreButton.setText("Restore from SD Card");
    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new Builder(this).setTitle(R.string.alert_dialog_restore_db).setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AdminDatabaseActivity.this.restoreDataBaseFromSDCardOrDropbox(false);
                        TasksFilterManager.getInstance().resetFilter();
                    }
                }).setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).create();
            case 2:
                return new Builder(this).setTitle(R.string.alert_dialog_backup_db).setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AdminDatabaseActivity.this.backupDatabaseToSDCardOrDropbox();
                    }
                }).setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).create();
            case 3:
                return new Builder(this).setTitle("IMPORTANT: Dropbox Sync Mode").setMessage("You are in dropbox sync mode and your backup is OLDER than SYNC DATABASE, do you want to restore and REPLACE the SYNC DATABASE?").setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AdminDatabaseActivity.this.restoreDataBaseFromSDCardOrDropbox(true);
                        TasksFilterManager.getInstance().resetFilter();
                    }
                }).setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).create();
            default:
                return null;
        }
    }

    private void restoreDataBaseFromSDCardOrDropbox(boolean isMakeLatest) {
        if (this.mDropboxMode.isChecked()) {
            restoreDatabaseFromDropbox(isMakeLatest);
        } else {
            restoreDatabaseFromSdCard(isMakeLatest);
        }
    }

    private void restoreDatabaseFromDropbox(boolean isMakeLatest) {
        File dataDirectory = Environment.getDataDirectory();
        int restoreAction = 1;
        if (isMakeLatest) {
            restoreAction = 3;
        }
        String toFilePath = dataDirectory.getAbsolutePath() + DATABASES_DIRECTORY;
        new ProcessBackupOrRestore(this, restoreAction, "Restoring from dropbox....", "Database was successfully restored").execute(new String[]{toFilePath});
    }

    private void restoreDatabaseFromSdCard(boolean isMakeLatest) {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        File dataDirectory = Environment.getDataDirectory();
        String message = "ERROR: Unable to restore database. Please turn OFF USB storage if it is ON.";
        if (AppUtil.isExternalStorageMounted()) {
            try {
                long lastAppModificationDateMillis;
                forceCopy(externalStorageDirectory.getAbsolutePath() + BACKUP_DIRECTORY + getExternalStorageToFilename(), dataDirectory.getAbsolutePath() + DATABASES_DIRECTORY, "irttasks");
                if (isMakeLatest) {
                    lastAppModificationDateMillis = new Date().getTime();
                    writeMetadataToSdCard(externalStorageDirectory, externalStorageDirectory.getAbsolutePath() + BACKUP_DIRECTORY, "", lastAppModificationDateMillis);
                } else {
                    lastAppModificationDateMillis = getLocalDatabaseLastModifiedDateMillis().longValue();
                }
                this.config.setSDCardLastBackupDateFromThisDeviceMillis(Long.valueOf(lastAppModificationDateMillis));
                AppUtil.setLastApplicationModifiedDate(lastAppModificationDateMillis);
                setupDeviceLastBackup();
                this.config.setBackupInitializedAfterInstall(Boolean.valueOf(true));
                message = "Database was successfully restored";
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                message = throwable.getMessage();
            }
        }
        AppUtil.scheduleAllAlarms(this);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private Long getLocalDatabaseLastModifiedDateMillis() {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        Long lastModifiedDateMillis = Long.valueOf(0);
        Long metadataFileModifiedDateMillis = Long.valueOf(0);
        Long localDatabaseFileModifiedDateMillis = Long.valueOf(0);
        String localDatabaseDirectory = externalStorageDirectory.getAbsolutePath() + BACKUP_DIRECTORY;
        String locaclDatabaseMetadataFilePath = localDatabaseDirectory + Constants.DROPBOX_DATABASE_META_DATA_RELATIVE_FILE_PATH.substring(0);
        String locaclDatabaseFilePath = localDatabaseDirectory + getExternalStorageToFilename();
        if (new File(locaclDatabaseMetadataFilePath).exists()) {
            metadataFileModifiedDateMillis = Long.valueOf(new File(locaclDatabaseMetadataFilePath).lastModified());
        }
        if (new File(locaclDatabaseFilePath).exists()) {
            localDatabaseFileModifiedDateMillis = Long.valueOf(new File(locaclDatabaseFilePath).lastModified());
        }
        DatabaseMetadata localDatabaseMetadata = new DatabaseMetadata();
        if (metadataFileModifiedDateMillis.longValue() > 0 && localDatabaseFileModifiedDateMillis.longValue() > 0) {
            String jsonMetadata = "";
            try {
                jsonMetadata = new BufferedReader(new FileReader(locaclDatabaseMetadataFilePath)).readLine();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            localDatabaseMetadata.setObjFromJsonString(jsonMetadata);
            return localDatabaseMetadata.getLastModifiedDateMillis();
        } else if (metadataFileModifiedDateMillis.longValue() != 0 || localDatabaseFileModifiedDateMillis.longValue() <= 0) {
            return lastModifiedDateMillis;
        } else {
            return localDatabaseFileModifiedDateMillis;
        }
    }

    private void backupDatabaseToSDCardOrDropbox() {
        if (this.mDropboxMode.isChecked()) {
            backupDatabaseToDropbox();
        } else {
            backupDatabaseToSdCard();
        }
    }

    private void backupDatabaseToDropbox() {
        File dataDirectory = Environment.getDataDirectory();
        String fromFilePath = AppUtil.getDatabaseFilePath();
        new ProcessBackupOrRestore(this, 0, "Backing up to dropbox....", "Database successfully copied to Dropbox: \n/Apps/irtgtasksoutliner/irttasks.db").execute(new String[]{fromFilePath});
    }

    private void backupDatabaseToSdCard() {
        ConfigManager config = ConfigManager.getInstance();
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        File dataDirectory = Environment.getDataDirectory();
        String message = "ERROR: Unable to backup database. Please turn OFF USB storage if it is ON.";
        if (AppUtil.isExternalStorageMounted()) {
            String fromFilePath = AppUtil.getDatabaseFilePath();
            String toFilePath = externalStorageDirectory.getAbsolutePath() + BACKUP_DIRECTORY;
            try {
                String toFilename = forceCopy(fromFilePath, toFilePath, getExternalStorageToFilename());
                writeMetadataToSdCard(externalStorageDirectory, toFilePath, "", 0);
                message = "Database successfully copied to: \n" + toFilename;
                initSDCardBackupFileDate();
                config.setBackupInitializedAfterInstall(Boolean.valueOf(true));
                setupDeviceLastBackup();
                if (config.isCreateExtraBackup()) {
                    String fileDate = getFileBackupStringDate();
                    String externalStorageToFilenameWithDate = "irttasks" + fileDate + ".db";
                    forceCopy(fromFilePath, toFilePath, externalStorageToFilenameWithDate);
                    message = message + "\n and " + externalStorageToFilenameWithDate;
                    writeMetadataToSdCard(externalStorageDirectory, toFilePath, fileDate, 0);
                }
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                message = throwable.getMessage();
            }
        }
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void writeMetadataToSdCard(File externalStorageDirectory, String toFilePath, String fileDateString, long lastModifiedDate) throws FileNotFoundException {
        File backupDatabaseFile = new File(toFilePath + getExternalStorageToFilename());
        DatabaseMetadata localDatabaseMetadata = new DatabaseMetadata();
        AppUtil.setLocalDatabaseMetadata(localDatabaseMetadata);
        if (lastModifiedDate > 0) {
            localDatabaseMetadata.setLastModifiedDateMillis(Long.valueOf(lastModifiedDate));
        }
        File metaDataFile = new File(externalStorageDirectory.getAbsolutePath() + BACKUP_DIRECTORY + Constants.DROPBOX_DATABASE_META_DATA_RELATIVE_FILE_PATH.substring(0) + fileDateString);
        backupDatabaseFile.getParentFile().mkdirs();
        PrintWriter out = new PrintWriter(metaDataFile);
        out.print(localDatabaseMetadata.toJsonString());
        out.close();
    }

    private String forceCopy(String fromFilePath, String toDirectory, String toFilename) throws Throwable {
        Throwable th;
        File fromFile = new File(fromFilePath);
        File toFile = new File(toDirectory);
        if (!fromFile.exists()) {
            throw new IOException("FileCopy: no such source file: " + fromFilePath);
        } else if (!fromFile.isFile()) {
            throw new IOException("FileCopy: can't copy directory: " + fromFilePath);
        } else if (fromFile.canRead()) {
            if (!toFile.exists()) {
                toFile.mkdirs();
            }
            if (toFile.isDirectory()) {
                toFile = new File(toFile, toFilename);
            }
            String toFileFullPath = toFile.getAbsolutePath();
            String parent = toFile.getParent();
            if (parent == null) {
                parent = System.getProperty("user.dir");
            }
            File dir = new File(parent);
            if (!dir.exists()) {
                throw new IOException("FileCopy: destination directory doesn't exist: " + parent);
            } else if (dir.isFile()) {
                throw new IOException("FileCopy: destination is not a directory: " + parent);
            } else if (dir.canWrite()) {
                FileInputStream from = null;
                FileOutputStream to = null;
                try {
                    FileInputStream from2 = new FileInputStream(fromFile);
                    try {
                        FileOutputStream to2 = new FileOutputStream(toFile);
                        try {
                            byte[] buffer = new byte[4096];
                            while (true) {
                                int bytesRead = from2.read(buffer);
                                if (bytesRead == -1) {
                                    break;
                                }
                                to2.write(buffer, 0, bytesRead);
                            }
                            if (from2 != null) {
                                try {
                                    from2.close();
                                } catch (IOException e) {
                                }
                            }
                            if (to2 != null) {
                                try {
                                    to2.close();
                                } catch (IOException e2) {
                                }
                            }
                            return toFileFullPath;
                        } catch (Throwable th2) {
                            th = th2;
                            to = to2;
                            from = from2;
                            if (from != null) {
                                try {
                                    from.close();
                                } catch (IOException e3) {
                                }
                            }
                            if (to != null) {
                                try {
                                    to.close();
                                } catch (IOException e4) {
                                }
                            }
                            throw th;
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        from = from2;
                        if (from != null) {
                            from.close();
                        }
                        if (to != null) {
                            to.close();
                        }
                        throw th;
                    }
                } catch (Throwable th4) {
                    th = th4;
                    if (from != null) {
                        from.close();
                    }
                    if (to != null) {
                        to.close();
                    }
                    throw th;
                }
            } else {
                throw new IOException("FileCopy: destination directory is unwriteable: " + parent);
            }
        } else {
            throw new IOException("FileCopy: source file is unreadable: " + fromFilePath);
        }
    }

    private String getExternalStorageToFilename() {
        return "irttasks.db";
    }

    private String getFileBackupStringDate() {
        return DateFormat.format("-yy-MM-dd-hh-mm", AppUtil.getTodayDateTime()).toString();
    }

    private Long getDropboxDatabaseLastModifiedDateMillis() {
        Long lastModifiedDateMillis = Long.valueOf(0);
        Long metadataFileModifiedDateMillis = Long.valueOf(this.fileDao.getFileLastModifiedDateMillis(Constants.DROPBOX_DATABASE_META_DATA_RELATIVE_FILE_PATH));
        Long dropboxDatabaseFileModifiedDateMillis = Long.valueOf(this.fileDao.getFileLastModifiedDateMillis(Constants.DROPBOX_DATABASE_RELATIVE_FILE_PATH));
        DatabaseMetadata dropboxDatabaseMetadata = new DatabaseMetadata();
        if (metadataFileModifiedDateMillis.longValue() > 0 && dropboxDatabaseFileModifiedDateMillis.longValue() > 0) {
            AppUtil.setDropboxMetadata(this.fileDao, dropboxDatabaseMetadata, Constants.DROPBOX_DATABASE_META_DATA_RELATIVE_FILE_PATH);
            return dropboxDatabaseMetadata.getLastModifiedDateMillis();
        } else if (metadataFileModifiedDateMillis.longValue() != 0 || dropboxDatabaseFileModifiedDateMillis.longValue() <= 0) {
            return lastModifiedDateMillis;
        } else {
            return dropboxDatabaseFileModifiedDateMillis;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != 0) {
            super.onActivityResult(requestCode, resultCode, data);
        } else if (resultCode == -1) {
            setDropboxMode();
            initializedDropboxFileDate();
        } else {
            setSdCardMode();
        }
    }
}
