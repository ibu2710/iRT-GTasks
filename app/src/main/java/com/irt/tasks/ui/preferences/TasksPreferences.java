package com.irt.tasks.ui.preferences;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.view.KeyEvent;

import com.dropbox.sync.android.DbxAccountManager;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Email;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.EmailBp;
import com.irt.tasks.app.businessprocess.EmailBpImpl;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.businessprocess.TasksListBp;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.DropboxDaoImpl;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.receiver.AlarmSetterHelper;
import com.irt.tasks.ui.sync.DropboxSyncHelper;
import com.irt.tasks.ui.taskslist.OptionsMenuHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TasksPreferences extends PreferenceActivity implements OnInitListener {
    public static final String APP_VERSION_PREF_KEY = "application_version_preference";
    public static final String AUTO_SNOOZE_DURATION_LIST_PREF_KEY = "auto_snooze_duration_preference";
    public static final String AUTO_SYNC_CHECKBOX_PREF_KEY = "aut_sync_checkbox";
    public static final String AUTO_SYNC_END_TIME_LIST_PREF_KEY = "auto_sync_end_time_preference";
    public static final String AUTO_SYNC_FREQUENCY_LIST_PREF_KEY = "auto_sync_frequency_preference";
    public static final String AUTO_SYNC_START_TIME_LIST_PREF_KEY = "auto_sync_start_time_preference";
    public static final String BACKUP_INITIALIZED_AFTER_INSTALL_CHECKBOX_PREF_KEY = "is_backup_initialized_after_install";
    public static final String BONSAI_DATE_FORMAT_LIST_PREF_KEY = "bonsai_date_format_list_preference";
    public static final String BOTTOM_LEFT_RIGHT_SWIPE_LIST_PREF_KEY = "bottom_left_right_swipe_preference";
    public static final String CREATE_EXTRA_BACKUP_CHECKBOX_KEY = "create_extra_backup";
    public static final String DATE_FORMAT_LIST_PREF_KEY = "date_format_preference";
    public static final String DEBUG_CHECKBOX_PREF_KEY = "debug_checkbox";
    public static final String DEFAULT_EMAIL_RECIPIENT_EDIT_TEXT_KEY = "default_email_recipient_edit_text";
    public static final String DRAG_AND_DROP_CHECKBOX_PREF_KEY = "drag_and_drop_preference_checkbox";
    public static final String DROPBOX_AUTO_SYNC_CHECKBOX_PREF_KEY = "dropbox_auto_sync_checkbox";
    public static final String DROPBOX_AUTO_SYNC_ON_WIFI_ONLY_CHECKBOX_PREF_KEY = "dropbox_auto_sync_on_wifi_only_checkbox";
    public static final String f73x6debeda0 = "dropbox_delay_in_receiving_files_from_other_devices_preference";
    public static final String DROPBOX_LAST_BACKUP_DATE_FROM_THIS_DEVICE_MILLIS_KEY = "dropbox_last_backup_date_from_this_device_millis";
    public static final String DROPBOX_MODE = "dropboxMode";
    public static final String DROPBOX_SYNC_AFTER_LAST_EDIT_IDLE_LIST_PREF_KEY = "dropbox_sync_after_last_edit_idle_preference";
    public static final String DROPBOX_SYNC_DATE_PREF_KEY = "dropbox_sync_date_preference";
    public static final String DROPBOX_SYNC_ICON_LIST_PREF_KEY = "dropbox_sync_icon_preference";
    public static final String DROPBOX_SYNC_STATUS_PREF_KEY = "dropbox_sync_status";
    public static final String ENABLE_REMINDER_CHECKBOX_KEY = "reminder_enable";
    public static final String ENABLE_REMINDER_EMAIL_CHECKBOX_PREF_KEY = "enable_reminder_email_preference_checkbox";
    public static final String ENABLE_TEXT_TO_SPEECH_CHECKBOX_KEY = "enable_text_to_speech";
    public static final String EXPIRATION_DATE_MILLIS_KEY = "expiration_date_millis";
    public static final String HIDE_COMPLETED_TASKS_CHECKBOX_PREF_KEY = "hide_completed_tasks";
    public static final String HIGHLIGHT_STYLE_LIST_PREF_KEY = "highlight_style_preference";
    public static final String IS_DATA_EDIT_INITIATED_PREF_KEY = "is_data_edit_initiated_preference";
    public static final String JSON_TASKS_FILTER_OBJECT_PREF_KEY = "json_tasks_filter_object";
    public static final String LAST_APP_MODIFCATION_DATE_MILLIS_KEY = "last_app_modification_date_millis";
    public static final String LAST_SELECTED_TASKS_LIST_PREF_KEY = "last_selected_tasks_list_preference";
    public static final String LAST_SYNC_INFO_DETAILS_PREF_KEY = "last_sync_info_details_preference";
    public static final String LAST_SYNC_INFO_PREF_KEY = "last_sync_info_preference";
    public static final String LEFT_DOUBLE_TAP_LIST_PREF_KEY = "left_double_tap_preference";
    public static final String LEFT_LONG_PRESS_LIST_PREF_KEY = "left_long_press_preference";
    public static final String LEFT_TAP_LIST_PREF_KEY = "left_tap_preference";
    public static final String LINKIFY_NOTES_CHECKBOX_PREF_KEY = "linkify_notes_checkbox";
    public static final String LOCAL_DATABASE_CREATION_DATE_PREF_KEY = "local_database_creation_date_preference";
    public static final String LOCAL_DATABASE_MODIFIED_DATE_PREF_KEY = "local_database_modified_date_preference";
    public static final String LOCK_DATE_FORMAT_CHECKBOX_PREF_KEY = "lock_date_format";
    public static final String LOCK_EXPORT_TYPE_CHECKBOX_PREF_KEY = "lock_export_type";
    public static final String MANUAL_SNOOZE_DURATION_DEFAULT_LIST_PREF_KEY = "manual_snooze_default_duration_preference";
    public static final String MIDDLE_LEFT_RIGHT_SWIPE_LIST_PREF_KEY = "middle_left_right_swipe_preference";
    private static final int MY_DATA_CHECK_CODE = 0;
    public static final String NOTE_VISIBLE_LINES_KEY = "note_visible_lines";
    public static final String PANELED_DRAG_AND_DROP_CHECKBOX_PREF_KEY = "paneled_drag_and_drop";
    public static final String PANEL_TO_SHOW_LIST_PREF_KEY = "panel_to_show_list_preference";
    public static final String REMINDER_EMAIL_VALIDATED_CHECKBOX_PREF_KEY = "reminder_email_validated";
    public static final String REMINDER_ENABLE_SOUND_CHECKBOX_KEY = "reminder_enable_sound";
    public static final String REMINDER_SNOOZE_DURATION_LIST_PREF_KEY = "snooze_duration_preference";
    static final int REQUEST_LINK_TO_DBX = 1;
    public static final String RIGHT_DOUBLE_TAP_LIST_PREF_KEY = "right_double_tap_preference";
    public static final String RIGHT_LONG_PRESS_LIST_PREF_KEY = "right_long_press_preference";
    public static final String RIGHT_TAP_LIST_PREF_KEY = "right_tap_preference";
    public static final String SCREEN_SIZE_PREF_KEY = "screen_size_preference";
    public static final String SD_CARD_LAST_BACKUP_DATE_FROM_THIS_DEVICE_MILLIS_KEY = "sd_card_last_backup_date_from_this_device_millis";
    public static final String SHOW_ALL_TASKS_ON_FILTER_CHECKBOX_PREF_KEY = "show_all_on_filters";
    public static final String SHOW_DONE_CHECKBOX_CHECKBOX_PREF_KEY = "show_done_checkbox_preference_checkbox";
    public static final String SHOW_DROPBOX_CLOUD_HELP_MESSAGE_KEY = "show_dropbox_cloud_help_message";
    public static final String SHOW_FLOATS_ON_AGENDA_CHECKBOX_PREF_KEY = "show_floats_on_agenda";
    public static final String SHOW_INVISIBLE_ACCOUNTS_PREF_KEY = "show_inivisible_accounts_preference";
    public static final String SHOW_MENU_BUTTON_CHECKBOX_PREF_KEY = "show_menu_button";
    public static final String SHOW_QUESTION_MARK_MESSAGE_KEY = "show_question_mark_message";
    public static final String SHOW_TASKS_CONTEXTS_CHECKBOX_PREF_KEY = "show_tasks_contexts";
    public static final String SHOW_TASKS_LIST_STATISTICS_CHECKBOX_PREF_KEY = "show_tasks_list_statistics";
    public static final String SHOW_TASK_NUMBERS_CHECKBOX_PREF_KEY = "show_task_numbers";
    public static final String SORT_FILENAMES_BY_DATE_CHECKBOX_PREF_KEY = "sort_by_date_desc";
    public static final String START_DAY_LIST_PREF_KEY = "start_day_preference";
    public static final String STICKY_VIEW_CHECKBOX_KEY = "sticky_view_checkbox";
    public static final String SWIPE_MOVE_LEFT_RIGHT_ON_DRAG_AND_DROP_PREF_KEY = "swipe_move_left_right_on_drag_and_drop";
    public static final String TALKING_CALENDAR_INITIALIZED_CHECKBOX_PREF_KEY = "talking_calendar_initialized_preference_checkbox";
    public static final String TASKS_LIST_FOR_NEW_TASK_PLUS_LIST_PREF_KEY = "tasks_list_for_new_task_plus_preference";
    public static final String TASKS_LIST_SORT_PREF_KEY = "tasks_list_sort_preference";
    public static final String TASK_NAME_FONT_SIZE_LIST_PREF_KEY = "task_name_font_size";
    public static final String TASK_NOTE_FONT_SIZE_LIST_PREF_KEY = "task_note_font_size";
    public static final String TIME_FORMAT_LIST_PREF_KEY = "time_format_preference";
    public static final String TOP_LEFT_RIGHT_SWIPE_LIST_PREF_KEY = "top_left_right_swipe_preference";
    public static final String TUTORIALS_TASKS_LISTS_CREATED_KEY = "tutorials_tasks_lists_created";
    public static final String VIEW_FROM_HOME_CHECKBOX_PREF_KEY = "view_from_home";
    ConfigManager config;
    EmailBp emailBp = new EmailBpImpl();
    private DbxAccountManager mAccountManager;
    Preference mApplicationVersion;
    ListPreference mAutoSyncEndTimeListPreference;
    ListPreference mAutoSyncFrequencyListPreference;
    ListPreference mAutoSyncStartTimeListPreference;
    Preference mDropboxSyncDate;
    boolean mIsAllowSpeech = false;
    CheckBoxPreference mIsAutoSyncCheckBoxPreference;
    CheckBoxPreference mIsEnableDropboxAutoSync;
    CheckBoxPreference mIsEnableReminderCheckBoxPreference;
    CheckBoxPreference mIsEnableReminderEmailCheckBoxPreference;
    CheckBoxPreference mIsEnableTextToSpeechCheckBoxPreference;
    CheckBoxPreference mIsWifiAutoSyncOnly;
    Preference mLastSyncInfo;
    Preference mLastSyncInfoDetails;
    Preference mLocalDatabaseCreatoinDate;
    Preference mLocalDatabaseModifiedDate;
    ListPreference mScreenSizeListPreference;
    int mSelectedTasksListPostion = 0;
    ListPreference mTasksListForNewTaskPlusListPreference;
    String[] mTasksListIds;
    String[] mTasksListNames;
    List<TasksList> mTasksLists;
    private TextToSpeech mTts;
    TasksListBp tasksListBp = new TasksListBpImpl();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.config = ConfigManager.getInstance();
        this.config.setPreferenceAccessed(true);
        checkForNewRelease(this);
        addPreferencesFromResource(R.xml.preferences);
        setShowInvisibleAccounts();
        setTasksListSort();
        setupScreenSize();
        setApplicationVersion();
        setLastSyncInfo();
        setLastSyncInfoDetails();
        setEnableReminderEmail();
        setAutoSyncCheckboxListener();
        setAutoSyncStartTimeListener();
        setAutoSyncEndTimeListener();
        setAutoSyncFrequencyListener();
        setTasksListsTutorials();
        setEnableTextToSpeech();
        setEnableReminder();
        setEnabldDropboxAutoSync();
        setDatabaseLastModificationDate();
        setLocalDatabaseCreationDate();
        setWifiAutoSyncOnly();
    }

    private void requestForSync() {
        if (this.config.isDropboxAutoSync()) {
            this.mAccountManager = DbxAccountManager.getInstance(getApplicationContext(), DropboxDaoImpl.APP_KEY, DropboxDaoImpl.SECRET_KEY);
            if (this.mAccountManager.hasLinkedAccount()) {
                DropboxSyncHelper.backendRequestSyncWithDelay(1);
            } else {
                this.mAccountManager.startLink((Activity) this, 1);
            }
        }
    }

    private void setEnableReminder() {
        this.mIsEnableReminderCheckBoxPreference = (CheckBoxPreference) getPreferenceScreen().findPreference(ENABLE_REMINDER_CHECKBOX_KEY);
        this.mIsEnableReminderCheckBoxPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                TasksPreferences.this.config.setEnableReminder(((Boolean) newValue).booleanValue());
                new TaskBpImpl().clearAllSnoozeTasks(DatabaseHelper.createTasksListCalledAll(), TasksPreferences.this);
                AlarmSetterHelper.resetAllAlarms(TasksPreferences.this.config.getContext());
                return true;
            }
        });
    }

    private void setEnabldDropboxAutoSync() {
        this.mIsEnableDropboxAutoSync = (CheckBoxPreference) getPreferenceScreen().findPreference(DROPBOX_AUTO_SYNC_CHECKBOX_PREF_KEY);
        this.mIsEnableDropboxAutoSync.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (((Boolean) newValue).booleanValue()) {
                    String title = "";
                    String message = "";
                    if (TasksPreferences.this.config.getDropboxSyncDateMillis().longValue() > 0) {
                        title = "IMPORTANT Initial Sync";
                        message = "Dropbox sync will use the latest database to REPLACE other databases on other devices. OVERWRITE this device from other devices, or make this the LATEST database. Cancel to abort sync mode.";
                    } else {
                        title = "IMPORTANT Initial Sync";
                        message = "Do a backup first! Dropbox sync will use the latest database to REPLACE other databases on other devices. OVERWRITE this device from other devices, or make this the LATEST database. Cancel to abort sync mode.";
                    }
                    new Builder(TasksPreferences.this).setTitle(title).setCancelable(false).setMessage(message).setPositiveButton("Overwrite", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            TasksPreferences.this.config.setLastAppModificationDateMillisWithNoDataEditInitiated(Long.valueOf(new Date("1/1/1950").getTime()));
                            TasksPreferences.this.requestForSync();
                        }
                    }).setNeutralButton("Latest", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            TasksPreferences.this.config.setLastAppModificationDateMillisWithNoDataEditInitiated(Long.valueOf(new Date().getTime()));
                            TasksPreferences.this.requestForSync();
                        }
                    }).setNegativeButton(R.string.alert_dialog_cancel, new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            TasksPreferences.this.mIsEnableDropboxAutoSync.setChecked(false);
                        }
                    }).show();
                } else {
                    DropboxSyncHelper.backendRequestSyncWithDelay(1);
                }
                return true;
            }
        });
    }

    private void setWifiAutoSyncOnly() {
        this.mIsWifiAutoSyncOnly = (CheckBoxPreference) getPreferenceScreen().findPreference(DROPBOX_AUTO_SYNC_ON_WIFI_ONLY_CHECKBOX_PREF_KEY);
        this.mIsWifiAutoSyncOnly.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (!((Boolean) newValue).booleanValue()) {
                    new Builder(TasksPreferences.this).setTitle("WARNING Sync All the Time").setCancelable(false).setMessage("Turning OFF \"Wifi Auto Sync only\" could result to lots of data minutes consumption due to syncing. Do you want to turn it OFF? ").setPositiveButton(R.string.alert_dialog_ok, new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            TasksPreferences.this.mIsWifiAutoSyncOnly.setChecked(false);
                        }
                    }).setNegativeButton(R.string.alert_dialog_cancel, new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            TasksPreferences.this.mIsWifiAutoSyncOnly.setChecked(true);
                        }
                    }).show();
                }
                return true;
            }
        });
    }

    private void setDatabaseLastModificationDate() {
        this.mDropboxSyncDate = getPreferenceScreen().findPreference(DROPBOX_SYNC_DATE_PREF_KEY);
        if (this.config.getDropboxSyncDateMillis().longValue() > 0) {
            this.mDropboxSyncDate.setSummary(AppUtil.formatLastDeviceUpdateDate(this.config.getDropboxSyncDateMillis().longValue()));
        }
        this.mLocalDatabaseModifiedDate = getPreferenceScreen().findPreference(LOCAL_DATABASE_MODIFIED_DATE_PREF_KEY);
        if (this.mLocalDatabaseModifiedDate != null && this.config.getLastAppModificationDateMillis().longValue() != 0) {
            this.mLocalDatabaseModifiedDate.setSummary(AppUtil.formatLastDeviceUpdateDate(this.config.getLastAppModificationDateMillis().longValue()));
        }
    }

    private void setLocalDatabaseCreationDate() {
        this.mLocalDatabaseCreatoinDate = getPreferenceScreen().findPreference(LOCAL_DATABASE_CREATION_DATE_PREF_KEY);
        this.mLocalDatabaseCreatoinDate.setSummary(AppUtil.formatLastDeviceUpdateDate(this.config.getDatabaseCreationDateMillis().longValue()));
    }

    private void setEnableTextToSpeech() {
        this.mIsEnableTextToSpeechCheckBoxPreference = (CheckBoxPreference) getPreferenceScreen().findPreference(ENABLE_TEXT_TO_SPEECH_CHECKBOX_KEY);
        this.mIsEnableTextToSpeechCheckBoxPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (((Boolean) newValue).booleanValue()) {
                    TasksPreferences.this.checkForTextToSpeachInstallation();
                    TasksPreferences.this.mIsAllowSpeech = true;
                }
                return true;
            }
        });
    }

    private void checkForTextToSpeachInstallation() {
        Intent checkIntent = new Intent();
        checkIntent.setAction("android.speech.tts.engine.CHECK_TTS_DATA");
        startActivityForResult(checkIntent, 0);
    }

    public void say(String text2say) {
        this.mTts.speak(text2say, 0, AppUtil.getHashAlarm());
    }

    public void onInit(int status) {
        if (!this.config.isTalkingCalendarInitialized() || this.mIsAllowSpeech) {
            say("Talking alarm enabled!");
            this.config.setTalkingCalendarInitialized(Boolean.valueOf(true));
            this.mIsAllowSpeech = false;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == -1) {
                DropboxSyncHelper.backendRequestSyncWithDelay(1);
            } else {
                this.mIsEnableDropboxAutoSync.setChecked(false);
            }
        } else if (requestCode != 0) {
        } else {
            if (resultCode == 1) {
                this.mTts = new TextToSpeech(this, this);
                this.mIsEnableTextToSpeechCheckBoxPreference.setChecked(true);
                return;
            }
            new Builder(this).setTitle(R.string.alert_dialog_install_text_to_speech).setPositiveButton(R.string.alert_dialog_ok, new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent installIntent = new Intent();
                    installIntent.setAction("android.speech.tts.engine.INSTALL_TTS_DATA");
                    TasksPreferences.this.startActivity(installIntent);
                    TasksPreferences.this.mIsEnableTextToSpeechCheckBoxPreference.setChecked(true);
                }
            }).setNegativeButton(R.string.alert_dialog_cancel, new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    TasksPreferences.this.mIsEnableTextToSpeechCheckBoxPreference.setChecked(false);
                }
            }).show();
        }
    }

    private void setEnableReminderEmail() {
        setReminderEmailValidated();
        this.mIsEnableReminderEmailCheckBoxPreference = (CheckBoxPreference) getPreferenceScreen().findPreference(ENABLE_REMINDER_EMAIL_CHECKBOX_PREF_KEY);
        this.mIsEnableReminderEmailCheckBoxPreference.setEnabled(this.config.isReminderEmailValidated());
        if (!this.config.isReminderEmailValidated()) {
            this.config.setEnableReminderEmail(false);
        }
    }

    private void setReminderEmailValidated() {
        Email email = this.emailBp.get();
        if (email == null || StringUtil.isNullOrEmpty(email.getEmail()) || StringUtil.isNullOrEmpty(email.getPassword())) {
            this.config.setReminderEmailValidated(Boolean.valueOf(false));
        }
    }

    private void setTasksListsTutorials() {
        if (this.config.isTutorialsTasksListsCreated() == null || !this.config.isTutorialsTasksListsCreated().booleanValue()) {
            new TasksListBpImpl().createTutorialTaskLists(DatabaseHelper.OFFLINE_ACCOUNT_ID);
            this.config.setTutorialsTasksListsCreated(Boolean.valueOf(true));
        }
    }

    protected void onResume() {
        super.onResume();
        setEnableReminderEmail();
        setTasksListForNewTaskPlusListPreference();
    }

    private void setTasksListForNewTaskPlusListPreference() {
        this.mTasksListForNewTaskPlusListPreference = (ListPreference) getPreferenceScreen().findPreference(TASKS_LIST_FOR_NEW_TASK_PLUS_LIST_PREF_KEY);
        initTasksLists();
        this.mTasksListForNewTaskPlusListPreference.setEntries(this.mTasksListNames);
        this.mTasksListForNewTaskPlusListPreference.setEntryValues(this.mTasksListIds);
        if (!isSelectedTasksListVisible()) {
            this.mTasksListForNewTaskPlusListPreference.setValue(Constants.TASKS_LIST_AUTO_ID + "");
        }
    }

    private boolean isSelectedTasksListVisible() {
        for (TasksList tasksList : this.mTasksLists) {
            if ((tasksList.getId() + "").equals(this.mTasksListForNewTaskPlusListPreference.getValue())) {
                return true;
            }
        }
        return false;
    }

    private void initTasksLists() {
        this.mTasksLists = new ArrayList();
        List<TasksList> tasksLists = this.tasksListBp.getAllTasksListBySortOrder();
        TasksList autoTasksList = new TasksList();
        autoTasksList.setId(Constants.TASKS_LIST_AUTO_ID);
        autoTasksList.setName("Last accessed");
        this.mTasksLists.add(autoTasksList);
        for (TasksList tasksList : tasksLists) {
            if (this.config.isShowInvisibleAccounts().booleanValue() || tasksList.isVisible().booleanValue()) {
                this.mTasksLists.add(tasksList);
            }
        }
        this.mTasksListNames = new String[this.mTasksLists.size()];
        this.mTasksListIds = new String[this.mTasksLists.size()];
        for (int i = 0; i < this.mTasksLists.size(); i++) {
            TasksList tasksList2 = (TasksList) this.mTasksLists.get(i);
            this.mTasksListNames[i] = tasksList2.getName();
            this.mTasksListIds[i] = tasksList2.getId() + "";
        }
    }

    private void setAutoSyncStartTimeListener() {
        this.mAutoSyncStartTimeListPreference = (ListPreference) getPreferenceScreen().findPreference(AUTO_SYNC_START_TIME_LIST_PREF_KEY);
        this.mAutoSyncStartTimeListPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                TasksPreferences.this.config.setAutoSyncStartTime(new Integer(newValue + ""));
                AlarmSetterHelper.scheduleAutoSync(TasksPreferences.this);
                return true;
            }
        });
    }

    private void setAutoSyncEndTimeListener() {
        this.mAutoSyncEndTimeListPreference = (ListPreference) getPreferenceScreen().findPreference(AUTO_SYNC_END_TIME_LIST_PREF_KEY);
        this.mAutoSyncEndTimeListPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                TasksPreferences.this.config.setAutoSyncEndTime(new Integer(newValue + ""));
                AlarmSetterHelper.scheduleAutoSync(TasksPreferences.this);
                return true;
            }
        });
    }

    private void setAutoSyncFrequencyListener() {
        this.mAutoSyncFrequencyListPreference = (ListPreference) getPreferenceScreen().findPreference(AUTO_SYNC_FREQUENCY_LIST_PREF_KEY);
        this.mAutoSyncFrequencyListPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                TasksPreferences.this.config.setAutoSyncFrequency(new Integer(newValue + ""));
                AlarmSetterHelper.scheduleAutoSync(TasksPreferences.this);
                return true;
            }
        });
    }

    private void setAutoSyncCheckboxListener() {
        this.mIsAutoSyncCheckBoxPreference = (CheckBoxPreference) getPreferenceScreen().findPreference(AUTO_SYNC_CHECKBOX_PREF_KEY);
        this.mIsAutoSyncCheckBoxPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                TasksPreferences.this.config.setAutoSync(((Boolean) newValue).booleanValue());
                AlarmSetterHelper.scheduleAutoSync(TasksPreferences.this);
                return true;
            }
        });
    }

    private void setShowInvisibleAccounts() {
        if (this.config.isShowInvisibleAccounts() == null) {
            this.config.setShowInvisibleAccounts(Boolean.valueOf(false));
        }
    }

    private void setTasksListSort() {
        if (this.config.getTasksListSort() == null) {
            this.config.setTasksListSort(Integer.valueOf(0));
        }
    }

    private void checkForNewRelease(Context context) {
        String applicationVersionName = AppUtil.getVersionName();
        if (!applicationVersionName.equals(this.config.getApplicationVersion())) {
            this.config.setApplicationVersion(applicationVersionName);
            OptionsMenuHelper.showReleaseNotes(context);
            if (!this.config.isTalkingCalendarInitialized()) {
                checkForTextToSpeachInstallation();
            }
        }
    }

    private void setApplicationVersion() {
        this.mApplicationVersion = getPreferenceScreen().findPreference(APP_VERSION_PREF_KEY);
        this.mApplicationVersion.setSummary(this.config.getApplicationVersion());
    }

    private void setLastSyncInfo() {
        this.mLastSyncInfo = getPreferenceScreen().findPreference(LAST_SYNC_INFO_PREF_KEY);
        if (!StringUtil.isNullOrEmpty(this.config.getLastSyncInfo())) {
            this.mLastSyncInfo.setSummary(this.config.getLastSyncInfo());
        }
    }

    private void setLastSyncInfoDetails() {
        this.mLastSyncInfoDetails = getPreferenceScreen().findPreference(LAST_SYNC_INFO_DETAILS_PREF_KEY);
        if (!StringUtil.isNullOrEmpty(this.config.getLastSyncInfoDetails())) {
            this.mLastSyncInfoDetails.setSummary(this.config.getLastSyncInfoDetails());
        }
    }

    private void setupScreenSize() {
        this.mScreenSizeListPreference = (ListPreference) getPreferenceScreen().findPreference(SCREEN_SIZE_PREF_KEY);
        this.mScreenSizeListPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference arg0, Object screenSize) {
                TasksPreferences.this.config.setScreenSize(Integer.valueOf(Integer.parseInt((String) screenSize)));
                TasksPreferences.this.config.setScreenSize();
                return true;
            }
        });
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.mTts != null) {
            this.mTts.shutdown();
        }
    }

    private void showCloudHelpMessage() {
        if (this.config.isDropboxAutoSync() && this.config.isShowDropboxCloudHelpMessage()) {
            new Builder(this).setTitle("Dropbox Cloud Sync Icon").setMessage("Dropbox CLOUD sync icon at the top right corner of views indicate the status of the sync. You can also tap on it for IMMEDIATE syncing.\n\nRED means sync is on-going, refrain from using the app until it becomes green.\n\nORANGE means remote modification detected, waiting for more changes, refrain from using the app until it becomes green. The wait time before starting to receive the remote modifications can be adjusted in Dropbox Sync Preferences.\n\nYELLOW means local modification detected, you can continue normal editing activities. After a time of no activity, sync will be started. The idle time before sync starts can be adjusted in Dropbox Sync Preferences.\n\nGREEN means the device database is in sync with other devices. Careful though if you have modifications on another device that are in yellow, wait first until the other device becomes green.\n\nShow this message again?").setPositiveButton(R.string.alert_dialog_yes, new OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    TasksPreferences.this.config.setShowDropboxCloudHelpMessage(Boolean.valueOf(true));
                    TasksPreferences.this.finish();
                }
            }).setNegativeButton(R.string.alert_dialog_no, new OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    TasksPreferences.this.config.setShowDropboxCloudHelpMessage(Boolean.valueOf(false));
                    TasksPreferences.this.finish();
                }
            }).show();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        try {
            showCloudHelpMessage();
            return super.onKeyDown(keyCode, event);
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return true;
        }
    }
}
