package com.irt.tasks.ui.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.os.Environment;
import com.irt.tasks.app.businessobjects.DatabaseMetadata;
import com.irt.tasks.app.businessobjects.Devices;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.dataaccess.DropboxDaoImpl;
import com.irt.tasks.app.dataaccess.FileDao;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.admin.AdminDatabaseActivity;
import com.irt.tasks.ui.receiver.AlarmSetterHelper;
import java.io.File;
import java.io.IOException;
import java.util.Date;

public class DropboxSyncAdapter extends AbstractThreadedSyncAdapter {
    public static final String SYNCING_STATUS_COMPLETE = "syncing_status_complete";
    public static final String SYNCING_STATUS_COMPLETE_WITH_DATABASE_UPDATE = "syncing_status_with_database_update";
    public static final String SYNCING_STATUS_SOURCE_DATA_MODIFIED = "syncing_status_source_data_modified";
    public static final String SYNCING_STATUS_STARTING = "syncing_status_starting";
    public static final String SYNCING_STATUS_STARTING_FROM_FILE_LISTENER = "syncing_status_starting_from_file_listener";
    public static final String SYNC_EXTRAS_FORCE_REFRESH = "forceRefresh";
    ConfigManager config;
    FileDao fileDao = new DropboxDaoImpl();
    boolean isDropboxListenerEnabled = false;
    ContentResolver mContentResolver;

    public DropboxSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        this.mContentResolver = context.getContentResolver();
    }

    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        this.config = ConfigManager.getInstance();
        if (!this.config.isDropboxAutoSync()) {
            AppUtil.logDebug("==========================> Remove File Listener.....<========================");
            this.fileDao.removeSyncMetadataFileListener();
            this.isDropboxListenerEnabled = false;
        } else if (!this.config.isDropboxSyncThisTimeOnly() && this.config.isDropboxAutoSyncOnWifiOnly() && !this.config.isWifiConnected()) {
            AppUtil.logDebug("==========================> Remove File Listener.....<========================");
            this.fileDao.removeSyncMetadataFileListener();
            this.isDropboxListenerEnabled = false;
        } else if (!this.config.isDropboxAutoSync() || this.isDropboxListenerEnabled) {
            DropboxSyncHelper.broadCastSyncStatus(SYNCING_STATUS_STARTING);
            AppUtil.logDebug("-");
            AppUtil.logDebug("-");
            AppUtil.logDebug("-" + new Date());
            AppUtil.logDebug("-");
            AppUtil.logDebug("==========================> [" + Devices.getDeviceName() + "] Syncing with Dropbox.....<========================");
            DropboxSyncHelper.cancelBackendSyncRequest();
            boolean isForceRefresh = false;
            if (extras != null) {
                isForceRefresh = extras.getBoolean(SYNC_EXTRAS_FORCE_REFRESH);
            }
            this.config.setDropboxSyncDateMillis(Long.valueOf(new Date().getTime()));
            if (syncDropbox(isForceRefresh)) {
                DropboxSyncHelper.broadCastSyncStatus(SYNCING_STATUS_COMPLETE_WITH_DATABASE_UPDATE);
                AppUtil.logDebug("Completed restoration of Local Database from dropbox.");
            } else {
                DropboxSyncHelper.broadCastSyncStatus(SYNCING_STATUS_COMPLETE);
                AppUtil.logDebug("Complete with no updates to local database.");
            }
            this.config.setDropboxSyncThisTimeOnly(Boolean.valueOf(false));
            DropboxSyncHelper.backendResetFirstTimeRunWithDelay();
            if (this.config.isDropboxAutoSyncOnWifiOnly() && !this.config.isWifiConnected()) {
                AppUtil.logDebug("==========================> Remove File Listener.....<========================");
                this.fileDao.removeSyncMetadataFileListener();
                this.isDropboxListenerEnabled = false;
            }
        } else {
            AppUtil.logDebug("==========================> Set File Listener.....<========================");
            this.fileDao.setSyncMetadataFileListener();
            this.isDropboxListenerEnabled = true;
            DropboxSyncHelper.backendRequestSyncWithDelay(2, this.config.isDropboxSyncThisTimeOnly());
        }
    }

    private boolean syncDropbox(boolean isForceRefresh) {
        FileDao fileDao = new DropboxDaoImpl();
        DatabaseMetadata dropboxDatabaseMetadata = new DatabaseMetadata();
        DatabaseMetadata localDatabaseMetadata = new DatabaseMetadata();
        File localDatabaseFile = AppUtil.setLocalDatabaseMetadata(localDatabaseMetadata);
        boolean isLocalDatabaseCurrent = isLocalDatabaseCurrent(fileDao, dropboxDatabaseMetadata, localDatabaseMetadata);
        AppUtil.logDebug("Dropbox metadata is " + dropboxDatabaseMetadata.toJsonString());
        if (localDatabaseMetadata.isTimeEquals(dropboxDatabaseMetadata)) {
            if (isForceRefresh) {
                return performCopyDropboxDatabaseToLocalDatabase(dropboxDatabaseMetadata);
            }
            AppUtil.logDebug("[" + localDatabaseMetadata.getDeviceName() + "] has the same time as dropbox database, don't do anything.");
            return false;
        } else if (!isLocalDatabaseCurrent) {
            return performCopyDropboxDatabaseToLocalDatabase(dropboxDatabaseMetadata);
        } else {
            copyLocalDatabaseAndMetadataToDropboxSync(fileDao, localDatabaseMetadata, localDatabaseFile);
            return false;
        }
    }

    private boolean performCopyDropboxDatabaseToLocalDatabase(DatabaseMetadata dropboxDatabaseMetadata) {
        copyDropboxDatabaseToLocalDatabase(dropboxDatabaseMetadata);
        AppUtil.delay(12000);
        AlarmSetterHelper.resetAllAlarms(this.config.getContext());
        return true;
    }

    private void copyDropboxDatabaseToLocalDatabase(DatabaseMetadata dropboxDatabaseMetadata) {
        this.config.setLastAppModificationDateMillisWithNoDataEditInitiated(dropboxDatabaseMetadata.getLastModifiedDateMillis());
        this.config.setDatabaseCreationDateMillis(dropboxDatabaseMetadata.getCreationDateMillis());
        AppUtil.logDebug("[" + Devices.getDeviceName() + "] is replacing current database with dropbox database...");
        try {
            AppUtil.forceCopyFromDropBox(Constants.DROPBOX_DATABASE_SYNC_RELATIVE_FILE_PATH, Environment.getDataDirectory().getAbsolutePath() + AdminDatabaseActivity.DATABASES_DIRECTORY, "irttasks");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private void copyLocalDatabaseAndMetadataToDropboxSync(FileDao fileDao, DatabaseMetadata localDatabaseMetadata, File localDatabaseFile) {
        this.config.setDropboxFileSentFromThisDevice(Boolean.valueOf(true));
        DropboxSyncHelper.backendDelayFileListening();
        AppUtil.copyLocalDatabaseAndMetadataToDropbox(fileDao, localDatabaseMetadata, localDatabaseFile, Constants.DROPBOX_DATABASE_SYNC_RELATIVE_FILE_PATH, Constants.DROPBOX_DATABASE_SYNC_META_DATA_RELATIVE_FILE_PATH);
    }

    private boolean isLocalDatabaseCurrent(FileDao fileDao, DatabaseMetadata dropboxDatabaseMetadata, DatabaseMetadata localDatabaseMetadata) {
        return DropboxSyncAdapterHelper.isLocalDatabaseCurrent(fileDao, dropboxDatabaseMetadata, localDatabaseMetadata, Long.valueOf(fileDao.getFileLastModifiedDateMillis(Constants.DROPBOX_DATABASE_SYNC_META_DATA_RELATIVE_FILE_PATH)), Long.valueOf(fileDao.getFileLastModifiedDateMillis(Constants.DROPBOX_DATABASE_SYNC_RELATIVE_FILE_PATH)));
    }
}
