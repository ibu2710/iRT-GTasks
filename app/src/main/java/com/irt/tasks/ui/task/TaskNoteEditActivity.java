package com.irt.tasks.ui.task;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.sync.DropboxSyncHelper;

public class TaskNoteEditActivity extends AbstractActivity {
    ConfigManager config;
    Button mCancelButton;
    EditText mNotesEditText;
    Button mSaveButton;
    Task mTaskFromCallingActivity;
    EditText mTaskNameEditText;
    TaskBp taskBp = new TaskBpImpl();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.config = ConfigManager.getInstance();
        setContentView(R.layout.task_note_edit);
        Object obj = ArchitectureContext.getObject();
        if (obj != null && (obj instanceof Task)) {
            this.mTaskFromCallingActivity = (Task) ArchitectureContext.getObject();
        }
        initViews();
        setTaskNameAndNotes();
        initTaskNameEditText();
        initNotesEditText();
        initCancelButton();
        initSaveButton();
    }

    private void setTaskNameAndNotes() {
        this.mTaskNameEditText.setText(this.mTaskFromCallingActivity.getName());
        this.mTaskNameEditText.setTextSize((float) this.config.getTaskNoteFontSize().intValue());
        this.mNotesEditText.setText(this.mTaskFromCallingActivity.getNote());
        this.mNotesEditText.setTextSize((float) this.config.getTaskNoteFontSize().intValue());
    }

    private void initViews() {
        this.mTaskNameEditText = (EditText) findViewById(R.id.task_name);
        this.mNotesEditText = (EditText) findViewById(R.id.notes);
        this.mSaveButton = (Button) findViewById(R.id.save_button);
        this.mCancelButton = (Button) findViewById(R.id.cancel_button);
    }

    private void enableSaving() {
        this.mSaveButton.setEnabled(true);
    }

    private void initTaskNameEditText() {
        this.mTaskNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                TaskNoteEditActivity.this.enableSaving();
                DropboxSyncHelper.uiRequestSyncWithDelay();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initNotesEditText() {
        this.mNotesEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                TaskNoteEditActivity.this.enableSaving();
                DropboxSyncHelper.uiRequestSyncWithDelay();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initSaveButton() {
        this.mSaveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                TaskNoteEditActivity.this.perfromSave();
                TaskNoteEditActivity.this.setResult(-1);
                TaskNoteEditActivity.this.finish();
            }
        });
    }

    private void perfromSave() {
        this.mTaskFromCallingActivity.setName(this.mTaskNameEditText.getText().toString());
        this.mTaskFromCallingActivity.setNote(this.mNotesEditText.getText().toString());
        this.mTaskFromCallingActivity.setLastContentModifiedDate(Long.valueOf(AppUtil.getTodayDateTimeMilisecond()));
        this.taskBp.saveOrUpdate(this.mTaskFromCallingActivity, null);
        this.config.setPreferenceAccessed(true);
    }

    private void initCancelButton() {
        this.mCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                TaskNoteEditActivity.this.performCancel();

            }
        });
    }

    private void performCancel() {
        if (this.mSaveButton.isEnabled()) {
            new Builder(this).setTitle(R.string.alert_dialog_document_modified).setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    TaskNoteEditActivity.this.setResult(-1);
                    TaskNoteEditActivity.this.finish();
                }
            }).setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).show();
            return;
        }
        setResult(-1);
        finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        performCancel();
        return true;
    }
}
