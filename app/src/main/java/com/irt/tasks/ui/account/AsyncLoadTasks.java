package com.irt.tasks.ui.account;

import java.io.IOException;

class AsyncLoadTasks extends CommonAsyncTask {
    AsyncLoadTasks(AccountEditActivity tasksSample) {
        super(tasksSample);
    }

    protected void doInBackground() throws IOException {
        this.client.tasklists().list().execute();
    }

    public static void run(AccountEditActivity tasksSample) {
        new AsyncLoadTasks(tasksSample).execute(new Void[0]);
    }
}
