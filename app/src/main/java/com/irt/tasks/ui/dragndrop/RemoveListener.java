package com.irt.tasks.ui.dragndrop;

public interface RemoveListener {
    void onRemove(int i);
}
