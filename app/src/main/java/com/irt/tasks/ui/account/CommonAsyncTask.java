package com.irt.tasks.ui.account;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.services.tasks.Tasks;
import com.irt.tasks.app.common.Constants;
import java.io.IOException;

abstract class CommonAsyncTask extends AsyncTask<Void, Void, Boolean> {
    final AccountEditActivity activity;
    final Tasks client;
    private ProgressDialog progressDialog;

    protected abstract void doInBackground() throws IOException;

    CommonAsyncTask(AccountEditActivity activity) {
        this.activity = activity;
        this.client = activity.mService;
    }

    protected void onPreExecute() {
        super.onPreExecute();
        AccountEditActivity accountEditActivity = this.activity;
        accountEditActivity.numAsyncTasks++;
        this.progressDialog = ProgressDialog.show(this.activity, null, "Verifying the account....", true, true);
    }

    protected final Boolean doInBackground(Void... ignored) {
        try {
            doInBackground();
            return Boolean.valueOf(true);
        } catch (GooglePlayServicesAvailabilityIOException availabilityException) {
            this.activity.showGooglePlayServicesAvailabilityErrorDialog(availabilityException.getConnectionStatusCode());
            return Boolean.valueOf(false);
        } catch (UserRecoverableAuthIOException userRecoverableException) {
            this.activity.startActivityForResult(userRecoverableException.getIntent(), 1);
            return Boolean.valueOf(false);
        } catch (IOException e) {
            Utils.logAndShow(this.activity, Constants.TAG, e);
            return Boolean.valueOf(false);
        }
    }

    protected final void onPostExecute(Boolean success) {
        super.onPostExecute(success);
        AccountEditActivity accountEditActivity = this.activity;
        int i = accountEditActivity.numAsyncTasks - 1;
        accountEditActivity.numAsyncTasks = i;
        if (i == 0) {
            this.progressDialog.dismiss();
        }
        if (success.booleanValue()) {
            Toast.makeText(this.activity, "Successful", Toast.LENGTH_LONG).show();
        }
    }
}
