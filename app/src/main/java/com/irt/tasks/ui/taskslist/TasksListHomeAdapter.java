package com.irt.tasks.ui.taskslist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.List;

public class TasksListHomeAdapter extends BaseAdapter {
    ConfigManager config = ConfigManager.getInstance();
    private Bitmap mAllFolderIcon;
    Context mContext;
    private Bitmap mDropboxFolderIcon;
    private Bitmap mDropboxSyncFolderIcon;
    private Bitmap mFolderIcon;
    private Bitmap mGoogleFolderIcon;
    private Bitmap mGoogleSyncFolderIcon;
    private LayoutInflater mInflater;
    protected List<TasksList> mTasksLists;
    TaskBp taskBp = new TaskBpImpl();

    static class ViewHolder {
        TextView accountName;
        ImageView folder;
        LinearLayout listItem;
        ImageView note;
        TextView taskListName;
        TextView totals;

        ViewHolder() {
        }
    }

    public TasksListHomeAdapter(Context context, List<TasksList> tasksLists) {
        this.mInflater = LayoutInflater.from(context);
        this.mTasksLists = tasksLists;
        this.mContext = context;
        initializeListItems(context);
    }

    private void initializeListItems(Context context) {
        this.mAllFolderIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.todo_list_all);
        this.mFolderIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.todo_list);
        this.mGoogleFolderIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.todo_list_google2);
        this.mDropboxFolderIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.todo_list_dropbox);
        this.mGoogleSyncFolderIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.todo_list_google_sync2);
        this.mDropboxSyncFolderIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.todo_list_dropbox_sync);
    }

    public int getCount() {
        return this.mTasksLists.size();
    }

    public Object getItem(int position) {
        return getTasksList(position);
    }

    public long getItemId(int position) {
        return getTasksList(position).getId().longValue();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate(R.layout.tasks_list_home_list_item, null);
            holder = initViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        setTasksListName(position, holder);
        setAccountName(position, holder);
        setTotals(position, holder);
        TasksList taskList = getTasksList(position);
        if (DatabaseHelper.ALL_TASKS_LIST_ID.equals(taskList.getId())) {
            holder.folder.setImageBitmap(this.mAllFolderIcon);
        } else if (taskList.getAccountId() == DatabaseHelper.OFFLINE_ACCOUNT_ID) {
            holder.folder.setImageBitmap(this.mFolderIcon);
        } else if (taskList.getAccountId() != DatabaseHelper.OFFLINE_ACCOUNT_ID && taskList.isAutoSync().booleanValue()) {
            holder.folder.setImageBitmap(this.mGoogleSyncFolderIcon);
        } else if (taskList.getAccountId() != DatabaseHelper.OFFLINE_ACCOUNT_ID) {
            holder.folder.setImageBitmap(this.mGoogleFolderIcon);
        }
        return convertView;
    }

    private void setTotals(int position, ViewHolder holder) {
        holder.totals.setText(getTasksList(position).getTotals());
    }

    private void setAccountName(int position, ViewHolder holder) {
        holder.accountName.setText(getTasksList(position).getAccount().getEmail().split("@")[0]);
    }

    private void setTasksListName(int position, ViewHolder holder) {
        holder.taskListName.setText(getTasksList(position).getName());
    }

    private TasksList getTasksList(int position) {
        return (TasksList) this.mTasksLists.get(position);
    }

    private ViewHolder initViewHolder(View convertView) {
        ViewHolder holder = new ViewHolder();
        holder.taskListName = (TextView) convertView.findViewById(R.id.list_name);
        holder.accountName = (TextView) convertView.findViewById(R.id.account_name);
        holder.folder = (ImageView) convertView.findViewById(R.id.folder);
        holder.totals = (TextView) convertView.findViewById(R.id.total_tasks);
        return holder;
    }
}
