package com.irt.tasks.ui.dragndrop;

import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

public interface DragListener {
    void blankOutDraggedItemListItem(View view);

    ListAdapter getMyListAdapter();

    void onDrag(int i, int i2, ListView listView, int i3);

    void onStartDrag(View view, int i);

    void onStopDrag(View view);

    void setDisplacedListItem(Object obj, View view);
}
