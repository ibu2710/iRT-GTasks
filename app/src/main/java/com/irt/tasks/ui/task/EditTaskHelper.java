package com.irt.tasks.ui.task;

import android.graphics.Color;
import com.google.android.gms.games.Notifications;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.ui.preferences.ArraysConstants;
import com.sun.activation.registries.MailcapTokenizer;
import java.util.Calendar;

public class EditTaskHelper {
    public static int getFontColor(int position, int autoColor) {
        switch (position) {
            case 0:
                return autoColor;
            case 1:
                return -1;
            case 2:
                return -65536;
            case 3:
                return -16776961;
            case 4:
                return -16711681;
            case 5:
                return -65281;
            case 6:
                return -16711936;
            case 7:
                return -256;
            case 8:
                return Color.parseColor("#FFD700");
            case 9:
                return Color.parseColor("#FF8409");
            case 10:
                return Color.parseColor("#800000");
            case 11:
                return Color.parseColor("#808000");
            case 12:
                return Color.parseColor("#009933");
            case 13:
                return -7829368;
            case 14:
                return -12303292;
            case 15:
                return -16777216;
            case 16:
                return Color.parseColor("#D50000");
            case 17:
                return Color.parseColor("#EA0000");
            case ArraysConstants.VIEW_MODIFIED /*18*/:
                return Color.parseColor("#FF1515");
            case 19:
                return Color.parseColor("#FF2B2B");
            case 20:
                return Color.parseColor("#0000D5");
            case 21:
                return Color.parseColor("#0000EA");
            case 22:
                return Color.parseColor("#1515FF");
            case 23:
                return Color.parseColor("#2B2BFF");
            case 24:
                return Color.parseColor("#00D5D5");
            case 25:
                return Color.parseColor("#00EAEA");
            case 26:
                return Color.parseColor("#15FFFF");
            case 27:
                return Color.parseColor("#2BFFFF");
            case 28:
                return Color.parseColor("#D500D5");
            case 29:
                return Color.parseColor("#EA00EA");
            case 30:
                return Color.parseColor("#FF15FF");
            case Notifications.NOTIFICATION_TYPES_ALL:
                return Color.parseColor("#FF2BFF");
            case 32:
                return Color.parseColor("#00552B");
            case 33:
                return Color.parseColor("#006A35");
            case 34:
                return Color.parseColor("#00954A");
            case Constants.WINDOW_TITLE_MAX_LENGTH /*35*/:
                return Color.parseColor("#00AA55");
            case 36:
                return Color.parseColor("#D5D500");
            case 37:
                return Color.parseColor("#EAEA00");
            case 38:
                return Color.parseColor("#FFFF15");
            case 39:
                return Color.parseColor("#FFFF2B");
            case 40:
                return Color.parseColor("#D58000");
            case 41:
                return Color.parseColor("#EA8C00");
            case 42:
                return Color.parseColor("#FFA215");
            case 43:
                return Color.parseColor("#FFAA2B");
            case 44:
                return Color.parseColor("#550000");
            case ArraysConstants.EXPAND_COLLAPSE_BRANCH_ALL_CHILDREN /*45*/:
                return Color.parseColor("#6A0000");
            case 46:
                return Color.parseColor("#950000");
            case MailcapTokenizer.SLASH_TOKEN /*47*/:
                return Color.parseColor("#AA0000");
            case 48:
                return Color.parseColor("#555500");
            case 49:
                return Color.parseColor("#6A6A00");
            case 50:
                return Color.parseColor("#959500");
            case 51:
                return Color.parseColor("#AAAA00");
            default:
                return -16777216;
        }
    }

    public static int getFontStyle(int stylePosition) {
        switch (stylePosition) {
            case 2:
                return 1;
            default:
                return 0;
        }
    }

    public static void calculateReminderDate(int daysEarlier, Task task, Calendar reminderDateTime) {
        Calendar dueDateCal = Calendar.getInstance();
        dueDateCal.setTimeInMillis(task.getDueDate().longValue());
        dueDateCal.set(Calendar.HOUR_OF_DAY, reminderDateTime.get(Calendar.HOUR_OF_DAY));
        dueDateCal.set(Calendar.MINUTE, reminderDateTime.get(Calendar.MINUTE));
        dueDateCal.add(Calendar.DAY_OF_YEAR, -daysEarlier);
        reminderDateTime.setTimeInMillis(dueDateCal.getTimeInMillis());
    }
}
