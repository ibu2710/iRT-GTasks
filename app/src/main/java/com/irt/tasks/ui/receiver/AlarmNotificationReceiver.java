package com.irt.tasks.ui.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Email;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessprocess.EmailBp;
import com.irt.tasks.app.businessprocess.EmailBpImpl;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.DateUtil;
import com.irt.tasks.app.common.ElapsedTime;
import com.irt.tasks.app.common.EmailUtility;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.TaskDao;
import com.irt.tasks.app.dataaccess.TaskDaoImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.task.SnoozeTaskActivity;
import com.irt.tasks.ui.taskslist.TasksListActivity;
import java.util.Calendar;
import java.util.Date;

public class AlarmNotificationReceiver extends BroadcastReceiver implements OnInitListener, OnUtteranceCompletedListener {
    private ConfigManager config;
    EmailBp emailBp = new EmailBpImpl();
    String mSpokenText;
    TextToSpeech mTts;
    TaskBp taskBp = new TaskBpImpl();
    TaskDao taskDao = new TaskDaoImpl();
    String utteranceId = "test";

    public void onReceive(Context context, Intent intent) {
        if (Constants.ACTION_ALARM.equals(intent.getAction())) {
            this.config = ConfigManager.getInstance();
            Long taskId = Long.valueOf(intent.getLongExtra("taskId", 0));
            Task task = this.taskBp.get(taskId);
            String message = intent.getStringExtra("sync_alarm_message");
            AppUtil.logDebug("-");
            AppUtil.logDebug("-");
            AppUtil.logDebug("       -------------  Start of Alarm Receiver,  taskId = [" + task.getId() + "]-----------------");
            AppUtil.logDebug("-");
            AppUtil.logDebug("-");
            AppUtil.logDebug("Alarm setter message = [" + message + "]");
            AppUtil.logDebug("Checking task.getSnoozeDateTime() = [" + new Date(task.getSnoozeDateTime().longValue()) + "] today date [" + new Date() + "][" + task.getId() + "][" + task.getName() + "]");
            long timeDiff = new Date().getTime() - task.getReminderDateTime().longValue();
            if (task.getSnoozeDateTime().longValue() == 0 && timeDiff > 120000) {
                AppUtil.logDebug("####################### Canceling bogus alarm for task [" + task.getId() + "][" + task.getName() + "]");
                AppUtil.clearNotification(context, task);
                AlarmSetterHelper.scheduleAlarm(context, task, false);
            } else if (task.getSnoozeDateTime().longValue() - new Date().getTime() > ElapsedTime.MINUTE_IN_MILLIS) {
                AppUtil.logDebug("####################### Canceling duplicate alarm for task [" + task.getId() + "][" + task.getName() + "]");
                AppUtil.logDebug("The canceled task.getSnoozeDateTime() = [" + new Date(task.getSnoozeDateTime().longValue()) + "] today date [" + new Date() + "]");
                AlarmSetterHelper.scheduleAlarm(context, task, false);
                AlarmSetterHelper.scheduleAlarm(context, task, true);
            } else {
                Intent notificationIntent;
                String taskTitle = task.getName();
                Long taskListId = task.getTasksListId();
                NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                Notification notification = new Notification(R.drawable.icon, taskTitle, System.currentTimeMillis());
                notification.flags = 16;
                if (this.config.isEnableReminderSound()) {
                    notification.defaults |= 1;
                }
                String contentTitle = taskTitle;
                CharSequence contentText = "Due " + AppUtil.getDueDateDescription(task.getDueDate(), true) + DateUtil.format(new Date(task.getDueDate().longValue()), "EEE " + AppUtil.formatDateByPreference(new Date(task.getDueDate().longValue())));
                if (task.isCompleted().booleanValue()) {
                    notificationIntent = new Intent(context, TasksListActivity.class);
                } else {
                    notificationIntent = new Intent(context, SnoozeTaskActivity.class);
                }
                notificationIntent.putExtra("taskId", taskId);
                notificationIntent.putExtra("taskListId", taskListId);
                //notification.setLatestEventInfo(context, contentTitle, contentText, PendingIntent.getActivity(context, task.getId().intValue(), notificationIntent, 0));
                mNotificationManager.notify(task.getId().intValue(), notification);
                if (this.config.isEnableTextToSpeech()) {
                    processTextToSpeech(context, task, taskTitle);
                }
                boolean isDontCheckForSnooze = task.getSnoozeDateTime().longValue() == 0;
                if (AppUtil.isAutoSnoozeOff(task)) {
                    task.setSnoozeDateTime(Long.valueOf(0));
                    this.taskBp.saveOrUpdate(task, null);
                }
                processAutoSnooze(context, task);
                if (!this.config.isEnableReminderEmail() || !task.isNotifyByEmail().booleanValue()) {
                    return;
                }
                if (isDontCheckForSnooze || AppUtil.isSnoozeEmailAllowed(task)) {
                    sendEmailNotificationSilently(task);
                }
            }
        }
    }

    private void processTextToSpeech(Context context, Task task, String taskTitle) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager.getCallState() != 2 && telephonyManager.getCallState() != 1) {
            this.utteranceId = task.getId().toString();
            this.mSpokenText = "..Task, due " + AppUtil.getDueDateDescription(task.getDueDate(), true) + ", " + taskTitle;
            this.mTts = new TextToSpeech(this.config.getContext(), this);
        }
    }

    public void onInit(int status) {
        if (status == 0 && this.mTts != null) {
            this.mTts.setOnUtteranceCompletedListener(this);
            this.mTts.speak(this.mSpokenText, 1, AppUtil.getHashAlarm(this.utteranceId));
        }
    }

    public void onUtteranceCompleted(String utteranceId) {
        if (this.mTts != null) {
            this.mTts.shutdown();
        }
    }

    private void processAutoSnooze(Context context, Task task) {
        if (isSnoozeAllowed(task)) {
            int autoSnoozeDurationMinutes = AppUtil.getAutoSnoozeDurationMinutes(task);
            if (autoSnoozeDurationMinutes >= 1) {
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(AppUtil.getTodayDateTimeMilisecond());
                cal.add(Calendar.MINUTE, autoSnoozeDurationMinutes);
                task.setSnoozeDateTime(Long.valueOf(cal.getTimeInMillis()));
                this.taskDao.saveOrUpdateNoUpdateToLastApplicationModifiedDate(task);
                AlarmSetterHelper.scheduleAlarm(context, task, true);
            }
        }
    }

    private boolean isSnoozeAllowed(Task task) {
        if (!task.isReminderEnabled().booleanValue() || ((this.config.getAutoSnoozeDuration().intValue() <= 0 || task.getAutoSnoozeDuration().intValue() < 1) && task.getAutoSnoozeDuration().intValue() <= 1)) {
            return false;
        }
        return true;
    }

    private void sendEmailNotificationSilently(Task task) {
        Email email = this.emailBp.get();
        String dueDateString = "Due " + AppUtil.getDueDateDescription(task.getDueDate(), true) + DateUtil.format(new Date(task.getDueDate().longValue()), "EEEE, MMMM d, yyyy");
        if (email != null) {
            try {
                EmailUtility.sendEmailUsingJavaMail(email.getEmail(), email.getPassword(), getReceipients(task, email), "REMINDER: " + task.getName(), "Task Name: " + task.getName() + "\n\n" + dueDateString + getNote(task.getNote()) + email.getSignature(), null, email.getName());
            } catch (Exception e) {
                Log.e("MailApp", "Could not send email", e);
            }
        }
    }

    private String[] getReceipients(Task task, Email email) {
        String[] recipients = new String[]{email.getEmail()};
        if (!task.isNotifyExtraEmails().booleanValue() || StringUtil.isNullOrEmpty(task.getExtraEmails())) {
            return recipients;
        }
        return new String[]{email.getEmail(), task.getExtraEmails()};
    }

    private String getNote(String note) {
        if (StringUtil.isNullOrEmpty(note)) {
            return "";
        }
        return "\n\n" + note;
    }
}
