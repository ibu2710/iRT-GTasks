package com.irt.tasks.ui.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.Calendar;
import java.util.Date;

public class AlarmSetterHelper {
    private static final int ALARM_REQUEST_CODE = 192847;
    private static final int AUTO_SYNC_REQUEST_CODE = 192837;

    public static void scheduleAutoSync(Context context) {
        ConfigManager config = ConfigManager.getInstance();
        Intent intent = new Intent(context, AutoSyncReceiver.class);
        intent.setAction(Constants.ACTION_AUTO_SYNC);
        intent.putExtra("sync_alarm_message", "Imee Rules!" + new Date());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, AUTO_SYNC_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (config.isAutoSync()) {
            AppUtil.logDebug("******* Scheduling auto-sync...." + new Date());
            alarmManager.setRepeating(0, calculateStartTime().longValue(), getAutoSyncFrequency(), pendingIntent);
            return;
        }
        AppUtil.logDebug("******* Canceled auto-sync...." + new Date());
        alarmManager.cancel(pendingIntent);
        pendingIntent.cancel();
    }

    public static void scheduleAlarm(Context context, Task task, boolean isAddAlarm) {
        performScheduleAlarm(context, task, isAddAlarm, 0, 0, false);
    }

    public static void flushAlarm(Context context, Task task) {
        performScheduleAlarm(context, task, false, 0, 0, true);
    }

    public static void performScheduleAlarm(Context context, Task task, boolean isAddAlarm, long todayMillis, long next5MinutesMillis, boolean isFlushAllAlarms) {
        PendingIntent pendingIntent;
        AlarmManager alarmManager;
        ConfigManager config = ConfigManager.getInstance();
        context = config.getContext();
        Intent intent = new Intent(context, AlarmNotificationReceiver.class);
        intent.setAction(Constants.ACTION_ALARM);
        intent.putExtra("taskId", task.getId());
        intent.putExtra("sync_alarm_message", "Imee Rules 2!" + new Date());
        if (isFlushAllAlarms) {
            pendingIntent = PendingIntent.getBroadcast(context, task.getId().intValue(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        } else {
            pendingIntent = PendingIntent.getBroadcast(context, task.getId().intValue(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        }
        if (isAddAlarm && config.isEnableReminder()) {
            long alarmDateTimeMillis = task.getReminderDateTime().longValue();
            if (task.getSnoozeDateTime().longValue() > 0) {
                alarmDateTimeMillis = task.getSnoozeDateTime().longValue();
                if (todayMillis > 0 && alarmDateTimeMillis <= todayMillis) {
                    alarmDateTimeMillis = next5MinutesMillis;
                    task.setSnoozeDateTime(Long.valueOf(alarmDateTimeMillis));
                    AppUtil.logDebug("Scheduling alarm from AUTO SNOOZE (adjusting snooze)...." + new Date(alarmDateTimeMillis));
                }
                AppUtil.logDebug("Scheduling alarm from AUTO SNOOZE...." + new Date(alarmDateTimeMillis));
            } else {
                AppUtil.logDebug("Scheduling alarm...." + new Date(alarmDateTimeMillis));
            }
            AppUtil.logDebug("Setting alarm ======>" + new Date(alarmDateTimeMillis) + "<======, today date [" + new Date() + "][" + task.getId() + "][" + task.getName() + "]");
            alarmManager.set(0, alarmDateTimeMillis, pendingIntent);
            return;
        }
        AppUtil.logDebug("******* Canceled alarm...." + new Date(task.getReminderDateTime().longValue()));
        alarmManager.cancel(pendingIntent);
        pendingIntent.cancel();
    }

    public static void resetAllAlarms(Context context) {
        ConfigManager.getInstance().setSyncing(false);
        scheduleAutoSync(context);
        AppUtil.scheduleAllAlarms(context);
    }

    private static Long calculateStartTime() {
        ConfigManager instance = ConfigManager.getInstance();
        instance = ConfigManager.getInstance();
        Calendar startDateTime = Calendar.getInstance();
        startDateTime.setTime(AppUtil.getTodayDateOnly());
        startDateTime.set(Calendar.HOUR_OF_DAY, instance.getAutoSyncStartTime().intValue());
        return Long.valueOf(startDateTime.getTimeInMillis());
    }

    private static long getAutoSyncFrequency() {
        return (long) ((ConfigManager.getInstance().getAutoSyncFrequency().intValue() * 60) * 1000);
    }
}
