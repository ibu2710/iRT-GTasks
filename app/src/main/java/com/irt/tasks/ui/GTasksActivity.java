package com.irt.tasks.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.irt.tasks.R;
import com.todoroo.gtasks.GoogleTaskService;
import com.todoroo.gtasks.GoogleTaskService.ConvenientTaskCreator;
import com.todoroo.gtasks.GoogleTaskTask;
import com.todoroo.gtasks.GoogleTaskView;
import com.todoroo.gtasks.Tree;
import com.todoroo.gtasks.actions.Actions;
import com.todoroo.gtasks.actions.ListAction;
import com.todoroo.gtasks.actions.ListActions;
import com.todoroo.gtasks.actions.ListActions.TaskCreator;
import com.todoroo.gtasks.actions.ListActions.TaskModifier;
import com.todoroo.gtasks.actions.TaskCreationListAction;
import java.util.List;

public class GTasksActivity extends Activity {
    private static final Actions actions = new Actions();
    private static final ListActions listActions = new ListActions();
    Button mRetryButton;
    TextView mTokenTextView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        this.mRetryButton = (Button) findViewById(R.id.retryButton);
        this.mRetryButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GTasksActivity.this.onResume();
            }
        });
    }

    protected void onResume() {
        super.onResume();
        try {
            gtasksExample();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void gtasksExample() throws Throwable {
        GoogleTaskService service = new GoogleTaskService("reybt1@gmail.com", "imeemyluv143");
        GoogleTaskView taskView = service.getTaskView();
        System.out.println("*******************************************");
        System.out.println(new Tree(taskView.getActiveTaskList().getTasks()).prettyPrint());
        String listId = taskView.getAllLists()[0].getId();
        service.executeActions(actions.getTasks(listId, false));
        //List googleTasks = getTasksAction.getGoogleTasks();
        List<GoogleTaskTask> tasks = service.getTasks(listId);
        TaskCreationListAction createTaskAction = ((TaskCreator) ((TaskCreator) listActions.createTask("mynewtask3").completed(true)).deleted(false)).done();
        ListAction modificationAction = ((TaskModifier) listActions.modifyTask(((GoogleTaskTask) tasks.get(0)).getId()).deleted(true)).done();
        ListAction listRenameAction = listActions.renameList("mynewlistname");
        service.executeListActions(listId, createTaskAction, modificationAction, listRenameAction);
        String newTaskId = createTaskAction.getNewId();
        String secondTaskId = ((ConvenientTaskCreator) service.createTask(listId, "anothernewtask").completed(false)).go();
    }
}
