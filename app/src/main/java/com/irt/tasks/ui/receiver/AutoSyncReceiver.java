package com.irt.tasks.ui.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import com.irt.tasks.app.businessprocess.SyncBp;
import com.irt.tasks.app.businessprocess.SyncBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.taskslist.OptionsMenuHelper.MonitorSync;
import java.util.Calendar;
import java.util.Date;

public class AutoSyncReceiver extends BroadcastReceiver {

    public static class SyncWithGoogleTasks extends AsyncTask<Void, Void, Boolean> {
        SyncBp syncBp = new SyncBpImpl();

        protected void onPreExecute() {
            super.onPreExecute();
            ConfigManager.getInstance().setCurrentTasksListBeingSynced(Constants.SYNC_TASKS_LIST_NAME_AT_START);
            AppUtil.refreshDisplay();
        }

        protected void onPostExecute(Boolean isSuccessful) {
            super.onPostExecute(isSuccessful);
            ConfigManager.getInstance().setCurrentTasksListBeingSynced(null);
            AppUtil.refreshDisplay();
        }

        protected Boolean doInBackground(Void... params) {
            boolean isSuccessful = false;
            synchronized (this) {
                if (!ConfigManager.getInstance().isSyncing()) {
                    isSuccessful = this.syncBp.syncWithGoogleTasks();
                }
            }
            return Boolean.valueOf(isSuccessful);
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (Constants.ACTION_AUTO_SYNC.equals(intent.getAction())) {
            try {
                ConfigManager config = ConfigManager.getInstance();
                if (config.isSyncing()) {
                    System.out.println("Sync on-going, skipping this auto-sync schedule...");
                    return;
                }
                boolean isRunAutoSync;
                Date currentDateTime = AppUtil.getTodayDateTime();
                Calendar startDateTime = Calendar.getInstance();
                startDateTime.setTime(AppUtil.getTodayDateOnly());
                int startTime = config.getAutoSyncStartTime().intValue();
                startDateTime.set(Calendar.HOUR_OF_DAY, startTime);
                Calendar endDateTime = Calendar.getInstance();
                endDateTime.setTime(AppUtil.getTodayDateOnly());
                int endTime = config.getAutoSyncEndTime().intValue();
                endDateTime.set(Calendar.HOUR_OF_DAY, endTime);
                if (endTime <= startTime) {
                    isRunAutoSync = currentDateTime.getTime() >= startDateTime.getTimeInMillis();
                    if (!isRunAutoSync) {
                        isRunAutoSync = currentDateTime.getTime() <= endDateTime.getTimeInMillis();
                    }
                } else {
                    isRunAutoSync = currentDateTime.getTime() >= startDateTime.getTimeInMillis() && currentDateTime.getTime() <= endDateTime.getTimeInMillis();
                }
                if (isRunAutoSync) {
                    new SyncWithGoogleTasks().execute(new Void[0]);
                    new MonitorSync().execute(new Void[0]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
