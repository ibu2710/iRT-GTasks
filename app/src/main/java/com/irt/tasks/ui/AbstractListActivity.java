package com.irt.tasks.ui;

import android.app.ListActivity;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.sync.DropboxSyncHelper;

public class AbstractListActivity extends ListActivity {
    protected ConfigManager config;

    protected void onResume() {
        super.onResume();
        this.config = ConfigManager.getInstance();
        DropboxSyncHelper.uiRequestSyncWithDelay();
    }

    public void onUserInteraction() {
        super.onUserInteraction();
        DropboxSyncHelper.uiRequestSyncWithDelay();
    }
}
