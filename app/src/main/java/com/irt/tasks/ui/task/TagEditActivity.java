package com.irt.tasks.ui.task;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Tag;
import com.irt.tasks.app.businessprocess.TagBp;
import com.irt.tasks.app.businessprocess.TagBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.sync.DropboxSyncHelper;

public class TagEditActivity extends AbstractActivity {
    Button mCancelButton;
    Button mDeleteButton;
    boolean mIsNewTag = false;
    Button mSaveButton;
    Tag mTag;
    EditText mTagNameEditText;
    TagBp tagBp = new TagBpImpl();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tag_edit);
        this.mTag = (Tag) ArchitectureContext.getObject();
        this.mIsNewTag = this.mTag.getId() == null;
        initViewControls();
        initDeleteButton();
        initCancelButton();
        initSaveButton();
        initViewValues();
        if (this.mIsNewTag) {
            this.mTagNameEditText.requestFocus();
        }
    }

    protected void onResume() {
        super.onResume();
    }

    private void initViewValues() {
        this.mTagNameEditText.setText(this.mTag.getName());
    }

    private void initViewControls() {
        this.mDeleteButton = (Button) findViewById(R.id.delete_button);
        this.mSaveButton = (Button) findViewById(R.id.save_button);
        this.mCancelButton = (Button) findViewById(R.id.cancel_button);
        this.mTagNameEditText = (EditText) findViewById(R.id.tag_name);
        DropboxSyncHelper.addDelayedIdleToEditText(this.mTagNameEditText);
        if (this.mIsNewTag) {
            this.mDeleteButton.setVisibility(View.GONE);
        }
    }

    private void initDeleteButton() {
        this.mDeleteButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new Builder(TagEditActivity.this).setTitle("Delete context").setMessage("Tag will be deleted?").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        TagEditActivity.this.tagBp.deleteTag(TagEditActivity.this.mTag);
                        TagEditActivity.this.tagBp.deleteAllTagTasksByTagId(TagEditActivity.this.mTag.getId());
                        TagEditActivity.this.finish();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
            }
        });
    }

    private void initSaveButton() {
        this.mSaveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                TagEditActivity.this.mTag.setName(TagEditActivity.this.mTagNameEditText.getText().toString());
                switch (TagEditActivity.this.tagBp.isValidTagName(TagEditActivity.this.mTag)) {
                    case NO_ERROR:
                        TagEditActivity.this.mTag.setName(TagEditActivity.this.mTag.getName().trim());
                        TagEditActivity.this.tagBp.saveOrUpdateTag(TagEditActivity.this.mTag);
                        TagEditActivity.this.finish();
                        return;
                    case BLANK:
                        AppUtil.showGenericWarningMessage(TagEditActivity.this, "Name required", "Please provide tag name");
                        return;
                    case DUPLICATE:
                        AppUtil.showGenericWarningMessage(TagEditActivity.this, "Duplicate name", "Please enter another tag name");
                        return;
                    default:
                        return;
                }
            }
        });
    }

    private void initCancelButton() {
        this.mCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                TagEditActivity.this.finish();
            }
        });
    }
}
