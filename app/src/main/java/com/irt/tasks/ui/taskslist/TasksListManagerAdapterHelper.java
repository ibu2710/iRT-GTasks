package com.irt.tasks.ui.taskslist;

import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.config.ConfigManager;

public class TasksListManagerAdapterHelper {

    static class ViewHolder {
        TextView accountName;
        TextView accountVisible;
        TextView autoSync;
        ImageView dragAndDrop;
        TextView name;
        TextView visible;

        ViewHolder() {
        }
    }

    public static View getView(TasksList tasksList, View convertView, LayoutInflater mInflater) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.tasks_list_dragitem, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.TextView01);
            holder.autoSync = (TextView) convertView.findViewById(R.id.auto_sync);
            holder.visible = (TextView) convertView.findViewById(R.id.enabled);
            holder.dragAndDrop = (ImageView) convertView.findViewById(R.id.drag_n_drop_image);
            holder.accountName = (TextView) convertView.findViewById(R.id.account_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        setTasksListName(tasksList, holder);
        setAutoSyncLabel(tasksList, holder);
        setVisibleLabel(tasksList, holder);
        setDragAndDrop(holder);
        setAccountName(tasksList, holder);
        return convertView;
    }

    private static void setTasksListName(TasksList tasksList, ViewHolder holder) {
        holder.name.setText(tasksList.getName());
    }

    private static void setAccountName(TasksList tasksList, ViewHolder holder) {
        SpannableStringBuilder accountName = new SpannableStringBuilder(tasksList.getAccount().getEmail());
        if (tasksList.getAccount().isEnable().booleanValue()) {
            accountName.setSpan(new ForegroundColorSpan(AppUtil.getGreenColorStatus()), 0, accountName.length(), 0);
        } else {
            accountName.setSpan(new ForegroundColorSpan(AppUtil.getRedColorStatus()), 0, accountName.length(), 0);
        }
        holder.accountName.setText(accountName);
    }

    private static void setDragAndDrop(ViewHolder holder) {
        if (ConfigManager.getInstance().getTasksListSort().equals(Integer.valueOf(3))) {
            holder.dragAndDrop.setVisibility(View.VISIBLE);
        } else {
            holder.dragAndDrop.setVisibility(View.GONE);
        }
    }

    private static void setVisibleLabel(TasksList tasksList, ViewHolder holder) {
        SpannableStringBuilder enableLabel = new SpannableStringBuilder();
        if (tasksList.isVisible().booleanValue()) {
            enableLabel.append("Visible");
            enableLabel.setSpan(new ForegroundColorSpan(getGreenOrYellow(tasksList.getAccount().isEnable().booleanValue())), 0, enableLabel.length(), 0);
        } else {
            enableLabel.append("Invisible");
            enableLabel.setSpan(new ForegroundColorSpan(AppUtil.getRedColorStatus()), 0, enableLabel.length(), 0);
        }
        holder.visible.setText(enableLabel);
    }

    private static void setAutoSyncLabel(TasksList tasksList, ViewHolder holder) {
        if (DatabaseHelper.OFFLINE_ACCOUNT_ID.equals(tasksList.getAccountId())) {
            holder.autoSync.setText("");
            return;
        }
        SpannableStringBuilder autoSyncLabel = new SpannableStringBuilder("Auto-sync ");
        if (tasksList.isAutoSync().booleanValue()) {
            autoSyncLabel.append("ON");
            autoSyncLabel.setSpan(new ForegroundColorSpan(getGreenOrYellow(tasksList.getAccount().isAutoSync().booleanValue())), 0, autoSyncLabel.length(), 0);
        } else {
            autoSyncLabel.append("OFF");
            autoSyncLabel.setSpan(new ForegroundColorSpan(AppUtil.getRedColorStatus()), 0, autoSyncLabel.length(), 0);
        }
        holder.autoSync.setText(autoSyncLabel);
    }

    private static int getGreenOrYellow(boolean isAccountON) {
        if (isAccountON) {
            return AppUtil.getGreenColorStatus();
        }
        return -256;
    }
}
