package com.irt.tasks.ui.account;

import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;

public class AccountAdapterHelper {

    static class ViewHolder {
        TextView autoSync;
        TextView enabled;
        TextView text;

        ViewHolder() {
        }
    }

    public static View getView(Account account, View convertView, LayoutInflater mInflater) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.account_dragitem, null);
            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.TextView01);
            holder.autoSync = (TextView) convertView.findViewById(R.id.auto_sync);
            holder.enabled = (TextView) convertView.findViewById(R.id.enabled);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        setEmailAddress(account, holder);
        setAutoSyncLabel(account, holder);
        setEnableLabel(account, holder);
        return convertView;
    }

    private static void setEnableLabel(Account account, ViewHolder holder) {
        SpannableStringBuilder enableLabel = new SpannableStringBuilder();
        if (account.isEnable().booleanValue()) {
            enableLabel.append("Visible");
            enableLabel.setSpan(new ForegroundColorSpan(AppUtil.getGreenColorStatus()), 0, enableLabel.length(), 0);
        } else {
            enableLabel.append("Invisible");
            enableLabel.setSpan(new ForegroundColorSpan(AppUtil.getRedColorStatus()), 0, enableLabel.length(), 0);
        }
        holder.enabled.setText(enableLabel);
    }

    private static void setAutoSyncLabel(Account account, ViewHolder holder) {
        if (DatabaseHelper.OFFLINE_ACCOUNT_ID.equals(account.getId())) {
            holder.autoSync.setText("");
        } else if (account.isNewGoogleId().booleanValue()) {
            SpannableStringBuilder autoSyncLabel = new SpannableStringBuilder("Auto-sync ");
            if (account.isAutoSync().booleanValue()) {
                autoSyncLabel.append("ON");
                autoSyncLabel.setSpan(new ForegroundColorSpan(AppUtil.getGreenColorStatus()), 0, autoSyncLabel.length(), 0);
            } else {
                autoSyncLabel.append("OFF");
                autoSyncLabel.setSpan(new ForegroundColorSpan(AppUtil.getRedColorStatus()), 0, autoSyncLabel.length(), 0);
            }
            holder.autoSync.setText(autoSyncLabel);
        } else {
            SpannableStringBuilder autoSyncLabel = new SpannableStringBuilder("Old Google Sync ID");
            autoSyncLabel.setSpan(new ForegroundColorSpan(AppUtil.getRedColorStatus()), 0, autoSyncLabel.length(), 0);
            holder.autoSync.setText(autoSyncLabel);
        }
    }

    private static void setEmailAddress(Account account, ViewHolder holder) {
        holder.text.setText(account.getEmail());
    }
}
