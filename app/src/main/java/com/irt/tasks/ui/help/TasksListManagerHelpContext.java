package com.irt.tasks.ui.help;

public class TasksListManagerHelpContext {
    public static void setTasksListManagerHelpContext(HelpContext helpContext) {
        helpContext.title = "Tasks List Manager";
        helpContext.content.append("<b>Description:</b>");
        helpContext.content.append("<br>This is an extension to the Tasks List view where you can manage Tasks Lists (Outlines) sort order and see invisible lists. Hidden accounts are not shown, unless 'Show invisible accounts/list' is checked. ");
        helpContext.content.append("<br><br><b>Navigation:</b>");
        helpContext.content.append(HelpUtility.UL_STYLE);
        helpContext.content.append("<li><b>Tap</b> on any list to edit.");
        helpContext.content.append("</ul>");
        helpContext.content.append("<b>Notes:</b>");
        helpContext.content.append("<br>Auto-sync and Visible Color:");
        helpContext.content.append(HelpUtility.UL_STYLE);
        helpContext.content.append("<li>Green means it will take effect");
        helpContext.content.append("<li>Yellow means it will only take effect if Account is enabled/visible");
        helpContext.content.append("<li>Red means it is disabled");
        helpContext.content.append("</ul>");
    }
}
