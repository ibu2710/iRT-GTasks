package com.irt.tasks.ui.filter;

import com.google.gson.Gson;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.ContextBase;
import com.irt.tasks.app.businessprocess.ContextBp;
import com.irt.tasks.app.businessprocess.ContextBpImpl;
import com.irt.tasks.app.businessprocess.ProjectBp;
import com.irt.tasks.app.businessprocess.ProjectBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.ArrayList;
import java.util.List;

public class TasksFilterManager {
    private static TasksFilterManager instance;
    private static TasksFilter tasksFilter;
    ContextBp contextBp = new ContextBpImpl();
    ProjectBp projectBp = new ProjectBpImpl();

    public static TasksFilterManager getInstance() {
        if (instance == null) {
            instance = new TasksFilterManager();
        }
        tasksFilter = (TasksFilter) new Gson().fromJson(ConfigManager.getInstance().getJsonTasksFilterObject(), TasksFilter.class);
        if (tasksFilter == null) {
            tasksFilter = new TasksFilter();
        }
        return instance;
    }

    public boolean isAnyProjectSelected() {
        return tasksFilter.getProjectId().longValue() == 0 && !tasksFilter.isProjectNone();
    }

    public boolean isAnyContextSelected() {
        return tasksFilter.getContextIds() == null && !tasksFilter.isContextNone();
    }

    public boolean isAnyTagSelected() {
        return tasksFilter.getTagIds() == null && !tasksFilter.isTagNone();
    }

    public TasksFilter getTaskFilter() {
        return tasksFilter;
    }

    public void setAnyContext() {
        tasksFilter.setContextNone(false);
        tasksFilter.setContextAnd(false);
        tasksFilter.setContextIds(null);
    }

    public void setAnyProject() {
        tasksFilter.setProjectId(Long.valueOf(0));
        tasksFilter.setProjectNone(false);
    }

    public void setAnyTag() {
        tasksFilter.setTagNone(false);
        tasksFilter.setTagAnd(false);
        tasksFilter.setTagIds(null);
    }

    public boolean isAnyStatus() {
        return tasksFilter.getStatus().intValue() == 0;
    }

    public void saveFilter() {
        ConfigManager.getInstance().setJsonTasksFilterObject(new Gson().toJson(tasksFilter));
    }

    public String getContextIdsCsv() {
        return formatToCsv(tasksFilter.getContextIds());
    }

    public String getTagIdsCsv() {
        return formatToCsv(tasksFilter.getTagIds());
    }

    public void removeContextId(Long contextId) {
        List<Long> contextIds = tasksFilter.getContextIds();
        if (contextIds != null && contextIds.size() > 0) {
            List<Long> newContextIds = new ArrayList();
            for (Long contextId2 : contextIds) {
                if (!contextId2.equals(contextId)) {
                    newContextIds.add(contextId2);
                }
            }
            tasksFilter.setContextIds(newContextIds);
            saveFilter();
        }
    }

    public void removeTagId(Long tagId) {
        List<Long> tagIds = tasksFilter.getTagIds();
        if (tagIds != null && tagIds.size() > 0) {
            List<Long> newTagIds = new ArrayList();
            for (Long tagId2 : tagIds) {
                if (!tagId2.equals(tagId)) {
                    newTagIds.add(tagId2);
                }
            }
            tasksFilter.setTagIds(newTagIds);
            saveFilter();
        }
    }

    public void resetProjectId(Long projectId) {
        if (tasksFilter.getProjectId().equals(projectId)) {
            tasksFilter.setProjectId(Long.valueOf(0));
            saveFilter();
        }
    }

    public void resetFilter() {
        setAnyContext();
        setAnyProject();
        setAnyTag();
        tasksFilter.setStatus(Integer.valueOf(6));
        saveFilter();
    }

    public String getFilterDescription() {
        ConfigManager config = ConfigManager.getInstance();
        StringBuffer filterDescription = new StringBuffer();
        if (tasksFilter.getProjectId() != null) {
            filterDescription.append(this.projectBp.get(tasksFilter.getProjectId()).getTitle());
        }
        if (tasksFilter.getStatus().intValue() != 0) {
            if (filterDescription.length() > 0) {
                filterDescription.append("; ");
            }
            filterDescription.append(AppUtil.getStringFromArrayValueBasedOnPosition(config.getContext(), R.array.task_status, tasksFilter.getStatus().intValue()));
        }
        if (tasksFilter.getContextIds() != null && tasksFilter.getContextIds().size() > 0) {
            if (filterDescription.length() > 0) {
                filterDescription.append("; ");
            }
            filterDescription.append(getContextDescription());
        }
        return filterDescription.toString();
    }

    private String formatToCsv(List<Long> ids) {
        if (ids == null || ids.size() < 1) {
            return "-1";
        }
        StringBuffer idsInCsv = new StringBuffer();
        for (int i = 0; i < ids.size(); i++) {
            Long id = (Long) ids.get(i);
            if (i > 0) {
                idsInCsv.append(Globals.COMMA + id);
            } else {
                idsInCsv.append(id);
            }
        }
        return idsInCsv.toString();
    }

    private String getContextDescription() {
        List<ContextBase> contextBases = this.contextBp.getAllContexts();
        StringBuffer contextString = new StringBuffer();
        boolean firstRecord = true;
        for (Long contextId : tasksFilter.getContextIds()) {
            if (!firstRecord) {
                contextString.append(", ");
            }
            appendContextName(contextString, contextId, contextBases);
            firstRecord = false;
        }
        return contextString.toString();
    }

    private void appendContextName(StringBuffer contextString, Long contextId, List<ContextBase> contextBases) {
        for (ContextBase contextBase : contextBases) {
            if (contextBase.getId().equals(contextId)) {
                contextString.append(contextBase.getName());
            }
        }
    }
}
