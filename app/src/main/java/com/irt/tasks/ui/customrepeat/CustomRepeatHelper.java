package com.irt.tasks.ui.customrepeat;

import android.text.format.DateFormat;
import android.text.format.Time;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static java.util.Calendar.DAY_OF_MONTH;

public class CustomRepeatHelper {
    public static final String BYDAY = "BYDAY";
    public static final String BYMONTHDAY = "BYMONTHDAY";
    public static final String DAILY = "DAILY";
    public static final String FR = "FR";
    public static final String FREQ = "FREQ";
    public static final String INTERVAL = "INTERVAL";
    private static final int INVALID_DAY_0F_WEEK = -1;
    public static final int LAST_DAY_OF_MONTH_INDICATOR = -1;
    public static final String MO = "MO";
    public static final String MONTHLY = "MONTHLY";
    public static final String SA = "SA";
    public static final String SU = "SU";
    public static final String TH = "TH";
    public static final String TU = "TU";
    public static final String UNTIL = "UNTIL";
    public static final String WE = "WE";
    public static final String WEEKLY = "WEEKLY";
    public static final String WKST = "WKST";
    public static final String YEARLY = "YEARLY";
    private static Map<String, String> mCustomRepeatParameters = new HashMap();

    public static Map<String, String> getCustomRepeatParameters() {
        return mCustomRepeatParameters;
    }

    public static long getNextDueDate(Task task) {
        long nextDueDateMillis = performGetNextDueDate(task);
        long untilDateMillis = getUntil();
        if (untilDateMillis == 0 || nextDueDateMillis <= untilDateMillis) {
            return nextDueDateMillis;
        }
        return task.getDueDate().longValue();
    }

    private static long performGetNextDueDate(Task task) {
        if (!task.isRecurringTask()) {
            return task.getDueDate().longValue();
        }
        Calendar initialDueDate = Calendar.getInstance();
        if (task.isReminderRepeatFromCompletionDate().booleanValue()) {
            initialDueDate.setTime(AppUtil.getTodayDateOnly());
        } else {
            long todayDateMillis = AppUtil.getTodayDateOnly().getTime();
            if (!task.isFloat().booleanValue() || task.getDueDate().longValue() > todayDateMillis) {
                initialDueDate.setTimeInMillis(task.getDueDate().longValue());
            } else {
                initialDueDate.setTimeInMillis(todayDateMillis);
            }
        }
        initCustomRepeatParameters(task.getReminderRepeatRule());
        int interval = getInterval() == 0 ? 1 : getInterval();
        if (DAILY.equals(getFrequency())) {
            initialDueDate.add(Calendar.DAY_OF_YEAR, interval);
            return initialDueDate.getTimeInMillis();
        } else if (WEEKLY.equals(getFrequency())) {
            if (task.isReminderRepeatFromCompletionDate().booleanValue()) {
                initialDueDate.add(Calendar.WEEK_OF_YEAR, interval);
                return initialDueDate.getTimeInMillis();
            }
            int initialDueDateDayOfWeek = initialDueDate.get(Calendar.DAY_OF_WEEK);
            if (isDayWithinTheLast6DaysOfWeek(initialDueDateDayOfWeek)) {
                int nextDayOfWeek = getNextDayOfWeek(initialDueDateDayOfWeek + 1);
                if (-1 != nextDayOfWeek) {
                    int daysDiffForNextDayOfWeek = Math.abs(nextDayOfWeek - initialDueDateDayOfWeek);
                    if (isStartDayModay() && 1 == nextDayOfWeek) {
                        daysDiffForNextDayOfWeek = 1;
                    }
                    initialDueDate.add(Calendar.DAY_OF_YEAR, daysDiffForNextDayOfWeek);
                    return initialDueDate.getTimeInMillis();
                }
            }
            initialDueDate.add(Calendar.DAY_OF_YEAR, -initialDueDateDayOfWeek);
            initialDueDate.add(Calendar.WEEK_OF_YEAR, interval);
            initialDueDate.add(Calendar.DAY_OF_YEAR, getNextDayOfWeek(getFirstDayOfWeek()));
            return initialDueDate.getTimeInMillis();
        } else if (MONTHLY.equals(getFrequency())) {
            if (task.isReminderRepeatFromCompletionDate().booleanValue()) {
                initialDueDate.add(Calendar.MONTH, interval);
                return initialDueDate.getTimeInMillis();
            } else if (isMonthlyByDay()) {
                String byDay = getWeeklyByDayDays();
                int dayOfWeekShortPosition = byDay.length() - 2;
                String dayOfWeekShort = byDay.substring(dayOfWeekShortPosition);
                int dayOfWeekInMonth = new Integer(byDay.substring(0, dayOfWeekShortPosition)).intValue();
                initialDueDate.add(Calendar.MONTH, interval);
                getNthDayOfMonth(initialDueDate, dayOfWeekInMonth, getDayFromShortDay(dayOfWeekShort));
                return initialDueDate.getTimeInMillis();
            } else {
                String monthDay = (String) mCustomRepeatParameters.get(BYMONTHDAY);
                initialDueDate.add(Calendar.MONTH, interval);
                int targetMonth = initialDueDate.get(Calendar.MONTH);
                initialDueDate.set(DAY_OF_MONTH, new Integer(monthDay).intValue());
                if (targetMonth != initialDueDate.get(Calendar.MONTH)) {
                    initialDueDate.set(DAY_OF_MONTH, 1);
                    initialDueDate.add(Calendar.DAY_OF_YEAR, -1);
                }
                return initialDueDate.getTimeInMillis();
            }
        } else if (!YEARLY.equals(getFrequency())) {
            return task.getDueDate().longValue();
        } else {
            initialDueDate.add(Calendar.YEAR, interval);
            return initialDueDate.getTimeInMillis();
        }
    }

    private static void getNthDayOfMonth(Calendar cal, int targetPosition, int day) {
        int positionCounter = 0;
        int i;
        if (targetPosition > 0) {
            cal.set(DAY_OF_MONTH, 1);
            for (i = 1; i <= 31; i++) {
                if (day == cal.get(Calendar.DAY_OF_WEEK)) {
                    positionCounter++;
                    if (targetPosition == positionCounter) {
                        return;
                    }
                }
                cal.add(Calendar.DAY_OF_MONTH, 1);
            }
            return;
        }
        cal.add(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        i = 1;
        while (i <= 31) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
            if (day != cal.get(Calendar.DAY_OF_WEEK)) {
                i++;
            } else {
                return;
            }
        }
    }

    private static int getFirstDayOfWeek() {
        if (isStartDayModay()) {
            return 2;
        }
        return 1;
    }

    private static boolean isDayWithinTheLast6DaysOfWeek(int initialDueDateDayOfWeek) {
        if (isStartDayModay()) {
            if (initialDueDateDayOfWeek <= 7) {
                return true;
            }
            return false;
        } else if (initialDueDateDayOfWeek >= 7) {
            return false;
        } else {
            return true;
        }
    }

    private static int getDayFromShortDay(String dayOfWeekShort) {
        if (SU.equals(dayOfWeekShort)) {
            return 1;
        }
        if (MO.equals(dayOfWeekShort)) {
            return 2;
        }
        if (TU.equals(dayOfWeekShort)) {
            return 3;
        }
        if (WE.equals(dayOfWeekShort)) {
            return 4;
        }
        if (TH.equals(dayOfWeekShort)) {
            return 5;
        }
        if (FR.equals(dayOfWeekShort)) {
            return 6;
        }
        if (SA.equals(dayOfWeekShort)) {
            return 7;
        }
        return -1;
    }

    private static int getNextDayOfWeek(int baseDayOfWeek) {
        int i;
        if (!isStartDayModay()) {
            if (baseDayOfWeek > 7) {
                baseDayOfWeek = 1;
            }
            for (i = baseDayOfWeek; i <= 7; i++) {
                if (-1 != getDayIfPresent(i)) {
                    return i;
                }
            }
        } else if (baseDayOfWeek > 7 && -1 != getDayIfPresent(1)) {
            return 1;
        } else {
            for (i = baseDayOfWeek; i <= 7; i++) {
                if (-1 != getDayIfPresent(i)) {
                    return i;
                }
            }
        }
        return -1;
    }

    private static int getDayIfPresent(int dayInWeek) {
        String daysOfWeekStr = getWeeklyByDayDays();
        switch (dayInWeek) {
            case 1:
                return containsDay(daysOfWeekStr, SU, 1);
            case 2:
                return containsDay(daysOfWeekStr, MO, 2);
            case 3:
                return containsDay(daysOfWeekStr, TU, 3);
            case 4:
                return containsDay(daysOfWeekStr, WE, 4);
            case 5:
                return containsDay(daysOfWeekStr, TH, 5);
            case 6:
                return containsDay(daysOfWeekStr, FR, 6);
            case 7:
                return containsDay(daysOfWeekStr, SA, 7);
            default:
                return -1;
        }
    }

    private static int containsDay(String daysOfWeekStr, String dayOfWeekXX, int dayOfWeek) {
        return daysOfWeekStr.contains(dayOfWeekXX) ? dayOfWeek : -1;
    }

    public static void appendFrequency(String frequency, StringBuffer customRepeatKeyValuePairs) {
        customRepeatKeyValuePairs.append("FREQ=" + frequency);
    }

    public static void appendWeekStart(StringBuffer customRepeatKeyValuePairs) {
        if (ConfigManager.getInstance().getStartDay().intValue() == 0) {
            customRepeatKeyValuePairs.append(";WKST=SU");
        } else {
            customRepeatKeyValuePairs.append(";WKST=MO");
        }
    }

    private static boolean isStartDayModay() {
        if (ConfigManager.getInstance().getStartDay().intValue() == 1) {
            return true;
        }
        return false;
    }

    public static void appendWeeklyByDay(String day, StringBuffer customRepeatKeyValuePairs) {
        customRepeatKeyValuePairs.append(";BYDAY=" + day);
    }

    public static void appendAnotherWeeklyByDay(String day, StringBuffer customRepeatKeyValuePairs) {
        customRepeatKeyValuePairs.append(Globals.COMMA + day);
    }

    public static void appendUntil(Date date, StringBuffer customRepeatKeyValuePairs, String timeZone) {
        if (StringUtil.isNullOrEmpty(timeZone)) {
            timeZone = "UTC";
        }
        if (date != null) {
            Time time = new Time(timeZone);
            time.set(date.getTime());
            customRepeatKeyValuePairs.append(";UNTIL=" + time.format2445().substring(0, 8));
        }
    }

    public static String calculateDuration(Task task) {
        return "P" + (86400 / 86400) + "D";
    }

    public static void appendInterval(int interval, StringBuffer customRepeatKeyValuePairs) {
        if (interval > 1) {
            customRepeatKeyValuePairs.append(";INTERVAL=" + interval);
        }
    }

    public static void appendMonthlyByDay(String monthlyByDay, StringBuffer customRepeatKeyValuePairs) {
        customRepeatKeyValuePairs.append(";BYDAY=" + monthlyByDay);
    }

    public static void appendByMonthDay(int day, StringBuffer customRepeatKeyValuePairs) {
        customRepeatKeyValuePairs.append(";BYMONTHDAY=" + day);
    }

    public static String getVerboseDescription(String customRepeatKeyValuePairs, Date baseDate, boolean isRepeatFromCompletionDate) {
        if (StringUtil.isNullOrEmpty(customRepeatKeyValuePairs)) {
            return "One-time event";
        }
        mCustomRepeatParameters.clear();
        initCustomRepeatParameters(customRepeatKeyValuePairs);
        StringBuffer description = new StringBuffer();
        String frequency = getFrequency();
        if (isRepeatFromCompletionDate) {
            getVerboseDescriptionForAfterCompleted(baseDate, description, frequency);
        } else {
            getVerboseDescriptionForFixedSchedule(baseDate, description, frequency);
        }
        if (mCustomRepeatParameters.containsKey(UNTIL)) {
            long millis = getUntil();
            description.append(" until ");
            description.append(DateFormat.format("MMMM dd, yyyy", new Date(millis)));
        }
        return description.toString();
    }

    private static void initCustomRepeatParameters(String customRepeatKeyValuePairs) {
        String[] customRepeatKeyValues = customRepeatKeyValuePairs.split(";");
        for (String split : customRepeatKeyValues) {
            String[] customRepeatKeyValue = split.split("=");
            mCustomRepeatParameters.put(customRepeatKeyValue[0], customRepeatKeyValue[1]);
        }
    }

    private static String getAfterCompletedWording(String basicUnit) {
        int interval = getInterval();
        if (interval <= 0) {
            interval = 1;
        }
        StringBuilder append = new StringBuilder().append(interval).append(" ");
        if (interval > 1) {
            basicUnit = basicUnit + "s";
        }
        return append.append(basicUnit).append(" after prior task is completed").toString();
    }

    private static void getVerboseDescriptionForAfterCompleted(Date baseDate, StringBuffer description, String frequency) {
        if (DAILY.equals(frequency)) {
            description.append(getAfterCompletedWording("day"));
        } else if (WEEKLY.equals(frequency)) {
            description.append(getAfterCompletedWording("week"));
        } else if (MONTHLY.equals(frequency)) {
            description.append(getAfterCompletedWording("month"));
        } else if (YEARLY.equals(frequency)) {
            description.append(getAfterCompletedWording("year"));
        }
    }

    private static void getVerboseDescriptionForFixedSchedule(Date baseDate, StringBuffer description, String frequency) {
        if (DAILY.equals(frequency)) {
            description.append("Every");
            description.append(translateToStNdRdTh(getInterval(), true));
            description.append("day");
        } else if (WEEKLY.equals(frequency)) {
            description.append("Every");
            description.append(translateToStNdRdTh(getInterval(), true));
            description.append("week on ");
            description.append(translateWeekDays());
        } else if (MONTHLY.equals(frequency)) {
            if (isMonthlyByDay()) {
                String byDay = getWeeklyByDayDays();
                int dayOfWeekShortPosition = byDay.length() - 2;
                String dayOfWeekShort = byDay.substring(dayOfWeekShortPosition);
                int dayOfWeekInMonth = new Integer(byDay.substring(0, dayOfWeekShortPosition)).intValue();
                description.append("The");
                if (dayOfWeekInMonth == -1) {
                    description.append(" last ");
                } else {
                    description.append(translateToStNdRdTh(dayOfWeekInMonth, false));
                }
                description.append(getLongDay(dayOfWeekShort));
                description.append(" of every");
                description.append(translateToStNdRdTh(getInterval(), true));
                description.append("month");
                return;
            }
            String monthDay = (String) mCustomRepeatParameters.get(BYMONTHDAY);
            description.append("The");
            description.append(translateToStNdRdTh(new Integer(monthDay).intValue(), false));
            description.append("of every");
            description.append(translateToStNdRdTh(getInterval(), true));
            description.append("month");
        } else if (YEARLY.equals(frequency)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(baseDate);
            description.append(DateFormat.format("MMMM", baseDate));
            description.append(translateToStNdRdTh(cal.get(Calendar.DAY_OF_MONTH), false));
            description.append("every");
            description.append(translateToStNdRdTh(getInterval(), true));
            description.append("year");
        }
    }

    public static boolean isMonthlyByDay() {
        return mCustomRepeatParameters.containsKey(BYDAY);
    }

    public static long getUntil() {
        if (!mCustomRepeatParameters.containsKey(UNTIL)) {
            return 0;
        }
        String untilDate = (String) mCustomRepeatParameters.get(UNTIL);
        Time time = new Time();
        time.parse(untilDate);
        return time.normalize(false);
    }

    public static long getUntil(String customRepeatKeyValuePairs) {
        initCustomRepeatParameters(customRepeatKeyValuePairs);
        if (!mCustomRepeatParameters.containsKey(UNTIL)) {
            return 0;
        }
        String untilDate = (String) mCustomRepeatParameters.get(UNTIL);
        Time time = new Time();
        time.parse(untilDate);
        return time.normalize(false);
    }

    public static String getFrequency() {
        return (String) mCustomRepeatParameters.get(FREQ);
    }

    private static String getLongDay(String dayOfWeekShort) {
        if (SU.equals(dayOfWeekShort)) {
            return getDayOfWeek(adjustWeekDayToZero(1), true);
        }
        if (MO.equals(dayOfWeekShort)) {
            return getDayOfWeek(adjustWeekDayToZero(2), true);
        }
        if (TU.equals(dayOfWeekShort)) {
            return getDayOfWeek(adjustWeekDayToZero(3), true);
        }
        if (WE.equals(dayOfWeekShort)) {
            return getDayOfWeek(adjustWeekDayToZero(4), true);
        }
        if (TH.equals(dayOfWeekShort)) {
            return getDayOfWeek(adjustWeekDayToZero(5), true);
        }
        if (FR.equals(dayOfWeekShort)) {
            return getDayOfWeek(adjustWeekDayToZero(6), true);
        }
        if (SA.equals(dayOfWeekShort)) {
            return getDayOfWeek(adjustWeekDayToZero(7), true);
        }
        return null;
    }

    public static int getInterval() {
        if (mCustomRepeatParameters.containsKey(INTERVAL)) {
            return new Integer((String) mCustomRepeatParameters.get(INTERVAL)).intValue();
        }
        return 0;
    }

    private static String translateWeekDays() {
        StringBuffer weekDays = new StringBuffer();
        String daysOfWeekStr = getWeeklyByDayDays();
        int numberOfCheckedWeekDays = daysOfWeekStr.split(Globals.COMMA).length;
        int checkCounter = 0;
        if (daysOfWeekStr.contains(SU)) {
            checkCounter = 0 + 1;
            weekDays.append(formatDay(adjustWeekDayToZero(1), numberOfCheckedWeekDays));
        }
        if (daysOfWeekStr.contains(MO)) {
            checkCounter = formatComma(weekDays, checkCounter, numberOfCheckedWeekDays);
            weekDays.append(formatDay(adjustWeekDayToZero(2), numberOfCheckedWeekDays));
        }
        if (daysOfWeekStr.contains(TU)) {
            checkCounter = formatComma(weekDays, checkCounter, numberOfCheckedWeekDays);
            weekDays.append(formatDay(adjustWeekDayToZero(3), numberOfCheckedWeekDays));
        }
        if (daysOfWeekStr.contains(WE)) {
            checkCounter = formatComma(weekDays, checkCounter, numberOfCheckedWeekDays);
            weekDays.append(formatDay(adjustWeekDayToZero(4), numberOfCheckedWeekDays));
        }
        if (daysOfWeekStr.contains(TH)) {
            checkCounter = formatComma(weekDays, checkCounter, numberOfCheckedWeekDays);
            weekDays.append(formatDay(adjustWeekDayToZero(5), numberOfCheckedWeekDays));
        }
        if (daysOfWeekStr.contains(FR)) {
            checkCounter = formatComma(weekDays, checkCounter, numberOfCheckedWeekDays);
            weekDays.append(formatDay(adjustWeekDayToZero(6), numberOfCheckedWeekDays));
        }
        if (daysOfWeekStr.contains(SA)) {
            checkCounter = formatComma(weekDays, checkCounter, numberOfCheckedWeekDays);
            weekDays.append(formatDay(adjustWeekDayToZero(7), numberOfCheckedWeekDays));
        }
        return weekDays.toString();
    }

    public static String getWeeklyByDayDays() {
        return (String) mCustomRepeatParameters.get(BYDAY);
    }

    private static String translateToStNdRdTh(int interval, boolean isUseBlankAndOther) {
        if (interval == 1 || interval == 0) {
            if (isUseBlankAndOther) {
                return " ";
            }
            return " 1st ";
        } else if (interval == 2) {
            if (isUseBlankAndOther) {
                return " other ";
            }
            return " 2nd ";
        } else if (interval == 3) {
            return " 3rd ";
        } else {
            if (interval == 21) {
                return " 21st ";
            }
            if (interval == 22) {
                return " 22nd ";
            }
            if (interval == 23) {
                return " 23rd ";
            }
            if (interval == 31) {
                return " 31st ";
            }
            return " " + interval + "th ";
        }
    }

    private static String getDayOfWeek(int dayOfWeek, boolean isLongName) {
        Calendar cal = Calendar.getInstance();
        cal.set(2010, 0, 10);
        cal.add(Calendar.DAY_OF_YEAR, dayOfWeek);
        if (isLongName) {
            return DateFormat.format("EEEE", cal) + "";
        }
        return DateFormat.format("EEE", cal) + "";
    }

    private static String formatDay(int dayOfWeek, int numberOfCheckedWeekDays) {
        if (numberOfCheckedWeekDays > 1) {
            return getDayOfWeek(dayOfWeek, false);
        }
        return getDayOfWeek(dayOfWeek, true);
    }

    private static int formatComma(StringBuffer weekDays, int checkCounter, int numberOfCheckedWeekDays) {
        checkCounter++;
        if (checkCounter > 1) {
            if (numberOfCheckedWeekDays == 2) {
                weekDays.append(" and ");
            } else if (checkCounter == numberOfCheckedWeekDays) {
                weekDays.append(", and ");
            } else {
                weekDays.append(", ");
            }
        }
        return checkCounter;
    }

    private static int adjustWeekDayToZero(int weekDay) {
        return weekDay - 1;
    }
}
