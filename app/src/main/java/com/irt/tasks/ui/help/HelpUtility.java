package com.irt.tasks.ui.help;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import com.irt.tasks.R;
import com.irt.tasks.app.common.Constants;

public class HelpUtility {
    public static final String UL_STYLE = "<ul style=\"margin-left: .9em; padding-left: .9em;\">";

    public enum HelpDialog {
        TASKS_LIST_HOME,
        TASKS_LIST_MANAGER,
        TASKS_TREE,
        TASKS_VIEW
    }

    public static void showHelp(Context context, HelpDialog helpDialog) {
        HelpContext helpContext = new HelpContext();
        switch (helpDialog) {
            case TASKS_LIST_HOME:
                TasksListHomeHelpContext.setTasksListHomeHelpContext(helpContext);
                break;
            case TASKS_LIST_MANAGER:
                TasksListManagerHelpContext.setTasksListManagerHelpContext(helpContext);
                break;
            case TASKS_TREE:
                TasksTreeHelpContext.setTasksTreeHelpContext(helpContext);
                break;
            case TASKS_VIEW:
                TasksViewHelpContext.setTasksViewHelpContext(helpContext);
                break;
        }
        showReleaseNotes(context, helpContext);
    }

    private static void showReleaseNotes(Context context, HelpContext helpContext) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.web_style_dialog, null);
        WebView webView = (WebView) convertView.findViewById(R.id.wv1);
        Builder alertDialogBuilder = new Builder(context);
        helpContext.content.append("<b>More info:</b>");
        helpContext.content.append(UL_STYLE);
        helpContext.content.append("<li> <a href='http://www.irtgtasksoutliner.com/userguide'>User Guide</a>");
        helpContext.content.append("<li> <a href='http://www.irtgtasksoutliner.com/faq'>Frequently Asked Questions (FAQ)</a>");
        helpContext.content.append("</ul>");
        webView.loadData(helpContext.content.toString(), "text/html", Constants.encoding);
        alertDialogBuilder.setTitle(helpContext.title);
        alertDialogBuilder.setView(convertView);
        alertDialogBuilder.setPositiveButton("OK", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialogBuilder.show();
    }
}
