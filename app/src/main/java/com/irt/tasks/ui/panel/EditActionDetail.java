package com.irt.tasks.ui.panel;

import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.common.Globals;
import java.util.ArrayList;
import java.util.List;

public class EditActionDetail {
    private Task destinationTask;
    private EditActions firstEditAction;
    private boolean isDestinationExpanded;
    private EditActions secondEditAction;
    private List<Task> selectedTasks = new ArrayList();

    public enum EditActions {
        COPY,
        CUT,
        DELETE,
        PASTE,
        SELECT,
        SELECT_TASK,
        SELECT_DESTINATION
    }

    public void addSelectedTask(Task task) {
        this.selectedTasks.add(task);
    }

    public void removeSelectedTask(Task task) {
        if (task != null) {
            for (Task task2 : this.selectedTasks) {
                if (task2.getId().equals(task.getId())) {
                    this.selectedTasks.remove(task2);
                    return;
                }
            }
        }
    }

    public List<Task> getSelectedTasks() {
        List<Task> tasks = new ArrayList();
        tasks.addAll(this.selectedTasks);
        return tasks;
    }

    public boolean isDestinationExpanded() {
        return this.isDestinationExpanded;
    }

    public Task getDestinationTask() {
        return this.destinationTask;
    }

    public void setDestinationTask(Task destinationTask, boolean isExpanded) {
        this.destinationTask = destinationTask;
        this.isDestinationExpanded = isExpanded;
    }

    boolean isActionReadyForProcessing() {
        boolean isActionReadyForProcessing = false;
        switch (this.firstEditAction) {
            case COPY:
            case CUT:
                if (this.selectedTasks.size() > 0) {
                    isActionReadyForProcessing = true;
                } else {
                    isActionReadyForProcessing = false;
                }
                if (isActionReadyForProcessing && this.secondEditAction.equals(EditActions.PASTE)) {
                    isActionReadyForProcessing = true;
                } else {
                    isActionReadyForProcessing = false;
                }
                break;
            case DELETE:
                isActionReadyForProcessing = this.selectedTasks.size() > 0;
                break;
        }
        return isActionReadyForProcessing;
    }

    public EditActions getFirstEditAction() {
        return this.firstEditAction;
    }

    public boolean isFirstActionComplete() {
        return this.firstEditAction != null;
    }

    public void setFirstEditAction(EditActions firstEditAction) {
        this.firstEditAction = firstEditAction;
    }

    public EditActions getSecondEditAction() {
        return this.secondEditAction;
    }

    public void setSecondEditAction(EditActions secondEditAction) {
        this.secondEditAction = secondEditAction;
    }

    public void clearAction() {
        this.selectedTasks.clear();
        this.destinationTask = null;
        this.firstEditAction = null;
        this.secondEditAction = null;
    }

    public boolean conatinsTask(Long taskId) {
        if (taskId != null) {
            for (Task task2 : this.selectedTasks) {
                if (task2.getId().equals(taskId)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void removeTask(Long taskId) {
        if (taskId != null) {
            for (Task task2 : this.selectedTasks) {
                if (task2.getId().equals(taskId)) {
                    this.selectedTasks.remove(task2);
                    return;
                }
            }
        }
    }

    public boolean isEmpty() {
        return this.selectedTasks.size() < 1;
    }

    public String toString() {
        return getDestinationTaskName() + Globals.FORWARDSLASH + this.firstEditAction + Globals.FORWARDSLASH + this.secondEditAction + Globals.FORWARDSLASH + this.selectedTasks.size();
    }

    private String getDestinationTaskName() {
        return this.destinationTask == null ? "N/A" : this.destinationTask.getName();
    }

    public void addSelectedTasks(List<Task> tasks) {
        if (tasks != null) {
            this.selectedTasks.addAll(tasks);
        }
    }
}
