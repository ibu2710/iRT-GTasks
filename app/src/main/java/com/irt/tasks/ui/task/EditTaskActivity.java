package com.irt.tasks.ui.task;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.ContextBase;
import com.irt.tasks.app.businessobjects.ContextTask;
import com.irt.tasks.app.businessobjects.Tag;
import com.irt.tasks.app.businessobjects.TagTask;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.ContextBp;
import com.irt.tasks.app.businessprocess.ContextBpImpl;
import com.irt.tasks.app.businessprocess.ProjectBp;
import com.irt.tasks.app.businessprocess.ProjectBpImpl;
import com.irt.tasks.app.businessprocess.TagBp;
import com.irt.tasks.app.businessprocess.TagBpImpl;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.businessprocess.TasksListBp;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.DateUtil;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.customrepeat.CustomRepeatActivity;
import com.irt.tasks.ui.customrepeat.CustomRepeatHelper;
import com.irt.tasks.ui.datetimepicker.DatePickerActivity;
import com.irt.tasks.ui.receiver.AlarmSetterHelper;
import com.irt.tasks.ui.sync.DropboxSyncHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EditTaskActivity extends AbstractActivity {
    private static final int FONT_BACKGROUND_COLOR_DIALOG = 4;
    private static final int FONT_COLOR_DIALOG = 3;
    private static final int FONT_STYLE_COLOR_DIALOG = 1;
    private static final int FONT_STYLE_COLOR_SELECTION_BACKGROUND_COLOR = 2;
    private static final int FONT_STYLE_COLOR_SELECTION_COLOR = 1;
    private static final int FONT_STYLE_COLOR_SELECTION_STYLE = 0;
    private static final int FONT_STYLE_DIALOG = 2;
    public static final int NUMBER_OF_COLORS = 52;
    ConfigManager config;
    ContextBp contextBp = new ContextBpImpl();
    boolean isAddAnother = false;
    private boolean isContentModified = false;
    Button mAddAnotherButton;
    Button mCancelButton;
    CheckBox mChildCheckBox;
    TableRow mContextTableRow;
    TextView mContextTextView;
    TextView mCreationDateTextView;
    Button mDeleteButton;
    CheckBox mDoneCheckBox;
    Button mDueDateButton;
    boolean mFirstTimeContextTaskAccess = true;
    boolean mFirstTimeTagTaskAccess = true;
    CheckBox mFloatCheckbox;
    private SpannableStringBuilder[] mFontBackgroundColor = new SpannableStringBuilder[52];
    private SpannableStringBuilder[] mFontColor = new SpannableStringBuilder[52];
    TextView mFontColorLabel;
    TextView mFontColorSample;
    CheckBox mHighlightCheckbox;
    ImageButton mHistoryImageButton;
    private boolean mIsActionSend = false;
    boolean mIsCreateAnother;
    private boolean mIsFromAgendaView = false;
    private boolean mIsFromCalenadrNewTask = false;
    private boolean mIsFromFilterView = false;
    boolean mIsNameOrNoteModified = false;
    TextView mLastModifiedTextView;
    EditText mNoteEditText;
    int mOrigTasksListPosition = -1;
    TableRow mProjectTableRow;
    TextView mProjectTextView;
    Button mReminderButton;
    private TextView mRepeatDescription;
    private TableLayout mRepeatDescriptionTableLayout;
    Button mSaveButton;
    int mSelectedTasksListPostion = 0;
    CheckBox mShowNoteAlwaysCheckbox;
    long mSnoozeDateTimeBuffer;
    TableRow mStatusTableRow;
    TextView mStatusTextView;
    TableRow mTagTableRow;
    TextView mTagTextView;
    Task mTask;
    Task mTaskFromCallingActivity;
    Long mTaskIdToHighlight = null;
    EditText mTaskNameEditText;
    String[] mTasksListNames;
    Spinner mTasksListSpinner;
    List<TasksList> mTasksLists;
    Button mViewButton;
    ProjectBp projectBp = new ProjectBpImpl();
    TagBp tagBp = new TagBpImpl();
    TaskBp taskBp = new TaskBpImpl();
    TasksListBp tasksListBp = new TasksListBpImpl();

    protected void onCreate(Bundle savedInstanceState) {
        boolean z = true;
        super.onCreate(savedInstanceState);
        setTitle("iRT GTasks - Task Details");
        setContentView(R.layout.task_edit);
        this.config = ConfigManager.getInstance();
        this.mIsFromAgendaView = !StringUtil.isNullOrEmpty(getIntent().getStringExtra(Constants.IS_FROM_AGENDA_VIEW));
        if (!this.mIsFromAgendaView) {
            if (StringUtil.isNullOrEmpty(getIntent().getStringExtra(Constants.IS_FROM_FILTER_VIEW))) {
                z = false;
            }
            this.mIsFromFilterView = z;
        }
        setTaskFromCallingActivity();
        if (!processExternalApplicationSends()) {
            processNewFromCalendar();
        }
        if (this.mTaskFromCallingActivity != null) {
            initializeView();
        }
        if (this.mTask.getSnoozeDateTime().longValue() < AppUtil.getTodayDateTimeMilisecond()) {
            this.mTask.setSnoozeDateTime(Long.valueOf(0));
        }
        this.mSnoozeDateTimeBuffer = this.mTask.getSnoozeDateTime().longValue();
        setTasksListSpinner();
        setDoneListener();
        setFloatListener();
        setupFontColor();
        setModificationAndCreationDates();
        setHighlightListener();
        setShowNoteAlwaysListener();
        setNoteListener();
        this.mIsActionSend = false;
    }

    private void setNameAndNoteModified() {
        this.mIsNameOrNoteModified = true;
    }

    private void initTaskNoteEditText() {
        this.mNoteEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                EditTaskActivity.this.setNameAndNoteModified();
                DropboxSyncHelper.uiRequestSyncWithDelay();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void initTaskNameEditText() {
        this.mTaskNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                EditTaskActivity.this.setNameAndNoteModified();
                DropboxSyncHelper.uiRequestSyncWithDelay();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setModificationAndCreationDates() {
        this.mLastModifiedTextView.setText("Last modified on  " + formatModificationDates(new Date(this.mTask.getLastContentModifiedDate().longValue())));
        this.mCreationDateTextView.setText("Created on  " + formatModificationDates(new Date(this.mTask.getCreationDate().longValue())));
    }

    private String formatModificationDates(Date date) {
        return AppUtil.formatDateByPreference(date)
               // + ItemSortKeyBase.MIN_BUT_TWO_SORT_KEY
                + AppUtil.formatTimeByPreference(date);
    }

    private void setRepeatDescription() {
        this.mRepeatDescription.setText(CustomRepeatHelper.getVerboseDescription(this.mTask.getReminderRepeatRule(), new Date(this.mTask.getDueDate().longValue()), this.mTask.isReminderRepeatFromCompletionDate().booleanValue()) + AppUtil.getNextDateIsOnWording(this.mTask));
    }

    private void setHighlightListener() {
        this.mHighlightCheckbox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EditTaskActivity.this.isContentModified = true;
            }
        });
    }

    private void setShowNoteAlwaysListener() {
        this.mShowNoteAlwaysCheckbox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EditTaskActivity.this.isContentModified = true;
            }
        });
    }

    private void setNoteListener() {
        this.mNoteEditText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EditTaskActivity.this.isContentModified = true;
            }
        });
    }

    private void setDoneListener() {
        this.mDoneCheckBox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EditTaskActivity.this.isContentModified = true;
                EditTaskActivity.this.mTask.setSnoozeDateTime(Long.valueOf(0));
                EditTaskActivity.this.mTask.setCompleted(Boolean.valueOf(EditTaskActivity.this.mDoneCheckBox.isChecked()));
                if (EditTaskActivity.this.mTask.isRecurringTask() && (CustomRepeatHelper.getUntil() == 0 || EditTaskActivity.this.mTask.getDueDate().longValue() < CustomRepeatHelper.getUntil())) {
                    EditTaskActivity.this.mTask.setDueDate(Long.valueOf(CustomRepeatHelper.getNextDueDate(EditTaskActivity.this.mTask)));
                    if (EditTaskActivity.this.mTask.getReminderDateTime().longValue() != 0) {
                        AppUtil.calculateReminderDateTime(EditTaskActivity.this.mTask.getDueDate(), EditTaskActivity.this.mTask, false);
                    }
                    EditTaskActivity.this.mTask.setCompleted(Boolean.valueOf(false));
                    EditTaskActivity.this.mDoneCheckBox.setChecked(EditTaskActivity.this.mTask.isCompleted().booleanValue());
                    EditTaskActivity.this.setDueDateButtonText(EditTaskActivity.this.mTask.getDueDate(), false);
                    Toast.makeText(EditTaskActivity.this, "Due date set to " + AppUtil.formatDateByPreference(new Date(EditTaskActivity.this.mTask.getDueDate().longValue())), Toast.LENGTH_LONG).show();
                }
                if (EditTaskActivity.this.mTask.isCompleted().booleanValue()) {
                    long todayDateMillis = AppUtil.getTodayDateOnly().getTime();
                    if (EditTaskActivity.this.mTask.isFloat().booleanValue() && EditTaskActivity.this.mTask.getDueDate().longValue() <= todayDateMillis) {
                        EditTaskActivity.this.mTask.setDueDate(Long.valueOf(todayDateMillis));
                        EditTaskActivity.this.setDueDateButtonText(EditTaskActivity.this.mTask.getDueDate(), false);
                    }
                    EditTaskActivity.this.mFloatCheckbox.setChecked(false);
                }
                EditTaskActivity.this.setRepeatDescription();
            }
        });
    }

    private void setFloatListener() {
        this.mFloatCheckbox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EditTaskActivity.this.isContentModified = true;
                EditTaskActivity.this.mTask.setIsFloat(Boolean.valueOf(EditTaskActivity.this.mFloatCheckbox.isChecked()));
                if (EditTaskActivity.this.mFloatCheckbox.isChecked()) {
                    EditTaskActivity.this.mDoneCheckBox.setChecked(false);
                }
                if (EditTaskActivity.this.mTask.isRecurringTask()) {
                    EditTaskActivity.this.setRepeatDescription();
                }
            }
        });
    }

    private void initDueDate() {
        if (this.mTask.getDueDate().longValue() == 0) {
            this.mTask.setDueDate(Long.valueOf(AppUtil.getTodayDateOnly().getTime()));
            setDueDateButtonText(this.mTask.getDueDate(), false);
        }
    }

    private void setTaskFromCallingActivity() {
        if (StringUtil.isNullOrEmpty(getIntent().getStringExtra(Constants.INTENT_FROM_CALENDAR))) {
            Object obj = ArchitectureContext.getObject();
            if (obj != null && (obj instanceof Task)) {
                this.mTaskFromCallingActivity = (Task) ArchitectureContext.getObject();
                Long taskId = this.mTaskFromCallingActivity.getId();
                int level = this.mTaskFromCallingActivity.getLevel().intValue();
                boolean isParent = this.mTaskFromCallingActivity.isParent();
                if (taskId != null) {
                    this.mTaskFromCallingActivity = this.taskBp.get(this.mTaskFromCallingActivity.getId());
                    this.mTaskFromCallingActivity.setLevel(Integer.valueOf(level));
                    this.mTaskFromCallingActivity.setTasksList(this.tasksListBp.get(this.mTaskFromCallingActivity.getTasksListId()));
                    this.mTaskFromCallingActivity.setParent(isParent);
                    return;
                }
                return;
            }
            return;
        }
        this.mTaskFromCallingActivity = this.taskBp.get(Long.valueOf(getIntent().getLongExtra(Constants.INTENT_CALENDAR_TASK_ID, 0)));
    }

    private boolean processExternalApplicationSends() {
        boolean z = !StringUtil.isNullOrEmpty(getIntent().getAction()) && getIntent().getAction().equals("android.intent.action.SEND");
        this.mIsActionSend = z;
        if (!this.mIsActionSend || isSelectedDateAvailable()) {
            return false;
        }
        this.mIsCreateAnother = true;
        this.mTaskFromCallingActivity = new Task();
        this.mTaskFromCallingActivity.setName(getIntent().getStringExtra("android.intent.extra.SUBJECT"));
        this.mTaskFromCallingActivity.setNote(getIntent().getStringExtra("android.intent.extra.TEXT"));
        if (Constants.TASKS_LIST_AUTO_ID.equals(this.config.getTasksListForNewTaskPlus())) {
            this.mTaskFromCallingActivity.setTasksListId(this.config.getLastSelectedTasksList());
        } else {
            this.mTaskFromCallingActivity.setTasksListId(this.config.getTasksListForNewTaskPlus());
        }
        initializeView();
        return true;
    }

    private boolean isSelectedDateAvailable() {
        return getIntent().getLongExtra(Constants.INTENT_SELECTED_DATE, 0) != 0;
    }

    private void processNewFromCalendar() {
        boolean z = !StringUtil.isNullOrEmpty(getIntent().getAction()) && getIntent().getAction().equals("android.intent.action.SEND");
        this.mIsActionSend = z;
        if (this.mIsActionSend && isSelectedDateAvailable()) {
            this.mIsFromCalenadrNewTask = true;
            this.mIsCreateAnother = true;
            this.mTaskFromCallingActivity = new Task();
            this.mTaskFromCallingActivity.setDueDate(Long.valueOf(getIntent().getLongExtra(Constants.INTENT_SELECTED_DATE, AppUtil.getTodayDateOnly().getTime())));
            if (Constants.TASKS_LIST_AUTO_ID.equals(this.config.getTasksListForNewTaskPlus())) {
                this.mTaskFromCallingActivity.setTasksListId(this.config.getLastSelectedTasksList());
            } else {
                this.mTaskFromCallingActivity.setTasksListId(this.config.getTasksListForNewTaskPlus());
            }
            initializeView();
        }
    }

    private void initializeView() {
        initView();
        initViewTaskAndFields(this.mTaskFromCallingActivity);
        initDeleteButton();
        initSaveButton();
        initCancelButton();
        initViewButton();
        initAddAnotherButton();
        initChildCheckBox();
        inttFontSampleColor();
        setDueDateButtonListener();
        initTaskNameEditText();
        initTaskNoteEditText();
    }

    protected void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case 3:
                initFontColor();
                removeDialog(3);
                return;
            case 4:
                initFontBackgroundColor();
                removeDialog(4);
                return;
            default:
                return;
        }
    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new Builder(this).setTitle("Select font style/color").setItems(R.array.font_sytle_color, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                EditTaskActivity.this.showDialog(2);
                                return;
                            case 1:
                                EditTaskActivity.this.showDialog(3);
                                return;
                            case 2:
                                EditTaskActivity.this.showDialog(4);
                                return;
                            default:
                                return;
                        }
                    }
                }).create();
            case 2:
                return new Builder(this).setTitle("Select font style").setSingleChoiceItems(R.array.font_sytle, this.mTask.getFontStyle().intValue(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditTaskActivity.this.mTask.setFontStyle(Integer.valueOf(which));
                        dialog.dismiss();
                        EditTaskActivity.this.setSampleFont();
                    }
                }).create();
            case 3:
                return new Builder(this).setTitle("Select font color").setSingleChoiceItems(this.mFontColor, this.mTask.getFontColor().intValue(), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichColor) {
                        EditTaskActivity.this.mTask.setFontColor(Integer.valueOf(whichColor));
                        dialog.dismiss();
                        EditTaskActivity.this.setSampleFont();
                    }
                }).create();
            case 4:
                return new Builder(this).setTitle("Select font background color").setSingleChoiceItems(this.mFontBackgroundColor, this.mTask.getFontBgColor().intValue(), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichColor) {
                        EditTaskActivity.this.mTask.setFontBgColor(Integer.valueOf(whichColor));
                        dialog.dismiss();
                        EditTaskActivity.this.setSampleFont();
                    }
                }).create();
            default:
                return null;
        }
    }

    private void initFontBackgroundColor() {
        for (int i = 0; i < 52; i++) {
            this.mFontBackgroundColor[i] = new SpannableStringBuilder();
            this.mFontBackgroundColor[i].append(getFontText(i));
            this.mFontBackgroundColor[i].setSpan(new ForegroundColorSpan(getFontColor()), 0, this.mFontBackgroundColor[i].length(), 0);
            this.mFontBackgroundColor[i].setSpan(new BackgroundColorSpan(EditTaskHelper.getFontColor(i, -1)), 0, this.mFontBackgroundColor[i].length(), 0);
            AppUtil.setFontStyle(this.mFontBackgroundColor[i], this.mTask);
        }
    }

    private int getFontColor() {
        return EditTaskHelper.getFontColor(this.mTask.getFontColor().intValue(), AppUtil.getFontAutoColor(this.mTask));
    }

    private void initFontColor() {
        for (int i = 0; i < 52; i++) {
            this.mFontColor[i] = new SpannableStringBuilder();
            this.mFontColor[i].append(getFontText(i));
            this.mFontColor[i].setSpan(new ForegroundColorSpan(EditTaskHelper.getFontColor(i, AppUtil.getFontAutoColor(this.mTask))), 0, this.mFontColor[i].length(), 0);
            this.mFontColor[i].setSpan(new BackgroundColorSpan(EditTaskHelper.getFontColor(this.mTask.getFontBgColor().intValue(), -1)), 0, this.mFontColor[i].length(), 0);
            AppUtil.setFontStyle(this.mFontColor[i], this.mTask);
        }
    }

    private String getFontText(int position) {
        if (position == 0) {
            return " Auto ";
        }
        return " Color " + position + " ";
    }

    private void setupFontColor() {
        this.mFontColorSample = (TextView) findViewById(R.id.font_color_sample);
        setSampleFont();
        this.mFontColorSample.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EditTaskActivity.this.showDialog(1);
            }
        });
    }

    private void inttFontSampleColor() {
        setSampleFont();
        this.mFontColorSample.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EditTaskActivity.this.showDialog(1);
            }
        });
    }

    private void setSampleFont() {
        this.mFontColorSample.setText(getFontColorSample());
    }

    private SpannableStringBuilder getFontColorSample() {
        SpannableStringBuilder fontColorSample = new SpannableStringBuilder();
        fontColorSample.append(" Sample Font ");
        AppUtil.setFontForegroundColor(fontColorSample, this.mTask);
        if (this.mTask.getFontBgColor().intValue() != 0) {
            AppUtil.setFontBackgroundColor(fontColorSample, this.mTask);
        }
        AppUtil.setFontStyle(fontColorSample, this.mTask);
        fontColorSample.setSpan(new UnderlineSpan(), 0, fontColorSample.length(), 0);
        return fontColorSample;
    }

    private void initChildCheckBox() {
        this.mChildCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChild) {
                EditTaskActivity.this.setNewTaskParentAndSibling(EditTaskActivity.this.mTaskFromCallingActivity, isChild);
            }
        });
    }

    private void initAddAnotherButton() {
        this.mAddAnotherButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                performAddAnother();
            }

            private void performAddAnother() {
                EditTaskActivity.this.performSave();
                EditTaskActivity.this.isAddAnother = true;
                EditTaskActivity.this.mIsCreateAnother = true;
                EditTaskActivity.this.mTaskFromCallingActivity = EditTaskActivity.this.mTask;
                EditTaskActivity.this.initializeView();
                EditTaskActivity.this.onResume();
            }
        });
    }

    private void initViewTaskAndFields(Task taskFromCallingActivity) {
        Bundle extras = getIntent().getExtras();
        if (isTaskNew(extras)) {
            this.mDeleteButton.setVisibility(View.GONE);
            this.mViewButton.setVisibility(View.GONE);
            this.mAddAnotherButton.setVisibility(View.VISIBLE);
            this.mChildCheckBox.setVisibility(View.VISIBLE);
            setSampleFontMargin();
            initForNewTask(taskFromCallingActivity, extras);
            initForEdit(this.mTask);
            this.mTaskNameEditText.requestFocus();
            return;
        }
        initForEdit(taskFromCallingActivity);
    }

    private void setSampleFontMargin() {
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.setMargins(40, 0, 0, 0);
        this.mFontColorLabel = (TextView) findViewById(R.id.font_color_label);
        this.mFontColorLabel.setLayoutParams(layoutParams);
    }

    private void turnOffAutoKeyboardPopUp() {
        getWindow().setSoftInputMode(3);
    }

    private boolean isTaskNew(Bundle extras) {
        if (this.mIsCreateAnother) {
            return true;
        }
        if (extras == null || extras.get(Constants.IS_NEW_TASK) == null) {
            return false;
        }
        return true;
    }

    private void initForNewTask(Task taskFromCallingActivity, Bundle extras) {
        boolean isChild = extras.get(Constants.IS_CHILD_TASK) != null;
        if (this.mIsCreateAnother) {
            isChild = false;
        }
        this.mChildCheckBox.setChecked(isChild);
        setNewTaskParentAndSibling(taskFromCallingActivity, isChild);
    }

    private void setNewTaskParentAndSibling(Task taskFromCallingActivity, boolean isChild) {
        if (!this.mIsActionSend || this.isAddAnother) {
            this.mTask = new Task();
            this.mTask.setTasksListId(taskFromCallingActivity.getTasksListId());
            this.mTask.setProjectId(taskFromCallingActivity.getProjectId());
            if (this.mIsFromAgendaView || this.mIsFromCalenadrNewTask) {
                this.mTask.setDueDate(taskFromCallingActivity.getDueDate());
            } else if (this.mIsFromFilterView) {
                this.mTask.setStatus(taskFromCallingActivity.getStatus());
                this.mTask.setContextTasks(taskFromCallingActivity.getContextTasks());
                this.mTask.setTagTasks(taskFromCallingActivity.getTagTasks());
            }
        } else {
            this.mTask = taskFromCallingActivity;
        }
        if (!taskFromCallingActivity.isTasksListAsTask()) {
            if (isChild) {
                this.mTask.setParentTaskId(taskFromCallingActivity.getId());
                return;
            }
            this.mTask.setParentTaskId(taskFromCallingActivity.getParentTaskId());
            this.mTask.setPriorSiblingTaskId(taskFromCallingActivity.getId());
        }
    }

    private void initForEdit(Task taskFromCallingActivity) {
        this.mTask = taskFromCallingActivity;
        this.mTaskNameEditText.setText(this.mTask.getName());
        this.mDoneCheckBox.setChecked(this.mTask.isCompleted().booleanValue());
        this.mNoteEditText.setText(this.mTask.getNote());
        this.mHighlightCheckbox.setChecked(this.mTask.isHighlight().booleanValue());
        this.mShowNoteAlwaysCheckbox.setChecked(this.mTask.isShowNote().booleanValue());
        this.mFloatCheckbox.setChecked(this.mTask.isFloat().booleanValue());
        setDueDateButtonText(this.mTask.getDueDate(), false);
        setReminderButton();
    }

    protected void onResume() {
        super.onResume();
        this.mDueDateButton.invalidate();
        setDueDateButtonText(this.mTask.getDueDate(), false);
        setRepeatDescription();
        setStatusDescription();
        setContextDescription();
        setProjectDescription();
        setTagDescription();
    }

    private void setTagDescription() {
        List<TagTask> tagTasks;
        if (!this.mFirstTimeTagTaskAccess || this.mTask.getId() == null) {
            tagTasks = this.mTask.getTagTasks();
        } else {
            tagTasks = this.tagBp.getAllTagTasksByTaskId(this.mTask.getId());
            this.mFirstTimeTagTaskAccess = false;
        }
        if (tagTasks != null) {
            this.mTask.setTagTasks(tagTasks);
            List<Tag> contextBases = this.tagBp.getAllTags();
            StringBuffer tagString = new StringBuffer();
            boolean firstRecord = true;
            for (TagTask tagTask : tagTasks) {
                if (!firstRecord) {
                    tagString.append(", ");
                }
                appendTagName(tagString, tagTask, contextBases);
                firstRecord = false;
            }
            this.mTagTextView.setText(tagString.toString());
        }
    }

    private void appendTagName(StringBuffer tagString, TagTask tagTask, List<Tag> tags) {
        for (Tag tag : tags) {
            if (tag.getId().equals(tagTask.getTagId())) {
                tagString.append(tag.getName());
            }
        }
    }

    private void setContextDescription() {
        List<ContextTask> contextTasks;
        if (!this.mFirstTimeContextTaskAccess || this.mTask.getId() == null) {
            contextTasks = this.mTask.getContextTasks();
        } else {
            contextTasks = this.contextBp.getAllContextTasksByTaskId(this.mTask.getId());
            this.mFirstTimeContextTaskAccess = false;
        }
        if (contextTasks != null) {
            this.mTask.setContextTasks(contextTasks);
            List<ContextBase> contextBases = this.contextBp.getAllContexts();
            StringBuffer contextString = new StringBuffer();
            boolean firstRecord = true;
            for (ContextTask contextTask : contextTasks) {
                if (!firstRecord) {
                    contextString.append(", ");
                }
                appendContextName(contextString, contextTask, contextBases);
                firstRecord = false;
            }
            this.mContextTextView.setText(contextString.toString());
        }
    }

    private void appendContextName(StringBuffer contextString, ContextTask contextTask, List<ContextBase> contextBases) {
        for (ContextBase contextBase : contextBases) {
            if (contextBase.getId().equals(contextTask.getContextId())) {
                contextString.append(contextBase.getName());
            }
        }
    }

    private void setProjectDescription() {
        Long projectId = this.mTask.getProjectId();
        if (projectId.longValue() == 0) {
            this.mProjectTextView.setText("");
            return;
        }
        this.mProjectTextView.setText(this.projectBp.get(projectId).getTitle());
    }

    private void setStatusDescription() {
        if (this.mTask.getStatus().intValue() == 0) {
            this.mStatusTextView.setText("");
        } else {
            this.mStatusTextView.setText(AppUtil.getStringFromArrayValueBasedOnPosition(this, R.array.task_status, this.mTask.getStatus().intValue()));
        }
    }

    private void setDueDateButtonText(Long selectedDueDateMillis, boolean isCalculateReminderDateTime) {
        if (selectedDueDateMillis.equals(Long.valueOf(0))) {
            this.mDueDateButton.setText("None");
            this.mTask.setReminderEnabled(Boolean.valueOf(false));
            this.mReminderButton.setEnabled(false);
            this.mTask.setReminderRepeatRule(null);
            this.mTask.setSnoozeDateTime(Long.valueOf(0));
            setRepeatDescription();
        } else {
            if (this.config.isEnableReminder()) {
                this.mReminderButton.setEnabled(true);
            } else {
                this.mReminderButton.setEnabled(false);
            }
            this.mDueDateButton.setText(DateUtil.format(new Date(selectedDueDateMillis.longValue()), getDueDateDateFormat()));
            if (isCalculateReminderDateTime) {
                AppUtil.calculateReminderDateTime(selectedDueDateMillis, this.mTask, false);
            }
            if (this.mTask.getReminderDateTime().longValue() > this.mTask.getSnoozeDateTime().longValue()) {
                this.mTask.setSnoozeDateTime(Long.valueOf(0));
            }
        }
        setReminderButtonText(selectedDueDateMillis);
    }

    private void setReminderButtonText(Long selectedDueDateMillis) {
        if (this.config.isEnableReminder() && !selectedDueDateMillis.equals(Long.valueOf(0)) && this.mTask.isReminderEnabled().booleanValue()) {
            calculateReminderDate(selectedDueDateMillis);
            Calendar reminderCal = Calendar.getInstance();
            reminderCal.setTimeInMillis(this.mTask.getReminderDateTime().longValue());
            if (AppUtil.getDateOnly(new Date(selectedDueDateMillis.longValue())).getTime() == AppUtil.getDateOnly(reminderCal.getTime()).getTime()) {
                this.mReminderButton.setText(DateUtil.format(reminderCal, AppUtil.getReminderTimeFormat()));
                moveDueDateButtonUp();
                return;
            }
            this.mReminderButton.setText(AppUtil.getReminderDaysEarlier(this.mTask.getReminderDateTime(), this.mTask.getDueDate()) + "\n   " + DateUtil.format(reminderCal, AppUtil.getReminderTimeFormat()));
        } else if (this.config.isEnableReminder()) {
            this.mReminderButton.setText("Reminder");
            moveDueDateButtonUp();
        } else {
            this.mReminderButton.setText("Reminder \n disabled");
        }
    }

    private void calculateReminderDate(Long selectedDueDateMillis) {
        Calendar reminderDateTime = Calendar.getInstance();
        reminderDateTime.setTimeInMillis(this.mTask.getReminderDateTime().longValue());
        EditTaskHelper.calculateReminderDate(this.mTask.getReminderDaysEarlier().intValue(), this.mTask, reminderDateTime);
        this.mTask.setReminderDateTime(Long.valueOf(reminderDateTime.getTimeInMillis()));
    }

    private void moveDueDateButtonUp() {
        LayoutParams layoutParams = new LayoutParams(this.mDueDateButton.getLayoutParams());
        layoutParams.setMargins(0, 0, 0, 0);
        this.mDueDateButton.setLayoutParams(layoutParams);
    }

    private void initView() {
        this.mTaskNameEditText = (EditText) findViewById(R.id.task_name);
        this.mDoneCheckBox = (CheckBox) findViewById(R.id.done_checkbox);
        this.mChildCheckBox = (CheckBox) findViewById(R.id.child_checkbox);
        this.mHistoryImageButton = (ImageButton) findViewById(R.id.task_name_history_button);
        this.mDueDateButton = (Button) findViewById(R.id.due_date_button);
        this.mReminderButton = (Button) findViewById(R.id.reminder_button);
        this.mTasksListSpinner = (Spinner) findViewById(R.id.tasks_list_spinner);
        this.mNoteEditText = (EditText) findViewById(R.id.note);
        this.mDeleteButton = (Button) findViewById(R.id.delete_button);
        this.mSaveButton = (Button) findViewById(R.id.save_button);
        this.mViewButton = (Button) findViewById(R.id.view_button);
        this.mCancelButton = (Button) findViewById(R.id.cancel_button);
        this.mAddAnotherButton = (Button) findViewById(R.id.add_another_button);
        this.mHighlightCheckbox = (CheckBox) findViewById(R.id.highlight_checkbox);
        this.mFontColorSample = (TextView) findViewById(R.id.font_color_sample);
        this.mShowNoteAlwaysCheckbox = (CheckBox) findViewById(R.id.show_note_always_checkbox);
        this.mFloatCheckbox = (CheckBox) findViewById(R.id.float_checkbox);
        this.mLastModifiedTextView = (TextView) findViewById(R.id.last_modified);
        this.mCreationDateTextView = (TextView) findViewById(R.id.creation_date);
        this.mRepeatDescription = (TextView) findViewById(R.id.custom_repeat_description);
        this.mStatusTableRow = (TableRow) findViewById(R.id.status_table_row);
        this.mProjectTableRow = (TableRow) findViewById(R.id.project_table_row);
        this.mContextTableRow = (TableRow) findViewById(R.id.context_table_row);
        this.mTagTableRow = (TableRow) findViewById(R.id.tag_table_row);
        this.mStatusTextView = (TextView) findViewById(R.id.status_value);
        this.mProjectTextView = (TextView) findViewById(R.id.project_value);
        this.mContextTextView = (TextView) findViewById(R.id.context_value);
        this.mTagTextView = (TextView) findViewById(R.id.tag_value);
        this.mRepeatDescriptionTableLayout = (TableLayout) findViewById(R.id.repeat_description_table_layout);
        setRepeatDescriptionListener();
        setStatusTableRowListener();
        setProjectTableRowListener();
        setContextTableRowListener();
        setTagTableRowListener();
    }

    private void setTagTableRowListener() {
        this.mTagTableRow.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EditTaskActivity.this.isContentModified = true;
                ArchitectureContext.setObject(EditTaskActivity.this.mTask);
                EditTaskActivity.this.startActivity(new Intent(EditTaskActivity.this, TagSelectionListActivity.class));
            }
        });
    }

    private void setContextTableRowListener() {
        this.mContextTableRow.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EditTaskActivity.this.isContentModified = true;
                ArchitectureContext.setObject(EditTaskActivity.this.mTask);
                EditTaskActivity.this.startActivity(new Intent(EditTaskActivity.this, ContextSelectionListActivity.class));
            }
        });
    }

    private void setProjectTableRowListener() {
        this.mProjectTableRow.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EditTaskActivity.this.isContentModified = true;
                ArchitectureContext.setObject(EditTaskActivity.this.mTask);
                EditTaskActivity.this.startActivity(new Intent(EditTaskActivity.this, ProjectSelectionListActivity.class));
            }
        });
    }

    private void setStatusTableRowListener() {
        this.mStatusTableRow.setOnClickListener(new OnClickListener() {

            class C18571 implements DialogInterface.OnClickListener {
                C18571() {
                }

                public void onClick(DialogInterface dialog, int whichButton) {
                    EditTaskActivity.this.mTask.setStatus(Integer.valueOf(whichButton));
                    dialog.dismiss();
                    EditTaskActivity.this.onResume();
                }
            }

            public void onClick(View v) {
                EditTaskActivity.this.isContentModified = true;
                new Builder(EditTaskActivity.this).setTitle("Task Status").setSingleChoiceItems(R.array.task_status, EditTaskActivity.this.mTask.getStatus().intValue(), new C18571()).show();
            }
        });
    }

    private void setRepeatDescriptionListener() {
        this.mRepeatDescriptionTableLayout.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EditTaskActivity.this.isContentModified = true;
                EditTaskActivity.this.mTask.setCompleted(Boolean.valueOf(false));
                EditTaskActivity.this.mDoneCheckBox.setChecked(false);
                EditTaskActivity.this.initDueDate();
                ArchitectureContext.setObject(EditTaskActivity.this.mTask);
                EditTaskActivity.this.startActivity(new Intent(EditTaskActivity.this, CustomRepeatActivity.class));
            }
        });
    }

    private void setTasksListSpinner() {
        initTasksLists();
        ArrayAdapter<String> tasksListNamesAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, this.mTasksListNames);
        tasksListNamesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.mTasksListSpinner.setAdapter(tasksListNamesAdapter);
        this.mTasksListSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                EditTaskActivity.this.mSelectedTasksListPostion = position;
                if (EditTaskActivity.this.mOrigTasksListPosition != EditTaskActivity.this.mSelectedTasksListPostion) {
                    EditTaskActivity.this.isContentModified = true;
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.mTasksListSpinner.setSelection(this.mSelectedTasksListPostion);
    }

    private void initTasksLists() {
        this.mTasksLists = new ArrayList();
        int index = 0;
        for (TasksList tasksList : this.tasksListBp.getAllTasksListBySortOrder()) {
            if (this.config.isShowInvisibleAccounts().booleanValue() || tasksList.isVisible().booleanValue()) {
                this.mTasksLists.add(tasksList);
                if (this.mTask.getTasksListId().equals(tasksList.getId())) {
                    this.mSelectedTasksListPostion = index;
                    this.mOrigTasksListPosition = index;
                }
                index++;
            }
        }
        this.mTasksListNames = new String[this.mTasksLists.size()];
        for (int i = 0; i < this.mTasksLists.size(); i++) {
            this.mTasksListNames[i] = ((TasksList) this.mTasksLists.get(i)).getName();
        }
    }

    private void setDueDateButtonListener() {
        this.mDueDateButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EditTaskActivity.this.isContentModified = true;
                String dueDateStr = EditTaskActivity.this.mDueDateButton.getText() + "";
                Date selectedDate = AppUtil.getTodayDateOnly();
                if (DateUtil.isValidDate(dueDateStr, EditTaskActivity.this.getDueDateDateFormat())) {
                    selectedDate = DateUtil.parseDate(dueDateStr, EditTaskActivity.this.getDueDateDateFormat());
                }
                Intent intent = new Intent(EditTaskActivity.this, DatePickerActivity.class);
                intent.putExtra(Constants.SELECTED_DATE_LONG_KEY, selectedDate.getTime());
                EditTaskActivity.this.startActivityForResult(intent, 1);
            }
        });
    }

    private void setReminderButton() {
        this.mReminderButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EditTaskActivity.this.isContentModified = true;
                ArchitectureContext.setObject(EditTaskActivity.this.mTask);
                EditTaskActivity.this.startActivity(new Intent(EditTaskActivity.this, ReminderEditActivity.class));
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (-1 == resultCode) {
            switch (requestCode) {
                case 1:
                    Long selectedDateLong = (Long) data.getExtras().get(Constants.SELECTED_DATE_LONG_KEY);
                    if (selectedDateLong != null) {
                        setDueDateButtonText(selectedDateLong, true);
                        this.mTask.setDueDate(selectedDateLong);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private String getDueDateDateFormat() {
        return Constants.DATE_FORMAT_MMMDYYYY;
    }

    private void initDeleteButton() {
        this.mDeleteButton.setOnClickListener(new OnClickListener() {

            class C18591 implements DialogInterface.OnClickListener {
                C18591() {
                }

                public void onClick(DialogInterface dialog, int button) {
                    if (EditTaskActivity.this.mTask.isReminderEnabled().booleanValue()) {
                        AlarmSetterHelper.scheduleAlarm(EditTaskActivity.this.config.getContext(), EditTaskActivity.this.mTask, false);
                    }
                    EditTaskActivity.this.taskBp.delete(EditTaskActivity.this.mTask);
                    EditTaskActivity.this.setResultCode();
                    EditTaskActivity.this.finish();
                }
            }

            public void onClick(View v) {
                new Builder(EditTaskActivity.this).setTitle(R.string.delete_title).setMessage(R.string.delete_this_task).setPositiveButton(R.string.alert_dialog_ok, new C18591()).setNegativeButton(R.string.alert_dialog_cancel, null).show();
            }
        });
    }

    private void setResultCode() {
        if (StringUtil.isNullOrEmpty(getIntent().getStringExtra(Constants.INTENT_FROM_VIEW_TASK))) {
            setResult(-1);
        } else {
            this.config.setPreferenceAccessed(true);
        }
    }

    private void initSaveButton() {
        this.mSaveButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EditTaskActivity.this.performSave();
                Intent data = new Intent();
                data.putExtra(Constants.NEW_TASK_ID, EditTaskActivity.this.mTaskIdToHighlight);
                if (StringUtil.isNullOrEmpty(EditTaskActivity.this.getIntent().getStringExtra(Constants.INTENT_FROM_VIEW_TASK))) {
                    EditTaskActivity.this.setResult(-1, data);
                } else {
                    EditTaskActivity.this.config.setPreferenceAccessed(true);
                }
                EditTaskActivity.this.finish();
            }
        });
    }

    private void initViewButton() {
        this.mViewButton.setOnClickListener(new OnClickListener() {

            class C18601 implements DialogInterface.OnClickListener {
                C18601() {
                }

                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }

            class C18612 implements DialogInterface.OnClickListener {
                C18612() {
                }

                public void onClick(DialogInterface dialog, int whichButton) {
                    //AnonymousClass26.this.preformView();
                }
            }

            class C18623 implements DialogInterface.OnClickListener {
                C18623() {
                }

                public void onClick(DialogInterface dialog, int whichButton) {
                    EditTaskActivity.this.performSave();
                    EditTaskActivity.this.config.setPreferenceAccessed(true);
                    //AnonymousClass26.this.preformView();
                }
            }

            public void onClick(View v) {
                if (EditTaskActivity.this.isContentModified) {
                    new Builder(EditTaskActivity.this).setTitle(R.string.alert_dialog_task_modified).setNeutralButton("Save & View", new C18623()).setPositiveButton(R.string.alert_dialog_ok, new C18612()).setNegativeButton(R.string.alert_dialog_cancel, new C18601()).show();
                } else {
                    preformView();
                }
            }

            private void preformView() {
                ArchitectureContext.setObject(EditTaskActivity.this.mTaskFromCallingActivity);
                EditTaskActivity.this.startActivity(new Intent(EditTaskActivity.this, TaskViewActivity.class));
                EditTaskActivity.this.finish();
            }
        });
    }

    private void initCancelButton() {
        this.mCancelButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EditTaskActivity.this.performCancel();
            }
        });
    }

    private void performCancel() {
        if (this.mIsNameOrNoteModified) {
            new Builder(this).setTitle(R.string.alert_dialog_task_name_note_modified).setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    EditTaskActivity.this.setResultCode();
                    EditTaskActivity.this.finish();
                }
            }).setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }).show();
            return;
        }
        setResultCode();
        finish();
    }

    private void performSave() {
        if (this.mTask.isRecurringTask()) {
            initDueDate();
        }
        this.mTask.setName(this.mTaskNameEditText.getText() + "");
        this.mTask.setCompleted(Boolean.valueOf(this.mDoneCheckBox.isChecked()));
        this.mTask.setTasksListId(((TasksList) this.mTasksLists.get(this.mSelectedTasksListPostion)).getId());
        this.mTask.setTasksListId(this.mTask.getTasksListId());
        this.mTask.setNote(this.mNoteEditText.getText() + "");
        this.mTask.setHighlight(Boolean.valueOf(this.mHighlightCheckbox.isChecked()));
        this.mTask.setShowNote(Boolean.valueOf(this.mShowNoteAlwaysCheckbox.isChecked()));
        this.mTask.setLastContentModifiedDate(Long.valueOf(AppUtil.getTodayDateTimeMilisecond()));
        this.mTask.setIsFloat(Boolean.valueOf(this.mFloatCheckbox.isChecked()));
        TasksList origTasksList = null;
        if (!(this.mTask.getId() == null || this.mSelectedTasksListPostion == this.mOrigTasksListPosition)) {
            origTasksList = (TasksList) this.mTasksLists.get(this.mOrigTasksListPosition);
        }
        if (this.mTask.isCompleted().booleanValue()) {
            AppUtil.clearNotification(this, this.mTask);
        }
        this.taskBp.saveOrUpdate(this.mTask, origTasksList);
        if (this.mTaskIdToHighlight == null) {
            this.mTaskIdToHighlight = this.mTask.getId();
        }
        AppUtil.setTaskAlarm(this, this.mTask);
        if (this.mSnoozeDateTimeBuffer > 0 && this.mTask.getSnoozeDateTime().equals(Long.valueOf(0))) {
            AppUtil.clearNotification(this, this.mTask);
        }
        performContextTasksSave();
        performTagTasksSave();
    }

    protected void performContextTasksSave() {
        List<ContextTask> contextTasks = this.mTask.getContextTasks();
        if (contextTasks != null) {
            this.contextBp.deleteAllConextTasksbyTaskId(this.mTask.getId());
            for (ContextTask contextTask : contextTasks) {
                contextTask.setTaskId(this.mTask.getId());
                this.contextBp.saveContextTask(contextTask);
            }
        }
    }

    protected void performTagTasksSave() {
        List<TagTask> tagTasks = this.mTask.getTagTasks();
        if (tagTasks != null) {
            this.tagBp.deleteAllTagTasksByTaskId(this.mTask.getId());
            for (TagTask tagTask : tagTasks) {
                tagTask.setTaskId(this.mTask.getId());
                this.tagBp.saveTagTask(tagTask);
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        performCancel();
        return true;
    }
}
