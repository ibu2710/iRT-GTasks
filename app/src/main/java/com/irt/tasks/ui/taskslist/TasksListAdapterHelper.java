package com.irt.tasks.ui.taskslist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.text.Layout;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.LevelColor;
import com.irt.tasks.app.businessobjects.LevelState;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.panel.EditActionDetail.EditActions;
import com.irt.tasks.ui.panel.EditManager;
import com.irt.tasks.ui.task.EditTaskHelper;
import java.util.Date;
import java.util.Map;

public class TasksListAdapterHelper {
    public static final float DATE_FONT_SIZE_MULTIPLIER = 0.65f;
    public static final float NOTE_FONT_SIZE_MULTIPLIER = 0.7f;
    public static OnTouchListener mLinkHandler = new C19552();

    static class C19552 implements OnTouchListener {
        C19552() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (v instanceof TextView) {
                TextView widget = (TextView) v;
                CharSequence text = widget.getText();
                if (text instanceof Spanned) {
                    Spanned buffer = (Spanned) text;
                    int action = event.getAction();
                    if (action == 1 || action == 0) {
                        int x = (((int) event.getX()) - widget.getTotalPaddingLeft()) + widget.getScrollX();
                        int y = (((int) event.getY()) - widget.getTotalPaddingTop()) + widget.getScrollY();
                        Layout layout = widget.getLayout();
                        int off = layout.getOffsetForHorizontal(layout.getLineForVertical(y), (float) x);
                        ClickableSpan[] link = (ClickableSpan[]) buffer.getSpans(off, off, ClickableSpan.class);
                        if (link.length != 0) {
                            if (action == 1) {
                                link[0].onClick(widget);
                                return true;
                            } else if (action == 0) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
    }

    static class ViewHolder {
        ImageView alarmClock;
        Bitmap alarmIcon;
        Bitmap alarmSnoozeIcon;
        Bitmap bulletIcon;
        ImageView dragAndDropIcon;
        TextView dueDate;
        TextView dueDate2;
        Bitmap emailAlarmIcon;
        Bitmap emailAlarmSnoozeIcon;
        Bitmap floatIcon;
        LinearLayout indentLayout;
        CheckBox isCompleted;
        LinearLayout listItem;
        Bitmap minusIcon;
        ImageView note;
        Bitmap noteIcon;
        Bitmap plusIcon;
        TextView plusMinusDot;
        ImageView plusMinusDotIcon;
        Bitmap repeatFloatIcon;
        ImageView repeatFloatImageView;
        Bitmap repeatIcon;
        TextView taskName;
        TextView taskNote;
        TextView taskNumber;
        TextView textNote;

        ViewHolder() {
        }
    }

    public static View getView(final Task task, View convertView, LayoutInflater mInflater, final Context context, final int position, LevelState levelState, Map<String, Long> expandedTasks, Task taskDecorator) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.tasks_list_list_item, null);
            holder = initViewHolder(convertView, context);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        setDoneCheckbox(holder, task);
        holder.isCompleted.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                boolean isRefreshDisplay;
                if (task.getSnoozeDateTime().longValue() > 0) {
                    isRefreshDisplay = true;
                } else {
                    isRefreshDisplay = false;
                }
                task.setSnoozeDateTime(Long.valueOf(0));
                if (holder.isCompleted.isChecked()) {
                    AppUtil.clearNotification(context, task);
                    task.setCompleted(Boolean.valueOf(true));
                } else {
                    task.setCompleted(Boolean.valueOf(false));
                }
                TaskBp taskBp = new TaskBpImpl();
                task.setLastContentModifiedDate(Long.valueOf(AppUtil.getTodayDateTimeMilisecond()));
                taskBp.setTaskAsDoneAndAdjustAlarm(task, context);
                task.setLastContentModifiedDate(Long.valueOf(AppUtil.getTodayDateTimeMilisecond()));
                taskBp.saveOrUpdate(task, null);
                //context.updateListItem(position, isRefreshDisplay);
            }
        });
        setIndentLayout(task, holder, levelState);
        setPlusMinusDot(task, holder, levelState, expandedTasks);
        setTaskName(task, holder);
        setNote(holder, task, context);
        setAlarmClock(holder, task, context);
        setRepeatFloat(holder, task, context);
        setDueDate(holder, task);
        setTaskNumber(task, holder);
        setSelectedTasksForEdit(task, holder, taskDecorator);
        setDragAndDropIcon(task, holder);
        return convertView;
    }

    private static void setDueDate(ViewHolder holder, Task task) {
        ConfigManager config = ConfigManager.getInstance();
        if (task.isTasksListAsTask() || !task.getTasksList().isShowDueDate().booleanValue() || task.getDueDate().equals(Long.valueOf(0))) {
            holder.dueDate.setVisibility(View.GONE);
            holder.dueDate2.setVisibility(View.GONE);
        } else if ((task.getTasksList().isShowNote().booleanValue() || task.isShowNote().booleanValue() || StringUtil.isNullOrEmpty(task.getNote())) && !config.isDragAndDropMode()) {
            holder.dueDate.setVisibility(View.GONE);
            holder.dueDate2.setVisibility(View.VISIBLE);
            holder.dueDate2.setText(AppUtil.formatDateByPreferenceYY(new Date(task.getDueDate().longValue()))
                   // + ItemSortKeyBase.MIN_BUT_TWO_SORT_KEY
            );
            holder.dueDate2.setTextSize(((float) config.getTaskNameFontSize().intValue()) * DATE_FONT_SIZE_MULTIPLIER);
        } else {
            holder.dueDate.setVisibility(View.VISIBLE);
            holder.dueDate2.setVisibility(View.GONE);
            holder.dueDate.setText(AppUtil.formatDateByPreferenceYY(new Date(task.getDueDate().longValue()))
                   // + ItemSortKeyBase.MIN_BUT_TWO_SORT_KEY
            );
            holder.dueDate.setTextSize(((float) config.getTaskNameFontSize().intValue()) * DATE_FONT_SIZE_MULTIPLIER);
        }
    }

    private static void setDragAndDropIcon(Task task, ViewHolder holder) {
        ConfigManager config = ConfigManager.getInstance();
        if (!config.isDragAndDropMode() || task.isTasksListAsTask()) {
            holder.dragAndDropIcon.setVisibility(View.GONE);
            return;
        }
        holder.dragAndDropIcon.setVisibility(View.VISIBLE);
        int iconSize = iconSizeCalc(config);
        LayoutParams layoutParams = new LayoutParams(iconSize, iconSize);
        layoutParams.setMargins(2, 0, 25, 0);
        holder.dragAndDropIcon.setLayoutParams(layoutParams);
    }

    private static void setSelectedTasksForEdit(Task task, ViewHolder holder, Task taskDecorator) {
        EditManager editManager = EditManager.getInstance();
        if (editManager.isTaskNeedColor(task)) {
            holder.listItem.setBackgroundColor(editManager.getTaskColor(task));
            if (!editManager.isFirstEditAction(EditActions.CUT)) {
                return;
            }
            if ((2 == task.getFontColor().intValue() || (task.getTasksList().isColorTasks().booleanValue() && 2 == getFontColor(task).getFontColor().intValue())) && !task.isCompleted().booleanValue()) {
                SpannableStringBuilder taskName = getTaskName(task);
                taskName.setSpan(new ForegroundColorSpan(-1), 0, taskName.length(), 0);
                holder.taskName.setText(taskName);
                return;
            }
            return;
        }
        formatForTaskSelected(task, holder, taskDecorator);
    }

    public static String trimToLength(TextPaint painter, String initialText, double width) {
        String message = initialText;
        String output = initialText;
        float currentWidth = painter.measureText(output);
        while (((double) currentWidth) > width) {
            message = message.substring(0, message.length() - 1);
            output = message + "...";
            currentWidth = painter.measureText(output);
        }
        return output;
    }

    private static void setNote(ViewHolder holder, Task task, Context context) {
        ConfigManager config = ConfigManager.getInstance();
        if (!task.isTasksListAsTask() && ((task.getTasksList().isShowNote().booleanValue() || task.isShowNote().booleanValue()) && !StringUtil.isNullOrEmpty(task.getNote()))) {
            holder.textNote.setVisibility(View.VISIBLE);
            holder.textNote.setText(task.getNote());
            holder.textNote.setTextSize(((float) config.getTaskNameFontSize().intValue()) * NOTE_FONT_SIZE_MULTIPLIER);
            if (!(task.isShowNote().booleanValue() || config.getNoteVisibleLines().intValue() == 0)) {
                holder.textNote.setMaxLines(config.getNoteVisibleLines().intValue());
            }
            if (config.isLinkifyNotes()) {
                linkifyNote(holder);
            }
            holder.note.setVisibility(View.GONE);
        } else if (config.isDragAndDropMode() || StringUtil.isNullOrEmpty(task.getNote())) {
            holder.note.setVisibility(View.GONE);
            holder.textNote.setVisibility(View.GONE);
        } else {
            holder.note.setVisibility(View.VISIBLE);
            holder.textNote.setVisibility(View.GONE);
            holder.note.setImageBitmap(holder.noteIcon);
            int iconSize = iconSizeCalc(config) - 10;
            if (iconSize > 29) {
                iconSize = 29;
            }
            holder.note.setLayoutParams(new LayoutParams(iconSize, iconSize));
        }
    }

    private static void setAlarmClock(ViewHolder holder, Task task, Context context) {
        ConfigManager config = ConfigManager.getInstance();
        if (task.isCompleted().booleanValue() || !task.isReminderEnabled().booleanValue() || (AppUtil.getTodayDateTimeMilisecond() > task.getReminderDateTime().longValue() && task.getSnoozeDateTime().longValue() <= 0)) {
            holder.alarmClock.setVisibility(View.GONE);
            return;
        }
        holder.alarmClock.setVisibility(View.VISIBLE);
        if (task.isNotifyByEmail().booleanValue()) {
            if (task.getSnoozeDateTime().longValue() <= 0) {
                holder.alarmClock.setImageBitmap(holder.emailAlarmIcon);
            } else if (AppUtil.isSnoozeEmailAllowed(task)) {
                holder.alarmClock.setImageBitmap(holder.emailAlarmSnoozeIcon);
            } else {
                holder.alarmClock.setImageBitmap(holder.alarmSnoozeIcon);
            }
        } else if (task.getSnoozeDateTime().longValue() > 0) {
            holder.alarmClock.setImageBitmap(holder.alarmSnoozeIcon);
        } else {
            holder.alarmClock.setImageBitmap(holder.alarmIcon);
        }
        int iconSize = iconSizeCalc(config) - 10;
        if (iconSize > 29) {
            iconSize = 29;
        }
        holder.alarmClock.setLayoutParams(new LayoutParams(iconSize, iconSize));
    }

    private static void setRepeatFloat(ViewHolder holder, Task task, Context context) {
        ConfigManager config = ConfigManager.getInstance();
        if (task.isFloat().booleanValue() || task.isRecurringTask()) {
            holder.repeatFloatImageView.setVisibility(View.VISIBLE);
            if (task.isFloat().booleanValue() && task.isRecurringTask()) {
                holder.repeatFloatImageView.setImageBitmap(holder.repeatFloatIcon);
            } else if (task.isRecurringTask()) {
                holder.repeatFloatImageView.setImageBitmap(holder.repeatIcon);
            } else {
                holder.repeatFloatImageView.setImageBitmap(holder.floatIcon);
            }
            int iconSize = iconSizeCalc(config) - 10;
            if (iconSize > 29) {
                iconSize = 29;
            }
            holder.repeatFloatImageView.setLayoutParams(new LayoutParams(iconSize, iconSize));
            return;
        }
        holder.repeatFloatImageView.setVisibility(View.GONE);
    }

    private static void linkifyNote(ViewHolder holder) {
        SpannableString spannableTextNote = SpannableString.valueOf(holder.textNote.getText());
        Linkify.addLinks(spannableTextNote, Linkify.ALL);
        holder.textNote.setText(spannableTextNote);
        holder.textNote.setOnTouchListener(mLinkHandler);
    }

    private static void setDoneCheckbox(ViewHolder holder, Task task) {
        ConfigManager config = ConfigManager.getInstance();
        holder.isCompleted.setChecked(task.isCompleted().booleanValue());
        if (!config.isShowDoneCheckbox() || task.isTasksListAsTask()) {
            holder.isCompleted.setVisibility(View.GONE);
        } else {
            holder.isCompleted.setVisibility(View.VISIBLE);
        }
    }

    private static ViewHolder initViewHolder(View convertView, Context context) {
        ViewHolder holder = new ViewHolder();
        holder.emailAlarmSnoozeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_email_alarm_snooze);
        holder.alarmSnoozeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_reminder_snooze);
        holder.repeatIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_recur);
        holder.repeatFloatIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_recur_float);
        holder.emailAlarmIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_email_alarm);
        holder.floatIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_float);
        holder.repeatFloatImageView = (ImageView) convertView.findViewById(R.id.float_icon);
        holder.noteIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.note);
        holder.alarmIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_reminder);
        holder.plusIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.plus);
        holder.minusIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.minus);
        holder.bulletIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.bullet);
        holder.indentLayout = (LinearLayout) convertView.findViewById(R.id.indent_layout);
        holder.taskName = (TextView) convertView.findViewById(R.id.task_name);
        holder.plusMinusDot = (TextView) convertView.findViewById(R.id.plus_minus_dot);
        holder.isCompleted = (CheckBox) convertView.findViewById(R.id.done_checkbox);
        holder.note = (ImageView) convertView.findViewById(R.id.note);
        holder.alarmClock = (ImageView) convertView.findViewById(R.id.alarm_clock);
        holder.listItem = (LinearLayout) convertView.findViewById(R.id.list_item);
        holder.taskNumber = (TextView) convertView.findViewById(R.id.task_number);
        holder.plusMinusDotIcon = (ImageView) convertView.findViewById(R.id.plus_minus_dot_img);
        holder.dragAndDropIcon = (ImageView) convertView.findViewById(R.id.ImageView01);
        holder.textNote = (TextView) convertView.findViewById(R.id.text_note);
        holder.dueDate = (TextView) convertView.findViewById(R.id.due_date);
        holder.dueDate2 = (TextView) convertView.findViewById(R.id.due_date2);
        return holder;
    }

    private static void setIndentLayout(Task task, ViewHolder holder, LevelState levelState) {
        int zoomLevel = 0;
        if (!StringUtil.isNullOrEmpty(levelState.getZoomTaskIdStr())) {
            zoomLevel = levelState.getZoomTask().getLevel().intValue();
        }
        int initialIndent = ((task.getLevel().intValue() - 1) - zoomLevel) * 30;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.setMargins(initialIndent, 0, 5, 0);
        holder.indentLayout.setLayoutParams(layoutParams);
    }

    private static void setPlusMinusDot(Task task, ViewHolder holder, LevelState levelState, Map<String, Long> expandedTasks) {
        ConfigManager config = ConfigManager.getInstance();
        SpannableStringBuilder plusMinusDots = new SpannableStringBuilder();
        int iconSize = iconSizeCalc(config);
        if (task.isParent()) {
            if (isCollapsed(task, expandedTasks) || (!isExpanded(task, expandedTasks) && (levelState.getLevel().equals(task.getLevel()) || (levelState.getLevel().compareTo(task.getLevel()) < 1 && !task.isTasksListAsTask())))) {
                plusMinusDots.append("+");
                holder.plusMinusDotIcon.setImageBitmap(holder.plusIcon);
            } else {
                plusMinusDots.append("-");
                holder.plusMinusDotIcon.setImageBitmap(holder.minusIcon);
            }
            plusMinusDots.setSpan(new StyleSpan(1), 0, plusMinusDots.length(), 0);
        } else {
            plusMinusDots.append("•");
            holder.plusMinusDotIcon.setImageBitmap(holder.bulletIcon);
        }
        holder.plusMinusDot.setText(plusMinusDots);
        holder.plusMinusDot.setTextSize((float) config.getTaskNameFontSize().intValue());
        holder.plusMinusDotIcon.setLayoutParams(new LinearLayout.LayoutParams(iconSize, iconSize));
        if (config.getTaskNameFontSize().intValue() > 19) {
            holder.isCompleted.setLayoutParams(new LinearLayout.LayoutParams(108, 100));
            holder.isCompleted.setButtonDrawable(R.drawable.btn_check_selector_32);
        } else if (config.getTaskNameFontSize().intValue() >= 16) {
            holder.isCompleted.setLayoutParams(new LinearLayout.LayoutParams(74, 70));
            holder.isCompleted.setButtonDrawable(R.drawable.btn_check_selector);
        } else {
            if (iconSize < 35) {
                iconSize = 55;
            }
            holder.isCompleted.setLayoutParams(new LinearLayout.LayoutParams(iconSize + 4, iconSize));
            holder.isCompleted.setButtonDrawable(R.drawable.btn_check_selector_19);
        }
    }

    private static int iconSizeCalc(ConfigManager config) {
        return config.getTaskNameFontSize().intValue() * 2;
    }

    private static boolean isCollapsed(Task task, Map<String, Long> expandedTasks) {
        return AppUtil.isExpanded(task, expandedTasks, Constants.TASK_COLLAPSE_INDICATOR);
    }

    private static boolean isExpanded(Task task, Map<String, Long> expandedTasks) {
        return AppUtil.isExpanded(task, expandedTasks, Constants.TASK_EXPAND_INDICATOR);
    }

    private static void setTaskNumber(Task task, ViewHolder holder) {
        ConfigManager config = ConfigManager.getInstance();
        if (config.isShowTaskNumbers()) {
            holder.taskNumber.setVisibility(View.VISIBLE);
            holder.taskNumber.setText(new SpannableStringBuilder(task.getTaskNumber() + Globals.PERIOD));
            holder.taskNumber.setTextSize((float) (config.getTaskNameFontSize().intValue() - 3));
        }
    }

    private static void setTaskName(Task task, ViewHolder holder) {
        ConfigManager config = ConfigManager.getInstance();
        SpannableStringBuilder taskName = getTaskName(task);
        formatTaskNameFromLevelColor(task, taskName);
        formatTaskNameFromDecorator(task, taskName, true);
        formatForTasksListName(task, taskName);
        holder.taskName.setText(taskName);
        holder.taskName.setTextSize((float) config.getTaskNameFontSize().intValue());
    }

    private static void formatTaskNameFromLevelColor(Task task, SpannableStringBuilder taskName) {
        if (!task.isTasksListAsTask() && task.getTasksList().isColorTasks().booleanValue()) {
            int fontColor = getFontColor(task).getFontColor().intValue();
            if (fontColor != 0) {
                taskName.setSpan(new ForegroundColorSpan(EditTaskHelper.getFontColor(fontColor, fontColor)), 0, taskName.length(), 0);
            }
            AppUtil.setFontStyle(taskName, task);
        }
    }

    private static LevelColor getFontColor(Task task) {
        ConfigManager config = ConfigManager.getInstance();
        int taskLevel = task.getLevel().intValue();
        if (config.getLastSelectedTasksList().equals(DatabaseHelper.ALL_TASKS_LIST_ID)) {
            taskLevel--;
        }
        return config.getLevelColor(task.getTasksListId(), taskLevel);
    }

    private static void formatForTaskSelected(Task task, ViewHolder holder, Task taskDecorator) {
        ConfigManager config = ConfigManager.getInstance();
        if (task.isTasksListAsTask() && task.getTasksListId().equals(taskDecorator.getTasksListId()) && taskDecorator.isSelected()) {
            holder.listItem.setBackgroundColor(Constants.COLOR_SELECTED_TASK_YELLOW);
        } else if (task.getId().equals(taskDecorator.getId()) && taskDecorator.isSelected()) {
            holder.listItem.setBackgroundColor(Constants.COLOR_SELECTED_TASK_YELLOW);
        } else {
            holder.listItem.setBackgroundDrawable(config.getContext().getResources().getDrawable(R.drawable.list_selector_background_normal));
        }
    }

    private static SpannableStringBuilder getTaskName(Task task) {
        SpannableStringBuilder taskName = new SpannableStringBuilder("");
        if (!StringUtil.isNullOrEmpty(task.getName())) {
            taskName.append(task.getName());
        }
        return taskName;
    }

    private static void formatForTasksListName(Task task, SpannableStringBuilder taskName) {
        if (task.isTasksListAsTask()) {
            taskName.setSpan(new StyleSpan(1), 0, taskName.length(), 0);
        }
    }

    private static void formatTaskNameFromDecorator(Task task, SpannableStringBuilder taskName, boolean isOverrideColor) {
        if (task.isCompleted().booleanValue()) {
            taskName.setSpan(new StrikethroughSpan(), 0, taskName.length(), 0);
            taskName.setSpan(new ForegroundColorSpan(Color.parseColor(Constants.DONE_COLOR)), 0, taskName.length(), 0);
        } else if (task.isHighlight().booleanValue()) {
            switch (ConfigManager.getInstance().getHighlightStyle().intValue()) {
                case 0:
                    taskName.setSpan(new BackgroundColorSpan(-256), 0, taskName.length(), 0);
                    return;
                case 1:
                    if (isOverrideColor) {
                        //taskName.setSpan(new ForegroundColorSpan(-65536), 0, taskName.length(), 0);
                        return;
                    }
                    return;
                case 2:
                    taskName.setSpan(new StyleSpan(1), 0, taskName.length(), 0);
                    if (isOverrideColor) {
                        //taskName.setSpan(new ForegroundColorSpan(-65536), 0, taskName.length(), 0);
                        return;
                    }
                    return;
                default:
                    return;
            }
        } else if (task.getFontStyle().intValue() != 0 || task.getFontColor().intValue() != 0 || task.getFontBgColor().intValue() != 0) {
            AppUtil.setFontStyle(taskName, task);
            if (isOverrideColor && task.getFontColor().intValue() != 0) {
                AppUtil.setFontForegroundColor(taskName, task);
            }
            if (task.getFontBgColor().intValue() != 0) {
                AppUtil.setFontBackgroundColor(taskName, task);
            }
        }
    }
}
