package com.irt.tasks.ui.search;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.text.TextUtils;
import com.irt.tasks.app.businessobjects.LevelState;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.businessprocess.TaskStateBp;
import com.irt.tasks.app.businessprocess.TaskStateBpImpl;
import com.irt.tasks.app.businessprocess.TasksListBp;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.CSVWriter;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.List;

public class SearchProvider extends ContentProvider {
    public static String AUTHORITY = "irt_gtasks_outliner_search";
    private static final String[] COLUMNS = new String[]{"_id", "suggest_text_1", "suggest_text_2", "suggest_intent_data"};
    private static final int SEARCH_SUGGEST = 0;
    private static final int SHORTCUT_REFRESH = 1;
    public static List<Task> mTasks;
    private static final UriMatcher sURIMatcher = buildUriMatcher();
    ConfigManager config;
    private TaskBp taskBp = new TaskBpImpl();
    private TaskStateBp taskStateBp = new TaskStateBpImpl();
    private TasksListBp tasksListBp = new TasksListBpImpl();

    private static UriMatcher buildUriMatcher() {
        UriMatcher matcher = new UriMatcher(-1);
        matcher.addURI(AUTHORITY, "search_suggest_query", 0);
        matcher.addURI(AUTHORITY, "search_suggest_query/*", 0);
        matcher.addURI(AUTHORITY, "search_suggest_shortcut", 1);
        matcher.addURI(AUTHORITY, "search_suggest_shortcut/*", 1);
        return matcher;
    }

    public boolean onCreate() {
        Resources resources = getContext().getResources();
        this.config = ConfigManager.getInstance();
        return true;
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        this.config = ConfigManager.getInstance();
        if (!TextUtils.isEmpty(selection)) {
            throw new IllegalArgumentException("selection not allowed for " + uri);
        } else if (selectionArgs != null && selectionArgs.length != 0) {
            throw new IllegalArgumentException("selectionArgs not allowed for " + uri);
        } else if (TextUtils.isEmpty(sortOrder)) {
            switch (sURIMatcher.match(uri)) {
                case 0:
                    String query = null;
                    if (uri.getPathSegments().size() > 1) {
                        query = uri.getLastPathSegment().toLowerCase();
                    }
                    return getSuggestions(query, projection);
                case 1:
                    String shortcutId = null;
                    if (uri.getPathSegments().size() > 1) {
                        shortcutId = uri.getLastPathSegment();
                    }
                    return refreshShortcut(shortcutId, projection);
                default:
                    throw new IllegalArgumentException("Unknown URL " + uri);
            }
        } else {
            throw new IllegalArgumentException("sortOrder not allowed for " + uri);
        }
    }

    private Cursor getSuggestions(String query, String[] projection) {
        List<Task> tasks = searchForTasks(query);
        MatrixCursor cursor = new MatrixCursor(COLUMNS);
        long id = 0;
        for (Task task : tasks) {
            long id2 = id + 1;
            cursor.addRow(columnValuesOfEvent(id, task));
            id = id2;
        }
        return cursor;
    }

    private Object[] columnValuesOfEvent(long id, Task task) {
        Object[] objArr = new Object[4];
        objArr[0] = Long.valueOf(id);
        objArr[1] = task.getName();
        objArr[2] = getTaskPath(task);
        objArr[3] = task.isTasksListAsTask() ? Constants.TASK_LIST + task.getTasksListId() : task.getId();
        return objArr;
    }

    private String getTaskPath(Task task) {
        if (task.isTasksListAsTask()) {
            return "";
        }
        return task.getNote() + CSVWriter.DEFAULT_LINE_END + task.getTasksListName() + task.getTaskPath() + (this.config.isShowTaskNumbers() ? task.getTaskNumber() : "");
    }

    private List<Task> searchForTasks(String query) {
        if (mTasks == null) {
            TasksList tasksList;
            if (DatabaseHelper.ALL_TASKS_LIST_ID.equals(this.config.getLastSelectedTasksList())) {
                tasksList = DatabaseHelper.createTasksListCalledAll();
            } else {
                tasksList = this.tasksListBp.get(this.config.getLastSelectedTasksList());
            }
            mTasks = this.taskBp.getTasksByLevel(null, 20, true, tasksList, true);
            LevelState levelState = this.taskStateBp.getLevelSate(tasksList.getId(), null);
            if (AppUtil.isZoomSet(levelState)) {
                mTasks = this.taskBp.getTaskChildrenAndGrandChildren(levelState.getZoomTask(), mTasks);
            }
        }
        return this.taskBp.searchAllTasks(query, mTasks);
    }

    private Cursor refreshShortcut(String shortcutId, String[] projection) {
        return null;
    }

    public String getType(Uri uri) {
        switch (sURIMatcher.match(uri)) {
            case 0:
                return "vnd.android.cursor.dir/vnd.android.search.suggest";
            case 1:
                return "vnd.android.cursor.item/vnd.android.search.suggest";
            default:
                throw new IllegalArgumentException("Unknown URL " + uri);
        }
    }

    public Uri insert(Uri uri, ContentValues values) {
        throw new UnsupportedOperationException();
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }
}
