package com.irt.tasks.ui.taskslist;

import android.app.AlertDialog.Builder;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;
import com.google.android.gms.drive.DriveFile;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.SyncBp;
import com.irt.tasks.app.businessprocess.SyncBpImpl;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.About;
import com.irt.tasks.ui.ReleaseNotes;
import com.irt.tasks.ui.admin.AdminDatabaseActivity;
import com.irt.tasks.ui.calendar.CalendarAdActivity;
import com.irt.tasks.ui.preferences.TasksPreferences;

public class OptionsMenuHelper {
    public static final String DECORATOR_TASK_ID = "decoratorTaskId";
    public static final String VIEW_TYPE = "viewType";

    static class C19425 implements OnClickListener {
        C19425() {
        }

        public void onClick(DialogInterface dialog, int arg1) {
        }
    }

    static class C19436 implements OnClickListener {
        C19436() {
        }

        public void onClick(DialogInterface dialog, int arg1) {
        }
    }

    public static class MonitorSync extends AsyncTask<Void, Void, Long> {
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            ConfigManager config = ConfigManager.getInstance();
            if (!StringUtil.isNullOrEmpty(config.getCurrentTasksListBeingSynced()) && config.getCurrentContext() != null && (config.getCurrentContext() instanceof AbstractMainListActivity)) {
                ((AbstractMainListActivity) config.getCurrentContext()).mTitle.setText(AppUtil.getMaxWindowTitle(config.getCurrentTasksListBeingSynced()));
            }
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected void onPostExecute(Long result) {
            super.onPostExecute(result);
        }

        protected Long doInBackground(Void... params) {
            ConfigManager config = ConfigManager.getInstance();
            int loopCounter = 0;
            while (config.isSyncing() && loopCounter < 5400) {
                AppUtil.delay(1000);
                publishProgress(new Void[0]);
                loopCounter++;
            }
            return Long.valueOf(0);
        }
    }

    public static class SyncWithGoogleTasks extends AsyncTask<Void, Void, Boolean> {
        Context mContext;
        TasksList mTasksList = null;
        SyncBp syncBp = new SyncBpImpl();

        public SyncWithGoogleTasks(Context context, TasksList tasksList) {
            this.mContext = context;
            this.mTasksList = tasksList;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            ConfigManager.getInstance().setCurrentTasksListBeingSynced(Constants.SYNC_TASKS_LIST_NAME_AT_START);
            AppUtil.refreshDisplay();
        }

        protected void onPostExecute(Boolean isSuccessful) {
            super.onPostExecute(isSuccessful);
            if (!isSuccessful.booleanValue()) {
                ConfigManager config = ConfigManager.getInstance();
                config.setCurrentTasksListBeingSynced(null);
                Toast.makeText(this.mContext, "ERROR: Network not available, please try again.", Toast.LENGTH_LONG).show();
                config.setSyncing(false);
            }
            AppUtil.refreshDisplay();
        }

        protected Boolean doInBackground(Void... params) {
            boolean isSucessful = false;
            synchronized (this) {
                if (!ConfigManager.getInstance().isSyncing()) {
                    if (this.mTasksList == null) {
                        isSucessful = this.syncBp.syncWithGoogleTasks();
                    } else {
                        isSucessful = this.syncBp.syncWithGoogleTasks(this.mTasksList);
                    }
                }
            }
            return Boolean.valueOf(isSucessful);
        }
    }

    public static boolean onOptionsItemSelected(final Context context, int selectionOtionsMenuItem, MenuItem item) {
        ConfigManager config = ConfigManager.getInstance();
        Intent intent;
        switch (selectionOtionsMenuItem) {
            case R.id.menu_preferences:
                context.startActivity(new Intent(context, TasksPreferences.class));
                return true;
            case R.id.menu_admin:
                context.startActivity(new Intent(context, AdminDatabaseActivity.class));
                return true;
            case R.id.menu_clear_recycle_bin:
                int totalRecordsDeleted = new TasksListBpImpl().clearTrashBin();
                AppUtil.refreshDisplay();
                Toast.makeText(context, "Cleared " + totalRecordsDeleted + " records from recycle bin", Toast.LENGTH_LONG).show();
                return true;
            case R.id.menu_user_guide:
                intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("http://www.irtgtasksoutliner.com/userguide/"));
                context.startActivity(intent);
                return true;
            case R.id.menu_faq:
                intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("http://www.irtgtasksoutliner.com/faq/"));
                context.startActivity(intent);
                return true;
            case R.id.menu_release_notes:
                showReleaseNotes(context);
                return true;
            case R.id.menu_about:
                setAboutContent(context).show();
                return true;
            case R.id.menu_tasks_list_views:
                showViews(context, true);
                return false;
            case R.id.menu_tasks_list_search:
                ((TasksListHomeActivity) context).onSearchRequested();
                return true;
            case R.id.menu_irt_calendar:
                if (AppUtil.isIRTCalendarInstalled()) {
                    intent = new Intent("android.intent.action.MAIN");
                    if (AppUtil.isIRTCalendarAvailable(context)) {
                        intent.setComponent(ComponentName.unflattenFromString("com.irt.calendar.app.ui/com.irt.calendar.app.ui.day.DayActivity"));
                    } else {
                        intent.setComponent(ComponentName.unflattenFromString("com.irt.calendar.app.trial/com.irt.calendar.app.trial.day.DayActivity"));
                    }
                    intent.addCategory("android.intent.category.LAUNCHER");
                    intent.addFlags(DriveFile.MODE_READ_ONLY);
                    context.startActivity(intent);
                } else {
                    context.startActivity(new Intent(context, CalendarAdActivity.class));
                }
                return true;
            case R.id.menu_tasks_list_actions:
                showHomeActions(context);
                return true;
            case R.id.menu_tasks_list_new:
                ArchitectureContext.setObject(new TasksList());
                context.startActivity(new Intent(context, TasksListEditActivity.class));
                return false;
            case R.id.menu_views:
                showViews(context, false);
                return true;
            case R.id.menu_actions:
                showActions(context);
                return true;
            case R.id.menu_panel:
                new Builder(context).setTitle("Select panel").setSingleChoiceItems(R.array.panel_types, config.getPanelToShow().intValue(), new OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ConfigManager.getInstance().setPanelToShow(Integer.valueOf(whichButton));
                        //context.setPlusImage();
                        dialog.dismiss();
                    }
                }).show();
                return true;
            default:
                return false;
        }
    }

    public static void showViews(final Context context, final boolean isViewsFromHome) {
        ConfigManager config = ConfigManager.getInstance();
        String[] items = context.getResources().getStringArray(R.array.tasks_lists_views);
        for (int i = 0; i < items.length; i++) {
            if (i == 0) {
                if (config.isShowAllTasksOnFilter()) {
                    items[i] = "Filter (All)";
                } else {
                    items[i] = "Filter";
                }
            }
        }
        new Builder(context).setTitle(R.string.select_view).setItems(items, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                OptionsMenuHelper.performGotoView(context, which, isViewsFromHome);
            }
        }).show();
    }

    private static void performGotoView(Context context, int which, boolean isViewsFromHome) {
        Intent intent = new Intent(context, TasksListViewActivity.class);
        intent.putExtra(VIEW_TYPE, which);
        if (isViewsFromHome) {
            ConfigManager config = ConfigManager.getInstance();
            TasksList tasksList = DatabaseHelper.createTasksListCalledAll();
            ArchitectureContext.setObject(tasksList);
            config.setLastSelectedTasksList(tasksList.getId());
            intent.putExtra(Constants.IS_VIEW_FROM_HOME, Constants.TRUE);
        } else {
            intent.putExtra(DECORATOR_TASK_ID, ((AbstractTasksListActivity) context).mTaskDecorator.getId());
        }
        context.startActivity(intent);
    }

    public static void showHomeActions(final Context context) {
        ConfigManager config = ConfigManager.getInstance();
        String[] items = context.getResources().getStringArray(R.array.tasks_lists_home_actions);
        for (int i = 0; i < items.length; i++) {
            if (i == 0) {
                if (config.isShowTasksListStatistics()) {
                    items[i] = "Statistics, Hide for speed";
                } else {
                    items[i] = "Statistics, Show";
                }
            }
        }
        new Builder(context).setTitle(R.string.select_action).setItems(items, new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichAction) {
                //context.performAction(whichAction);
            }
        }).show();
    }

    public static void showActions(final Context context) {
        ConfigManager config = ConfigManager.getInstance();
        AbstractTasksListActivity abstractTasksListActivity = (AbstractTasksListActivity) context;
        boolean isAllTasksList = DatabaseHelper.ALL_TASKS_LIST_ID.equals(abstractTasksListActivity.mTasksList.getId());
        String[] items = context.getResources().getStringArray(R.array.tasks_lists_actions);
        SpannableStringBuilder[] actionSpan = new SpannableStringBuilder[items.length];
        int i = 0;
        while (i < actionSpan.length) {
            actionSpan[i] = new SpannableStringBuilder(items[i]);
            if (i == 1) {
                if (config.getRightTapAction().intValue() == 30) {
                    actionSpan[i] = new SpannableStringBuilder("Edit, Right tap (Note)");
                } else {
                    actionSpan[i] = new SpannableStringBuilder("Edit, Right tap (Task)");
                }
            }
            if (isAllTasksList && (i == 6 || i == 7 || i == 12 || i == 5 || i == 4)) {
                grayTheAction(actionSpan[i]);
            }
            if (DatabaseHelper.OFFLINE_ACCOUNT_ID.equals(abstractTasksListActivity.mTasksList.getAccountId()) && (i == 6 || i == 7 || i == 13)) {
                grayTheAction(actionSpan[i]);
            }
            if (abstractTasksListActivity.mTasksList.istActiveTasksList().booleanValue() && i == 6) {
                grayTheAction(actionSpan[i]);
            }
            i++;
        }
        new Builder(context).setTitle(R.string.select_action).setItems(actionSpan, new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichAction) {
                OptionsMenuHelper.performAction(context, whichAction);
            }
        }).show();
    }

    private static void grayTheAction(SpannableStringBuilder spannableStringBuilder) {
        spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.parseColor("#DFDFDF")), 0, spannableStringBuilder.length(), 0);
    }

    public static void performAction(Context context, int whichAction) {
        if (context instanceof AbstractTasksListActivity) {
            ((AbstractTasksListActivity) context).performAction(whichAction);
        }
    }

    public static void showReleaseNotes(Context context) {
        StringBuffer content = ReleaseNotes.getReleaseNotes();
        View convertView = LayoutInflater.from(context).inflate(R.layout.web_style_dialog, null);
        WebView webView = (WebView) convertView.findViewById(R.id.wv1);
        Builder alertDialogBuilder = new Builder(context);
        webView.loadData(content.toString(), "text/html", Constants.encoding);
        alertDialogBuilder.setIcon(R.drawable.icon);
        alertDialogBuilder.setTitle("Release Notes");
        alertDialogBuilder.setView(convertView);
        alertDialogBuilder.setPositiveButton("OK", new C19425());
        alertDialogBuilder.show();
    }

    private static Builder setAboutContent(Context context) {
        StringBuffer content = About.getAbout(AppUtil.getVersionName());
        View convertView = LayoutInflater.from(context).inflate(R.layout.web_style_dialog, null);
        WebView webView = (WebView) convertView.findViewById(R.id.wv1);
        Builder alertDialogBuilder = new Builder(context);
        webView.loadData(content.toString(), "text/html", Constants.encoding);
        alertDialogBuilder.setIcon(R.drawable.icon);
        alertDialogBuilder.setTitle("About " + AppUtil.getAppName());
        alertDialogBuilder.setView(convertView);
        alertDialogBuilder.setPositiveButton("OK", new C19436());
        return alertDialogBuilder;
    }
}
