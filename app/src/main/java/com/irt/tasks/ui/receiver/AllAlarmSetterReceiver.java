package com.irt.tasks.ui.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AllAlarmSetterReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        AlarmSetterHelper.resetAllAlarms(context);
    }
}
