package com.irt.tasks.ui.taskslist;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.AccountBp;
import com.irt.tasks.app.businessprocess.AccountBpImpl;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.businessprocess.TasksListBp;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.ErrorReporter;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.app.framework.trial.TrialSpecificUtility;
import com.irt.tasks.ui.help.HelpUtility;
import com.irt.tasks.ui.help.HelpUtility.HelpDialog;
import com.irt.tasks.ui.search.SearchProvider;
import com.irt.tasks.ui.taskslist.OptionsMenuHelper.MonitorSync;
import com.irt.tasks.ui.taskslist.OptionsMenuHelper.SyncWithGoogleTasks;
import java.util.ArrayList;
import java.util.List;

public class TasksListHomeActivity extends AbstractMainListActivity {
    private static final int ACTION_TREE_VIEW = 0;
    protected static int mFirstVisibleEvent = 0;
    AccountBp accountBp = new AccountBpImpl();
    private GestureDetector gestureDetector;
    SpannableStringBuilder[] mAccountEmails;
    List<Account> mAccounts;
    Button mAgendaButton;
    Button mCreatedButton;
    Button mFiltersButton;
    ImageView mHelpImageView;
    boolean mIsLongPress = false;
    boolean mIsRightTap = false;
    private boolean mIsSelectedTaskPositionSet = false;
    Button mModifiedButton;
    ImageView mPlusImageView;
    int mSelectedPosition = 0;
    private int mSelectedTaskPosition = 0;
    Button mSnoozedButton;
    private long mTapTime = 0;
    List<TasksList> mTasksLists;
    TaskBp taskBp = new TaskBpImpl();
    TasksListBp tasksListBp = new TasksListBpImpl();

    class C19611 implements OnClickListener {
        C19611() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(TasksListHomeActivity.this, TasksListViewActivity.class);
            intent.putExtra(OptionsMenuHelper.VIEW_TYPE, 18);
            TasksList tasksList = DatabaseHelper.createTasksListCalledAll();
            ArchitectureContext.setObject(tasksList);
            ArchitectureContext.setObjectExtra(TasksListHomeActivity.this.mTasksLists);
            TasksListHomeActivity.this.config.setLastSelectedTasksList(tasksList.getId());
            intent.putExtra(Constants.IS_VIEW_FROM_HOME, Constants.TRUE);
            TasksListHomeActivity.this.startActivity(intent);
        }
    }

    class C19622 implements OnClickListener {
        C19622() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(TasksListHomeActivity.this, TasksListViewActivity.class);
            intent.putExtra(OptionsMenuHelper.VIEW_TYPE, 0);
            TasksList tasksList = DatabaseHelper.createTasksListCalledAll();
            ArchitectureContext.setObject(tasksList);
            ArchitectureContext.setObjectExtra(TasksListHomeActivity.this.mTasksLists);
            TasksListHomeActivity.this.config.setLastSelectedTasksList(tasksList.getId());
            intent.putExtra(Constants.IS_VIEW_FROM_HOME, Constants.TRUE);
            TasksListHomeActivity.this.startActivity(intent);
        }
    }

    class C19633 implements OnClickListener {
        C19633() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(TasksListHomeActivity.this, TasksListViewActivity.class);
            intent.putExtra(OptionsMenuHelper.VIEW_TYPE, 19);
            TasksList tasksList = DatabaseHelper.createTasksListCalledAll();
            ArchitectureContext.setObject(tasksList);
            ArchitectureContext.setObjectExtra(TasksListHomeActivity.this.mTasksLists);
            TasksListHomeActivity.this.config.setLastSelectedTasksList(tasksList.getId());
            intent.putExtra(Constants.IS_VIEW_FROM_HOME, Constants.TRUE);
            TasksListHomeActivity.this.startActivity(intent);
        }
    }

    class C19644 implements OnClickListener {
        C19644() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(TasksListHomeActivity.this, TasksListViewActivity.class);
            intent.putExtra(OptionsMenuHelper.VIEW_TYPE, 17);
            TasksList tasksList = DatabaseHelper.createTasksListCalledAll();
            ArchitectureContext.setObject(tasksList);
            ArchitectureContext.setObjectExtra(TasksListHomeActivity.this.mTasksLists);
            TasksListHomeActivity.this.config.setLastSelectedTasksList(tasksList.getId());
            intent.putExtra(Constants.IS_VIEW_FROM_HOME, Constants.TRUE);
            TasksListHomeActivity.this.startActivity(intent);
        }
    }

    class C19655 implements OnClickListener {
        C19655() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(TasksListHomeActivity.this, TasksListViewActivity.class);
            intent.putExtra(OptionsMenuHelper.VIEW_TYPE, 1);
            TasksList tasksList = DatabaseHelper.createTasksListCalledAll();
            ArchitectureContext.setObject(tasksList);
            ArchitectureContext.setObjectExtra(TasksListHomeActivity.this.mTasksLists);
            TasksListHomeActivity.this.config.setLastSelectedTasksList(tasksList.getId());
            intent.putExtra(Constants.IS_VIEW_FROM_HOME, Constants.TRUE);
            TasksListHomeActivity.this.startActivity(intent);
        }
    }

    class C19666 implements OnClickListener {
        C19666() {
        }

        public void onClick(View v) {
            HelpUtility.showHelp(TasksListHomeActivity.this, HelpDialog.TASKS_LIST_HOME);
        }
    }

    class C19677 implements OnClickListener {
        C19677() {
        }

        public void onClick(View v) {
            ArchitectureContext.setObject(TasksListHomeActivity.this.getTasksListForNewTask());
            Intent intent = new Intent(TasksListHomeActivity.this, TasksListActivity.class);
            intent.putExtra(Constants.IS_PLUS_NEW_TASK, Constants.TRUE);
            TasksListHomeActivity.this.startActivity(intent);
            TasksListHomeActivity.this.mPlusImageView.setVisibility(View.GONE);
        }
    }

    class C19688 implements OnItemClickListener {
        C19688() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            TasksListHomeActivity.this.mSelectedTaskPosition = position;
            TasksListHomeActivity.this.mIsSelectedTaskPositionSet = true;
            if (TasksListHomeActivity.this.isNextActionAllowed()) {
                TasksListHomeActivity.this.setTapTime();
                if (TasksListHomeActivity.this.mIsRightTap) {
                    TasksListHomeActivity.this.performEventFocusedAction(0);
                } else {
                    TasksListHomeActivity.this.performEventFocusedAction(0);
                }
                TasksListHomeActivity.this.mIsLongPress = false;
                return;
            }
            TasksListHomeActivity.this.resetTapTime();
        }
    }

    class C19699 implements OnTouchListener {
        C19699() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
            }
            if (TasksListHomeActivity.this.gestureDetector.onTouchEvent(event)) {
                return true;
            }
            switch (event.getAction()) {
                case 0:
                    TasksListHomeActivity.this.mIsRightTap = AppUtil.isRightTap(event.getX(), v);
                    break;
            }
            return false;
        }
    }

    class MyGestureDetector extends SimpleOnGestureListener {
        MyGestureDetector() {
        }

        public void onLongPress(MotionEvent e) {
            TasksListHomeActivity.this.mIsLongPress = true;
            super.onLongPress(e);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isTaskRoot()) {
            requestWindowFeature(1);
            setContentView(R.layout.tasks_list_home);
            this.config = ConfigManager.getInstance();
            this.config.setViewFromHome(Boolean.valueOf(true));
            if (this.config.getLastSelectedTasksList() == null) {
                this.config.setLastSelectedTasksList(Constants.HOME_TASKS_LIST_ID);
            }
            setPlusImage();
            setTitle();
            setHelpImage();
            setListOnTouchListener();
            setListListeners();
            setAgendaButton();
            setModifiedButton();
            setCreatedButton();
            setFiltersButton();
            setSnoozedButton();
            this.gestureDetector = new GestureDetector(new MyGestureDetector());
            setLocalOnScrollListener();
            return;
        }
        finish();
    }

    private void setModifiedButton() {
        this.mModifiedButton = (Button) findViewById(R.id.last_modified_button);
        this.mModifiedButton.setOnClickListener(new C19611());
    }

    private void setFiltersButton() {
        this.mFiltersButton = (Button) findViewById(R.id.filters_button);
        this.mFiltersButton.setOnClickListener(new C19622());
    }

    private void setCreatedButton() {
        this.mCreatedButton = (Button) findViewById(R.id.creation_date_button);
        this.mCreatedButton.setOnClickListener(new C19633());
    }

    private void setAgendaButton() {
        this.mAgendaButton = (Button) findViewById(R.id.agenda_button);
        this.mAgendaButton.setOnClickListener(new C19644());
    }

    private void setSnoozedButton() {
        this.mSnoozedButton = (Button) findViewById(R.id.snoozed_button);
        this.mSnoozedButton.setOnClickListener(new C19655());
    }

    private void setHelpImage() {
        this.mHelpImageView = (ImageView) findViewById(R.id.help_image);
        this.mHelpImageView.setOnClickListener(new C19666());
    }

    private void setTitle() {
        this.mTitle = (TextView) findViewById(R.id.title);
    }

    private void setPlusImage() {
        this.mPlusImageView = (ImageView) findViewById(R.id.plus);
        this.mPlusImageView.setOnClickListener(new C19677());
    }

    private void setPlusImageViewVisibilityBasedOnTasksListCount() {
        if (this.mTasksLists.size() < 2) {
            this.mPlusImageView.setVisibility(View.GONE);
        } else {
            this.mPlusImageView.setVisibility(View.VISIBLE);
        }
    }

    private TasksList getTasksListForNewTask() {
        if (Constants.TASKS_LIST_AUTO_ID.equals(this.config.getTasksListForNewTaskPlus())) {
            return getLastSelectedTasksList();
        }
        for (TasksList tasksList : this.mTasksLists) {
            if (tasksList.getId().equals(this.config.getTasksListForNewTaskPlus())) {
                return tasksList;
            }
        }
        this.config.setTasksListForNewTaskPlus(Constants.TASKS_LIST_AUTO_ID);
        return getLastSelectedTasksList();
    }

    private TasksList getLastSelectedTasksList() {
        for (TasksList tasksList : this.mTasksLists) {
            if (tasksList.getId().equals(this.config.getLastSelectedTasksList())) {
                return tasksList;
            }
        }
        return null;
    }

    private void setListListeners() {
        getListView().setOnItemClickListener(new C19688());
    }

    private void setTapTime() {
        this.mTapTime = AppUtil.getTodayDateTimeMilisecond();
    }

    private void resetTapTime() {
        this.mTapTime = 0;
    }

    private boolean isNextActionAllowed() {
        return AppUtil.getTodayDateTimeMilisecond() - this.mTapTime > TasksListGestureHelper.TAP_INTERVAL_ALLOWED;
    }

    private void performEventFocusedAction(int action) {
        switch (action) {
            case 0:
                ArchitectureContext.setObject(this.mTasksLists.get(this.mSelectedTaskPosition));
                startActivity(new Intent(this, TasksListActivity.class));
                this.mPlusImageView.setVisibility(View.GONE);
                return;
            default:
                return;
        }
    }

    private void setListOnTouchListener() {
        getListView().setOnTouchListener(new C19699());
    }

    private void showQuestionMarkMessage() {
        if (this.config.isShowQuestionMarkMessage()) {
            new Builder(this).setTitle("Help Information").setMessage("Context specific help is available on views that have a 'Question Mark' icon on the top right corner. Tap on the question mark icon to see help information for the view\n\nShow this message again?").setPositiveButton(R.string.alert_dialog_yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    TasksListHomeActivity.this.config.setShowQuestionMarkMessage(Boolean.valueOf(true));
                }
            }).setNegativeButton(R.string.alert_dialog_no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    TasksListHomeActivity.this.config.setShowQuestionMarkMessage(Boolean.valueOf(false));
                }
            }).show();
        }
    }

    private void initTasksLists() {
        this.mAccounts = this.accountBp.getAllVisibleAccounts();
        this.mAccountEmails = new SpannableStringBuilder[this.mAccounts.size()];
        for (int i = 0; i < this.mAccounts.size(); i++) {
            this.mAccountEmails[i] = new SpannableStringBuilder(((Account) this.mAccounts.get(i)).getEmail());
        }
        if (this.mTasksLists == null) {
            this.mTasksLists = new ArrayList();
        }
        this.mTasksLists.clear();
        addTasksListCalledAll(this.mTasksLists);
        int allTasksTotal = 0;
        int allTasksCompletedTotal = 0;
        for (TasksList tasksList : this.tasksListBp.getAllTasksListBySortOrder()) {
            if (this.config.isShowInvisibleAccounts().booleanValue() || tasksList.isVisible().booleanValue()) {
                tasksList.setAccount(getAccount(tasksList.getAccountId(), this.mAccounts));
                if (this.config.isShowTasksListStatistics()) {
                    int totalTasks = this.taskBp.countTasksByTasksListId(tasksList.getId(), false);
                    int totalCompletedTasks = this.taskBp.countTasksByTasksListId(tasksList.getId(), true);
                    tasksList.setTotals((totalTasks - totalCompletedTasks) + Globals.FORWARDSLASH + totalTasks);
                    allTasksTotal += totalTasks;
                    allTasksCompletedTotal += totalCompletedTasks;
                }
                this.mTasksLists.add(tasksList);
            }
        }
        if (this.config.isShowTasksListStatistics()) {
            ((TasksList) this.mTasksLists.get(0)).setTotals((allTasksTotal - allTasksCompletedTotal) + Globals.FORWARDSLASH + allTasksTotal);
        }
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        int position = ((AdapterContextMenuInfo) menuInfo).position;
        if (((TasksList) this.mTasksLists.get(position)).getId().equals(DatabaseHelper.ALL_TASKS_LIST_ID)) {
            menu.setHeaderTitle("Selection Option");
            menu.add(0, R.id.context_menu_add_list, 0, "    Add new list");
        } else {
            getMenuInflater().inflate(R.menu.list_context_menu, menu);
            hideMenuItem(menu, R.id.context_menu_add);
            hideMenuItem(menu, R.id.context_menu_paste);
            if (((TasksList) this.mTasksLists.get(position)).istActiveTasksList().booleanValue()) {
                hideMenuItem(menu, R.id.context_menu_delete_list);
            } else {
                hideMenuItem(menu, R.id.context_menu_clear_local_and_google_tasks);
            }
        }
        this.mIsLongPress = false;
    }

    private void hideMenuItem(ContextMenu menu, int menuItemId) {
        getMenuItem(menu, menuItemId).setVisible(false);
    }

    private MenuItem getMenuItem(Menu menu, int menuItemId) {
        for (int i = 0; i < menu.size(); i++) {
            MenuItem menuItem = menu.getItem(i);
            if (menuItem.getItemId() == menuItemId) {
                return menuItem;
            }
        }
        return null;
    }

    public boolean onContextItemSelected(MenuItem item) {
        this.mSelectedPosition = ((AdapterContextMenuInfo) item.getMenuInfo()).position;
        return ContextMenuHelper.onContextItemSelected(item, this, ((TasksList) this.mTasksLists.get(this.mSelectedPosition)).getName(), ((TasksList) this.mTasksLists.get(this.mSelectedPosition)).getId());
    }

    public void showCopyList() {
        ContextMenuHelper.showCopyList(((TasksList) this.mTasksLists.get(this.mSelectedPosition)).getId(), this.mAccounts, this.mAccountEmails, this);
    }

    private void addTasksListCalledAll(List<TasksList> tasksLists) {
        tasksLists.add(DatabaseHelper.createTasksListCalledAll());
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu_list, menu);
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        item.getItemId();
        return OptionsMenuHelper.onOptionsItemSelected(this, item.getItemId(), item);
    }

    public void performAction(int whichAction) {
        boolean z = true;
        switch (whichAction) {
            case 0:
                ConfigManager configManager = this.config;
                if (this.config.isShowTasksListStatistics()) {
                    z = false;
                }
                configManager.setShowTasksListStatistics(Boolean.valueOf(z));
                refreshDisplay();
                return;
            case 1:
                if (this.config.isSyncing()) {
                    Toast.makeText(this, "Sync is on going...", Toast.LENGTH_LONG).show();
                    return;
                }
                List<Account> accounts = this.accountBp.getAll();
                StringBuffer accountWithOldGoogleIds = new StringBuffer();
                for (Account account : accounts) {
                    if (!(account.isNewGoogleId().booleanValue() || account.getId() == DatabaseHelper.OFFLINE_ACCOUNT_ID)) {
                        accountWithOldGoogleIds.append("\n  - " + account.getEmail());
                    }
                }
                if (accountWithOldGoogleIds.length() > 1) {
                    new Builder(this).setTitle("Old Google Sync ID's").setMessage("The following Google accounts will not be synced because it uses old sync ID's." + accountWithOldGoogleIds + "\n\nPlease check Preferences -> Account Manager for details. " + "\n\nContinue with sync? ").setPositiveButton(R.string.alert_dialog_yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            new SyncWithGoogleTasks(TasksListHomeActivity.this, null).execute(new Void[0]);
                            new MonitorSync().execute(new Void[0]);
                        }
                    }).setNegativeButton(R.string.alert_dialog_no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    }).show();
                    return;
                }
                new SyncWithGoogleTasks(this, null).execute(new Void[0]);
                new MonitorSync().execute(new Void[0]);
                return;
            default:
                AppUtil.showGenericMessage(this, "TODO Message", "Sorry, this action is still a TODO.");
                return;
        }
    }

    private Account getAccount(Long accountId, List<Account> accounts) {
        for (Account account : accounts) {
            if (accountId.equals(account.getId())) {
                return account;
            }
        }
        return null;
    }

    public void refreshDisplay() {
        onResume();
    }

    protected void onResume() {
        super.onResume();
        this.config.setCurrentContext(this);
        ErrorReporter.CheckErrorAndSendMailCall(this);
        AppUtil.checkForProVersion();
        TrialSpecificUtility.checkTrialExpiration(this);
        setSelectedTaskFromSearch(getIntent());
        setWindowTitle();
        showQuestionMarkMessage();
        initTasksLists();
        setListAdapter(new TasksListHomeAdapter(this, this.mTasksLists));
        getListView().setSelection(mFirstVisibleEvent);
        setPlusImageViewVisibilityBasedOnTasksListCount();
        AppUtil.setMenuImage(this);
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        setSelectedTaskFromSearch(intent);
    }

    private void setSelectedTaskFromSearch(Intent intent) {
        if (!StringUtil.isNullOrEmpty(intent.getStringExtra(Constants.IS_SEARCH_FROM_HOME))) {
            onSearchRequested();
        }
        String str;
        if ("android.intent.action.VIEW".equals(getIntent().getAction()) && !StringUtil.isNullOrEmpty(intent.getDataString())) {
            String realId;
            TasksList searchedTasksList;
            str = "";
            str = intent.getDataString();
            boolean isTasksListAsTask = false;
            if (str.contains(Constants.TASK_LIST)) {
                realId = str.split(Constants.TASK_LIST)[1];
                isTasksListAsTask = true;
            } else {
                realId = str;
            }
            if (isTasksListAsTask) {
                searchedTasksList = this.tasksListBp.get(new Long(realId));
            } else {
                searchedTasksList = this.tasksListBp.get(this.taskBp.get(new Long(realId)).getTasksListId());
            }
            ArchitectureContext.setObject(searchedTasksList);
            intent = new Intent(this, TasksListActivity.class);
            intent.setAction(getIntent().getAction());
            intent.setData(Uri.parse(str).buildUpon().build());
            startActivity(intent);
            this.mPlusImageView.setVisibility(View.GONE);
        } else if ("android.intent.action.SEARCH".equals(intent.getAction())) {
            ArchitectureContext.setObject(DatabaseHelper.createTasksListCalledAll());
            str = intent.getStringExtra("query");
            intent = new Intent(this, TasksListViewActivity.class);
            intent.setAction(getIntent().getAction());
            intent.putExtra(Constants.IS_VIEW_FROM_HOME, Constants.TRUE);
            intent.putExtra(OptionsMenuHelper.VIEW_TYPE, 20);
            intent.putExtra("query", str);
            startActivity(intent);
            this.mPlusImageView.setVisibility(View.GONE);
        }
    }

    private void setLocalOnScrollListener() {
        getListView().setOnScrollListener(new OnScrollListener() {
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                TasksListHomeActivity.mFirstVisibleEvent = firstVisibleItem;
            }
        });
        registerForContextMenu(getListView());
    }

    private void setWindowTitle() {
        if (StringUtil.isNullOrEmpty(AppUtil.getCurrentlySyncedTasksList())) {
            this.mTitle.setText(AppUtil.getAppName());
        } else {
            this.mTitle.setText(AppUtil.getCurrentlySyncedTasksList());
        }
        this.mTitle.setBackgroundColor(Color.parseColor(Constants.COLOR_BLUISH_DAY_HEADER));
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 84 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        try {
            onSearchRequested();
            return super.onKeyDown(keyCode, event);
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return true;
        }
    }

    public boolean onSearchRequested() {
        SearchProvider.mTasks = null;
        this.config.setLastSelectedTasksList(DatabaseHelper.ALL_TASKS_LIST_ID);
        return super.onSearchRequested();
    }

    public String getEmailAllTasksBody() {
        return null;
    }

    public void showCopyListAsBranch() {
        List<TasksList> copyToTasksLists = new ArrayList(this.mTasksLists);
        copyToTasksLists.remove(0);
        ContextMenuHelper.showCopyListAsBranch(this, getTasksListNames(copyToTasksLists), ((TasksList) this.mTasksLists.get(this.mSelectedPosition)).getId(), copyToTasksLists);
    }

    private String[] getTasksListNames(List<TasksList> copyToTasksLists) {
        String[] tasksListNames = new String[copyToTasksLists.size()];
        for (int i = 0; i < copyToTasksLists.size(); i++) {
            tasksListNames[i] = ((TasksList) copyToTasksLists.get(i)).getName();
        }
        return tasksListNames;
    }
}
