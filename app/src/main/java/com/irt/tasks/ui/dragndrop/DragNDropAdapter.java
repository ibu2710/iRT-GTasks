package com.irt.tasks.ui.dragndrop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;

public final class DragNDropAdapter extends BaseAdapter implements RemoveListener, DropListener {
    private ArrayList<String> mContent;
    private int[] mIds;
    private LayoutInflater mInflater;
    private int[] mLayouts;

    static class ViewHolder {
        TextView text;

        ViewHolder() {
        }
    }

    public DragNDropAdapter(Context context, ArrayList<String> content) {
        init(context, new int[]{17367043}, new int[]{16908308}, content);
    }

    public DragNDropAdapter(Context context, int[] itemLayouts, int[] itemIDs, ArrayList<String> content) {
        init(context, itemLayouts, itemIDs, content);
    }

    private void init(Context context, int[] layouts, int[] ids, ArrayList<String> content) {
        this.mInflater = LayoutInflater.from(context);
        this.mIds = ids;
        this.mLayouts = layouts;
        this.mContent = content;
    }

    public int getCount() {
        return this.mContent.size();
    }

    public String getItem(int position) {
        return (String) this.mContent.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate(this.mLayouts[0], null);
            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(this.mIds[0]);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.text.setText((CharSequence) this.mContent.get(position));
        return convertView;
    }

    public void onRemove(int which) {
        if (which >= 0 && which <= this.mContent.size()) {
            this.mContent.remove(which);
        }
    }

    public void onDrop(int from, int to) {
        String temp = (String) this.mContent.get(from);
        this.mContent.remove(from);
        this.mContent.add(to, temp);
    }
}
