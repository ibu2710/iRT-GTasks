package com.irt.tasks.ui.help;

public class TasksListHomeHelpContext {
    public static void setTasksListHomeHelpContext(HelpContext helpContext) {
        helpContext.title = "Tasks List Home";
        helpContext.content.append("<b>Description:</b>");
        helpContext.content.append("<br>The Home of iRT GTasks Outliner. Tasks Lists (Outlines) can be edited/created on this view. The 'ALL TASKS LIST' is a special list that contains all the lists in this view, if you tap on it, it will show the lists in tree view.");
        helpContext.content.append("<br><br><b>New/Edit List:</b>");
        helpContext.content.append(HelpUtility.UL_STYLE);
        helpContext.content.append("<li><b>Add new List</b> through <b>Menu -> Add new List</b> or long press on any List, Add new list.");
        helpContext.content.append("<li><b>Edit List</b> by doing long press on any List, Edit list.");
        helpContext.content.append("<li><b>Tap</b> on any list to access the Tasks Tree view.");
        helpContext.content.append("</ul>");
        helpContext.content.append("<b>Blue Plus Icon:</b>");
        helpContext.content.append("<br>In the Tasks List Home view or Tasks Tree of 'ALL TASKS LIST', the Blue Plus icon by default will go to the last accessed list and <b>add a new task</b> on top of that list. This can be configured to create a new task on a specific list on Preferences -> Navigation Preferences -> Blue Plus Tasks List.");
        helpContext.content.append("<br><br><b>Navigation:</b>");
        helpContext.content.append(HelpUtility.UL_STYLE);
        helpContext.content.append("<li><b>Long Press</b> on any of the list for <b>List Context Menu</b>.");
        helpContext.content.append("<li><b>Tap</b> on any list to access the <b>Tasks Tree view</b>.");
        helpContext.content.append("</ul>");
        helpContext.content.append("<b>Note:</b>");
        helpContext.content.append(HelpUtility.UL_STYLE);
        helpContext.content.append("<li> Some of the Tasks Lists might not be displayed on this view because the account is hidden or the list is hidden. Check Preferences -> Account Manager and Preferences -> Tasks List Manager.");
        helpContext.content.append("</ul>");
        helpContext.content.append("<b>Search:</b>");
        helpContext.content.append("<br>The phone's <b>Search button</b> will open 'ALL TASKS LIST' in Tasks Tree view and process search on that view.<br><br>");
    }
}
