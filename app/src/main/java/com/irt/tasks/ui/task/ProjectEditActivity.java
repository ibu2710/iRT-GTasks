package com.irt.tasks.ui.task;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Project;
import com.irt.tasks.app.businessprocess.ProjectBp;
import com.irt.tasks.app.businessprocess.ProjectBpImpl;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.sync.DropboxSyncHelper;

public class ProjectEditActivity extends AbstractActivity {
    Button mCancelButton;
    Button mDeleteButton;
    boolean mIsNewProject = false;
    Project mProject;
    EditText mProjectDescriptionEditText;
    EditText mProjectTitleEditText;
    Button mSaveButton;
    ProjectBp projectBp = new ProjectBpImpl();
    TaskBp taskBp = new TaskBpImpl();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.project_edit);
        this.mProject = (Project) ArchitectureContext.getObject();
        this.mIsNewProject = this.mProject.getId() == null;
        initViewControls();
        initDeleteButton();
        initCancelButton();
        initSaveButton();
        initViewValues();
        if (this.mIsNewProject) {
            this.mProjectTitleEditText.requestFocus();
        }
    }

    private void initViewControls() {
        this.mDeleteButton = (Button) findViewById(R.id.delete_button);
        this.mSaveButton = (Button) findViewById(R.id.save_button);
        this.mCancelButton = (Button) findViewById(R.id.cancel_button);
        this.mProjectTitleEditText = (EditText) findViewById(R.id.project_title);
        DropboxSyncHelper.addDelayedIdleToEditText(this.mProjectTitleEditText);
        this.mProjectDescriptionEditText = (EditText) findViewById(R.id.project_description);
        DropboxSyncHelper.addDelayedIdleToEditText(this.mProjectDescriptionEditText);
        if (this.mIsNewProject) {
            this.mDeleteButton.setVisibility(View.GONE);
        }
    }

    private void initViewValues() {
        this.mProjectTitleEditText.setText(this.mProject.getTitle());
        this.mProjectDescriptionEditText.setText(this.mProject.getDescription());
    }

    private void initDeleteButton() {
        this.mDeleteButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new Builder(ProjectEditActivity.this).setTitle("Delete project").setMessage("Project will be deleted?").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ProjectEditActivity.this.projectBp.delete(ProjectEditActivity.this.mProject);
                        ProjectEditActivity.this.finish();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
            }
        });
    }

    private void initSaveButton() {
        this.mSaveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ProjectEditActivity.this.mProject.setTitle(ProjectEditActivity.this.mProjectTitleEditText.getText().toString());
                switch (ProjectEditActivity.this.projectBp.isValidTitle(ProjectEditActivity.this.mProject)) {
                    case NO_ERROR:
                        ProjectEditActivity.this.mProject.setTitle(ProjectEditActivity.this.mProject.getTitle().trim());
                        ProjectEditActivity.this.mProject.setDescription(ProjectEditActivity.this.mProjectDescriptionEditText.getText().toString());
                        ProjectEditActivity.this.projectBp.saveOrUpdate(ProjectEditActivity.this.mProject);
                        ProjectEditActivity.this.finish();
                        return;
                    case BLANK:
                        AppUtil.showGenericWarningMessage(ProjectEditActivity.this, "Title required", "Please provide project name");
                        return;
                    case DUPLICATE:
                        AppUtil.showGenericWarningMessage(ProjectEditActivity.this, "Duplicate title", "Please enter another project name");
                        return;
                    default:
                        return;
                }
            }
        });
    }

    private void initCancelButton() {
        this.mCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ProjectEditActivity.this.finish();
            }
        });
    }
}
