package com.irt.tasks.ui.filter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.ContextBase;
import com.irt.tasks.app.businessobjects.ContextTask;
import com.irt.tasks.app.businessobjects.Tag;
import com.irt.tasks.app.businessobjects.TagTask;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessprocess.ContextBp;
import com.irt.tasks.app.businessprocess.ContextBpImpl;
import com.irt.tasks.app.businessprocess.ProjectBp;
import com.irt.tasks.app.businessprocess.ProjectBpImpl;
import com.irt.tasks.app.businessprocess.TagBp;
import com.irt.tasks.app.businessprocess.TagBpImpl;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.Constants.FILTER_PARAMETER_TYPES;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.task.ContextSelectionListActivity;
import com.irt.tasks.ui.task.ProjectSelectionListActivity;
import com.irt.tasks.ui.task.TagSelectionListActivity;
import java.util.ArrayList;
import java.util.List;

public class TasksFilterParametersActivity extends AbstractActivity {
    ContextBp contextBp = new ContextBpImpl();
    TasksFilterManager filter;
    Button mCancelButton;
    TableLayout mContextsDescriptionTableLayout;
    TextView mContextsDescrption;
    Button mFilterResetButton;
    boolean mFirstTimeContextTaskAccess = true;
    boolean mFirstTimeProjectAccess = true;
    boolean mFirstTimeTagTaskAccess = true;
    Button mOkButton;
    TableLayout mProjectDescriptionTableLayout;
    TextView mProjectDescrption;
    Spinner mStatusSpinner;
    TableLayout mTagsDescriptionTableLayout;
    TextView mTagsDescrption;
    Task mTaskForContext;
    Task mTaskForProject;
    Task mTaskForTag;
    ProjectBp projectBp = new ProjectBpImpl();
    TagBp tagBp = new TagBpImpl();

    class C18011 implements OnItemSelectedListener {
        C18011() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View arg1, int position, long arg3) {
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    class C18022 implements OnClickListener {
        C18022() {
        }

        public void onClick(View v) {
            if (TasksFilterParametersActivity.this.mTaskForProject == null) {
                TasksFilterParametersActivity.this.mTaskForProject = new Task();
                TasksFilterParametersActivity.this.mTaskForProject.setAnySelected(TasksFilterParametersActivity.this.filter.isAnyProjectSelected());
                TasksFilterParametersActivity.this.mTaskForProject.setNoneSelected(TasksFilterParametersActivity.this.filter.getTaskFilter().isProjectNone());
                TasksFilterParametersActivity.this.mTaskForProject.setProjectId(TasksFilterParametersActivity.this.filter.getTaskFilter().getProjectId());
            }
            TasksFilterParametersActivity.this.mTaskForProject.setFilterParameterType(FILTER_PARAMETER_TYPES.PROJECT);
            ArchitectureContext.setObject(TasksFilterParametersActivity.this.mTaskForProject);
            Intent intent = new Intent(TasksFilterParametersActivity.this, ProjectSelectionListActivity.class);
            intent.putExtra(Constants.INTENT_IS_SHOW_ANY, true);
            intent.putExtra(Constants.INTENT_IS_HIDE_AND_OR, true);
            TasksFilterParametersActivity.this.startActivity(intent);
        }
    }

    class C18033 implements OnClickListener {
        C18033() {
        }

        public void onClick(View v) {
            List<ContextTask> contextTasks;
            if (TasksFilterParametersActivity.this.mTaskForContext == null) {
                TasksFilterParametersActivity.this.mTaskForContext = new Task();
                TasksFilterParametersActivity.this.mTaskForContext.setAnySelected(TasksFilterParametersActivity.this.filter.isAnyContextSelected());
                TasksFilterParametersActivity.this.mTaskForContext.setNoneSelected(TasksFilterParametersActivity.this.filter.getTaskFilter().isContextNone());
                TasksFilterParametersActivity.this.mTaskForContext.setAndOperation(TasksFilterParametersActivity.this.filter.getTaskFilter().isContextAnd());
                contextTasks = TasksFilterParametersActivity.this.getContextTasks();
            } else {
                contextTasks = TasksFilterParametersActivity.this.mTaskForContext.getContextTasks();
            }
            TasksFilterParametersActivity.this.mTaskForContext.setContextTasks(contextTasks);
            TasksFilterParametersActivity.this.mTaskForContext.setFilterParameterType(FILTER_PARAMETER_TYPES.CONTEXT);
            ArchitectureContext.setObject(TasksFilterParametersActivity.this.mTaskForContext);
            Intent intent = new Intent(TasksFilterParametersActivity.this, ContextSelectionListActivity.class);
            intent.putExtra(Constants.INTENT_IS_SHOW_ANY, true);
            TasksFilterParametersActivity.this.startActivity(intent);
        }
    }

    class C18044 implements OnClickListener {
        C18044() {
        }

        public void onClick(View v) {
            List<TagTask> tagTasks;
            if (TasksFilterParametersActivity.this.mTaskForTag == null) {
                TasksFilterParametersActivity.this.mTaskForTag = new Task();
                TasksFilterParametersActivity.this.mTaskForTag.setAnySelected(TasksFilterParametersActivity.this.filter.isAnyTagSelected());
                TasksFilterParametersActivity.this.mTaskForTag.setNoneSelected(TasksFilterParametersActivity.this.filter.getTaskFilter().isTagNone());
                TasksFilterParametersActivity.this.mTaskForTag.setAndOperation(TasksFilterParametersActivity.this.filter.getTaskFilter().isTagAnd());
                tagTasks = TasksFilterParametersActivity.this.getTagTasks();
            } else {
                tagTasks = TasksFilterParametersActivity.this.mTaskForTag.getTagTasks();
            }
            TasksFilterParametersActivity.this.mTaskForTag.setTagTasks(tagTasks);
            TasksFilterParametersActivity.this.mTaskForTag.setFilterParameterType(FILTER_PARAMETER_TYPES.TAG);
            ArchitectureContext.setObject(TasksFilterParametersActivity.this.mTaskForTag);
            Intent intent = new Intent(TasksFilterParametersActivity.this, TagSelectionListActivity.class);
            intent.putExtra(Constants.INTENT_IS_SHOW_ANY, true);
            TasksFilterParametersActivity.this.startActivity(intent);
        }
    }

    class C18055 implements OnClickListener {
        C18055() {
        }

        public void onClick(View v) {
            TasksFilter tasksFilter = TasksFilterParametersActivity.this.filter.getTaskFilter();
            if (TasksFilterParametersActivity.this.mTaskForProject != null) {
                Long projectId = TasksFilterParametersActivity.this.mTaskForProject.getProjectId();
                if (projectId.equals(Long.valueOf(0)) && !TasksFilterParametersActivity.this.mTaskForProject.isNoneSelected()) {
                    TasksFilterParametersActivity.this.filter.setAnyProject();
                } else if (projectId.equals(Long.valueOf(0)) && TasksFilterParametersActivity.this.mTaskForProject.isNoneSelected()) {
                    tasksFilter.setProjectNone(TasksFilterParametersActivity.this.mTaskForProject.isNoneSelected());
                } else {
                    tasksFilter.setProjectNone(false);
                }
                tasksFilter.setProjectId(projectId);
            }
            tasksFilter.setStatus(Integer.valueOf(TasksFilterParametersActivity.this.mStatusSpinner.getSelectedItemPosition()));
            if (TasksFilterParametersActivity.this.mTaskForContext != null) {
                if (TasksFilterParametersActivity.this.mTaskForContext.getContextTasks() != null && TasksFilterParametersActivity.this.mTaskForContext.getContextTasks().size() > 0) {
                    List<Long> contextIds = new ArrayList();
                    for (ContextTask contextTask : TasksFilterParametersActivity.this.mTaskForContext.getContextTasks()) {
                        contextIds.add(contextTask.getContextId());
                    }
                    tasksFilter.setContextIds(contextIds);
                    tasksFilter.setContextAnd(TasksFilterParametersActivity.this.mTaskForContext.isAndOperation());
                    tasksFilter.setContextNone(false);
                } else if (TasksFilterParametersActivity.this.mTaskForContext.isAnySelected()) {
                    TasksFilterParametersActivity.this.filter.setAnyContext();
                    tasksFilter.setContextNone(false);
                    tasksFilter.setContextAnd(false);
                } else {
                    tasksFilter.setContextNone(true);
                    tasksFilter.setContextAnd(false);
                    tasksFilter.setContextIds(null);
                }
            }
            if (TasksFilterParametersActivity.this.mTaskForTag != null) {
                if (TasksFilterParametersActivity.this.mTaskForTag.getTagTasks() != null && TasksFilterParametersActivity.this.mTaskForTag.getTagTasks().size() > 0) {
                    List<Long> tagIds = new ArrayList();
                    for (TagTask tagTask : TasksFilterParametersActivity.this.mTaskForTag.getTagTasks()) {
                        tagIds.add(tagTask.getTagId());
                    }
                    tasksFilter.setTagIds(tagIds);
                    tasksFilter.setTagAnd(TasksFilterParametersActivity.this.mTaskForTag.isAndOperation());
                    tasksFilter.setTagNone(false);
                } else if (TasksFilterParametersActivity.this.mTaskForTag.isAnySelected()) {
                    TasksFilterParametersActivity.this.filter.setAnyTag();
                    tasksFilter.setTagNone(false);
                    tasksFilter.setTagAnd(false);
                } else {
                    tasksFilter.setTagNone(true);
                    tasksFilter.setTagAnd(false);
                    tasksFilter.setTagIds(null);
                }
            }
            TasksFilterParametersActivity.this.filter.saveFilter();
            TasksFilterParametersActivity.this.finish();
        }
    }

    class C18066 implements OnClickListener {
        C18066() {
        }

        public void onClick(View v) {
            TasksFilterParametersActivity.this.finish();
        }
    }

    class C18077 implements OnClickListener {
        C18077() {
        }

        public void onClick(View v) {
            if (TasksFilterParametersActivity.this.mTaskForProject == null) {
                TasksFilterParametersActivity.this.mTaskForProject = new Task();
            }
            TasksFilterParametersActivity.this.mTaskForProject.setProjectId(Long.valueOf(0));
            TasksFilterParametersActivity.this.mTaskForProject.setNoneSelected(false);
            TasksFilterParametersActivity.this.mTaskForProject.setAnySelected(true);
            TasksFilterParametersActivity.this.mStatusSpinner.setSelection(0);
            if (TasksFilterParametersActivity.this.mTaskForContext == null) {
                TasksFilterParametersActivity.this.mTaskForContext = new Task();
            }
            TasksFilterParametersActivity.this.mTaskForContext.setContextTasks(null);
            TasksFilterParametersActivity.this.mTaskForContext.setNoneSelected(false);
            TasksFilterParametersActivity.this.mTaskForContext.setAnySelected(true);
            TasksFilterParametersActivity.this.mTaskForContext.setAndOperation(false);
            if (TasksFilterParametersActivity.this.mTaskForTag == null) {
                TasksFilterParametersActivity.this.mTaskForTag = new Task();
            }
            TasksFilterParametersActivity.this.mTaskForTag.setTagTasks(null);
            TasksFilterParametersActivity.this.mTaskForTag.setNoneSelected(false);
            TasksFilterParametersActivity.this.mTaskForTag.setAnySelected(true);
            TasksFilterParametersActivity.this.mTaskForContext.setAndOperation(false);
            TasksFilterParametersActivity.this.onResume();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tasks_filter_parameters);
        this.filter = TasksFilterManager.getInstance();
        initViews();
        initCancelButton();
        initOkButton();
        initFilterResetButton();
        initProjectSpinner();
        initStatusSpinner();
        initContextsDescriptionTableLayout();
        initProjectDescriptionTableLayout();
        initTagsDescriptionTableLayout();
    }

    private void initProjectSpinner() {
    }

    protected void onResume() {
        super.onResume();
        setContextsDescrption();
        setProjectDescrption();
        setTagsDescrption();
    }

    private void setProjectDescrption() {
        Long projectId = Long.valueOf(0);
        boolean isAnyProject = false;
        if (this.mFirstTimeProjectAccess) {
            projectId = this.filter.getTaskFilter().getProjectId();
            isAnyProject = this.filter.isAnyProjectSelected();
        } else if (this.mTaskForProject != null) {
            projectId = this.mTaskForProject.getProjectId();
            if (projectId.equals(Long.valueOf(0))) {
                isAnyProject = this.mTaskForProject.isAnySelected();
            }
        }
        if (this.mFirstTimeProjectAccess || this.mTaskForProject != null) {
            String projectString;
            if (!projectId.equals(Long.valueOf(0))) {
                projectString = this.projectBp.get(projectId).getTitle();
            } else if (isAnyProject) {
                projectString = "";
            } else {
                projectString = "no project";
            }
            this.mProjectDescrption.setText(projectString);
        }
        this.mFirstTimeProjectAccess = false;
    }

    private void setContextsDescrption() {
        StringBuffer contextString = new StringBuffer();
        boolean isAnyContext = false;
        boolean isAndOperation = false;
        List<ContextTask> contextTasks = null;
        if (this.mFirstTimeContextTaskAccess) {
            contextTasks = getContextTasks();
            if (contextTasks == null) {
                isAnyContext = this.filter.isAnyContextSelected();
            }
            isAndOperation = this.filter.getTaskFilter().isContextAnd();
        } else if (this.mTaskForContext != null) {
            contextTasks = this.mTaskForContext.getContextTasks();
            if (contextTasks == null) {
                isAnyContext = this.mTaskForContext.isAnySelected();
            } else if (contextTasks.size() == 1) {
                this.mTaskForContext.setAndOperation(false);
            }
            isAndOperation = this.mTaskForContext.isAndOperation();
        }
        if (this.mFirstTimeContextTaskAccess || this.mTaskForContext != null) {
            if (contextTasks != null) {
                List<ContextBase> contextBases = this.contextBp.getAllContexts();
                boolean firstRecord = true;
                for (ContextTask contextTask : contextTasks) {
                    if (!firstRecord) {
                        contextString.append(isAndOperation ? " & " : ", ");
                    }
                    appendContextName(contextString, contextTask, contextBases);
                    firstRecord = false;
                }
            } else if (isAnyContext) {
                contextString.append("");
            } else {
                contextString.append(Constants.NO_CONTEXT);
            }
            this.mContextsDescrption.setText(contextString.toString());
        }
        this.mFirstTimeContextTaskAccess = false;
    }

    private void appendContextName(StringBuffer contextString, ContextTask contextTask, List<ContextBase> contextBases) {
        for (ContextBase contextBase : contextBases) {
            if (contextBase.getId().equals(contextTask.getContextId())) {
                contextString.append(contextBase.getName());
            }
        }
    }

    private void setTagsDescrption() {
        StringBuffer tagString = new StringBuffer();
        boolean isAnyTag = false;
        boolean isAndOperation = false;
        List<TagTask> tagTasks = null;
        if (this.mFirstTimeTagTaskAccess) {
            tagTasks = getTagTasks();
            if (tagTasks == null) {
                isAnyTag = this.filter.isAnyTagSelected();
            }
            isAndOperation = this.filter.getTaskFilter().isTagAnd();
        } else if (this.mTaskForTag != null) {
            tagTasks = this.mTaskForTag.getTagTasks();
            if (tagTasks == null) {
                isAnyTag = this.mTaskForTag.isAnySelected();
            } else if (tagTasks.size() == 1) {
                this.mTaskForTag.setAndOperation(false);
            }
            isAndOperation = this.mTaskForTag.isAndOperation();
        }
        if (this.mFirstTimeTagTaskAccess || this.mTaskForTag != null) {
            if (tagTasks != null) {
                List<Tag> tags = this.tagBp.getAllTags();
                boolean firstRecord = true;
                for (TagTask tagTask : tagTasks) {
                    if (!firstRecord) {
                        tagString.append(isAndOperation ? " & " : ", ");
                    }
                    appendTagName(tagString, tagTask, tags);
                    firstRecord = false;
                }
            } else if (isAnyTag) {
                tagString.append("");
            } else {
                tagString.append("no tag");
            }
            this.mTagsDescrption.setText(tagString.toString());
        }
        this.mFirstTimeTagTaskAccess = false;
    }

    private void appendTagName(StringBuffer tagString, TagTask tagTask, List<Tag> tags) {
        for (Tag tag : tags) {
            if (tag.getId().equals(tagTask.getTagId())) {
                tagString.append(tag.getName());
            }
        }
    }

    private void initStatusSpinner() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.tasks_status_filter));
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.mStatusSpinner.setAdapter(spinnerArrayAdapter);
        this.mStatusSpinner.setSelection(this.filter.getTaskFilter().getStatus().intValue());
        this.mStatusSpinner.setOnItemSelectedListener(new C18011());
    }

    private void initProjectDescriptionTableLayout() {
        this.mProjectDescriptionTableLayout.setOnClickListener(new C18022());
    }

    private void initContextsDescriptionTableLayout() {
        this.mContextsDescriptionTableLayout.setOnClickListener(new C18033());
    }

    private List<ContextTask> getContextTasks() {
        List<Long> contextIds = this.filter.getTaskFilter().getContextIds();
        List<ContextTask> contextTasks = null;
        if (contextIds != null) {
            contextTasks = new ArrayList();
            for (Long contextId : contextIds) {
                contextTasks.add(new ContextTask(contextId, null));
            }
        }
        return contextTasks;
    }

    private void initTagsDescriptionTableLayout() {
        this.mTagsDescriptionTableLayout.setOnClickListener(new C18044());
    }

    private List<TagTask> getTagTasks() {
        List<Long> tagIds = this.filter.getTaskFilter().getTagIds();
        List<TagTask> tagTasks = null;
        if (tagIds != null) {
            tagTasks = new ArrayList();
            for (Long tagId : tagIds) {
                tagTasks.add(new TagTask(tagId, null));
            }
        }
        return tagTasks;
    }

    private void initViews() {
        this.mOkButton = (Button) findViewById(R.id.ok_button);
        this.mCancelButton = (Button) findViewById(R.id.cancel_button);
        this.mFilterResetButton = (Button) findViewById(R.id.reset_button);
        this.mStatusSpinner = (Spinner) findViewById(R.id.status_spinner);
        this.mProjectDescriptionTableLayout = (TableLayout) findViewById(R.id.project_description_table_layout);
        this.mProjectDescrption = (TextView) findViewById(R.id.project_description);
        this.mContextsDescriptionTableLayout = (TableLayout) findViewById(R.id.contexts_description_table_layout);
        this.mContextsDescrption = (TextView) findViewById(R.id.contexts_description);
        this.mTagsDescriptionTableLayout = (TableLayout) findViewById(R.id.tags_description_table_layout);
        this.mTagsDescrption = (TextView) findViewById(R.id.tags_description);
    }

    private void initOkButton() {
        this.mOkButton.setOnClickListener(new C18055());
    }

    private void initCancelButton() {
        this.mCancelButton.setOnClickListener(new C18066());
    }

    private void initFilterResetButton() {
        this.mFilterResetButton.setOnClickListener(new C18077());
    }
}
