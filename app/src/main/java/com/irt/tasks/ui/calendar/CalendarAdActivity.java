package com.irt.tasks.ui.calendar;

import android.os.Bundle;
import android.webkit.WebView;

import com.irt.tasks.R;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.ui.AbstractActivity;

public class CalendarAdActivity extends AbstractActivity {
    private static final String UL_STYLE = "<ul style=\"margin-left: .9em; padding-left: .9em;\">";
    private WebView mProAdWebView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_ad);
        this.mProAdWebView = (WebView) findViewById(R.id.calendar_ad_web_view);
        StringBuffer content = new StringBuffer();
        content.append("<b>" + AppUtil.getAppName() + "</b> integrates and will come for <b>FREE</b> with the purchase of <b>iRT Calendar Pro</b>.<br><br>");
        content.append("You can install <a href='market://details?id=com.irt.calendar.app.trial'>iRT Calendar Pro Trial</a> ");
        content.append("to try the calendar for 30 days.<br><br>");
        content.append("To get the full version of iRT Calendar Pro, you need to buy the following from the Android market:");
        content.append("<ul style=\"margin-left: .9em; padding-left: .9em;\">");
        content.append("<li><a href='market://details?id=com.irt.calendar.app.ui'>iRT Calendar</a>");
        content.append("<li><a href='market://details?id=com.irt.calendar.pro.key'>iRT Calendar Pro Key</a>");
        content.append("</ul>");
        content.append("<br><br>");
        content.append("Visit or contact us at: <br><br>");
        content.append("<a href='http://www.irtgtasksoutliner.com'>www.iRTGTasksOutliner.com</a><br><br>");
        content.append("<a href='mailto:support@iRTWebSolutions.com'>support@iRTWebSolutions.com.</a><br><br>");
        this.mProAdWebView.loadData(content.toString(), "text/html", Constants.encoding);
    }
}
