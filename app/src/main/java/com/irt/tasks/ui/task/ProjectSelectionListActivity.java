package com.irt.tasks.ui.task;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Project;
import com.irt.tasks.app.businessprocess.ProjectBp;
import com.irt.tasks.app.businessprocess.ProjectBpImpl;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import java.util.ArrayList;
import java.util.List;

public class ProjectSelectionListActivity extends AbstractEditListActivity {
    Long mOriginalProjectId;
    List<Project> mProjects;
    ProjectBp projectBp = new ProjectBpImpl();
    
    protected void performOnCreate(Bundle savedInstanceState) {
        this.mOriginalProjectId = this.mTask.getProjectId();
    }

    private void refereshList() {
        setListAdapter(new ProjectInteractiveArrayAdapter(this, R.layout.list_item_black_text_radiobutton, 0, getProjects()));
    }

    protected List<Project> getProjects() {
        List<Project> projects = this.projectBp.getAll();
        this.mProjects = new ArrayList();
        Project noneProject = new Project();
        if (!this.mIsShowAny) {
            noneProject.setId(Long.valueOf(0));
            noneProject.setTitle("None");
            noneProject.setSelected(true);
            this.mProjects.add(noneProject);
        }
        if (projects.size() > 0) {
            for (int i = 0; i < projects.size(); i++) {
                Project project = (Project) projects.get(i);
                if (this.mTask.getProjectId().equals(project.getId())) {
                    project.setSelected(true);
                    if (!this.mIsShowAny) {
                        noneProject.setSelected(false);
                    }
                } else {
                    project.setSelected(false);
                }
                this.mProjects.add(project);
            }
        }
        return this.mProjects;
    }

    protected void performSetRadioButtons() {
        if (this.mTask.getParentTaskId() == null) {
            this.mAnyRadioButton.setChecked(true);
            this.mTask.setAnySelected(true);
        }
    }

    protected CharSequence getAnyRadioButtonText() {
        return "Any project";
    }

    protected CharSequence getNoneRadioButtonText() {
        return "No project";
    }

    protected void performCancel() {
        this.mTask.setProjectId(this.mOriginalProjectId);
    }

    protected void onResume() {
        super.onResume();
        refereshList();
    }

    protected void performNew() {
        editProject(new Project());
    }

    protected void performSave() {
    }

    protected void performSetTaskForSelectedItem(int position) {
        this.mTask.setProjectId(((Project) this.mProjects.get(position)).getId());
        finish();
    }

    protected void performListOnItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        this.mTask.setAnySelected(false);
        this.mTask.setNoneSelected(false);
        performSetTaskForSelectedItem(position);
    }

    private void editProject(Project project) {
        ArchitectureContext.setObject(project);
        startActivity(new Intent(this, ProjectEditActivity.class));
    }

    protected void performDeleteSelectedListItem(final int position) {
        if (this.mIsShowAny || position > 0) {
            new Builder(this).setTitle("Delete project").setMessage("Project [" + ((Project) this.mProjects.get(position)).getTitle() + "] will be deleted?").setPositiveButton("OK", new OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    ProjectSelectionListActivity.this.projectBp.delete((Project) ProjectSelectionListActivity.this.mProjects.get(position));
                    ProjectSelectionListActivity.this.onResume();
                }
            }).setNegativeButton("Cancel", new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    
                }
            }).show();
        }
    }

    protected void performEditSelectedListItem(int position) {
        if (this.mIsShowAny || position > 0) {
            editProject((Project) this.mProjects.get(position));
        }
    }

    protected boolean isShowContextMenu(int position) {
        if (this.mIsShowAny || position > 0) {
            return true;
        }
        return false;
    }
}
