package com.irt.tasks.ui.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.ui.sync.DropboxSyncHelper;

public class WifiReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        NetworkInfo netInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (netInfo == null || netInfo.getType() != 1) {
            AppUtil.logDebug("Don't have Wifi Connection");
        } else {
            AppUtil.logDebug("Have Wifi Connection");
            DropboxSyncHelper.backendSetWifiStatusDelay();
        }
        DropboxSyncHelper.backendSetWifiStatusDelay();
    }
}
