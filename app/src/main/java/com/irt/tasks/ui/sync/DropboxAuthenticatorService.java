package com.irt.tasks.ui.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class DropboxAuthenticatorService extends Service {
    private DropboxAuthenticator mAuthenticator;

    public void onCreate() {
        this.mAuthenticator = new DropboxAuthenticator(this);
    }

    public IBinder onBind(Intent intent) {
        return this.mAuthenticator.getIBinder();
    }
}
