package com.irt.tasks.ui.dragndrop;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;

public class DragNDropListView extends ListView {
    int DIRECTION_DOWN = -99;
    int DIRECTION_INVALID = 99;
    int DIRECTION_NEUTRAL = 0;
    int DIRECTION_UP = 1;
    int direction = this.DIRECTION_NEUTRAL;
    boolean mDisableDown = false;
    boolean mDisableUp = false;
    DragListener mDragListener;
    boolean mDragMode;
    private int mDragPoint;
    int mDragPointOffset;
    ImageView mDragView;
    DropListener mDropListener;
    int mEndPosition;
    private int mFirstDragPos;
    GestureDetector mGestureDetector;
    private int mHeight;
    int mInitialDirection = this.DIRECTION_INVALID;
    private boolean mIsEnableDragMode;
    boolean mIsFirstLoop = true;
    boolean mIsInitialDirectionFound = false;
    private boolean mIsRightDrag;
    int mLastValidEndPosition = -1;
    private int mLowerBound;
    int mPrevPosition = 0;
    RemoveListener mRemoveListener;
    int mStartPosition;
    private Rect mTempRect = new Rect();
    private final int mTouchSlop;
    private int mUpperBound;

    class C17971 extends SimpleOnGestureListener {
        C17971() {
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (DragNDropListView.this.mDragView == null) {
                return false;
            }
            int deltaY = (int) Math.abs(e1.getY() - e2.getY());
            if (((int) Math.abs(e1.getX() - e2.getX())) > DragNDropListView.this.mDragView.getWidth() / 2 && deltaY < DragNDropListView.this.mDragView.getHeight()) {
                DragNDropListView.this.mRemoveListener.onRemove(DragNDropListView.this.mStartPosition);
            }
            DragNDropListView.this.stopDrag(DragNDropListView.this.mStartPosition - DragNDropListView.this.getFirstVisiblePosition());
            return true;
        }
    }

    public void enableDragMode(boolean isEnableDragMode) {
        this.mIsEnableDragMode = isEnableDragMode;
    }

    public void setRightDrag() {
        this.mIsRightDrag = true;
    }

    public void setLeftDrag() {
        this.mIsRightDrag = false;
    }

    public DragNDropListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public void setDropListener(DropListener l) {
        this.mDropListener = l;
    }

    public void setRemoveListener(RemoveListener l) {
        this.mRemoveListener = l;
    }

    public void setDragListener(DragListener l) {
        this.mDragListener = l;
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (!this.mIsEnableDragMode) {
            return super.onTouchEvent(ev);
        }
        int action = ev.getAction();
        int x = (int) ev.getX();
        int y = (int) ev.getY();
        if (action == 0 && isInDragArea(x)) {
            this.mDragMode = true;
        }
        if (!this.mDragMode) {
            return super.onTouchEvent(ev);
        }
        switch (action) {
            case 0:
                this.mStartPosition = pointToPosition(x, y);
                if (this.mStartPosition != -1) {
                    int mItemPosition = this.mStartPosition - getFirstVisiblePosition();
                    this.mDragPointOffset = y - getChildAt(mItemPosition).getTop();
                    this.mDragPointOffset -= ((int) ev.getRawY()) - y;
                    startDrag(mItemPosition, y);
                    drag(x, y);
                }
                initAutoScrollingBounderies(y);
                break;
            case 2:
                drag(x, y);
                int samplePosition = pointToPosition(x, y);
                if (samplePosition != -1) {
                    this.mLastValidEndPosition = samplePosition;
                    break;
                }
                break;
            default:
                this.mDragMode = false;
                this.mEndPosition = pointToPosition(x, y);
                stopDrag(this.mStartPosition - getFirstVisiblePosition());
                if (this.mDropListener != null && this.mStartPosition != -1 && this.mEndPosition != -1) {
                    this.mDropListener.onDrop(this.mStartPosition, this.mEndPosition);
                } else if (!(this.mDropListener == null || this.mStartPosition == -1 || this.mLastValidEndPosition == -1)) {
                    this.mDropListener.onDrop(this.mStartPosition, this.mLastValidEndPosition);
                }
                this.mLastValidEndPosition = -1;
                this.mDisableDown = false;
                this.mDisableUp = false;
                break;
        }
        performAutoScroll(y);
        return true;
    }

    private boolean isInDragArea(int x) {
        if (this.mIsRightDrag) {
            if (x > (getWidth() * 4) / 5) {
                return true;
            }
            return false;
        } else if (x >= getWidth() / 5) {
            return false;
        } else {
            return true;
        }
    }

    private void initAutoScrollingBounderies(int y) {
        this.mHeight = getHeight();
        int touchSlop = this.mTouchSlop;
        this.mUpperBound = Math.min(y - touchSlop, this.mHeight / 3);
        this.mLowerBound = Math.max(y + touchSlop, (this.mHeight * 2) / 3);
    }

    private void performAutoScroll(int y) {
        int speed = 0;
        if (getItemForPosition(y) >= 0) {
            adjustScrollBounds(y);
            if (y > this.mLowerBound) {
                if (y > (this.mHeight + this.mLowerBound) / 2) {
                    speed = 16;
                } else {
                    speed = 2;
                }
            } else if (y < this.mUpperBound) {
                speed = y < this.mUpperBound / 2 ? -16 : -2;
            }
            if (speed != 0) {
                int ref = pointToPosition(0, this.mHeight / 2);
                if (ref == -1) {
                    ref = pointToPosition(0, ((this.mHeight / 2) + getDividerHeight()) + 64);
                }
                View v = getChildAt(ref - getFirstVisiblePosition());
                if (v != null) {
                    setSelectionFromTop(ref, v.getTop() - speed);
                }
            }
        }
    }

    private void adjustScrollBounds(int y) {
        if (y >= this.mHeight / 3) {
            this.mUpperBound = this.mHeight / 3;
        }
        if (y <= (this.mHeight * 2) / 3) {
            this.mLowerBound = (this.mHeight * 2) / 3;
        }
    }

    private int getItemForPosition(int y) {
        int adjustedy = (y - this.mDragPoint) - 32;
        int pos = myPointToPosition(0, adjustedy);
        if (pos >= 0) {
            if (pos <= this.mFirstDragPos) {
                return pos + 1;
            }
            return pos;
        } else if (adjustedy < 0) {
            return 0;
        } else {
            return pos;
        }
    }

    private int myPointToPosition(int x, int y) {
        Rect frame = this.mTempRect;
        for (int i = getChildCount() - 1; i >= 0; i--) {
            getChildAt(i).getHitRect(frame);
            if (frame.contains(x, y)) {
                return getFirstVisiblePosition() + i;
            }
        }
        return -1;
    }

    public void onDragLocal(int x, int y, int startPostion) {
        ListAdapter adapter = this.mDragListener.getMyListAdapter();
        if (this.mIsFirstLoop) {
            this.mPrevPosition = startPostion;
            this.mIsFirstLoop = false;
        }
        int position = pointToPosition(x, y);
        if (position != -1 && ((this.mInitialDirection == this.DIRECTION_DOWN && position < startPostion) || (this.mInitialDirection == this.DIRECTION_UP && position > startPostion))) {
            this.mPrevPosition = startPostion;
            this.mInitialDirection = this.DIRECTION_INVALID;
            this.mIsInitialDirectionFound = false;
        }
        if (position != -1) {
            if (this.mPrevPosition == position) {
                this.direction = this.DIRECTION_NEUTRAL;
            } else if (this.mPrevPosition > position) {
                this.direction = this.DIRECTION_UP;
            } else {
                this.direction = this.DIRECTION_DOWN;
            }
            Object displacedItem;
            View itemView;
            if (!this.mDisableUp && this.direction == this.DIRECTION_UP) {
                if (getChildAt(position - getFirstVisiblePosition()).getHeight() > getChildAt((position + 1) - getFirstVisiblePosition()).getHeight()) {
                    this.mDisableDown = true;
                }
                displacedItem = blankCurrentListItem(this, adapter, position);
                if (isDirectionNotEqualToOriginalDirection()) {
                    displacedItem = adapter.getItem(position + 1);
                }
                itemView = getChildAt((position + 1) - getFirstVisiblePosition());
                if (itemView != null) {
                    itemView.setVisibility(VISIBLE);
                    if (this.mDragListener != null) {
                        this.mDragListener.setDisplacedListItem(displacedItem, itemView);
                    }
                }
            } else if (!this.mDisableDown && this.direction == this.DIRECTION_DOWN) {
                if (getChildAt(position - getFirstVisiblePosition()).getHeight() > getChildAt((position - 1) - getFirstVisiblePosition()).getHeight()) {
                    this.mDisableUp = true;
                }
                displacedItem = blankCurrentListItem(this, adapter, position);
                if (isDirectionNotEqualToOriginalDirection()) {
                    displacedItem = adapter.getItem(position - 1);
                }
                itemView = getChildAt((position - 1) - getFirstVisiblePosition());
                if (itemView != null) {
                    itemView.setVisibility(VISIBLE);
                    if (this.mDragListener != null) {
                        this.mDragListener.setDisplacedListItem(displacedItem, itemView);
                    }
                }
            } else if (this.direction == this.DIRECTION_NEUTRAL) {
            }
            if (!(this.mIsInitialDirectionFound || this.direction == this.DIRECTION_NEUTRAL)) {
                this.mInitialDirection = this.direction;
                this.mIsInitialDirectionFound = true;
            }
            this.mPrevPosition = position;
        }
    }

    private boolean isDirectionNotEqualToOriginalDirection() {
        return (this.mInitialDirection == this.DIRECTION_INVALID || this.mInitialDirection == this.direction) ? false : true;
    }

    private Object blankCurrentListItem(ListView listView, ListAdapter adapter, int position) {
        View itemView = listView.getChildAt(position - listView.getFirstVisiblePosition());
        if (itemView != null) {
            itemView.setVisibility(VISIBLE);
            if (this.mDragListener != null) {
                this.mDragListener.blankOutDraggedItemListItem(itemView);
            }
        }
        return adapter.getItem(position);
    }

    private void drag(int x, int y) {
        if (this.mDragView != null) {
            LayoutParams layoutParams = (LayoutParams) this.mDragView.getLayoutParams();
            if (this.mIsRightDrag) {
                layoutParams.width = 0;
            } else {
                layoutParams.width = 0;
            }
            layoutParams.height = y - this.mDragPointOffset;
            ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE)).updateViewLayout(this.mDragView, layoutParams);
            onDragLocal(x, y, this.mStartPosition);
        }
    }

    private void startDrag(int itemIndex, int y) {
        stopDrag(itemIndex);
        View item = getChildAt(itemIndex);
        if (item != null) {
            item.setDrawingCacheEnabled(true);
            if (this.mDragListener != null) {
                this.mDragListener.onStartDrag(item, itemIndex);
            }
            Bitmap bitmap = Bitmap.createBitmap(item.getDrawingCache());
            LayoutParams mWindowParams = new LayoutParams(-2, -2);
            mWindowParams.height = -2;
            mWindowParams.width = -2;
            Context context = getContext();
            ImageView v = new ImageView(context);
            v.setImageBitmap(bitmap);
            ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).addView(v, mWindowParams);
            this.mDragView = v;
        }
    }

    private void stopDrag(int itemIndex) {
        if (this.mDragView != null) {
            if (this.mDragListener != null) {
                this.mDragListener.onStopDrag(getChildAt(itemIndex));
            }
            this.mDragView.setVisibility(GONE);
            ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE)).removeView(this.mDragView);
            this.mDragView.setImageDrawable(null);
            this.mDragView = null;
        }
        this.mPrevPosition = 0;
        this.mInitialDirection = 99;
        this.mIsInitialDirectionFound = false;
        this.mIsFirstLoop = true;
    }

    private GestureDetector createFlingDetector() {
        return new GestureDetector(getContext(), new C17971());
    }
}
