package com.irt.tasks.ui.taskslist;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.SpannableStringBuilder;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessobjects.LevelState;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.AccountBp;
import com.irt.tasks.app.businessprocess.AccountBpImpl;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.businessprocess.TaskStateBp;
import com.irt.tasks.app.businessprocess.TaskStateBpImpl;
import com.irt.tasks.app.businessprocess.TasksListBp;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.CSVWriter;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.Constants.HORIZONTAL_DIRECTION;
import com.irt.tasks.app.common.Constants.VERTICAL_VIEW_SECTIONS;
import com.irt.tasks.app.common.EmailUtility;
import com.irt.tasks.app.common.ErrorReporter;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.business.ErrorTypes;
import com.irt.tasks.app.framework.business.ErrorTypes.ERROR;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.app.framework.config.ScreenSizes;
import com.irt.tasks.app.framework.trial.TrialSpecificUtility;
import com.irt.tasks.ui.dragndrop.DragListener;
import com.irt.tasks.ui.dragndrop.DragNDropListView;
import com.irt.tasks.ui.dragndrop.DropListener;
import com.irt.tasks.ui.dragndrop.RemoveListener;
import com.irt.tasks.ui.help.HelpUtility;
import com.irt.tasks.ui.help.HelpUtility.HelpDialog;
import com.irt.tasks.ui.panel.EditActionDetail.EditActions;
import com.irt.tasks.ui.panel.EditManager;
import com.irt.tasks.ui.preferences.ArraysConstants;
import com.irt.tasks.ui.receiver.AlarmSetterHelper;
import com.irt.tasks.ui.search.SearchProvider;
import com.irt.tasks.ui.task.EditTaskActivity;
import com.irt.tasks.ui.task.TaskViewActivity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TasksListActivity extends AbstractTasksListActivity {
    private static final int TASK_POSITION_FROM_TOP = 3;
    int NUMBER_OF_LISTS = 2;
    AccountBp accountBp = new AccountBpImpl();
    ConfigManager config;
    EditManager editManager;
    private GestureDetector gestureDetector;
    private SpannableStringBuilder[] mAccountEmails = null;
    List<Account> mAccounts;
    Bitmap mBlankBitMap;
    private ToggleButton mCopy;
    private ToggleButton mCut;
    private ToggleButton mDelete;
    private ImageButton mDownImageButton;
    private DragListener mDragListener = new C19517();
    private DropListener mDropListener = new C19484();
    TableLayout mEditPanelTableLayout;
    Map<String, Long> mExpandedTasks;
    ImageView mHelpImageView;
    boolean mIsDecoratorSelected = false;
    boolean mIsLongPress = false;
    boolean mIsNotifcationConsumed = false;
    boolean mIsRightTap = false;
    boolean mIsSearchConsumed = false;
    private ImageButton mLeftImageButton;
    TextView mLevelInfoTextView;
    LevelState mLevelState;
    boolean mListItemSelected = false;
    TableLayout mMovePanelTableLayout;
    private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
        public void onItemClick(AdapterView a, View v, int position, long id) {
            TasksListActivity.this.markAsNOTSelected();
            if (TasksListActivity.this.editManager.isDestinationSelected() || !TasksListActivity.this.config.getPanelToShow().equals(Integer.valueOf(1))) {
                TasksListActivity.this.markAsNotForEdit();
            }
            TasksListActivity.this.setTaskPosition(position);
            if (!(TasksListActivity.this.config.getPanelToShow().equals(Integer.valueOf(1)) && TasksListActivity.this.mSelect.isChecked())) {
                TasksListActivity.this.markAsSelected();
            }
            if (TasksListActivity.this.isNextActionAllowed()) {
                TasksListActivity.this.setTapTime();
            } else {
                TasksListActivity.this.resetTapTime();
            }
        }
    };
    private ToggleButton mPaste;
    ImageView mPlusImageView;
    Task mPrevSelectedTask;
    String mQueryString;
    private RemoveListener mRemoveListener = new C19506();
    private ImageButton mRightImageButton;
    private ToggleButton mSelect;
    long mTapTime = 0;
    List<Task> mTasksAtMaxLevel;
    int mTasksCurrentMaxLevel;
    String[] mTasksListNames;
    List<TasksList> mTasksLists;
    private ImageButton mUpImageButton;
    VERTICAL_VIEW_SECTIONS mVerticalViewSection = VERTICAL_VIEW_SECTIONS.BOTTOM;
    private int mWhichTasksList;
    RelativeLayout mZoomHeaderLayout;
    ImageView mZoomOutAllImageView;
    ImageView mZoomOutImageView;
    TextView mZoomTitleTextView;
    TaskBp taskBp = new TaskBpImpl();
    TaskStateBp taskStateBp = new TaskStateBpImpl();
    TasksListBp tasksListBp = new TasksListBpImpl();

    class C19451 implements OnClickListener {
        C19451() {
        }

        public void onClick(View v) {
            HelpUtility.showHelp(TasksListActivity.this, HelpDialog.TASKS_TREE);
        }
    }

    class C19462 implements OnClickListener {
        C19462() {
        }

        public void onClick(View v) {
            TasksListActivity.this.vibrate();
            TasksListActivity.this.mLevelState.setZoomTaskIdStr(null);
            TasksListActivity.this.refreshDisplay();
        }
    }

    class C19473 implements OnClickListener {
        C19473() {
        }

        public void onClick(View v) {
            TasksListActivity.this.vibrate();
            if (TasksListActivity.this.mLevelState.getZoomTask().getParentTaskId().equals(Long.valueOf(0))) {
                TasksListActivity.this.mLevelState.setZoomTaskIdStr(null);
            } else {
                TasksListActivity.this.mLevelState.setZoomTaskIdStr(TasksListActivity.this.mLevelState.getZoomTask().getParentTaskId() + "");
            }
            TasksListActivity.this.refreshDisplay();
        }
    }

    class C19484 implements DropListener {
        C19484() {
        }

        public void onDrop(int from, int to) {
            ListAdapter adapter = TasksListActivity.this.getListAdapter();
            Task task = (Task) adapter.getItem(from);
            if (!task.isTasksListAsTask()) {
                TasksListActivity.this.setTaskPosition(to);
                TasksListActivity.this.turnONTaskSelector(task.getId(), false);
                int toTaskposition = to - 1;
                boolean isFirstTask = isFirstTask(to);
                if (isFirstTask || from < to) {
                    toTaskposition = to;
                }
                boolean isCrossListEdit = false;
                if (TasksListActivity.this.validateCutAndPaste(task, (Task) TasksListActivity.this.mTasks.get(toTaskposition))) {
                    if (task.getTasksListId().equals(((Task) TasksListActivity.this.mTasks.get(toTaskposition)).getTasksListId())) {
                        isCrossListEdit = false;
                    } else {
                        isCrossListEdit = true;
                    }
                    TasksListActivity.this.taskBp.cutAndPasteBranch(task, (Task) TasksListActivity.this.mTasks.get(toTaskposition), TasksListActivity.this.isTaskExpanded((Task) TasksListActivity.this.mTasks.get(toTaskposition)), isFirstTask);
                }
                if (adapter instanceof TasksListAdapter) {
                    ((TasksListAdapter) adapter).onDrop(from, to);
                    TasksListActivity.this.getListView().invalidateViews();
                }
                if (isCrossListEdit) {
                    TasksListActivity.this.setTaskPositionByTask(task);
                    TasksListActivity.this.turnONTaskSelector(null, false);
                }
            }
            TasksListActivity.this.refreshDisplay(null);
        }

        private boolean isFirstTask(int to) {
            return to == 0;
        }
    }

    class C19495 implements DialogInterface.OnClickListener {
        C19495() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
        }
    }

    class C19506 implements RemoveListener {
        C19506() {
        }

        public void onRemove(int which) {
            ListAdapter adapter = TasksListActivity.this.getListAdapter();
            if (adapter instanceof TasksListAdapter) {
                ((TasksListAdapter) adapter).onRemove(which);
                TasksListActivity.this.getListView().invalidateViews();
            }
        }
    }

    class C19517 implements DragListener {
        int backgroundColor = -535810032;
        int defaultBackgroundColor;

        C19517() {
        }

        public void onDrag(int x, int y, ListView listView, int startPostion) {
        }

        public void setDisplacedListItem(Object displacedItem, View itemView) {
            ImageView iv = (ImageView) itemView.findViewById(R.id.ImageView01);
            if (iv != null) {
                iv.setVisibility(View.VISIBLE);
            }
            TasksListAdapterHelper.getView((Task) displacedItem, itemView, null, TasksListActivity.this, 0, TasksListActivity.this.mLevelState, TasksListActivity.this.mExpandedTasks, TasksListActivity.this.mTaskDecorator);
        }

        public void blankOutDraggedItemListItem(View itemView) {
            ((TextView) itemView.findViewById(R.id.task_name)).setText("");
            ((CheckBox) itemView.findViewById(R.id.done_checkbox)).setVisibility(View.INVISIBLE);
            ((ImageView) itemView.findViewById(R.id.note)).setImageBitmap(TasksListActivity.this.mBlankBitMap);
            ((TextView) itemView.findViewById(R.id.task_number)).setText("");
            ((ImageView) itemView.findViewById(R.id.plus_minus_dot_img)).setImageBitmap(TasksListActivity.this.mBlankBitMap);
        }

        public void onStartDrag(View itemView, int position) {
            itemView.setVisibility(View.INVISIBLE);
            ImageView iv = (ImageView) itemView.findViewById(R.id.ImageView01);
            if (iv != null) {
                iv.setVisibility(View.INVISIBLE);
            }
            TasksListActivity.this.mListItemSelected = true;
            TasksListActivity.this.markAsNOTSelected();
            //itemView.setBackgroundColor(-256);
            TasksListActivity.this.setTaskPosition(position);
        }

        public void onStopDrag(View itemView) {
            if (itemView != null) {
                itemView.setVisibility(View.VISIBLE);
                ((TextView) itemView.findViewById(R.id.task_name)).setVisibility(View.VISIBLE);
                CheckBox isCompleted = (CheckBox) itemView.findViewById(R.id.done_checkbox);
                ImageView note = (ImageView) itemView.findViewById(R.id.note);
                TextView taskNumber = (TextView) itemView.findViewById(R.id.task_number);
                ((ImageView) itemView.findViewById(R.id.plus_minus_dot_img)).setVisibility(View.VISIBLE);
                if (!TasksListActivity.this.getTask().isTasksListAsTask()) {
                    ImageView iv = (ImageView) itemView.findViewById(R.id.ImageView01);
                    if (iv != null) {
                        iv.setVisibility(View.VISIBLE);
                    }
                }
                TasksListActivity.this.markAsSelected();
                TasksListActivity.this.turnONTaskSelector(null, false);
            }
        }

        public ListAdapter getMyListAdapter() {
            return TasksListActivity.this.getListAdapter();
        }
    }

    class C19528 implements OnClickListener {
        C19528() {
        }

        public void onClick(View v) {
            TasksListActivity.this.performMoveLeft();
        }
    }

    class C19539 implements OnClickListener {
        C19539() {
        }

        public void onClick(View v) {
            TasksListActivity.this.performMoveRight();
        }
    }

    class MyGestureDetector extends SimpleOnGestureListener {
        MyGestureDetector() {
        }

        public void onLongPress(MotionEvent e) {
            TasksListActivity.this.mIsLongPress = true;
            super.onLongPress(e);
        }

        public boolean onDoubleTap(MotionEvent e) {
            TasksListActivity.this.mIsLongPress = false;
            if (TasksListActivity.this.mIsRightTap) {
                TasksListActivity.this.performTaskFocusedAction(TasksListActivity.this.config.getRightDoubleTapAction().intValue());
            } else {
                TasksListActivity.this.performTaskFocusedAction(TasksListActivity.this.config.getLeftDoubleTapAction().intValue());
            }
            return true;
        }

        public boolean onSingleTapConfirmed(MotionEvent e) {
            TasksListActivity.this.mIsLongPress = false;
            if (!TasksListActivity.this.mIsRightTap) {
                TasksListActivity.this.performTaskFocusedAction(TasksListActivity.this.config.getLeftTapAction().intValue());
            } else if (TasksListActivity.this.config.getPanelToShow().equals(Integer.valueOf(1)) && TasksListActivity.this.mSelect.isChecked()) {
                TasksListActivity.this.editManager.processSelectedTask(TasksListActivity.this.getTask(), TasksListActivity.this.isTaskExpanded(null));
                TasksListActivity.this.markForEdit();
            } else if (TasksListActivity.this.config.getPanelToShow().equals(Integer.valueOf(2))) {
                TasksListActivity.this.turnONTaskSelector(null, false);
            } else if (TasksListActivity.this.config.isDragAndDropMode() && TasksListActivity.this.config.isSwipeMoveLeftRightOnDragAndDropMode()) {
                TasksListActivity.this.turnONTaskSelector(null, false);
            } else {
                TasksListActivity.this.performTaskFocusedAction(TasksListActivity.this.config.getRightTapAction().intValue());
            }
            return true;
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) > 250.0f || Math.abs(e1.getX() - e2.getX()) < ((float) TasksListGestureHelper.CONSIDERED_AS_HORIZONTAL_SWIPE_DISTANCE) || Math.abs(e1.getY() - e2.getY()) > ((float) TasksListGestureHelper.CONSIDERED_AS_VERITCAL_SWIPE_DISTANCE)) {
                    return false;
                }
                if (e1.getX() - e2.getX() > BitmapDescriptorFactory.HUE_GREEN && Math.abs(velocityX) > 200.0f) {
                    switch (TasksListActivity.this.mVerticalViewSection) {
                        case TOP:
                            TasksListActivity.this.performSwipeAction(TasksListActivity.this.config.getTopLeftRightSwipeAction().intValue(), HORIZONTAL_DIRECTION.RIGHT);
                            break;
                        case MIDDLE:
                            TasksListActivity.this.performSwipeAction(TasksListActivity.this.config.getMiddleLeftRightSwipeAction().intValue(), HORIZONTAL_DIRECTION.RIGHT);
                            break;
                        case BOTTOM:
                            TasksListActivity.this.performSwipeAction(TasksListActivity.this.config.getBottomLeftRightSwipeAction().intValue(), HORIZONTAL_DIRECTION.RIGHT);
                            break;
                    }
                    return true;
                } else if (e2.getX() - e1.getX() <= BitmapDescriptorFactory.HUE_GREEN || Math.abs(velocityX) <= 200.0f) {
                    return false;
                } else {
                    switch (TasksListActivity.this.mVerticalViewSection) {
                        case TOP:
                            TasksListActivity.this.performSwipeAction(TasksListActivity.this.config.getTopLeftRightSwipeAction().intValue(), HORIZONTAL_DIRECTION.LEFT);
                            break;
                        case MIDDLE:
                            TasksListActivity.this.performSwipeAction(TasksListActivity.this.config.getMiddleLeftRightSwipeAction().intValue(), HORIZONTAL_DIRECTION.LEFT);
                            break;
                        case BOTTOM:
                            TasksListActivity.this.performSwipeAction(TasksListActivity.this.config.getBottomLeftRightSwipeAction().intValue(), HORIZONTAL_DIRECTION.LEFT);
                            break;
                    }
                    return true;
                }
            } catch (Exception e) {
                return false;
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.tasks_list);
        this.config = ConfigManager.getInstance();
        setSelectedTasksList();
        if (this.mTasksList == null) {
            goBackToHomeView();
            return;
        }
        this.config.setLastSelectedTasksList(this.mTasksList.getId());
        if (this.mTasksList.getId().equals(DatabaseHelper.ALL_TASKS_LIST_ID)) {
            this.config.setViewFromHome(Boolean.valueOf(false));
        } else {
            this.config.setViewFromHome(Boolean.valueOf(true));
        }
        this.mExpandedTasks = this.taskStateBp.getTaskStateMapByTasksListId(this.mTasksList.getId());
        this.mLevelState = this.taskStateBp.getLevelSate(this.mTasksList.getId(), null);
        unzoomForCalendarAndNotificationActions();
        setTasksCurrentMaxLevel();
        if (!isFromView()) {
            initTasks();
        }
        this.mBlankBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.blank);
        this.mHelpImageView = (ImageView) findViewById(R.id.help_image);
        this.mHelpImageView.setOnClickListener(new C19451());
        this.mTitle = (TextView) findViewById(R.id.title);
        setLevelInfo();
        setPanel();
        setScreenSpecificGestureParameters();
        setListOnTouchListener();
        setListListeners();
        this.gestureDetector = new GestureDetector(this, new MyGestureDetector());
        setNewTaskSource();
        setPanelButtons();
        setPlusImage();
        setZoomLayout();
        ListView listView = getListView();
        if (listView instanceof DragNDropListView) {
            ((DragNDropListView) listView).enableDragMode(this.config.isDragAndDropMode());
            ((DragNDropListView) listView).setRightDrag();
            ((DragNDropListView) listView).setDropListener(this.mDropListener);
            ((DragNDropListView) listView).setRemoveListener(this.mRemoveListener);
            ((DragNDropListView) listView).setDragListener(this.mDragListener);
        }
        EmailUtility.initIndentions();
        this.mFirstVisibleTask = this.mLevelState.getPosition().intValue();
    }

    private void unzoomForCalendarAndNotificationActions() {
        if ((isFromCalendar() || (!this.mIsNotifcationConsumed && isIntentFromNotification())) && this.mLevelState != null) {
            this.mLevelState.setLevel(Integer.valueOf(20));
            this.mLevelState.setPosition(Integer.valueOf(this.mFirstVisibleTask));
            this.mLevelState.setZoomTaskIdStr(null);
            this.taskStateBp.saveOrUpdateLevelState(this.mLevelState);
        }
    }

    private void setZoomLayout() {
        this.mZoomHeaderLayout = (RelativeLayout) findViewById(R.id.zoom_header_layout);
        this.mZoomTitleTextView = (TextView) findViewById(R.id.zoom_title);
        this.mZoomOutAllImageView = (ImageView) findViewById(R.id.zoom_out_all_image);
        this.mZoomOutAllImageView.setOnClickListener(new C19462());
        this.mZoomOutImageView = (ImageView) findViewById(R.id.zoom_out_image);
        this.mZoomOutImageView.setOnClickListener(new C19473());
    }

    private void setZoomVisibility() {
        if (AppUtil.isZoomSet(this.mLevelState)) {
            this.mZoomHeaderLayout.setVisibility(View.VISIBLE);
        } else {
            this.mZoomHeaderLayout.setVisibility(View.GONE);
        }
    }

    private boolean validateCutAndPaste(Task task, Task targetTask) {
        ERROR errorType = this.taskBp.validateCutAndPaste(task, targetTask, this.mTasksAtMaxLevel);
        if (errorType.equals(ERROR.NO_ERROR)) {
            return true;
        }
        showErrorMessage(ErrorTypes.getErrorMessage(errorType));
        return false;
    }

    private void showErrorMessage(String errorMessage) {
        new Builder(this).setTitle("Please check").setMessage(errorMessage).setPositiveButton("OK", new C19495()).show();
    }

    private void setPanelButtons() {
        setPanelEditButtons();
        setPanelMoveButtons();
    }

    private void setPanelMoveButtons() {
        this.mLeftImageButton = (ImageButton) findViewById(R.id.left_button);
        this.mLeftImageButton.setOnClickListener(new C19528());
        this.mRightImageButton = (ImageButton) findViewById(R.id.right_button);
        this.mRightImageButton.setOnClickListener(new C19539());
        this.mUpImageButton = (ImageButton) findViewById(R.id.up_button);
        this.mUpImageButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Task task = TasksListActivity.this.getTask();
                if (!task.isTasksListAsTask() && TasksListActivity.this.mSelectedTaskPosition != 0) {
                    TasksListActivity.this.taskBp.moveUp(task, TasksListActivity.this.mTasksAtMaxLevel, TasksListActivity.this.mExpandedTasks);
                    TasksListActivity.this.refreshDisplay(null);
                    TasksListActivity.this.setTaskPositionByTask(task);
                    TasksListActivity.this.turnONTaskSelector(null, false);
                    TasksListActivity.this.setWindowTitle();
                }
            }
        });
        this.mDownImageButton = (ImageButton) findViewById(R.id.down_button);
        this.mDownImageButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Task task = TasksListActivity.this.getTask();
                if (!task.isTasksListAsTask()) {
                    TasksListActivity.this.taskBp.moveDown(task, TasksListActivity.this.mTasksAtMaxLevel, TasksListActivity.this.mExpandedTasks);
                    TasksListActivity.this.refreshDisplay(null);
                    TasksListActivity.this.setTaskPositionByTask(task);
                    TasksListActivity.this.turnONTaskSelector(null, false);
                    TasksListActivity.this.setWindowTitle();
                }
            }
        });
    }

    private void performMoveLeft() {
        Task task = getTask();
        if (!task.isTasksListAsTask()) {
            this.taskBp.moveLeft(task, this.mTasksAtMaxLevel, this.mExpandedTasks);
            refreshDisplay(null);
            setTaskPositionByTask(task);
            setWindowTitle();
        }
    }

    private void performMoveRight() {
        Task task = getTask();
        if (!task.isTasksListAsTask()) {
            if (task.isParent()) {
                this.mExpandedTasks.put(AppUtil.getExpandedTaskId(task), Constants.TASK_EXPAND_INDICATOR);
            }
            this.taskBp.moveRight(task, this.mTasksAtMaxLevel, this.mExpandedTasks);
            refreshDisplay(null);
            setTaskPositionByTask(task);
            setWindowTitle();
        }
    }

    private void setPanelEditButtons() {
        this.mSelect = (ToggleButton) findViewById(R.id.select_button);
        this.mCut = (ToggleButton) findViewById(R.id.cut_button);
        this.mCopy = (ToggleButton) findViewById(R.id.copy_button);
        this.mPaste = (ToggleButton) findViewById(R.id.paste_button);
        this.mDelete = (ToggleButton) findViewById(R.id.delete_button);
        this.editManager = EditManager.getInstance(this.mSelect, this.mCut, this.mCopy, this.mPaste, this.mDelete);
        this.mSelect.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                TasksListActivity.this.editManager.processAction(EditActions.SELECT);
                if (!isChecked) {
                    TasksListActivity.this.refreshDisplay(null);
                }
            }
        });
        this.mCut.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                TasksListActivity.this.editManager.processAction(EditActions.CUT);
                TasksListActivity.this.refreshDisplay(null);
            }
        });
        this.mCopy.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                TasksListActivity.this.editManager.processAction(EditActions.COPY);
                TasksListActivity.this.refreshDisplay(null);
            }
        });
        this.mPaste.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                TasksListActivity.this.editManager.setSortedTasks(TasksListActivity.this.mTasksAtMaxLevel);
                ERROR errorType = TasksListActivity.this.editManager.processAction(EditActions.PASTE);
                if (!errorType.equals(ERROR.NO_ERROR)) {
                    TasksListActivity.this.showErrorMessage(ErrorTypes.getErrorMessage(errorType));
                }
                TasksListActivity.this.turnONTaskSelector(null, false);
                TasksListActivity.this.refreshDisplay(null);
            }
        });
        this.mDelete.setOnClickListener(new OnClickListener() {

            class C19441 implements DialogInterface.OnClickListener {
                C19441() {
                }

                public void onClick(DialogInterface dialog, int button) {
                    TasksListActivity.this.editManager.setSortedTasks(TasksListActivity.this.getTasksAtMaxLevelWithCompetedTasks());
                    TasksListActivity.this.editManager.processAction(EditActions.DELETE);
                    TasksListActivity.this.refreshDisplay(null);
                }
            }

            public void onClick(View v) {
                TasksListActivity.this.mDelete.setChecked(true);
                new Builder(TasksListActivity.this).setTitle(R.string.delete_title).setMessage(R.string.delete_this_tasks).setPositiveButton(R.string.alert_dialog_ok, new C19441()).setNegativeButton(R.string.alert_dialog_cancel, null).show();
            }
        });
    }

    private void performMarkAsCopy() {
        performMartAsCutOrPaste(EditActions.COPY);
    }

    private void performMarkAsCut() {
        performMartAsCutOrPaste(EditActions.CUT);
    }

    private void performMartAsCutOrPaste(EditActions editAction) {
        if (isTasksMarkedForEditAction(editAction)) {
            this.editManager.reset();
        } else {
            this.editManager.reset();
            this.editManager.processSelectedTask(getTask(), isTaskExpanded(null));
            this.editManager.processAction(editAction);
        }
        refreshDisplay(null);
    }

    private boolean isTasksMarkedForEditAction(EditActions editAction) {
        return this.editManager.isTaskMarkedForEdit(getTask()) && this.editManager.isFirstEditAction(editAction);
    }

    private void performPaste() {
        if (!this.editManager.isTaskMarkedForEdit(getTask()) && (this.editManager.isFirstEditAction(EditActions.CUT) || this.editManager.isFirstEditAction(EditActions.COPY))) {
            this.editManager.processSelectedTask(getTask(), isTaskExpanded(null));
            this.editManager.setSortedTasks(this.mTasksAtMaxLevel);
            ERROR errorType = this.editManager.processAction(EditActions.PASTE);
            if (errorType.equals(ERROR.NO_ERROR)) {
                this.editManager.reset();
            } else {
                showErrorMessage(ErrorTypes.getErrorMessage(errorType));
            }
            turnONTaskSelector(null, false);
        }
        refreshDisplay(null);
    }

    private List<Task> getTasksAtMaxLevelWithCompetedTasks() {
        List<Task> tasksAtMaxLevelWithCompletedTasks = this.mTasksAtMaxLevel;
        if (!this.config.isHideCompletedTasks()) {
            return tasksAtMaxLevelWithCompletedTasks;
        }
        boolean isHideCompletedTasks = this.config.isHideCompletedTasks();
        this.config.setHideCompletedTasks(false);
        tasksAtMaxLevelWithCompletedTasks = this.taskBp.getTasksByLevel(null, 20, false, this.mTasksList, true);
        this.config.setHideCompletedTasks(isHideCompletedTasks);
        return tasksAtMaxLevelWithCompletedTasks;
    }

    private void setNewTaskSource() {
        Intent intent = getIntent();
        if (!StringUtil.isNullOrEmpty(intent.getStringExtra(Constants.IS_PLUS_NEW_TASK))) {
            performAddNewTaskUsingPlus();
        }
        if (!StringUtil.isNullOrEmpty(intent.getStringExtra(Constants.IS_SEARCH_FROM_HOME))) {
            onSearchRequested();
        }
    }

    private void setPanel() {
        this.mEditPanelTableLayout = (TableLayout) findViewById(R.id.edit_panel);
        this.mMovePanelTableLayout = (TableLayout) findViewById(R.id.move_panel);
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    public void setPlusImage() {
        switch (this.config.getPanelToShow().intValue()) {
            case 0:
                this.mEditPanelTableLayout.setVisibility(View.GONE);
                turnOffMovePanel();
                this.editManager.reset();
                break;
            case 1:
                this.mEditPanelTableLayout.setVisibility(View.VISIBLE);
                turnOffMovePanel();
                break;
            case 2:
                this.mEditPanelTableLayout.setVisibility(View.GONE);
                this.mMovePanelTableLayout.setVisibility(View.VISIBLE);
                if (this.config.isPaneledDragAndDrop()) {
                    this.config.setDragAndDropMode(Boolean.valueOf(true));
                    break;
                }
                break;
            default:
                this.mEditPanelTableLayout.setVisibility(View.GONE);
                turnOffMovePanel();
                this.editManager.reset();
                break;
        }
        if (this.mPlusImageView != null) {
            this.mPlusImageView.setVisibility(View.GONE);
        }
        if (this.config.getPanelToShow().equals(Integer.valueOf(0))) {
            this.mPlusImageView = (ImageView) findViewById(R.id.plus);
        } else {
            this.mPlusImageView = (ImageView) findViewById(R.id.plus_panel);
        }
        this.mPlusImageView.setVisibility(View.VISIBLE);
        this.mPlusImageView.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                TasksListActivity.this.performAddNewTaskUsingPlus();
            }
        });
    }

    private void turnOffMovePanel() {
        this.mMovePanelTableLayout.setVisibility(View.GONE);
        if (this.config.isPaneledDragAndDrop() && this.config.getPanelToShow().equals(Integer.valueOf(0))) {
            this.config.setDragAndDropMode(Boolean.valueOf(false));
        }
    }

    public void performAddNewTaskUsingPlus() {
        turnONTaskSelector(getTasksListIdForPlusNewTask(), true);
        Task task = new Task();
        task.setTasksListAsTask(true);
        task.setTasksListId(getTasksListIdForPlusNewTask());
        if (AppUtil.isZoomSet(this.mLevelState)) {
            task.setId(this.mLevelState.getZoomTask().getId());
            task.setTasksListAsTask(false);
        }
        ArchitectureContext.setObject(task);
        Intent intent = new Intent(this, EditTaskActivity.class);
        intent.putExtra(Constants.IS_NEW_TASK, Constants.TRUE);
        intent.putExtra(Constants.IS_CHILD_TASK, Constants.TRUE);
        startActivityForResult(intent, 1);
    }

    private Long getTasksListIdForPlusNewTask() {
        if (this.mTasksList.getId().equals(DatabaseHelper.ALL_TASKS_LIST_ID) && !this.config.getTasksListForNewTaskPlus().equals(Constants.TASKS_LIST_AUTO_ID)) {
            for (Task task : this.mTasks) {
                if (task.getTasksListId().equals(this.config.getTasksListForNewTaskPlus())) {
                    return task.getTasksListId();
                }
            }
        }
        if (this.mTasks.size() == 0) {
            return this.config.getLastSelectedTasksList();
        }
        return getTask().getTasksListId();
    }

    private void setSelectedTasksList() {
        Object object = ArchitectureContext.getObject();
        if (isFromCalendar()) {
            this.mTasksList = this.tasksListBp.get(Long.valueOf(getIntent().getLongExtra(Constants.INTENT_CALENDAR_TASK_LIST_ID, 0)));
        } else if (isIntentFromNotification()) {
            this.mTasksList = this.tasksListBp.get(Long.valueOf(getIntent().getLongExtra("taskListId", 0)));
        } else if (object != null && (object instanceof TasksList)) {
            this.mTasksList = (TasksList) object;
        } else if (object != null && (object instanceof Task)) {
            this.mTasksList = this.tasksListBp.get(this.config.getLastSelectedTasksList());
        }
    }

    private boolean isFromCalendar() {
        return !StringUtil.isNullOrEmpty(getIntent().getStringExtra(Constants.INTENT_FROM_CALENDAR_FOR_LIST));
    }

    private boolean isIntentFromNotification() {
        if (!isFromCalendar() && getIntent().getLongExtra("taskListId", 0) > 0) {
            return true;
        }
        return false;
    }

    private boolean isFromView() {
        if (!"android.intent.action.VIEW".equals(getIntent().getAction())) {
            return false;
        }
        Bundle extras = getIntent().getExtras();
        if (extras == null || StringUtil.isNullOrEmpty((String) extras.get(Constants.IS_FROM_VIEW))) {
            return false;
        }
        return true;
    }

    private void setSelectedTaskFromSearch() {
        String query = "";
        Intent intent = getIntent();
        if (!this.mIsSearchConsumed && "android.intent.action.VIEW".equals(intent.getAction())) {
            String realId;
            Long taskIdSearched = null;
            this.mIsSearchConsumed = true;
            query = intent.getDataString();
            boolean isTasksListAsTask = false;
            if (query.contains(Constants.TASK_LIST)) {
                realId = query.split(Constants.TASK_LIST)[1];
                isTasksListAsTask = true;
            } else {
                realId = query;
            }
            Long l = new Long(realId);
            Bundle extras = getIntent().getExtras();
            List<Task> tasks = SearchProvider.mTasks;
            SearchProvider.mTasks = null;
            if (extras != null) {
                if (!StringUtil.isNullOrEmpty((String) extras.get(Constants.IS_FROM_VIEW))) {
                    tasks = (List) ArchitectureContext.getObjectExta();
                }
                if (!StringUtil.isNullOrEmpty((String) extras.get(Constants.INTENT_FROM_CALENDAR_FOR_LIST))) {
                    taskIdSearched = Long.valueOf(getIntent().getLongExtra(Constants.INTENT_CALENDAR_TASK_ID, 0));
                    tasks = this.taskBp.getTasksByLevel(null, 20, false, this.mTasksList, true);
                }
            }
            if (tasks != null) {
                for (Task task : tasks) {
                    if (isTasksListAsTask) {
                        if (task.getTasksListId().equals(taskIdSearched)) {
                            postSearchInitialization(taskIdSearched, Integer.valueOf(1));
                            turnONTaskSelector(task.getTasksListId(), true);
                            break;
                        }
                    } else if (task.getId().equals(taskIdSearched)) {
                        postSearchInitialization(taskIdSearched, task.getLevel());
                        turnONTaskSelector(task.getId(), false);
                        break;
                    }
                }
            }
        } else if ("android.intent.action.SEARCH".equals(intent.getAction())) {
            query = intent.getStringExtra("query");
            this.mQueryString = query;
            intent = new Intent(this, TasksListViewActivity.class);
            intent.putExtra(OptionsMenuHelper.VIEW_TYPE, 20);
            intent.putExtra("query", query);
            startActivity(intent);
        }
        SearchProvider.mTasks = null;
        ArchitectureContext.setObjectExtra(null);
    }

    private void postSearchInitialization(Long taskIdSearched, Integer levelToDisplay) {
        this.mLevelState.setLevel(levelToDisplay);
        initTasks();
        for (int i = 0; i < this.mTasks.size(); i++) {
            if (((Task) this.mTasks.get(i)).getId().equals(taskIdSearched)) {
                adjustFirstVisibleTask(i);
                return;
            }
        }
    }

    public boolean onSearchRequested() {
        return super.onSearchRequested();
    }

    private void initTasks() {
        if (AppUtil.isZoomSet(this.mLevelState)) {
            this.mLevelState.setZoomTask(getTaskFromTasks(new Long(this.mLevelState.getZoomTaskIdStr())));
            int level = this.mLevelState.getZoomTask().getLevel().intValue();
            if (level >= this.mLevelState.getLevel().intValue()) {
                this.mLevelState.setLevel(Integer.valueOf(level + 1));
            }
            this.mExpandedTasks.remove(AppUtil.getExpandedTaskId(this.mLevelState.getZoomTask()));
            this.mTasks = this.taskBp.getTaskChildrenAndGrandChildren(this.mLevelState.getZoomTask(), this.taskBp.getTasksByLevel(this.mExpandedTasks, this.mLevelState.getLevel().intValue(), false, this.mTasksList, true));
            return;
        }
        this.mTasks = this.taskBp.getTasksByLevel(this.mExpandedTasks, this.mLevelState.getLevel().intValue(), false, this.mTasksList, true);
    }

    private Task getTaskFromTasks(Long taskId) {
        for (Task task : this.mTasksAtMaxLevel) {
            if (task.getId().equals(taskId)) {
                return task;
            }
        }
        return null;
    }

    private void setWindowTitle() {
        if (StringUtil.isNullOrEmpty(AppUtil.getCurrentlySyncedTasksList())) {
            this.mTitle.setText(AppUtil.getMaxWindowTitle(this.mTasksList.getName() + getWindowTitleTaskName()));
        } else {
            this.mTitle.setText(AppUtil.getCurrentlySyncedTasksList());
        }
        this.mTitle.setBackgroundColor(Color.parseColor(Constants.COLOR_BLUISH_DAY_HEADER));
        if (AppUtil.isZoomSet(this.mLevelState)) {
            this.mZoomTitleTextView.setText(" " + this.mLevelState.getZoomTask().getName());
        }
    }

    private String getWindowTitleTaskName() {
        if (AppUtil.isZoomSet(this.mLevelState)) {
            return " - (L" + (this.mLevelState.getZoomTask().getLevel().intValue() + 1) + ") " + this.mLevelState.getZoomTask().getName();
        }
        return getTask() != null ? " - " + getTask().getName() : "";
    }

    private void setLevelInfo() {
        this.mLevelInfoTextView = (TextView) findViewById(R.id.level_info);
        this.mLevelInfoTextView.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                HelpUtility.showHelp(TasksListActivity.this, HelpDialog.TASKS_TREE);
            }
        });
    }

    private void displayLevelInfo() {
        if (this.mLevelState.getLevel().intValue() == 0 || this.mTasksCurrentMaxLevel == 0) {
            this.mLevelState.setLevel(Integer.valueOf(1));
            this.mTasksCurrentMaxLevel = 1;
            this.taskStateBp.saveOrUpdateLevelState(this.mLevelState);
        }
        if (this.mLevelState.getLevel().intValue() > this.mTasksCurrentMaxLevel) {
            this.mLevelState.setLevel(Integer.valueOf(this.mTasksCurrentMaxLevel));
            this.taskStateBp.saveOrUpdateLevelState(this.mLevelState);
        }
        this.mLevelInfoTextView.setText("L" + this.mLevelState.getLevel() + Globals.FORWARDSLASH + this.mTasksCurrentMaxLevel);
    }

    void setTasksCurrentMaxLevel() {
        this.mTasksCurrentMaxLevel = 0;
        List tasksByLevel = this.taskBp.getTasksByLevel(null, 20, false, this.mTasksList, true);
        this.mTasks = tasksByLevel;
        this.mTasksAtMaxLevel = tasksByLevel;
        for (Task task : this.mTasksAtMaxLevel) {
            if (this.mTasksCurrentMaxLevel < task.getLevel().intValue()) {
                this.mTasksCurrentMaxLevel = task.getLevel().intValue();
            }
        }
    }

    protected void onStop() {
        super.onStop();
        this.taskStateBp.saveTaskStates(this.mTasksList.getId());
        this.mLevelState.setPosition(Integer.valueOf(this.mFirstVisibleTask));
        this.taskStateBp.saveOrUpdateLevelState(this.mLevelState);
    }

    protected void onResume() {
        super.onResume();
        this.config.setCurrentContext(this);
        ErrorReporter.CheckErrorAndSendMailCall(this);
        TrialSpecificUtility.checkTrialExpiration(this);
        if (this.config.isPreferenceAccessed()) {
            if (!this.mTasksList.getId().equals(DatabaseHelper.ALL_TASKS_LIST_ID)) {
                this.mTasksList = this.tasksListBp.get(this.mTasksList.getId());
            }
            setTasksCurrentMaxLevel();
            initTasks();
            this.config.setPreferenceAccessed(false);
        }
        initAccounts();
        displayLevelInfo();
        setSelectedTaskFromSearch();
        setListAdapter(new TasksListAdapter(this, this.mTasks, this.mExpandedTasks, this.mTaskDecorator, this.mLevelState));
        getListView().setSelection(this.mFirstVisibleTask);
        setPlusImageViewVisibilityBasedOnTasksCount();
        setTaskPosition(getDecoratedEventPosition());
        setWindowTitle();
        setZoomVisibility();
        setNotificationActions();
        AppUtil.setMenuImage(this);
    }

    private void setNotificationActions() {
        if (!this.mIsNotifcationConsumed && isIntentFromNotification()) {
            this.mSelectedTaskPosition = getTaskPositionFromTask(getTaskFromTasks(Long.valueOf(getIntent().getLongExtra("taskId", 0))));
            adjustFirstVisibleTask(this.mSelectedTaskPosition);
            performTaskFocusedAction(30);
            this.mIsNotifcationConsumed = true;
        }
    }

    private void adjustFirstVisibleTask(int actualPosition) {
        this.mFirstVisibleTask = actualPosition - 3;
        if (this.mFirstVisibleTask < 0) {
            this.mFirstVisibleTask = 0;
        }
    }

    private void setPlusImageViewVisibilityBasedOnTasksCount() {
        if (!DatabaseHelper.ALL_TASKS_LIST_ID.equals(this.config.getLastSelectedTasksList()) || this.mTasks.size() >= 1) {
            this.mPlusImageView.setVisibility(View.VISIBLE);
        } else {
            this.mPlusImageView.setVisibility(View.GONE);
        }
    }

    public void updateListItem(int position, boolean isRefreshDisplay) {
        Task task = (Task) this.mTasks.get(position);
        if (isRefreshDisplay || (task.isRecurringTask() && !task.isCompleted().booleanValue())) {
            refreshDisplay();
            return;
        }
        View view = getListView().getChildAt(position - this.mFirstVisibleTask);
        SpannableStringBuilder taskName = new SpannableStringBuilder("");
        taskName.append(task.getName());
        AppUtil.formatTaskNameForDone(taskName, task);
        setTaskNameInListItem(view, taskName);
    }

    public void markAsSelected() {
        if (!this.editManager.isTaskMarkedForEdit(getTask())) {
            View view = getViewForListItemProcessing();
            if (view != null) {
                getListItemLayout(view).setBackgroundColor(Constants.COLOR_SELECTED_TASK_YELLOW);
            } else {
                return;
            }
        }
        this.mListItemSelected = true;
    }

    private void markAsNOTSelected() {
        if (this.mListItemSelected && !this.editManager.isTaskMarkedForEdit((Task) this.mTasks.get(getDecoratedEventPosition()))) {
            performUnmarkingFromSelect();
            this.mListItemSelected = false;
        }
    }

    private void performUnmarkingFromSelect() {
        View view = getViewForListItemUnSelecting();
        if (view != null) {
            setListItemWithNormalBackground(view);
        }
    }

    private void setListItemWithNormalBackground(View view) {
        getListItemLayout(view).setBackgroundDrawable(this.config.getContext().getResources().getDrawable(R.drawable.list_selector_background_normal));
    }

    public void markAsNotForEdit() {
        View view = getViewForListItemEditUnSelecting();
        if (view != null) {
            setListItemWithNormalBackground(view);
        }
    }

    private View getViewForListItemEditUnSelecting() {
        Task taskToClear = this.editManager.getDestinationTask();
        if (taskToClear != null) {
            return getListView().getChildAt(getTaskPositionFromTask(taskToClear) - this.mFirstVisibleTask);
        }
        return null;
    }

    public void markForEdit() {
        View view = getViewForListItemProcessing();
        if (view != null) {
            LinearLayout listItem = getListItemLayout(view);
            if (this.editManager.isTaskNeedColor(getTask())) {
                listItem.setBackgroundColor(this.editManager.getTaskColor(getTask()));
            } else {
                listItem.setBackgroundDrawable(this.config.getContext().getResources().getDrawable(R.drawable.list_selector_background_normal));
            }
        }
    }

    private LinearLayout getListItemLayout(View view) {
        return (LinearLayout) view.findViewById(R.id.list_item);
    }

    private void setTaskNameInListItem(View view, SpannableStringBuilder taskName) {
        ((TextView) view.findViewById(R.id.task_name)).setText(taskName);
    }

    private View getViewForListItemProcessing() {
        return getListView().getChildAt(this.mSelectedTaskPosition - this.mFirstVisibleTask);
    }

    private View getViewForListItemUnSelecting() {
        return getListView().getChildAt(getDecoratedEventPosition() - this.mFirstVisibleTask);
    }

    private void setScreenSpecificGestureParameters() {
        if (this.config.getScreenHeight() < ScreenSizes.NEXUS_ONE_WVGA_HEIGHT) {
            TasksListGestureHelper.CONSIDERED_AS_HORIZONTAL_SWIPE_DISTANCE = 115;
            TasksListGestureHelper.CONSIDERED_AS_VERITCAL_SWIPE_DISTANCE = 115;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (-1 == resultCode) {
            Long newTaskId = null;
            switch (requestCode) {
                case 1:
                    if (data != null) {
                        newTaskId = (Long) data.getExtras().get(Constants.NEW_TASK_ID);
                        break;
                    }
                    break;
                case 2:
                    break;
                default:
                    return;
            }
            refreshDisplay(newTaskId);
        }
    }

    private boolean isNewTaskIdVisible(Long newTaskId) {
        for (Task task : this.mTasks) {
            if (newTaskId.equals(task.getId())) {
                return true;
            }
        }
        return false;
    }

    public boolean onContextItemSelected(MenuItem item) {
        final ConfigManager config = ConfigManager.getInstance();
        setTaskPosition(((AdapterContextMenuInfo) item.getMenuInfo()).position);
        switch (item.getItemId()) {
            case R.id.context_menu_zoom:
                if (config.getLeftLongPressAction().intValue() == 70) {
                    performTaskFocusedAction(45);
                    return true;
                }
                performTaskFocusedAction(70);
                return true;
            case R.id.context_menu_view:
                performTaskFocusedAction(65);
                return true;
            case R.id.context_menu_edit:
                performTaskFocusedAction(30);
                return true;
            case R.id.context_menu_delete:
                new Builder(this).setTitle(R.string.delete_title).setMessage("Delete task [" + getTask().getName() + "] ?").setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int button) {
                        if (TasksListActivity.this.getTask().isReminderEnabled().booleanValue()) {
                            AlarmSetterHelper.scheduleAlarm(config.getContext(), TasksListActivity.this.getTask(), false);
                        }
                        TasksListActivity.this.taskBp.delete(TasksListActivity.this.getTask());
                        TasksListActivity.this.refreshDisplay(null);
                    }
                }).setNegativeButton(R.string.alert_dialog_cancel, null).show();
                return true;
            case R.id.context_menu_add:
                turnONTaskSelector(null, false);
                ArchitectureContext.setObject(getTask());
                Intent intent = new Intent(this, EditTaskActivity.class);
                intent.putExtra(Constants.IS_NEW_TASK, Constants.TRUE);
                if (getTask().isTasksListAsTask() || isTaskExpanded(null)) {
                    intent.putExtra(Constants.IS_CHILD_TASK, Constants.TRUE);
                }
                startActivityForResult(intent, 1);
                return true;
            case R.id.context_menu_cut:
                performMarkAsCut();
                return true;
            case R.id.context_menu_paste:
                performPaste();
                return true;
            case R.id.context_menu_copy:
                performMarkAsCopy();
                return true;
            case R.id.context_menu_delete_branch:
                new Builder(this).setTitle(R.string.delete_title).setMessage(getDeleteBranchMessage()).setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int button) {
                        TasksListActivity.this.taskBp.deleteRecursively(TasksListActivity.this.getTask(), TasksListActivity.this.getTasksAtMaxLevelWithCompetedTasks());
                        TasksListActivity.this.refreshDisplay(null);
                    }
                }).setNegativeButton(R.string.alert_dialog_cancel, null).show();
                return true;
            case R.id.context_menu_copy_branch_to_list:
                initTasksLists(true);
                showCopyBranchToList();
                return true;
            case R.id.context_menu_show_completed:
                if (config.isHideCompletedTasks()) {
                    config.setHideCompletedTasks(false);
                } else {
                    config.setHideCompletedTasks(true);
                }
                refreshDisplay();
                return true;
            case R.id.context_menu_show_checkboxes:
                performTaskFocusedAction(50);
                return true;
            case R.id.context_menu_show_numbers:
                performTaskFocusedAction(60);
                return true;
            case R.id.context_menu_drag_and_drop:
                performTaskFocusedAction(20);
                return true;
            case R.id.context_menu_multi_edit:
                if (config.getPanelToShow().equals(Integer.valueOf(1))) {
                    config.setPanelToShow(Integer.valueOf(0));
                } else {
                    config.setPanelToShow(Integer.valueOf(1));
                }
                setPlusImage();
                return true;
            case R.id.context_menu_send_task:
                String[] recipients = null;
                if (!StringUtil.isNullOrEmpty(config.getDefaultEmailRecipient())) {
                    recipients = new String[]{config.getDefaultEmailRecipient()};
                }
                EmailUtility.sendEmail(recipients, getTask().getName(), getTask().getNote(), this);
                return true;
            default:
                return ContextMenuHelper.onContextItemSelected(item, this, getTask().getName(), getTask().getTasksListId());
        }
    }

    private void showCopyBranchToList() {
        this.mWhichTasksList = 0;
        new Builder(this).setTitle("Copy branch to").setSingleChoiceItems(this.mTasksListNames, 0, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichTasksList) {
                TasksListActivity.this.mWhichTasksList = whichTasksList;
            }
        }).setPositiveButton("Copy", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                TasksListActivity.this.taskBp.copyBranchToList(TasksListActivity.this.getTask(), (TasksList) TasksListActivity.this.mTasksLists.get(TasksListActivity.this.mWhichTasksList));
                dialog.dismiss();
                Toast.makeText(TasksListActivity.this, "Copied to [" + ((TasksList) TasksListActivity.this.mTasksLists.get(TasksListActivity.this.mWhichTasksList)).getName() + "] list", Toast.LENGTH_LONG).show();
            }
        }).setNegativeButton(R.string.alert_dialog_cancel, null).show();
    }

    private void initTasksLists(boolean isIncludeNewList) {
        this.mTasksLists = new ArrayList();
        if (isIncludeNewList) {
            TasksList newTasksList = new TasksList();
            newTasksList.setAccountId(this.mTasksList.getAccountId());
            newTasksList.setName("NEW " + getTask().getName());
            this.mTasksLists.add(newTasksList);
        }
        for (TasksList tasksList : this.tasksListBp.getAllTasksListBySortOrder()) {
            if (this.config.isShowInvisibleAccounts().booleanValue() || tasksList.isVisible().booleanValue()) {
                this.mTasksLists.add(tasksList);
            }
        }
        this.mTasksListNames = new String[this.mTasksLists.size()];
        for (int i = 0; i < this.mTasksLists.size(); i++) {
            this.mTasksListNames[i] = ((TasksList) this.mTasksLists.get(i)).getName();
        }
    }

    private String getDeleteBranchMessage() {
        if (getTask().isParent()) {
            return "Delete task [" + getTask().getName() + "] and its children?";
        }
        return "Delete task [" + getTask().getName() + "]?";
    }

    private void initAccounts() {
        this.mAccounts = this.accountBp.getAllVisibleAccounts();
        this.mAccountEmails = new SpannableStringBuilder[this.mAccounts.size()];
        for (int i = 0; i < this.mAccounts.size(); i++) {
            this.mAccountEmails[i] = new SpannableStringBuilder(((Account) this.mAccounts.get(i)).getEmail());
        }
    }

    public void showCopyList() {
        ContextMenuHelper.showCopyList(getTask().getTasksListId(), this.mAccounts, this.mAccountEmails, this);
    }

    public void showCopyListAsBranch() {
        initTasksLists(false);
        ContextMenuHelper.showCopyListAsBranch(this, this.mTasksListNames, getTask().getTasksListId(), this.mTasksLists);
    }

    private void setTaskPositionByTask(Task task) {
        for (int i = 0; i < this.mTasks.size(); i++) {
            if (((Task) this.mTasks.get(i)).getId().equals(task.getId())) {
                this.mSelectedTaskPosition = i;
            }
        }
    }

    private int getDecoratedEventPosition() {
        return getTaskPositionFromTask(this.mTaskDecorator);
    }

    private int getTaskPositionFromTask(Task task) {
        if (task != null) {
            int i;
            if (task.isTasksListAsTask()) {
                for (i = 0; i < this.mTasks.size(); i++) {
                    if (((Task) this.mTasks.get(i)).getTasksListId().equals(task.getTasksListId())) {
                        return i;
                    }
                }
            } else {
                for (i = 0; i < this.mTasks.size(); i++) {
                    if (((Task) this.mTasks.get(i)).getId().equals(task.getId())) {
                        return i;
                    }
                }
            }
        }
        return 0;
    }

    private boolean isTaskExpanded(Task task) {
        Task taskToTest = task;
        if (taskToTest == null) {
            taskToTest = getTask();
        }
        if (!taskToTest.isParent()) {
            return false;
        }
        boolean isTaskExpanded;
        if (this.mExpandedTasks.containsKey(AppUtil.getExpandedTaskId(taskToTest))) {
            isTaskExpanded = ((Long) this.mExpandedTasks.get(AppUtil.getExpandedTaskId(taskToTest))).equals(Constants.TASK_EXPAND_INDICATOR);
        } else {
            isTaskExpanded = taskToTest.getLevel().intValue() < this.mLevelState.getLevel().intValue();
        }
        return isTaskExpanded;
    }

    private Task getTask() {
        if (this.mSelectedTaskPosition >= this.mTasks.size()) {
            return this.mPrevSelectedTask;
        }
        Task task = (Task) this.mTasks.get(this.mSelectedTaskPosition);
        this.mPrevSelectedTask = task;
        return task;
    }

    private void resetTapTime() {
        this.mTapTime = 0;
    }

    private boolean isNextActionAllowed() {
        return AppUtil.getTodayDateTimeMilisecond() - this.mTapTime > TasksListGestureHelper.TAP_INTERVAL_ALLOWED;
    }

    private void setListListeners() {
        getListView().setOnItemClickListener(this.mOnItemClickListener);
        getListView().setOnScrollListener(new OnScrollListener() {
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                TasksListActivity.this.mFirstVisibleTask = firstVisibleItem;
            }
        });
        registerForContextMenu(getListView());
    }

    private void turnOffTaskSelector() {
        this.mTaskDecorator.setSelected(false);
    }

    private void setListOnTouchListener() {
        getListView().setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        TasksListActivity.this.mVerticalViewSection = AppUtil.getVerticalScreenSection(event.getY(), v);
                        int x = (int) event.getX();
                        int y = (int) event.getY();
                        TasksListActivity.this.mIsDecoratorSelected = TasksListActivity.this.mSelectedTaskPosition == TasksListActivity.this.getListView().pointToPosition(x, y);
                        break;
                }
                if (TasksListActivity.this.gestureDetector.onTouchEvent(event)) {
                    return true;
                }
                switch (event.getAction()) {
                    case 0:
                        TasksListActivity.this.mIsRightTap = AppUtil.isRightTap(event.getX(), v);
                        break;
                    case 1:
                        if (TasksListActivity.this.mIsLongPress) {
                            TasksListActivity.this.setTapTime();
                            TasksListActivity.this.mIsLongPress = false;
                            break;
                        }
                        break;
                }
                return false;
            }
        });
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        markAsNOTSelected();
        setTaskPosition(((AdapterContextMenuInfo) menuInfo).position);
        markAsSelected();
        turnONTaskSelector(null, false);
        Integer localAction = this.config.getLeftLongPressAction();
        if (this.mIsRightTap) {
            localAction = this.config.getRightLongPressAction();
        }
        processLongPress(menu, v, menuInfo, localAction);
    }

    void processLongPress(ContextMenu menu, View v, ContextMenuInfo menuInfo, Integer localAction) {
        if (isNextActionAllowedOnDragAndDropMode()) {
            setTapTime();
            if (10 == localAction.intValue()) {
                MenuInflater inflater = getMenuInflater();
                if (getTask().isTasksListAsTask()) {
                    inflater.inflate(R.menu.list_context_menu, menu);
                    return;
                }
                inflater.inflate(R.menu.context_menu, menu);
                MenuItem dragNdropMenuIem = AppUtil.getMenuItem(menu, R.id.context_menu_drag_and_drop);
                if (this.config.isDragAndDropMode()) {
                    dragNdropMenuIem.setTitle("Drag and drop OFF");
                } else {
                    dragNdropMenuIem.setTitle("Drag and drop ON");
                }
                MenuItem multiEditMenuIem = AppUtil.getMenuItem(menu, R.id.context_menu_multi_edit);
                if (this.config.getPanelToShow().equals(Integer.valueOf(1))) {
                    multiEditMenuIem.setTitle("Multi-edit panel HIDE");
                } else {
                    multiEditMenuIem.setTitle("Multi-edit panel SHOW");
                }
                MenuItem copyMenuIem = AppUtil.getMenuItem(menu, R.id.context_menu_copy);
                if (isTasksMarkedForEditAction(EditActions.COPY)) {
                    copyMenuIem.setTitle("Copy branch CLEAR");
                } else {
                    copyMenuIem.setTitle("Copy branch");
                }
                MenuItem cutMenuIem = AppUtil.getMenuItem(menu, R.id.context_menu_cut);
                if (isTasksMarkedForEditAction(EditActions.CUT)) {
                    cutMenuIem.setTitle("Cut branch CLEAR");
                } else {
                    cutMenuIem.setTitle("Cut branch");
                }
                MenuItem zoomMenuIem = AppUtil.getMenuItem(menu, R.id.context_menu_zoom);
                if (this.config.getLeftLongPressAction().intValue() != 70) {
                    zoomMenuIem.setTitle("Zoom on branch");
                } else if (isTaskExpanded(getTask())) {
                    zoomMenuIem.setTitle("Collapse branch ALL Children");
                } else {
                    zoomMenuIem.setTitle("Expand branch ALL Children");
                }
                MenuItem completedMenuIem = AppUtil.getMenuItem(menu, R.id.context_menu_show_completed);
                if (this.config.isHideCompletedTasks()) {
                    completedMenuIem.setTitle("Show completed tasks");
                    return;
                } else {
                    completedMenuIem.setTitle("Hide completed tasks");
                    return;
                }
            }
            vibrate();
            performTaskFocusedAction(this.config.getLeftLongPressAction().intValue());
            return;
        }
        resetTapTime();
    }

    boolean isNextActionAllowedOnDragAndDropMode() {
        if (isPrioritizeDragAndDropMode()) {
            return false;
        }
        return isNextActionAllowed();
    }

    private void vibrate() {
        ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).vibrate(40);
    }

    private void setTapTime() {
        this.mTapTime = AppUtil.getTodayDateTimeMilisecond();
    }

    boolean isPrioritizeDragAndDropMode() {
        return this.config.isDragAndDropMode() && this.config.isSwipeMoveLeftRightOnDragAndDropMode() && this.mIsDecoratorSelected;
    }

    private void performSwipeAction(int action, HORIZONTAL_DIRECTION horizontalDirection) {
        if (!isPrioritizeDragAndDropMode()) {
            setTapTime();
            switch (action) {
                case 1:
                    int newFontSize;
                    if (horizontalDirection != HORIZONTAL_DIRECTION.RIGHT) {
                        if (28 > this.config.getTaskNameFontSize().intValue()) {
                            newFontSize = this.config.getTaskNameFontSize().intValue() + 3;
                            ConfigManager configManager = this.config;
                            if (28 < newFontSize) {
                                newFontSize = 28;
                            }
                            configManager.setTaskNameFontSize(Integer.valueOf(newFontSize));
                            break;
                        }
                    } else if (10 < this.config.getTaskNameFontSize().intValue()) {
                        newFontSize = this.config.getTaskNameFontSize().intValue() - 3;
                        ConfigManager configManager2 = this.config;
                        if (10 > newFontSize) {
                            newFontSize = 10;
                        }
                        configManager2.setTaskNameFontSize(Integer.valueOf(newFontSize));
                        break;
                    }
                    break;
                case 2:
                    int level;
                    if (horizontalDirection != HORIZONTAL_DIRECTION.RIGHT) {
                        level = this.mLevelState.getLevel().intValue() + 1;
                        if (level > this.mTasksCurrentMaxLevel) {
                            level = this.mTasksCurrentMaxLevel;
                        }
                        setTasksLevelToDisplay(level);
                        break;
                    }
                    level = this.mLevelState.getLevel().intValue() - 1;
                    if (level < 1) {
                        level = 1;
                    }
                    setTasksLevelToDisplay(level);
                    break;
                case 3:
                    if (horizontalDirection != HORIZONTAL_DIRECTION.RIGHT) {
                        setTasksLevelToDisplay(this.mTasksCurrentMaxLevel);
                        break;
                    } else {
                        setTasksLevelToDisplay(1);
                        break;
                    }
            }
            onResume();
        } else if (horizontalDirection == HORIZONTAL_DIRECTION.RIGHT) {
            performMoveLeft();
        } else {
            performMoveRight();
        }
    }

    private void setTasksLevelToDisplay(int level) {
        this.mExpandedTasks.clear();
        this.mLevelState.setLevel(Integer.valueOf(level));
        initTasks();
    }

    private void turnONTaskSelector(Long taskId, boolean isTasksListAsTask) {
        if (isTasksListAsTask || (taskId == null && getTask().isTasksListAsTask())) {
            if (taskId == null) {
                this.mTaskDecorator.setTasksListId(getTask().getTasksListId());
            } else {
                this.mTaskDecorator.setTasksListId(taskId);
            }
            this.mTaskDecorator.setTasksListAsTask(true);
            this.mTaskDecorator.setId(Long.valueOf(-1));
        } else {
            this.mTaskDecorator.setTasksListId(Long.valueOf(-1));
            if (taskId == null) {
                this.mTaskDecorator.setId(getTask().getId());
            } else {
                this.mTaskDecorator.setId(taskId);
            }
            this.mTaskDecorator.setTasksListAsTask(false);
        }
        this.mTaskDecorator.setSelected(true);
        setWindowTitle();
    }

    private void performTaskFocusedAction(int action) {
        TasksList tasksList;
        switch (action) {
            case 20:
                if (this.config.isDragAndDropMode()) {
                    ((DragNDropListView) getListView()).enableDragMode(false);
                    this.config.setDragAndDropMode(Boolean.valueOf(false));
                    if (this.config.isPaneledDragAndDrop() && this.config.getPanelToShow().equals(Integer.valueOf(2))) {
                        this.config.setPanelToShow(Integer.valueOf(0));
                    }
                } else {
                    ((DragNDropListView) getListView()).enableDragMode(true);
                    this.config.setDragAndDropMode(Boolean.valueOf(true));
                    if (this.config.isPaneledDragAndDrop() && this.config.getPanelToShow().equals(Integer.valueOf(0))) {
                        this.config.setPanelToShow(Integer.valueOf(2));
                    }
                }
                setPlusImage();
                onResume();
                return;
            case 30:
                turnONTaskSelector(null, false);
                if (getTask().isTasksListAsTask()) {
                    tasksList = new TasksListBpImpl().get(getTask().getTasksListId());
                    tasksList.setAccount(new AccountBpImpl().get(tasksList.getAccountId()));
                    ArchitectureContext.setObject(tasksList);
                    startActivityForResult(new Intent(this, TasksListEditActivity.class), 2);
                    return;
                }
                turnONTaskSelector(null, false);
                ArchitectureContext.setObject(getTask());
                startActivityForResult(new Intent(this, EditTaskActivity.class), 2);
                return;
            case 40:
                if (this.mExpandedTasks.containsKey(AppUtil.getExpandedTaskId((Task) this.mTasks.get(this.mSelectedTaskPosition)))) {
                    this.mExpandedTasks.remove(AppUtil.getExpandedTaskId((Task) this.mTasks.get(this.mSelectedTaskPosition)));
                } else {
                    this.mExpandedTasks.put(AppUtil.getExpandedTaskId((Task) this.mTasks.get(this.mSelectedTaskPosition)), AppUtil.getExpandOrCollapseIndicator(((Task) this.mTasks.get(this.mSelectedTaskPosition)).getLevel().intValue(), this.mLevelState.getLevel().intValue()));
                }
                initTasks();
                onResume();
                return;
            case ArraysConstants.EXPAND_COLLAPSE_BRANCH_ALL_CHILDREN /*45*/:
                this.taskBp.expandCollapseBranchAllChildren(this.mExpandedTasks, getTask(), this.mTasksAtMaxLevel, this.mLevelState.getLevel(), isTaskExpanded(getTask()));
                initTasks();
                onResume();
                return;
            case 50:
                if (this.config.isShowDoneCheckbox()) {
                    this.config.setShowDoneCheckbox(false);
                } else {
                    this.config.setShowDoneCheckbox(true);
                }
                onResume();
                return;
            case 60:
                performShowHideNumbers();
                return;
            case ArraysConstants.VIEW_TASK_DETAILS /*65*/:
                turnONTaskSelector(null, false);
                if (getTask().isTasksListAsTask()) {
                    tasksList = new TasksListBpImpl().get(getTask().getTasksListId());
                    tasksList.setAccount(new AccountBpImpl().get(tasksList.getAccountId()));
                    ArchitectureContext.setObject(tasksList);
                    startActivityForResult(new Intent(this, TasksListEditActivity.class), 2);
                    return;
                }
                turnONTaskSelector(null, false);
                ArchitectureContext.setObject(getTask());
                startActivityForResult(new Intent(this, TaskViewActivity.class), 2);
                return;
            case ArraysConstants.ZOOM_ON_BRANCH /*70*/:
                if (getTask().isTasksListAsTask() || (DatabaseHelper.ALL_TASKS_LIST_ID.equals(this.mTasksList.getId()) && !getTask().isParent() && getTask().getParentTaskId().equals(Long.valueOf(0)))) {
                    this.mTasksList = this.tasksListBp.get(getTask().getTasksListId());
                    this.config.setLastSelectedTasksList(this.mTasksList.getId());
                    setTasksCurrentMaxLevel();
                    initTasks();
                    onResume();
                    return;
                }
                if (DatabaseHelper.ALL_TASKS_LIST_ID.equals(this.mTasksList.getId())) {
                    this.mLevelState = this.taskStateBp.getLevelSate(getTask().getTasksListId(), null);
                }
                if (getTask().isParent()) {
                    this.mLevelState.setZoomTaskIdStr(AppUtil.getExpandedTaskId(getTask()));
                    zoomSetSpecificTasksList();
                    return;
                } else if (!getTask().getParentTaskId().equals(Long.valueOf(0))) {
                    this.mLevelState.setZoomTaskIdStr(getTask().getParentTaskId() + "");
                    zoomSetSpecificTasksList();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    private void zoomSetSpecificTasksList() {
        if (DatabaseHelper.ALL_TASKS_LIST_ID.equals(this.mTasksList.getId())) {
            this.mTasksList = this.tasksListBp.get(getTask().getTasksListId());
            this.config.setLastSelectedTasksList(this.mTasksList.getId());
            setTasksCurrentMaxLevel();
        }
        initTasks();
        onResume();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_search:
                onSearchRequested();
                return true;
            default:
                return OptionsMenuHelper.onOptionsItemSelected(this, item.getItemId(), item);
        }
    }

    public void refreshDisplay(Long newTaskId) {
        setTasksCurrentMaxLevel();
        initTasks();
        if (newTaskId != null && isNewTaskIdVisible(newTaskId)) {
            turnONTaskSelector(newTaskId, false);
        }
        onResume();
    }

    private void showActionType(String actionMessage) {
        new Builder(this).setTitle(actionMessage).setMessage(actionMessage).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu_tasks, menu);
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        try {
            goBackToHomeView();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        return true;
    }

    private void goBackToHomeView() {
        finish();
        startActivity(new Intent(this, TasksListHomeActivity.class));
        if (this.mPlusImageView != null) {
            this.mPlusImageView.setVisibility(View.GONE);
        }
    }

    public String getEmailAllTasksBody() {
        List<Task> allTasks = this.taskBp.getAllTasksWithCompletedAndSupportForAllTasks(this.mTasksList, true);
        if (AppUtil.isZoomSet(this.mLevelState)) {
            allTasks = this.taskBp.getTaskChildrenAndGrandChildren(this.mLevelState.getZoomTask(), allTasks);
        }
        StringBuffer emailBody = new StringBuffer();
        setEmailZoomHeader(emailBody);
        for (Task task : allTasks) {
            emailBody.append(getEmailIndention(task) + task.getTaskNumber() + ". " + EmailUtility.getCheckTask(task) + task.getName() + CSVWriter.DEFAULT_LINE_END);
            setEmailNote(emailBody, task);
        }
        return emailBody.toString();
    }

    private void setEmailNote(StringBuffer emailBody, Task task) {
        if (!StringUtil.isNullOrEmpty(task.getNote())) {
            emailBody.append(getEmailIndention(task) + EmailUtility.getTaskNumberSpacer(task) + "   " + "Note: " + task.getNote() + CSVWriter.DEFAULT_LINE_END);
        }
    }

    public String getEmailVisibleTasksBody() {
        StringBuffer emailBody = new StringBuffer();
        setEmailZoomHeader(emailBody);
        for (Task task : this.mTasks) {
            emailBody.append(getEmailIndention(task) + task.getTaskNumber() + ". " + EmailUtility.getExpandedTask(task, isTaskExpanded(task)) + EmailUtility.getCheckTask(task) + task.getName() + CSVWriter.DEFAULT_LINE_END);
            setEmailNote(emailBody, task);
        }
        return emailBody.toString();
    }

    private void setEmailZoomHeader(StringBuffer emailBody) {
        if (AppUtil.isZoomSet(this.mLevelState)) {
            emailBody.append(CSVWriter.DEFAULT_LINE_END);
            emailBody.append("ZOOM on " + this.mLevelState.getZoomTask().getName() + ":\n");
            emailBody.append(CSVWriter.DEFAULT_LINE_END);
        }
    }

    private String getEmailIndention(Task task) {
        int zoomLevel = 0;
        if (AppUtil.isZoomSet(this.mLevelState)) {
            zoomLevel = this.mLevelState.getZoomTask().getLevel().intValue();
        }
        return EmailUtility.INDENTS[(task.getLevel().intValue() - 1) - zoomLevel];
    }

    public void refreshDisplay() {
        refreshDisplay(null);
    }
}
