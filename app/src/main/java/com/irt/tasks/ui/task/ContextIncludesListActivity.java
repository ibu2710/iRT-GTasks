package com.irt.tasks.ui.task;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import com.irt.tasks.app.businessobjects.ContextBase;
import com.irt.tasks.app.businessobjects.ContextIncludes;
import com.irt.tasks.app.businessprocess.ContextBp;
import com.irt.tasks.app.businessprocess.ContextBpImpl;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.ui.task.ContextBaseInteractiveArrayAdapter.ViewHolder;
import java.util.ArrayList;
import java.util.List;

public class ContextIncludesListActivity extends AbstractEditListActivity {
    ContextBp contextBp = new ContextBpImpl();
    ContextBase mContextBase;
    List<ContextBase> mContexts;

    protected void performOnCreate(Bundle savedInstanceState) {
        this.mContextBase = (ContextBase) ArchitectureContext.getObject();
        refereshList();
        this.mNewButton.setVisibility(View.GONE);
    }

    private void refereshList() {
        setListAdapter(new ContextBaseInteractiveArrayAdapter(this, getContexts()));
    }

    protected List<ContextBase> getContexts() {
        this.mContexts = this.contextBp.getAllContextsExcludingSpecificContext(this.mContextBase.getId());
        if (this.mContexts.size() > 0) {
            List<ContextIncludes> contextIncludess = this.mContextBase.getContextIncludess();
            if (contextIncludess != null) {
                for (ContextIncludes contextIncludes : contextIncludess) {
                    setSelectedContext(contextIncludes.getIncludedContextId());
                }
            }
        }
        return this.mContexts;
    }

    private void setSelectedContext(Long includedContextId) {
        for (ContextBase contextBase : this.mContexts) {
            if (contextBase.getId().longValue() == includedContextId.longValue()) {
                contextBase.setSelected(true);
            }
        }
    }

    protected void performNew() {
    }

    protected void performSave() {
        List<ContextIncludes> contextIncludess = new ArrayList();
        for (ContextBase contextBase : this.mContexts) {
            if (contextBase.isSelected()) {
                contextIncludess.add(new ContextIncludes(this.mContextBase.getId(), contextBase.getId()));
            }
        }
        this.mContextBase.setContextIncludess(contextIncludess);
    }

    protected void performListOnItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        CheckBox checkbox = ((ViewHolder) view.getTag()).checkbox;
        if (checkbox.isChecked()) {
            checkbox.setChecked(false);
        } else {
            checkbox.setChecked(true);
        }
    }

    protected void performDeleteSelectedListItem(int position) {
    }

    protected void performEditSelectedListItem(int position) {
    }

    protected boolean isShowContextMenu(int position) {
        return false;
    }
}
