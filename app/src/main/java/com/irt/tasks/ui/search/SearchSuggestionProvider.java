package com.irt.tasks.ui.search;

import android.content.SearchRecentSuggestionsProvider;

public class SearchSuggestionProvider extends SearchRecentSuggestionsProvider {
    static final String AUTHORITY = "com.irt.calendar.app.ui.search.SearchSuggestionProvider";
    static final int MODE = 1;

    public SearchSuggestionProvider() {
        setupSuggestions(AUTHORITY, 1);
    }
}
