package com.irt.tasks.ui.customrepeat;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.CSVWriter;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.datetimepicker.DatePickerActivity;
import java.util.Calendar;
import java.util.Date;

public class CustomRepeatActivity extends AbstractActivity {
    private static final int DAYS_IN_WEEK = 7;
    private static final int MAX_DAY_OF_MONTH_BEFORE_CONSIDERED_AS_LAST_DAYS = 4;
    private ConfigManager config;
    private Button mCancelButton;
    private StringBuffer mCustomRepeatParameters = null;
    private TextView mCustomRepeatTypeLabel;
    private CheckBox[] mDayCheckBoxes = new CheckBox[7];
    private RadioGroup mDayDateRadioGroup;
    private String[] mDayLabel = new String[]{"Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "", "", "", "", "", "", ""};
    private CheckBox mEndOnCheckBox;
    private Date mEndOnDate;
    private TextView mEndOnDateTextView;
    private LinearLayout mEndOnLinearLayout;
    boolean mInitialIsReminderRepeatFromCompletionDate;
    String mInitialRepeatRule;
    private boolean mIsReadyForCheckBoxListener = false;
    private RadioButton mMonthDateRadioButton;
    private RadioButton mMonthDayRadioButton;
    private TextView mRepeatDescription;
    private TableLayout mRepeatDescriptionTableLayout;
    CheckBox mRepeatFromCompletionDateCheckbox;
    private LinearLayout mRepeatIntervalLinearLayout;
    private Spinner mRepeatIntervalSpinner;
    private TextView mRepeatIntervalUnitTextView;
    private Spinner mRepeatTypeSpinner;
    private Button mSaveButton;
    private int mSelectedInterval;
    private int mSelectedType;
    private Task mTask;
    private GridView mWeekGridView;
    private TextView mYearTip;

    public class WeekAdapter extends BaseAdapter {
        public View getView(int position, View convertView, ViewGroup parent) {
            if (position < 7) {
                TextView labelTextView;
                if (convertView == null) {
                    labelTextView = new TextView(CustomRepeatActivity.this);
                    labelTextView.setTypeface(Typeface.DEFAULT_BOLD);
                    labelTextView.setTextSize(19.0f);
                    labelTextView.setTextColor(getResources().getColor(android.R.color.black));
                    labelTextView.setGravity(17);
                } else {
                    labelTextView = (TextView) convertView;
                }
                labelTextView.setText(CustomRepeatActivity.this.mDayLabel[position]);
                return labelTextView;
            }
            return CustomRepeatActivity.this.mDayCheckBoxes[position - 7];
        }

        public final int getCount() {
            return CustomRepeatActivity.this.mDayLabel.length;
        }

        public final Object getItem(int position) {
            if (position < 7) {
                return CustomRepeatActivity.this.mDayLabel[position];
            }
            return CustomRepeatActivity.this.mDayCheckBoxes[position - 7];
        }

        public final long getItemId(int position) {
            return (long) position;
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_repeat);
        this.config = ConfigManager.getInstance();
        this.mTask = (Task) ArchitectureContext.getObject();
        this.mEndOnDate = new Date(this.mTask.getDueDate().longValue());
        initLayouts();
        initRepeatTypeControls();
        initRepeatInterval();
        initEndOnControls();
        initWeekGridView();
        initMonthRadioButtons();
        initRepeatFromCompletionDate();
        initRepeatDescription();
        initSaveAndCancelButtons();
        setVisibility();
        initValues();
        this.mInitialRepeatRule = this.mTask.getReminderRepeatRule();
        this.mInitialIsReminderRepeatFromCompletionDate = this.mTask.isReminderRepeatFromCompletionDate().booleanValue();
    }

    private void initRepeatFromCompletionDate() {
        this.mRepeatFromCompletionDateCheckbox = (CheckBox) findViewById(R.id.repeat_from_completion_date_checkbox);
        this.mRepeatFromCompletionDateCheckbox.setChecked(this.mTask.isReminderRepeatFromCompletionDate().booleanValue());
        this.mRepeatFromCompletionDateCheckbox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomRepeatActivity.this.mTask.setReminderRepeatFromCompletionDate(Boolean.valueOf(CustomRepeatActivity.this.mRepeatFromCompletionDateCheckbox.isChecked()));
                CustomRepeatActivity.this.setVisibility();
            }
        });
    }

    private void initValues() {
        if (StringUtil.isNullOrEmpty(this.mTask.getReminderRepeatRule())) {
            this.mSelectedType = 0;
            return;
        }
        CustomRepeatHelper.getVerboseDescription(this.mTask.getReminderRepeatRule(), new Date(this.mTask.getDueDate().longValue()), this.mTask.isReminderRepeatFromCompletionDate().booleanValue());
        String frequency = CustomRepeatHelper.getFrequency();
        if (CustomRepeatHelper.DAILY.equals(frequency)) {
            this.mSelectedType = 1;
        } else if (CustomRepeatHelper.WEEKLY.equals(frequency)) {
            this.mSelectedType = 6;
            setWeekDays();
        } else if (CustomRepeatHelper.MONTHLY.equals(frequency)) {
            this.mSelectedType = 7;
            if (CustomRepeatHelper.isMonthlyByDay()) {
                this.mMonthDayRadioButton.setChecked(true);
            } else {
                this.mMonthDateRadioButton.setChecked(true);
            }
        } else if (CustomRepeatHelper.YEARLY.equals(frequency)) {
            this.mSelectedType = 8;
        }
        this.mRepeatTypeSpinner.setSelection(this.mSelectedType);
        this.mRepeatIntervalSpinner.setSelection(CustomRepeatHelper.getInterval() - 1);
        long until = CustomRepeatHelper.getUntil();
        if (until == 0) {
            this.mEndOnCheckBox.setChecked(false);
            return;
        }
        this.mEndOnCheckBox.setChecked(true);
        this.mEndOnDate = new Date(until);
        displayEndOnDate();
    }

    private void setWeekDays() {
        String daysOfWeekStr = CustomRepeatHelper.getWeeklyByDayDays();
        if (daysOfWeekStr.contains(CustomRepeatHelper.SU)) {
            this.mDayCheckBoxes[0].setChecked(true);
        }
        if (daysOfWeekStr.contains(CustomRepeatHelper.MO)) {
            this.mDayCheckBoxes[1].setChecked(true);
        }
        if (daysOfWeekStr.contains(CustomRepeatHelper.TU)) {
            this.mDayCheckBoxes[2].setChecked(true);
        }
        if (daysOfWeekStr.contains(CustomRepeatHelper.WE)) {
            this.mDayCheckBoxes[3].setChecked(true);
        }
        if (daysOfWeekStr.contains(CustomRepeatHelper.TH)) {
            this.mDayCheckBoxes[4].setChecked(true);
        }
        if (daysOfWeekStr.contains(CustomRepeatHelper.FR)) {
            this.mDayCheckBoxes[5].setChecked(true);
        }
        if (daysOfWeekStr.contains(CustomRepeatHelper.SA)) {
            this.mDayCheckBoxes[6].setChecked(true);
        }
    }

    private void initMonthRadioButtons() {
        this.mMonthDayRadioButton = (RadioButton) findViewById(R.id.month_day_radio_button);
        this.mMonthDayRadioButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CustomRepeatActivity.this.setDescription();
            }
        });
        this.mMonthDateRadioButton = (RadioButton) findViewById(R.id.month_date_radio_button);
        this.mMonthDateRadioButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CustomRepeatActivity.this.setDescription();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (-1 == resultCode) {
            switch (requestCode) {
                case 3:
                    Long selectedDateLong = (Long) data.getExtras().get(Constants.SELECTED_DATE_LONG_KEY);
                    if (selectedDateLong != null) {
                        this.mEndOnDate = new Date(selectedDateLong.longValue());
                        displayEndOnDate();
                    }
                    setDescription();
                    return;
                default:
                    return;
            }
        }
    }

    private void initEndOnControls() {
        this.mEndOnDateTextView = (TextView) findViewById(R.id.end_on_date);
        this.mEndOnDateTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CustomRepeatActivity.this, DatePickerActivity.class);
                intent.putExtra(Constants.SELECTED_DATE, DateFormat.format(Constants.DATE_FORMAT_MMDDYYYY, CustomRepeatActivity.this.mEndOnDate));
                CustomRepeatActivity.this.startActivityForResult(intent, 3);
            }
        });
        this.mEndOnCheckBox = (CheckBox) findViewById(R.id.end_on_checkbox);
        this.mEndOnCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    CustomRepeatActivity.this.mEndOnDate = new Date(CustomRepeatActivity.this.mTask.getDueDate().longValue());
                    CustomRepeatActivity.this.displayEndOnDate();
                } else {
                    CustomRepeatActivity.this.mEndOnDateTextView.setText("never");
                }
                CustomRepeatActivity.this.setDescription();
            }
        });
    }

    private void displayEndOnDate() {
        SpannableStringBuilder endOnDate = new SpannableStringBuilder();
        endOnDate.append(DateFormat.format("MMM dd, yyyy", this.mEndOnDate));
        endOnDate.setSpan(new UnderlineSpan(), 0, endOnDate.length(), 0);
        this.mEndOnDateTextView.setText(endOnDate);
    }

    private void initRepeatDescription() {
        this.mYearTip = (TextView) findViewById(R.id.year_tip_text_view);
        this.mRepeatDescription = (TextView) findViewById(R.id.custom_repeat_description);
    }

    private void initSaveAndCancelButtons() {
        this.mSaveButton = (Button) findViewById(R.id.saveButton);
        this.mSaveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomRepeatActivity.this.mTask.setReminderRepeatFromCompletionDate(Boolean.valueOf(CustomRepeatActivity.this.mRepeatFromCompletionDateCheckbox.isChecked()));
                if (CustomRepeatActivity.this.mRepeatTypeSpinner.getSelectedItemPosition() == 0) {
                    CustomRepeatActivity.this.mTask.setReminderRepeatRule(null);
                } else {
                    CustomRepeatActivity.this.mTask.setReminderRepeatRule(CustomRepeatActivity.this.mCustomRepeatParameters.toString());
                }
                CustomRepeatActivity.this.finish();
            }
        });
        this.mCancelButton = (Button) findViewById(R.id.cancelButton);
        this.mCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomRepeatActivity.this.mTask.setReminderRepeatRule(CustomRepeatActivity.this.mInitialRepeatRule);
                CustomRepeatActivity.this.mTask.setReminderRepeatFromCompletionDate(Boolean.valueOf(CustomRepeatActivity.this.mInitialIsReminderRepeatFromCompletionDate));
                CustomRepeatActivity.this.finish();
            }
        });
    }

    private void initWeekGridView() {
        this.mWeekGridView = (GridView) findViewById(R.id.week_grid_view);
        for (int i = 0; i < 7; i++) {
            this.mDayCheckBoxes[i] = new CheckBox(this);
            this.mDayCheckBoxes[i].setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (CustomRepeatActivity.this.mIsReadyForCheckBoxListener) {
                        CustomRepeatActivity.this.mRepeatTypeSpinner.setSelection(6);
                        CustomRepeatActivity.this.defaultDayOfWeekIfNoneIsSelected();
                        CustomRepeatActivity.this.setDescription();
                    }
                }
            });
        }
        this.mWeekGridView.setAdapter(new WeekAdapter());
    }

    private void initRepeatInterval() {
        this.mRepeatIntervalSpinner = (Spinner) findViewById(R.id.repeat_interval_spinner);
        this.mSelectedInterval = 0;
        ArrayAdapter<String> repeatIntervalAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.repeat_interval));
        repeatIntervalAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.mRepeatIntervalSpinner.setAdapter(repeatIntervalAdapter);
        this.mRepeatIntervalSpinner.setSelection(this.mSelectedInterval);
        this.mRepeatIntervalSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CustomRepeatActivity.this.setDescription();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        this.mRepeatIntervalUnitTextView = (TextView) findViewById(R.id.repeat_interval_unit);
    }

    private void setDescription() {
        this.mCustomRepeatParameters = new StringBuffer();
        switch (this.mRepeatTypeSpinner.getSelectedItemPosition()) {
            case 0:
                return;
            case 1:
                CustomRepeatHelper.appendFrequency(CustomRepeatHelper.DAILY, this.mCustomRepeatParameters);
                break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                CustomRepeatHelper.appendFrequency(CustomRepeatHelper.WEEKLY, this.mCustomRepeatParameters);
                CustomRepeatHelper.appendWeeklyByDay(translateWeekDays(), this.mCustomRepeatParameters);
                break;
            case 7:
                CustomRepeatHelper.appendFrequency(CustomRepeatHelper.MONTHLY, this.mCustomRepeatParameters);
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(this.mTask.getDueDate().longValue());
                if (!this.mMonthDayRadioButton.isChecked()) {
                    CustomRepeatHelper.appendByMonthDay(cal.get(Calendar.DAY_OF_MONTH), this.mCustomRepeatParameters);
                    break;
                }
                CustomRepeatHelper.appendMonthlyByDay(getDayOfWeekInMonth(cal) + formatMonthlyByDay(cal.get(Calendar.DAY_OF_WEEK)), this.mCustomRepeatParameters);
                break;
            case 8:
                CustomRepeatHelper.appendFrequency(CustomRepeatHelper.YEARLY, this.mCustomRepeatParameters);
                break;
        }
        CustomRepeatHelper.appendInterval(this.mRepeatIntervalSpinner.getSelectedItemPosition() + 1, this.mCustomRepeatParameters);
        if (this.mEndOnCheckBox.isChecked()) {
            CustomRepeatHelper.appendUntil(this.mEndOnDate, this.mCustomRepeatParameters, null);
        }
        CustomRepeatHelper.appendWeekStart(this.mCustomRepeatParameters);
        this.mTask.setReminderRepeatRule(this.mCustomRepeatParameters.toString());
        this.mRepeatDescription.setText(CustomRepeatHelper.getVerboseDescription(this.mCustomRepeatParameters.toString(), new Date(this.mTask.getDueDate().longValue()), this.mRepeatFromCompletionDateCheckbox.isChecked()) + AppUtil.getNextDateIsOnWording(this.mTask));
    }

    private int getDayOfWeekInMonth(Calendar cal) {
        int dayOfWeekInMonth = cal.get(Calendar.DAY_OF_WEEK_IN_MONTH);
        if (dayOfWeekInMonth > 4) {
            return -1;
        }
        return dayOfWeekInMonth;
    }

    private String formatMonthlyByDay(int day) {
        switch (day) {
            case 1:
                return CustomRepeatHelper.SU;
            case 2:
                return CustomRepeatHelper.MO;
            case 3:
                return CustomRepeatHelper.TU;
            case 4:
                return CustomRepeatHelper.WE;
            case 5:
                return CustomRepeatHelper.TH;
            case 6:
                return CustomRepeatHelper.FR;
            case 7:
                return CustomRepeatHelper.SA;
            default:
                return "ERROR";
        }
    }

    private String translateWeekDays() {
        defaultDayOfWeekIfNoneIsSelected();
        StringBuffer weekDays = new StringBuffer();
        int checkCounter = 0;
        if (this.mDayCheckBoxes[0].isChecked()) {
            checkCounter = 0 + 1;
            weekDays.append(CustomRepeatHelper.SU);
        }
        if (this.mDayCheckBoxes[1].isChecked()) {
            checkCounter++;
            weekDays.append(formatDay(CustomRepeatHelper.MO, checkCounter));
        }
        if (this.mDayCheckBoxes[2].isChecked()) {
            checkCounter++;
            weekDays.append(formatDay(CustomRepeatHelper.TU, checkCounter));
        }
        if (this.mDayCheckBoxes[3].isChecked()) {
            checkCounter++;
            weekDays.append(formatDay(CustomRepeatHelper.WE, checkCounter));
        }
        if (this.mDayCheckBoxes[4].isChecked()) {
            checkCounter++;
            weekDays.append(formatDay(CustomRepeatHelper.TH, checkCounter));
        }
        if (this.mDayCheckBoxes[5].isChecked()) {
            checkCounter++;
            weekDays.append(formatDay(CustomRepeatHelper.FR, checkCounter));
        }
        if (this.mDayCheckBoxes[6].isChecked()) {
            weekDays.append(formatDay(CustomRepeatHelper.SA, checkCounter + 1));
        }
        return weekDays.toString();
    }

    private Object formatDay(String day, int checkCounter) {
        if (checkCounter > 1) {
            return Globals.COMMA + day;
        }
        return day;
    }

    private void defaultDayOfWeekIfNoneIsSelected() {
        if (numberOfCheckedWeekDays() == 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(this.mTask.getDueDate().longValue());
            this.mDayCheckBoxes[cal.get(Calendar.DAY_OF_WEEK) - 1].setChecked(true);
        }
    }

    private int numberOfCheckedWeekDays() {
        int numberOfCheckedWeekDays = 0;
        for (int i = 0; i < 7; i++) {
            if (this.mDayCheckBoxes[i].isChecked()) {
                numberOfCheckedWeekDays++;
            }
        }
        return numberOfCheckedWeekDays;
    }

    private void initRepeatTypeControls() {
        this.mCustomRepeatTypeLabel = (TextView) findViewById(R.id.custom_repeat_type_label);
        this.mCustomRepeatTypeLabel.setText(this.mTask.getName() + CSVWriter.DEFAULT_LINE_END + DateFormat.format("MMM dd, yyyy", new Date(this.mTask.getDueDate().longValue())));
        this.mRepeatTypeSpinner = (Spinner) findViewById(R.id.custom_repeat_type_spinner);
        this.mSelectedType = 0;
        setRepeatTypeSpinner();
        this.mRepeatTypeSpinner.setSelection(this.mSelectedType);
        this.mRepeatTypeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                CustomRepeatActivity.this.mIsReadyForCheckBoxListener = false;
                CustomRepeatActivity.this.setVisibility();
                CustomRepeatActivity.this.mIsReadyForCheckBoxListener = true;
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setRepeatTypeSpinner() {
        ArrayAdapter<String> repeatTypeAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.custom_repeat_type));
        repeatTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.mRepeatTypeSpinner.setAdapter(repeatTypeAdapter);
    }

    private void initLayouts() {
        this.mRepeatIntervalLinearLayout = (LinearLayout) findViewById(R.id.repeat_interval_linear_layout);
        this.mEndOnLinearLayout = (LinearLayout) findViewById(R.id.end_on_linear_layout);
        this.mDayDateRadioGroup = (RadioGroup) findViewById(R.id.day_date_radio_group);
        this.mWeekGridView = (GridView) findViewById(R.id.week_grid_view);
        this.mRepeatDescriptionTableLayout = (TableLayout) findViewById(R.id.repeat_description_table_layout);
    }

    private void setVisibility() {
        switch (this.mRepeatTypeSpinner.getSelectedItemPosition()) {
            case 0:
                this.mRepeatIntervalLinearLayout.setVisibility(View.GONE);
                this.mEndOnLinearLayout.setVisibility(View.GONE);
                this.mWeekGridView.setVisibility(View.GONE);
                this.mDayDateRadioGroup.setVisibility(View.GONE);
                this.mYearTip.setVisibility(View.GONE);
                this.mRepeatDescriptionTableLayout.setVisibility(View.GONE);
                this.mRepeatFromCompletionDateCheckbox.setVisibility(View.GONE);
                break;
            case 1:
                this.mRepeatIntervalLinearLayout.setVisibility(View.VISIBLE);
                this.mEndOnLinearLayout.setVisibility(View.VISIBLE);
                this.mWeekGridView.setVisibility(View.GONE);
                this.mDayDateRadioGroup.setVisibility(View.GONE);
                this.mYearTip.setVisibility(View.GONE);
                this.mRepeatDescriptionTableLayout.setVisibility(View.VISIBLE);
                this.mRepeatIntervalUnitTextView.setText(" Day(s)");
                this.mRepeatFromCompletionDateCheckbox.setVisibility(View.VISIBLE);
                break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                setWeekCheckboxes();
                this.mRepeatIntervalLinearLayout.setVisibility(View.VISIBLE);
                this.mEndOnLinearLayout.setVisibility(View.VISIBLE);
                this.mWeekGridView.setVisibility(View.VISIBLE);
                this.mDayDateRadioGroup.setVisibility(View.GONE);
                this.mRepeatDescriptionTableLayout.setVisibility(View.GONE);
                this.mWeekGridView.setAdapter(new WeekAdapter());
                this.mYearTip.setVisibility(View.GONE);
                this.mRepeatFromCompletionDateCheckbox.setVisibility(View.GONE);
                this.mRepeatIntervalUnitTextView.setText(" Week(s)");
                if (this.mRepeatFromCompletionDateCheckbox.isChecked()) {
                    this.mRepeatTypeSpinner.setSelection(6);
                    break;
                }
                break;
            case 7:
                this.mRepeatIntervalLinearLayout.setVisibility(View.VISIBLE);
                this.mEndOnLinearLayout.setVisibility(View.VISIBLE);
                this.mWeekGridView.setVisibility(View.GONE);
                this.mDayDateRadioGroup.setVisibility(View.VISIBLE);
                this.mYearTip.setVisibility(View.GONE);
                this.mRepeatDescriptionTableLayout.setVisibility(View.VISIBLE);
                this.mRepeatIntervalUnitTextView.setText(" Month(s)");
                this.mRepeatFromCompletionDateCheckbox.setVisibility(View.VISIBLE);
                break;
            case 8:
                this.mRepeatIntervalLinearLayout.setVisibility(View.VISIBLE);
                this.mEndOnLinearLayout.setVisibility(View.VISIBLE);
                this.mWeekGridView.setVisibility(View.GONE);
                this.mDayDateRadioGroup.setVisibility(View.GONE);
                this.mYearTip.setVisibility(View.VISIBLE);
                this.mRepeatDescriptionTableLayout.setVisibility(View.VISIBLE);
                this.mRepeatIntervalUnitTextView.setText(" Year(s)");
                this.mRepeatFromCompletionDateCheckbox.setVisibility(View.VISIBLE);
                break;
        }
        setVisibilityBasedOnRepeatType();
        setDescription();
    }

    private void setVisibilityBasedOnRepeatType() {
        if (this.mRepeatFromCompletionDateCheckbox.isChecked()) {
            this.mWeekGridView.setVisibility(View.GONE);
            this.mYearTip.setVisibility(View.GONE);
            this.mDayDateRadioGroup.setVisibility(View.GONE);
        }
    }

    private void setWeekCheckboxes() {
        int i;
        switch (this.mRepeatTypeSpinner.getSelectedItemPosition()) {
            case 2:
                i = 0;
                while (i < 7) {
                    if (i <= adjustWeekDayToZero(1) || i >= adjustWeekDayToZero(7)) {
                        this.mDayCheckBoxes[i].setChecked(false);
                    } else {
                        this.mDayCheckBoxes[i].setChecked(true);
                    }
                    i++;
                }
                return;
            case 3:
                i = 0;
                while (i < 7) {
                    if (i == adjustWeekDayToZero(1) || i == adjustWeekDayToZero(7)) {
                        this.mDayCheckBoxes[i].setChecked(true);
                    } else {
                        this.mDayCheckBoxes[i].setChecked(false);
                    }
                    i++;
                }
                return;
            case 4:
                i = 0;
                while (i < 7) {
                    if (i == adjustWeekDayToZero(2) || i == adjustWeekDayToZero(4) || i == adjustWeekDayToZero(6)) {
                        this.mDayCheckBoxes[i].setChecked(true);
                    } else {
                        this.mDayCheckBoxes[i].setChecked(false);
                    }
                    i++;
                }
                return;
            case 5:
                i = 0;
                while (i < 7) {
                    if (i == adjustWeekDayToZero(3) || i == adjustWeekDayToZero(5)) {
                        this.mDayCheckBoxes[i].setChecked(true);
                    } else {
                        this.mDayCheckBoxes[i].setChecked(false);
                    }
                    i++;
                }
                return;
            default:
                return;
        }
    }

    private int adjustWeekDayToZero(int weekDay) {
        return weekDay - 1;
    }
}
