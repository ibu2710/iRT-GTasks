package com.irt.tasks.ui.task;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.text.util.Linkify;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.Constants.HORIZONTAL_DIRECTION;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.app.framework.config.ScreenSizes;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.receiver.AlarmSetterHelper;
import com.irt.tasks.ui.taskslist.TasksListGestureHelper;
import com.irt.tasks.ui.taskslist.TasksListHomeActivity;

public class TaskViewActivity extends AbstractActivity {
    ConfigManager config;
    private GestureDetector gestureDetector;
    Button mCancelButton;
    Button mDeleteButton;
    Button mEditButton;
    Button mNoteButton;
    ScrollView mScrollView;
    Task mTaskFromCallingActivity = null;
    TextView mTaskNameAndNoteTextView;
    TaskBp taskBp = new TaskBpImpl();

    class MyGestureDetector extends SimpleOnGestureListener {
        MyGestureDetector() {
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) > 250.0f || Math.abs(e1.getX() - e2.getX()) < ((float) TasksListGestureHelper.CONSIDERED_AS_HORIZONTAL_SWIPE_DISTANCE) || Math.abs(e1.getY() - e2.getY()) > ((float) TasksListGestureHelper.CONSIDERED_AS_VERITCAL_SWIPE_DISTANCE)) {
                    return false;
                }
                if (e1.getX() - e2.getX() > BitmapDescriptorFactory.HUE_GREEN && Math.abs(velocityX) > 200.0f) {
                    TaskViewActivity.this.performSwipeAction(HORIZONTAL_DIRECTION.RIGHT);
                    return true;
                } else if (e2.getX() - e1.getX() <= BitmapDescriptorFactory.HUE_GREEN || Math.abs(velocityX) <= 200.0f) {
                    return false;
                } else {
                    TaskViewActivity.this.performSwipeAction(HORIZONTAL_DIRECTION.LEFT);
                    return true;
                }
            } catch (Exception e) {
                return false;
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.config = ConfigManager.getInstance();
        setContentView(R.layout.task_view);
        Object obj = ArchitectureContext.getObject();
        if (obj != null && (obj instanceof Task)) {
            this.mTaskFromCallingActivity = (Task) ArchitectureContext.getObject();
        }
        if (this.mTaskFromCallingActivity == null) {
            finish();
            startActivity(new Intent(this, TasksListHomeActivity.class));
            return;
        }
        initViews();
        setTaskNameAndNote();
        initEditButton();
        initNoteButton();
        initDeleteButton();
        initCancelButton();
        setScreenSpecificGestureParameters();
        setScrollViewOnTouchListener();
        this.gestureDetector = new GestureDetector(new MyGestureDetector());
    }

    private void setScreenSpecificGestureParameters() {
        if (this.config.getScreenHeight() < ScreenSizes.NEXUS_ONE_WVGA_HEIGHT) {
            TasksListGestureHelper.CONSIDERED_AS_HORIZONTAL_SWIPE_DISTANCE = 115;
            TasksListGestureHelper.CONSIDERED_AS_VERITCAL_SWIPE_DISTANCE = 115;
        }
    }

    protected void onResume() {
        super.onResume();
        setTaskNameAndNote();
    }

    private void setTaskNameAndNote() {
        SpannableString spannableTextNote = SpannableString.valueOf(this.mTaskFromCallingActivity.getName() + "\n\n" + this.mTaskFromCallingActivity.getNote());
        if (this.config.isLinkifyNotes()) {
            Linkify.addLinks(spannableTextNote, Linkify.ALL);
        }
        this.mTaskNameAndNoteTextView.setText(spannableTextNote);
        this.mTaskNameAndNoteTextView.setTextSize((float) this.config.getTaskNoteFontSize().intValue());
    }

    private void initViews() {
        this.mTaskNameAndNoteTextView = (TextView) findViewById(R.id.task_name_note);
        this.mTaskNameAndNoteTextView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v instanceof TextView) {
                    TextView widget = (TextView) v;
                    CharSequence text = widget.getText();
                    if (text instanceof Spanned) {
                        Spanned buffer = (Spanned) text;
                        int action = event.getAction();
                        if (action == 1 || action == 0) {
                            int x = (((int) event.getX()) - widget.getTotalPaddingLeft()) + widget.getScrollX();
                            int y = (((int) event.getY()) - widget.getTotalPaddingTop()) + widget.getScrollY();
                            Layout layout = widget.getLayout();
                            int off = layout.getOffsetForHorizontal(layout.getLineForVertical(y), (float) x);
                            ClickableSpan[] link = (ClickableSpan[]) buffer.getSpans(off, off, ClickableSpan.class);
                            if (link.length != 0) {
                                if (action == 1) {
                                    link[0].onClick(widget);
                                    return true;
                                } else if (action == 0) {
                                    return true;
                                }
                            }
                        }
                    }
                }
                return false;
            }
        });
        this.mDeleteButton = (Button) findViewById(R.id.delete_button);
        this.mEditButton = (Button) findViewById(R.id.edit_button);
        this.mNoteButton = (Button) findViewById(R.id.note_button);
        this.mCancelButton = (Button) findViewById(R.id.cancel_button);
        this.mScrollView = (ScrollView) findViewById(R.id.scroll_view);
    }

    private void setScrollViewOnTouchListener() {
        this.mScrollView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (TaskViewActivity.this.gestureDetector.onTouchEvent(event)) {
                    return true;
                }
                return false;
            }
        });
    }

    private void performSwipeAction(HORIZONTAL_DIRECTION horizontalDirection) {
        int newFontSize;
        if (horizontalDirection == HORIZONTAL_DIRECTION.RIGHT) {
            if (10 < this.config.getTaskNoteFontSize().intValue()) {
                newFontSize = this.config.getTaskNoteFontSize().intValue() - 3;
                ConfigManager configManager = this.config;
                if (10 > newFontSize) {
                    newFontSize = 10;
                }
                configManager.setTaskNoteFontSize(Integer.valueOf(newFontSize));
                setTaskNameAndNote();
            }
        } else if (28 > this.config.getTaskNoteFontSize().intValue()) {
            newFontSize = this.config.getTaskNoteFontSize().intValue() + 3;
            ConfigManager configManager2 = this.config;
            if (28 < newFontSize) {
                newFontSize = 28;
            }
            configManager2.setTaskNoteFontSize(Integer.valueOf(newFontSize));
            setTaskNameAndNote();
        }
    }

    private void initNoteButton() {
        this.mNoteButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ArchitectureContext.setObject(TaskViewActivity.this.mTaskFromCallingActivity);
                TaskViewActivity.this.startActivity(new Intent(TaskViewActivity.this, TaskNoteEditActivity.class));
            }
        });
    }

    private void initEditButton() {
        this.mEditButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ArchitectureContext.setObject(TaskViewActivity.this.mTaskFromCallingActivity);
                Intent intent = new Intent(TaskViewActivity.this, EditTaskActivity.class);
                intent.putExtra(Constants.INTENT_FROM_VIEW_TASK, Constants.INTENT_FROM_VIEW_TASK);
                TaskViewActivity.this.startActivity(intent);
                TaskViewActivity.this.finish();
            }
        });
    }

    private void initDeleteButton() {
        this.mDeleteButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new Builder(TaskViewActivity.this).setTitle(R.string.delete_title).setMessage(R.string.delete_this_task).setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (TaskViewActivity.this.mTaskFromCallingActivity.isReminderEnabled().booleanValue()) {
                            AlarmSetterHelper.scheduleAlarm(TaskViewActivity.this.config.getContext(), TaskViewActivity.this.mTaskFromCallingActivity, false);
                        }
                        TaskViewActivity.this.taskBp.delete(TaskViewActivity.this.mTaskFromCallingActivity);
                        TaskViewActivity.this.setResult(-1);
                        TaskViewActivity.this.finish();
                    }
                }).setNegativeButton(R.string.alert_dialog_cancel, null).show();
            }
        });
    }

    private void initCancelButton() {
        this.mCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                TaskViewActivity.this.setResult(-1);
                TaskViewActivity.this.finish();
            }
        });
    }
}
