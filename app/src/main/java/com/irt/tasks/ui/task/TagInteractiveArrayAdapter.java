package com.irt.tasks.ui.task;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Tag;

import java.util.List;

public class TagInteractiveArrayAdapter extends ArrayAdapter<Tag> {
    private final Activity context;
    private final List<Tag> list;

    static class ViewHolder {
        protected CheckBox checkbox;
        protected TextView text;

        ViewHolder() {
        }
    }

    public TagInteractiveArrayAdapter(Activity context, List<Tag> list) {
        super(context, R.layout.list_black_text_checkbox, list);
        this.context = context;
        this.list = list;
    }

    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = this.context.getLayoutInflater().inflate(R.layout.list_black_text_checkbox, null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) view.findViewById(R.id.label);
            viewHolder.checkbox = (CheckBox) view.findViewById(R.id.check);
            viewHolder.checkbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ((Tag) viewHolder.checkbox.getTag()).setSelected(buttonView.isChecked());
                    ((AbstractEditListActivity) TagInteractiveArrayAdapter.this.context).setRadioButtons(isChecked);
                }
            });
            view.setTag(viewHolder);
            viewHolder.checkbox.setTag(this.list.get(position));
        } else {
            view = convertView;
            ((ViewHolder) view.getTag()).checkbox.setTag(this.list.get(position));
        }
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.text.setText(((Tag) this.list.get(position)).getName());
        holder.checkbox.setChecked(((Tag) this.list.get(position)).isSelected());
        return view;
    }
}
