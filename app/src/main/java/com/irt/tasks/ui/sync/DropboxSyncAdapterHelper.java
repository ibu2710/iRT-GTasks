package com.irt.tasks.ui.sync;

import com.irt.tasks.app.businessobjects.DatabaseMetadata;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.dataaccess.FileDao;

public class DropboxSyncAdapterHelper {
    public static boolean isLocalDatabaseCurrent(FileDao fileDao, DatabaseMetadata dropboxDatabaseMetadata, DatabaseMetadata localDatabaseMetadata, Long metadataModifiedDateMillis, Long syncFileModifiedDateMillis) {
        if (metadataModifiedDateMillis.longValue() <= 0 || syncFileModifiedDateMillis.longValue() <= 0) {
            return true;
        }
        AppUtil.setDropboxMetadata(fileDao, dropboxDatabaseMetadata, Constants.DROPBOX_DATABASE_SYNC_META_DATA_RELATIVE_FILE_PATH);
        long localProbablyCurrent = 0;
        if (!localDatabaseMetadata.getDatatabaseName().equals(dropboxDatabaseMetadata.getDatatabaseName())) {
            return true;
        }
        long diff = localDatabaseMetadata.getLastModifiedDateMillis().longValue() - dropboxDatabaseMetadata.getLastModifiedDateMillis().longValue();
        if (diff > 100) {
            localProbablyCurrent = 0 + diff;
        }
        if (localProbablyCurrent >= 500) {
            AppUtil.logDebug("localProbablyCurrent points [" + localProbablyCurrent + "] >= [" + 500 + "] so localDatabase is current");
            return true;
        }
        AppUtil.logDebug("localProbablyCurrent points [" + localProbablyCurrent + "] < [" + 500 + "] so localDatabase is NOT current");
        return false;
    }
}
