package com.irt.tasks.ui.account;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.tasks.Tasks;
import com.google.api.services.tasks.TasksScopes;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessprocess.AccountBp;
import com.irt.tasks.app.businessprocess.AccountBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.sync.DropboxSyncHelper;
import java.util.Collections;

public class AccountEditActivity extends AbstractActivity {
    private static final String PREF_ACCOUNT_NAME = "accountName";
    static final int REQUEST_ACCOUNT_PICKER = 2;
    static final int REQUEST_AUTHORIZATION = 1;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 0;
    AccountBp accountBp = new AccountBpImpl();
    ConfigManager config;
    final HttpTransport httpTransport = AndroidHttp.newCompatibleTransport();
    final JsonFactory jsonFactory = GsonFactory.getDefaultInstance();
    Account mAccount;
    CheckBox mAutoSyncCheckBox;
    Button mCancelButton;
    GoogleAccountCredential mCredential;
    Button mDeleteButton;
    TextView mEmailText;
    CheckBox mEnableCheckBox;
    TextView mFontColorSample;
    boolean mIsNew = false;
    Button mLoginButton;
    TextView mMessageText;
    EditText mNoteEditText;
    Button mSaveButton;
    Tasks mService;
    int numAsyncTasks;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_edit);
        turnOffAutoKeyboardPopUp();
        this.config = ConfigManager.getInstance();
        this.mAccount = (Account) ArchitectureContext.getObject();
        if (this.mAccount.getId() == null) {
            this.mIsNew = true;
        }
        initControl();
        fillfields();
        initLoginButton();
        initSaveButton();
        initCancelButton();
        initDeleteButton();
        setVisibility();
        setGoogleAccountPicker();
    }

    private void setVisibility() {
        if (DatabaseHelper.OFFLINE_ACCOUNT_ID.equals(this.mAccount.getId())) {
            this.mAutoSyncCheckBox.setVisibility(View.GONE);
            this.mDeleteButton.setVisibility(View.GONE);
            this.mLoginButton.setVisibility(View.GONE);
            this.mSaveButton.setVisibility(View.VISIBLE);
        } else if (this.mIsNew) {
            this.mDeleteButton.setVisibility(View.GONE);
        }
        if (this.mAccount.getId() != null && !this.mAccount.isNewGoogleId().booleanValue()) {
            this.mLoginButton.setVisibility(View.GONE);
        }
    }

    private void fillfields() {
        this.mEmailText.setText(this.mAccount.getEmail());
        this.mAutoSyncCheckBox.setChecked(this.mAccount.isAutoSync().booleanValue());
        this.mEnableCheckBox.setChecked(this.mAccount.isEnable().booleanValue());
        this.mNoteEditText.setText(this.mAccount.getNote());
        if (this.mAccount.getId() == DatabaseHelper.OFFLINE_ACCOUNT_ID) {
            this.mMessageText.setText("");
        } else if (this.mAccount.getId() != null && !this.mAccount.isNewGoogleId().booleanValue()) {
            SpannableStringBuilder autoSyncLabel = new SpannableStringBuilder(this.config.getContext().getString(R.string.delete_this_account_due_to_old_sync_id) + " Note that you can copy task lists to another account in Tasks List Home View.\n\nClick the Delete button below to delete.");
            autoSyncLabel.setSpan(new ForegroundColorSpan(AppUtil.getRedColorStatus()), 0, autoSyncLabel.length(), 0);
            this.mMessageText.setText(autoSyncLabel);
        }
    }

    private void initControl() {
        this.mEmailText = (TextView) findViewById(R.id.email_address);
        this.mAutoSyncCheckBox = (CheckBox) findViewById(R.id.auto_sync_checkbox);
        this.mEnableCheckBox = (CheckBox) findViewById(R.id.enable_checkbox);
        this.mFontColorSample = (TextView) findViewById(R.id.font_color_sample);
        this.mNoteEditText = (EditText) findViewById(R.id.note);
        this.mMessageText = (TextView) findViewById(R.id.message);
        DropboxSyncHelper.addDelayedIdleToEditText(this.mNoteEditText);
        this.mDeleteButton = (Button) findViewById(R.id.delete_button);
        this.mSaveButton = (Button) findViewById(R.id.save_button);
        this.mLoginButton = (Button) findViewById(R.id.login_button);
        this.mCancelButton = (Button) findViewById(R.id.cancel_button);
    }

    private void initLoginButton() {
        this.mLoginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountEditActivity.this.setAccountFromFields();
                AccountEditActivity.this.verifyAccount(AccountEditActivity.this.mAccount.getEmail());
            }
        });
    }

    private void initDeleteButton() {
        this.mDeleteButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new Builder(AccountEditActivity.this).setTitle(R.string.delete_title).setMessage(R.string.delete_this_account).setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AccountEditActivity.this.accountBp.deleteAccount(AccountEditActivity.this.mAccount);
                        AccountEditActivity.this.finish();
                    }
                }).setNegativeButton(R.string.alert_dialog_cancel, null).show();
            }
        });
    }

    private void initCancelButton() {
        this.mCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountEditActivity.this.finish();
            }
        });
    }

    private void initSaveButton() {
        this.mSaveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountEditActivity.this.setAccountFromFields();
                AccountEditActivity.this.accountBp.saveOrUpdate(AccountEditActivity.this.mAccount);
                AccountEditActivity.this.finish();
            }
        });
    }

    private void setAccountFromFields() {
        this.mAccount.setAutoSync(Boolean.valueOf(this.mAutoSyncCheckBox.isChecked()));
        this.mAccount.setEnable(Boolean.valueOf(this.mEnableCheckBox.isChecked()));
        this.mAccount.setNote(this.mNoteEditText.getText() + "");
    }

    private void turnOffAutoKeyboardPopUp() {
        getWindow().setSoftInputMode(3);
    }

    protected void setGoogleAccountPicker() {
        this.mCredential = GoogleAccountCredential.usingOAuth2(this, Collections.singleton(TasksScopes.TASKS));
        android.accounts.Account[] accountsTmp = this.mCredential.getAllAccounts();
        this.mService = new Tasks.Builder(this.httpTransport, this.jsonFactory, this.mCredential).setApplicationName("Google-TasksAndroidSample/1.0").build();
    }

    void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        runOnUiThread(new Runnable() {
            public void run() {
                GooglePlayServicesUtil.getErrorDialog(connectionStatusCode, AccountEditActivity.this, 0).show();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (resultCode == -1) {
                    chooseAccount();
                    return;
                } else {
                    checkGooglePlayServicesAvailable();
                    return;
                }
            case 1:
                if (resultCode == -1) {
                    AsyncLoadTasks.run(this);
                    return;
                } else {
                    chooseAccount();
                    return;
                }
            case 2:
                if (resultCode == -1 && data != null && data.getExtras() != null) {
                    String accountName = data.getExtras().getString("authAccount");
                    if (accountName != null) {
                        this.mCredential.setSelectedAccountName(accountName);
                        Editor editor = getPreferences(0).edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.commit();
                        AsyncLoadTasks.run(this);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    private boolean checkGooglePlayServicesAvailable() {
        int connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (!GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            return true;
        }
        showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        return false;
    }

    private void chooseAccount() {
        startActivityForResult(this.mCredential.newChooseAccountIntent(), 2);
    }

    protected void verifyAccount(String accountName) {
        this.mCredential.setSelectedAccountName(accountName);
        AsyncLoadTasks.run(this);
    }
}
