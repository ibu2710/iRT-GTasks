package com.irt.tasks.ui.task;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import com.irt.tasks.app.businessobjects.Tag;
import com.irt.tasks.app.businessobjects.TagTask;
import com.irt.tasks.app.businessprocess.TagBp;
import com.irt.tasks.app.businessprocess.TagBpImpl;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import java.util.ArrayList;
import java.util.List;

public class TagSelectionListActivity extends AbstractEditListActivity {
    List<TagTask> mTagTasksOriginal;
    List<Tag> mTags;
    TagBp tagBp = new TagBpImpl();

    protected void performOnCreate(Bundle savedInstanceState) {
        if (this.mTask.getTagTasks() != null) {
            this.mTagTasksOriginal = new ArrayList<TagTask>(this.mTask.getTagTasks());
        }
    }

    private void refereshList() {
        setListAdapter(new TagInteractiveArrayAdapter(this, getTags()));
    }

    protected List<Tag> getTags() {
        this.mTags = this.tagBp.getAllTags();
        if (this.mTags.size() > 0) {
            List<TagTask> tagTasks = this.mTask.getTagTasks();
            if (tagTasks != null) {
                for (TagTask tagTask : tagTasks) {
                    setSelectedTag(tagTask.getTagId());
                }
            }
        }
        return this.mTags;
    }

    private void setSelectedTag(Long contextId) {
        for (Tag contextBase : this.mTags) {
            if (contextBase.getId().longValue() == contextId.longValue()) {
                contextBase.setSelected(true);
            }
        }
    }

    protected void performSetRadioButtons() {
        for (Tag contextBase : this.mTags) {
            if (contextBase.isSelected()) {
                return;
            }
        }
        this.mAnyRadioButton.setChecked(true);
        this.mTask.setAnySelected(true);
    }

    protected CharSequence getAnyRadioButtonText() {
        return "Any tag";
    }

    protected CharSequence getNoneRadioButtonText() {
        return "No tag";
    }

    protected void performCancel() {
        this.mTask.setTagTasks(this.mTagTasksOriginal);
    }

    protected void onResume() {
        super.onResume();
        refereshList();
    }

    protected void performNew() {
        editTag(new Tag());
    }

    private void editTag(Tag contextBase) {
        performSave();
        ArchitectureContext.setObject(contextBase);
        startActivity(new Intent(this, TagEditActivity.class));
    }

    protected void performSave() {
        List<TagTask> tagTasks = new ArrayList();
        for (Tag tag : this.mTags) {
            if (tag.isSelected()) {
                tagTasks.add(new TagTask(tag.getId(), this.mTask.getId()));
            }
        }
        this.mTask.setTagTasks(tagTasks);
        this.mTask.setAndOperation(this.mAndOperatorRadioButton.isChecked());
    }

    protected void performListOnItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        CheckBox checkbox = ((TagInteractiveArrayAdapter.ViewHolder) view.getTag()).checkbox;
        if (checkbox.isChecked()) {
            checkbox.setChecked(false);
        } else {
            checkbox.setChecked(true);
        }
    }

    protected void performDeleteSelectedListItem(final int position) {
        new Builder(this).setTitle("Delete context").setMessage("Tag [" + ((Tag) this.mTags.get(position)).getName() + "] will be deleted?").setPositiveButton("OK", new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                TagSelectionListActivity.this.tagBp.deleteTag((Tag) TagSelectionListActivity.this.mTags.get(position));
                TagSelectionListActivity.this.tagBp.deleteAllTagTasksByTagId(((Tag) TagSelectionListActivity.this.mTags.get(position)).getId());
                TagSelectionListActivity.this.onResume();
            }
        }).setNegativeButton("Cancel", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }

    protected void performEditSelectedListItem(int position) {
        editTag((Tag) this.mTags.get(position));
    }
}
