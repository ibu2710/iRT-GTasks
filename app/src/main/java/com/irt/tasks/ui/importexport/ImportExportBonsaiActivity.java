package com.irt.tasks.ui.importexport;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.sync.android.DbxAccountManager;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.ExternalApplicationsBp;
import com.irt.tasks.app.businessprocess.ExternalApplicationsBpImpl;
import com.irt.tasks.app.businessprocess.TasksListBp;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.DropboxDaoImpl;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.sync.DropboxSyncHelper;

import java.util.ArrayList;
import java.util.List;

public class ImportExportBonsaiActivity extends AbstractActivity {
    private static final String EXPORT_CSV_TASKID_NOTE = "This will put Task ID on Bonsai Conatct field to preserve decorators during import.";
    private static final int IMPORT_REPLACE_THIS_LIST = 0;
    private static final String IMPORT_REPLACE_THIS_LIST_NOTE = "If Task ID is in Bonsai Contact field, preserve task decorators.";
    private static final String NO_FILES = "No files to import";
    static final int REQUEST_LINK_TO_DBX = 0;
    ConfigManager config;
    ExternalApplicationsBp externalApplicationsBp = new ExternalApplicationsBpImpl();
    private DbxAccountManager mAccountManager;
    Button mCancelButton;
    TextView mDateFormatDefaultValueTextView;
    TextView mDateFormatLabelTextView;
    Spinner mDateFormatSpinner;
    TextView mDirectoryLabelTextView;
    TextView mDirectoryNoteLable;
    TextView mDirectoryTextView;
    CheckBox mDropboxMode;
    RadioButton mExportButton;
    TextView mExportTypeDefaultValueTextView;
    EditText mFileNameEditText;
    TextView mFileNameTextView;
    Spinner mFilenameSpinner;
    List<String> mFiles = new ArrayList();
    RadioButton mImportButton;
    Button mImportExportButton;
    TextView mImportExportTypeLabelTextView;
    Spinner mImportExportTypeSpinner;
    EditText mNewImportListName;
    TextView mNoteTextView;
    TasksList mTasksList;
    TextView mTasksListNameTextView;
    TasksListBp tasksListBp = new TasksListBpImpl();

    public class ProcessImportExportProgressBar extends AsyncTask<Boolean, Void, Long> {
        Context mContext;
        String mMessage;
        private ProgressDialog progressDialog;

        public ProcessImportExportProgressBar(Context context, String message) {
            this.mContext = context;
            this.mMessage = message;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = ProgressDialog.show(this.mContext, null, StringUtil.isNullOrEmpty(this.mMessage) ? ImportExportBonsaiActivity.this.getMessage() : this.mMessage, true, true);
        }

        @Override
        protected void onPostExecute(Long result) {
            super.onPostExecute(result);
            ImportExportBonsaiActivity.this.initFilenameSpinner();
            this.progressDialog.dismiss();
            if (!ImportExportBonsaiActivity.this.isImportExportPossible()) {
                ImportExportBonsaiActivity.this.mFiles.clear();
                if (ImportExportBonsaiActivity.this.config.isDropboxMode()) {
                    Toast.makeText(this.mContext, "Error: Offline, please check Internet connection.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this.mContext, "Error: SD Card not mounted, please turn OFF USB storage if it is ON.", Toast.LENGTH_LONG).show();
                }
            } else if (StringUtil.isNullOrEmpty(this.mMessage)) {
                Toast.makeText(this.mContext, "Completed processing.", Toast.LENGTH_LONG).show();
                ImportExportBonsaiActivity.this.config.setPreferenceAccessed(true);
                ((Activity) this.mContext).finish();
            }
        }

        @Override
        protected Long doInBackground(Boolean... isListFiles) {
            Long valueOf;
            synchronized (this) {
                ConfigManager config = ConfigManager.getInstance();
                if (ImportExportBonsaiActivity.this.isImportExportPossible()) {
                    if (isListFiles[0] == null || !isListFiles[0].booleanValue()) {
                        if (!config.isSyncing()) {
                            if (!ImportExportBonsaiActivity.this.mImportButton.isChecked()) {
                                switch (ImportExportBonsaiActivity.this.mImportExportTypeSpinner.getSelectedItemPosition()) {
                                    case 0:
                                        ImportExportBonsaiActivity.this.externalApplicationsBp.exportToRemoteServer(ImportExportBonsaiActivity.this.mTasksList, ImportExportBonsaiActivity.this.mFileNameEditText.getText().toString(), config.getBonsaiDateFormat().intValue(), true);
                                        break;
                                    case 1:
                                        ImportExportBonsaiActivity.this.externalApplicationsBp.exportToRemoteServer(ImportExportBonsaiActivity.this.mTasksList, ImportExportBonsaiActivity.this.mFileNameEditText.getText().toString(), config.getBonsaiDateFormat().intValue(), false);
                                        break;
                                    default:
                                        break;
                                }
                            }
                            switch (ImportExportBonsaiActivity.this.mImportExportTypeSpinner.getSelectedItemPosition()) {
                                case 0:
                                    ImportExportBonsaiActivity.this.externalApplicationsBp.importReplaceListFromRemoteServer(ImportExportBonsaiActivity.this.mTasksList, ImportExportBonsaiActivity.this.mFilenameSpinner.getSelectedItem() + "");
                                    break;
                                case 1:
                                    ImportExportBonsaiActivity.this.externalApplicationsBp.importAppendToListFromRemoteServer(ImportExportBonsaiActivity.this.mTasksList, ImportExportBonsaiActivity.this.mFilenameSpinner.getSelectedItem() + "");
                                    break;
                                case 2:
                                    ImportExportBonsaiActivity.this.externalApplicationsBp.importToNewListFromRemoteServer(ImportExportBonsaiActivity.this.mTasksList.getAccountId(), ImportExportBonsaiActivity.this.mNewImportListName.getText() + "", ImportExportBonsaiActivity.this.mFilenameSpinner.getSelectedItem() + "");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    ImportExportBonsaiActivity.this.mFiles = ImportExportBonsaiActivity.this.externalApplicationsBp.getRemoteFilesListForImport();
                }
                valueOf = Long.valueOf(0);
            }
            return valueOf;
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.import_export);
        this.config = ConfigManager.getInstance();
        AppUtil.turnOffAutoKeyboardPopUp(this);
        Object obj = ArchitectureContext.getObject();
        if (obj != null && (obj instanceof TasksList)) {
            this.mTasksList = (TasksList) ArchitectureContext.getObject();
        }
        initViews();
        setImportExportButtonListener();
        setCancelButtonListener();
        if (this.config.isLockDateFormat()) {
            this.mDateFormatDefaultValueTextView.setVisibility(View.VISIBLE);
            //this.mDateFormatDefaultValueTextView.setText(ItemSortKeyBase.MIN_BUT_TWO_SORT_KEY + this.mDateFormatSpinner.getSelectedItem());
            this.mDateFormatSpinner.setVisibility(View.GONE);
        } else {
            this.mDateFormatSpinner.setVisibility(View.VISIBLE);
            this.mDateFormatDefaultValueTextView.setVisibility(View.GONE);
        }
        if (this.mTasksList == null) {
            Long tasksListId = Long.valueOf(getIntent().getLongExtra("taskListId", 0));
            this.mTasksList = this.tasksListBp.get(tasksListId);
            this.config.setLastSelectedTasksList(tasksListId);
        }
        this.mTasksListNameTextView.setText(this.mTasksList.getName());
    }

    private void initViews() {
        this.mTasksListNameTextView = (TextView) findViewById(R.id.tasks_list_name);
        this.mImportExportButton = (Button) findViewById(R.id.import_export_button);
        this.mCancelButton = (Button) findViewById(R.id.cancel_button);
        this.mDateFormatSpinner = (Spinner) findViewById(R.id.date_format_import_export);
        this.mImportExportTypeSpinner = (Spinner) findViewById(R.id.import_export_type);
        this.mDropboxMode = (CheckBox) findViewById(R.id.dropbox_mode_checkbox);
        this.mImportButton = (RadioButton) findViewById(R.id.importid);
        this.mExportButton = (RadioButton) findViewById(R.id.export);
        this.mImportExportTypeLabelTextView = (TextView) findViewById(R.id.import_export_type_label);
        this.mFileNameEditText = (EditText) findViewById(R.id.export_filename_edit_text);
        DropboxSyncHelper.addDelayedIdleToEditText(this.mFileNameEditText);
        this.mFilenameSpinner = (Spinner) findViewById(R.id.import_export_filename);
        this.mNoteTextView = (TextView) findViewById(R.id.note);
        this.mDirectoryLabelTextView = (TextView) findViewById(R.id.directory_label);
        this.mDirectoryTextView = (TextView) findViewById(R.id.directory);
        this.mFileNameTextView = (TextView) findViewById(R.id.filename);
        this.mNewImportListName = (EditText) findViewById(R.id.new_list_import_name);
        DropboxSyncHelper.addDelayedIdleToEditText(this.mNewImportListName);
        this.mDateFormatLabelTextView = (TextView) findViewById(R.id.date_format_import_export_label);
        this.mDateFormatDefaultValueTextView = (TextView) findViewById(R.id.date_format_import_export_default);
        this.mExportTypeDefaultValueTextView = (TextView) findViewById(R.id.export_type__default);
        this.mDirectoryNoteLable = (TextView) findViewById(R.id.directory_note_label);
        if (this.mDropboxMode.isChecked()) {
            checkForDropboxAccount();
        } else {
            getListOfFilesForImportExport();
            setImportExportTextAndVisibility();
        }
        initImportExportSpinner();
        initImportExportButtons();
        initDateFormatSpinner();
        initDropboxMode();
    }

    private void initImportExportButtons() {
        boolean z = true;
        this.mImportButton.setChecked(true);
        RadioButton radioButton = this.mExportButton;
        if (this.mImportButton.isChecked()) {
            z = false;
        }
        radioButton.setChecked(z);
        initImportRadioButton();
    }

    private void initImportRadioButton() {
        this.mImportButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ImportExportBonsaiActivity.this.setImportExportMode();
                ImportExportBonsaiActivity.this.getListOfFilesForImportExport();
            }
        });
    }

    private void setImportExportMode() {
        initImportExportSpinner();
        setImportExportTextAndVisibility();
    }

    private void setImportExportTextAndVisibility() {
        if (this.mImportButton.isChecked()) {
            this.mImportExportTypeLabelTextView.setText("Import type:");
            this.mImportExportButton.setText("Import");
            switch (this.mImportExportTypeSpinner.getSelectedItemPosition()) {
                case 0:
                    this.mNewImportListName.setVisibility(View.GONE);
                    this.mNoteTextView.setVisibility(View.VISIBLE);
                    this.mNoteTextView.setText(IMPORT_REPLACE_THIS_LIST_NOTE);
                    break;
                case 1:
                    this.mNewImportListName.setVisibility(View.GONE);
                    this.mNoteTextView.setVisibility(View.GONE);
                    break;
                case 2:
                    this.mNewImportListName.setVisibility(View.VISIBLE);
                    if (NO_FILES.equals(this.mFilenameSpinner.getSelectedItem())) {
                        this.mNewImportListName.setText("");
                    } else {
                        this.mNewImportListName.setText(getNewListName());
                    }
                    this.mNoteTextView.setVisibility(View.GONE);
                    break;
            }
            this.mExportTypeDefaultValueTextView.setVisibility(View.GONE);
            this.mImportExportTypeSpinner.setVisibility(View.VISIBLE);
        } else {
            this.mImportExportButton.setText("Export");
            this.mImportExportTypeLabelTextView.setText("Export type:");
            switch (this.mImportExportTypeSpinner.getSelectedItemPosition()) {
                case 0:
                    this.mNoteTextView.setVisibility(View.VISIBLE);
                    this.mNoteTextView.setText(EXPORT_CSV_TASKID_NOTE);
                    break;
                case 1:
                    this.mNoteTextView.setVisibility(View.GONE);
                    break;
            }
            if (this.config.isLockExportType()) {
                this.mExportTypeDefaultValueTextView.setVisibility(View.VISIBLE);
                //this.mExportTypeDefaultValueTextView.setText(ItemSortKeyBase.MIN_BUT_TWO_SORT_KEY + this.mImportExportTypeSpinner.getSelectedItem());
                this.mImportExportTypeSpinner.setVisibility(View.GONE);
            } else {
                this.mImportExportTypeSpinner.setVisibility(View.VISIBLE);
                this.mExportTypeDefaultValueTextView.setVisibility(View.GONE);
            }
        }
        if (this.mDropboxMode.isChecked()) {
            this.mDirectoryLabelTextView.setText("Dropbox directory:");
            this.mDirectoryTextView.setText("/Apps/irtgtasksoutliner/bonsai/");
            this.mDirectoryNoteLable.setVisibility(View.VISIBLE);
            return;
        }
        this.mDirectoryNoteLable.setVisibility(View.GONE);
        this.mDirectoryLabelTextView.setText("SD Card directory:");
        this.mDirectoryTextView.setText(AppUtil.getBonsaiSdCardDirectory());
    }

    private String getNewListName() {
        String newListName = this.mFileNameTextView.getText() + "";
        int dotLocation = newListName.indexOf(Globals.PERIOD);
        if (dotLocation > 0) {
            return newListName.substring(0, dotLocation);
        }
        return newListName;
    }

    private void initDropboxMode() {
        this.mDropboxMode.setChecked(this.config.isDropboxMode());
        setImportExportTextAndVisibility();
        this.mDropboxMode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ImportExportBonsaiActivity.this.config.setDropboxMode(ImportExportBonsaiActivity.this.mDropboxMode.isChecked());
                if (ImportExportBonsaiActivity.this.mDropboxMode.isChecked()) {
                    ImportExportBonsaiActivity.this.checkForDropboxAccount();
                    return;
                }
                ImportExportBonsaiActivity.this.getListOfFilesForImportExport();
                ImportExportBonsaiActivity.this.setImportExportTextAndVisibility();
            }
        });
    }

    private void getListOfFilesForImportExport() {
        this.mFiles.clear();
        new ProcessImportExportProgressBar(this, "Getting list of files...").execute(new Boolean[]{Boolean.valueOf(true)});
    }

    private void initFilenameSpinner() {
        final boolean isFilesFound;
        int size;
        if (this.mFiles == null) {
            this.mFiles = new ArrayList();
        }
        if (this.mFiles.size() > 0) {
            isFilesFound = true;
        } else {
            isFilesFound = false;
        }
        if (isFilesFound) {
            size = this.mFiles.size();
        } else {
            size = 1;
        }
        String[] textArray = new String[size];
        if (this.mImportButton.isChecked()) {
            this.mImportExportTypeSpinner.setPrompt("Import file");
            if (isFilesFound) {
                textArray = (String[]) this.mFiles.toArray(new String[this.mFiles.size()]);
            } else {
                textArray[0] = NO_FILES;
            }
        } else {
            this.mFiles.add(0, "Enter file name");
            textArray = (String[]) this.mFiles.toArray(new String[this.mFiles.size()]);
            this.mImportExportTypeSpinner.setPrompt("Export file");
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, textArray);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.mFilenameSpinner.setAdapter(spinnerArrayAdapter);
        this.mFilenameSpinner.setSelection(selectFile(), true);
        if (this.mImportButton.isChecked()) {
            this.mFileNameEditText.setVisibility(View.GONE);
            this.mFileNameTextView.setVisibility(View.VISIBLE);
            if (isFilesFound) {
                this.mFileNameTextView.setText(this.mFilenameSpinner.getSelectedItem().toString());
                this.mImportExportButton.setEnabled(true);
            } else {
                this.mFileNameTextView.setText("Put Bonsai CSV files on bonsai directory");
                this.mImportExportButton.setEnabled(false);
            }
        } else {
            this.mImportExportButton.setEnabled(true);
            this.mFileNameEditText.setVisibility(View.VISIBLE);
            this.mFileNameTextView.setVisibility(View.GONE);
        }
        this.mFilenameSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                if (ImportExportBonsaiActivity.this.mImportButton.isChecked()) {
                    if (isFilesFound) {
                        ImportExportBonsaiActivity.this.mFileNameTextView.setText(adapterView.getItemAtPosition(position).toString());
                        ImportExportBonsaiActivity.this.mNewImportListName.setText(ImportExportBonsaiActivity.this.getNewListName());
                    }
                } else if (ImportExportBonsaiActivity.this.mFilenameSpinner.getSelectedItemPosition() == 0) {
                    ImportExportBonsaiActivity.this.mFileNameEditText.setText(ImportExportBonsaiActivity.this.mTasksList.getName() + ".csv");
                } else {
                    ImportExportBonsaiActivity.this.mFileNameEditText.setText(adapterView.getItemAtPosition(position).toString());
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private int selectFile() {
        if (this.mImportButton.isChecked()) {
            for (int i = 0; i < this.mFiles.size(); i++) {
                String filename = (String) this.mFiles.get(i);
                int dotLocation = filename.indexOf(Globals.PERIOD);
                if (dotLocation > 0) {
                    filename = ((String) this.mFiles.get(i)).substring(0, dotLocation);
                }
                if (filename.contains(this.mTasksList.getName())) {
                    return i;
                }
            }
        }
        return 0;
    }

    private void initImportExportSpinner() {
        String[] textArray;
        if (this.mImportButton.isChecked()) {
            textArray = getResources().getStringArray(R.array.import_types);
            this.mImportExportTypeSpinner.setPrompt("Import type");
        } else {
            textArray = getResources().getStringArray(R.array.export_types);
            this.mImportExportTypeSpinner.setPrompt("Export type");
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, textArray);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.mImportExportTypeSpinner.setAdapter(spinnerArrayAdapter);
        this.mImportExportTypeSpinner.setSelection(0);
        this.mImportExportTypeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ImportExportBonsaiActivity.this.setImportExportTextAndVisibility();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initDateFormatSpinner() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.date_format_import_export));
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.mDateFormatSpinner.setAdapter(spinnerArrayAdapter);
        this.mDateFormatSpinner.setSelection(this.config.getBonsaiDateFormat().intValue());
        this.mDateFormatSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ImportExportBonsaiActivity.this.config.setBonsaiDateFormat(Integer.valueOf(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setImportExportButtonListener() {
        this.mImportExportButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ImportExportBonsaiActivity.this.mImportButton.isChecked() && ImportExportBonsaiActivity.this.mImportExportTypeSpinner.getSelectedItemPosition() == 0) {
                    ImportExportBonsaiActivity.this.showDialog(0);
                } else {
                    ImportExportBonsaiActivity.this.performImportExport();
                }
            }
        });
    }

    private void performImportExport() {
        new ProcessImportExportProgressBar(this, null).execute(new Boolean[]{Boolean.valueOf(false)});
    }

    private void setCancelButtonListener() {
        this.mCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ImportExportBonsaiActivity.this.finish();
            }
        });
    }

    private String getMessage() {
        if (this.mImportButton.isChecked()) {
            return "Importing tasks ...";
        }
        return "Exporting tasks ...";
    }

    private boolean isImportExportPossible() {
        if (this.config.isDropboxMode()) {
            return true;
        }
        return AppUtil.isExternalStorageMounted();
    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return new Builder(this).setTitle(R.string.alert_dialog_import_replace_list).setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ImportExportBonsaiActivity.this.performImportExport();
                    }
                }).setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).create();
            default:
                return null;
        }
    }

    private void checkForDropboxAccount() {
        if (this.mDropboxMode.isChecked()) {
            this.mAccountManager = DbxAccountManager.getInstance(getApplicationContext(), DropboxDaoImpl.APP_KEY, DropboxDaoImpl.SECRET_KEY);
            if (this.mAccountManager.hasLinkedAccount()) {
                getListOfFilesForImportExport();
                setImportExportTextAndVisibility();
                this.config.setDropboxMode(true);
                return;
            }
            this.mAccountManager.startLink((Activity) this, 0);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != 0) {
            super.onActivityResult(requestCode, resultCode, data);
        } else if (resultCode == -1) {
            getListOfFilesForImportExport();
            setImportExportTextAndVisibility();
            this.config.setDropboxMode(true);
        } else {
            this.mDropboxMode.setChecked(false);
            this.config.setDropboxMode(false);
            getListOfFilesForImportExport();
            setImportExportTextAndVisibility();
        }
    }
}
