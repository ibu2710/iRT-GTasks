package com.irt.tasks.ui;

import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.app.framework.trial.TrialSpecificUtility;

public class About {
    public static final StringBuffer getAbout(String versionName) {
        ConfigManager config = ConfigManager.getInstance();
        AppUtil.checkForProVersion();
        StringBuffer content = new StringBuffer();
        content.append("<b>" + AppUtil.getAppName() + " BETA</b> is a Google Tasks Android outliner that features:");
        content.append("<ul>");
        content.append("<li>Powerful outlining capabilities");
        content.append("<li>Google Tasks auto-sync");
        content.append("<li>iRT Calendar Pro integration");
        content.append("<li>Drag and drop of lists/tasks");
        content.append("<li>Multi-task edit");
        content.append("<li>Email lists/outlines");
        content.append("<li><a href='http://www.irtgtasksoutliner.com'>And many more...</a>");
        content.append("</ul>");
        content.append("<p style='text-align: center;'>Version <b>" + versionName + "</b><br>");
        if (!config.isProVersion()) {
            content.append("Expires on <b>" + TrialSpecificUtility.getTrialEndDate() + "</b><br><br>");
        }
        content.append("<a href='mailto:support@iRTWebSolutions.com'>support@iRTWebSolutions.com</a><br><br>");
        content.append("&copy; 2015 <a href='http://www.irtgtasksoutliner.com'>iRT Web Solutions</a>, LLC<br>");
        content.append("All Rights Reserved.</p>");
        return content;
    }
}
