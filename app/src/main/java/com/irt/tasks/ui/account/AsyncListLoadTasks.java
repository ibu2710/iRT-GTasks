package com.irt.tasks.ui.account;

import java.io.IOException;

class AsyncListLoadTasks extends CommonListAsyncTask {
    AsyncListLoadTasks(AccountActivity tasksSample) {
        super(tasksSample);
    }

    protected void doInBackground() throws IOException {
        this.client.tasklists().list().execute();
    }

    public static void run(AccountActivity tasksSample) {
        new AsyncListLoadTasks(tasksSample).execute(new Void[0]);
    }
}
