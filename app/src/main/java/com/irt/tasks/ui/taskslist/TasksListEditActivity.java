package com.irt.tasks.ui.taskslist;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.AccountBp;
import com.irt.tasks.app.businessprocess.AccountBpImpl;
import com.irt.tasks.app.businessprocess.TasksListBp;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.sync.DropboxSyncHelper;

import java.util.List;

public class TasksListEditActivity extends AbstractActivity {
    AccountBp accountBp = new AccountBpImpl();
    ConfigManager config;
    TextView mAccountAutoSyncTextView;
    TextView mAccountNameTextView;
    String[] mAccountNames;
    Spinner mAccountSpinner;
    List<Account> mAccounts;
    TextView mAdvancedOptionsTextView;
    CheckBox mApplyColorToTasksCheckBox;
    CheckBox mAutoSyncCheckBox;
    Button mCancelButton;
    Button mDeleteButton;
    TextView mFontColorSample;
    CheckBox mHighlightCheckBox;
    boolean mIsNew = false;
    EditText mNameEditText;
    EditText mNoteEditText;
    Button mSaveButton;
    int mSelectedAccountPostion = 0;
    CheckBox mShowTaskDueDatesCheckBox;
    CheckBox mShowTaskNotesCheckBox;
    TasksList mTasksList;
    TextView mTasksListNameLabelTextView;
    CheckBox mVisibleCheckBox;
    TasksListBp tasksListBp = new TasksListBpImpl();

    class C19561 implements OnItemSelectedListener {
        C19561() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View arg1, int position, long arg3) {
            TasksListEditActivity.this.mSelectedAccountPostion = position;
            TasksListEditActivity.this.setVisibility();
            TasksListEditActivity.this.setAccountAutoSync();
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    class C19582 implements OnClickListener {

        class C19571 implements DialogInterface.OnClickListener {
            C19571() {
            }

            public void onClick(DialogInterface dialog, int button) {
                TasksListEditActivity.this.setResult(-1);
                TasksListEditActivity.this.finish();
            }
        }

        C19582() {
        }

        public void onClick(View v) {
            new Builder(TasksListEditActivity.this).setTitle(R.string.delete_title).setMessage(getDeleteMessageId()).setPositiveButton(R.string.alert_dialog_ok, new C19571()).setNegativeButton(R.string.alert_dialog_cancel, null).show();
        }

        int getDeleteMessageId() {
            if (AppUtil.isOfflineAccount(TasksListEditActivity.this.mTasksList.getAccountId())) {
                return R.string.delete_this_offline_tasks_list;
            }
            return R.string.delete_this_tasks_list;
        }
    }

    class C19593 implements OnClickListener {
        C19593() {
        }

        public void onClick(View v) {
            TasksListEditActivity.this.setResult(-1);
            TasksListEditActivity.this.finish();
        }
    }

    class C19604 implements OnClickListener {
        C19604() {
        }

        public void onClick(View v) {
            TasksListEditActivity.this.setAccountFromFields();
            TasksListEditActivity.this.tasksListBp.saveOrUpdateWithTimeStamp(TasksListEditActivity.this.mTasksList);
            TasksListEditActivity.this.setResult(-1);
            TasksListEditActivity.this.finish();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tasks_list_edit);
        this.config = ConfigManager.getInstance();
        this.mTasksList = (TasksList) ArchitectureContext.getObject();
        if (this.mTasksList.getId() == null) {
            this.mIsNew = true;
        }
        initControl();
        if (this.mIsNew) {
            this.mNameEditText.requestFocus();
        } else {
            fillfields();
        }
        setAccountSpinner();
        initSaveButton();
        initCancelButton();
        initDeleteButton();
        setVisibility();
    }

    private void setVisibility() {
        if (this.mIsNew) {
            this.mDeleteButton.setVisibility(View.GONE);
            this.mAccountSpinner.setVisibility(View.VISIBLE);
            this.mAccountNameTextView.setVisibility(View.GONE);
            this.mAutoSyncCheckBox.setVisibility(View.VISIBLE);
            this.mAccountAutoSyncTextView.setVisibility(View.VISIBLE);
            this.mAutoSyncCheckBox.setChecked(true);
        }
        if (DatabaseHelper.OFFLINE_ACCOUNT_ID.equals(getAccountToCheck().getId())) {
            this.mAutoSyncCheckBox.setVisibility(View.GONE);
            this.mAccountAutoSyncTextView.setVisibility(View.GONE);
            this.mAutoSyncCheckBox.setChecked(false);
        }
        if (this.mTasksList.istActiveTasksList().booleanValue()) {
            this.mDeleteButton.setVisibility(View.GONE);
        }
        setListNameLabel();
    }

    private void setListNameLabel() {
        SpannableStringBuilder finalListNameLabel = new SpannableStringBuilder();
        if (this.mTasksList.istActiveTasksList().booleanValue()) {
            SpannableStringBuilder listNameLabel = new SpannableStringBuilder("List Name");
            listNameLabel.setSpan(new StyleSpan(1), 0, listNameLabel.length(), 0);
            SpannableStringBuilder googleDefaultListLabel = new SpannableStringBuilder("     (Google default list, can't delete)");
            //googleDefaultListLabel.setSpan(new ForegroundColorSpan(-7829368), 0, googleDefaultListLabel.length(), 0);
            googleDefaultListLabel.setSpan(new StyleSpan(0), 0, googleDefaultListLabel.length(), 0);
            finalListNameLabel = new SpannableStringBuilder();
            finalListNameLabel.append(listNameLabel);
            finalListNameLabel.append(googleDefaultListLabel);
            this.mTasksListNameLabelTextView.setText(finalListNameLabel);
        } else {
            finalListNameLabel = new SpannableStringBuilder("List Name");
            finalListNameLabel.setSpan(new StyleSpan(1), 0, finalListNameLabel.length(), 0);
        }
        this.mTasksListNameLabelTextView.setText(finalListNameLabel);
    }

    private Account getAccountToCheck() {
        if (this.mIsNew) {
            return (Account) this.mAccounts.get(this.mSelectedAccountPostion);
        }
        return this.mTasksList.getAccount();
    }

    private void fillfields() {
        this.mNameEditText.setText(this.mTasksList.getName());
        this.mAutoSyncCheckBox.setChecked(this.mTasksList.isAutoSync().booleanValue());
        this.mVisibleCheckBox.setChecked(this.mTasksList.isVisible().booleanValue());
        this.mNoteEditText.setText(this.mTasksList.getNote());
        this.mHighlightCheckBox.setChecked(this.mTasksList.isHighlight().booleanValue());
        this.mApplyColorToTasksCheckBox.setChecked(this.mTasksList.isColorTasks().booleanValue());
        this.mShowTaskNotesCheckBox.setChecked(this.mTasksList.isShowNote().booleanValue());
        this.mShowTaskDueDatesCheckBox.setChecked(this.mTasksList.isShowDueDate().booleanValue());
        setAccountName();
        setAccountAutoSync();
    }

    private void setAccountSpinner() {
        populateAccountNames();
        ArrayAdapter<String> accountNameAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, this.mAccountNames);
        accountNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.mAccountSpinner.setAdapter(accountNameAdapter);
        this.mAccountSpinner.setOnItemSelectedListener(new C19561());
        this.mAccountSpinner.setSelection(this.mSelectedAccountPostion);
    }

    private void populateAccountNames() {
        this.mAccounts = this.accountBp.getAllVisibleAccounts();
        this.mAccountNames = new String[this.mAccounts.size()];
        for (int i = 0; i < this.mAccounts.size(); i++) {
            this.mAccountNames[i] = ((Account) this.mAccounts.get(i)).getEmail();
        }
    }

    private void setAccountAutoSync() {
        SpannableStringBuilder accountAutoSyncFull = new SpannableStringBuilder("Account: ");
        SpannableStringBuilder accountAutoSync = new SpannableStringBuilder("");
        if (getAccountToCheck().isAutoSync().booleanValue()) {
            accountAutoSync.append("Auto-sync ON");
            accountAutoSync.setSpan(new ForegroundColorSpan(AppUtil.getGreenColorStatus()), 0, accountAutoSync.length(), 0);
        } else {
            accountAutoSync.append("Auto-sync OFF");
            accountAutoSync.setSpan(new ForegroundColorSpan(getRedColorStatus()), 0, accountAutoSync.length(), 0);
        }
        accountAutoSyncFull.append(accountAutoSync);
        this.mAccountAutoSyncTextView.setText(accountAutoSyncFull);
    }

    private void setAccountName() {
        SpannableStringBuilder accountNameFull = new SpannableStringBuilder("Account: ");
        SpannableStringBuilder accountName = new SpannableStringBuilder(this.mTasksList.getAccount().getEmail());
        if (getAccountToCheck().isEnable().booleanValue()) {
            accountName.setSpan(new ForegroundColorSpan(AppUtil.getGreenColorStatus()), 0, accountName.length(), 0);
        } else {
            accountName.setSpan(new ForegroundColorSpan(getRedColorStatus()), 0, accountName.length(), 0);
        }
        accountNameFull.append(accountName);
        this.mAccountNameTextView.setText(accountNameFull);
    }

    private int getRedColorStatus() {
        return -65536;
    }

    private void initControl() {
        this.mTasksListNameLabelTextView = (TextView) findViewById(R.id.tasks_list_name_label);
        this.mNameEditText = (EditText) findViewById(R.id.tasks_list_name);
        DropboxSyncHelper.addDelayedIdleToEditText(this.mNameEditText);
        this.mAccountNameTextView = (TextView) findViewById(R.id.account_name);
        this.mAccountAutoSyncTextView = (TextView) findViewById(R.id.account_auto_sync);
        this.mAccountSpinner = (Spinner) findViewById(R.id.account_spinner);
        this.mAutoSyncCheckBox = (CheckBox) findViewById(R.id.auto_sync_checkbox);
        this.mVisibleCheckBox = (CheckBox) findViewById(R.id.visible_checkbox);
        this.mFontColorSample = (TextView) findViewById(R.id.font_color_sample);
        this.mHighlightCheckBox = (CheckBox) findViewById(R.id.highlight_checkbox);
        this.mApplyColorToTasksCheckBox = (CheckBox) findViewById(R.id.apply_font_color_to_task);
        this.mAdvancedOptionsTextView = (TextView) findViewById(R.id.tasks_list_advanced);
        this.mNoteEditText = (EditText) findViewById(R.id.note);
        DropboxSyncHelper.addDelayedIdleToEditText(this.mNoteEditText);
        this.mShowTaskNotesCheckBox = (CheckBox) findViewById(R.id.show_task_notes_checkbox);
        this.mShowTaskDueDatesCheckBox = (CheckBox) findViewById(R.id.show_task_due_date);
        this.mDeleteButton = (Button) findViewById(R.id.delete_button);
        this.mSaveButton = (Button) findViewById(R.id.save_button);
        this.mCancelButton = (Button) findViewById(R.id.cancel_button);
    }

    private void initDeleteButton() {
        this.mDeleteButton.setOnClickListener(new C19582());
    }

    private void initCancelButton() {
        this.mCancelButton.setOnClickListener(new C19593());
    }

    private void initSaveButton() {
        this.mSaveButton.setOnClickListener(new C19604());
    }

    private void setAccountFromFields() {
        this.mTasksList.setName(this.mNameEditText.getText() + "");
        this.mTasksList.setVisible(Boolean.valueOf(this.mVisibleCheckBox.isChecked()));
        this.mTasksList.setNote(this.mNoteEditText.getText() + "");
        this.mTasksList.setHighlight(Boolean.valueOf(this.mHighlightCheckBox.isChecked()));
        this.mTasksList.setColorTasks(Boolean.valueOf(this.mApplyColorToTasksCheckBox.isChecked()));
        this.mTasksList.setShowNote(Boolean.valueOf(this.mShowTaskNotesCheckBox.isChecked()));
        this.mTasksList.setShowDueDate(Boolean.valueOf(this.mShowTaskDueDatesCheckBox.isChecked()));
        if (this.mIsNew) {
            this.mTasksList.setAccountId(((Account) this.mAccounts.get(this.mSelectedAccountPostion)).getId());
            this.mTasksList.setAccount((Account) this.mAccounts.get(this.mSelectedAccountPostion));
        }
        if (!DatabaseHelper.OFFLINE_ACCOUNT_ID.equals(getAccountToCheck().getId())) {
            this.mTasksList.setAutoSync(Boolean.valueOf(this.mAutoSyncCheckBox.isChecked()));
        }
    }

    private void turnOffAutoKeyboardPopUp() {
        getWindow().setSoftInputMode(3);
    }
}
