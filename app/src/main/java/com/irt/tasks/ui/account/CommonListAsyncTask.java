package com.irt.tasks.ui.account;

import android.os.AsyncTask;
import android.view.View;

import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.services.tasks.Tasks;
import com.irt.tasks.R;
import com.irt.tasks.app.common.Constants;

import java.io.IOException;

abstract class CommonListAsyncTask extends AsyncTask<Void, Void, Boolean> {
    final AccountActivity activity;
    final Tasks client;
    private final View progressBar;

    protected abstract void doInBackground() throws IOException;

    CommonListAsyncTask(AccountActivity activity) {
        this.activity = activity;
        this.client = activity.mService;
        this.progressBar = activity.findViewById(R.id.title_refresh_progress);
    }

    protected void onPreExecute() {
        super.onPreExecute();
        AccountActivity accountActivity = this.activity;
        accountActivity.numAsyncTasks++;
        this.progressBar.setVisibility(View.VISIBLE);
    }

    protected final Boolean doInBackground(Void... ignored) {
        try {
            doInBackground();
            return Boolean.valueOf(true);
        } catch (UserRecoverableAuthIOException userRecoverableException) {
            this.activity.startActivityForResult(userRecoverableException.getIntent(), 1);
            return Boolean.valueOf(false);
        } catch (IOException e) {
            Utils.logAndShow(this.activity, Constants.TAG, e);
            return Boolean.valueOf(false);
        }
    }

    protected final void onPostExecute(Boolean success) {
        super.onPostExecute(success);
        AccountActivity accountActivity = this.activity;
        int i = accountActivity.numAsyncTasks - 1;
        accountActivity.numAsyncTasks = i;
        if (i == 0) {
            this.progressBar.setVisibility(View.GONE);
        }
        if (success.booleanValue()) {
            this.activity.editActivity();
        }
    }
}
