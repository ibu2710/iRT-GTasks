package com.irt.tasks.ui.email;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Email;
import com.irt.tasks.app.businessprocess.EmailBp;
import com.irt.tasks.app.businessprocess.EmailBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.EmailUtility;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.sync.DropboxSyncHelper;

public class EmailActivity extends AbstractActivity {
    ConfigManager config;
    EmailBp emailBp = new EmailBpImpl();
    Button mCancelButton;
    private Email mEmail;
    EditText mEmailAddressEditText;
    EditText mEmailNameEditText;
    EditText mExtraEmailsEditText;
    EditText mPasswordEditText;
    Button mSaveButton;
    EditText mSignatureEditText;
    Button mTestSendButton;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.email_details_edit);
        this.config = ConfigManager.getInstance();
        turnOffAutoKeyboardPopUp();
        initViews();
        initFields();
        initSaveButton();
        initCancelButton();
        initTestSendButton();
    }

    private void initFields() {
        this.mEmail = this.emailBp.get();
        if (this.mEmail == null) {
            this.mEmail = new Email();
        }
        this.mEmailNameEditText.setText(this.mEmail.getName());
        this.mEmailAddressEditText.setText(this.mEmail.getEmail());
        this.mPasswordEditText.setText(this.mEmail.getPassword());
        this.mSignatureEditText.setText(this.mEmail.getSignature());
        this.mExtraEmailsEditText.setText(this.mEmail.getExtraEmail());
    }

    private void initViews() {
        this.mEmailNameEditText = (EditText) findViewById(R.id.email_name);
        DropboxSyncHelper.addDelayedIdleToEditText(this.mEmailNameEditText);
        this.mEmailAddressEditText = (EditText) findViewById(R.id.email_address);
        DropboxSyncHelper.addDelayedIdleToEditText(this.mEmailAddressEditText);
        this.mPasswordEditText = (EditText) findViewById(R.id.password);
        DropboxSyncHelper.addDelayedIdleToEditText(this.mPasswordEditText);
        this.mSignatureEditText = (EditText) findViewById(R.id.email_signature);
        DropboxSyncHelper.addDelayedIdleToEditText(this.mSignatureEditText);
        this.mExtraEmailsEditText = (EditText) findViewById(R.id.extra_emails);
        DropboxSyncHelper.addDelayedIdleToEditText(this.mExtraEmailsEditText);
        this.mSaveButton = (Button) findViewById(R.id.save_button);
        this.mTestSendButton = (Button) findViewById(R.id.test_email_button);
        this.mCancelButton = (Button) findViewById(R.id.cancel_button);
    }

    private void initTestSendButton() {
        this.mTestSendButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EmailActivity.this.performTestSendEmail();
            }
        });
    }

    private boolean performTestSendEmail() {
        if (!validateEmailSetup()) {
            return false;
        }
        if (sendTestEmail()) {
            Toast.makeText(this, "Email sent to " + this.mEmailAddressEditText.getText(), Toast.LENGTH_LONG).show();
            return true;
        }
        AppUtil.showGenericWarningMessage(this, "Test Email", "Failed to send email, please check email setup or Phone's Internet access.");
        return false;
    }

    private boolean sendTestEmail() {
        try {
            return EmailUtility.sendEmailUsingJavaMail(this.mEmailAddressEditText.getText().toString(), this.mPasswordEditText.getText().toString(), new String[]{this.mEmailAddressEditText.getText().toString()}, "iRT GTasks Outliner Test Reminder Email", "This is a test iRT GTasks Outliner email." + this.mSignatureEditText.getText(), null, this.mEmailNameEditText.getText().toString());
        } catch (Exception e) {
            Log.e(Constants.TAG, "Could not send email", e);
            return false;
        }
    }

    private boolean validateEmailSetup() {
        StringBuffer message = new StringBuffer();
        String emailAddress = this.mEmailAddressEditText.getText().toString();
        if (!StringUtil.isNullOrEmpty(emailAddress)) {
            this.mEmailAddressEditText.setText(emailAddress.trim());
        }
        if (!EmailUtility.isEmailValid(this.mEmailAddressEditText.getText().toString())) {
            message.append("\n- Gmail address invalid format");
        }
        if (StringUtil.isNullOrEmpty(this.mPasswordEditText.getText().toString())) {
            message.append("\n- Password required");
        }
        String extraEmails = this.mExtraEmailsEditText.getText().toString();
        if (!(StringUtil.isNullOrEmpty(extraEmails) || AppUtil.validateExtraEmails(this.mExtraEmailsEditText, extraEmails))) {
            message.append("\n- Extra emails invalid format");
        }
        boolean isValid = StringUtil.isNullOrEmpty(message.toString());
        if (!isValid) {
            AppUtil.showGenericWarningMessage(this, "Email validation", message.toString());
        }
        return isValid;
    }

    private void initSaveButton() {
        this.mSaveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (EmailActivity.this.performTestSendEmail()) {
                    performSave();
                    EmailActivity.this.config.setReminderEmailValidated(Boolean.valueOf(true));
                    EmailActivity.this.finish();
                }
            }

            private void performSave() {
                EmailActivity.this.mEmail.setName(EmailActivity.this.mEmailNameEditText.getText().toString());
                EmailActivity.this.mEmail.setEmail(EmailActivity.this.mEmailAddressEditText.getText().toString());
                EmailActivity.this.mEmail.setPassword(EmailActivity.this.mPasswordEditText.getText().toString());
                EmailActivity.this.mEmail.setSignature(EmailActivity.this.mSignatureEditText.getText().toString());
                EmailActivity.this.mEmail.setExtraEmail(EmailActivity.this.mExtraEmailsEditText.getText().toString());
                EmailActivity.this.emailBp.saveOrUpdate(EmailActivity.this.mEmail);
            }
        });
    }

    private void initCancelButton() {
        this.mCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EmailActivity.this.finish();
            }
        });
    }

    private void turnOffAutoKeyboardPopUp() {
        getWindow().setSoftInputMode(3);
    }
}
