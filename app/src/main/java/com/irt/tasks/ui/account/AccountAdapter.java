package com.irt.tasks.ui.account;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessprocess.AccountBp;
import com.irt.tasks.app.businessprocess.AccountBpImpl;
import com.irt.tasks.ui.dragndrop.DropListener;
import com.irt.tasks.ui.dragndrop.RemoveListener;
import java.util.List;

public final class AccountAdapter extends BaseAdapter implements RemoveListener, DropListener {
    AccountBp accountBp = new AccountBpImpl();
    private List<Account> mAccounts;
    private LayoutInflater mInflater;

    public AccountAdapter(Context context, List<Account> content) {
        init(context, content);
    }

    private void init(Context context, List<Account> content) {
        this.mInflater = LayoutInflater.from(context);
        this.mAccounts = content;
    }

    public List<Account> getSortedAccounts() {
        return this.mAccounts;
    }

    public int getCount() {
        return this.mAccounts.size();
    }

    public Object getItem(int position) {
        return getAccount(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        getAccount(position).setSortPosition(Integer.valueOf(position));
        return AccountAdapterHelper.getView(getAccount(position), convertView, this.mInflater);
    }

    private Account getAccount(int position) {
        return (Account) this.mAccounts.get(position);
    }

    public void onRemove(int which) {
        if (which >= 0 && which <= this.mAccounts.size()) {
            this.mAccounts.remove(which);
        }
    }

    public void onDrop(int from, int to) {
        Account temp = getAccount(from);
        this.mAccounts.remove(from);
        this.mAccounts.add(to, temp);
    }
}
