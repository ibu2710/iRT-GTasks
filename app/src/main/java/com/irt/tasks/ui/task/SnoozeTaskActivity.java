package com.irt.tasks.ui.task;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.CSVWriter;
import com.irt.tasks.app.common.DateUtil;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.TaskDao;
import com.irt.tasks.app.dataaccess.TaskDaoImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.customrepeat.CustomRepeatHelper;
import com.irt.tasks.ui.receiver.AlarmSetterHelper;
import com.irt.tasks.ui.taskslist.TasksListActivity;
import java.util.Calendar;
import java.util.Date;

public class SnoozeTaskActivity extends AbstractActivity {
    ConfigManager config;
    TextView mAutoSnoozeDurationTextView;
    Button mDismissButton;
    CheckBox mDoneCheckbox;
    TextView mDueDateDescriptionTextView;
    Button mEditButton;
    TextView mNextAlarmAutoSnoozeOffTextView;
    TextView mNextAlarmLabelTextView;
    TextView mNextAlarmTextView;
    TextView mNoteTextView;
    TextView mRepeatDesctriptionTextView;
    Button mSnoozeButton;
    Spinner mSnoozeDurationSpinner;
    Task mTask;
    TextView mTaskNameTextView;
    TaskBp taskBp = new TaskBpImpl();
    TaskDao taskDao = new TaskDaoImpl();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.config = ConfigManager.getInstance();
        setContentView(R.layout.snooze_task);
        this.mTask = this.taskBp.get(Long.valueOf(getIntent().getLongExtra("taskId", 0)));
        if (this.mTask == null) {
            finish();
        }
        if (this.mTask.isCompleted().booleanValue()) {
            performEditTask();
        }
        initViews();
        initFieldValues();
        initSnoozeButton();
        initEditButton();
        initDismissButton();
        initSnoozeDurationSpinner();
        initDoneCheckbox();
        this.config.setPreferenceAccessed(true);
    }

    private void initFieldValues() {
        this.mTaskNameTextView.setText(this.mTask.getName());
        this.mDueDateDescriptionTextView.setText(AppUtil.getDueDateDescription(this.mTask, "EEE, MMM d, yyyy", false));
        if (AppUtil.isAutoSnoozeOff(this.mTask)) {
            this.mAutoSnoozeDurationTextView.setVisibility(View.GONE);
            this.mNextAlarmLabelTextView.setText("Tap Snooze to be reminded");
        } else {
            this.mAutoSnoozeDurationTextView.setText("(" + AppUtil.getFinalAutoSnoozeDescrtiptionForTask(this, this.mTask) + ")");
        }
        setNextAlarmTextView();
        setTasksListAndNote();
        this.mRepeatDesctriptionTextView.setText(CustomRepeatHelper.getVerboseDescription(this.mTask.getReminderRepeatRule(), new Date(this.mTask.getDueDate().longValue()), this.mTask.isReminderRepeatFromCompletionDate().booleanValue()) + AppUtil.getNextDateIsOnWording(this.mTask));
    }

    private void setTasksListAndNote() {
        String note = new TasksListBpImpl().get(this.mTask.getTasksListId()).getName();
        if (!StringUtil.isNullOrEmpty(this.mTask.getNote())) {
            note = note + "\n\n" + this.mTask.getNote();
        }
        this.mNoteTextView.setText(note);
    }

    private void setNextAlarmTextView() {
        if (AppUtil.isAutoSnoozeOff(this.mTask)) {
            this.mNextAlarmTextView.setVisibility(View.GONE);
            this.mNextAlarmAutoSnoozeOffTextView.setVisibility(View.VISIBLE);
            this.mNextAlarmAutoSnoozeOffTextView.setText(AppUtil.getStringFromArrayValueBasedOnValue(this, R.array.manual_snooze_default_duration, R.array.manual_snooze_default_duration_values, this.config.getManualSnoozeDurationDefault().intValue()));
            this.mNextAlarmAutoSnoozeOffTextView.setTextSize(28.0f);
            return;
        }
        SpannableStringBuilder nextAlarmString = new SpannableStringBuilder("In " + AppUtil.formatTimeBeforeNextAlarm((((double) (this.mTask.getSnoozeDateTime().longValue() - AppUtil.getTodayDateTimeMilisecond())) / 1000.0d) / 60.0d) + CSVWriter.DEFAULT_LINE_END);
        nextAlarmString.append("   " + DateUtil.format(new Date(this.mTask.getSnoozeDateTime().longValue()), AppUtil.getNextAlarmFormat(true)));
        this.mNextAlarmTextView.setText(nextAlarmString);
    }

    private void initViews() {
        this.mDoneCheckbox = (CheckBox) findViewById(R.id.done_checkbox);
        this.mTaskNameTextView = (TextView) findViewById(R.id.task_name);
        this.mDueDateDescriptionTextView = (TextView) findViewById(R.id.due_date_description);
        this.mNextAlarmTextView = (TextView) findViewById(R.id.next_alarm);
        this.mNoteTextView = (TextView) findViewById(R.id.note);
        this.mNextAlarmLabelTextView = (TextView) findViewById(R.id.next_alarm_label);
        this.mNextAlarmAutoSnoozeOffTextView = (TextView) findViewById(R.id.next_alarm_auto_snooze_off);
        this.mRepeatDesctriptionTextView = (TextView) findViewById(R.id.repeat_description);
        this.mAutoSnoozeDurationTextView = (TextView) findViewById(R.id.auto_snooze_auto_duration_label);
        this.mSnoozeDurationSpinner = (Spinner) findViewById(R.id.snooze_duration_spinner);
        this.mDismissButton = (Button) findViewById(R.id.clear_button);
        this.mEditButton = (Button) findViewById(R.id.edit_button);
        this.mSnoozeButton = (Button) findViewById(R.id.ok_button);
    }

    private void initSnoozeDurationSpinner() {
        this.mSnoozeDurationSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SnoozeTaskActivity.this.mSnoozeDurationSpinner.setSelection(0);
                SnoozeTaskActivity.this.mSnoozeDurationSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        SnoozeTaskActivity.this.setSnoozeDateTime(SnoozeTaskActivity.this.getResources().getStringArray(R.array.snooze_duration_values)[SnoozeTaskActivity.this.mSnoozeDurationSpinner.getSelectedItemPosition()]);
                        SnoozeTaskActivity.this.clearNotification();
                        SnoozeTaskActivity.this.finish();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setSnoozeDateTime(String snoozeDurationMinutesStr) {
        int snoozeDurationMinutes = new Integer(snoozeDurationMinutesStr).intValue();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(AppUtil.getTodayDateTimeMilisecond());
        cal.add(Calendar.MINUTE, snoozeDurationMinutes);
        this.mTask.setSnoozeDateTime(Long.valueOf(cal.getTimeInMillis()));
        this.taskBp.saveOrUpdate(this.mTask, null);
        AlarmSetterHelper.scheduleAlarm(this, this.mTask, true);
    }

    private void initDoneCheckbox() {
        this.mDoneCheckbox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SnoozeTaskActivity.this.clearNotification();
                SnoozeTaskActivity.this.mTask.setCompleted(Boolean.valueOf(true));
                SnoozeTaskActivity.this.mTask.setSnoozeDateTime(Long.valueOf(0));
                SnoozeTaskActivity.this.mTask.setLastContentModifiedDate(Long.valueOf(AppUtil.getTodayDateTimeMilisecond()));
                SnoozeTaskActivity.this.taskBp.setTaskAsDoneAndAdjustAlarm(SnoozeTaskActivity.this.mTask, SnoozeTaskActivity.this);
                SnoozeTaskActivity.this.taskBp.saveOrUpdate(SnoozeTaskActivity.this.mTask, null);
                SnoozeTaskActivity.this.finish();
            }
        });
    }

    private void initDismissButton() {
        this.mDismissButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.clearSnooze(SnoozeTaskActivity.this, SnoozeTaskActivity.this.taskDao, SnoozeTaskActivity.this.mTask);
                SnoozeTaskActivity.this.finish();
            }
        });
    }

    private void clearNotification() {
        AppUtil.clearNotification(this, this.mTask);
    }

    private void initEditButton() {
        this.mEditButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SnoozeTaskActivity.this.clearNotification();
                SnoozeTaskActivity.this.performEditTask();
            }
        });
    }

    private void performEditTask() {
        Intent intent = new Intent(this, TasksListActivity.class);
        intent.putExtra("taskId", this.mTask.getId());
        intent.putExtra("taskListId", this.mTask.getTasksListId());
        startActivity(intent);
        finish();
    }

    private void initSnoozeButton() {
        if (AppUtil.isAutoSnoozeOff(this.mTask)) {
            this.mSnoozeButton.setText("Snooze");
        }
        this.mSnoozeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SnoozeTaskActivity.this.clearNotification();
                if (AppUtil.isAutoSnoozeOff(SnoozeTaskActivity.this.mTask)) {
                    SnoozeTaskActivity.this.setSnoozeDateTime(SnoozeTaskActivity.this.config.getManualSnoozeDurationDefault() + "");
                    SnoozeTaskActivity.this.finish();
                } else if (!SnoozeTaskActivity.this.config.isDropboxAutoSync()) {
                    SnoozeTaskActivity.this.finish();
                } else if (SnoozeTaskActivity.this.taskBp.get(SnoozeTaskActivity.this.mTask.getId()).getSnoozeDateTime().longValue() == 0) {
                    new Builder(SnoozeTaskActivity.this).setTitle("Notification dismissed").setMessage("This notification has already been dismissed, probably from another device.").setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SnoozeTaskActivity.this.finish();
                        }
                    }).show();
                } else {
                    SnoozeTaskActivity.this.finish();
                }
            }
        });
    }
}
