package com.irt.tasks.ui.task;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.ContextBase;
import com.irt.tasks.app.businessobjects.ContextIncludes;
import com.irt.tasks.app.businessprocess.ContextBp;
import com.irt.tasks.app.businessprocess.ContextBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.sync.DropboxSyncHelper;
import java.util.List;

public class ContextEditActivity extends AbstractActivity {
    ContextBp contextBp = new ContextBpImpl();
    Button mCancelButton;
    ContextBase mContextBase;
    TextView mContextIncludesDescription;
    TableLayout mContextIncludesDescriptionTableLayout;
    EditText mContextNameEditText;
    Button mDeleteButton;
    boolean mFirstTimeContextIncludesAccess = true;
    boolean mIsNewContext = false;
    Button mSaveButton;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.context_edit);
        this.mContextBase = (ContextBase) ArchitectureContext.getObject();
        this.mIsNewContext = this.mContextBase.getId() == null;
        initViewControls();
        initDeleteButton();
        initCancelButton();
        initSaveButton();
        initViewValues();
        initContextIncludesDescriptionTableLayout();
        if (this.mIsNewContext) {
            this.mContextNameEditText.requestFocus();
        }
    }

    protected void onResume() {
        super.onResume();
        setContextIncludesDescription();
    }

    private void setContextIncludesDescription() {
        List<ContextIncludes> contextIncludess;
        if (!this.mFirstTimeContextIncludesAccess || this.mContextBase.getId() == null) {
            contextIncludess = this.mContextBase.getContextIncludess();
        } else {
            contextIncludess = this.contextBp.getAllContextIncludesByContextBaseId(this.mContextBase.getId());
            this.mFirstTimeContextIncludesAccess = false;
        }
        if (contextIncludess != null) {
            this.mContextBase.setContextIncludess(contextIncludess);
            List<ContextBase> contextBases = this.contextBp.getAllContextsExcludingSpecificContext(this.mContextBase.getId());
            StringBuffer contextString = new StringBuffer();
            boolean firstRecord = true;
            for (ContextIncludes contextIncludes : contextIncludess) {
                if (!firstRecord) {
                    contextString.append(Globals.COMMA);
                }
                appendContextName(contextString, contextIncludes, contextBases);
                firstRecord = false;
            }
            this.mContextIncludesDescription.setText(contextString.toString());
        }
    }

    private void appendContextName(StringBuffer contextString, ContextIncludes contextIncludes, List<ContextBase> contextBases) {
        for (ContextBase contextBase : contextBases) {
            if (contextBase.getId().equals(contextIncludes.getIncludedContextId())) {
                contextString.append(contextBase.getName());
            }
        }
    }

    private void initViewValues() {
        this.mContextNameEditText.setText(this.mContextBase.getName());
    }

    private void initViewControls() {
        this.mDeleteButton = (Button) findViewById(R.id.delete_button);
        this.mSaveButton = (Button) findViewById(R.id.save_button);
        this.mCancelButton = (Button) findViewById(R.id.cancel_button);
        this.mContextNameEditText = (EditText) findViewById(R.id.context_name);
        DropboxSyncHelper.addDelayedIdleToEditText(this.mContextNameEditText);
        this.mContextIncludesDescription = (TextView) findViewById(R.id.context_includes_description);
        this.mContextIncludesDescriptionTableLayout = (TableLayout) findViewById(R.id.context_includes_description_table_layout);
        if (this.mIsNewContext) {
            this.mDeleteButton.setVisibility(View.GONE);
        }
    }

    private void initDeleteButton() {
        this.mDeleteButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new Builder(ContextEditActivity.this).setTitle("Delete context").setMessage("Context will be deleted?").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ContextEditActivity.this.contextBp.deleteContext(ContextEditActivity.this.mContextBase);
                        ContextEditActivity.this.contextBp.deleteAllContextIncludesByContextBaseId(ContextEditActivity.this.mContextBase.getId());
                        ContextEditActivity.this.contextBp.deleteAllConextTasksbyContextBaseId(ContextEditActivity.this.mContextBase.getId());
                        ContextEditActivity.this.finish();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
            }
        });
    }

    private void initSaveButton() {
        this.mSaveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ContextEditActivity.this.mContextBase.setName(ContextEditActivity.this.mContextNameEditText.getText().toString());
                switch (ContextEditActivity.this.contextBp.isValidContextName(ContextEditActivity.this.mContextBase)) {
                    case NO_ERROR:
                        ContextEditActivity.this.mContextBase.setName(ContextEditActivity.this.mContextBase.getName().trim());
                        ContextEditActivity.this.contextBp.saveOrUpdateContext(ContextEditActivity.this.mContextBase);
                        ContextEditActivity.this.performContextIncludesSave();
                        ContextEditActivity.this.finish();
                        return;
                    case BLANK:
                        AppUtil.showGenericWarningMessage(ContextEditActivity.this, "Name required", "Please provide context name");
                        return;
                    case DUPLICATE:
                        AppUtil.showGenericWarningMessage(ContextEditActivity.this, "Duplicate name", "Please enter another context name");
                        return;
                    default:
                        return;
                }
            }
        });
    }

    protected void performContextIncludesSave() {
        List<ContextIncludes> contextIncludess = this.mContextBase.getContextIncludess();
        if (contextIncludess != null) {
            this.contextBp.deleteAllContextIncludesByContextBaseId(this.mContextBase.getId());
            for (ContextIncludes contextIncludes : contextIncludess) {
                contextIncludes.setContextId(this.mContextBase.getId());
                this.contextBp.saveContextIncludes(contextIncludes);
            }
        }
    }

    private void initContextIncludesDescriptionTableLayout() {
        this.mContextIncludesDescriptionTableLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ArchitectureContext.setObject(ContextEditActivity.this.mContextBase);
                ContextEditActivity.this.startActivity(new Intent(ContextEditActivity.this, ContextIncludesListActivity.class));
            }
        });
    }

    private void initCancelButton() {
        this.mCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ContextEditActivity.this.finish();
            }
        });
    }
}
