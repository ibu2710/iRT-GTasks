package com.irt.tasks.ui.taskslist.extras;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.LevelColor;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.LevelColorBp;
import com.irt.tasks.app.businessprocess.LevelColorBpImpl;
import com.irt.tasks.app.businessprocess.TasksListBp;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.AbstractActivity;
import com.irt.tasks.ui.task.EditTaskHelper;
import java.util.List;

public class ListColorActivity extends AbstractActivity {
    private static final int FONT_COLOR_DIALOG = 3;
    private static final int FONT_STYLE_COLOR_DIALOG = 1;
    private static final int FONT_STYLE_COLOR_SELECTION_COLOR = 1;
    private static final int FONT_STYLE_COLOR_SELECTION_STYLE = 0;
    private static final int FONT_STYLE_DIALOG = 2;
    private int[] index = new int[]{0, 10, 1, 11, 2, 12, 3, 13, 4, 14, 5, 15, 6, 16, 7, 17, 8, 18, 9, 19};
    LevelColorBp levelColorBp = new LevelColorBpImpl();
    CheckBox mApplyListColorToTasksCheckbox;
    private Button mCancelButton;
    private ImageButton mCopyListColorImageButton;
    private SpannableStringBuilder[] mFontColors = new SpannableStringBuilder[52];
    private OnItemClickListener mLevelColorItemClickListener = new C19963();
    private SpannableStringBuilder[] mLevelColorLabels = new SpannableStringBuilder[20];
    private int mLevelColorPosition;
    private List<LevelColor> mLevelColors;
    private GridView mListColorGridView;
    private Button mSaveButton;
    private TasksList mTasksList;
    TasksListBp tasksListBp = new TasksListBpImpl();

    class C19941 implements OnClickListener {
        C19941() {
        }

        public void onClick(View v) {
            showActions();
        }

        private void showActions() {
            final List<TasksList> tasksLists = ListColorActivity.this.levelColorBp.getAllLevelColorTasksLists();
            SpannableStringBuilder[] tasksListNameSpan = new SpannableStringBuilder[tasksLists.size()];
            for (int i = 0; i < tasksListNameSpan.length; i++) {
                tasksListNameSpan[i] = new SpannableStringBuilder(((TasksList) tasksLists.get(i)).getName() + "\n- ");
                List<LevelColor> levelColors = ListColorActivity.this.levelColorBp.getAllByTasksListId(((TasksList) tasksLists.get(i)).getId());
                for (int j = 0; j < 20; j++) {
                    int level = j + 1;
                    if (level > 19) {
                        level -= 20;
                    } else if (level > 9) {
                        level -= 10;
                    }
                    SpannableStringBuilder levelSpan = new SpannableStringBuilder(level + "");
                    int fontColor = ((LevelColor) levelColors.get(j)).getFontColor().intValue();
                    if (fontColor != 0) {
                        levelSpan.setSpan(new ForegroundColorSpan(EditTaskHelper.getFontColor(fontColor, fontColor)), 0, levelSpan.length(), 0);
                    }
                    int fontStyle = ((LevelColor) levelColors.get(j)).getFontStyle().intValue();
                    if (fontStyle != 0) {
                        levelSpan.setSpan(new StyleSpan(EditTaskHelper.getFontStyle(fontStyle)), 0, levelSpan.length(), 0);
                    }
                    tasksListNameSpan[i].append(levelSpan);
                }
            }
            new Builder(ListColorActivity.this).setTitle("Copy color from").setItems(tasksListNameSpan, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichTasksList) {
                    ListColorActivity.this.mLevelColors = ListColorActivity.this.levelColorBp.copyLevelColorFromTasksLists(((TasksList) tasksLists.get(whichTasksList)).getId(), ListColorActivity.this.mTasksList.getId());
                    ListColorActivity.this.initLevelColorLabels();
                    ListColorActivity.this.mListColorGridView.invalidateViews();
                }
            }).show();
        }
    }

    class C19952 implements OnCheckedChangeListener {
        C19952() {
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            ListColorActivity.this.mTasksList.setColorTasks(Boolean.valueOf(isChecked));
        }
    }

    class C19963 implements OnItemClickListener {
        C19963() {
        }

        public void onItemClick(AdapterView a, View v, int position, long id) {
            if (id != -1) {
                ListColorActivity.this.mLevelColorPosition = position;
                ListColorActivity.this.showDialog(1);
            }
        }
    }

    class C19974 implements DialogInterface.OnClickListener {
        C19974() {
        }

        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case 0:
                    ListColorActivity.this.showDialog(2);
                    return;
                case 1:
                    ListColorActivity.this.showDialog(3);
                    return;
                default:
                    return;
            }
        }
    }

    class C19985 implements DialogInterface.OnClickListener {
        C19985() {
        }

        public void onClick(DialogInterface dialog, int whichFontStyle) {
            ((LevelColor) ListColorActivity.this.mLevelColors.get(ListColorActivity.this.index[ListColorActivity.this.mLevelColorPosition])).setFontStyle(Integer.valueOf(whichFontStyle));
            dialog.dismiss();
            ListColorActivity.this.initLevelColorLabels();
            ListColorActivity.this.mListColorGridView.invalidateViews();
        }
    }

    class C19996 implements DialogInterface.OnClickListener {
        C19996() {
        }

        public void onClick(DialogInterface dialog, int whichColor) {
            ((LevelColor) ListColorActivity.this.mLevelColors.get(ListColorActivity.this.index[ListColorActivity.this.mLevelColorPosition])).setFontColor(Integer.valueOf(whichColor));
            ListColorActivity.this.initLevelColorLabels();
            ListColorActivity.this.mListColorGridView.invalidateViews();
            dialog.dismiss();
        }
    }

    class C20007 implements OnClickListener {
        C20007() {
        }

        public void onClick(View v) {
            ListColorActivity.this.finish();
        }
    }

    class C20018 implements OnClickListener {
        C20018() {
        }

        public void onClick(View v) {
            ConfigManager.getInstance().setPreferenceAccessed(true);
            new SaveListColor(ListColorActivity.this.mLevelColors, ListColorActivity.this).execute(new TasksList[]{ListColorActivity.this.mTasksList});
        }
    }

    public class ListColorAdapter extends BaseAdapter {
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView levelTextView = initLevelTextView();
            levelTextView.setText(ListColorActivity.this.mLevelColorLabels[position]);
            return levelTextView;
        }

        private TextView initLevelTextView() {
            TextView levelTextView = new TextView(ListColorActivity.this);
            levelTextView.setTextSize(19.0f);
            levelTextView.setTextColor(getResources().getColor(android.R.color.black));
            levelTextView.setPadding(35, 0, 0, 0);
            return levelTextView;
        }

        public final int getCount() {
            return ListColorActivity.this.mLevelColorLabels.length;
        }

        public final Object getItem(int position) {
            return ListColorActivity.this.mLevelColorLabels[position];
        }

        public final long getItemId(int position) {
            return (long) position;
        }
    }

    private class SaveListColor extends AsyncTask<TasksList, Void, Boolean> {
        Context mContext;
        List<LevelColor> mLevelColors;
        private ProgressDialog progressDialog;

        public SaveListColor(List<LevelColor> levelColors, Context context) {
            this.mLevelColors = levelColors;
            this.mContext = context;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = ProgressDialog.show(this.mContext, null, "Please wait while saving colors...", true, true);
        }

        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            AppUtil.refreshDisplay();
            this.progressDialog.dismiss();
            Toast.makeText(this.mContext, "Saved colors.", Toast.LENGTH_LONG).show();
            ListColorActivity.this.finish();
        }

        protected Boolean doInBackground(TasksList... params) {
            new TasksListBpImpl().saveOrUpdate(params[0]);
            LevelColorBp levelColorBp = new LevelColorBpImpl();
            for (LevelColor levelColor : this.mLevelColors) {
                levelColorBp.saveOrUpdate(levelColor);
            }
            ConfigManager.getInstance().addLevelColorToMap(params[0].getId(), this.mLevelColors);
            return Boolean.valueOf(true);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_color);
        initLevelColors();
        initFontColor();
        initLevelColorLabels();
        initCopyListColor();
        initCancelButton();
        initSaveButton();
        initColorGridView();
        initApplyListColor();
    }

    private void initCopyListColor() {
        this.mCopyListColorImageButton = (ImageButton) findViewById(R.id.copy_list_color_from);
        this.mCopyListColorImageButton.setOnClickListener(new C19941());
    }

    private void initApplyListColor() {
        this.mApplyListColorToTasksCheckbox = (CheckBox) findViewById(R.id.apply_list_color_to_tasks);
        this.mApplyListColorToTasksCheckbox.setChecked(this.mTasksList.isColorTasks().booleanValue());
        this.mApplyListColorToTasksCheckbox.setOnCheckedChangeListener(new C19952());
    }

    private void initLevelColors() {
        long tasksListId = getIntent().getLongExtra(Constants.TASK_LIST, DatabaseHelper.ALL_TASKS_LIST_ID.longValue());
        this.mTasksList = this.tasksListBp.get(Long.valueOf(tasksListId));
        this.mLevelColors = this.levelColorBp.getAllByTasksListId(Long.valueOf(tasksListId));
        if (this.mLevelColors.size() < 1) {
            this.levelColorBp.createLevelColorsForTasksList(Long.valueOf(tasksListId));
            this.mLevelColors = this.levelColorBp.getAllByTasksListId(Long.valueOf(tasksListId));
        }
    }

    private void initColorGridView() {
        this.mListColorGridView = (GridView) findViewById(R.id.list_color_grid);
        this.mListColorGridView.setOnItemClickListener(this.mLevelColorItemClickListener);
        this.mListColorGridView.setAdapter(new ListColorAdapter());
    }

    private void initFontColor() {
        for (int i = 0; i < 52; i++) {
            this.mFontColors[i] = new SpannableStringBuilder();
            this.mFontColors[i].append(getFontText(i));
            this.mFontColors[i].setSpan(new ForegroundColorSpan(EditTaskHelper.getFontColor(i, Color.parseColor("#111111"))), 0, this.mFontColors[i].length(), 0);
            int fontStyle = ((LevelColor) this.mLevelColors.get(this.index[this.mLevelColorPosition])).getFontStyle().intValue();
            if (fontStyle != 0) {
                this.mFontColors[i].setSpan(new StyleSpan(EditTaskHelper.getFontStyle(fontStyle)), 0, this.mFontColors[i].length(), 0);
            }
        }
    }

    private String getFontText(int position) {
        if (position == 0) {
            return " Auto ";
        }
        return " Color " + position + " ";
    }

    protected void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case 2:
                removeDialog(2);
                return;
            case 3:
                initFontColor();
                removeDialog(3);
                return;
            default:
                return;
        }
    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new Builder(this).setTitle("Select font style/color").setItems(R.array.level_font_sytle_color, new C19974()).create();
            case 2:
                return new Builder(this).setTitle("Select font style").setSingleChoiceItems(R.array.font_sytle, ((LevelColor) this.mLevelColors.get(this.index[this.mLevelColorPosition])).getFontStyle().intValue(), new C19985()).create();
            case 3:
                return new Builder(this).setTitle("Select font color").setSingleChoiceItems(this.mFontColors, ((LevelColor) this.mLevelColors.get(this.index[this.mLevelColorPosition])).getFontColor().intValue(), new C19996()).create();
            default:
                return null;
        }
    }

    private void initLevelColorLabels() {
        for (int i = 0; i < 20; i++) {
            this.mLevelColorLabels[i] = new SpannableStringBuilder("Level " + (this.index[i] + 1));
            this.mLevelColorLabels[i].setSpan(new UnderlineSpan(), 0, this.mLevelColorLabels[i].length(), 0);
            if (((LevelColor) this.mLevelColors.get(this.index[i])).getFontColor().intValue() != 0) {
                this.mLevelColorLabels[i].setSpan(new ForegroundColorSpan(EditTaskHelper.getFontColor(((LevelColor) this.mLevelColors.get(this.index[i])).getFontColor().intValue(), -16777216)), 0, this.mLevelColorLabels[i].length(), 0);
            }
            int fontStyle = ((LevelColor) this.mLevelColors.get(this.index[i])).getFontStyle().intValue();
            if (fontStyle != 0) {
                this.mLevelColorLabels[i].setSpan(new StyleSpan(EditTaskHelper.getFontStyle(fontStyle)), 0, this.mLevelColorLabels[i].length(), 0);
            }
        }
    }

    private void initCancelButton() {
        this.mCancelButton = (Button) findViewById(R.id.cancel_button);
        this.mCancelButton.setOnClickListener(new C20007());
    }

    private void initSaveButton() {
        this.mSaveButton = (Button) findViewById(R.id.save_button);
        this.mSaveButton.setOnClickListener(new C20018());
    }
}
