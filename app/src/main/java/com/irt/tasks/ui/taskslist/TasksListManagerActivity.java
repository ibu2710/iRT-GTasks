package com.irt.tasks.ui.taskslist;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.AccountBp;
import com.irt.tasks.app.businessprocess.AccountBpImpl;
import com.irt.tasks.app.businessprocess.TasksListBp;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.AbstractListActivity;
import com.irt.tasks.ui.dragndrop.DragListener;
import com.irt.tasks.ui.dragndrop.DragNDropListView;
import com.irt.tasks.ui.dragndrop.DropListener;
import com.irt.tasks.ui.dragndrop.RemoveListener;
import com.irt.tasks.ui.help.HelpUtility;
import com.irt.tasks.ui.help.HelpUtility.HelpDialog;
import java.util.ArrayList;
import java.util.List;

public class TasksListManagerActivity extends AbstractListActivity {
    protected static int mFirstVisibleEvent = 0;
    AccountBp accountBp = new AccountBpImpl();
    ConfigManager config;
    private Button mAddTasksListButton;
    private DragListener mDragListener = new DragListener() {
        int backgroundColor = -535810032;
        int defaultBackgroundColor;

        public void onDrag(int x, int y, ListView listView, int startPostion) {
        }

        public void setDisplacedListItem(Object displacedItem, View itemView) {
            TasksListManagerAdapterHelper.getView((TasksList) displacedItem, itemView, null);
        }

        public void blankOutDraggedItemListItem(View itemView) {
            ((TextView) itemView.findViewById(R.id.TextView01)).setText("");
            ((TextView) itemView.findViewById(R.id.auto_sync)).setText("");
            ((TextView) itemView.findViewById(R.id.enabled)).setText("");
            ((TextView) itemView.findViewById(R.id.account_name)).setText("");
        }

        public void onStartDrag(View itemView, int position) {
            itemView.setVisibility(View.INVISIBLE);
            this.defaultBackgroundColor = itemView.getDrawingCacheBackgroundColor();
            ImageView iv = (ImageView) itemView.findViewById(R.id.ImageView01);
            if (iv != null) {
                iv.setVisibility(View.INVISIBLE);
            }
        }

        public void onStopDrag(View itemView) {
            if (itemView != null) {
                itemView.setVisibility(View.VISIBLE);
                itemView.setBackgroundColor(this.defaultBackgroundColor);
                ImageView iv = (ImageView) itemView.findViewById(R.id.ImageView01);
                if (iv != null) {
                    iv.setVisibility(View.VISIBLE);
                }
            }
            TasksListManagerActivity.this.mIsSaveSortedTasksLists = true;
        }

        public ListAdapter getMyListAdapter() {
            return TasksListManagerActivity.this.getListAdapter();
        }
    };
    private DropListener mDropListener = new C19778();
    ImageView mHelpImageView;
    boolean mIsSaveSortedTasksLists;
    private RemoveListener mRemoveListener = new C19789();
    private CheckBox mShowInvisibleAccounts;
    private Spinner mSortSpinner;
    List<TasksList> mTasksLists;
    TextView mTitle;
    TasksListBp tasksListBp = new TasksListBpImpl();

    class C19701 implements OnClickListener {
        C19701() {
        }

        public void onClick(View v) {
            HelpUtility.showHelp(TasksListManagerActivity.this, HelpDialog.TASKS_LIST_MANAGER);
        }
    }

    class C19712 implements OnClickListener {
        C19712() {
        }

        public void onClick(View v) {
            HelpUtility.showHelp(TasksListManagerActivity.this, HelpDialog.TASKS_LIST_MANAGER);
        }
    }

    class C19723 implements OnScrollListener {
        C19723() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            TasksListManagerActivity.mFirstVisibleEvent = firstVisibleItem;
        }
    }

    class C19734 implements OnCheckedChangeListener {
        C19734() {
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            TasksListManagerActivity.this.config.setShowInvisibleAccounts(Boolean.valueOf(isChecked));
            AppUtil.scheduleAllAlarms(TasksListManagerActivity.this.config.getContext());
            TasksListManagerActivity.this.refreshList();
        }
    }

    class C19745 implements OnClickListener {
        C19745() {
        }

        public void onClick(View v) {
            TasksListManagerActivity.this.saveSortedTasksList();
            TasksList newTasksList = new TasksList();
            newTasksList.setSortPosition(Integer.valueOf(TasksListManagerActivity.this.mTasksLists.size()));
            ArchitectureContext.setObject(newTasksList);
            TasksListManagerActivity.this.startActivity(new Intent(TasksListManagerActivity.this, TasksListEditActivity.class));
        }
    }

    class C19756 implements OnItemSelectedListener {
        C19756() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View arg1, int whichSort, long arg3) {
            TasksListManagerActivity.this.config.setTasksListSort(Integer.valueOf(whichSort));
            TasksListManagerActivity.this.setDragAndDropMode();
            TasksListManagerActivity.this.refreshList();
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    class C19767 implements OnItemClickListener {
        C19767() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            TasksListManagerActivity.this.saveSortedTasksList();
            ArchitectureContext.setObject(TasksListManagerActivity.this.mTasksLists.get(position));
            TasksListManagerActivity.this.startActivity(new Intent(TasksListManagerActivity.this, TasksListEditActivity.class));
        }
    }

    class C19778 implements DropListener {
        C19778() {
        }

        public void onDrop(int from, int to) {
            ListAdapter adapter = TasksListManagerActivity.this.getListAdapter();
            if (adapter instanceof TasksListManagerAdapter) {
                ((TasksListManagerAdapter) adapter).onDrop(from, to);
                TasksListManagerActivity.this.getListView().invalidateViews();
            }
        }
    }

    class C19789 implements RemoveListener {
        C19789() {
        }

        public void onRemove(int which) {
            ListAdapter adapter = TasksListManagerActivity.this.getListAdapter();
            if (adapter instanceof TasksListManagerAdapter) {
                ((TasksListManagerAdapter) adapter).onRemove(which);
                TasksListManagerActivity.this.getListView().invalidateViews();
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.tasks_list_dragndroplistview);
        this.config = ConfigManager.getInstance();
        this.mHelpImageView = (ImageView) findViewById(R.id.help_image);
        this.mHelpImageView.setOnClickListener(new C19701());
        initSortSpinner();
        setTasksListButton();
        setShowInvisibleAccounts();
        setWindowTitle();
    }

    private void setWindowTitle() {
        this.mTitle = (TextView) findViewById(R.id.title);
        this.mTitle.setText(getResources().getText(R.string.app_name_tasks_list));
        this.mTitle.setOnClickListener(new C19712());
    }

    private void setListScrollListeners() {
        getListView().setOnScrollListener(new C19723());
        registerForContextMenu(getListView());
    }

    private void setShowInvisibleAccounts() {
        this.mShowInvisibleAccounts = (CheckBox) findViewById(R.id.show_invisible_account_checkbox);
        this.mShowInvisibleAccounts.setChecked(this.config.isShowInvisibleAccounts().booleanValue());
        this.mShowInvisibleAccounts.setOnCheckedChangeListener(new C19734());
    }

    private void setTasksListButton() {
        this.mAddTasksListButton = (Button) findViewById(R.id.add_tasks_list_button);
        this.mAddTasksListButton.setOnClickListener(new C19745());
    }

    protected void onResume() {
        super.onResume();
        refreshList();
    }

    private void initSortSpinner() {
        String[] textArray = getResources().getStringArray(R.array.tasks_lists_sort_options);
        this.mSortSpinner = (Spinner) findViewById(R.id.tasks_list_sort_spinner);
        ArrayAdapter<String> sortAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, textArray);
        sortAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.mSortSpinner.setAdapter(sortAdapter);
        this.mSortSpinner.setSelection(this.config.getTasksListSort().intValue());
        this.mSortSpinner.setOnItemSelectedListener(new C19756());
    }

    private void setDragAndDropMode() {
        if (3 == this.config.getTasksListSort().intValue()) {
            ((DragNDropListView) getListView()).enableDragMode(true);
        } else {
            ((DragNDropListView) getListView()).enableDragMode(false);
        }
    }

    private void refreshList() {
        ArrayList<TasksList> tasksLists = initTasksLists();
        getListView().invalidate();
        setListeners(setListView(tasksLists));
        getListView().setSelection(mFirstVisibleEvent);
    }

    private ListView setListView(ArrayList<TasksList> tasksLists) {
        setListAdapter(new TasksListManagerAdapter(this, tasksLists));
        return getListView();
    }

    private ArrayList<TasksList> initTasksLists() {
        List<Account> accounts = this.accountBp.getAll();
        this.mTasksLists = this.tasksListBp.getAllTasksListBySortOrder();
        ArrayList<TasksList> tasksLists = new ArrayList(this.mTasksLists.size());
        for (int i = 0; i < this.mTasksLists.size(); i++) {
            TasksList tasksList = (TasksList) this.mTasksLists.get(i);
            tasksList.setAccount(getAccount(tasksList.getAccountId(), accounts));
            tasksLists.add(this.mTasksLists.get(i));
        }
        return tasksLists;
    }

    private Account getAccount(Long accountId, List<Account> accounts) {
        for (Account account : accounts) {
            if (accountId.equals(account.getId())) {
                return account;
            }
        }
        return null;
    }

    private void setListeners(ListView listView) {
        if ((listView instanceof DragNDropListView) && this.config.getTasksListSort().equals(Integer.valueOf(3))) {
            ((DragNDropListView) listView).setDropListener(this.mDropListener);
            ((DragNDropListView) listView).setRemoveListener(this.mRemoveListener);
            ((DragNDropListView) listView).setDragListener(this.mDragListener);
        }
        listView.setOnItemClickListener(new C19767());
        setListScrollListeners();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        try {
            saveSortedTasksList();
            finish();
            return super.onKeyDown(keyCode, event);
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return true;
        }
    }

    private void saveSortedTasksList() {
        if (this.mIsSaveSortedTasksLists) {
            for (TasksList tasksList : ((TasksListManagerAdapter) getListAdapter()).getSortedTasksLists()) {
                this.tasksListBp.saveOrUpdate(tasksList);
            }
            Toast.makeText(this, "Saved sorted lists", Toast.LENGTH_LONG).show();
            this.mIsSaveSortedTasksLists = false;
        }
    }
}
