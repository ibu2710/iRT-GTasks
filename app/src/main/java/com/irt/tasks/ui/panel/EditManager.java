package com.irt.tasks.ui.panel;

import android.graphics.Color;
import android.widget.ToggleButton;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.framework.business.ErrorTypes.ERROR;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.panel.EditActionDetail.EditActions;
import java.util.ArrayList;
import java.util.List;

public class EditManager {
    private static final String CANCEL_LABEL = "Clr";
    private static final int MAX_EDIT_ACTIONS_HISTORY = 70;
    private static final int REDUCE_EDIT_ACTIONS_HISTORY = 50;
    public static final int SELECTED_TASK_COLOR = Color.parseColor(Constants.COLOR_BLUISH_WEEK_SELECTOR);
    private static final String SELECT_LABEL = "Sel";
    private static EditManager instance;
    private ToggleButton copy;
    private ToggleButton cut;
    private ToggleButton delete;
    private EditActionDetail editActionDetail;
    private List<EditActionDetail> editActionsHistory = new ArrayList();
    private boolean isCancelEnabled;
    private boolean isCopyChecked = false;
    private boolean isCopyEnabled = false;
    private boolean isCutChecked = false;
    private boolean isCutEnabled = false;
    private boolean isDeleteChecked = false;
    private boolean isDeleteEnabled = false;
    private boolean isPasteChecked = false;
    private boolean isPasteEnabled = false;
    private boolean isSelectChecked = true;
    private boolean isSelectEnabled = true;
    private ToggleButton paste;
    private ToggleButton select;
    private List<Task> sortedTasks;
    private TaskBp taskBp = new TaskBpImpl();

    public enum SelectLabels {
        SELECT,
        CANCEL
    }

    public static EditManager getInstance(ToggleButton select, ToggleButton cut, ToggleButton copy, ToggleButton paste, ToggleButton delete) {
        if (instance == null) {
            instance = new EditManager();
        }
        validateToggleButtons(select, cut, copy, paste, delete);
        recoverState(select, cut, copy, paste, delete);
        return instance;
    }

    private static void validateToggleButtons(ToggleButton select, ToggleButton cut, ToggleButton copy, ToggleButton paste, ToggleButton delete) {
        if (select == null) {
            throw new RuntimeException("Select ToggleButton is null.");
        } else if (cut == null) {
            throw new RuntimeException("Cut ToggleButton is null.");
        } else if (copy == null) {
            throw new RuntimeException("Copy ToggleButton is null.");
        } else if (paste == null) {
            throw new RuntimeException("Paste ToggleButton is null.");
        } else if (delete == null) {
            throw new RuntimeException("Delete ToggleButton is null.");
        }
    }

    private static void recoverState(ToggleButton select, ToggleButton cut, ToggleButton copy, ToggleButton paste, ToggleButton delete) {
        instance.select = select;
        instance.cut = cut;
        instance.copy = copy;
        instance.paste = paste;
        instance.delete = delete;
        instance.select.setChecked(instance.isSelectChecked);
        instance.cut.setChecked(instance.isCutChecked);
        instance.copy.setChecked(instance.isCopyChecked);
        instance.paste.setChecked(instance.isPasteChecked);
        instance.delete.setChecked(instance.isDeleteChecked);
        instance.select.setEnabled(instance.isSelectEnabled);
        instance.cut.setEnabled(instance.isCutEnabled);
        instance.copy.setEnabled(instance.isCopyEnabled);
        instance.paste.setEnabled(instance.isPasteEnabled);
        instance.delete.setEnabled(instance.isDeleteEnabled);
        if (instance.isCancelEnabled) {
            instance.setSelectLabel(SelectLabels.CANCEL);
        }
    }

    public static EditManager getInstance() {
        if (instance == null) {
            instance = new EditManager();
        }
        return instance;
    }

    public void setSelectToggleButton(ToggleButton selectToggleButton) {
        this.select = selectToggleButton;
    }

    public void setSelectLabel(SelectLabels selectLabel) {
        switch (selectLabel) {
            case SELECT:
                this.select.setTextOn(SELECT_LABEL);
                this.select.setTextOff(SELECT_LABEL);
                this.isCancelEnabled = false;
                return;
            case CANCEL:
                this.select.setTextOn(CANCEL_LABEL);
                this.select.setTextOff(SELECT_LABEL);
                this.isCancelEnabled = true;
                return;
            default:
                return;
        }
    }

    public Task getDestinationTask() {
        if (this.editActionDetail != null) {
            return this.editActionDetail.getDestinationTask();
        }
        return null;
    }

    public void processSelectedTask(Task task, boolean isTaskExpanded) {
        if (this.editActionDetail == null) {
            this.editActionDetail = new EditActionDetail();
        }
        if (this.editActionDetail.isFirstActionComplete()) {
            if (!this.editActionDetail.conatinsTask(task.getId())) {
                this.editActionDetail.setDestinationTask(task, isTaskExpanded);
                prepareForPaste();
                processAction(EditActions.SELECT_DESTINATION);
            }
        } else if (!task.isTasksListAsTask()) {
            if (this.editActionDetail.conatinsTask(task.getId())) {
                this.editActionDetail.removeTask(task.getId());
            } else {
                this.editActionDetail.addSelectedTask(task);
            }
            if (this.editActionDetail.isEmpty()) {
                this.editActionDetail = null;
            }
            processAction(EditActions.SELECT_TASK);
        }
    }

    public boolean isTaskMarkedForEdit(Task task) {
        if (this.editActionDetail != null) {
            return this.editActionDetail.conatinsTask(task.getId());
        }
        return false;
    }

    public boolean isFirstEditAction(EditActions firstEditAction) {
        if (this.editActionDetail == null || firstEditAction != this.editActionDetail.getFirstEditAction()) {
            return false;
        }
        return true;
    }

    public boolean isTaskDestination(Task task) {
        if (this.editActionDetail == null || this.editActionDetail.getDestinationTask() == null) {
            return false;
        }
        return this.editActionDetail.getDestinationTask().getId().equals(task.getId());
    }

    public void undoAction() {
    }

    public void redoAction() {
    }

    public ERROR processAction(EditActions currentEditAction) {
        ERROR errorType = ERROR.NO_ERROR;
        switch (currentEditAction) {
            case CUT:
                this.editActionDetail.setFirstEditAction(EditActions.CUT);
                prepareForDestinationSelection();
                break;
            case COPY:
                this.editActionDetail.setFirstEditAction(EditActions.COPY);
                prepareForDestinationSelection();
                break;
            case SELECT_TASK:
                if (this.editActionDetail != null) {
                    if (this.editActionDetail.getFirstEditAction() == null) {
                        prepareForCutOrCopy();
                        break;
                    }
                }
                reset();
                break;
            case SELECT:
                if (this.isCancelEnabled) {
                    reset();
                    break;
                }
                break;
            case DELETE:
                this.editActionDetail.setFirstEditAction(EditActions.DELETE);
                this.editActionsHistory.add(this.editActionDetail);
                this.taskBp.multipleDeletes(this.editActionDetail, this.sortedTasks);
                reset();
                break;
            case PASTE:
                this.editActionDetail.setSecondEditAction(EditActions.PASTE);
                errorType = this.taskBp.multipleEditAndPaste(this.editActionDetail, this.sortedTasks);
                if (ERROR.NO_ERROR.equals(errorType)) {
                    this.editActionsHistory.add(this.editActionDetail);
                    switch (this.editActionDetail.getFirstEditAction()) {
                        case CUT:
                            reset();
                            break;
                        case COPY:
                            prepareForPaste();
                            EditActionDetail editActionDetailBuff = this.editActionDetail;
                            this.editActionDetail = new EditActionDetail();
                            this.editActionDetail.setFirstEditAction(editActionDetailBuff.getFirstEditAction());
                            this.editActionDetail.addSelectedTasks(editActionDetailBuff.getSelectedTasks());
                            this.editActionDetail.setSecondEditAction(editActionDetailBuff.getSecondEditAction());
                            this.editActionDetail.setDestinationTask(editActionDetailBuff.getDestinationTask(), editActionDetailBuff.isDestinationExpanded());
                            break;
                        default:
                            break;
                    }
                }
                break;
        }
        persistState();
        trimHistory();
        if (ConfigManager.getInstance().isDebug()) {
            printHistory();
        }
        return errorType;
    }

    private void persistState() {
        this.isSelectChecked = this.select.isChecked();
        this.isCutChecked = this.cut.isChecked();
        this.isCopyChecked = this.copy.isChecked();
        this.isPasteChecked = this.paste.isChecked();
        this.isDeleteChecked = this.delete.isChecked();
        this.isSelectEnabled = this.select.isEnabled();
        this.isCutEnabled = this.cut.isEnabled();
        this.isCopyEnabled = this.copy.isEnabled();
        this.isPasteEnabled = this.paste.isEnabled();
        this.isDeleteEnabled = this.delete.isEnabled();
    }

    private void prepareForPaste() {
        setSelectLabel(SelectLabels.CANCEL);
        this.select.setChecked(true);
        this.select.setEnabled(true);
        this.cut.setChecked(false);
        this.cut.setEnabled(false);
        this.copy.setChecked(false);
        this.copy.setEnabled(false);
        this.paste.setChecked(true);
        this.paste.setEnabled(true);
        this.delete.setChecked(false);
        this.delete.setEnabled(false);
    }

    private void prepareForDestinationSelection() {
        setSelectLabel(SelectLabels.CANCEL);
        this.select.setChecked(true);
        this.select.setEnabled(true);
        this.cut.setChecked(false);
        this.cut.setEnabled(false);
        this.copy.setChecked(false);
        this.copy.setEnabled(false);
        this.paste.setChecked(false);
        this.paste.setEnabled(false);
        this.delete.setChecked(false);
        this.delete.setEnabled(false);
    }

    private void prepareForCutOrCopy() {
        setSelectLabel(SelectLabels.CANCEL);
        this.select.setChecked(true);
        this.select.setEnabled(true);
        this.cut.setChecked(true);
        this.cut.setEnabled(true);
        this.copy.setChecked(true);
        this.copy.setEnabled(true);
        this.paste.setChecked(false);
        this.paste.setEnabled(false);
        this.delete.setChecked(true);
        this.delete.setEnabled(true);
    }

    public void reset() {
        this.editActionDetail = null;
        setSelectLabel(SelectLabels.SELECT);
        this.select.setChecked(true);
        this.select.setEnabled(true);
        this.cut.setEnabled(true);
        this.cut.setChecked(false);
        this.cut.setEnabled(false);
        this.copy.setEnabled(true);
        this.copy.setChecked(false);
        this.copy.setEnabled(false);
        this.paste.setEnabled(true);
        this.paste.setChecked(false);
        this.paste.setEnabled(false);
        this.delete.setEnabled(true);
        this.delete.setChecked(false);
        this.delete.setEnabled(false);
        this.isCancelEnabled = false;
    }

    public boolean isTaskNeedColor(Task task) {
        if (this.editActionDetail == null) {
            return false;
        }
        boolean isTaskNeedColor = this.editActionDetail.conatinsTask(task.getId());
        if (isTaskNeedColor || this.editActionDetail.getDestinationTask() == null) {
            return isTaskNeedColor;
        }
        if (task.isTasksListAsTask() && this.editActionDetail.getDestinationTask().isTasksListAsTask()) {
            return this.editActionDetail.getDestinationTask().getTasksListId().equals(task.getTasksListId());
        }
        return this.editActionDetail.getDestinationTask().getId().equals(task.getId());
    }

    public boolean isDestinationSelected() {
        if (this.editActionDetail == null || this.editActionDetail.getDestinationTask() == null) {
            return false;
        }
        return true;
    }

    public void setSortedTasks(List<Task> sortedTasks) {
        this.sortedTasks = sortedTasks;
    }

    public int getTaskColor(Task task) {
        if (isTaskNeedColor(task)) {
            if (!this.editActionDetail.conatinsTask(task.getId())) {
                return -16711936;
            }
            if (this.editActionDetail.getFirstEditAction() == null) {
                return SELECTED_TASK_COLOR;
            }
            switch (this.editActionDetail.getFirstEditAction()) {
                case CUT:
                    return Constants.EDIT_COLOR_CUT;
                case COPY:
                    return Constants.EDIT_COLOR_COPY;
            }
        }
        return 0;
    }

    public void trimHistory() {
        if (this.editActionsHistory.size() > 70) {
            List<EditActionDetail> newEditActionsHistory = new ArrayList();
            for (int i = this.editActionsHistory.size() - 50; i < this.editActionsHistory.size(); i++) {
                newEditActionsHistory.add(this.editActionsHistory.get(i));
            }
            this.editActionsHistory = newEditActionsHistory;
        }
    }

    private void printHistory() {
        if (this.editActionsHistory.size() > 0) {
            System.out.println("*****************************");
            for (EditActionDetail editActionDetail : this.editActionsHistory) {
                System.out.println(editActionDetail.toString() + Globals.FORWARDSLASH + this.editActionsHistory.size());
            }
        }
    }
}
