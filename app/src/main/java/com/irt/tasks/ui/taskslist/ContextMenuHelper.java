package com.irt.tasks.ui.taskslist;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.SpannableStringBuilder;
import android.view.MenuItem;
import android.widget.Toast;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.AccountBpImpl;
import com.irt.tasks.app.businessprocess.TasksListBp;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import java.util.List;

public class ContextMenuHelper {
    private static int mWhichAccount;
    private static int mWhichTasksList = 0;

    static class C19354 implements OnClickListener {
        C19354() {
        }

        public void onClick(DialogInterface dialog, int whichTasksList) {
            ContextMenuHelper.mWhichTasksList = whichTasksList;
        }
    }

    static class C19376 implements OnClickListener {
        C19376() {
        }

        public void onClick(DialogInterface dialog, int whichAccount) {
            ContextMenuHelper.mWhichAccount = whichAccount;
        }
    }

    private static class CopyList extends AsyncTask<Void, Void, Boolean> {
        Context mContext;
        Long mTargetAccountId;
        Long mTasksListId;
        private ProgressDialog progressDialog;
        TasksListBp tasksListBp = new TasksListBpImpl();

        public CopyList(Long tasksListId, Long targetAccountId, Context context) {
            this.mTasksListId = tasksListId;
            this.mTargetAccountId = targetAccountId;
            this.mContext = context;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = ProgressDialog.show(this.mContext, null, "Please wait while copying list...", true, true);
        }

        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            AppUtil.refreshDisplay();
            this.progressDialog.dismiss();
            Toast.makeText(this.mContext, "Completed copying list", Toast.LENGTH_LONG).show();
        }

        protected Boolean doInBackground(Void... params) {
            this.tasksListBp.copyList(this.mTasksListId, this.mTargetAccountId);
            return Boolean.valueOf(true);
        }
    }

    private static class CopyListAsBranch extends AsyncTask<Long, Void, Boolean> {
        Context mContext;
        TasksList mTasksList;
        private ProgressDialog progressDialog;

        public CopyListAsBranch(TasksList tasksList, Context context) {
            this.mTasksList = tasksList;
            this.mContext = context;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = ProgressDialog.show(this.mContext, null, "Please wait while copying list...", true, true);
        }

        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            AppUtil.refreshDisplay();
            this.progressDialog.dismiss();
            Toast.makeText(this.mContext, "Copied to [" + this.mTasksList.getName() + "] list", Toast.LENGTH_LONG).show();
        }

        protected Boolean doInBackground(Long... params) {
            new TasksListBpImpl().copyListAsBranch(params[0], this.mTasksList);
            return Boolean.valueOf(true);
        }
    }

    private static class DeleteList extends AsyncTask<Void, Void, Boolean> {
        Context mContext;
        Long mTasksListId;
        private ProgressDialog progressDialog;
        TasksListBp tasksListBp = new TasksListBpImpl();

        public DeleteList(Long tasksListId, Context context) {
            this.mTasksListId = tasksListId;
            this.mContext = context;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = ProgressDialog.show(this.mContext, null, "Please wait while deleting tasks list...", true, true);
        }

        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            AppUtil.refreshDisplay();
            this.progressDialog.dismiss();
            Toast.makeText(this.mContext, "Completed deleting tasks list", Toast.LENGTH_LONG).show();
        }

        protected Boolean doInBackground(Void... params) {
            this.tasksListBp.deleteTasksList(this.mTasksListId, false);
            return Boolean.valueOf(true);
        }
    }

    private static class EmptyTasksListAndSyncWithGoogle extends AsyncTask<Void, Void, Boolean> {
        Context mContext;
        TasksList mTasksList;
        private ProgressDialog progressDialog;
        TasksListBp tasksListBp = new TasksListBpImpl();

        public EmptyTasksListAndSyncWithGoogle(TasksList tasksList, Context context) {
            this.mTasksList = tasksList;
            this.mContext = context;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = ProgressDialog.show(this.mContext, null, "Please wait while deleting local and Google tasks...", true, true);
        }

        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            AppUtil.refreshDisplay();
            this.progressDialog.dismiss();
            Toast.makeText(this.mContext, "Completed deleting tasks in list", Toast.LENGTH_LONG).show();
        }

        protected Boolean doInBackground(Void... params) {
            this.tasksListBp.emptyTasksListAndSyncWithGoogle(this.mTasksList);
            return Boolean.valueOf(true);
        }
    }

    public static boolean onContextItemSelected(MenuItem item, final Context context, String tasksListName, final Long tasksListId) {
        switch (item.getItemId()) {
            case R.id.context_menu_edit_list:
                TasksList tasksList = new TasksListBpImpl().get(tasksListId);
                tasksList.setAccount(new AccountBpImpl().get(tasksList.getAccountId()));
                ArchitectureContext.setObject(tasksList);
                ((Activity) context).startActivityForResult(new Intent(context, TasksListEditActivity.class), 2);
                return true;
            case R.id.context_menu_delete_list:
                new Builder(context).setTitle(R.string.delete_title).setMessage(getDeleteMessageId(tasksListId)).setPositiveButton(R.string.alert_dialog_ok, new OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichTaksList) {
                        dialog.dismiss();
                        new DeleteList(tasksListId, context).execute(new Void[0]);
                        AppUtil.refreshDisplay();
                    }
                }).setNegativeButton(R.string.alert_dialog_cancel, null).show();
                return true;
            case R.id.context_menu_clear_local_and_google_tasks:
                new Builder(context).setTitle("Clear local/Google tasks").setMessage(getClearLocalAndGoogleTasksMessage(tasksListId)).setPositiveButton(R.string.alert_dialog_ok, new OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichTaksList) {
                        TasksList tasksList = new TasksListBpImpl().get(tasksListId);
                        dialog.dismiss();
                        new EmptyTasksListAndSyncWithGoogle(tasksList, context).execute(new Void[0]);
                    }
                }).setNegativeButton(R.string.alert_dialog_cancel, null).show();
                return true;
            case R.id.context_menu_add_list:
                ArchitectureContext.setObject(new TasksList());
                context.startActivity(new Intent(context, TasksListEditActivity.class));
                return true;
            case R.id.context_menu_copy_list:
                ((AbstractMainListActivity) context).showCopyList();
                return true;
            case R.id.context_menu_copy_list_as_branch:
                ((AbstractMainListActivity) context).showCopyListAsBranch();
                return true;
            default:
                return true;
        }
    }

    public static void showCopyListAsBranch(final Context context, String[] tasksListNames, final Long sourceTasksListId, final List<TasksList> tasksLists) {
        mWhichTasksList = 0;
        new Builder(context).setTitle("Copy list as branch to").setSingleChoiceItems(tasksListNames, 0, new C19354()).setPositiveButton("Copy", new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                new CopyListAsBranch((TasksList) tasksLists.get(ContextMenuHelper.mWhichTasksList), context).execute(new Long[]{sourceTasksListId});
                AppUtil.refreshDisplay();
                dialog.dismiss();
            }
        }).setNegativeButton(R.string.alert_dialog_cancel, null).show();
    }

    private static String getDeleteMessageId(Long tasksListId) {
        TasksList tasksList = new TasksListBpImpl().get(tasksListId);
        if (AppUtil.isOfflineAccount(tasksList.getAccountId())) {
            return "Delete Offline TASK LIST [" + tasksList.getName() + "] ?";
        }
        return "This Task List [" + tasksList.getName() + "] will be deleted on the phone. " + "Next sync will delete it on Google Tasks. If you " + "delete this tasks list and sync has not yet occurred and " + "you change your mind, modify any task on Google  Web to " + "restore it back on next sync.";
    }

    private static String getClearLocalAndGoogleTasksMessage(Long tasksListId) {
        return "Task List [" + new TasksListBpImpl().get(tasksListId).getName() + "] is the default Google Task List. Default Google Task List cannot be deleted, " + "it can only be cleared of tasks. This will clear the local tasks then clear Google tasks.";
    }

    public static void showCopyList(final Long tasksListId, final List<Account> accounts, SpannableStringBuilder[] accountEmails, final Context context) {
        mWhichAccount = 0;
        new Builder(context).setTitle("Copy list to").setSingleChoiceItems(accountEmails, 0, new C19376()).setPositiveButton("Copy", new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                new CopyList(tasksListId, ((Account) accounts.get(ContextMenuHelper.mWhichAccount)).getId(), context).execute(new Void[0]);
                AppUtil.refreshDisplay();
                dialog.dismiss();
            }
        }).setNegativeButton(R.string.alert_dialog_cancel, null).show();
    }
}
