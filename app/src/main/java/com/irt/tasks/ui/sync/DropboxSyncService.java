package com.irt.tasks.ui.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.irt.tasks.app.framework.config.ConfigManager;

public class DropboxSyncService extends Service {
    private static DropboxSyncAdapter sSyncAdapter = null;
    private static final Object sSyncAdapterLock = new Object();

    public void onCreate() {
        ConfigManager config = ConfigManager.getInstance();
        synchronized (sSyncAdapterLock) {
            if (sSyncAdapter == null) {
                sSyncAdapter = new DropboxSyncAdapter(config.getContext(), true);
            }
        }
    }

    public IBinder onBind(Intent intent) {
        return sSyncAdapter.getSyncAdapterBinder();
    }
}
