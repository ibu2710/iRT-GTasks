package com.irt.tasks.ui.help;

public class TasksViewHelpContext {
    public static void setTasksViewHelpContext(HelpContext helpContext) {
        helpContext.title = "Tasks Views";
        helpContext.content.append("<b>Description:</b>");
        helpContext.content.append("<br>Tasks Views are views that display the tasks in particular sort order or attributes.");
        helpContext.content.append("<br><br><b>Navigation:</b>");
        helpContext.content.append(HelpUtility.UL_STYLE);
        helpContext.content.append("<li><b>Show/hide CHECKBOXES</b> by doing <b>Left Double tap</b> on task.");
        helpContext.content.append("<li><b>Edit task</b> by doing  <b>Right Single tap</b> on task.");
        helpContext.content.append("<li><b>Go to Tasks Tree view</b> by doing <b>Right Double tap</b> on task.");
        helpContext.content.append("</ul>");
        helpContext.content.append("<b>Notes:</b>");
        helpContext.content.append(HelpUtility.UL_STYLE);
        helpContext.content.append("<li>For Deleted and Recycle bin views, you can only <b>restore</b> them and not edit by doing <b>Right Double tap</b> or <b>Right Single tap</b> on the task.");
        helpContext.content.append("<li>Deleted <b>Tasks Lists</b> don't show in the Deleted or Recycle bin views.");
        helpContext.content.append("<li>To clear the Recycle bin, Menu -> More -> Clear recycle bin");
        helpContext.content.append("<li>A task when deleted goes to either Deleted view or Recycle bin view. A deleted task goes to Deleted view if it needs to sync with Google Tasks. If the task does not need syncing, it will go to the Recycle bin.");
        helpContext.content.append("<li>Tasks highlighted (selected) on Tasks Tree view will be automatically selected on the views. To turn this off, go to Menu -> Preferences -> Navigation Preferences -> Auto select on view (uncheck)");
        helpContext.content.append("</ul>");
        helpContext.content.append("<b>Search:</b>");
        helpContext.content.append("<br>The phone's <b>Search button</b> will search the tasks tree of the view.<br><br>");
    }
}
