package com.irt.tasks.ui.dragndrop;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.irt.tasks.R;
import com.irt.tasks.ui.AbstractListActivity;
import java.util.ArrayList;

public class DragNDropListActivity extends AbstractListActivity {
    private static String[] mListContent = new String[]{"Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7", "Item 8", "Item 9", "Item 10", "Item 11", "Item 12", "Item 13", "Item 14", "Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7", "Item 8", "Item 9", "Item 10", "Item 11", "Item 12", "Item 13", "Item 14"};

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dragndroplistview);
        ArrayList<String> content = new ArrayList(mListContent.length);
        for (String add : mListContent) {
            content.add(add);
        }
        setListAdapter(new DragNDropAdapter(this, new int[]{R.layout.dragitem}, new int[]{R.id.TextView01}, content));
        ListView listView = getListView();
        if (listView instanceof DragNDropListView) {
            ((DragNDropListView) listView).enableDragMode(true);
            ((DragNDropListView) listView).setDropListener(new DropListener() {
                @Override
                public void onDrop(int from, int to) {
                    ListAdapter adapter = DragNDropListActivity.this.getListAdapter();
                    if (adapter instanceof DragNDropAdapter) {
                        ((DragNDropAdapter) adapter).onDrop(from, to);
                        DragNDropListActivity.this.getListView().invalidateViews();
                    }
                }
            });
            ((DragNDropListView) listView).setRemoveListener(new RemoveListener() {
                @Override
                public void onRemove(int which) {
                    ListAdapter adapter = DragNDropListActivity.this.getListAdapter();
                    if (adapter instanceof DragNDropAdapter) {
                        ((DragNDropAdapter) adapter).onRemove(which);
                        DragNDropListActivity.this.getListView().invalidateViews();
                    }
                }
            });
            ((DragNDropListView) listView).setDragListener(new DragListener() {
                int backgroundColor = -535810032;
                int defaultBackgroundColor;
                @Override
                public void blankOutDraggedItemListItem(View itemView) {
                    ((TextView) itemView.findViewById(R.id.TextView01)).setText("");
                }

                @Override
                public ListAdapter getMyListAdapter() {
                    return DragNDropListActivity.this.getListAdapter();
                }

                @Override
                public void onDrag(int i, int i2, ListView listView, int i3) {

                }

                @Override
                public void onStartDrag(View itemView, int i) {
                    itemView.setVisibility(View.INVISIBLE);
                    this.defaultBackgroundColor = itemView.getDrawingCacheBackgroundColor();
                    ImageView iv = (ImageView) itemView.findViewById(R.id.ImageView01);
                    if (iv != null) {
                        iv.setVisibility(View.INVISIBLE);
                    }
                }

                @Override
                public void onStopDrag(View itemView) {
                    if (itemView != null) {
                        itemView.setVisibility(View.VISIBLE);
                        itemView.setBackgroundColor(this.defaultBackgroundColor);
                        ImageView iv = (ImageView) itemView.findViewById(R.id.ImageView01);
                        if (iv != null) {
                            iv.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void setDisplacedListItem(Object displacedItem, View itemView) {
                    TextView tv = (TextView) itemView.findViewById(R.id.TextView01);
                    tv.setText((String) displacedItem);
                    tv.setVisibility(View.VISIBLE);
                    ((ImageView) itemView.findViewById(R.id.ImageView01)).setVisibility(View.VISIBLE);
                }
            });
        }
    }
}
