package com.irt.tasks.ui.datetimepicker;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.irt.tasks.R;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.AbstractActivity;

import java.util.Calendar;
import java.util.Date;

public class DatePickerActivity extends AbstractActivity {
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private static final int WEEK_MAX_POSITION = 6;
    Button cancelButton;
    ConfigManager config;
    private GestureDetector gestureDetector;
    boolean isOverrideGotoDayAlways = true;
    private SpannableStringBuilder[] mCalDays = new SpannableStringBuilder[this.numberOfCalDays];
    GridView mDaysGridView;
    private Calendar mFirstSelectedDate = Calendar.getInstance();
    private SpannableStringBuilder[] mMonths = new SpannableStringBuilder[this.numberOfMonths];
    GridView mMonthsGridView;
    private Calendar mSelectedDateCal = Calendar.getInstance();
    private Calendar mTodayCal = AppUtil.getTodayDateOnlyCal();
    Button nextButton;
    Button noneButton;
    private int numberOfCalDays = 49;
    private int numberOfMonths = 12;
    Button previousButton;
    Button todayButton;
    TextView yearView;

    public class DaysAdapter extends BaseAdapter {
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView dayTextView;
            if (convertView == null) {
                dayTextView = new TextView(DatePickerActivity.this);
                dayTextView.setTypeface(Typeface.DEFAULT_BOLD);
                dayTextView.setTextSize(20.0f);
                dayTextView.setGravity(17);
            } else {
                dayTextView = (TextView) convertView;
            }
            if (position <= 6) {
                dayTextView.setText(DatePickerActivity.this.mCalDays[AppUtil.getAdjustedPosition(position)]);
                dayTextView.setBackgroundColor(Color.parseColor(Constants.COLOR_BLUISH_WEEK_HEADER));
            } else {
                dayTextView.setText(DatePickerActivity.this.mCalDays[position]);
            }
            return dayTextView;
        }

        public final int getCount() {
            return DatePickerActivity.this.mCalDays.length;
        }

        public final Object getItem(int position) {
            return DatePickerActivity.this.mCalDays[position];
        }

        public final long getItemId(int position) {
            return (long) position;
        }
    }

    public class MonthsAdapter extends BaseAdapter {
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView dayTextView;
            if (convertView == null) {
                dayTextView = new TextView(DatePickerActivity.this);
                dayTextView.setTypeface(Typeface.DEFAULT_BOLD);
                dayTextView.setTextSize(19.0f);
                dayTextView.setTextColor(getResources().getColor(android.R.color.black));
                dayTextView.setGravity(17);
            } else {
                dayTextView = (TextView) convertView;
            }
            dayTextView.setText(DatePickerActivity.this.mMonths[position]);
            return dayTextView;
        }

        public final int getCount() {
            return DatePickerActivity.this.mMonths.length;
        }

        public final Object getItem(int position) {
            return DatePickerActivity.this.mMonths[position];
        }

        public final long getItemId(int position) {
            return (long) position;
        }
    }

    class MyGestureDetector extends SimpleOnGestureListener {
        MyGestureDetector() {
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) <= 250.0f) {
                    if (e1.getX() - e2.getX() > BitmapDescriptorFactory.HUE_GREEN && Math.abs(velocityX) > 200.0f) {
                        DatePickerActivity.this.mSelectedDateCal.add(Calendar.MONTH, 1);
                        DatePickerActivity.this.displayYear();
                    } else if (e2.getX() - e1.getX() > BitmapDescriptorFactory.HUE_GREEN && Math.abs(velocityX) > 200.0f) {
                        DatePickerActivity.this.mSelectedDateCal.add(Calendar.MONTH, -1);
                        DatePickerActivity.this.displayYear();
                    }
                }
            } catch (Exception e) {
            }
            return false;
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.date_picker);
        this.config = ConfigManager.getInstance();
        LinearLayout layout = (LinearLayout) findViewById(R.id.linear_layout);
        initializeSelectedDate();
        initializeViews();
        initializeButtonListeners();
        displayYear();
        initializeWeekEndsAndDaysHeader();
        this.gestureDetector = new GestureDetector(new MyGestureDetector());
        this.mMonthsGridView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return processGesture(event);
            }

            private boolean processGesture(MotionEvent event) {
                if (DatePickerActivity.this.gestureDetector.onTouchEvent(event)) {
                    return true;
                }
                return false;
            }
        });
        this.mDaysGridView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return processGesture(event);
            }

            private boolean processGesture(MotionEvent event) {
                if (DatePickerActivity.this.gestureDetector.onTouchEvent(event)) {
                    return true;
                }
                return false;
            }
        });
    }

    private void displayMonths() {
        formatMonth(0, "Jan");
        formatMonth(1, "Feb");
        formatMonth(2, "Mar");
        formatMonth(3, "Apr");
        formatMonth(4, "May");
        formatMonth(5, "Jun");
        formatMonth(6, "Jul");
        formatMonth(7, "Aug");
        formatMonth(8, "Sep");
        formatMonth(9, "Oct");
        formatMonth(10, "Nov");
        formatMonth(11, "Dec");
    }

    private void formatMonth(int index, String month) {
        this.mMonths[index] = new SpannableStringBuilder(month);
        if (index == this.mSelectedDateCal.get(Calendar.MONTH)) {
            this.mMonths[index].setSpan(new BackgroundColorSpan(Color.parseColor("#FFFF7D")), 0, this.mMonths[index].length(), 0);
        }
        if (this.mTodayCal.get(Calendar.YEAR) == this.mSelectedDateCal.get(Calendar.YEAR) && index == this.mTodayCal.get(Calendar.MONTH)) {
            this.mMonths[index].setSpan(new ForegroundColorSpan(getResources().getColor(android.R.color.black)), 0, this.mMonths[index].length(), 0);
        }
        this.mMonths[index].setSpan(new UnderlineSpan(), 0, this.mMonths[index].length(), 0);
        this.mMonthsGridView.setAdapter(new MonthsAdapter());
    }

    private void initializeButtonListeners() {
        this.previousButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                displayPreviousYear();
            }

            private void displayPreviousYear() {
                DatePickerActivity.this.mSelectedDateCal.add(Calendar.YEAR, -1);
                DatePickerActivity.this.displayYear();
            }
        });
        this.nextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                displayNextYear();
            }

            private void displayNextYear() {
                DatePickerActivity.this.mSelectedDateCal.add(Calendar.YEAR, 1);
                DatePickerActivity.this.displayYear();
            }
        });
        this.cancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerActivity.this.finish();
            }
        });
        this.todayButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent();
                data.putExtra(Constants.SELECTED_DATE_LONG_KEY, DatePickerActivity.this.mTodayCal.getTimeInMillis());
                DatePickerActivity.this.setResult(-1, data);
                DatePickerActivity.this.finish();
            }
        });
        this.noneButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent();
                data.putExtra(Constants.SELECTED_DATE_LONG_KEY, 0);
                DatePickerActivity.this.setResult(-1, data);
                DatePickerActivity.this.finish();
            }
        });
    }

    private void initializeSelectedDate() {
        Long selectedDateLong = (Long) getIntent().getExtras().get(Constants.SELECTED_DATE_LONG_KEY);
        Date selectedDate = AppUtil.getTodayDateOnly();
        if (selectedDateLong != null) {
            selectedDate = new Date(selectedDateLong.longValue());
        }
        this.mSelectedDateCal.setTime(selectedDate);
        this.mFirstSelectedDate.setTime(selectedDate);
    }

    private void displayYear() {
        this.yearView.setText(this.mSelectedDateCal.get(Calendar.YEAR) + "");
        displayMonths();
        initializeCalendarDays();
    }

    private void initializeViews() {
        this.yearView = (TextView) findViewById(R.id.yearView);
        this.mDaysGridView = (GridView) findViewById(R.id.daysGridView);
        this.mDaysGridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (id != -1 && position > 6) {
                    int day = new Integer(((SpannableStringBuilder) DatePickerActivity.this.mDaysGridView.getAdapter().getItem(position)) + "").intValue();
                    adjustMonth(DatePickerActivity.this.mDaysGridView.getAdapter().getItemId(position), day);
                    DatePickerActivity.this.mSelectedDateCal.set(Calendar.DAY_OF_MONTH, day);
                    Intent intent = new Intent();
                    intent.putExtra(Constants.SELECTED_DATE_LONG_KEY, DatePickerActivity.this.mSelectedDateCal.getTimeInMillis());
                    DatePickerActivity.this.setResult(-1, intent);
                    DatePickerActivity.this.finish();
                }
            }

            private void adjustMonth(long position, int day) {
                if (day > 20 && position < 15) {
                    DatePickerActivity.this.mSelectedDateCal.add(Calendar.MONTH, -1);
                } else if (day < 15 && position > 27) {
                    DatePickerActivity.this.mSelectedDateCal.add(Calendar.MONTH, 1);
                }
            }
        });
        this.mDaysGridView.setAdapter(new DaysAdapter());
        this.mMonthsGridView = (GridView) findViewById(R.id.monthsGridView);
        this.mMonthsGridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (id != -1) {
                    int monthInt = new Integer(DatePickerActivity.this.mMonthsGridView.getAdapter().getItemId(position) + "").intValue();
                    adjustDayForNewMonth(monthInt);
                    DatePickerActivity.this.mSelectedDateCal.set(Calendar.MONTH, monthInt);
                    DatePickerActivity.this.displayYear();
                }
            }

            private void adjustDayForNewMonth(int month) {
                int previouslySelectedDay = DatePickerActivity.this.mSelectedDateCal.get(Calendar.DAY_OF_MONTH);
                Calendar calBuff = Calendar.getInstance();
                calBuff.setTime(DatePickerActivity.this.mSelectedDateCal.getTime());
                calBuff.set(Calendar.DAY_OF_MONTH, 1);
                calBuff.set(Calendar.DAY_OF_MONTH, month);
                int lastDayOfNewMonth = calBuff.getActualMaximum(Calendar.DAY_OF_MONTH);
                if (previouslySelectedDay > lastDayOfNewMonth) {
                    DatePickerActivity.this.mSelectedDateCal.set(Calendar.DAY_OF_MONTH, lastDayOfNewMonth);
                }
            }
        });
        this.mMonthsGridView.setAdapter(new MonthsAdapter());
        this.previousButton = (Button) findViewById(R.id.previousButton);
        this.nextButton = (Button) findViewById(R.id.nextButton);
        this.cancelButton = (Button) findViewById(R.id.cancelButton);
        this.todayButton = (Button) findViewById(R.id.todayButton);
        this.noneButton = (Button) findViewById(R.id.noneButton);
    }

    private void initializeCalendarDays() {
        int selectedDateMonth = this.mSelectedDateCal.get(Calendar.MONTH);
        Calendar tempCal = AppUtil.getFirstDayOfMonthCalendar(this.mSelectedDateCal.getTime());
        for (int i = 7; i < this.numberOfCalDays; i++) {
            this.mCalDays[i] = new SpannableStringBuilder(tempCal.get(Calendar.MONTH) + "");
            if (tempCal.equals(this.mFirstSelectedDate)) {
                this.mCalDays[i].setSpan(new BackgroundColorSpan(Color.parseColor("#FFFF7D")), 0, this.mCalDays[i].length(), 0);
            }
            if (tempCal.equals(this.mTodayCal)) {
                this.mCalDays[i].setSpan(new ForegroundColorSpan(getResources().getColor(android.R.color.black)), 0, this.mCalDays[i].length(), 0);
            } else if (selectedDateMonth == tempCal.get(Calendar.MONTH)) {
                this.mCalDays[i].setSpan(new ForegroundColorSpan(Color.parseColor("#2952A3")), 0, this.mCalDays[i].length(), 0);
            } else {
                this.mCalDays[i].setSpan(new ForegroundColorSpan(Color.parseColor("#DFDFDF")), 0, this.mCalDays[i].length(), 0);
            }
            this.mCalDays[i].setSpan(new UnderlineSpan(), 0, this.mCalDays[i].length(), 0);
            tempCal.add(Calendar.DAY_OF_YEAR, 1);
        }
        this.mDaysGridView.setAdapter(new DaysAdapter());
    }

    private void initializeWeekEndsAndDaysHeader() {
        setWeekEndAndFontFormat(0, "Su");
        setWeekDayAndFontFormat(1, "Mo");
        setWeekDayAndFontFormat(2, "Tu");
        setWeekDayAndFontFormat(3, "We");
        setWeekDayAndFontFormat(4, "Th");
        setWeekDayAndFontFormat(5, "Fr");
        setWeekEndAndFontFormat(6, "Sa");
    }

    private void setWeekDayAndFontFormat(int index, String day) {
        setWeekEndAndFontFormat(index, day, -16777216);
    }

    private void setWeekEndAndFontFormat(int index, String day) {
        setWeekEndAndFontFormat(index, day, Color.parseColor("#000099"));
    }

    private void setWeekEndAndFontFormat(int index, String day, int color) {
        this.mCalDays[index] = new SpannableStringBuilder(day);
        this.mCalDays[index].setSpan(new ForegroundColorSpan(color), 0, this.mCalDays[0].length(), 0);
    }
}
