package com.irt.tasks.ui.taskslist;

import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.ui.preferences.ArraysConstants;
import java.util.Date;

public class TasksListViewHelper {
    public static CharSequence formatExtraInfoTop(int viewType, Task task) {
        Date date;
        switch (viewType) {
            case 1:
            case 14:
                date = new Date(task.getSnoozeDateTime().longValue());
                return AppUtil.formatDateByPreference(date) + " " + AppUtil.formatTimeByPreference(date);
            case 2:
            case 8:
            case 15:
            case 16:
            case 20:
                return getFormattedLastContentModifiedDate(task);
            case 3:
            case 9:
                return getFormattedLastContentModifiedDate(task);
            case 4:
            case 10:
            case 19:
                date = new Date(task.getCreationDate().longValue());
                return AppUtil.formatDateByPreference(date) + " " + AppUtil.formatTimeByPreference(date);
            case 5:
            case 11:
            case 17:
                return AppUtil.getDueDateDescription(task, "", false);
            case 6:
            case 12:
                return getFormattedLastContentModifiedDate(task);
            case 7:
            case 13:
            case ArraysConstants.VIEW_MODIFIED /*18*/:
                date = new Date(task.getLastContentModifiedDate().longValue());
                return AppUtil.formatDateByPreference(date) + " " + AppUtil.formatTimeByPreference(date);
            default:
                return null;
        }
    }

    private static CharSequence getFormattedLastContentModifiedDate(Task task) {
        Date date = new Date(task.getLastContentModifiedDate().longValue());
        return "Last modified: " + AppUtil.formatDateByPreference(date) + " " + AppUtil.formatTimeByPreference(date);
    }
}
