package com.irt.tasks.ui.taskslist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.irt.tasks.app.businessobjects.LevelState;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.businessprocess.TasksListBp;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.dragndrop.DropListener;
import com.irt.tasks.ui.dragndrop.RemoveListener;
import com.irt.tasks.ui.panel.EditManager;
import java.util.List;
import java.util.Map;

public class TasksListAdapter extends BaseAdapter implements RemoveListener, DropListener {
    ConfigManager config = ConfigManager.getInstance();
    EditManager editManager = EditManager.getInstance();
    Context mContext;
    Map<String, Long> mExpandedTasks;
    private LayoutInflater mInflater;
    private LevelState mLevelState;
    Task mTaskDecorator = new Task();
    protected List<Task> mTasks;
    TaskBp taskBp = new TaskBpImpl();
    TasksListBp tasksListBp = new TasksListBpImpl();

    public TasksListAdapter(Context context, List<Task> tasks, Map<String, Long> expandedTasks, Task taskDecorator, LevelState levelState) {
        this.mInflater = LayoutInflater.from(context);
        this.mTasks = tasks;
        this.mContext = context;
        this.mExpandedTasks = expandedTasks;
        this.mTaskDecorator = taskDecorator;
        this.mLevelState = levelState;
        initializeListItems(context);
    }

    private void initializeListItems(Context context) {
    }

    public int getCount() {
        return this.mTasks.size();
    }

    public Object getItem(int position) {
        return getTask(position);
    }

    public long getItemId(int position) {
        return getTask(position).getId().longValue();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return TasksListAdapterHelper.getView(getTask(position), convertView, this.mInflater, this.mContext, position, this.mLevelState, this.mExpandedTasks, this.mTaskDecorator);
    }

    private Task getTask(int position) {
        return (Task) this.mTasks.get(position);
    }

    public void onRemove(int which) {
        if (which >= 0 && which <= this.mTasks.size()) {
            this.mTasks.remove(which);
        }
    }

    public void onDrop(int from, int to) {
        Task temp = getTask(from);
        this.mTasks.remove(from);
        this.mTasks.add(to, temp);
    }
}
