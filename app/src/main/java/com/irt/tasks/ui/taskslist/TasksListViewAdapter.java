package com.irt.tasks.ui.taskslist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.CSVWriter;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.DateUtil;
import com.irt.tasks.app.common.ElapsedTime;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.TaskDaoImpl;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.preferences.ArraysConstants;
import com.irt.tasks.ui.task.EditTaskHelper;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TasksListViewAdapter extends BaseAdapter {
    ConfigManager config = ConfigManager.getInstance();
    Context mContext;
    private LayoutInflater mInflater;
    Task mTaskDecorator = new Task();
    String[] mTaskStatusArray;
    protected List<Task> mTasks;
    final long mTodayDateMillis = AppUtil.getTodayDateOnly().getTime();
    int mViewType = 0;
    TaskBp taskBp = new TaskBpImpl();

    static class ViewHolder {
        ImageView alarmClock;
        Bitmap alarmIcon;
        Bitmap alarmSnoozeIcon;
        TextView dueDateDescription;
        Bitmap emailAlarmIcon;
        Bitmap emailAlarmSnoozeIcon;
        TextView extraInfoBottom;
        TextView extraInfoTop;
        Bitmap floatIcon;
        CheckBox isCompleted;
        LinearLayout listItem;
        TextView reminderDateTime;
        Bitmap repeatFloatIcon;
        ImageView repeatFloatImageView;
        Bitmap repeatIcon;
        ImageButton snoozeRemove;
        TextView taskName;
        TextView taskNote;
        TextView task_header;

        ViewHolder() {
        }
    }

    public TasksListViewAdapter(Context context, List<Task> tasks, Task taskDecorator, int viewType) {
        this.mInflater = LayoutInflater.from(context);
        this.mTasks = tasks;
        this.mContext = context;
        this.mTaskDecorator = taskDecorator;
        this.mViewType = viewType;
        this.mTaskStatusArray = context.getResources().getStringArray(R.array.tasks_status_filter);
    }

    public int getCount() {
        return this.mTasks.size();
    }

    public Object getItem(int position) {
        return getTask(position);
    }

    public long getItemId(int position) {
        return getTask(position).getId().longValue();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate(R.layout.tasks_list_view_list_item, null);
            holder = initViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Task task = getTask(position);
        setSnoozeRemoveImageButton(holder);
        holder.snoozeRemove.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AppUtil.clearSnooze(TasksListViewAdapter.this.mContext, new TaskDaoImpl(), TasksListViewAdapter.this.getTask(position));
                //TasksListViewAdapter.this.mContext.refreshDisplay();
            }
        });
        setDoneCheckbox(holder, position);
        holder.isCompleted.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                boolean isRefreshDisplay;
                Task task = TasksListViewAdapter.this.getTask(position);
                if (task.getSnoozeDateTime().longValue() > 0) {
                    isRefreshDisplay = true;
                } else {
                    isRefreshDisplay = false;
                }
                task.setSnoozeDateTime(Long.valueOf(0));
                if (holder.isCompleted.isChecked()) {
                    AppUtil.clearNotification(TasksListViewAdapter.this.mContext, task);
                    task.setCompleted(Boolean.valueOf(true));
                } else {
                    task.setCompleted(Boolean.valueOf(false));
                }
                task.setLastContentModifiedDate(Long.valueOf(AppUtil.getTodayDateTimeMilisecond()));
                TasksListViewAdapter.this.taskBp.setTaskAsDoneAndAdjustAlarm(task, TasksListViewAdapter.this.mContext);
                TasksListViewAdapter.this.taskBp.saveOrUpdate(task, null);
                //TasksListViewAdapter.this.mContext.updateListItem(position, isRefreshDisplay);
            }
        });
        setTaskName(position, holder);
        setExtraInfoTop(position, holder);
        setNote(getTask(position), holder);
        setAlarmClock(holder, task, this.mContext);
        setRepeatFloat(holder, task, this.mContext);
        setExtraInfoBottom(position, holder);
        setTasksHeader(position, holder);
        return convertView;
    }

    private void setTasksHeader(int position, ViewHolder holder) {
        Task task = getTask(position);
        long prevDateToEvaluate = 0;
        long dateToEvaluate;
        switch (this.mViewType) {
            case 0:
                if (this.config.isShowTasksContexts()) {
                    String contextName;
                    if (StringUtil.isNullOrEmpty(getTask(position).getContextName()) && getTask(position).getContextName().equals(Constants.NO_CONTEXT_INDICATOR)) {
                        contextName = " ";
                    } else {
                        contextName = getTask(position).getContextName();
                    }
                    String prevContextNameToEvaluate = " ";
                    if (position > 0) {
                        String prevContextName;
                        if (StringUtil.isNullOrEmpty(getTask(position).getContextName()) && getTask(position).getContextName().equals(Constants.NO_CONTEXT_INDICATOR)) {
                            prevContextName = " ";
                        } else {
                            prevContextName = getTask(position - 1).getContextName();
                        }
                        prevContextNameToEvaluate = prevContextName;
                    }
                    if (position == 0 || !contextName.toLowerCase().equals(prevContextNameToEvaluate.toLowerCase())) {
                        if (contextName.equals(Constants.NO_CONTEXT_INDICATOR)) {
                            contextName = " --- no context ---";
                        }
                        SpannableStringBuilder contextNameSpan = new SpannableStringBuilder(contextName);
                        contextNameSpan.setSpan(new StyleSpan(1), 0, contextNameSpan.length(), 0);
                        holder.task_header.setText(contextNameSpan);
                        holder.task_header.setVisibility(View.VISIBLE);
                        return;
                    }
                    holder.task_header.setVisibility(View.GONE);
                    return;
                }
                break;
            case 2:
            case 8:
                String taskTitle;
                if (StringUtil.isNullOrEmpty(getTask(position).getName())) {
                    taskTitle = " ";
                } else {
                    taskTitle = getTask(position).getName();
                }
                String subStringToEvaluate = taskTitle.substring(0, 1);
                String prevSubStringToEvaluate = " ";
                if (position > 0) {
                    String prevTaskTitle;
                    if (StringUtil.isNullOrEmpty(getTask(position - 1).getName())) {
                        prevTaskTitle = " ";
                    } else {
                        prevTaskTitle = getTask(position - 1).getName();
                    }
                    prevSubStringToEvaluate = prevTaskTitle.substring(0, 1);
                }
                if (position == 0 || !subStringToEvaluate.toLowerCase().equals(prevSubStringToEvaluate.toLowerCase())) {
                    holder.task_header.setText(subStringToEvaluate.toUpperCase());
                    holder.task_header.setVisibility(View.VISIBLE);
                    return;
                }
                holder.task_header.setVisibility(View.GONE);
                return;
            case 3:
            case 7:
            case 9:
            case 13:
            case 15:
            case 16:
            case ArraysConstants.VIEW_MODIFIED /*18*/:
                dateToEvaluate = AppUtil.getDateOnly(new Date(task.getLastContentModifiedDate().longValue())).getTime();
                if (position > 0) {
                    prevDateToEvaluate = AppUtil.getDateOnly(new Date(getTask(position - 1).getLastContentModifiedDate().longValue())).getTime();
                }
                setTaskHeaderAndCurrentDate(position, holder, task, dateToEvaluate, prevDateToEvaluate);
                return;
            case 4:
            case 10:
            case 19:
                dateToEvaluate = AppUtil.getDateOnly(new Date(task.getCreationDate().longValue())).getTime();
                if (position > 0) {
                    prevDateToEvaluate = AppUtil.getDateOnly(new Date(getTask(position - 1).getCreationDate().longValue())).getTime();
                }
                setTaskHeaderAndCurrentDate(position, holder, task, dateToEvaluate, prevDateToEvaluate);
                return;
            case 5:
            case 11:
            case 17:
                dateToEvaluate = getDueDateWithFloats(task);
                if (position > 0) {
                    prevDateToEvaluate = getDueDateWithFloats(getTask(position - 1));
                }
                setTaskHeaderAndCurrentDateForDueDate(position, holder, task, dateToEvaluate, prevDateToEvaluate);
                return;
        }
        holder.task_header.setVisibility(View.GONE);
    }

    private long getDueDateWithFloats(Task task) {
        if (task.isConsideredAsFloat()) {
            return this.mTodayDateMillis;
        }
        return task.getDueDate().longValue();
    }

    private void setTaskHeaderAndCurrentDate(int position, ViewHolder holder, Task task, long dateToEvaluate, long prevDateToEvaluate) {
        if (position == 0 || dateToEvaluate != prevDateToEvaluate) {
            holder.task_header.setText(formatTaskHeaderDate(Long.valueOf(dateToEvaluate), false));
            holder.task_header.setVisibility(View.VISIBLE);
        } else {
            holder.task_header.setVisibility(View.GONE);
        }
        setTodayHeader(position, holder, task, dateToEvaluate);
    }

    private void setTaskHeaderAndCurrentDateForDueDate(int position, ViewHolder holder, Task task, long dateToEvaluate, long prevDateToEvaluate) {
        if (position == 0 || dateToEvaluate != prevDateToEvaluate) {
            long dayDiff = determineDateDifference(task);
            if (isYesterdayOrTomorrow(dayDiff)) {
                if (dayDiff > 0) {
                    holder.task_header.setText("Tomorrow, " + formatTaskHeaderDate(Long.valueOf(dateToEvaluate), true));
                } else {
                    holder.task_header.setText("Yesterday, " + formatTaskHeaderDate(Long.valueOf(dateToEvaluate), true));
                }
            } else if (dayDiff > 0) {
                holder.task_header.setText("In " + dayDiff + " days, " + formatTaskHeaderDate(Long.valueOf(dateToEvaluate), true));
            } else {
                holder.task_header.setText(Math.abs(dayDiff) + " days ago, " + formatTaskHeaderDate(Long.valueOf(dateToEvaluate), true));
            }
            holder.task_header.setVisibility(View.VISIBLE);
        } else {
            holder.task_header.setVisibility(View.GONE);
        }
        setTodayHeader(position, holder, task, dateToEvaluate);
    }

    private boolean isYesterdayOrTomorrow(long daysDiff) {
        return Math.abs(daysDiff) == 1;
    }

    private long determineDateDifference(Task task) {
        Calendar calToday = AppUtil.getTodayDateOnlyCal();
        Calendar calDueDate = Calendar.getInstance();
        calDueDate.setTimeInMillis(task.getDueDate().longValue());
        return (calDueDate.getTimeInMillis() - calToday.getTimeInMillis()) / ElapsedTime.DAY_IN_MILLIS;
    }

    private void setTodayHeader(int position, ViewHolder holder, Task task, long dateToEvaluate) {
        task.setMarkAsToday(setTodayBackgroundColor(holder, Long.valueOf(dateToEvaluate)));
        setTodayHeaderColor(holder, task, "Today, " + formatTaskHeaderDate(Long.valueOf(dateToEvaluate), true));
        if (getTask(position).getId().equals(this.mTaskDecorator.getId()) && this.mTaskDecorator.isSelected()) {
            formatForTaskSelected(position, holder);
        }
    }

    private void setTodayHeaderColor(ViewHolder holder, Task task, String headerString) {
        if (task.isMarkAsToday()) {
            SpannableStringBuilder dateSpan = new SpannableStringBuilder(headerString);
            //dateSpan.setSpan(new ForegroundColorSpan(-256), 0, dateSpan.length(), 0);
            dateSpan.setSpan(new StyleSpan(1), 0, dateSpan.length(), 0);
            holder.task_header.setText(dateSpan);
        }
    }

    private boolean setTodayBackgroundColor(ViewHolder holder, Long dateToEvaluate) {
        if (AppUtil.getTodayDateOnly().getTime() != dateToEvaluate.longValue()) {
            return false;
        }
        holder.listItem.setBackgroundColor(Constants.COLOR_TODAY_YELLOW);
        return true;
    }

    private String formatTaskHeaderDate(Long dateLong, boolean isDayInfo) {
        if (isDayInfo) {
            return DateUtil.format(new Date(dateLong.longValue()), "EEE, MMM d, yyyy");
        }
        return DateUtil.format(new Date(dateLong.longValue()), "EEEE, MMMM d, yyyy");
    }

    private static String formatReminderDateTime(Long reminderDateTimeLong, Long dueDate) {
        String timeFormat = "h:mm a";
        if (ConfigManager.getInstance().is24HourFormat()) {
            timeFormat = "k:mm";
        }
        return AppUtil.getReminderDaysEarlier(reminderDateTimeLong, dueDate) + " at " + DateUtil.format(new Date(reminderDateTimeLong.longValue()), timeFormat);
    }

    private void setExtraInfoBottom(int position, ViewHolder holder) {
        holder.extraInfoBottom.setVisibility(View.VISIBLE);
        holder.extraInfoBottom.setText(getTaskPath(getTask(position)));
        holder.extraInfoBottom.setTextSize(((float) this.config.getTaskNameFontSize().intValue()) * TasksListAdapterHelper.NOTE_FONT_SIZE_MULTIPLIER);
    }

    private void setNote(Task task, ViewHolder holder) {
        SpannableStringBuilder note = new SpannableStringBuilder();
        if (StringUtil.isNullOrEmpty(task.getNote())) {
            holder.taskNote.setVisibility(View.GONE);
            return;
        }
        holder.taskNote.setVisibility(View.VISIBLE);
        SpannableString spannableTextNote = SpannableString.valueOf(task.getNote());
        if (!(task.isShowNote().booleanValue() || this.config.getNoteVisibleLines().intValue() == 0)) {
            holder.taskNote.setMaxLines(this.config.getNoteVisibleLines().intValue());
        }
        if (this.config.isLinkifyNotes()) {
            Linkify.addLinks(spannableTextNote, Linkify.ALL);
        }
        note.append(spannableTextNote);
        holder.taskNote.setText(note);
        if (this.config.isLinkifyNotes()) {
            holder.taskNote.setOnTouchListener(TasksListAdapterHelper.mLinkHandler);
        }
        holder.taskNote.setTextSize(((float) this.config.getTaskNameFontSize().intValue()) * TasksListAdapterHelper.NOTE_FONT_SIZE_MULTIPLIER);
    }

    private void setAlarmClock(ViewHolder holder, Task task, Context context) {
        ConfigManager config = ConfigManager.getInstance();
        if (task.isCompleted().booleanValue() || !isReminderAlarmActive(task)) {
            holder.alarmClock.setVisibility(View.GONE);
            holder.reminderDateTime.setVisibility(View.GONE);
            return;
        }
        holder.alarmClock.setVisibility(View.VISIBLE);
        if (task.isNotifyByEmail().booleanValue()) {
            if (task.getSnoozeDateTime().longValue() <= 0) {
                holder.alarmClock.setImageBitmap(holder.emailAlarmIcon);
            } else if (AppUtil.isSnoozeEmailAllowed(task)) {
                holder.alarmClock.setImageBitmap(holder.emailAlarmSnoozeIcon);
            } else {
                holder.alarmClock.setImageBitmap(holder.alarmSnoozeIcon);
            }
        } else if (task.getSnoozeDateTime().longValue() > 0) {
            holder.alarmClock.setImageBitmap(holder.alarmSnoozeIcon);
        } else {
            holder.alarmClock.setImageBitmap(holder.alarmIcon);
        }
        int iconSize = iconSizeCalc(config) - 10;
        if (iconSize > 29) {
            iconSize = 29;
        }
        LayoutParams layoutParams = new LayoutParams(iconSize, iconSize);
        layoutParams.setMargins(10, 0, 0, 0);
        holder.alarmClock.setLayoutParams(layoutParams);
        holder.reminderDateTime.setVisibility(View.VISIBLE);
        holder.reminderDateTime.setText(getSnoozeLastReminder(task) + "REMINDER: " + formatReminderDateTime(task.getReminderDateTime(), task.getDueDate()));
        holder.reminderDateTime.setTextSize(((float) config.getTaskNameFontSize().intValue()) * TasksListAdapterHelper.NOTE_FONT_SIZE_MULTIPLIER);
    }

    private String getStatus(Task task) {
        if (task.getStatus().equals(Integer.valueOf(0))) {
            return "";
        }
        return "STATUS: " + this.mTaskStatusArray[task.getStatus().intValue() + 1] + CSVWriter.DEFAULT_LINE_END;
    }

    private String getSnoozeLastReminder(Task task) {
        if (isViewSnooze()) {
            return "Auto snooze: " + AppUtil.getFinalAutoSnoozeDescrtiptionForTask(this.mContext, task) + CSVWriter.DEFAULT_LINE_END;
        }
        return "";
    }

    private static void setRepeatFloat(ViewHolder holder, Task task, Context context) {
        ConfigManager config = ConfigManager.getInstance();
        if (task.isFloat().booleanValue() || task.isRecurringTask()) {
            holder.repeatFloatImageView.setVisibility(View.VISIBLE);
            if (task.isFloat().booleanValue() && task.isRecurringTask()) {
                holder.repeatFloatImageView.setImageBitmap(holder.repeatFloatIcon);
            } else if (task.isRecurringTask()) {
                holder.repeatFloatImageView.setImageBitmap(holder.repeatIcon);
            } else {
                holder.repeatFloatImageView.setImageBitmap(holder.floatIcon);
            }
            int iconSize = iconSizeCalc(config) - 10;
            if (iconSize > 29) {
                iconSize = 29;
            }
            LayoutParams layoutParams = new LayoutParams(iconSize, iconSize);
            layoutParams.setMargins(10, 0, 5, 0);
            holder.repeatFloatImageView.setLayoutParams(layoutParams);
            return;
        }
        holder.repeatFloatImageView.setVisibility(View.GONE);
    }

    private static boolean isReminderAlarmActive(Task task) {
        return task.isReminderEnabled().booleanValue() && (AppUtil.getTodayDateTimeMilisecond() <= task.getReminderDateTime().longValue() || task.getSnoozeDateTime().longValue() > 0);
    }

    private static int iconSizeCalc(ConfigManager config) {
        return config.getTaskNameFontSize().intValue() * 2;
    }

    private SpannableStringBuilder getTaskPath(Task task) {
        if (task.isTasksListAsTask()) {
            return new SpannableStringBuilder("");
        }
        SpannableStringBuilder note = new SpannableStringBuilder(getStatus(task));
        if (StringUtil.isNullOrEmpty(task.getTaskPath())) {
            note.append(task.getTasksListName() + "...");
            return note;
        }
        note.append(task.getTasksListName() + task.getTaskPath() + (this.config.isShowTaskNumbers() ? task.getTaskNumber() : ""));
        return note;
    }

    private void setExtraInfoTop(int position, ViewHolder holder) {
        Task task = getTask(position);
        if (isViewDueDate() && task.isConsideredAsFloat()) {
            holder.extraInfoTop.setVisibility(View.GONE);
            holder.dueDateDescription.setVisibility(View.GONE);
        } else {
            float fontSize = ((float) this.config.getTaskNameFontSize().intValue()) * TasksListAdapterHelper.DATE_FONT_SIZE_MULTIPLIER;
            if (isViewDueDate()) {
                holder.extraInfoTop.setVisibility(View.GONE);
            } else {
                holder.extraInfoTop.setVisibility(View.VISIBLE);
                if (isViewSnooze()) {
                    holder.extraInfoTop.setText(getExtraInfoForSnooze(task));
                } else {
                    holder.extraInfoTop.setText(TasksListViewHelper.formatExtraInfoTop(this.mViewType, task));
                }
                holder.extraInfoTop.setTextSize(fontSize);
            }
            if (task.getDueDate().longValue() <= 0 || isViewDueDate()) {
                holder.dueDateDescription.setVisibility(View.GONE);
            } else {
                holder.dueDateDescription.setVisibility(View.VISIBLE);
                holder.dueDateDescription.setText(TasksListViewHelper.formatExtraInfoTop(5, task));
                holder.dueDateDescription.setTextSize(5.0f + fontSize);
            }
        }
        if (this.mViewType == 0) {
            holder.extraInfoTop.setVisibility(View.GONE);
        }
    }

    private SpannableStringBuilder getExtraInfoForSnooze(Task task) {
        String autoOrManual;
        double minutesBeforeNextReminder = (((double) (task.getSnoozeDateTime().longValue() - AppUtil.getTodayDateTimeMilisecond())) / 1000.0d) / 60.0d;
        if (!AppUtil.isAutoSnoozeOff(task)) {
            autoOrManual = "In ";
        } else if (this.config.getTaskNameFontSize().intValue() > 19) {
            autoOrManual = "In ";
        } else {
            autoOrManual = "Manual, in ";
        }
        SpannableStringBuilder nextAlarmString = new SpannableStringBuilder(autoOrManual + AppUtil.formatTimeBeforeNextAlarm(minutesBeforeNextReminder) + " at ");
        nextAlarmString.append(TasksListViewHelper.formatExtraInfoTop(this.mViewType, task));
        return nextAlarmString;
    }

    private boolean isViewDueDate() {
        return 5 == this.mViewType || 11 == this.mViewType || 17 == this.mViewType;
    }

    private void setDoneCheckbox(ViewHolder holder, int position) {
        holder.isCompleted.setChecked(getTask(position).isCompleted().booleanValue());
        if (this.config.isShowDoneCheckbox()) {
            holder.isCompleted.setVisibility(View.VISIBLE);
        } else {
            holder.isCompleted.setVisibility(View.GONE);
        }
    }

    private void setSnoozeRemoveImageButton(ViewHolder holder) {
        if (isViewSnooze()) {
            holder.snoozeRemove.setVisibility(View.VISIBLE);
        } else {
            holder.snoozeRemove.setVisibility(View.GONE);
        }
    }

    private boolean isViewSnooze() {
        switch (this.mViewType) {
            case 1:
            case 14:
                return true;
            default:
                return false;
        }
    }

    private ViewHolder initViewHolder(View convertView) {
        ViewHolder holder = new ViewHolder();
        holder.snoozeRemove = (ImageButton) convertView.findViewById(R.id.snooze_remove);
        holder.snoozeRemove.setFocusable(false);
        holder.emailAlarmSnoozeIcon = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.ic_email_alarm_snooze);
        holder.alarmSnoozeIcon = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.ic_reminder_snooze);
        holder.repeatIcon = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.ic_recur);
        holder.repeatFloatIcon = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.ic_recur_float);
        holder.emailAlarmIcon = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.ic_email_alarm);
        holder.taskName = (TextView) convertView.findViewById(R.id.task_name);
        holder.extraInfoTop = (TextView) convertView.findViewById(R.id.task_extra_info_top);
        holder.extraInfoBottom = (TextView) convertView.findViewById(R.id.task_extra_info_bottom);
        holder.isCompleted = (CheckBox) convertView.findViewById(R.id.done_checkbox);
        holder.listItem = (LinearLayout) convertView.findViewById(R.id.list_item);
        holder.task_header = (TextView) convertView.findViewById(R.id.task_header);
        holder.taskNote = (TextView) convertView.findViewById(R.id.note_text);
        holder.reminderDateTime = (TextView) convertView.findViewById(R.id.reminder_date_time);
        holder.dueDateDescription = (TextView) convertView.findViewById(R.id.due_date_description);
        holder.alarmIcon = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.ic_reminder);
        holder.alarmClock = (ImageView) convertView.findViewById(R.id.alarm_clock);
        holder.floatIcon = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.ic_float);
        holder.repeatFloatImageView = (ImageView) convertView.findViewById(R.id.float_icon);
        return holder;
    }

    private void setTaskName(int position, ViewHolder holder) {
        SpannableStringBuilder taskName = getTaskName(position);
        formatTaskNameFromLevelColor(getTask(position), taskName);
        formatTaskNameFromDecorator(getTask(position), taskName, true);
        formatForTasksListName(position, taskName);
        formatForTaskSelected(position, holder);
        holder.taskName.setText(taskName);
        holder.taskName.setTextSize((float) this.config.getTaskNameFontSize().intValue());
    }

    private static void formatTaskNameFromLevelColor(Task task, SpannableStringBuilder taskName) {
        if (!task.isTasksListAsTask() && task.getTasksList() != null) {
            ConfigManager config = ConfigManager.getInstance();
            int taskLevel = task.getLevel().intValue();
            if (config.getLastSelectedTasksList().equals(DatabaseHelper.ALL_TASKS_LIST_ID)) {
                taskLevel--;
            }
            if (task.getTasksList().isColorTasks().booleanValue()) {
                int fontColor = config.getLevelColor(task.getTasksListId(), taskLevel).getFontColor().intValue();
                if (fontColor != 0) {
                    taskName.setSpan(new ForegroundColorSpan(EditTaskHelper.getFontColor(fontColor, fontColor)), 0, taskName.length(), 0);
                }
                AppUtil.setFontStyle(taskName, task);
            }
        }
    }

    private void formatForTaskSelected(int position, ViewHolder holder) {
        if (getTask(position).isTasksListAsTask() && getTask(position).getTasksListId().equals(this.mTaskDecorator.getTasksListId()) && this.mTaskDecorator.isSelected()) {
            holder.listItem.setBackgroundColor(Constants.COLOR_SELECTED_TASK_YELLOW);
        } else if (getTask(position).getId().equals(this.mTaskDecorator.getId()) && this.mTaskDecorator.isSelected()) {
            holder.listItem.setBackgroundColor(Constants.COLOR_SELECTED_TASK_YELLOW);
        } else {
            holder.listItem.setBackgroundDrawable(this.config.getContext().getResources().getDrawable(R.drawable.list_selector_background_normal));
        }
    }

    private Task getTask(int position) {
        return (Task) this.mTasks.get(position);
    }

    private SpannableStringBuilder getTaskName(int position) {
        SpannableStringBuilder taskName = new SpannableStringBuilder("");
        if (!StringUtil.isNullOrEmpty(getTask(position).getName())) {
            taskName.append(getTask(position).getName());
        }
        return taskName;
    }

    private void formatForTasksListName(int position, SpannableStringBuilder taskName) {
        if (getTask(position).isTasksListAsTask()) {
            taskName.setSpan(new StyleSpan(1), 0, taskName.length(), 0);
        }
    }

    private void formatTaskNameFromDecorator(Task task, SpannableStringBuilder taskName, boolean isOverrideColor) {
        if (task.isCompleted().booleanValue()) {
            taskName.setSpan(new StrikethroughSpan(), 0, taskName.length(), 0);
            taskName.setSpan(new ForegroundColorSpan(Color.parseColor(Constants.DONE_COLOR)), 0, taskName.length(), 0);
        } else if (task.isHighlight().booleanValue()) {
            switch (this.config.getHighlightStyle().intValue()) {
                case 0:
                    taskName.setSpan(new BackgroundColorSpan(-256), 0, taskName.length(), 0);
                    return;
                case 1:
                    if (isOverrideColor) {
                        taskName.setSpan(new ForegroundColorSpan(-65536), 0, taskName.length(), 0);
                        return;
                    }
                    return;
                case 2:
                    taskName.setSpan(new StyleSpan(1), 0, taskName.length(), 0);
                    if (isOverrideColor) {
                        taskName.setSpan(new ForegroundColorSpan(-65536), 0, taskName.length(), 0);
                        return;
                    }
                    return;
                default:
                    return;
            }
        } else if (task.getFontStyle().intValue() != 0 || task.getFontColor().intValue() != 0 || task.getFontBgColor().intValue() != 0) {
            AppUtil.setFontStyle(taskName, task);
            if (isOverrideColor && task.getFontColor().intValue() != 0) {
                AppUtil.setFontForegroundColor(taskName, task);
            }
            if (task.getFontBgColor().intValue() != 0) {
                AppUtil.setFontBackgroundColor(taskName, task);
            }
        }
    }
}
