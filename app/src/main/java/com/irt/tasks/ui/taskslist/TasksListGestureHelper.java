package com.irt.tasks.ui.taskslist;

public class TasksListGestureHelper {

    public static final int CREATE_NEW_EVENT_VIBRATE_DURATION_MILIS = 40;
    public static final int DEFAULT_FONT_SIZE = 19;
    public static final int FONT_SIZE_INCREMENT = 3;
    public static final int MAX_FONT_SIZE = 28;
    public static final int MIN_FONT_SIZE = 10;
    public static final int SWIPE_MAX_OFF_PATH = 250;
    public static final int SWIPE_MIN_DISTANCE = 120;
    public static final int SWIPE_THRESHOLD_VELOCITY = 200;
    public static final long TAP_INTERVAL_ALLOWED = 1500;
    public static int CONSIDERED_AS_HORIZONTAL_SWIPE_DISTANCE = SWIPE_MIN_DISTANCE;
    public static int CONSIDERED_AS_VERITCAL_SWIPE_DISTANCE = SWIPE_MIN_DISTANCE;
}
