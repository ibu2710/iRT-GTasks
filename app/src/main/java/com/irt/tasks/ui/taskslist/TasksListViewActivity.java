package com.irt.tasks.ui.taskslist;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.ContextBase;
import com.irt.tasks.app.businessobjects.ContextTask;
import com.irt.tasks.app.businessobjects.Tag;
import com.irt.tasks.app.businessobjects.TagTask;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.ContextBp;
import com.irt.tasks.app.businessprocess.ContextBpImpl;
import com.irt.tasks.app.businessprocess.ProjectBp;
import com.irt.tasks.app.businessprocess.ProjectBpImpl;
import com.irt.tasks.app.businessprocess.TagBp;
import com.irt.tasks.app.businessprocess.TagBpImpl;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.businessprocess.TasksListBp;
import com.irt.tasks.app.businessprocess.TasksListBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.Constants.HORIZONTAL_DIRECTION;
import com.irt.tasks.app.common.Constants.VERTICAL_VIEW_SECTIONS;
import com.irt.tasks.app.common.ErrorReporter;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.filter.TasksFilter;
import com.irt.tasks.ui.filter.TasksFilterManager;
import com.irt.tasks.ui.filter.TasksFilterParametersActivity;
import com.irt.tasks.ui.help.HelpUtility;
import com.irt.tasks.ui.help.HelpUtility.HelpDialog;
import com.irt.tasks.ui.preferences.ArraysConstants;
import com.irt.tasks.ui.task.EditTaskActivity;
import com.irt.tasks.ui.task.TaskViewActivity;
import java.util.ArrayList;
import java.util.List;

public class TasksListViewActivity extends AbstractTasksListActivity {
    private static final int TASK_POSITION_FROM_TOP = 3;
    ConfigManager config;
    ContextBp contextBp = new ContextBpImpl();
    private GestureDetector gestureDetector;
    boolean isGotoSelectedDecorator = false;
    Button mClearAllButton;
    Button mContextsTasksButton;
    Button mFilterParametersButton;
    TableLayout mFiltersTableLayout;
    ImageView mHelpImageView;
    public boolean mIsFromHomeView = false;
    public boolean mIsGotoToday = true;
    boolean mIsLongPress = false;
    boolean mIsRightTap = false;
    boolean mListItemSelected = false;
    ImageView mPlusImageFilterView;
    ImageView mPlusImageView;
    TextView mProjectDescription;
    Button mRefreshButton;
    TableLayout mSnoozeTableLayout;
    TextView mStatusContextDescription;
    long mTapTime = 0;
    List<TasksList> mTasksLists;
    VERTICAL_VIEW_SECTIONS mVerticalViewSection = VERTICAL_VIEW_SECTIONS.BOTTOM;
    int mViewType;
    ProjectBp projectBp = new ProjectBpImpl();
    TagBp tagBp = new TagBpImpl();
    TaskBp taskBp = new TaskBpImpl();
    TasksListBp tasksListBp = new TasksListBpImpl();

    class C19791 implements OnClickListener {
        C19791() {
        }

        public void onClick(View v) {
            TasksListViewActivity.this.refreshDisplay();
        }
    }

    class C19822 implements OnClickListener {

        class C19801 implements DialogInterface.OnClickListener {
            C19801() {
            }

            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }

        class C19812 implements DialogInterface.OnClickListener {
            C19812() {
            }

            public void onClick(DialogInterface dialog, int whichButton) {
                TasksListViewActivity.this.taskBp.clearAllSnoozeTasks(TasksListViewActivity.this.mTasksList, TasksListViewActivity.this);
                TasksListViewActivity.this.performBackFinish();
            }
        }

        C19822() {
        }

        public void onClick(View v) {
            new Builder(TasksListViewActivity.this).setTitle(R.string.alert_dialog_clear_all_snoozed_tasks).setPositiveButton(R.string.alert_dialog_ok, new C19812()).setNegativeButton(R.string.alert_dialog_cancel, new C19801()).show();
        }
    }

    class C19833 implements OnClickListener {
        C19833() {
        }

        public void onClick(View v) {
            HelpUtility.showHelp(TasksListViewActivity.this, HelpDialog.TASKS_VIEW);
        }
    }

    class C19844 implements OnClickListener {
        C19844() {
        }

        public void onClick(View v) {
            HelpUtility.showHelp(TasksListViewActivity.this, HelpDialog.TASKS_VIEW);
        }
    }

    class C19855 implements OnClickListener {
        C19855() {
        }

        public void onClick(View v) {
            TasksListViewActivity.this.startActivity(new Intent(TasksListViewActivity.this, TasksFilterParametersActivity.class));
        }
    }

    class C19866 implements OnClickListener {
        C19866() {
        }

        public void onClick(View v) {
            if (TasksListViewActivity.this.config.isShowTasksContexts()) {
                TasksListViewActivity.this.config.setShowTasksContexts(Boolean.valueOf(false));
                TasksListViewActivity.this.mContextsTasksButton.setText("Contexts");
            } else {
                TasksListViewActivity.this.config.setShowTasksContexts(Boolean.valueOf(true));
                TasksListViewActivity.this.mContextsTasksButton.setText("Tasks");
            }
            TasksListViewActivity.this.refreshDisplay();
        }
    }

    class C19877 implements OnTouchListener {
        C19877() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case 0:
                    TasksListViewActivity.this.mVerticalViewSection = AppUtil.getVerticalScreenSection(event.getY(), v);
                    break;
            }
            if (TasksListViewActivity.this.gestureDetector.onTouchEvent(event)) {
                return true;
            }
            switch (event.getAction()) {
                case 0:
                    TasksListViewActivity.this.mIsRightTap = AppUtil.isRightTap(event.getX(), v);
                    break;
            }
            return false;
        }
    }

    class C19888 implements OnClickListener {
        C19888() {
        }

        public void onClick(View v) {
            TasksListViewActivity.this.performAddNewTaskUsingPlus(Constants.IS_FROM_FILTER_VIEW);
        }
    }

    class C19899 implements OnClickListener {
        C19899() {
        }

        public void onClick(View v) {
            TasksListViewActivity.this.performAddNewTaskUsingPlus(Constants.IS_FROM_AGENDA_VIEW);
        }
    }

    class MyGestureDetector extends SimpleOnGestureListener {

        class C19901 implements DialogInterface.OnClickListener {
            C19901() {
            }

            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }

        MyGestureDetector() {
        }

        public void onLongPress(MotionEvent e) {
            if (TasksListViewActivity.this.mTasks.size() > 0) {
                TasksListViewActivity.this.mIsLongPress = true;
                TasksListViewActivity.this.markAsNOTSelected();
                TasksListViewActivity.this.turnONTaskSelector(null, false);
            }
            super.onLongPress(e);
        }

        public boolean onDoubleTap(MotionEvent e) {
            TasksListViewActivity.this.mIsLongPress = false;
            if (!TasksListViewActivity.this.mIsRightTap) {
                TasksListViewActivity.this.performEventFocusedAction(50);
            } else if (TasksListViewActivity.this.isViewTypeNotDeletedOrRecycleBin()) {
                Task task = TasksListViewActivity.this.getTask();
                if (TasksListViewActivity.this.config.isHideCompletedTasks() && (task == null || task.isCompleted().booleanValue())) {
                    new Builder(TasksListViewActivity.this).setTitle("Completed task hidden").setMessage("The completed task has been hidden. To show completed hidden tasks, uncheck 'Display Preferences -> Hide completed tasks'").setPositiveButton("OK", new C19901()).show();
                } else {
                    TasksListViewActivity.this.performRightDoubleTap();
                }
            } else {
                TasksListViewActivity.this.promptRestore();
            }
            return true;
        }

        public boolean onSingleTapConfirmed(MotionEvent e) {
            TasksListViewActivity.this.mIsLongPress = false;
            if (TasksListViewActivity.this.mIsRightTap) {
                TasksListViewActivity.this.performEventFocusedAction(30);
            }
            return true;
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) > 250.0f || Math.abs(e1.getX() - e2.getX()) < ((float) TasksListGestureHelper.CONSIDERED_AS_HORIZONTAL_SWIPE_DISTANCE) || Math.abs(e1.getY() - e2.getY()) > ((float) TasksListGestureHelper.CONSIDERED_AS_VERITCAL_SWIPE_DISTANCE)) {
                    return false;
                }
                if (e1.getX() - e2.getX() > BitmapDescriptorFactory.HUE_GREEN && Math.abs(velocityX) > 200.0f) {
                    switch (TasksListViewActivity.this.mVerticalViewSection) {
                        case TOP:
                            TasksListViewActivity.this.performSwipeAction(TasksListViewActivity.this.config.getTopLeftRightSwipeAction().intValue(), HORIZONTAL_DIRECTION.RIGHT);
                            break;
                        case MIDDLE:
                            TasksListViewActivity.this.performSwipeAction(TasksListViewActivity.this.config.getMiddleLeftRightSwipeAction().intValue(), HORIZONTAL_DIRECTION.RIGHT);
                            break;
                        case BOTTOM:
                            TasksListViewActivity.this.performSwipeAction(TasksListViewActivity.this.config.getBottomLeftRightSwipeAction().intValue(), HORIZONTAL_DIRECTION.RIGHT);
                            break;
                    }
                    return true;
                } else if (e2.getX() - e1.getX() <= BitmapDescriptorFactory.HUE_GREEN || Math.abs(velocityX) <= 200.0f) {
                    return false;
                } else {
                    switch (TasksListViewActivity.this.mVerticalViewSection) {
                        case TOP:
                            TasksListViewActivity.this.performSwipeAction(TasksListViewActivity.this.config.getTopLeftRightSwipeAction().intValue(), HORIZONTAL_DIRECTION.LEFT);
                            break;
                        case MIDDLE:
                            TasksListViewActivity.this.performSwipeAction(TasksListViewActivity.this.config.getMiddleLeftRightSwipeAction().intValue(), HORIZONTAL_DIRECTION.LEFT);
                            break;
                        case BOTTOM:
                            TasksListViewActivity.this.performSwipeAction(TasksListViewActivity.this.config.getBottomLeftRightSwipeAction().intValue(), HORIZONTAL_DIRECTION.LEFT);
                            break;
                    }
                    return true;
                }
            } catch (Exception e) {
                return false;
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        boolean z;
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.tasks_list);
        this.config = ConfigManager.getInstance();
        initViews();
        initHelpImageView();
        initTitle();
        initClearAllButton();
        initRefereshButton();
        initFilterParametersButton();
        initContextTasksButton();
        if (StringUtil.isNullOrEmpty(getIntent().getStringExtra(Constants.IS_VIEW_FROM_HOME))) {
            z = false;
        } else {
            z = true;
        }
        this.mIsFromHomeView = z;
        setSelectedTasksList();
        setListOnTouchListener();
        setListListeners();
        this.gestureDetector = new GestureDetector(new MyGestureDetector());
        Long decoratorTaskId = Long.valueOf(getIntent().getLongExtra(OptionsMenuHelper.DECORATOR_TASK_ID, Constants.INVALID_TASK_ID.longValue()));
        if (this.config.isStickyView() && !Constants.INVALID_TASK_ID.equals(decoratorTaskId)) {
            turnONTaskSelector(decoratorTaskId, false);
            this.isGotoSelectedDecorator = true;
        }
    }

    private void initRefereshButton() {
        this.mRefreshButton.setOnClickListener(new C19791());
    }

    private void initClearAllButton() {
        this.mClearAllButton.setOnClickListener(new C19822());
    }

    private void initTitle() {
        this.mTitle.setOnClickListener(new C19833());
    }

    private void initHelpImageView() {
        this.mHelpImageView.setOnClickListener(new C19844());
    }

    private void initFilterParametersButton() {
        this.mFilterParametersButton.setOnClickListener(new C19855());
    }

    private void initContextTasksButton() {
        if (this.config.isShowTasksContexts()) {
            this.mContextsTasksButton.setText("Tasks");
        } else {
            this.mContextsTasksButton.setText("Contexts");
        }
        this.mContextsTasksButton.setOnClickListener(new C19866());
    }

    private void initViews() {
        this.mTitle = (TextView) findViewById(R.id.title);
        this.mSnoozeTableLayout = (TableLayout) findViewById(R.id.snooze_table_layout);
        this.mClearAllButton = (Button) findViewById(R.id.clear_all_button);
        this.mRefreshButton = (Button) findViewById(R.id.refresh_button);
        this.mPlusImageView = (ImageView) findViewById(R.id.plus);
        this.mPlusImageFilterView = (ImageView) findViewById(R.id.plus_filter);
        this.mHelpImageView = (ImageView) findViewById(R.id.help_image);
        this.mFilterParametersButton = (Button) findViewById(R.id.filter_parameters_button);
        this.mContextsTasksButton = (Button) findViewById(R.id.contexts_button);
        this.mFiltersTableLayout = (TableLayout) findViewById(R.id.filters_table_layout);
        this.mProjectDescription = (TextView) findViewById(R.id.project_description);
        this.mStatusContextDescription = (TextView) findViewById(R.id.status_context_description);
    }

    public void refreshDisplay(Long newTaskId) {
        onResume();
        if (newTaskId != null && isNewTaskIdVisible(newTaskId)) {
            turnONTaskSelector(newTaskId, false);
        }
    }

    private boolean isNewTaskIdVisible(Long newTaskId) {
        for (Task task : this.mTasks) {
            if (newTaskId.equals(task.getId())) {
                return true;
            }
        }
        return false;
    }

    private void resetTapTime() {
        this.mTapTime = 0;
    }

    private void setSelectedTasksList() {
        Object object = ArchitectureContext.getObject();
        if (object != null && (object instanceof TasksList)) {
            this.mTasksList = (TasksList) object;
        } else if (object == null || !(object instanceof Task)) {
            this.mTasksList = DatabaseHelper.createTasksListCalledAll();
        } else {
            this.mTasksList = this.tasksListBp.get(this.config.getLastSelectedTasksList());
        }
    }

    public void updateListItem(int position, boolean isRefreshDisplay) {
        Task task = (Task) this.mTasks.get(position);
        if (isRefreshDisplay || (task.isRecurringTask() && !task.isCompleted().booleanValue())) {
            refreshDisplay();
            return;
        }
        View view = getListView().getChildAt(position - this.mFirstVisibleTask);
        SpannableStringBuilder taskName = new SpannableStringBuilder("");
        taskName.append(((Task) this.mTasks.get(position)).getName());
        AppUtil.formatTaskNameForDone(taskName, (Task) this.mTasks.get(position));
        setTaskNameInListItem(view, taskName);
        ((TextView) view.findViewById(R.id.due_date_description)).setText(TasksListViewHelper.formatExtraInfoTop(this.mViewType, (Task) this.mTasks.get(position)));
    }

    private void turnONTaskSelector(Long taskId, boolean isTasksListAsTask) {
        if (isTasksListAsTask || (taskId == null && getTask().isTasksListAsTask())) {
            if (taskId == null) {
                this.mTaskDecorator.setTasksListId(getTask().getTasksListId());
            } else {
                this.mTaskDecorator.setTasksListId(taskId);
            }
            this.mTaskDecorator.setId(Long.valueOf(-1));
        } else {
            this.mTaskDecorator.setTasksListId(Long.valueOf(-1));
            if (taskId == null) {
                this.mTaskDecorator.setId(getTask().getId());
            } else {
                this.mTaskDecorator.setId(taskId);
            }
        }
        this.mListItemSelected = true;
        this.mTaskDecorator.setSelected(true);
    }

    private void setListOnTouchListener() {
        getListView().setOnTouchListener(new C19877());
    }

    private void setTapTime() {
        this.mTapTime = AppUtil.getTodayDateTimeMilisecond();
    }

    private void performSwipeAction(int action, HORIZONTAL_DIRECTION horizontalDirection) {
        setTapTime();
        switch (action) {
            case 1:
                int newFontSize;
                if (horizontalDirection != HORIZONTAL_DIRECTION.RIGHT) {
                    if (28 > this.config.getTaskNameFontSize().intValue()) {
                        newFontSize = this.config.getTaskNameFontSize().intValue() + 3;
                        ConfigManager configManager = this.config;
                        if (28 < newFontSize) {
                            newFontSize = 28;
                        }
                        configManager.setTaskNameFontSize(Integer.valueOf(newFontSize));
                        break;
                    }
                } else if (10 < this.config.getTaskNameFontSize().intValue()) {
                    newFontSize = this.config.getTaskNameFontSize().intValue() - 3;
                    ConfigManager configManager2 = this.config;
                    if (10 > newFontSize) {
                        newFontSize = 10;
                    }
                    configManager2.setTaskNameFontSize(Integer.valueOf(newFontSize));
                    break;
                }
                break;
        }
        onResume();
    }

    private Task getTask() {
        if (this.mSelectedTaskPosition == this.mTasks.size()) {
            this.mSelectedTaskPosition--;
        }
        return (Task) this.mTasks.get(this.mSelectedTaskPosition);
    }

    public void markAsSelected() {
        View view = getViewForListItemProcessing();
        if (view != null) {
            getListItemLayout(view).setBackgroundColor(Constants.COLOR_SELECTED_TASK_YELLOW);
            this.mListItemSelected = true;
        }
    }

    private void markAsNOTSelected() {
        if (this.mListItemSelected) {
            View view;
            if (getTask().isMarkAsToday()) {
                view = getViewForListItemProcessing();
                if (view != null) {
                    getListItemLayout(view).setBackgroundColor(Constants.COLOR_TODAY_YELLOW);
                } else {
                    return;
                }
            }
            view = getViewForListItemProcessing();
            if (view != null) {
                getListItemLayout(view).setBackgroundDrawable(this.config.getContext().getResources().getDrawable(R.drawable.list_selector_background_normal));
            } else {
                return;
            }
            this.mListItemSelected = false;
        }
    }

    private LinearLayout getListItemLayout(View view) {
        return (LinearLayout) view.findViewById(R.id.list_item);
    }

    private void setTaskNameInListItem(View view, SpannableStringBuilder taskName) {
        ((TextView) view.findViewById(R.id.task_name)).setText(taskName);
    }

    private View getViewForListItemProcessing() {
        return getListView().getChildAt(this.mSelectedTaskPosition - this.mFirstVisibleTask);
    }

    private String getContextsDescrption(List<Long> contextIds) {
        List<ContextBase> contextBases = this.contextBp.getAllContexts();
        StringBuffer contextString = new StringBuffer();
        boolean firstRecord = true;
        TasksFilterManager filter = TasksFilterManager.getInstance();
        for (Long contextId : contextIds) {
            if (!firstRecord) {
                contextString.append(filter.getTaskFilter().isContextAnd() ? " & " : ", ");
            }
            appendContextName(contextString, contextId, contextBases);
            firstRecord = false;
        }
        return contextString.toString();
    }

    private void appendContextName(StringBuffer contextString, Long contextId, List<ContextBase> contextBases) {
        for (ContextBase contextBase : contextBases) {
            if (contextBase.getId().equals(contextId)) {
                contextString.append(contextBase.getName());
            }
        }
    }

    private String getTagsDescrption(List<Long> tagIds) {
        List<Tag> tags = this.tagBp.getAllTags();
        StringBuffer tagString = new StringBuffer();
        boolean firstRecord = true;
        TasksFilterManager filter = TasksFilterManager.getInstance();
        for (Long tagId : tagIds) {
            if (!firstRecord) {
                tagString.append(filter.getTaskFilter().isTagAnd() ? " & " : ", ");
            }
            appendTagName(tagString, tagId, tags);
            firstRecord = false;
        }
        return tagString.toString();
    }

    private void appendTagName(StringBuffer tagString, Long tagId, List<Tag> tags) {
        for (Tag tag : tags) {
            if (tag.getId().equals(tagId)) {
                tagString.append(tag.getName());
            }
        }
    }

    private void initTasks() {
        this.mViewType = getIntent().getIntExtra(OptionsMenuHelper.VIEW_TYPE, 7);
        String query = getIntent().getStringExtra("query");
        if (!this.mTasksList.getId().equals(DatabaseHelper.ALL_TASKS_LIST_ID)) {
            this.mTasksList = this.tasksListBp.get(this.mTasksList.getId());
        }
        this.mTasks = this.taskBp.getAllForView(this.mViewType, this.mTasksList, query);
        String viewTitle = "";
        switch (this.mViewType) {
            case 0:
                TasksFilter tasksFilter = TasksFilterManager.getInstance().getTaskFilter();
                if (tasksFilter.getProjectId().longValue() > 0 || tasksFilter.isProjectNone()) {
                    this.mProjectDescription.setVisibility(View.VISIBLE);
                    String projectDescription = "";
                    if (tasksFilter.isProjectNone()) {
                        projectDescription = "no project";
                    } else {
                        projectDescription = this.projectBp.get(tasksFilter.getProjectId()).getTitle();
                    }
                    this.mProjectDescription.setText("Project: " + projectDescription);
                } else {
                    this.mProjectDescription.setVisibility(View.GONE);
                }
                if (tasksFilter.getStatus().intValue() == 0 && tasksFilter.getContextIds() == null && !tasksFilter.isContextNone() && tasksFilter.getTagIds() == null && !tasksFilter.isTagNone()) {
                    this.mStatusContextDescription.setVisibility(View.GONE);
                } else {
                    this.mStatusContextDescription.setVisibility(View.VISIBLE);
                    StringBuffer statusContextDescription = new StringBuffer();
                    if (tasksFilter.getStatus().intValue() > 0) {
                        statusContextDescription.append("S: ");
                        statusContextDescription.append(AppUtil.getStringFromArrayValueBasedOnPosition(this, R.array.task_status, tasksFilter.getStatus().intValue() - 1));
                    }
                    if (tasksFilter.getContextIds() != null) {
                        if (tasksFilter.getStatus().intValue() != 0) {
                            statusContextDescription.append("; ");
                        }
                        statusContextDescription.append(getContextsDescrption(tasksFilter.getContextIds()));
                    } else if (tasksFilter.isContextNone()) {
                        if (tasksFilter.getStatus().intValue() != 0) {
                            statusContextDescription.append("; ");
                        }
                        statusContextDescription.append(Constants.NO_CONTEXT);
                    }
                    if (tasksFilter.getTagIds() != null) {
                        if (statusContextDescription.length() > 0) {
                            statusContextDescription.append("; T: ");
                        }
                        statusContextDescription.append(getTagsDescrption(tasksFilter.getTagIds()));
                    } else if (tasksFilter.isTagNone()) {
                        if (statusContextDescription.length() != 0) {
                            statusContextDescription.append("; T: ");
                        }
                        statusContextDescription.append("no tag");
                    }
                    this.mStatusContextDescription.setText(statusContextDescription.toString());
                }
                this.mFiltersTableLayout.setVisibility(View.VISIBLE);
                this.mPlusImageFilterView.setVisibility(View.VISIBLE);
                this.mPlusImageFilterView.setOnClickListener(new C19888());
                if (!this.config.isShowAllTasksOnFilter()) {
                    viewTitle = getWindowTitle("Filter");
                    break;
                } else {
                    viewTitle = getWindowTitle("Filter (All)");
                    break;
                }
            case 1:
                viewTitle = getWindowTitle("Snoozed Tasks");
                if (this.mTasks.size() <= 0) {
                    this.mSnoozeTableLayout.setVisibility(View.GONE);
                    break;
                } else {
                    this.mSnoozeTableLayout.setVisibility(View.GONE);
                    break;
                }
            case 2:
                viewTitle = getWindowTitle("Alphabetical");
                break;
            case 3:
                viewTitle = getWindowTitle("Completed tasks");
                break;
            case 4:
                viewTitle = getWindowTitle("Creation date");
                break;
            case 5:
                viewTitle = getWindowTitle("Due date");
                break;
            case 6:
                viewTitle = getWindowTitle("Flat list");
                break;
            case 7:
                viewTitle = getWindowTitle("Last Modified");
                break;
            case 8:
                viewTitle = getWindowTitle("Alphabetical (desc)");
                break;
            case 9:
                viewTitle = getWindowTitle("Completed tasks (asc)");
                break;
            case 10:
                viewTitle = getWindowTitle("Creation date (asc)");
                break;
            case 11:
                viewTitle = getWindowTitle("Due date  (desc)");
                break;
            case 12:
                viewTitle = getWindowTitle("Flat list  (desc)");
                break;
            case 13:
                viewTitle = getWindowTitle("Last Modified  (asc)");
                break;
            case 14:
                viewTitle = getWindowTitle("Snooze  (asc)");
                break;
            case 15:
                viewTitle = getWindowTitle("Deleted");
                break;
            case 16:
                viewTitle = getWindowTitle("Recycle bin");
                break;
            case 17:
                this.mPlusImageView.setVisibility(View.VISIBLE);
                this.mPlusImageView.setOnClickListener(new C19899());
                viewTitle = getWindowTitle("Agenda");
                break;
            case ArraysConstants.VIEW_MODIFIED /*18*/:
                viewTitle = getWindowTitle("Modified");
                break;
            case 19:
                viewTitle = getWindowTitle("Created");
                break;
            case 20:
                viewTitle = getWindowTitle("Search - " + query);
                break;
        }
        setWindowTitle(viewTitle);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (-1 == resultCode) {
            Long newTaskId = null;
            switch (requestCode) {
                case 1:
                    if (data != null) {
                        newTaskId = (Long) data.getExtras().get(Constants.NEW_TASK_ID);
                        break;
                    }
                    break;
                case 2:
                    break;
                default:
                    return;
            }
            refreshDisplay(newTaskId);
        }
    }

    public void performAddNewTaskUsingPlus(String fromWhichView) {
        Long tasksListId = this.config.getLastSelectedTasksList();
        if (DatabaseHelper.ALL_TASKS_LIST_ID.equals(tasksListId)) {
            tasksListId = getTasksListForNewTask().getId();
        }
        turnONTaskSelector(tasksListId, true);
        Task task = new Task();
        task.setTasksListAsTask(true);
        task.setTasksListId(tasksListId);
        if (Constants.IS_FROM_AGENDA_VIEW.equals(fromWhichView)) {
            task.setDueDate(Long.valueOf(AppUtil.getTodayDateOnly().getTime()));
        } else if (Constants.IS_FROM_FILTER_VIEW.equals(fromWhichView)) {
            TasksFilter tasksFilter = TasksFilterManager.getInstance().getTaskFilter();
            task.setProjectId(tasksFilter.getProjectId());
            task.setStatus(Integer.valueOf(tasksFilter.getStatus().intValue() == 0 ? 0 : tasksFilter.getStatus().intValue() - 1));
            task.setContextTasks(getContextTasks(tasksFilter.getContextIds()));
            task.setTagTasks(getTagTasks(tasksFilter.getTagIds()));
        }
        ArchitectureContext.setObject(task);
        Intent intent = new Intent(this, EditTaskActivity.class);
        intent.putExtra(Constants.IS_NEW_TASK, Constants.TRUE);
        intent.putExtra(Constants.IS_CHILD_TASK, Constants.TRUE);
        intent.putExtra(fromWhichView, Constants.TRUE);
        startActivityForResult(intent, 1);
    }

    private List<ContextTask> getContextTasks(List<Long> contextIds) {
        List<ContextTask> contextTasks = null;
        if (contextIds != null && contextIds.size() > 0) {
            contextTasks = new ArrayList();
            for (Long contextId : contextIds) {
                contextTasks.add(new ContextTask(contextId, null));
            }
        }
        return contextTasks;
    }

    private List<TagTask> getTagTasks(List<Long> tagIds) {
        List<TagTask> tagTasks = null;
        if (tagIds != null && tagIds.size() > 0) {
            tagTasks = new ArrayList();
            for (Long tagId : tagIds) {
                tagTasks.add(new TagTask(tagId, null));
            }
        }
        return tagTasks;
    }

    private TasksList getTasksListForNewTask() {
        if (this.mTasksLists == null) {
            this.mTasksLists = (List) ArchitectureContext.getObjectExta();
            if (this.mTasksLists == null) {
                Log.i(Constants.TAG, "mTasksList is null, fetching from DB");
                this.mTasksLists = new ArrayList();
                for (TasksList tasksList : this.tasksListBp.getAllTasksListBySortOrder()) {
                    if (this.config.isShowInvisibleAccounts().booleanValue() || tasksList.isVisible().booleanValue()) {
                        this.mTasksLists.add(tasksList);
                    }
                }
            }
        }
        if (Constants.TASKS_LIST_AUTO_ID.equals(this.config.getTasksListForNewTaskPlus())) {
            return getLastSelectedTasksList();
        }
        for (TasksList tasksList2 : this.mTasksLists) {
            if (tasksList2.getId().equals(this.config.getTasksListForNewTaskPlus())) {
                return tasksList2;
            }
        }
        Log.i(Constants.TAG, "mTasksList is hidden " + this.mTasksList.getName());
        this.config.setTasksListForNewTaskPlus(Constants.TASKS_LIST_AUTO_ID);
        return getLastSelectedTasksList();
    }

    private TasksList getLastSelectedTasksList() {
        for (TasksList tasksList : this.mTasksLists) {
            if (tasksList.getId().equals(this.config.getLastSelectedTasksList())) {
                return tasksList;
            }
        }
        return null;
    }

    private String getWindowTitle(String windowTitlePrefix) {
        return windowTitlePrefix + getTaskCount();
    }

    private String getTaskCount() {
        return " (" + this.mTasks.size() + ")";
    }

    private void setListListeners() {
        getListView().setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                TasksListViewActivity.this.markAsNOTSelected();
                TasksListViewActivity.this.mSelectedTaskPosition = position;
                TasksListViewActivity.this.markAsSelected();
                if (TasksListViewActivity.this.isNextActionAllowed()) {
                    TasksListViewActivity.this.setTapTime();
                } else {
                    TasksListViewActivity.this.resetTapTime();
                }
            }
        });
        getListView().setOnScrollListener(new OnScrollListener() {
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                TasksListViewActivity.this.mFirstVisibleTask = firstVisibleItem;
            }
        });
        registerForContextMenu(getListView());
    }

    private void performRightDoubleTap() {
        Task task = getTask();
        if (this.config.getLastSelectedTasksList().equals(DatabaseHelper.ALL_TASKS_LIST_ID) || isViewConsideredAll()) {
            this.config.setLastSelectedTasksList(getTask().getTasksListId());
            this.mTasksList = this.tasksListBp.get(getTask().getTasksListId());
            ArchitectureContext.setObject(this.mTasksList);
            this.mTasks = this.taskBp.getTasksByLevel(null, 20, false, this.mTasksList, true);
            task = getTaskFromTasks(task.getId());
        }
        ArchitectureContext.setObjectExtra(this.mTasks);
        Intent intent = new Intent(this, TasksListActivity.class);
        intent.setAction("android.intent.action.VIEW");
        intent.setData(Uri.parse(task.getId() + "").buildUpon().build());
        intent.putExtra(Constants.IS_FROM_VIEW, Constants.TRUE);
        startActivity(intent);
        finish();
        this.mPlusImageView.setVisibility(View.GONE);
        this.mPlusImageFilterView.setVisibility(View.GONE);
    }

    private boolean isViewConsideredAll() {
        switch (this.mViewType) {
            case 0:
            case 17:
            case ArraysConstants.VIEW_MODIFIED /*18*/:
            case 19:
                return true;
            default:
                return false;
        }
    }

    private Task getTaskFromTasks(Long taskId) {
        for (Task task : this.mTasks) {
            if (task.getId().equals(taskId)) {
                return task;
            }
        }
        return null;
    }

    private void promptRestore() {
        new Builder(this).setTitle("Restore task").setMessage("Restore [" + ((Task) this.mTasks.get(this.mSelectedTaskPosition)).getName() + "] to top of [" + ((Task) this.mTasks.get(this.mSelectedTaskPosition)).getTasksListName() + "] list?").setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichTaksList) {
                dialog.dismiss();
                TasksListViewActivity.this.taskBp.restoreTask((Task) TasksListViewActivity.this.mTasks.get(TasksListViewActivity.this.mSelectedTaskPosition), 16 == TasksListViewActivity.this.mViewType);
                TasksListViewActivity.this.onResume();
            }
        }).setNegativeButton(R.string.alert_dialog_cancel, null).show();
    }

    private boolean isViewTypeNotDeletedOrRecycleBin() {
        return (15 == this.mViewType || 16 == this.mViewType) ? false : true;
    }

    private void performEventFocusedAction(int action) {
        switch (action) {
            case 30:
                if (isViewTypeNotDeletedOrRecycleBin()) {
                    Intent intent;
                    turnONTaskSelector(null, false);
                    ArchitectureContext.setObject(getTask());
                    if (this.config.getRightTapAction().intValue() == 65) {
                        intent = new Intent(this, TaskViewActivity.class);
                    } else {
                        intent = new Intent(this, EditTaskActivity.class);
                    }
                    startActivity(intent);
                    return;
                }
                promptRestore();
                return;
            case 50:
                if (this.config.isShowDoneCheckbox()) {
                    this.config.setShowDoneCheckbox(false);
                } else {
                    this.config.setShowDoneCheckbox(true);
                }
                onResume();
                return;
            default:
                return;
        }
    }

    private void showActionType(String actionMessage) {
        new Builder(this).setTitle(actionMessage).setMessage(actionMessage).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    private boolean isNextActionAllowed() {
        return AppUtil.getTodayDateTimeMilisecond() - this.mTapTime > TasksListGestureHelper.TAP_INTERVAL_ALLOWED;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu_tasks, menu);
        inflater.inflate(R.menu.options_menu, menu);
        MenuItem dueTodayMenuItem = AppUtil.getMenuItem(menu, R.id.menu_panel);
        dueTodayMenuItem.setTitle("Due Today");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_search:
                onSearchRequested();
                return true;
            case R.id.menu_panel:
                int viewType = this.mViewType;
                if (!(5 == this.mViewType || 11 == this.mViewType)) {
                    viewType = 5;
                }
                Intent intent = new Intent(this, TasksListViewActivity.class);
                intent.putExtra(OptionsMenuHelper.VIEW_TYPE, viewType);
                startActivity(intent);
                finish();
                return true;
            default:
                return OptionsMenuHelper.onOptionsItemSelected(this, item.getItemId(), item);
        }
    }

    protected void onResume() {
        super.onResume();
        this.config.setCurrentContext(this);
        ErrorReporter.CheckErrorAndSendMailCall(this);
        initTasks();
        setListAdapter(new TasksListViewAdapter(this, this.mTasks, this.mTaskDecorator, this.mViewType));
        if (this.mIsGotoToday && (5 == this.mViewType || 11 == this.mViewType || 17 == this.mViewType)) {
            setNextTask();
        }
        if (this.isGotoSelectedDecorator) {
            setFirstVisibleTaskBasedOnSelectedDecorator();
            this.isGotoSelectedDecorator = false;
        }
        this.mIsGotoToday = false;
        getListView().setSelection(this.mFirstVisibleTask);
        AppUtil.setMenuImage(this);
    }

    private void setNextTask() {
        long todayDate = AppUtil.getTodayDateOnly().getTime();
        int i = 0;
        while (i < this.mTasks.size()) {
            Task task = (Task) this.mTasks.get(i);
            if ((5 == this.mViewType || 17 == this.mViewType) && (task.getDueDate().longValue() >= todayDate || task.isFloat().booleanValue())) {
                this.mFirstVisibleTask = i;
                return;
            } else if (11 != this.mViewType || task.getDueDate().longValue() > todayDate) {
                i++;
            } else {
                this.mFirstVisibleTask = i;
                return;
            }
        }
    }

    private boolean setFirstVisibleTaskBasedOnSelectedDecorator() {
        boolean isFoundSelectedTask = false;
        for (int i = 0; i < this.mTasks.size(); i++) {
            if (((Task) this.mTasks.get(i)).getId().equals(this.mTaskDecorator.getId())) {
                this.mFirstVisibleTask = i - 3;
                if (this.mFirstVisibleTask < 0) {
                    this.mFirstVisibleTask = 0;
                }
                this.mSelectedTaskPosition = i;
                isFoundSelectedTask = true;
            }
        }
        return isFoundSelectedTask;
    }

    private void setWindowTitle(String viewTitle) {
        if (StringUtil.isNullOrEmpty(AppUtil.getCurrentlySyncedTasksList())) {
            String listName;
            if (StringUtil.isNullOrEmpty(this.mTasksList.getGtasksListName())) {
                listName = "ALL TASKS LIST";
            } else {
                listName = this.mTasksList.getGtasksListName();
            }
            this.mTitle.setText(AppUtil.getMaxWindowTitle(listName + " - " + viewTitle));
        } else {
            this.mTitle.setText(AppUtil.getCurrentlySyncedTasksList());
        }
        this.mTitle.setBackgroundColor(Color.parseColor(Constants.COLOR_BLUISH_DAY_HEADER));
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && event.getRepeatCount() == 0) {
            try {
                performBackFinish();
                return super.onKeyDown(keyCode, event);
            } catch (IllegalStateException e) {
                e.printStackTrace();
                return true;
            }
        }
        if (keyCode == 84 && event.getRepeatCount() == 0) {
            try {
                onSearchRequested();
                return super.onKeyDown(keyCode, event);
            } catch (IllegalStateException e2) {
                e2.printStackTrace();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void performBackFinish() {
        Intent intent;
        finish();
        if (isUseHomeView()) {
            intent = new Intent(this, TasksListHomeActivity.class);
        } else {
            intent = new Intent(this, TasksListActivity.class);
        }
        startActivity(intent);
        this.mPlusImageView.setVisibility(View.GONE);
        this.mPlusImageFilterView.setVisibility(View.GONE);
    }

    public boolean onSearchRequested() {
        Intent intent;
        finish();
        if (isUseHomeView()) {
            intent = new Intent(this, TasksListHomeActivity.class);
        } else {
            intent = new Intent(this, TasksListActivity.class);
        }
        intent.putExtra(Constants.IS_SEARCH_FROM_HOME, Constants.TRUE);
        startActivity(intent);
        return true;
    }

    private boolean isUseHomeView() {
        return this.mIsFromHomeView || (this.mTasksList.getId().equals(DatabaseHelper.ALL_TASKS_LIST_ID) && this.config.isViewFromHome());
    }

    public String getEmailAllTasksBody() {
        return null;
    }

    public String getEmailVisibleTasksBody() {
        return null;
    }

    public void refreshDisplay() {
        refreshDisplay(null);
    }

    public void showCopyList() {
    }

    public void showCopyListAsBranch() {
    }
}
