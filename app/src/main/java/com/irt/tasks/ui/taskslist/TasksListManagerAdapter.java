package com.irt.tasks.ui.taskslist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.dragndrop.DropListener;
import com.irt.tasks.ui.dragndrop.RemoveListener;
import java.util.List;

public final class TasksListManagerAdapter extends BaseAdapter implements RemoveListener, DropListener {
    ConfigManager config;
    private LayoutInflater mInflater;
    private List<TasksList> mTasksLists;

    public TasksListManagerAdapter(Context context, List<TasksList> content) {
        init(context, content);
    }

    private void init(Context context, List<TasksList> content) {
        this.config = ConfigManager.getInstance();
        this.mInflater = LayoutInflater.from(context);
        this.mTasksLists = content;
    }

    public List<TasksList> getSortedTasksLists() {
        return this.mTasksLists;
    }

    public int getCount() {
        return this.mTasksLists.size();
    }

    public Object getItem(int position) {
        return getTasksList(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        getTasksList(position).setSortPosition(Integer.valueOf(position));
        return TasksListManagerAdapterHelper.getView(getTasksList(position), convertView, this.mInflater);
    }

    private TasksList getTasksList(int position) {
        return (TasksList) this.mTasksLists.get(position);
    }

    public void onRemove(int which) {
        if (which >= 0 && which <= this.mTasksLists.size()) {
            this.mTasksLists.remove(which);
        }
    }

    public void onDrop(int from, int to) {
        TasksList temp = getTasksList(from);
        this.mTasksLists.remove(from);
        this.mTasksLists.add(to, temp);
    }
}
