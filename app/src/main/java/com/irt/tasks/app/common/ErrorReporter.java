package com.irt.tasks.app.common;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.StatFs;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Date;
import java.util.Random;

public class ErrorReporter implements UncaughtExceptionHandler {
    static String FilePath = null;
    private static ErrorReporter S_mInstance;
    String AndroidVersion;
    String Board;
    String Brand;
    private Context CurContext;
    String Device;
    String Display;
    String FingerPrint;
    String Host;
    String ID;
    String Manufacturer;
    String Model;
    String PackageName;
    String PhoneModel;
    private UncaughtExceptionHandler PreviousHandler;
    String Product;
    String Tags;
    long Time;
    String Type;
    String User;
    String VersionName;
    String calendarVersion;
    boolean isEclairCalendarVersionCallable;
    boolean isFroyoCalendarVersionCallable;
    boolean isHtcCalendarVersionCallable;
    boolean isLandscape;
    boolean isVndAndroidCursorItemCallable;
    boolean isVndGoogleCursorItemCallable;
    boolean isVndHtcCursorItemCallable;
    int screenHeight;
    int screenWidth;

    public void Init(Context context) {
        this.PreviousHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
        RecoltInformations(context);
        this.CurContext = context;
    }

    public long getAvailableInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize());
    }

    public long getTotalInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) stat.getBlockCount()) * ((long) stat.getBlockSize());
    }

    void RecoltInformations(Context context) {
        boolean z = false;
        try {
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            this.VersionName = pi.versionName;
            this.PackageName = pi.packageName;
            if (FilePath == null) {
                FilePath = context.getFilesDir().getAbsolutePath();
            }
            this.PhoneModel = Build.MODEL;
            this.AndroidVersion = VERSION.RELEASE;
            this.Board = Build.BOARD;
            this.Brand = Build.BRAND;
            this.Device = Build.DEVICE;
            this.Display = Build.DISPLAY;
            this.FingerPrint = Build.FINGERPRINT;
            this.Host = Build.HOST;
            this.ID = Build.ID;
            this.Model = Build.MODEL;
            this.Product = Build.PRODUCT;
            this.Tags = Build.TAGS;
            this.Time = Build.TIME;
            this.Type = Build.TYPE;
            this.User = Build.USER;
            DisplayMetrics dm = new DisplayMetrics();
            ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(dm);
            this.screenHeight = dm.heightPixels;
            this.screenWidth = dm.widthPixels;
            if (context.getResources().getConfiguration().orientation == 2) {
                z = true;
            }
            this.isLandscape = z;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String CreateInformationString() {
        return (((((((((((((((((((((((((((((((((((((((((((((("" + "Version : " + this.VersionName) + CSVWriter.DEFAULT_LINE_END) + "Package : " + this.PackageName) + CSVWriter.DEFAULT_LINE_END) + "FilePath : " + FilePath) + CSVWriter.DEFAULT_LINE_END) + "Phone Model" + this.PhoneModel) + CSVWriter.DEFAULT_LINE_END) + "Android Version : " + this.AndroidVersion) + CSVWriter.DEFAULT_LINE_END) + "Board : " + this.Board) + CSVWriter.DEFAULT_LINE_END) + "Brand : " + this.Brand) + CSVWriter.DEFAULT_LINE_END) + "Device : " + this.Device) + CSVWriter.DEFAULT_LINE_END) + "Display : " + this.Display) + CSVWriter.DEFAULT_LINE_END) + "Finger Print : " + this.FingerPrint) + CSVWriter.DEFAULT_LINE_END) + "Host : " + this.Host) + CSVWriter.DEFAULT_LINE_END) + "ID : " + this.ID) + CSVWriter.DEFAULT_LINE_END) + "Model : " + this.Model) + CSVWriter.DEFAULT_LINE_END) + "Landscape Mode : " + this.isLandscape) + CSVWriter.DEFAULT_LINE_END) + "Screen Size : " + this.screenWidth + "x" + this.screenHeight) + CSVWriter.DEFAULT_LINE_END) + "Product : " + this.Product) + CSVWriter.DEFAULT_LINE_END) + "Tags : " + this.Tags) + CSVWriter.DEFAULT_LINE_END) + "Time : " + this.Time) + CSVWriter.DEFAULT_LINE_END) + "Type : " + this.Type) + CSVWriter.DEFAULT_LINE_END) + "User : " + this.User) + CSVWriter.DEFAULT_LINE_END) + "Calendar Version : " + this.calendarVersion) + CSVWriter.DEFAULT_LINE_END) + CSVWriter.DEFAULT_LINE_END) + "Total Internal memory : " + getTotalInternalMemorySize()) + CSVWriter.DEFAULT_LINE_END) + "Available Internal memory : " + getAvailableInternalMemorySize()) + CSVWriter.DEFAULT_LINE_END;
    }

    public void uncaughtException(Thread t, Throwable e) {
        Date CurDate = new Date();
        String Report = ((((((((((("" + "Error Report collected on : " + CurDate.toString()) + CSVWriter.DEFAULT_LINE_END) + CSVWriter.DEFAULT_LINE_END) + "Informations :") + CSVWriter.DEFAULT_LINE_END) + "==============") + CSVWriter.DEFAULT_LINE_END) + CSVWriter.DEFAULT_LINE_END) + CreateInformationString()) + "\n\n") + "Stack : \n") + "======= \n";
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        e.printStackTrace(printWriter);
        Report = (((Report + result.toString()) + CSVWriter.DEFAULT_LINE_END) + "Cause : \n") + "======= \n";
        for (Throwable cause = e.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
            Report = Report + result.toString();
        }
        printWriter.close();
        SaveAsFile(Report + "****  End of current Report ***");
        this.PreviousHandler.uncaughtException(t, e);
    }

    static ErrorReporter getInstance() {
        if (S_mInstance == null) {
            S_mInstance = new ErrorReporter();
        }
        return S_mInstance;
    }

    private void SendErrorMail(Context _context, String ErrorContent) {
        Intent sendIntent = new Intent("android.intent.action.SEND");
        String body = "[v" + AppUtil.getVersionName() + "] iRT GTasks Outliner Error Report content...." + "\n\n" + ErrorContent + "\n\n";
        sendIntent.putExtra("android.intent.extra.EMAIL", new String[]{Constants.IRT_WEB_SOLUTIONS_EMAIL});
        sendIntent.putExtra("android.intent.extra.TEXT", body);
        sendIntent.putExtra("android.intent.extra.SUBJECT", "iRT GTasks Outliner Error Report");
        sendIntent.setType("message/rfc822");
        _context.startActivity(Intent.createChooser(sendIntent, "Detected an error, send to\niRT Web Solutions"));
    }

    private void SaveAsFile(String ErrorContent) {
        try {
            FileOutputStream trace = this.CurContext.openFileOutput("stack-" + new Random().nextInt(99999) + ".stacktrace", 0);
            trace.write(ErrorContent.getBytes());
            trace.close();
        } catch (IOException e) {
        }
    }

    private String[] GetErrorFileList() {
        File dir = new File(FilePath + Globals.FORWARDSLASH);
        dir.mkdir();
        return dir.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".stacktrace");
            }
        });
    }

    private boolean bIsThereAnyErrorFile() {
        return GetErrorFileList().length > 0;
    }

    public void CheckErrorAndSendMail(Context _context) {
        try {
            if (bIsThereAnyErrorFile()) {
                String WholeErrorText = "";
                String[] ErrorFileList = GetErrorFileList();
                int length = ErrorFileList.length;
                int i = 0;
                int curIndex = 0;
                while (i < length) {
                    String curString = ErrorFileList[i];
                    int curIndex2 = curIndex + 1;
                    if (curIndex <= 5) {
                        WholeErrorText = (WholeErrorText + "New Trace collected :\n") + "=====================\n ";
                        BufferedReader input = new BufferedReader(new FileReader(FilePath + Globals.FORWARDSLASH + curString));
                        while (true) {
                            String line = input.readLine();
                            if (line == null) {
                                break;
                            }
                            WholeErrorText = WholeErrorText + line + CSVWriter.DEFAULT_LINE_END;
                        }
                        input.close();
                    }
                    new File(FilePath + Globals.FORWARDSLASH + curString).delete();
                    i++;
                    curIndex = curIndex2;
                }
                SendErrorMail(_context, WholeErrorText);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void CheckErrorAndSendMailCall(Context context) {
        new ErrorReporter().CheckErrorAndSendMail(context);
    }
}
