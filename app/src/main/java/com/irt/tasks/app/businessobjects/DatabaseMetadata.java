package com.irt.tasks.app.businessobjects;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.framework.business.IrtTasksBo;
import java.util.Map;
import org.json.simple.JSONObject;

public class DatabaseMetadata implements IrtTasksBo {
    private Long creationDateMillis;
    private String datatabaseName;
    private String deviceName = Devices.getDeviceName();
    private Long fileSize;
    private Long lastModifiedDateMillis;

    class C17381 extends TypeToken<Map<String, String>> {
        C17381() {
        }
    }

    public String getDatatabaseName() {
        return this.datatabaseName;
    }

    public void setDatatabaseName(String datatabaseName) {
        this.datatabaseName = datatabaseName;
    }

    public Long getLastModifiedDateMillis() {
        return this.lastModifiedDateMillis;
    }

    public void setLastModifiedDateMillis(Long lastModifiedDateMillis) {
        this.lastModifiedDateMillis = lastModifiedDateMillis;
    }

    public Long getCreationDateMillis() {
        return this.creationDateMillis;
    }

    public void setCreationDateMillis(Long creationDateMillis) {
        this.creationDateMillis = creationDateMillis;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Long getFileSize() {
        return this.fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String toJsonString() {
        JSONObject obj = new JSONObject();
        obj.put("datatabaseName", this.datatabaseName);
        obj.put("lastModifiedDateMillis", this.lastModifiedDateMillis);
        obj.put("creationDateMillis", this.creationDateMillis);
        obj.put("fileSize", this.fileSize);
        obj.put("deviceName", this.deviceName);
        return obj.toJSONString();
    }

    public void setObjFromJsonString(String jsonString) {
        Map<String, String> map = (Map) new Gson().fromJson(jsonString, new C17381().getType());
        this.datatabaseName = (String) map.get("datatabaseName");
        this.lastModifiedDateMillis = new Long((String) map.get("lastModifiedDateMillis"));
        this.creationDateMillis = new Long((String) map.get("creationDateMillis"));
        this.fileSize = new Long((String) map.get("fileSize"));
        this.deviceName = (String) map.get("deviceName");
    }

    public boolean isEquals(DatabaseMetadata databaseSyncMetadata) {
        AppUtil.logDebug("Comparing...");
        AppUtil.logDebug("local.. = " + toJsonString());
        AppUtil.logDebug("dropbox = " + databaseSyncMetadata.toJsonString());
        return toJsonString().equals(databaseSyncMetadata.toJsonString());
    }

    public boolean isTimeEquals(DatabaseMetadata databaseSyncMetadata) {
        if (databaseSyncMetadata.getLastModifiedDateMillis() == null) {
            return false;
        }
        AppUtil.logDebug("Comparing...");
        AppUtil.logDebug("local...   modified = " + AppUtil.formatLastDeviceUpdateDate(this.lastModifiedDateMillis.longValue()) + " [" + this.lastModifiedDateMillis + "] created = " + AppUtil.formatLastDeviceUpdateDate(this.creationDateMillis.longValue()) + " [" + this.creationDateMillis + "]");
        AppUtil.logDebug("dropbox... modified = " + AppUtil.formatLastDeviceUpdateDate(databaseSyncMetadata.getLastModifiedDateMillis().longValue()) + " [" + databaseSyncMetadata.getLastModifiedDateMillis() + "] created = " + AppUtil.formatLastDeviceUpdateDate(databaseSyncMetadata.getCreationDateMillis().longValue()) + " [" + databaseSyncMetadata.getCreationDateMillis() + "]");
        if ((this.lastModifiedDateMillis + "").equals(databaseSyncMetadata.getLastModifiedDateMillis() + "") && (this.creationDateMillis + "").equals(databaseSyncMetadata.getCreationDateMillis() + "")) {
            return true;
        }
        return false;
    }
}
