package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.Reminder;
import java.util.List;

public interface ReminderDao {
    List<Reminder> getAll();

    List<Reminder> getAllByTasksListId(Long l);

    void saveOrUpdate(Reminder reminder);
}
