package com.irt.tasks.app.dataaccess.adapter;

public interface TagDBAdapter extends DBAdapter {
    boolean isNameUsed(String str, Long l);
}
