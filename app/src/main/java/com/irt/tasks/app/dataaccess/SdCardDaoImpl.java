package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.ImportExportFile;
import com.irt.tasks.app.common.AppUtil;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SdCardDaoImpl implements FileDao {

    class C17481 implements FileFilter {
        C17481() {
        }

        public boolean accept(File file) {
            return file.isFile();
        }
    }

    public long getFileLastModifiedDateMillis(String remoteRelativeFilePath) {
        return 0;
    }

    public InputStream getFileStream(String remoteFromRelativeFilePath) throws IOException {
        return new FileInputStream(remoteFromRelativeFilePath);
    }

    public void putFileOverwrite(String localFromFilePath, String remoteToRelativeFilePath) throws Throwable {
        Throwable th;
        InputStream localFromInputStream = new FileInputStream(new File(localFromFilePath));
        FileOutputStream to = null;
        try {
            FileOutputStream to2 = new FileOutputStream(remoteToRelativeFilePath);
            try {
                byte[] buffer = new byte[4096];
                while (true) {
                    int bytesRead = localFromInputStream.read(buffer);
                    if (bytesRead == -1) {
                        break;
                    }
                    to2.write(buffer, 0, bytesRead);
                }
                if (localFromInputStream != null) {
                    try {
                        localFromInputStream.close();
                    } catch (IOException e) {
                    }
                }
                if (to2 != null) {
                    try {
                        to2.close();
                    } catch (IOException e2) {
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                to = to2;
                if (localFromInputStream != null) {
                    try {
                        localFromInputStream.close();
                    } catch (IOException e3) {
                    }
                }
                if (to != null) {
                    try {
                        to.close();
                    } catch (IOException e4) {
                    }
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            if (localFromInputStream != null) {
                localFromInputStream.close();
            }
            if (to != null) {
                to.close();
            }
            throw th;
        }
    }

    public List<ImportExportFile> getListOfFiles(String remotDirectory) {
        File bonsaiDirectory = new File(remotDirectory);
        if (!bonsaiDirectory.exists()) {
            File sdCardMainDirectory = new File(AppUtil.getSdCardMainDirectory());
            if (!sdCardMainDirectory.exists()) {
                sdCardMainDirectory.mkdir();
            }
            bonsaiDirectory.mkdir();
        } else if (bonsaiDirectory.isFile()) {
            bonsaiDirectory.delete();
            bonsaiDirectory.mkdir();
        }
        File[] bonsaiFiles = bonsaiDirectory.listFiles(new C17481());
        List<ImportExportFile> remoteFiles = new ArrayList();
        if (bonsaiFiles != null && bonsaiFiles.length > 0) {
            for (File bonsaiFile : bonsaiFiles) {
                ImportExportFile importExportFile = new ImportExportFile();
                importExportFile.setFilename(bonsaiFile.getName());
                importExportFile.setDateMillis(Long.valueOf(bonsaiFile.lastModified()));
                remoteFiles.add(importExportFile);
            }
        }
        return remoteFiles;
    }

    public void closeFile() {
    }

    public boolean setSyncMetadataFileListener() {
        AppUtil.throwDaoNotImplmentedException();
        return false;
    }

    public boolean removeSyncMetadataFileListener() {
        AppUtil.throwDaoNotImplmentedException();
        return false;
    }
}
