package com.irt.tasks.app.businessobjects;

import java.util.Comparator;

public class ImportExportFile {
    private Long dateMillis;
    private String filename;

    public static class FileDateDescendingComparator implements Comparator<ImportExportFile> {
        public int compare(ImportExportFile ieFile1, ImportExportFile ieFile2) {
            long DateMillis1 = ieFile1.getDateMillis().longValue();
            long DateMillis2 = ieFile2.getDateMillis().longValue();
            if (DateMillis1 > DateMillis2) {
                return -1;
            }
            if (DateMillis1 < DateMillis2) {
                return 1;
            }
            return 0;
        }
    }

    public static class FilenameComparator implements Comparator<ImportExportFile> {
        public int compare(ImportExportFile ieFile1, ImportExportFile ieFile2) {
            return ieFile1.getFilename().compareTo(ieFile2.getFilename());
        }
    }

    public String getFilename() {
        return this.filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Long getDateMillis() {
        return this.dateMillis;
    }

    public void setDateMillis(Long dateMillis) {
        this.dateMillis = dateMillis;
    }
}
