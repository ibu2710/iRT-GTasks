package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.Project;
import com.irt.tasks.app.common.Constants.UNIQUE_NAME_ERROR;
import java.util.List;

public interface ProjectBp {
    void delete(Project project);

    Project get(Long l);

    List<Project> getAll();

    UNIQUE_NAME_ERROR isValidTitle(Project project);

    void saveOrUpdate(Project project);
}
