package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.Reminder;
import java.util.List;

public interface ReminderBp {
    List<Reminder> getAll();

    List<Reminder> getAllByTasksListId(Long l);

    void saveOrUpdate(Reminder reminder);
}
