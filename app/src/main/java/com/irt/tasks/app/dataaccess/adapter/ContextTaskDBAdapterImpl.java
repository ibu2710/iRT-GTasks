package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.irt.tasks.app.businessobjects.ContextTask;
import com.irt.tasks.app.framework.business.IrtTasksBo;

public class ContextTaskDBAdapterImpl implements ContextTaskDBAdapter {
    private static final String[] ALL_COLUMNS = new String[]{"context_id", "task_id"};
    public static final String CONTEXT_ID = "context_id";
    public static final String DATABASE_CREATE = "create table context_task (context_id integer not null, task_id integer default 0 not null); ";
    public static final String DATABASE_TABLE = "context_task";
    public static final String INDEX_ON_CONTEXT_ID = "CREATE INDEX idx_context_task_context_id ON context_task (context_id) ";
    public static final String TASK_ID = "task_id";
    private DatabaseHelper DBHelper;
    private final Context context;
    private SQLiteDatabase db;

    public ContextTaskDBAdapterImpl(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(this.context);
    }

    public ContextTaskDBAdapter open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.DBHelper.close();
    }

    public boolean delete(long rowId) {
        throw new RuntimeException("Not yet implemented");
    }

    public int deleteAllbyContextBaseId(Long contextBaseId) {
        return this.db.delete(DATABASE_TABLE, "context_id=" + contextBaseId, null);
    }

    public int deleteAllbyTaskId(Long taskId) {
        return this.db.delete(DATABASE_TABLE, "task_id=" + taskId, null);
    }

    public int deleteAllWhereTaskDoesNotExist() {
        return this.db.delete(DATABASE_TABLE, "task_id not in (select " + TaskDBAdapterHelper.getTaskRawColumn("_id") + " from " + "task" + ") ", null);
    }

    public Cursor get(long rowId) throws SQLException {
        throw new RuntimeException("Not yet implemented");
    }

    public Cursor getAll() {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, null, null, null, null, null);
    }

    public Cursor getAllByContextBaseId(Long contextBaseId) {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, "context_id=" + contextBaseId, null, null, null, null);
    }

    public Cursor getAllByTaskId(Long taskId) {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, "task_id=" + taskId, null, null, null, null);
    }

    public Cursor getAllDistinctContextBasesIds() {
        return this.db.rawQuery("SELECT DISTINCT context_id from context_task", null);
    }

    public long insert(IrtTasksBo businessObject) {
        return this.db.insert(DATABASE_TABLE, null, populateBusinessObjectContentValues((ContextTask) businessObject));
    }

    public boolean update(IrtTasksBo businessObject) {
        throw new RuntimeException("Not yet implemented");
    }

    private ContentValues populateBusinessObjectContentValues(ContextTask contextTask) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("context_id", contextTask.getContextId());
        contentValues.put("task_id", contextTask.getTaskId());
        return contentValues;
    }
}
