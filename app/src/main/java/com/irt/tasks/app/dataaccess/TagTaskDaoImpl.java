package com.irt.tasks.app.dataaccess;

import android.database.Cursor;
import com.irt.tasks.app.businessobjects.TagTask;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.dataaccess.adapter.TagTaskDBAdapter;
import com.irt.tasks.app.dataaccess.adapter.TagTaskDBAdapterImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.ArrayList;
import java.util.List;

public class TagTaskDaoImpl implements TagTaskDao {
    private ConfigManager config;

    public List<TagTask> getAllByTagId(Long tagId) {
        TagTaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllByTagId(tagId);
        List<TagTask> tagTasks = buildContextTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tagTasks;
    }

    public List<TagTask> getAllTagTasksByTaskId(Long taskId) {
        TagTaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllByTaskId(taskId);
        List<TagTask> tagTasks = buildContextTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tagTasks;
    }

    public int deleteAllByTagId(Long tagId) {
        TagTaskDBAdapter dBAdapter = openDbAdapter();
        int deletes = dBAdapter.deleteAllbyTagId(tagId);
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
        return deletes;
    }

    public int deleteAllByTaskId(Long taskId) {
        TagTaskDBAdapter dBAdapter = openDbAdapter();
        int deletes = dBAdapter.deleteAllbyTaskId(taskId);
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
        return deletes;
    }

    public int deleteAllWhereTaskDoesNotExist() {
        TagTaskDBAdapter dBAdapter = openDbAdapter();
        int deletes = dBAdapter.deleteAllWhereTaskDoesNotExist();
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
        return deletes;
    }

    public void save(TagTask tagTask) {
        TagTaskDBAdapter dBAdapter = openDbAdapter();
        dBAdapter.insert(tagTask);
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    public List<Long> getAllDistinctTagIds() {
        TagTaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllDistinctTagIds();
        List<Long> contextBasesIds = new ArrayList();
        while (cursor.moveToNext()) {
            contextBasesIds.add(Long.valueOf(cursor.getLong(0)));
        }
        cursor.close();
        closeDbAdapter(dBAdapter);
        return contextBasesIds;
    }

    private List<TagTask> buildContextTasks(Cursor cursor) {
        List<TagTask> tagTasks = new ArrayList();
        while (cursor.moveToNext()) {
            tagTasks.add(buildContextTask(cursor));
        }
        return tagTasks;
    }

    private TagTask buildContextTask(Cursor cursor) {
        Long tagId = Long.valueOf(cursor.getLong(0));
        Long taskId = Long.valueOf(cursor.getLong(1));
        TagTask tagTask = new TagTask(tagId,taskId);
        tagTask.setTagId(tagId);
        tagTask.setTaskId(taskId);
        return tagTask;
    }

    private TagTaskDBAdapter openDbAdapter() {
        this.config = ConfigManager.getInstance();
        TagTaskDBAdapter dBAdapter = new TagTaskDBAdapterImpl(this.config.getContext());
        dBAdapter.open();
        return dBAdapter;
    }

    private void closeDbAdapter(TagTaskDBAdapter dBAdapter) {
        dBAdapter.close();
    }
}
