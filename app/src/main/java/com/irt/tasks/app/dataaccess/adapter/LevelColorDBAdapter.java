package com.irt.tasks.app.dataaccess.adapter;

import android.database.Cursor;

public interface LevelColorDBAdapter extends DBAdapter {
    int deleteAllbyTasksListId(Long l);

    Cursor getAllByTasksListId(Long l);

    Cursor getAllDistinctTasksListIds();
}
