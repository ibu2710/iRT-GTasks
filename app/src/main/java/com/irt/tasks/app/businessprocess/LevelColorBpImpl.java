package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.LevelColor;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.dataaccess.LevelColorDao;
import com.irt.tasks.app.dataaccess.LevelColorDaoImpl;
import com.irt.tasks.app.dataaccess.TasksListDao;
import com.irt.tasks.app.dataaccess.TasksListDaoImpl;
import java.util.ArrayList;
import java.util.List;

public class LevelColorBpImpl implements LevelColorBp {
    private LevelColorDao levelColorDao = new LevelColorDaoImpl();
    private TasksListBp tasksListBp = new TasksListBpImpl();
    private TasksListDao tasksListDao = new TasksListDaoImpl();

    public List<LevelColor> getAllByTasksListId(Long tasksListId) {
        return this.levelColorDao.getAllByTasksListId(tasksListId);
    }

    public void saveOrUpdate(LevelColor levelColor) {
        this.levelColorDao.saveOrUpdate(levelColor);
    }

    public void deleteAllbyTasksListId(Long tasksListId) {
        this.levelColorDao.deleteAllbyTasksListId(tasksListId);
    }

    public void createLevelColorsForTasksList(Long tasksListId) {
        if (this.levelColorDao.getAllByTasksListId(tasksListId).size() < 1) {
            for (int i = 0; i < 20; i++) {
                LevelColor levelColor = new LevelColor();
                levelColor.setTasksListId(tasksListId);
                levelColor.setLevel(Integer.valueOf(i + 1));
                this.levelColorDao.saveOrUpdate(levelColor);
            }
        }
    }

    public List<LevelColor> copyLevelColorFromTasksLists(Long sourceTasksListId, Long targetTasksListId) {
        List<LevelColor> sourceLevelColors = this.levelColorDao.getAllByTasksListId(sourceTasksListId);
        List<LevelColor> targetLevelColors = this.levelColorDao.getAllByTasksListId(targetTasksListId);
        for (int i = 0; i < 20; i++) {
            ((LevelColor) targetLevelColors.get(i)).setFontColor(((LevelColor) sourceLevelColors.get(i)).getFontColor());
            ((LevelColor) targetLevelColors.get(i)).setFontStyle(((LevelColor) sourceLevelColors.get(i)).getFontStyle());
        }
        return targetLevelColors;
    }

    public List<TasksList> getAllLevelColorTasksLists() {
        List<Long> tasksListIds = this.levelColorDao.getAllDistinctTasksListIds();
        List<TasksList> sortedTasksLists = this.tasksListBp.getAllTasksListBySortOrder();
        List<TasksList> tasksLists = new ArrayList();
        for (TasksList tasksList : sortedTasksLists) {
            if (isTasksListHasColor(tasksList.getId(), tasksListIds)) {
                tasksLists.add(tasksList);
            }
        }
        return tasksLists;
    }

    private boolean isTasksListHasColor(Long tasksListId, List<Long> tasksListIds) {
        for (Long tasksListIdWithColor : tasksListIds) {
            if (tasksListId.equals(tasksListIdWithColor)) {
                return true;
            }
        }
        return false;
    }
}
