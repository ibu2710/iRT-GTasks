package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.Project;
import com.irt.tasks.app.common.Constants.UNIQUE_NAME_ERROR;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.ProjectDao;
import com.irt.tasks.app.dataaccess.ProjectDaoImpl;
import java.util.List;

public class ProjectBpImpl implements ProjectBp {
    ProjectDao projectDao = new ProjectDaoImpl();

    public List<Project> getAll() {
        return this.projectDao.getAll();
    }

    public void saveOrUpdate(Project project) {
        this.projectDao.saveOrUpdate(project);
    }

    public void delete(Project project) {
        this.projectDao.delete(project);
    }

    public Project get(Long projectId) {
        return this.projectDao.get(projectId);
    }

    public UNIQUE_NAME_ERROR isValidTitle(Project project) {
        if (StringUtil.isNullOrEmpty(project.getTitle())) {
            return UNIQUE_NAME_ERROR.BLANK;
        }
        if (this.projectDao.isTitleUsed(project.getTitle(), project.getId())) {
            return UNIQUE_NAME_ERROR.DUPLICATE;
        }
        return UNIQUE_NAME_ERROR.NO_ERROR;
    }
}
