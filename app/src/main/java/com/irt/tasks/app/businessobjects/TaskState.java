package com.irt.tasks.app.businessobjects;

import com.irt.tasks.app.framework.business.IrtTasksBo;

public class TaskState implements IrtTasksBo {
    public static final String TASKS_LIST_ID = "tasksListId";
    public static final String TASK_STR_ID = "taskStrId";
    public static final String TYPE = "type";
    private String taskStrId;
    private Long tasksListId;
    private Long type;

    public Long getTasksListId() {
        return this.tasksListId;
    }

    public void setTasksListId(Long tasksListId) {
        this.tasksListId = tasksListId;
    }

    public String getTaskStrId() {
        return this.taskStrId;
    }

    public void setTaskStrId(String taskStrId) {
        this.taskStrId = taskStrId;
    }

    public Long getType() {
        return this.type;
    }

    public void setType(Long type) {
        this.type = type;
    }
}
