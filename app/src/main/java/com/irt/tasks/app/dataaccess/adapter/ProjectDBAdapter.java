package com.irt.tasks.app.dataaccess.adapter;

public interface ProjectDBAdapter extends DBAdapter {
    boolean isTitleUsed(String str, Long l);
}
