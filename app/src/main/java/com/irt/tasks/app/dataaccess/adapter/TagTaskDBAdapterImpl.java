package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.irt.tasks.app.businessobjects.TagTask;
import com.irt.tasks.app.framework.business.IrtTasksBo;

public class TagTaskDBAdapterImpl implements TagTaskDBAdapter {
    public static final String DATABASE_CREATE = "create table tag_task (tag_id integer not null, task_id integer default 0 not null); ";
    public static final String DATABASE_TABLE = "tag_task";
    public static final String INDEX_ON_TAG_ID = "CREATE INDEX idx_tag_task_tag_id ON tag_task (tag_id) ";
    public static final String TAG_ID = "tag_id";
    public static final String TASK_ID = "task_id";
    private DatabaseHelper DBHelper;
    private static final String[] ALL_COLUMNS = new String[]{TAG_ID, "task_id"};
    private final Context context;
    private SQLiteDatabase db;

    public TagTaskDBAdapterImpl(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(this.context);
    }

    public TagTaskDBAdapter open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.DBHelper.close();
    }

    public boolean delete(long rowId) {
        throw new RuntimeException("Not yet implemented");
    }

    public int deleteAllbyTagId(Long tagId) {
        return this.db.delete(DATABASE_TABLE, "tag_id=" + tagId, null);
    }

    public int deleteAllbyTaskId(Long taskId) {
        return this.db.delete(DATABASE_TABLE, "task_id=" + taskId, null);
    }

    public int deleteAllWhereTaskDoesNotExist() {
        return this.db.delete(DATABASE_TABLE, "task_id not in (select " + TaskDBAdapterHelper.getTaskRawColumn("_id") + " from " + "task" + ") ", null);
    }

    public Cursor get(long rowId) throws SQLException {
        throw new RuntimeException("Not yet implemented");
    }

    public Cursor getAll() {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, null, null, null, null, null);
    }

    public Cursor getAllByTagId(Long tagId) {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, "tag_id=" + tagId, null, null, null, null);
    }

    public Cursor getAllByTaskId(Long taskId) {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, "task_id=" + taskId, null, null, null, null);
    }

    public Cursor getAllDistinctTagIds() {
        return this.db.rawQuery("SELECT DISTINCT tag_id from tag_task", null);
    }

    public long insert(IrtTasksBo businessObject) {
        return this.db.insert(DATABASE_TABLE, null, populateBusinessObjectContentValues((TagTask) businessObject));
    }

    public boolean update(IrtTasksBo businessObject) {
        throw new RuntimeException("Not yet implemented");
    }

    private ContentValues populateBusinessObjectContentValues(TagTask tagTask) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TAG_ID, tagTask.getTagId());
        contentValues.put("task_id", tagTask.getTaskId());
        return contentValues;
    }
}
