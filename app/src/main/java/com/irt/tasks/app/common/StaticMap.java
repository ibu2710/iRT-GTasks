package com.irt.tasks.app.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class StaticMap extends HashMap implements Serializable {
    public StaticMap(int initialCapacity) {
        super(initialCapacity);
    }

    public StaticMap(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public StaticMap(Map t) {
        super(t);
    }

    public Object put(Object key, Object value) {
        if (!containsKey(key)) {
            return super.put(key, value);
        }
        throw new AssertFailedException("Duplicate keys not allowed. Key already exists: " + key);
    }

    public void putAll(Map t) {
        throw new UnsupportedOperationException("Method not yet implemented. Try back later.");
    }

    public Object clone() {
        return super.clone();
    }
}
