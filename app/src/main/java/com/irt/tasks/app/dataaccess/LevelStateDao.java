package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.LevelState;

public interface LevelStateDao {
    LevelState getLevelStateByTasksListId(Long l);

    void saveOrUpdate(LevelState levelState);
}
