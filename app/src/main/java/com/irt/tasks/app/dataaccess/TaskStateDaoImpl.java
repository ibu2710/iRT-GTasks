package com.irt.tasks.app.dataaccess;

import android.database.Cursor;
import com.irt.tasks.app.businessobjects.TaskState;
import com.irt.tasks.app.dataaccess.adapter.TaskStateDBAdapter;
import com.irt.tasks.app.dataaccess.adapter.TaskStateDBAdapterImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.ArrayList;
import java.util.List;

public class TaskStateDaoImpl implements TaskStateDao {
    ConfigManager config;

    public List<TaskState> getAll() {
        TaskStateDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAll();
        List<TaskState> taskStates = buildTaskStates(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return taskStates;
    }

    public void save(TaskState taskState) {
        TaskStateDBAdapter dBAdapter = openDbAdapter();
        dBAdapter.insert(taskState);
        closeDbAdapter(dBAdapter);
    }

    public List<TaskState> getAllByTasksListId(Long tasksListId) {
        TaskStateDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllByTasksListId(tasksListId);
        List<TaskState> taskStates = buildTaskStates(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return taskStates;
    }

    public int deleteAllByTasksListId(Long tasksListId) {
        TaskStateDBAdapter dBAdapter = openDbAdapter();
        int totalDeletes = dBAdapter.deleteAllByTasksListId(tasksListId);
        closeDbAdapter(dBAdapter);
        return totalDeletes;
    }

    private List<TaskState> buildTaskStates(Cursor cursor) {
        List<TaskState> taskStates = new ArrayList();
        while (cursor.moveToNext()) {
            taskStates.add(buildTaskState(cursor));
        }
        return taskStates;
    }

    private TaskState buildTaskState(Cursor cursor) {
        Long tasksListId = Long.valueOf(cursor.getLong(0));
        String taskStrId = cursor.getString(1);
        Long type = Long.valueOf(cursor.getLong(2));
        TaskState taskState = new TaskState();
        taskState.setTasksListId(tasksListId);
        taskState.setTaskStrId(taskStrId);
        taskState.setType(type);
        return taskState;
    }

    private TaskStateDBAdapter openDbAdapter() {
        this.config = ConfigManager.getInstance();
        TaskStateDBAdapter dBAdapter = new TaskStateDBAdapterImpl(this.config.getContext());
        dBAdapter.open();
        return dBAdapter;
    }

    private void closeDbAdapter(TaskStateDBAdapter dBAdapter) {
        dBAdapter.close();
    }
}
