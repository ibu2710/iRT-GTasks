package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.LevelState;
import com.irt.tasks.app.businessobjects.TaskState;
import com.irt.tasks.app.dataaccess.LevelStateDao;
import com.irt.tasks.app.dataaccess.LevelStateDaoImpl;
import com.irt.tasks.app.dataaccess.TaskStateDao;
import com.irt.tasks.app.dataaccess.TaskStateDaoImpl;
import com.irt.tasks.app.dataaccess.TasksListDao;
import com.irt.tasks.app.dataaccess.TasksListDaoImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.HashMap;
import java.util.Map;

public class TaskStateBpImpl implements TaskStateBp {
    ConfigManager config;
    LevelStateDao levelStateDao = new LevelStateDaoImpl();
    TaskStateDao taskStateDao = new TaskStateDaoImpl();
    TasksListDao tasksListDao = new TasksListDaoImpl();

    public Map<String, Long> getTaskStateMapByTasksListId(Long tasksListId) {
        this.config = ConfigManager.getInstance();
        Map<String, Long> taskStateMap = this.config.getTaskStateMapByTasksListId(tasksListId);
        if (taskStateMap == null) {
            taskStateMap = new HashMap();
            for (TaskState taskState : this.taskStateDao.getAllByTasksListId(tasksListId)) {
                taskStateMap.put(taskState.getTaskStrId(), taskState.getType());
            }
            this.config.addTaskStateMap(tasksListId, taskStateMap);
        }
        return taskStateMap;
    }

    public void saveTaskStates(Long tasksListId) {
        this.config = ConfigManager.getInstance();
        this.taskStateDao.deleteAllByTasksListId(tasksListId);
        Map<String, Long> taskStateMap = this.config.getTaskStateMapByTasksListId(tasksListId);
        if (taskStateMap != null) {
            for (String taskIdStr : taskStateMap.keySet()) {
                Long type = (Long) taskStateMap.get(taskIdStr);
                TaskState taskState = new TaskState();
                taskState.setTasksListId(tasksListId);
                taskState.setTaskStrId(taskIdStr);
                taskState.setType(type);
                this.taskStateDao.save(taskState);
            }
        }
    }

    public LevelState getLevelSate(Long tasksListId, String zoomTaskIdStr) {
        this.config = ConfigManager.getInstance();
        LevelState levelState = this.config.getLevelState(tasksListId);
        if (levelState == null) {
            levelState = this.levelStateDao.getLevelStateByTasksListId(tasksListId);
            if (levelState == null) {
                levelState = new LevelState();
                levelState.setTasksListId(tasksListId);
                levelState.setLevel(Integer.valueOf(1));
                levelState.setZoomTaskIdStr(zoomTaskIdStr);
                levelState.setPosition(Integer.valueOf(0));
            }
            this.config.addLevelState(levelState);
        }
        return levelState;
    }

    public void saveOrUpdateLevelState(LevelState levelState) {
        this.levelStateDao.saveOrUpdate(levelState);
    }
}
