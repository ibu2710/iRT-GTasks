package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.Tag;
import com.irt.tasks.app.businessobjects.TagTask;
import com.irt.tasks.app.common.Constants.UNIQUE_NAME_ERROR;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.TagDao;
import com.irt.tasks.app.dataaccess.TagDaoImpl;
import com.irt.tasks.app.dataaccess.TagTaskDao;
import com.irt.tasks.app.dataaccess.TagTaskDaoImpl;
import java.util.List;

public class TagBpImpl implements TagBp {
    TagDao tagDao = new TagDaoImpl();
    TagTaskDao tagTaskDao = new TagTaskDaoImpl();

    public List<Tag> getAllTags() {
        return this.tagDao.getAll();
    }

    public void saveOrUpdateTag(Tag tag) {
        this.tagDao.saveOrUpdate(tag);
    }

    public void deleteTag(Tag tag) {
        this.tagDao.delete(tag);
    }

    public List<TagTask> getAllTagTasksByTagId(Long tagId) {
        return this.tagTaskDao.getAllByTagId(tagId);
    }

    public int deleteAllTagTasksByTagId(Long tagId) {
        return this.tagTaskDao.deleteAllByTagId(tagId);
    }

    public List<Long> getAllDistinctTagIdsForTagTasks() {
        return this.tagTaskDao.getAllDistinctTagIds();
    }

    public int deleteAllTagTasksByTaskId(Long taskId) {
        return this.tagTaskDao.deleteAllByTaskId(taskId);
    }

    public void saveTagTask(TagTask tagTask) {
        this.tagTaskDao.save(tagTask);
    }

    public UNIQUE_NAME_ERROR isValidTagName(Tag tag) {
        if (StringUtil.isNullOrEmpty(tag.getName())) {
            return UNIQUE_NAME_ERROR.BLANK;
        }
        if (this.tagDao.isNameUsed(tag.getName(), tag.getId())) {
            return UNIQUE_NAME_ERROR.DUPLICATE;
        }
        return UNIQUE_NAME_ERROR.NO_ERROR;
    }

    public List<TagTask> getAllTagTasksByTaskId(Long taskId) {
        return this.tagTaskDao.getAllTagTasksByTaskId(taskId);
    }
}
