package com.irt.tasks.app.dataaccess.adapter;

import android.database.Cursor;

public interface TaskDBAdapter extends DBAdapter {
    int countTasksByTasksListId(Long l, boolean z);

    int deleteTasksInTrashBin();

    int forceDeleteByTaskListId(Long l);

    Cursor getAllTasksByTasksListId(Long l, String str, boolean z, boolean z2, boolean z3);

    Cursor getAllTasksByTasksListIdWithDeleted(Long l);

    Cursor getAllTasksDeletedOrTrashBinByTasksListId(Long l, boolean z, boolean z2);

    Cursor getAllTasksWithEligibleAlarms();

    Cursor getAllTasksWithEligibleAlarmsIncludingInvisibleAccounts();

    Cursor getAllVisibleTasks(String str, boolean z, boolean z2, boolean z3, boolean z4);

    Cursor getAllVisibleTasksDeletedOrTashBinOnly(boolean z, boolean z2, boolean z3);

    Cursor getFilteredVisibleTasks(Long l);

    Cursor getQuickAllVisibleTasks(String str, String str2);

    Cursor getTasksMarkedForDeletionButNotInTrashBin(Long l);

    boolean resetTasksProject(Long l);

    Cursor searchAllExcludeDeleteAndTrashBin(String str);
}
