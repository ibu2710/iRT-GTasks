package com.irt.tasks.app.businessobjects;

import com.irt.tasks.app.framework.business.IrtTasksBo;

public class Email implements IrtTasksBo {
    private String email;
    private String extraEmail;
    private Long id;
    private String name;
    private String password;
    private String signature = "\n\nSent from my iRT GTasks Outliner";

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getExtraEmail() {
        return this.extraEmail;
    }

    public void setExtraEmail(String extraEmail) {
        this.extraEmail = extraEmail;
    }

    public String getSignature() {
        return this.signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
