package com.irt.tasks.app.common;

public final class ElapsedTime {
    public static final long DAY_IN_MILLIS = 86400000;
    public static final long HOUR_IN_MILLIS = 3600000;
    public static final long MINUTE_IN_MILLIS = 60000;
    public static final int PRECISION_DAYS = 1;
    public static final int PRECISION_DEFAULT = 0;
    public static final int PRECISION_HOURS = 2;
    public static final int PRECISION_MILLIS = 5;
    public static final int PRECISION_MINUTES = 3;
    public static final int PRECISION_SECONDS = 4;
    public static final int PRECISION_WEEKS = 0;
    public static final long SECOND_IN_MILLIS = 1000;
    public static final long WEEK_IN_MILLIS = 604800000;
    private long numDays;
    private long numHours;
    private long numMillis;
    private long numMinutes;
    private long numSeconds;
    private long numWeeks;
    private long wholeTime;

    public ElapsedTime(long elapsedTime) {
        this(elapsedTime, 0);
    }

    public ElapsedTime(long elapsedTime, int precision) {
        this.numWeeks = 0;
        this.numDays = 0;
        this.numHours = 0;
        this.numMinutes = 0;
        this.numSeconds = 0;
        this.numMillis = 0;
        initialize(elapsedTime, precision);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void initialize(long r6, int r8) {
        /*
        r5 = this;
        r5.wholeTime = r6;
        r0 = r6;
        switch(r8) {
            case 0: goto L_0x001f;
            case 1: goto L_0x002a;
            case 2: goto L_0x0035;
            case 3: goto L_0x0040;
            case 4: goto L_0x004b;
            case 5: goto L_0x0054;
            default: goto L_0x0006;
        };
    L_0x0006:
        r2 = new com.irt.tasks.app.common.AssertFailedException;
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r4 = "Precision not valid: ";
        r3 = r3.append(r4);
        r3 = r3.append(r8);
        r3 = r3.toString();
        r2.<init>(r3);
        throw r2;
    L_0x001f:
        r2 = 604800000; // 0x240c8400 float:3.046947E-17 double:2.988109026E-315;
        r2 = r0 / r2;
        r5.numWeeks = r2;
        r2 = 604800000; // 0x240c8400 float:3.046947E-17 double:2.988109026E-315;
        r0 = r0 % r2;
    L_0x002a:
        r2 = 86400000; // 0x5265c00 float:7.82218E-36 double:4.2687272E-316;
        r2 = r0 / r2;
        r5.numDays = r2;
        r2 = 86400000; // 0x5265c00 float:7.82218E-36 double:4.2687272E-316;
        r0 = r0 % r2;
    L_0x0035:
        r2 = 3600000; // 0x36ee80 float:5.044674E-39 double:1.7786363E-317;
        r2 = r0 / r2;
        r5.numHours = r2;
        r2 = 3600000; // 0x36ee80 float:5.044674E-39 double:1.7786363E-317;
        r0 = r0 % r2;
    L_0x0040:
        r2 = 60000; // 0xea60 float:8.4078E-41 double:2.9644E-319;
        r2 = r0 / r2;
        r5.numMinutes = r2;
        r2 = 60000; // 0xea60 float:8.4078E-41 double:2.9644E-319;
        r0 = r0 % r2;
    L_0x004b:
        r2 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        r2 = r0 / r2;
        r5.numSeconds = r2;
        r2 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        r0 = r0 % r2;
    L_0x0054:
        r5.numMillis = r0;
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.irt.tasks.app.common.ElapsedTime.initialize(long, int):void");
    }

    public long getNumDays() {
        return this.numDays;
    }

    public long getNumHours() {
        return this.numHours;
    }

    public long getNumMillis() {
        return this.numMillis;
    }

    public long getNumMinutes() {
        return this.numMinutes;
    }

    public long getNumSeconds() {
        return this.numSeconds;
    }

    public long getNumWeeks() {
        return this.numWeeks;
    }

    public long getWholeTime() {
        return this.wholeTime;
    }
}
