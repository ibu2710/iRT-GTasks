package com.irt.tasks.app.dataaccess.adapter;

import android.database.Cursor;

public interface ReminderDBAdapter extends DBAdapter {
    Cursor getAllByTasksListId(Long l);
}
