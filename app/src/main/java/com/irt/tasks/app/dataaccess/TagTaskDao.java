package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.TagTask;
import java.util.List;

public interface TagTaskDao {
    int deleteAllByTagId(Long l);

    int deleteAllByTaskId(Long l);

    int deleteAllWhereTaskDoesNotExist();

    List<TagTask> getAllByTagId(Long l);

    List<Long> getAllDistinctTagIds();

    List<TagTask> getAllTagTasksByTaskId(Long l);

    void save(TagTask tagTask);
}
