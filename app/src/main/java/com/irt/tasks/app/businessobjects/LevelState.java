package com.irt.tasks.app.businessobjects;

import com.irt.tasks.app.framework.business.IrtTasksBo;

public class LevelState implements IrtTasksBo {
    public static final String LEVEL = "level";
    public static final String POSITION = "position";
    public static final String TASKS_LIST_ID = "tasksListId";
    public static final String ZOOM_TASK = "zoomTask";
    public static final String ZOOM_TASK_STR_ID = "zoomTaskIdStr";
    private Integer level;
    private Integer position = Integer.valueOf(0);
    private Long tasksListId;
    private Task zoomTask;
    private String zoomTaskIdStr;

    public Long getTasksListId() {
        return this.tasksListId;
    }

    public void setTasksListId(Long tasksListId) {
        this.tasksListId = tasksListId;
    }

    public String getZoomTaskIdStr() {
        return this.zoomTaskIdStr;
    }

    public void setZoomTaskIdStr(String zoomTaskIdStr) {
        this.zoomTaskIdStr = zoomTaskIdStr;
    }

    public Integer getLevel() {
        return this.level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getPosition() {
        return this.position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Task getZoomTask() {
        return this.zoomTask;
    }

    public void setZoomTask(Task zoomTask) {
        this.zoomTask = zoomTask;
    }
}
