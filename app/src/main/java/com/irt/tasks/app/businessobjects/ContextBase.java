package com.irt.tasks.app.businessobjects;

import com.irt.tasks.app.framework.business.IrtTasksBo;
import java.util.List;

public class ContextBase implements IrtTasksBo {
    public static final String NAME = "name";
    public static final String ROW_ID = "id";
    private List<ContextIncludes> contextIncludess;
    private Long id;
    private String name;
    private boolean selected = false;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return this.selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public List<ContextIncludes> getContextIncludess() {
        return this.contextIncludess;
    }

    public void setContextIncludess(List<ContextIncludes> contextIncludess) {
        this.contextIncludess = contextIncludess;
    }
}
