package com.irt.tasks.app.framework.security;

import android.os.Build.VERSION;
import com.google.android.gms.cast.Cast;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

public class AndroidCrypto {
    private static final String HEX = "0123456789ABCDEF";

    public static String encrypt(String seed, String cleartext) throws Exception {
        return toHex(encrypt(getRawKey(seed.getBytes(), false), cleartext.getBytes()));
    }

    public static String decrypt(String seed, String encrypted, boolean isOverrideRawKey) throws Exception {
        return new String(decrypt(getRawKey(seed.getBytes(), isOverrideRawKey), toByte(encrypted)));
    }

    private static byte[] getRawKey(byte[] seed, boolean isOverrideRawKey) throws Exception {
        SecureRandom sr;
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        if (VERSION.SDK_INT >= 17) {
            sr = SecureRandom.getInstance("SHA1PRNG", "Crypto");
        } else {
            sr = SecureRandom.getInstance("SHA1PRNG");
        }
        sr.setSeed(seed);
        kgen.init(Cast.MAX_NAMESPACE_LENGTH, sr);
        byte[] raw = kgen.generateKey().getEncoded();
        if (!isOverrideRawKey) {
            return raw;
        }
        if (SecurityKey.ENCRYPTION_RAW_KEY_2_2.equals(toHex(raw))) {
            return toByte(SecurityKey.ENCRYPTION_RAW_KEY_2_3);
        }
        return toByte(SecurityKey.ENCRYPTION_RAW_KEY_2_2);
    }

    private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(1, skeySpec);
        return cipher.doFinal(clear);
    }

    private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(2, skeySpec);
        return cipher.doFinal(encrypted);
    }

    public static String toHex(String txt) {
        return toHex(txt.getBytes());
    }

    public static String fromHex(String hex) {
        return new String(toByte(hex));
    }

    public static byte[] toByte(String hexString) {
        int len = hexString.length() / 2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++) {
            result[i] = Integer.valueOf(hexString.substring(i * 2, (i * 2) + 2), 16).byteValue();
        }
        return result;
    }

    public static String toHex(byte[] buf) {
        if (buf == null) {
            return "";
        }
        StringBuffer result = new StringBuffer(buf.length * 2);
        for (byte appendHex : buf) {
            appendHex(result, appendHex);
        }
        return result.toString();
    }

    private static void appendHex(StringBuffer sb, byte b) {
        sb.append(HEX.charAt((b >> 4) & 15)).append(HEX.charAt(b & 15));
    }
}
