package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.ContextIncludesDao;
import com.irt.tasks.app.dataaccess.ContextIncludesDaoImpl;
import com.irt.tasks.app.framework.business.IrtTasksBo;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.filter.TasksFilter;
import com.irt.tasks.ui.filter.TasksFilterManager;
import java.util.List;

public class TaskDBAdapterImpl implements TaskDBAdapter {
    public static final String AUTO_SNOOZE_DURATION = "auto_snooze_duration";
    public static final String COMPLETED = "completed";
    public static final String COMPLETION_DATE = "completion_date";
    public static final String CREATION_DATE = "creation_date";
    public static final String DATABASE_CREATE = "create table task (_id integer primary key autoincrement, gtask_id text null, tasks_list_id integer not null, parent_task_id default 0 not null, prior_sibling_task_id default 0 not null, name text null, note text null, due_date integer default 0 not null, last_modified integer default 0 not null, deleted integer default 0 not null, completed integer default 0 not null, inTrashBin integer default 0 not null, moved integer default 0 not null, highlight integer default 0 not null, fontStyle integer default 0 not null, fontColor integer default 0 not null, fontBgColor integer default 0 not null, sync_time integer default 0 not null, show_note integer default 0 not null, creation_date integer default 0 not null, last_content_modified_date integer default 0 not null, priority integer default 1 not null,\treminder_enabled integer default 0 not null, reminder_days_earlier integer default 0 not null, reminder_date_time integer default 0 not null, reminder_repeat_from_completion_date integer default 0 not null, reminder_repeat_rule text null, notify_by_email integer default 0 not null, notify_extra_emails integer default 0 not null, extra_emails text null, is_float integer default 0 not null, exclude_from_delete integer default 0 not null, icon_name text null, snooze_date_time integer default 0 not null, auto_snooze_duration integer default 1 not null, completion_date integer default 0 not null, starred integer default 0 not null, projectId integer default 0 not null, status integer default 0 not null ); ";
    public static final String DATABASE_TABLE = "task";
    public static final String DELETED = "deleted";
    public static final String DUE_DATE = "due_date";
    public static final String EXCLUDE_FROM_DELETE = "exclude_from_delete";
    public static final String EXTRA_EMAILS = "extra_emails";
    public static final String FONT_BACKGROUND_COLOR = "fontBgColor";
    public static final String FONT_COLOR = "fontColor";
    public static final String FONT_STYLE = "fontStyle";
    public static final String GTASK_ID = "gtask_id";
    public static final String HIGHLIGHT = "highlight";
    public static final String ICON_NAME = "icon_name";
    public static final String INDEX_ON_DELETED = "CREATE INDEX idx_task_deleted ON task (deleted) ";
    public static final String INDEX_ON_TASK_LIST_ID = "CREATE INDEX idx_task_tasks_list_id ON task (tasks_list_id) ";
    public static final String INDEX_ON_TRASH_BIN = "CREATE INDEX idx_task_inTrashBin ON task (inTrashBin) ";
    public static final String IN_TRASH_BIN = "inTrashBin";
    public static final String IS_FLOAT = "is_float";
    public static final String LAST_CONTENT_MODIFIED_DATE = "last_content_modified_date";
    public static final String LAST_MODIFIED = "last_modified";
    public static final String MOVED = "moved";
    public static final String NAME = "name";
    public static final String NOTE = "note";
    public static final String NOTIFY_BY_EMAIL = "notify_by_email";
    public static final String NOTIFY_EXTRA_EMAILS = "notify_extra_emails";
    public static final String PARENT_TASK_ID = "parent_task_id";
    public static final String PRIORITY = "priority";
    public static final String PRIOR_SIBLING_TASK_ID = "prior_sibling_task_id";
    public static final String PROJECT_ID = "projectId";
    public static final String REMINDER_DATE_TIME = "reminder_date_time";
    public static final String REMINDER_DAYS_EARLIER = "reminder_days_earlier";
    public static final String REMINDER_ENABLED = "reminder_enabled";
    public static final String REMINDER_REPEAT_FROM_COMPLETION_DATE = "reminder_repeat_from_completion_date";
    public static final String REMINDER_REPEAT_RULE = "reminder_repeat_rule";
    public static final String ROW_ID = "_id";
    public static final String SHOW_NOTE = "show_note";
    public static final String SNOOZE_DATE_TIME = "snooze_date_time";
    public static final String STARRED = "starred";
    public static final String STATUS = "status";
    public static final String SYNC_TIME = "sync_time";
    public static final String TASKS_LIST_ID = "tasks_list_id";
    public static final String[] ALL_COLUMNS = new String[]{"_id", GTASK_ID, "tasks_list_id", PARENT_TASK_ID, PRIOR_SIBLING_TASK_ID, "name", "note", DUE_DATE, "last_modified", "deleted", "completed", "inTrashBin", "moved", "highlight", "fontStyle", "fontColor", "fontBgColor", SYNC_TIME, "show_note", CREATION_DATE, LAST_CONTENT_MODIFIED_DATE, "priority", REMINDER_ENABLED, REMINDER_DAYS_EARLIER, REMINDER_DATE_TIME, REMINDER_REPEAT_FROM_COMPLETION_DATE, REMINDER_REPEAT_RULE, NOTIFY_BY_EMAIL, NOTIFY_EXTRA_EMAILS, "extra_emails", IS_FLOAT, "exclude_from_delete", ICON_NAME, SNOOZE_DATE_TIME, AUTO_SNOOZE_DURATION, COMPLETION_DATE, "starred", "projectId", "status"};
    private DatabaseHelper DBHelper;
    private ConfigManager config;
    private final Context context;
    ContextIncludesDao contextIncludesDao = new ContextIncludesDaoImpl();
    private SQLiteDatabase db;
    boolean mIsContextIncludesAvailable = false;

    public TaskDBAdapterImpl(Context ctx) {
        this.context = ctx;
        this.DBHelper = new DatabaseHelper(this.context);
    }

    public TaskDBAdapter open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.DBHelper.close();
    }

    public boolean delete(long rowId) {
        return this.db.delete("task", new StringBuilder().append("_id=").append(rowId).toString(), null) > 0;
    }

    public Cursor get(long rowId) throws SQLException {
        Cursor mCursor = this.db.query(true, "task", ALL_COLUMNS, "_id=" + rowId, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor getAll() {
        return this.db.query("task", ALL_COLUMNS, null, null, null, null, null);
    }

    public long insert(IrtTasksBo businessObject) {
        return this.db.insert("task", null, populateBusinessObjectContentValues((Task) businessObject));
    }

    public boolean update(IrtTasksBo businessObject) {
        Task task = (Task) businessObject;
        return this.db.update("task", populateBusinessObjectContentValues(task), new StringBuilder().append("_id=").append(task.getId()).toString(), null) > 0;
    }

    public boolean resetTasksProject(Long projectId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("projectId", Integer.valueOf(0));
        if (this.db.update("task", contentValues, "projectId=" + projectId, null) > 0) {
            return true;
        }
        return false;
    }

    private ContentValues populateBusinessObjectContentValues(Task task) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GTASK_ID, task.getGtaskId());
        contentValues.put("tasks_list_id", task.getTasksListId());
        contentValues.put(PARENT_TASK_ID, task.getParentTaskId());
        contentValues.put(PRIOR_SIBLING_TASK_ID, task.getPriorSiblingTaskId());
        contentValues.put("name", task.getName());
        contentValues.put("note", task.getNote());
        contentValues.put(DUE_DATE, task.getDueDate());
        contentValues.put("last_modified", task.getLastModified());
        contentValues.put("deleted", AppUtil.getBooleanAs1or0(task.isDeleted()));
        contentValues.put("completed", AppUtil.getBooleanAs1or0(task.isCompleted()));
        contentValues.put("inTrashBin", AppUtil.getBooleanAs1or0(task.isInTrashBin()));
        contentValues.put("moved", AppUtil.getBooleanAs1or0(task.isMoved()));
        contentValues.put("highlight", AppUtil.getBooleanAs1or0(task.isHighlight()));
        contentValues.put("fontStyle", task.getFontStyle());
        contentValues.put("fontColor", task.getFontColor());
        contentValues.put("fontBgColor", task.getFontBgColor());
        contentValues.put(SYNC_TIME, task.getSyncTime());
        contentValues.put("show_note", AppUtil.getBooleanAs1or0(task.isShowNote()));
        contentValues.put(CREATION_DATE, task.getCreationDate());
        contentValues.put(LAST_CONTENT_MODIFIED_DATE, task.getLastContentModifiedDate());
        contentValues.put("priority", task.getPriority());
        contentValues.put(REMINDER_ENABLED, AppUtil.getBooleanAs1or0(task.isReminderEnabled()));
        contentValues.put(REMINDER_DAYS_EARLIER, task.getReminderDaysEarlier());
        contentValues.put(REMINDER_DATE_TIME, task.getReminderDateTime());
        contentValues.put(REMINDER_REPEAT_FROM_COMPLETION_DATE, task.isReminderRepeatFromCompletionDate());
        contentValues.put(REMINDER_REPEAT_RULE, task.getReminderRepeatRule());
        contentValues.put(NOTIFY_BY_EMAIL, AppUtil.getBooleanAs1or0(task.isNotifyByEmail()));
        contentValues.put(NOTIFY_EXTRA_EMAILS, AppUtil.getBooleanAs1or0(task.isNotifyExtraEmails()));
        contentValues.put("extra_emails", task.getExtraEmails());
        contentValues.put(IS_FLOAT, AppUtil.getBooleanAs1or0(task.isFloat()));
        contentValues.put("exclude_from_delete", AppUtil.getBooleanAs1or0(task.isExcludeFromDelete()));
        contentValues.put(ICON_NAME, task.getIconName());
        contentValues.put(SNOOZE_DATE_TIME, task.getSnoozeDateTime());
        contentValues.put(AUTO_SNOOZE_DURATION, task.getAutoSnoozeDuration());
        contentValues.put(COMPLETION_DATE, task.getCompletionDate());
        contentValues.put("starred", AppUtil.getBooleanAs1or0(task.isStarred()));
        contentValues.put("projectId", task.getProjectId());
        contentValues.put("status", task.getStatus());
        return contentValues;
    }

    public Cursor getAllTasksByTasksListIdWithDeleted(Long tasksListId) {
        return this.db.query("task", ALL_COLUMNS, "tasks_list_id=" + tasksListId + " AND " + "inTrashBin" + "!= 1", null, null, null, null);
    }

    public Cursor getAllTasksByTasksListId(Long tasksListId, String orderBy, boolean isCompletedOnly, boolean isDueDateOnly, boolean isSnoozeOnly) {
        String completedFilter = "";
        String dueDateOnlyFilter = "";
        String snoozeOnlyFilter = "";
        if (isSnoozeOnly) {
            snoozeOnlyFilter = " AND snooze_date_time> 0";
        }
        if (isCompletedOnly) {
            completedFilter = " AND completed= 1";
        }
        if (isDueDateOnly) {
            dueDateOnlyFilter = getDueDateOnlyFilter();
        }
        return this.db.query("task", ALL_COLUMNS, "tasks_list_id=" + tasksListId + " AND " + "inTrashBin" + "!= 1" + " AND " + "deleted" + "!= 1 " + completedFilter + dueDateOnlyFilter + snoozeOnlyFilter, null, null, null, orderBy);
    }

    public Cursor getAllTasksDeletedOrTrashBinByTasksListId(Long tasksListId, boolean isDeletedOnly, boolean isTrashBinOnly) {
        String deletedFilter = "";
        String trashBinFilter = " AND inTrashBin = 0";
        if (isDeletedOnly) {
            deletedFilter = " AND deleted= 1 ";
            isTrashBinOnly = false;
        }
        if (isTrashBinOnly) {
            trashBinFilter = " AND inTrashBin= 1";
            deletedFilter = "";
        }
        return this.db.query("task", ALL_COLUMNS, "tasks_list_id=" + tasksListId + trashBinFilter + deletedFilter, null, null, null, "last_content_modified_date DESC");
    }

    public Cursor getTasksMarkedForDeletionButNotInTrashBin(Long tasksListId) {
        return this.db.query("task", ALL_COLUMNS, "tasks_list_id=" + tasksListId + " AND " + "inTrashBin" + "!= 1" + " AND " + "deleted" + "= 1 ", null, null, null, null);
    }

    public Cursor searchAllExcludeDeleteAndTrashBin(String queryString) {
        String finalQuery = queryString;
        if (!StringUtil.isNullOrEmpty(finalQuery)) {
            finalQuery = queryString.trim().toLowerCase();
        }
        return this.db.query("task", ALL_COLUMNS, "( lower(name) like '%" + finalQuery + "%' OR lower(" + "note" + ") like '%" + finalQuery + "%' ) AND " + "inTrashBin" + "!= 1" + " AND " + "deleted" + "!= 1", null, null, null, null);
    }

    public int deleteTasksInTrashBin() {
        return this.db.delete("task", "inTrashBin = 1", null);
    }

    public int forceDeleteByTaskListId(Long taskListId) {
        return this.db.delete("task", "tasks_list_id = " + taskListId, null);
    }

    public int countTasksByTasksListId(Long tasksListId, boolean isCompleted) {
        String completedStr = "";
        if (isCompleted) {
            completedStr = " AND completed = 1";
        }
        Cursor cursor = this.db.rawQuery("SELECT name from task WHERE tasks_list_id = " + tasksListId + " AND " + "inTrashBin" + " = 0 " + "AND " + "deleted" + " = 0 " + completedStr, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public Cursor getFilteredVisibleTasks(Long tasksListId) {
        boolean z;
        this.config = ConfigManager.getInstance();
        if (this.contextIncludesDao.getAllDistinctContextBasesIds().size() > 0) {
            z = true;
        } else {
            z = false;
        }
        this.mIsContextIncludesAvailable = z;
        TasksFilterManager filter = TasksFilterManager.getInstance();
        TasksFilter tasksFilter = filter.getTaskFilter();
        String contextIdsCsv = filter.getContextIdsCsv();
        String showVisible = "";
        showVisible = " AND tasks_list_id IN (" + TaskDBAdapterHelper.getVisibleTasksList(false) + ")";
        if (this.config.isHideCompletedTasks()) {
            showVisible = " AND completed = 0 ";
        }
        StringBuffer query = new StringBuffer();
        if (filter.isAnyContextSelected()) {
            getQueryForAnyContexts(filter, tasksFilter, contextIdsCsv, showVisible, query, tasksListId);
        } else if (tasksFilter.isContextNone()) {
            getQueryForNoneContexts(filter, tasksFilter, contextIdsCsv, showVisible, query, tasksListId);
        } else if (tasksFilter.isContextAnd()) {
            getQueryForAndContexts(filter, tasksFilter, contextIdsCsv, showVisible, query, tasksListId);
        } else {
            getQueryForOrContexts(filter, tasksFilter, contextIdsCsv, showVisible, query, tasksListId);
        }
        return this.db.rawQuery(query.toString(), null);
    }

    private void getQueryForNoneContexts(TasksFilterManager filter, TasksFilter tasksFilter, String contextIdsCsv, String showVisible, StringBuffer query, Long tasksListId) {
        query.append("select distinct task.* from ");
        query.append("( ");
        query.append("Select  distinct task.*, tasks_list.name tasksListName, 'zzzzzzz' contextName ");
        query.append("  from task, tasks_list ");
        query.append(" where task._id not in ( ");
        query.append("       Select  distinct task_id ");
        query.append("\t      From context_task) ");
        tasksListIdWhereClause(query, tasksListId);
        query.append(" ) task ");
        projectFromQuery(filter, tasksFilter, query);
        tagTaskFromQuery(filter, tasksFilter, query);
        whereNotInTrashBinAndDeleted(query);
        statusWhereClause(filter, tasksFilter, query);
        projectWhereClause(filter, tasksFilter, query);
        tagTaskWhereClause(filter, tasksFilter, query);
        query.append(showVisible);
        if (this.config.isShowTasksContexts()) {
            query.append(" order by contextName, name");
        } else {
            query.append(" order by name");
        }
    }

    private void getQueryForAnyContexts(TasksFilterManager filter, TasksFilter tasksFilter, String contextIdsCsv, String showVisible, StringBuffer query, Long tasksListId) {
        query.append("select distinct task.* from ");
        query.append("( ");
        if (this.config.isShowTasksContexts()) {
            query.append("Select  distinct task.*, tasks_list.name tasksListName, context_base.name contextName ");
        } else {
            query.append("Select  distinct task.*, tasks_list.name tasksListName, 'zzzzzzz' contextName  ");
        }
        query.append("\tFrom task, context_task , context_base , tasks_list ");
        query.append(" where task._id = context_task.task_id ");
        query.append("   and context_task.context_id = context_base._id ");
        tasksListIdWhereClause(query, tasksListId);
        query.append(" union ");
        if (this.config.isShowTasksContexts()) {
            query.append("Select  distinct task.*, tasks_list.name tasksListName, 'zzzzzzz' contextName ");
        } else {
            query.append("Select  distinct task.*, tasks_list.name tasksListName, 'zzzzzzz' contextName ");
        }
        query.append("  from task, tasks_list ");
        query.append(" where task._id not in ( ");
        query.append("       Select  distinct task_id ");
        query.append("\t      From context_task) ");
        tasksListIdWhereClause(query, tasksListId);
        query.append(" ) task ");
        projectFromQuery(filter, tasksFilter, query);
        tagTaskFromQuery(filter, tasksFilter, query);
        whereNotInTrashBinAndDeleted(query);
        statusWhereClause(filter, tasksFilter, query);
        projectWhereClause(filter, tasksFilter, query);
        tagTaskWhereClause(filter, tasksFilter, query);
        query.append(showVisible);
        if (this.config.isShowTasksContexts()) {
            query.append(" order by contextName, name");
        } else {
            query.append(" order by name");
        }
    }

    private void getQueryForAndContexts(TasksFilterManager filter, TasksFilter tasksFilter, String contextIdsCsv, String showVisible, StringBuffer query, Long tasksListId) {
        if (this.config.isShowTasksContexts()) {
            query.append("Select  distinct task.*, tasks_list.name, context_base.name ");
        } else {
            query.append("Select  distinct task.*, tasks_list.name, 'zzzzzzz' contextName ");
        }
        query.append("From task, context_task, context_base, tasks_list ");
        projectFromQuery(filter, tasksFilter, query);
        tagTaskFromQuery(filter, tasksFilter, query);
        whereNotInTrashBinAndDeleted(query);
        for (Long contextId : tasksFilter.getContextIds()) {
            setAndForContext(contextId, query);
        }
        query.append("and task._id = context_task.task_id ");
        query.append("and context_task.context_id = context_base._id ");
        statusWhereClause(filter, tasksFilter, query);
        projectWhereClause(filter, tasksFilter, query);
        tagTaskWhereClause(filter, tasksFilter, query);
        tasksListIdWhereClause(query, tasksListId);
        query.append(showVisible);
        if (this.config.isShowTasksContexts()) {
            query.append(" order by context_base.name, task.name ");
        } else {
            query.append(" order by task.name ");
        }
    }

    private void getQueryForOrContexts(TasksFilterManager filter, TasksFilter tasksFilter, String contextIdsCsv, String showVisible, StringBuffer query, Long tasksListId) {
        if (this.config.isShowTasksContexts()) {
            query.append("Select  distinct task.*, tasks_list.name, context_base.name ");
        } else {
            query.append("Select  distinct task.*, tasks_list.name, 'zzzzzzz' contextName ");
        }
        query.append("From task, context_task, context_base");
        if (this.mIsContextIncludesAvailable) {
            query.append(", context_includes ");
        }
        query.append(", tasks_list ");
        projectFromQuery(filter, tasksFilter, query);
        tagTaskFromQuery(filter, tasksFilter, query);
        whereNotInTrashBinAndDeleted(query);
        query.append(" AND (context_task.context_id in (" + contextIdsCsv + ") ");
        if (this.mIsContextIncludesAvailable) {
            query.append("  OR (context_task.context_id = context_includes.included_context_id ");
            query.append("      AND context_includes.context_id in (" + contextIdsCsv + ") ");
            query.append(") ");
        }
        query.append(") ");
        query.append("and task._id = context_task.task_id ");
        query.append("and context_task.context_id = context_base._id ");
        statusWhereClause(filter, tasksFilter, query);
        projectWhereClause(filter, tasksFilter, query);
        tagTaskWhereClause(filter, tasksFilter, query);
        tasksListIdWhereClause(query, tasksListId);
        query.append(showVisible);
        if (this.config.isShowTasksContexts()) {
            query.append(" order by context_base.name, task.name ");
        } else {
            query.append(" order by task.name ");
        }
    }

    private void tasksListIdWhereClause(StringBuffer query, Long tasksListId) {
        if (!(DatabaseHelper.ALL_TASKS_LIST_ID.equals(tasksListId) || this.config.isShowAllTasksOnFilter())) {
            query.append("   and task.tasks_list_id = " + tasksListId + " ");
        }
        query.append("   and task.tasks_list_id = tasks_list._id ");
    }

    private void whereNotInTrashBinAndDeleted(StringBuffer query) {
        query.append(" where  task.inTrashBin = 0 AND  task.deleted = 0 ");
    }

    private void setAndForContext(Long contextId, StringBuffer query) {
        query.append(" AND  task._id  in ( ");
        query.append("      select context_task.task_id ");
        query.append("       from  context_task ");
        if (this.mIsContextIncludesAvailable) {
            query.append("             , context_includes ");
        }
        query.append("      where  context_task.context_id in (" + contextId + ") ");
        if (this.mIsContextIncludesAvailable) {
            query.append("              OR (context_task.context_id = context_includes.included_context_id ");
            query.append("                  AND context_includes.context_id in (" + contextId + ")) ");
        }
        query.append("       )");
    }

    private void projectFromQuery(TasksFilterManager filter, TasksFilter tasksFilter, StringBuffer query) {
        if (!filter.isAnyProjectSelected() && !tasksFilter.isProjectNone()) {
            query.append(", project ");
        }
    }

    private void tagTaskWhereClause(TasksFilterManager filter, TasksFilter tasksFilter, StringBuffer query) {
        if (!(filter.isAnyTagSelected() || tasksFilter.isTagNone())) {
            if (tasksFilter.isTagAnd()) {
                List<Long> tagIds = tasksFilter.getTagIds();
                if (tagIds != null && tagIds.size() > 0) {
                    for (Long long1 : tagIds) {
                        query.append(" AND  task._id in (select task_id from tag_task where tag_id = " + long1 + ") ");
                    }
                }
            } else {
                query.append(" AND  task._id = tag_task.task_id ");
                query.append(" AND  tag_task.tag_id in (" + filter.getTagIdsCsv() + ") ");
            }
        }
        if (tasksFilter.isTagNone()) {
            query.append(" AND  task._id not in (select distinct task_id from tag_task)");
        }
    }

    private void tagTaskFromQuery(TasksFilterManager filter, TasksFilter tasksFilter, StringBuffer query) {
        if (!filter.isAnyTagSelected() && !tasksFilter.isTagNone()) {
            query.append(", tag_task ");
        }
    }

    private void statusWhereClause(TasksFilterManager filter, TasksFilter tasksFilter, StringBuffer query) {
        if (!filter.isAnyStatus()) {
            query.append(" AND  status = " + (tasksFilter.getStatus().intValue() - 1));
        }
    }

    private void projectWhereClause(TasksFilterManager filter, TasksFilter tasksFilter, StringBuffer query) {
        if (!(filter.isAnyProjectSelected() || tasksFilter.isProjectNone())) {
            query.append(" AND  projectId = " + tasksFilter.getProjectId());
            query.append(" AND  projectId = project._id");
        }
        if (tasksFilter.isProjectNone()) {
            query.append(" AND  projectId = 0");
        }
    }

    public Cursor getQuickAllVisibleTasks(String query, String orderBy) {
        String orderByFinal = "";
        String showVisible = "";
        showVisible = " AND tasks_list_id IN (" + TaskDBAdapterHelper.getVisibleTasksList(false) + ")";
        if (!StringUtil.isNullOrEmpty(query)) {
            query = " AND " + query;
        }
        if (!StringUtil.isNullOrEmpty(orderBy)) {
            orderByFinal = " ORDER BY " + orderBy;
        }
        return this.db.rawQuery("SELECT " + getRawColumns() + ", " + TaskDBAdapterHelper.getListRawColumn("name") + " from " + "task" + " JOIN " + TasksListDBAdapterImpl.DATABASE_TABLE + " ON " + "tasks_list_id" + " = " + TaskDBAdapterHelper.getListRawColumn("_id") + " WHERE " + DatabaseUtil.getRawColumn("task", "inTrashBin") + " = 0 " + "AND " + DatabaseUtil.getRawColumn("task", "deleted") + " = 0 " + showVisible + query + orderByFinal, null);
    }

    public Cursor getAllVisibleTasks(String orderBy, boolean isCompleted, boolean isShowInvisible, boolean isDueDateOnly, boolean isSnoozeOnly) {
        String completedFilter = "";
        String dueDateOnlyFilter = "";
        String orderByFinal = "";
        String snoozeOnlyFiler = "";
        if (isSnoozeOnly) {
            snoozeOnlyFiler = " AND snooze_date_time> 0";
        }
        if (isCompleted) {
            completedFilter = " AND completed= 1";
        }
        String showInvisible = "";
        if (!isShowInvisible) {
            showInvisible = " AND tasks_list_id IN (" + TaskDBAdapterHelper.getVisibleTasksList(false) + ")";
        }
        if (isDueDateOnly) {
            dueDateOnlyFilter = getDueDateOnlyFilter();
        }
        if (!StringUtil.isNullOrEmpty(orderBy)) {
            orderByFinal = " ORDER BY " + orderBy;
        }
        return this.db.rawQuery("SELECT " + getRawColumns() + " from " + "task" + " WHERE " + "inTrashBin" + " = 0 " + "AND " + "deleted" + " = 0 " + showInvisible + completedFilter + dueDateOnlyFilter + snoozeOnlyFiler + orderByFinal, null);
    }

    private String getDueDateOnlyFilter() {
        return " AND (due_date> 0 OR is_float = 1 ) AND completed = 0";
    }

    public Cursor getAllTasksWithEligibleAlarmsIncludingInvisibleAccounts() {
        return performGetAllTasksWithEligibleAlarms(true);
    }

    public Cursor getAllTasksWithEligibleAlarms() {
        this.config = ConfigManager.getInstance();
        return performGetAllTasksWithEligibleAlarms(this.config.isShowInvisibleAccounts().booleanValue());
    }

    public Cursor performGetAllTasksWithEligibleAlarms(boolean isShowInvisibleAccounts) {
        long todayMillis = AppUtil.getTodayDateTimeMilisecond();
        return this.db.rawQuery("SELECT " + getRawColumns() + " from " + "task" + " WHERE " + "inTrashBin" + " = 0 " + "AND " + "deleted" + " = 0 " + "AND " + REMINDER_ENABLED + " = 1 " + "AND (" + REMINDER_DATE_TIME + " > " + todayMillis + " OR " + SNOOZE_DATE_TIME + " > " + 0 + ") " + (" AND tasks_list_id IN (" + TaskDBAdapterHelper.getVisibleTasksList(isShowInvisibleAccounts) + ") "), null);
    }

    public Cursor getAllVisibleTasksDeletedOrTashBinOnly(boolean isDeletedOnly, boolean isTrashBinOnly, boolean isShowInvisible) {
        String deletedFilter = "";
        String tashBinFilter = "inTrashBin = 0 ";
        if (isDeletedOnly) {
            deletedFilter = " AND deleted = 1 ";
            isTrashBinOnly = false;
        }
        if (isTrashBinOnly) {
            tashBinFilter = "inTrashBin = 1 ";
            deletedFilter = "";
        }
        return this.db.rawQuery("SELECT " + getRawColumns() + " from " + "task" + " WHERE " + tashBinFilter + deletedFilter + (" AND tasks_list_id IN (" + TaskDBAdapterHelper.getVisibleTasksList(isShowInvisible) + ")") + " ORDER BY " + LAST_CONTENT_MODIFIED_DATE + " DESC", null);
    }

    String getRawColumns() {
        return DatabaseUtil.getRawColumns("task", ALL_COLUMNS);
    }
}
