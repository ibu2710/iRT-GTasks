package com.irt.tasks.app.businessprocess;

import android.content.Context;
import com.irt.tasks.app.businessobjects.Account;
import java.util.List;

public interface AccountBp {
    void deleteAccount(Account account);

    Account get(Long l);

    List<Account> getAll();

    List<Account> getAllAccountsFromSystem(Context context);

    List<Account> getAllVisibleAccounts();

    Boolean isValidGoogleAccount(Account account, boolean z);

    void saveOrUpdate(Account account);
}
