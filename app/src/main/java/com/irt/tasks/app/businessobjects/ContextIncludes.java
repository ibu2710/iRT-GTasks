package com.irt.tasks.app.businessobjects;

import com.irt.tasks.app.framework.business.IrtTasksBo;

public class ContextIncludes implements IrtTasksBo {
    public static final String CONTEXT_ID = "contextId";
    public static final String INCLUDED_CONTEXT_ID = "includedContextId";
    private Long contextId;
    private Long includedContextId;

    public ContextIncludes(Long contextId, Long includedContextId) {
        this.contextId = contextId;
        this.includedContextId = includedContextId;
    }

    public Long getContextId() {
        return this.contextId;
    }

    public void setContextId(Long contextId) {
        this.contextId = contextId;
    }

    public Long getIncludedContextId() {
        return this.includedContextId;
    }

    public void setIncludedContextId(Long includedContextId) {
        this.includedContextId = includedContextId;
    }
}
