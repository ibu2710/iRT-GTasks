package com.irt.tasks.app.dataaccess;

import com.google.api.client.util.DateTime;
import com.google.api.services.tasks.Tasks.TasksOperations.Move;
import com.google.api.services.tasks.model.Tasks;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.todoroo.gtasks.GoogleLoginException;
import com.todoroo.gtasks.actions.Action;
import com.todoroo.gtasks.actions.Actions;
import com.todoroo.gtasks.actions.ListActions;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GtaskDao2Impl implements GtaskDao {
    private static final int DELAY_PER_SUB_BATCH_IN_MILLIS = 2000;
    private static final int SUB_BATCH_SIZE_DELETE = 15;
    private static final int SUB_BATCH_SIZE_INSERT = 15;
    private static final int SUB_BATCH_SIZE_UPDATE = 15;
    private static final Actions actions = new Actions();
    private static final ListActions listActions = new ListActions();
    ConfigManager config;
    TaskDao taskDao = new TaskDaoImpl();

    public List<Task> getAllTasksByTasksListId(String tasksListId) {
        this.config = ConfigManager.getInstance();
        List<Task> tasks = new ArrayList();
        try {
            buildTasks(tasks, getAllGoogleTasksByTasksListId(tasksListId), tasksListId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tasks;
    }

    private List<com.google.api.services.tasks.model.Task> getAllGoogleTasksByTasksListId(String tasksListId) throws IOException {
        return ((Tasks) this.config.getGoogleTaskService2().tasks().list(tasksListId).execute()).getItems();
    }

    public void batchUpdateTasks(List<Task> tasks, SubBatchType subBatchType) {
        this.config = ConfigManager.getInstance();
        List<Task> subBatchTasks = new ArrayList();
        List<com.google.api.services.tasks.model.Task> googleTasks = new ArrayList();
        if (subBatchType == SubBatchType.UPDATE && tasks.size() > 0) {
            try {
                googleTasks = getAllGoogleTasksByTasksListId(((Task) tasks.get(0)).getGtasksListId());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        int subBatchSize = getSubBatchSize(subBatchType);
        int submitCounter = 1;
        long totalRecords = (long) tasks.size();
        int subBatchCounter = 0;
        for (int i = 0; i < tasks.size(); i++) {
            subBatchTasks.add((Task) tasks.get(i));
            if (subBatchCounter < subBatchSize) {
                subBatchCounter++;
            } else {
                if (this.config.isDebug()) {
                    logBatchInfo(totalRecords, subBatchSize, submitCounter);
                    submitCounter++;
                }
                contolledPerformanceOfBatchUpdate(subBatchTasks, googleTasks);
                subBatchTasks.clear();
                subBatchCounter = 0;
                delay(2000);
            }
        }
        if (subBatchCounter > 0) {
            contolledPerformanceOfBatchUpdate(subBatchTasks, googleTasks);
        }
    }

    private void delay(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    private void contolledPerformanceOfBatchUpdate(List<Task> tasks, List<com.google.api.services.tasks.model.Task> googleTasks) {
        try {
            performBatchUpdateTasks(tasks, googleTasks);
            for (Task task2 : tasks) {
                if (task2.isDeleted().booleanValue()) {
                    task2.setInTrashBin(Boolean.valueOf(true));
                } else {
                    Task taskFromDB = this.taskDao.get(task2.getId());
                    if (taskFromDB != null) {
                        taskFromDB.setGtaskId(task2.getGtaskId());
                        taskFromDB.setMoved(Boolean.valueOf(false));
                        task2 = taskFromDB;
                    } else {
                        return;
                    }
                }
                this.taskDao.saveOrUpdate(task2);
            }
        } catch (GoogleLoginException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        } catch (JSONException e2) {
            e2.printStackTrace();
            throw new RuntimeException(e2.getMessage());
        } catch (IOException e3) {
            e3.printStackTrace();
            throw new RuntimeException(e3.getMessage());
        }
    }

    public void moveTaskFromListToList(Task task, TasksList sourceList, TasksList destList, Task destParent) {
        try {
            String str;
            this.config = ConfigManager.getInstance();
            String gtaskId = task.getGtaskId();
            String gtasksListId = sourceList.getGtasksListId();
            String gtasksListId2 = destList.getGtasksListId();
            if (destParent == null) {
                str = null;
            } else {
                str = destParent.getGtaskId();
            }
            Action moveTaskAction = actions.moveTask(gtaskId, gtasksListId, gtasksListId2, str);
            this.config.getGoogleTaskService().executeActions(moveTaskAction);
        } catch (JSONException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        } catch (GoogleLoginException e2) {
            e2.printStackTrace();
            throw new RuntimeException(e2.getMessage());
        } catch (IOException e3) {
            e3.printStackTrace();
            throw new RuntimeException(e3.getMessage());
        }
    }

    public void moveTaskWithinSameList(Task task) {
        this.config = ConfigManager.getInstance();
        try {
            Move move = this.config.getGoogleTaskService2().tasks().move(task.getGtasksListId(), task.getGtaskId());
            move.setParent(task.getParentGtaskId());
            move.setPrevious(task.getPriorSiblingGtaskId());
            move.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void performBatchUpdateTasks(List<Task> tasks, List<com.google.api.services.tasks.model.Task> googleTasks) throws JSONException, IOException, GoogleLoginException {
        this.config = ConfigManager.getInstance();
        com.google.api.services.tasks.Tasks service = this.config.getGoogleTaskService2();
        if (tasks.size() > 0) {
            for (int i = 0; i < tasks.size(); i++) {
                Task task = (Task) tasks.get(i);
                com.google.api.services.tasks.model.Task gTask;
                if (StringUtil.isNullOrEmpty(task.getGtaskId())) {
                    try {
                        DateTime dateTime;
                        gTask = new com.google.api.services.tasks.model.Task();
                        gTask.setTitle(task.getName());
                        gTask.setDeleted(Boolean.valueOf(false));
                        gTask.setCompleted(task.getCompletionDate().longValue() == 0 ? null : new DateTime(task.getCompletionDate().longValue()));
                        if (task.getDueDate().longValue() == 0) {
                            dateTime = null;
                        } else {
                            dateTime = new DateTime(task.getDueDate().longValue());
                        }
                        gTask.setDue(dateTime);
                        gTask.setNotes(task.getNote());
                        gTask.setParent(task.getParentGtaskId());
                        task.setGtaskId(((com.google.api.services.tasks.model.Task) service.tasks().insert(task.getGtasksListId(), gTask).execute()).getId());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (this.config.isDebug()) {
                        System.out.println("Inserting to web [" + task.getName() + "] isDelete = " + task.isDeleted());
                    }
                } else {
                    try {
                        if (task.isDeleted().booleanValue()) {
                            service.tasks().delete(task.getGtasksListId(), task.getGtaskId()).execute();
                        } else {
                            gTask = getGoogleTask(task.getGtaskId(), googleTasks);
                            gTask.setTitle(task.getName());
                            gTask.setDeleted(task.isDeleted());
                            gTask.setDue(task.getDueDate().longValue() == 0 ? null : new DateTime(task.getDueDate().longValue()));
                            if (task.isCompleted().booleanValue()) {
                                gTask.setStatus("completed");
                            } else {
                                gTask.setStatus("needsAction");
                                gTask.setCompleted(null);
                            }
                            gTask.setNotes(task.getNote());
                            service.tasks().update(task.getGtasksListId(), task.getGtaskId(), gTask).execute();
                        }
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                    if (this.config.isDebug()) {
                        System.out.println("Updating web [" + task.getName() + "] isDelete = " + task.isDeleted());
                    }
                }
                if (!task.isDeleted().booleanValue() && task.isMoved().booleanValue()) {
                    if (this.config.isDebug()) {
                        System.out.println("Updating web move ...[" + task.getName() + "] isMoved = " + task.isMoved());
                    }
                    moveTaskWithinSameList(task);
                }
            }
        }
    }

    private com.google.api.services.tasks.model.Task getGoogleTask(String gTaskId, List<com.google.api.services.tasks.model.Task> googleTasks) {
        for (com.google.api.services.tasks.model.Task task : googleTasks) {
            if (gTaskId.equals(task.getId())) {
                return task;
            }
        }
        return null;
    }

    private int countNumberOfBatchMoves(List<Task> tasks) {
        int numberOfBatchMoves = 0;
        for (Task task : tasks) {
            if (!task.isDeleted().booleanValue() && task.isMoved().booleanValue()) {
                numberOfBatchMoves++;
            }
        }
        return numberOfBatchMoves;
    }

    private void buildTasks(List<Task> tasks, List<com.google.api.services.tasks.model.Task> googleTasks, String tasksListId) {
        if (googleTasks != null && googleTasks.size() > 0) {
            String priorGtaskId = null;
            for (int i = 0; i < googleTasks.size(); i++) {
                Task task = new Task();
                buildTask(priorGtaskId, task, (com.google.api.services.tasks.model.Task) googleTasks.get(i), tasks, tasksListId);
                tasks.add(task);
                priorGtaskId = task.getGtaskId();
            }
        }
    }

    private void buildTask(String priorGtaskId, Task task, com.google.api.services.tasks.model.Task googleTask, List<Task> tasks, String gTasksListId) {
        boolean z = true;
        boolean z2 = false;
        task.setWebTask(true);
        task.setName(googleTask.getTitle());
        task.setGtaskId(googleTask.getId());
        task.setGtasksListId(gTasksListId);
        task.setNote(googleTask.getNotes());
        if (googleTask.getCompleted() == null) {
            z = false;
        }
        task.setCompleted(Boolean.valueOf(z));
        if (googleTask.getDeleted() != null) {
            z2 = googleTask.getDeleted().booleanValue();
        }
        task.setDeleted(Boolean.valueOf(z2));
        task.setLastModified(Long.valueOf(googleTask.getUpdated().getValue()));
        task.setDueDate(Long.valueOf(gtasksDueTimeToUnixTime(googleTask.getDue(), 0)));
        task.setPriorSiblingGtaskId(priorGtaskId);
        task.setParentGtaskId(googleTask.getParent());
        if (priorGtaskId != null && priorGtaskId.equals(task.getParentGtaskId())) {
            task.setPriorSiblingGtaskId(null);
        }
        if (!StringUtil.isNullOrEmpty(task.getPriorSiblingGtaskId())) {
            Task priorSiblingTask = null;
            for (Task task2 : tasks) {
                if (StringUtil.isNullOrEmpty(task2.getParentGtaskId()) && StringUtil.isNullOrEmpty(task.getParentGtaskId())) {
                    priorSiblingTask = task2;
                } else if (!StringUtil.isNullOrEmpty(task2.getParentGtaskId()) && task2.getParentGtaskId().equals(task.getParentGtaskId())) {
                    priorSiblingTask = task2;
                }
            }
            if (priorSiblingTask != null) {
                task.setPriorSiblingGtaskId(priorSiblingTask.getGtaskId());
            }
        }
    }

    public long gtasksDueTimeToUnixTime(DateTime gtasksDueTime, long defaultValue) {
        if (gtasksDueTime != null) {
            try {
                Date date = new Date(gtasksDueTime.getValue());
                defaultValue = new Date(date.getTime() + ((long) (date.getTimezoneOffset() * 60000))).getTime();
            } catch (NumberFormatException e) {
            }
        }
        return defaultValue;
    }

    private int getSubBatchSize(SubBatchType subBatchType) {
        switch (subBatchType) {
            case DELETE:
            case INSERT:
            case UPDATE:
                return 15;
            default:
                return 0;
        }
    }

    private void logBatchInfo(long totalRecords, int subBatchSize, int submitCounter) {
        System.out.println("*****************************************************************************");
        System.out.println("Submiting contolledPerformanceOfBatchUpdate...." + submitCounter + " of " + (totalRecords / ((long) subBatchSize)) + " with " + subBatchSize + " records per batch" + " / " + AppUtil.getTodayDateTime());
        System.out.println("*****************************************************************************");
    }
}
