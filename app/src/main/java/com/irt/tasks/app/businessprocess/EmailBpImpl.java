package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.Email;
import com.irt.tasks.app.dataaccess.EmailDao;
import com.irt.tasks.app.dataaccess.EmailDaoImpl;

public class EmailBpImpl implements EmailBp {
    EmailDao emailDao = new EmailDaoImpl();

    public Email get() {
        return this.emailDao.get();
    }

    public void saveOrUpdate(Email email) {
        this.emailDao.saveOrUpdate(email);
    }
}
