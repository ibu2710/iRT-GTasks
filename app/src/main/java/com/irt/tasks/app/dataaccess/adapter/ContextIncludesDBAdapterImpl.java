package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.irt.tasks.app.businessobjects.ContextIncludes;
import com.irt.tasks.app.framework.business.IrtTasksBo;

public class ContextIncludesDBAdapterImpl implements ContextIncludesDBAdapter {
    public static final String CONTEXT_ID = "context_id";
    public static final String DATABASE_CREATE = "create table context_includes (context_id integer not null, included_context_id integer default 0 not null); ";
    public static final String DATABASE_TABLE = "context_includes";
    public static final String INCLUDED_CONTEXT_ID = "included_context_id";
    public static final String INDEX_ON_CONTEXT_ID = "CREATE INDEX idx_context_includes_context_id ON context_includes (context_id) ";
    private static final String[] ALL_COLUMNS = new String[]{"context_id", INCLUDED_CONTEXT_ID};
    private DatabaseHelper DBHelper;
    private final Context context;
    private SQLiteDatabase db;

    public ContextIncludesDBAdapterImpl(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(this.context);
    }

    public ContextIncludesDBAdapter open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.DBHelper.close();
    }

    public boolean delete(long rowId) {
        throw new RuntimeException("Not yet implemented");
    }

    public int deleteAllbyContextBaseId(Long contextBaseId) {
        return this.db.delete(DATABASE_TABLE, "context_id=" + contextBaseId, null);
    }

    public Cursor get(long rowId) throws SQLException {
        throw new RuntimeException("Not yet implemented");
    }

    public Cursor getAll() {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, null, null, null, null, null);
    }

    public Cursor getAllByContextBaseId(Long contextBaseId) {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, "context_id=" + contextBaseId, null, null, null, null);
    }

    public Cursor getAllDistinctContextBasesIds() {
        return this.db.rawQuery("SELECT DISTINCT context_id from context_includes", null);
    }

    public long insert(IrtTasksBo businessObject) {
        return this.db.insert(DATABASE_TABLE, null, populateBusinessObjectContentValues((ContextIncludes) businessObject));
    }

    public boolean update(IrtTasksBo businessObject) {
        throw new RuntimeException("Not yet implemented");
    }

    private ContentValues populateBusinessObjectContentValues(ContextIncludes contextIncludes) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("context_id", contextIncludes.getContextId());
        contentValues.put(INCLUDED_CONTEXT_ID, contextIncludes.getIncludedContextId());
        return contentValues;
    }
}
