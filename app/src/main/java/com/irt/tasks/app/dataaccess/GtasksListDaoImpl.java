package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.todoroo.gtasks.GoogleLoginException;
import com.todoroo.gtasks.GoogleTaskListInfo;
import com.todoroo.gtasks.GoogleTaskView;
import com.todoroo.gtasks.actions.Action;
import com.todoroo.gtasks.actions.Actions;
import com.todoroo.gtasks.actions.ListAction;
import com.todoroo.gtasks.actions.ListActions;
import com.todoroo.gtasks.actions.ListCreationAction;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;

public class GtasksListDaoImpl implements GtasksListDao {
    private static final Actions actions = new Actions();
    private static final ListActions listActions = new ListActions();
    ConfigManager config;

    public List<TasksList> getAllTasksLists() {
        this.config = ConfigManager.getInstance();
        List<TasksList> tasksLists = new ArrayList();
        try {
            Account account = ArchitectureContext.getAccount();
            GoogleTaskView taskView = this.config.getGoogleTaskService().getTaskView();
            if (taskView != null && taskView.getAllLists().length > 0) {
                GoogleTaskListInfo[] googleTaskListInfos = taskView.getAllLists();
                String activeTaskGTasksListId = getActiveTasksListId(taskView);
                for (GoogleTaskListInfo googleTaskListInfo : googleTaskListInfos) {
                    TasksList tasksList = new TasksList();
                    tasksList.setName(googleTaskListInfo.getName());
                    tasksList.setGtasksListId(googleTaskListInfo.getId());
                    tasksList.setAccountId(account.getId());
                    tasksList.setActiveTasksList(Boolean.valueOf(googleTaskListInfo.getId().equals(activeTaskGTasksListId)));
                    tasksLists.add(tasksList);
                }
            }
            return tasksLists;
        } catch (GoogleLoginException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        } catch (JSONException e2) {
            e2.printStackTrace();
            throw new RuntimeException(e2.getMessage());
        } catch (IOException e3) {
            e3.printStackTrace();
            throw new RuntimeException(e3.getMessage());
        }
    }

    private String getActiveTasksListId(GoogleTaskView taskView) {
        this.config = ConfigManager.getInstance();
        GoogleTaskListInfo gTasksListInfo = taskView.getActiveTaskList().getInfo();
        if (gTasksListInfo == null) {
            return null;
        }
        if (this.config.isDebug()) {
            System.out.println("Active Tasks List is [" + gTasksListInfo.getName() + "]");
        }
        return gTasksListInfo.getId();
    }

    public TasksList getTasksList(String tasksListId) {
        for (TasksList tasksList : getAllTasksLists()) {
            if (tasksList.getGtasksListId().equals(tasksListId)) {
                return tasksList;
            }
        }
        return null;
    }

    public void deleteTasksList(TasksList tasksList) {
        try {
            Action deleteAction = actions.deleteList(tasksList.getGtasksListId());
            this.config.getGoogleTaskService().executeActions(deleteAction);
        } catch (JSONException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        } catch (GoogleLoginException e2) {
            e2.printStackTrace();
            throw new RuntimeException(e2.getMessage());
        } catch (IOException e3) {
            e3.printStackTrace();
            throw new RuntimeException(e3.getMessage());
        }
    }

    public void renameTasksList(TasksList tasksList) {
        try {
            ListAction listRenameAction = listActions.renameList(tasksList.getName());
            this.config.getGoogleTaskService().executeListActions(tasksList.getGtasksListId(), listRenameAction);
        } catch (GoogleLoginException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        } catch (IOException e2) {
            e2.printStackTrace();
            throw new RuntimeException(e2.getMessage());
        } catch (JSONException e3) {
            e3.printStackTrace();
            throw new RuntimeException(e3.getMessage());
        }
    }

    public void insertTasksList(TasksList tasksList) {
        try {
            ListCreationAction listCreationAction = actions.createList(this.config.getGoogleTaskService().getTaskView().getNextListIndex(), tasksList.getName());
            this.config.getGoogleTaskService().executeActions(listCreationAction);
            tasksList.setGtasksListId(listCreationAction.getNewId());
        } catch (GoogleLoginException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        } catch (JSONException e2) {
            e2.printStackTrace();
            throw new RuntimeException(e2.getMessage());
        } catch (IOException e3) {
            e3.printStackTrace();
            throw new RuntimeException(e3.getMessage());
        }
    }
}
