package com.irt.tasks.app.dataaccess.adapter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.DateUtil;
import java.util.Date;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final Long ALL_TASKS_LIST_ID = Long.valueOf(1);
    public static final String DATABASE_NAME = "irttasks";
    private static final int DATABASE_VERSION = 9;
    public static final Long OFFLINE_ACCOUNT_ID = Long.valueOf(1);
    public static final String ORDER_ASC = " asc";
    public static final String ORDER_DESC = " desc";

    public static final String formatDbDate(Date date) {
        return DateUtil.format(date, Constants.DATE_FORMAT_YYYY_MM_DD);
    }

    DatabaseHelper(Context context) {
        super(context, "irttasks", null, 9);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(AccountDBAdapterImpl.DATABASE_CREATE);
        db.execSQL(TasksListDBAdapterImpl.DATABASE_CREATE);
        db.execSQL(TaskDBAdapterImpl.DATABASE_CREATE);
        db.execSQL(TaskStateDBAdapterImpl.DATABASE_CREATE);
        db.execSQL(LevelStateDBAdapterImpl.DATABASE_CREATE);
        db.execSQL(LevelColorDBAdapterImpl.DATABASE_CREATE);
        db.execSQL(ReminderDBAdapterImpl.DATABASE_CREATE);
        db.execSQL(EmailDBAdapterImpl.DATABASE_CREATE);
        db.execSQL(ContextBaseDBAdapterImpl.DATABASE_CREATE);
        db.execSQL(ContextIncludesDBAdapterImpl.DATABASE_CREATE);
        db.execSQL(ContextTaskDBAdapterImpl.DATABASE_CREATE);
        db.execSQL(ProjectDBAdapterImpl.DATABASE_CREATE);
        db.execSQL(TagDBAdapterImpl.DATABASE_CREATE);
        db.execSQL(TagTaskDBAdapterImpl.DATABASE_CREATE);
        db.execSQL(TaskDBAdapterImpl.INDEX_ON_TASK_LIST_ID);
        db.execSQL(TaskDBAdapterImpl.INDEX_ON_DELETED);
        db.execSQL(TaskDBAdapterImpl.INDEX_ON_TRASH_BIN);
        db.execSQL(TasksListDBAdapterImpl.INDEX_ON_ACCOUNT_ID);
        db.execSQL(TasksListDBAdapterImpl.INDEX_ON_VISIBLE);
        db.execSQL(TasksListDBAdapterImpl.INDEX_ON_DELETED);
        db.execSQL(TasksListDBAdapterImpl.INDEX_ON_TRASH_BIN);
        db.execSQL(TaskStateDBAdapterImpl.INDEX_ON_TASK_LIST_ID);
        db.execSQL(LevelColorDBAdapterImpl.INDEX_ON_TASK_LIST_ID);
        db.execSQL(ReminderDBAdapterImpl.INDEX_ON_TASK_ID);
        db.execSQL(ContextBaseDBAdapterImpl.INDEX_UNIQUE_ON_NAME);
        db.execSQL(ContextIncludesDBAdapterImpl.INDEX_ON_CONTEXT_ID);
        db.execSQL(ContextTaskDBAdapterImpl.INDEX_ON_CONTEXT_ID);
        db.execSQL(ProjectDBAdapterImpl.INDEX_UNIQUE_ON_TITLE);
        db.execSQL(TagDBAdapterImpl.INDEX_UNIQUE_ON_NAME);
        db.execSQL(TagTaskDBAdapterImpl.INDEX_ON_TAG_ID);
        db.execSQL(getLocalAccountInsertStatement());
        db.execSQL(getTasksListInsertStatementAllTasksList());
        insertContextBaseData(db);
    }

    private String getLocalAccountInsertStatement() {
        return "INSERT INTO account (email, note, enable) values ('Offline Tasks','This account does not sync with Google tasks.', 1);";
    }

    private String getTasksListInsertStatementAllTasksList() {
        return "INSERT INTO tasks_list (name, account_id,note, auto_sync, visible) values ('ALL TASKS LIST', 0,'This is ALL tasksList as tasksList', 0, 0);";
    }

    private void insertContextBaseData(SQLiteDatabase db) {
        db.execSQL(getContextBaseDataInsertStatement("@any_computer"));
        db.execSQL(getContextBaseDataInsertStatement("@anywhere"));
        db.execSQL(getContextBaseDataInsertStatement("@home"));
        db.execSQL(getContextBaseDataInsertStatement("@my_computer"));
        db.execSQL(getContextBaseDataInsertStatement("@office"));
        db.execSQL(getContextBaseDataInsertStatement("@shop"));
    }

    private String getContextBaseDataInsertStatement(String contextName) {
        return "INSERT INTO context_base (name) values ('" + contextName + "');";
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(Constants.TAG, "Downgrading database from version " + oldVersion + " to " + newVersion);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onUpgrade(android.database.sqlite.SQLiteDatabase r7, int r8, int r9) {
        /*
        r6 = this;
        r3 = "iRTGTasks";
        r4 = new java.lang.StringBuilder;
        r4.<init>();
        r5 = "Upgrading database from version ";
        r4 = r4.append(r5);
        r4 = r4.append(r8);
        r5 = " to ";
        r4 = r4.append(r5);
        r4 = r4.append(r9);
        r4 = r4.toString();
        android.util.Log.w(r3, r4);
        r1 = 0;
        r2 = 0;
        r0 = 0;
        switch(r8) {
            case 1: goto L_0x0029;
            case 2: goto L_0x003d;
            case 3: goto L_0x004b;
            case 4: goto L_0x004f;
            case 5: goto L_0x0058;
            case 6: goto L_0x005c;
            case 7: goto L_0x0064;
            case 8: goto L_0x00ab;
            default: goto L_0x0028;
        };
    L_0x0028:
        return;
    L_0x0029:
        r3 = "DROP TABLE level_color; ";
        r7.execSQL(r3);
        r3 = "create table level_color (_id integer primary key autoincrement, tasks_list_id integer not null, level integer default 0 not null, font_color integer default 0 not null, font_style integer default 0 not null); ";
        r7.execSQL(r3);
        r3 = "CREATE INDEX idx_level_color_tasks_list_id ON level_color (tasks_list_id) ";
        r7.execSQL(r3);
        r3 = "UPDATE tasks_list SET color_tasks = 1; ";
        r7.execSQL(r3);
    L_0x003d:
        r3 = "create table reminder (task_id integer not null, enabled integer default 1 not null, days_earlier integer default 0 not null, date_time integer default 0 not null, repeat_from_completion_date integer default 0 not null, repeat_rule text null); ";
        r7.execSQL(r3);
        r3 = "CREATE INDEX idx_reminder_task_id ON reminder (task_id) ";
        r7.execSQL(r3);
        r6.alterTaskTable(r7, r1);
        r1 = 1;
    L_0x004b:
        r6.alterTaskTable(r7, r1);
        r1 = 1;
    L_0x004f:
        r3 = "create table email (_id integer primary key autoincrement, name text null, email text not null, password text not null, extra_emails text null, signature  text default 'Sent from my iRT GTasks Outliner' null);";
        r7.execSQL(r3);
        r6.alterTaskTable_0_0_19(r7, r1);
        r1 = 1;
    L_0x0058:
        r6.alterTaskTable_0_0_19(r7, r1);
        r1 = 1;
    L_0x005c:
        r6.alterTaskTable_0_0_27(r7, r1);
        r1 = 1;
        r6.alterTasksListTable(r7, r2);
        r2 = 1;
    L_0x0064:
        r6.alterTaskTable_0_0_34(r7, r1);
        r1 = 1;
        r6.alterTasksListTable_0_0_34(r7, r2);
        r2 = 1;
        r3 = "create table context_base (_id integer primary key autoincrement, name text null);";
        r7.execSQL(r3);
        r3 = "CREATE UNIQUE INDEX idx_unique_context_base_name ON context_base (name) ";
        r7.execSQL(r3);
        r3 = "create table context_includes (context_id integer not null, included_context_id integer default 0 not null); ";
        r7.execSQL(r3);
        r3 = "CREATE INDEX idx_context_includes_context_id ON context_includes (context_id) ";
        r7.execSQL(r3);
        r3 = "create table context_task (context_id integer not null, task_id integer default 0 not null); ";
        r7.execSQL(r3);
        r3 = "CREATE INDEX idx_context_task_context_id ON context_task (context_id) ";
        r7.execSQL(r3);
        r3 = "create table project (_id integer primary key autoincrement, title text null, description text null);";
        r7.execSQL(r3);
        r3 = "CREATE UNIQUE INDEX idx__unique_project_title ON project (title) ";
        r7.execSQL(r3);
        r3 = "create table tag (_id integer primary key autoincrement, name text null);";
        r7.execSQL(r3);
        r3 = "CREATE UNIQUE INDEX idx__unique_tag_name ON tag (name) ";
        r7.execSQL(r3);
        r3 = "create table tag_task (tag_id integer not null, task_id integer default 0 not null); ";
        r7.execSQL(r3);
        r3 = "CREATE INDEX idx_tag_task_tag_id ON tag_task (tag_id) ";
        r7.execSQL(r3);
        r6.insertContextBaseData(r7);
    L_0x00ab:
        r6.alterAccountTable(r7, r0);
        r0 = 1;
        goto L_0x0028;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.irt.tasks.app.dataaccess.adapter.DatabaseHelper.onUpgrade(android.database.sqlite.SQLiteDatabase, int, int):void");
    }

    private void alterTasksListTable(SQLiteDatabase db, boolean isTasksListTableAltered) {
        if (!isTasksListTableAltered) {
            db.execSQL("ALTER TABLE tasks_list RENAME TO tmp_tasks_list; ");
            db.execSQL(TasksListDBAdapterImpl.DATABASE_CREATE);
            db.execSQL("INSERT INTO tasks_list(" + getTasksListColumns() + ") " + "SELECT " + getTasksListColumns() + "FROM tmp_tasks_list ;");
            db.execSQL("DROP TABLE tmp_tasks_list; ");
        }
    }

    private String getTasksListColumns() {
        return "_id, gtasks_list_name, name, gtasks_list_id, deleted, last_modified, sort_position, account_id, font_style, font_color, font_bg_color, inTrashBin, auto_sync, visible, task_last_modified, note, highlight, color_tasks, active_tasks_list, show_note, show_due_date, new_task_position ";
    }

    private void alterTasksListTable_0_0_34(SQLiteDatabase db, boolean isTasksListTableAltered) {
        if (!isTasksListTableAltered) {
            db.execSQL("ALTER TABLE tasks_list RENAME TO tmp_tasks_list; ");
            db.execSQL(TasksListDBAdapterImpl.DATABASE_CREATE);
            db.execSQL("INSERT INTO tasks_list(" + getTasksListColumns_0_0_34() + ") " + "SELECT " + getTasksListColumns_0_0_34() + "FROM tmp_tasks_list ;");
            db.execSQL("DROP TABLE tmp_tasks_list; ");
        }
    }

    private String getTasksListColumns_0_0_34() {
        return getTasksListColumns();
    }

    private void alterTaskTable(SQLiteDatabase db, boolean isTaskTableAltered) {
        if (!isTaskTableAltered) {
            db.execSQL("ALTER TABLE task RENAME TO tmp_task; ");
            db.execSQL(TaskDBAdapterImpl.DATABASE_CREATE);
            db.execSQL("INSERT INTO task(" + getTaskColumns() + ") " + "SELECT " + getTaskColumns() + "FROM tmp_task ;");
            db.execSQL("DROP TABLE tmp_task; ");
        }
    }

    private String getTaskColumns() {
        return "_id, gtask_id, tasks_list_id, parent_task_id, prior_sibling_task_id, name, note, due_date, last_modified, deleted, completed, inTrashBin, moved, highlight, fontStyle, fontColor, fontBgColor, sync_time, show_note, creation_date, last_content_modified_date, priority ";
    }

    private void alterTaskTable_0_0_19(SQLiteDatabase db, boolean isTaskTableAltered) {
        if (!isTaskTableAltered) {
            db.execSQL("ALTER TABLE task RENAME TO tmp_task; ");
            db.execSQL(TaskDBAdapterImpl.DATABASE_CREATE);
            db.execSQL("INSERT INTO task(" + getTaskColumns_0_0_19() + ") " + "SELECT " + getTaskColumns_0_0_19() + "FROM tmp_task ;");
            db.execSQL("DROP TABLE tmp_task; ");
        }
    }

    private String getTaskColumns_0_0_19() {
        return getTaskColumns() + ", reminder_enabled" + ", reminder_days_earlier" + ", reminder_date_time" + ", reminder_repeat_from_completion_date" + ", reminder_repeat_rule ";
    }

    private void alterTaskTable_0_0_27(SQLiteDatabase db, boolean isTaskTableAltered) {
        if (!isTaskTableAltered) {
            db.execSQL("ALTER TABLE task RENAME TO tmp_task; ");
            db.execSQL(TaskDBAdapterImpl.DATABASE_CREATE);
            db.execSQL("INSERT INTO task(" + getTaskColumns_0_0_27() + ") " + "SELECT " + getTaskColumns_0_0_27() + "FROM tmp_task ;");
            db.execSQL("DROP TABLE tmp_task; ");
        }
    }

    private String getTaskColumns_0_0_27() {
        return getTaskColumns_0_0_19() + ", notify_by_email" + ", notify_extra_emails" + ", extra_emails" + ", is_float ";
    }

    private void alterTaskTable_0_0_34(SQLiteDatabase db, boolean isTaskTableAltered) {
        if (!isTaskTableAltered) {
            db.execSQL("ALTER TABLE task RENAME TO tmp_task; ");
            db.execSQL(TaskDBAdapterImpl.DATABASE_CREATE);
            db.execSQL("INSERT INTO task(" + getTaskColumns_0_0_34() + ") " + "SELECT " + getTaskColumns_0_0_34() + "FROM tmp_task ;");
            db.execSQL("DROP TABLE tmp_task; ");
        }
    }

    private String getTaskColumns_0_0_34() {
        return getTaskColumns_0_0_27() + ", exclude_from_delete" + ", icon_name" + ", snooze_date_time" + ", auto_snooze_duration ";
    }

    private void alterAccountTable(SQLiteDatabase db, boolean isAccountTableAltered) {
        if (!isAccountTableAltered) {
            db.execSQL("ALTER TABLE account RENAME TO tmp_account; ");
            db.execSQL(AccountDBAdapterImpl.DATABASE_CREATE);
            db.execSQL("INSERT INTO account(" + getAccountColumns() + ") " + "SELECT " + getAccountColumns() + "FROM tmp_account ;");
            db.execSQL("DROP TABLE tmp_account; ");
        }
    }

    private String getAccountColumns() {
        return "_id, email, password, domain, auto_sync, enable, note, sort_position, font_style, font_color, font_bg_color ";
    }

    public static TasksList createTasksListCalledAll() {
        TasksList tasksList = new TasksList();
        Account account = new Account();
        account.setEmail("");
        tasksList.setName("ALL TASKS LIST");
        tasksList.setId(ALL_TASKS_LIST_ID);
        tasksList.setAccount(account);
        return tasksList;
    }
}
