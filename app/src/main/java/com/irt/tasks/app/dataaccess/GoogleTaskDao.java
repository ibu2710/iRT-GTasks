package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.GoogleTask;
import com.irt.tasks.app.framework.dataaccess.Dao;
import java.util.List;

public interface GoogleTaskDao extends Dao {
    void delete(GoogleTask googleTask);

    GoogleTask get(String str);

    List<GoogleTask> getAll();

    void save(GoogleTask googleTask);

    void saveOrUpdate(GoogleTask googleTask);
}
