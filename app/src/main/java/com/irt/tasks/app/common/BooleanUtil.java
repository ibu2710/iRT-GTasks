package com.irt.tasks.app.common;

import java.util.Set;

public final class BooleanUtil {
    private static final String FALSE_STR = "false";
    private static final String TRUE_STR = "true";

    public static final boolean isValidBoolean(String string) {
        return string != null && (string.equalsIgnoreCase("true") || string.equalsIgnoreCase("false"));
    }

    public static final boolean parseBoolean(String string) {
        Assert.isTrue(isValidBoolean(string), "Expecting \"false\" or \"true\". Received: " + string);
        return Boolean.valueOf(string).booleanValue();
    }

    public static final boolean parseBoolean(String string, boolean defaultBoolean) {
        return string == null ? defaultBoolean : parseBoolean(string);
    }

    public static final boolean parseBoolean(String string, Set trueValueSet) {
        if (string == null) {
            return false;
        }
        if (Boolean.valueOf(string).booleanValue() || trueValueSet.contains(string)) {
            return true;
        }
        return false;
    }

    private BooleanUtil() {
    }
}
