package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.framework.dataaccess.Dao;
import java.util.List;

public interface TasksListDao extends Dao {
    void delete(TasksList tasksList);

    int deleteTasksListInTrashBin();

    void forceDeleteByAccountId(Long l);

    TasksList get(Long l);

    List<TasksList> getAll();

    List<TasksList> getAllByAccountId(Long l);

    List<TasksList> getAllByAccountIdExcludeMarkedAsDelete(Long l);

    List<TasksList> getAllIncludingDeletedAndTrashBin();

    List<TasksList> getAllSortedExcludeMarkedAsDelete(String str);

    void saveOrUpdate(TasksList tasksList);

    void saveOrUpdateWithTimeStamp(TasksList tasksList);
}
