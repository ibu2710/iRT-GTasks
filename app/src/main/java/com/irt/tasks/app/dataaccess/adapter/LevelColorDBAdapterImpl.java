package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.irt.tasks.app.businessobjects.LevelColor;
import com.irt.tasks.app.framework.business.IrtTasksBo;

public class LevelColorDBAdapterImpl implements LevelColorDBAdapter {
    private static final String[] ALL_COLUMNS = new String[]{"_id", "tasks_list_id", "level", "font_color", "font_style"};
    public static final String DATABASE_CREATE = "create table level_color (_id integer primary key autoincrement, tasks_list_id integer not null, level integer default 0 not null, font_color integer default 0 not null, font_style integer default 0 not null); ";
    public static final String DATABASE_TABLE = "level_color";
    public static final String FONT_COLOR = "font_color";
    public static final String FONT_STYLE = "font_style";
    public static final String INDEX_ON_TASK_LIST_ID = "CREATE INDEX idx_level_color_tasks_list_id ON level_color (tasks_list_id) ";
    public static final String LEVEL = "level";
    public static final String ROW_ID = "_id";
    public static final String TASKS_LIST_ID = "tasks_list_id";
    private DatabaseHelper DBHelper;
    private final Context context;
    private SQLiteDatabase db;

    public LevelColorDBAdapterImpl(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(this.context);
    }

    public LevelColorDBAdapter open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.DBHelper.close();
    }

    public boolean delete(long rowId) {
        return this.db.delete(DATABASE_TABLE, new StringBuilder().append("_id=").append(rowId).toString(), null) > 0;
    }

    public int deleteAllbyTasksListId(Long tasksListId) {
        return this.db.delete(DATABASE_TABLE, "tasks_list_id=" + tasksListId, null);
    }

    public Cursor get(long rowId) throws SQLException {
        Cursor mCursor = this.db.query(true, DATABASE_TABLE, ALL_COLUMNS, "_id=" + rowId, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor getAll() {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, null, null, null, null, null);
    }

    public Cursor getAllDistinctTasksListIds() {
        return this.db.rawQuery("SELECT DISTINCT tasks_list_id from level_color", null);
    }

    public Cursor getAllByTasksListId(Long tasksListId) {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, "tasks_list_id=" + tasksListId, null, null, null, "level");
    }

    public long insert(IrtTasksBo businessObject) {
        return this.db.insert(DATABASE_TABLE, null, populateBusinessObjectContentValues((LevelColor) businessObject));
    }

    public boolean update(IrtTasksBo businessObject) {
        LevelColor levelColor = (LevelColor) businessObject;
        return this.db.update(DATABASE_TABLE, populateBusinessObjectContentValues(levelColor), new StringBuilder().append("_id=").append(levelColor.getId()).toString(), null) > 0;
    }

    private ContentValues populateBusinessObjectContentValues(LevelColor levelColor) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("tasks_list_id", levelColor.getTasksListId());
        contentValues.put("level", levelColor.getLevel());
        contentValues.put("font_color", levelColor.getFontColor());
        contentValues.put("font_style", levelColor.getFontStyle());
        return contentValues;
    }
}
