package com.irt.tasks.app.dataaccess.adapter;

import android.database.Cursor;

public interface TasksListDBAdapter extends DBAdapter {
    int deleteTasksListInTrashBin();

    boolean forceDeleteByAccountId(Long l);

    Cursor getAllByAccountId(Long l);

    Cursor getAllByAccountIdExcludeMarkedAsDelete(Long l);

    Cursor getAllIncludingDeletedAndTrashBin();

    Cursor getAllSortedExcludeMarkedAsDelete(String str, boolean z, boolean z2);
}
