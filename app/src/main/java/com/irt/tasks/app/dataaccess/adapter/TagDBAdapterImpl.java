package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.irt.tasks.app.businessobjects.Tag;
import com.irt.tasks.app.framework.business.IrtTasksBo;

public class TagDBAdapterImpl implements TagDBAdapter {
    private static final String[] ALL_COLUMNS = new String[]{"_id", "name"};
    public static final String DATABASE_CREATE = "create table tag (_id integer primary key autoincrement, name text null);";
    public static final String DATABASE_TABLE = "tag";
    public static final String INDEX_UNIQUE_ON_NAME = "CREATE UNIQUE INDEX idx__unique_tag_name ON tag (name) ";
    public static final String NAME = "name";
    public static final String ROW_ID = "_id";
    private DatabaseHelper DBHelper;
    private final Context context;
    private SQLiteDatabase db;

    public TagDBAdapterImpl(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(this.context);
    }

    public TagDBAdapterImpl open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.DBHelper.close();
    }

    public boolean delete(long rowId) {
        return this.db.delete(DATABASE_TABLE, new StringBuilder().append("_id=").append(rowId).toString(), null) > 0;
    }

    public boolean isNameUsed(String name, Long tagId) {
        String idCriteria = " ";
        if (tagId != null) {
            idCriteria = " AND _id != " + tagId;
        }
        Cursor mCursor = this.db.query(true, DATABASE_TABLE, ALL_COLUMNS, "lower(name)= '" + name.toLowerCase().trim() + "' " + idCriteria, null, null, null, null, null);
        if (mCursor == null) {
            return false;
        }
        if (mCursor.getCount() > 0) {
            return true;
        }
        return false;
    }

    public Cursor get(long rowId) throws SQLException {
        Cursor mCursor = this.db.query(true, DATABASE_TABLE, ALL_COLUMNS, "_id=" + rowId, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor getAll() {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, null, null, null, null, "lower(name)");
    }

    public long insert(IrtTasksBo businessObject) {
        return this.db.insert(DATABASE_TABLE, null, populateBusinessObjectContentValues((Tag) businessObject));
    }

    public boolean update(IrtTasksBo businessObject) {
        Tag tag = (Tag) businessObject;
        return this.db.update(DATABASE_TABLE, populateBusinessObjectContentValues(tag), new StringBuilder().append("_id=").append(tag.getId()).toString(), null) > 0;
    }

    private ContentValues populateBusinessObjectContentValues(Tag tag) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", tag.getName());
        return contentValues;
    }
}
