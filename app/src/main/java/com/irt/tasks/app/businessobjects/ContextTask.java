package com.irt.tasks.app.businessobjects;

import com.irt.tasks.app.framework.business.IrtTasksBo;

public class ContextTask implements IrtTasksBo {
    public static final String CONTEXT_ID = "contextId";
    public static final String TASK_ID = "taskId";
    private Long contextId;
    private String contextName;
    private Long taskId;
    private String taskName;

    public ContextTask(Long contextId, Long taskId) {
        this.contextId = contextId;
        this.taskId = taskId;
    }

    public Long getContextId() {
        return this.contextId;
    }

    public void setContextId(Long contextId) {
        this.contextId = contextId;
    }

    public Long getTaskId() {
        return this.taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
}
