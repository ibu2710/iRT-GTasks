package com.irt.tasks.app.dataaccess;

import android.database.Cursor;
import com.irt.tasks.app.businessobjects.LevelColor;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.dataaccess.adapter.LevelColorDBAdapter;
import com.irt.tasks.app.dataaccess.adapter.LevelColorDBAdapterImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.ArrayList;
import java.util.List;

public class LevelColorDaoImpl implements LevelColorDao {
    private ConfigManager config;

    public List<Long> getAllDistinctTasksListIds() {
        LevelColorDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllDistinctTasksListIds();
        List<Long> tasksListIds = new ArrayList();
        while (cursor.moveToNext()) {
            tasksListIds.add(Long.valueOf(cursor.getLong(0)));
        }
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasksListIds;
    }

    public List<LevelColor> getAllByTasksListId(Long tasksListId) {
        LevelColorDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllByTasksListId(tasksListId);
        List<LevelColor> levelColor = buildLevelColors(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return levelColor;
    }

    public void saveOrUpdate(LevelColor levelColor) {
        LevelColorDBAdapter dBAdapter = openDbAdapter();
        if (levelColor.getId() == null) {
            levelColor.setId(Long.valueOf(dBAdapter.insert(levelColor)));
        } else {
            dBAdapter.update(levelColor);
        }
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    public void deleteAllbyTasksListId(Long tasksListId) {
        LevelColorDBAdapter dBAdapter = openDbAdapter();
        dBAdapter.deleteAllbyTasksListId(tasksListId);
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    private List<LevelColor> buildLevelColors(Cursor cursor) {
        List<LevelColor> levelColors = new ArrayList();
        while (cursor.moveToNext()) {
            levelColors.add(buildLevelColor(cursor));
        }
        return levelColors;
    }

    private LevelColor buildLevelColor(Cursor cursor) {
        Long id = Long.valueOf(cursor.getLong(0));
        Long tasksListId = Long.valueOf(cursor.getLong(1));
        Integer level = Integer.valueOf(cursor.getInt(2));
        Integer fontColor = Integer.valueOf(cursor.getInt(3));
        Integer fontStyle = Integer.valueOf(cursor.getInt(4));
        LevelColor levelColor = new LevelColor();
        levelColor.setId(id);
        levelColor.setTasksListId(tasksListId);
        levelColor.setLevel(level);
        levelColor.setFontColor(fontColor);
        levelColor.setFontStyle(fontStyle);
        return levelColor;
    }

    private LevelColorDBAdapter openDbAdapter() {
        this.config = ConfigManager.getInstance();
        LevelColorDBAdapter dBAdapter = new LevelColorDBAdapterImpl(this.config.getContext());
        dBAdapter.open();
        return dBAdapter;
    }

    private void closeDbAdapter(LevelColorDBAdapter dBAdapter) {
        dBAdapter.close();
    }
}
