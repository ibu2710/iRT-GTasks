package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.Account;
import java.util.List;

public interface AccountDao {
    void delete(Account account);

    Account get(Long l);

    List<Account> getAll();

    List<Account> getAllAutoSync();

    List<Account> getAllVisible();

    void saveOrUpdate(Account account);
}
