package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.TasksList;
import java.util.List;

public interface TasksListBp {
    int clearTrashBin();

    void copyList(Long l, Long l2);

    void copyListAsBranch(Long l, TasksList tasksList);

    void createTutorialTaskLists(Long l);

    void deleteTasksList(Long l, boolean z);

    void emptyTasksListAndSyncWithGoogle(TasksList tasksList);

    TasksList get(Long l);

    List<TasksList> getAllExcludeMarkedAsDelete();

    List<TasksList> getAllTasksListBySortOrder();

    void overwriteGoogleTasks(TasksList tasksList);

    void overwriteLocalTasks(TasksList tasksList);

    void restoreDeletedTasksList(Long l, String str);

    void saveOrUpdate(TasksList tasksList);

    void saveOrUpdateWithTimeStamp(TasksList tasksList);
}
