package com.irt.tasks.app.dataaccess.adapter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.irt.tasks.app.common.Constants;

public class GoogleDatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "google_tasks";
    private static final int DATABASE_VERSION = 1;

    GoogleDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(GoogleTaskContentProvider.DATABASE_CREATE);
        db.execSQL(GoogleTaskContentProvider.INDEX_ON_TASK_LIST_ID);
        db.execSQL(GoogleTaskContentProvider.INDEX_UNIQUE_ON_ID);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(Constants.TAG, "Upgrading Google database from version " + oldVersion + " to " + newVersion);
    }
}
