package com.irt.tasks.app.dataaccess;

import android.util.Log;
import com.dropbox.sync.android.DbxAccountManager;
import com.dropbox.sync.android.DbxDatastore;
import com.dropbox.sync.android.DbxDatastoreManager;
import com.dropbox.sync.android.DbxException;
import com.dropbox.sync.android.DbxException.Unauthorized;
import com.dropbox.sync.android.DbxRecord;
import com.dropbox.sync.android.DbxTable;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.List;

public class DropboxDatastoreDaoImpl implements RemoteDatastoreDao {
    private static final String COLUMN_FONT_BACKGROUND_COLOR = "fontBgColor";
    private static final String COLUMN_FONT_COLOR = "fontColor";
    private static final String COLUMN_FONT_STYLE = "fontStyle";
    private static final String COLUMN_HIGHLIGHT = "highlight";
    private static final String COLUMN_TASK_ID = "taskId";
    private ConfigManager config = ConfigManager.getInstance();
    private DbxAccountManager mAccountManager = DbxAccountManager.getInstance(this.config.getContext(), DropboxDaoImpl.APP_KEY, DropboxDaoImpl.SECRET_KEY);
    private DbxDatastoreManager mDatastoreManager;

    public DropboxDatastoreDaoImpl() {
        if (this.mAccountManager.hasLinkedAccount()) {
            try {
                this.mDatastoreManager = DbxDatastoreManager.forAccount(this.mAccountManager.getLinkedAccount());
            } catch (Unauthorized e) {
                Log.e(Constants.TAG, "Account was unlinked remotely");
            }
        }
        if (this.mDatastoreManager == null) {
            this.mDatastoreManager = DbxDatastoreManager.localManager(this.mAccountManager);
        }
    }

    public List<Task> getAll() {
        return null;
    }

    public void save(Task task) {
        try {
            DbxDatastore datastore = this.mDatastoreManager.openDefaultDatastore();
            DbxTable tasksTbl = datastore.getTable("tasks");
            DbxRecord record = tasksTbl.get("9NRzdvcvVJcAKraBZm902A");
            DbxRecord firstTask = tasksTbl.insert().set("taskId", task.getGtaskId()).set("highlight", task.isHighlight().booleanValue()).set("fontStyle", (long) task.getFontStyle().intValue()).set("fontColor", (long) task.getFontColor().intValue()).set("fontBgColor", (long) task.getFontBgColor().intValue());
            datastore.sync();
            datastore.close();
        } catch (DbxException e) {
            Log.e(Constants.TAG, "ERROR: Unable to update remote task due to " + e.getMessage());
        }
    }

    public void update(Task task) {
    }

    public void delete(Task task) {
    }

    public Task get(String gTaskId) {
        return null;
    }
}
