package com.irt.tasks.app.dataaccess;

import android.database.Cursor;
import com.irt.tasks.app.businessobjects.LevelState;
import com.irt.tasks.app.dataaccess.adapter.LevelStateDBAdapter;
import com.irt.tasks.app.dataaccess.adapter.LevelStateDBAdapterImpl;
import com.irt.tasks.app.framework.config.ConfigManager;

public class LevelStateDaoImpl implements LevelStateDao {
    ConfigManager config;

    public LevelState getLevelStateByTasksListId(Long tasksListId) {
        LevelStateDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.get(tasksListId.longValue());
        LevelState levelState = null;
        if (cursor != null && cursor.moveToNext()) {
            levelState = buildLevelState(cursor);
        }
        cursor.close();
        closeDbAdapter(dBAdapter);
        return levelState;
    }

    public void saveOrUpdate(LevelState levelState) {
        LevelStateDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.get(levelState.getTasksListId().longValue());
        if (cursor.getCount() == 0) {
            dBAdapter.insert(levelState);
        } else {
            dBAdapter.update(levelState);
        }
        cursor.close();
        closeDbAdapter(dBAdapter);
    }

    private LevelState buildLevelState(Cursor cursor) {
        Long tasksListId = Long.valueOf(cursor.getLong(0));
        String zoomTaskIdStr = cursor.getString(1);
        Integer level = Integer.valueOf(cursor.getInt(2));
        Integer position = Integer.valueOf(cursor.getInt(3));
        LevelState levelState = new LevelState();
        levelState.setTasksListId(tasksListId);
        levelState.setZoomTaskIdStr(zoomTaskIdStr);
        levelState.setLevel(level);
        levelState.setPosition(position);
        return levelState;
    }

    private LevelStateDBAdapter openDbAdapter() {
        this.config = ConfigManager.getInstance();
        LevelStateDBAdapter dBAdapter = new LevelStateDBAdapterImpl(this.config.getContext());
        dBAdapter.open();
        return dBAdapter;
    }

    private void closeDbAdapter(LevelStateDBAdapter dBAdapter) {
        dBAdapter.close();
    }
}
