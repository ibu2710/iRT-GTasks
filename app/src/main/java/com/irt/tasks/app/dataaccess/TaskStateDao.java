package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.TaskState;
import java.util.List;

public interface TaskStateDao {
    int deleteAllByTasksListId(Long l);

    List<TaskState> getAll();

    List<TaskState> getAllByTasksListId(Long l);

    void save(TaskState taskState);
}
