package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.irt.tasks.app.businessobjects.Project;
import com.irt.tasks.app.framework.business.IrtTasksBo;

public class ProjectDBAdapterImpl implements ProjectDBAdapter {
    private static final String[] ALL_COLUMNS = new String[]{"_id", "title", "description"};
    public static final String DATABASE_CREATE = "create table project (_id integer primary key autoincrement, title text null, description text null);";
    public static final String DATABASE_TABLE = "project";
    public static final String DESCRIPTION = "description";
    public static final String INDEX_UNIQUE_ON_TITLE = "CREATE UNIQUE INDEX idx__unique_project_title ON project (title) ";
    public static final String ROW_ID = "_id";
    public static final String TITLE = "title";
    private DatabaseHelper DBHelper;
    private final Context context;
    private SQLiteDatabase db;

    public ProjectDBAdapterImpl(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(this.context);
    }

    public ProjectDBAdapterImpl open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.DBHelper.close();
    }

    public boolean delete(long rowId) {
        return this.db.delete(DATABASE_TABLE, new StringBuilder().append("_id=").append(rowId).toString(), null) > 0;
    }

    public Cursor get(long rowId) throws SQLException {
        Cursor mCursor = this.db.query(true, DATABASE_TABLE, ALL_COLUMNS, "_id=" + rowId, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public boolean isTitleUsed(String title, Long projectId) {
        String idCriteria = " ";
        if (projectId != null) {
            idCriteria = " AND _id != " + projectId;
        }
        Cursor mCursor = this.db.query(true, DATABASE_TABLE, ALL_COLUMNS, "lower(title)= '" + title.toLowerCase().trim() + "' " + idCriteria, null, null, null, null, null);
        if (mCursor == null) {
            return false;
        }
        if (mCursor.getCount() > 0) {
            return true;
        }
        return false;
    }

    public Cursor getAll() {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, null, null, null, null, "lower(title)");
    }

    public long insert(IrtTasksBo businessObject) {
        return this.db.insert(DATABASE_TABLE, null, populateBusinessObjectContentValues((Project) businessObject));
    }

    public boolean update(IrtTasksBo businessObject) {
        Project project = (Project) businessObject;
        return this.db.update(DATABASE_TABLE, populateBusinessObjectContentValues(project), new StringBuilder().append("_id=").append(project.getId()).toString(), null) > 0;
    }

    private ContentValues populateBusinessObjectContentValues(Project project) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", project.getTitle());
        contentValues.put("description", project.getDescription());
        return contentValues;
    }
}
