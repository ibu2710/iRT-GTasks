package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.Project;
import java.util.List;

public interface ProjectDao {
    void delete(Project project);

    Project get(Long l);

    List<Project> getAll();

    boolean isTitleUsed(String str, Long l);

    void saveOrUpdate(Project project);
}
