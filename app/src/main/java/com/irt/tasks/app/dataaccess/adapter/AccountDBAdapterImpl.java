package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.framework.business.IrtTasksBo;
import com.irt.tasks.app.framework.security.AndroidCrypto;
import com.irt.tasks.app.framework.security.SecurityKey;

public class AccountDBAdapterImpl implements AccountDBAdapter {
    public static final String IS_NEW_GOOGLE_ID = "is_new_google_id";
    private static final String[] ALL_COLUMNS = new String[]{"_id", "email", "password", "domain", "auto_sync", "enable", "note", "sort_position", "font_style", "font_color", "font_bg_color", IS_NEW_GOOGLE_ID};
    public static final String AUTO_SYNC = "auto_sync";
    public static final String DATABASE_CREATE = "create table account (_id integer primary key autoincrement, email text not null, password text null, domain text null, auto_sync integer default 0 not null, enable integer default 0 not null, note text null, sort_position integer default 0 not null, font_style integer default 0 not null, font_color integer default 0 not null, font_bg_color integer default 0 not null, is_new_google_id integer default 0 not null);";
    public static final String DATABASE_TABLE = "account";
    public static final String DOMAIN = "domain";
    public static final String EMAIL = "email";
    public static final String ENABLED = "enable";
    public static final String FONT_BACKGROUND_COLOR = "font_bg_color";
    public static final String FONT_COLOR = "font_color";
    public static final String FONT_STYLE = "font_style";
    public static final String NOTE = "note";
    public static final String PASSWORD = "password";
    public static final String ROW_ID = "_id";
    public static final String SORT_POSITION = "sort_position";
    private DatabaseHelper DBHelper;
    private final Context context;
    private SQLiteDatabase db;

    public AccountDBAdapterImpl(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(this.context);
    }

    public AccountDBAdapter open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.DBHelper.close();
    }

    public boolean delete(long rowId) {
        return this.db.delete("account", new StringBuilder().append("_id=").append(rowId).toString(), null) > 0;
    }

    public Cursor get(long rowId) throws SQLException {
        Cursor mCursor = this.db.query(true, "account", ALL_COLUMNS, "_id=" + rowId, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor getAll() {
        return this.db.query("account", ALL_COLUMNS, null, null, null, null, "sort_position");
    }

    public long insert(IrtTasksBo businessObject) {
        return this.db.insert("account", null, populateBusinessObjectContentValues((Account) businessObject));
    }

    public boolean update(IrtTasksBo businessObject) {
        Account account = (Account) businessObject;
        return this.db.update("account", populateBusinessObjectContentValues(account), new StringBuilder().append("_id=").append(account.getId()).toString(), null) > 0;
    }

    private ContentValues populateBusinessObjectContentValues(Account account) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("email", account.getEmail());
        contentValues.put("password", encryptPassword(account.getPassword()));
        contentValues.put("domain", account.getDomain());
        contentValues.put("sort_position", account.getSortPosition());
        contentValues.put("auto_sync", AppUtil.getBooleanAs1or0(account.isAutoSync()));
        contentValues.put("enable", account.isEnable());
        contentValues.put("note", account.getNote());
        contentValues.put("font_style", account.getFontStyle());
        contentValues.put("font_color", account.getFontColor());
        contentValues.put("font_bg_color", account.getFontBgColor());
        contentValues.put(IS_NEW_GOOGLE_ID, account.isNewGoogleId());
        return contentValues;
    }

    private String encryptPassword(String clearPassword) {
        if (StringUtil.isNullOrEmpty(clearPassword)) {
            return null;
        }
        try {
            return AndroidCrypto.encrypt(SecurityKey.ENCRYPTION_MASTER_KEY, clearPassword);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public Cursor getAllVisible(boolean isShowInvisible) {
        return this.db.query("account", ALL_COLUMNS, "1 = " + (isShowInvisible ? "1.0" : "0") + " OR " + "enable" + "= 1", null, null, null, "sort_position");
    }

    public Cursor getAllAutoSync() {
        return this.db.query("account", ALL_COLUMNS, "auto_sync= 1", null, null, null, "sort_position");
    }
}
