package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.irt.tasks.app.businessobjects.Reminder;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.framework.business.IrtTasksBo;

public class ReminderDBAdapterImpl implements ReminderDBAdapter {
    public static final String DATABASE_CREATE = "create table reminder (task_id integer not null, enabled integer default 1 not null, days_earlier integer default 0 not null, date_time integer default 0 not null, repeat_from_completion_date integer default 0 not null, repeat_rule text null); ";
    public static final String DATABASE_TABLE = "reminder";
    public static final String DATE_TIME = "date_time";
    public static final String DAYS_EARLIER = "days_earlier";
    public static final String ENABLED = "enabled";
    public static final String INDEX_ON_TASK_ID = "CREATE INDEX idx_reminder_task_id ON reminder (task_id) ";
    public static final String REPEAT_FROM_COMPLETION_DATE = "repeat_from_completion_date";
    public static final String REPEAT_RULE = "repeat_rule";
    public static final String TASK_ID = "task_id";
    private DatabaseHelper DBHelper;
    private static final String[] ALL_COLUMNS = new String[]{"task_id", "enabled", DAYS_EARLIER, DATE_TIME, REPEAT_FROM_COMPLETION_DATE, REPEAT_RULE};
    private final Context context;
    private SQLiteDatabase db;

    public ReminderDBAdapterImpl(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(this.context);
    }

    public ReminderDBAdapter open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.DBHelper.close();
    }

    public boolean delete(long rowId) {
        return this.db.delete(DATABASE_TABLE, new StringBuilder().append("task_id=").append(rowId).toString(), null) > 0;
    }

    public Cursor get(long rowId) throws SQLException {
        Cursor mCursor = this.db.query(true, DATABASE_TABLE, ALL_COLUMNS, "task_id=" + rowId, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor getAll() {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, null, null, null, null, null);
    }

    public long insert(IrtTasksBo businessObject) {
        return this.db.insert(DATABASE_TABLE, null, populateBusinessObjectContentValues((Reminder) businessObject));
    }

    public boolean update(IrtTasksBo businessObject) {
        Reminder reminder = (Reminder) businessObject;
        return this.db.update(DATABASE_TABLE, populateBusinessObjectContentValues(reminder), new StringBuilder().append("task_id=").append(reminder.getTaskId()).toString(), null) > 0;
    }

    private ContentValues populateBusinessObjectContentValues(Reminder reminder) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("task_id", reminder.getTaskId());
        contentValues.put("enabled", AppUtil.getBooleanAs1or0(reminder.isEnabled()));
        contentValues.put(DAYS_EARLIER, reminder.getDaysEarlier());
        contentValues.put(DATE_TIME, reminder.getDateTime());
        contentValues.put(REPEAT_FROM_COMPLETION_DATE, AppUtil.getBooleanAs1or0(reminder.isRepeatFromCompletionDate()));
        contentValues.put(REPEAT_RULE, reminder.getRepeatRule());
        return contentValues;
    }

    public Cursor getAllByTasksListId(Long tasksListId) {
        return this.db.rawQuery("SELECT " + getRawColumns() + " from " + DATABASE_TABLE + " JOIN " + "task" + " ON " + getRawColumn("task_id") + " = " + getTaskRawColumn("_id") + " WHERE " + getTaskRawColumn("tasks_list_id") + " = " + tasksListId, null);
    }

    String getRawColumns() {
        return DatabaseUtil.getRawColumns(DATABASE_TABLE, ALL_COLUMNS);
    }

    String getRawColumn(String column) {
        return DatabaseUtil.getRawColumn(TasksListDBAdapterImpl.DATABASE_TABLE, column);
    }

    String getTaskRawColumn(String column) {
        return DatabaseUtil.getRawColumn("task", column);
    }
}
