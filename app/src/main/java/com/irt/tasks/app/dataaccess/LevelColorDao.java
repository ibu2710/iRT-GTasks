package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.LevelColor;
import java.util.List;

public interface LevelColorDao {
    void deleteAllbyTasksListId(Long l);

    List<LevelColor> getAllByTasksListId(Long l);

    List<Long> getAllDistinctTasksListIds();

    void saveOrUpdate(LevelColor levelColor);
}
