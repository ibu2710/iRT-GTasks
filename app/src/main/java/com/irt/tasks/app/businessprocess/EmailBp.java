package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.Email;

public interface EmailBp {
    Email get();

    void saveOrUpdate(Email email);
}
