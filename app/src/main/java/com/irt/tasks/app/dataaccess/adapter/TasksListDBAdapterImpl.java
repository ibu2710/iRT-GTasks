package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.framework.business.IrtTasksBo;

public class TasksListDBAdapterImpl implements TasksListDBAdapter {
    public static final String ACCOUNT_ID = "account_id";
    public static final String ACTIVE_TASKS_LIST = "active_tasks_list";
    public static final String AUTO_SYNC = "auto_sync";
    public static final String COLOR_TASKS = "color_tasks";
    public static final String DATABASE_CREATE = "create table tasks_list (_id integer primary key autoincrement, gtasks_list_name text null, name text null, gtasks_list_id text null, deleted integer default 0 not null, last_modified integer default 0 not null, sort_position integer default 0 not null, account_id integer not null, font_style integer default 0 not null, font_color integer default 0 not null, font_bg_color integer default 0 not null, inTrashBin integer default 0 not null, auto_sync integer default 1 not null, visible integer default 1 not null, task_last_modified integer default 0 not null, note text null, highlight integer default 0 not null, color_tasks integer default 0 not null, active_tasks_list integer default 0 not null, show_note integer default 1 not null, show_due_date integer default 1 not null, new_task_position integer default 0 not null, exclude_from_delete integer default 0 not null, secure integer default 0 not null);";
    public static final String DATABASE_TABLE = "tasks_list";
    public static final String DELETED = "deleted";
    public static final String EXCLUDE_FROM_DELETE = "exclude_from_delete";
    public static final String FONT_BACKGROUND_COLOR = "font_bg_color";
    public static final String FONT_COLOR = "font_color";
    public static final String FONT_STYLE = "font_style";
    public static final String GTASKS_LIST_ID = "gtasks_list_id";
    public static final String GTASKS_LIST_NAME = "gtasks_list_name";
    public static final String HIGHLIGHT = "highlight";
    public static final String INDEX_ON_ACCOUNT_ID = "CREATE INDEX idx_tasks_list_account_id ON tasks_list (account_id) ";
    public static final String INDEX_ON_DELETED = "CREATE INDEX idx_tasks_list_deleted ON tasks_list (deleted) ";
    public static final String INDEX_ON_TRASH_BIN = "CREATE INDEX idx_tasks_list_inTrashBin ON tasks_list (inTrashBin) ";
    public static final String INDEX_ON_VISIBLE = "CREATE INDEX idx_tasks_list_visible ON tasks_list (visible) ";
    public static final String IN_TRASH_BIN = "inTrashBin";
    public static final String LAST_MODIFIED = "last_modified";
    public static final String NAME = "name";
    public static final String NEW_TASK_POSITION = "new_task_position";
    public static final String NOTE = "note";
    public static final String ROW_ID = "_id";
    public static final String SECURE = "secure";
    public static final String SHOW_DUE_DATE = "show_due_date";
    public static final String SHOW_NOTE = "show_note";
    public static final String SORT_POSITION = "sort_position";
    public static final String TASK_LAST_MODIFIED = "task_last_modified";
    public static final String VISIBLE = "visible";
    private static final String[] ALL_COLUMNS = new String[]{"_id", "name", GTASKS_LIST_NAME, GTASKS_LIST_ID, "deleted", "last_modified", "sort_position", ACCOUNT_ID, "font_style", "font_color", "font_bg_color", "inTrashBin", "auto_sync", "visible", TASK_LAST_MODIFIED, "note", "highlight", COLOR_TASKS, ACTIVE_TASKS_LIST, "show_note", SHOW_DUE_DATE, NEW_TASK_POSITION, "exclude_from_delete", "secure"};
    private DatabaseHelper DBHelper;
    private final Context context;
    private SQLiteDatabase db;

    public TasksListDBAdapterImpl(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(this.context);
    }

    public TasksListDBAdapter open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.DBHelper.close();
    }

    public boolean delete(long rowId) {
        return this.db.delete(DATABASE_TABLE, new StringBuilder().append("_id=").append(rowId).toString(), null) > 0;
    }

    public Cursor get(long rowId) throws SQLException {
        Cursor mCursor = this.db.query(true, DATABASE_TABLE, ALL_COLUMNS, "_id=" + rowId, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor getAll() {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, "inTrashBin != 1 ", null, null, null, null);
    }

    public Cursor getAllIncludingDeletedAndTrashBin() {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, null, null, null, null, null);
    }

    public long insert(IrtTasksBo businessObject) {
        return this.db.insert(DATABASE_TABLE, null, populateBusinessObjectContentValues((TasksList) businessObject));
    }

    public boolean update(IrtTasksBo businessObject) {
        TasksList tasksList = (TasksList) businessObject;
        return this.db.update(DATABASE_TABLE, populateBusinessObjectContentValues(tasksList), new StringBuilder().append("_id=").append(tasksList.getId()).toString(), null) > 0;
    }

    private ContentValues populateBusinessObjectContentValues(TasksList tasksList) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", tasksList.getName());
        contentValues.put(GTASKS_LIST_NAME, tasksList.getGtasksListName());
        contentValues.put(GTASKS_LIST_ID, tasksList.getGtasksListId());
        contentValues.put("deleted", AppUtil.getBooleanAs1or0(tasksList.isDeleted()));
        contentValues.put("last_modified", tasksList.getLastModified());
        contentValues.put("sort_position", tasksList.getSortPosition());
        contentValues.put(ACCOUNT_ID, tasksList.getAccountId());
        contentValues.put("font_style", tasksList.getFontStyle());
        contentValues.put("font_color", tasksList.getFontColor());
        contentValues.put("font_bg_color", tasksList.getFontBgColor());
        contentValues.put("inTrashBin", AppUtil.getBooleanAs1or0(tasksList.isInTrashBin()));
        contentValues.put("auto_sync", AppUtil.getBooleanAs1or0(tasksList.isAutoSync()));
        contentValues.put("visible", AppUtil.getBooleanAs1or0(tasksList.isVisible()));
        contentValues.put(TASK_LAST_MODIFIED, tasksList.getTaskLastModified());
        contentValues.put("note", tasksList.getNote());
        contentValues.put("highlight", AppUtil.getBooleanAs1or0(tasksList.isHighlight()));
        contentValues.put(COLOR_TASKS, AppUtil.getBooleanAs1or0(tasksList.isColorTasks()));
        contentValues.put(ACTIVE_TASKS_LIST, AppUtil.getBooleanAs1or0(tasksList.istActiveTasksList()));
        contentValues.put("show_note", AppUtil.getBooleanAs1or0(tasksList.isShowNote()));
        contentValues.put(SHOW_DUE_DATE, AppUtil.getBooleanAs1or0(tasksList.isShowDueDate()));
        contentValues.put(NEW_TASK_POSITION, tasksList.getNewTaskPosition());
        contentValues.put("exclude_from_delete", AppUtil.getBooleanAs1or0(tasksList.isExcludeFromDelete()));
        contentValues.put("secure", AppUtil.getBooleanAs1or0(tasksList.isSecure()));
        return contentValues;
    }

    public Cursor getAllByAccountIdExcludeMarkedAsDelete(Long accountId) {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, "account_id = " + accountId + " AND " + "deleted" + "!= 1" + " AND " + "inTrashBin" + " != 1", null, null, null, "lower(name)");
    }

    public Cursor getAllByAccountId(Long accountId) {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, "account_id = " + accountId + " AND " + "inTrashBin" + " != 1", null, null, null, null);
    }

    public boolean forceDeleteByAccountId(Long accountId) {
        return this.db.delete(DATABASE_TABLE, new StringBuilder().append("account_id=").append(accountId).toString(), null) > 0;
    }

    public int deleteTasksListInTrashBin() {
        return this.db.delete(DATABASE_TABLE, "inTrashBin = 1", null);
    }

    public Cursor getAllSortedExcludeMarkedAsDelete(String orderBy, boolean isShowInvisible, boolean isDescending) {
        String isShowInvisibleStr = isShowInvisible ? "1.0" : "0";
        String descending = "";
        if (isDescending) {
            descending = " desc ";
        }
        if ("name".equals(orderBy)) {
            orderBy = " ORDER BY LOWER(" + getRawColumn("name") + ")";
        } else if ("sort_position".equals(orderBy)) {
            orderBy = " ORDER BY " + getRawColumn("sort_position");
        } else {
            orderBy = " ORDER BY " + getRawColumn(orderBy) + descending + ", LOWER(" + getRawColumn("name") + ")";
        }
        return this.db.rawQuery("SELECT " + getRawColumns() + " from " + DATABASE_TABLE + " JOIN " + "account" + " ON " + getRawColumn(ACCOUNT_ID) + " = " + getAccountRowColumn("_id") + " WHERE " + "( 1 = " + isShowInvisibleStr + " OR " + getAccountRowColumn("enable") + " = 1) " + "AND " + "inTrashBin" + " = 0 " + "AND " + "deleted" + " = 0 " + orderBy, null);
    }

    String getRawColumns() {
        return DatabaseUtil.getRawColumns(DATABASE_TABLE, ALL_COLUMNS);
    }

    String getRawColumn(String column) {
        return DatabaseUtil.getRawColumn(DATABASE_TABLE, column);
    }

    String getAccountRowColumn(String column) {
        return DatabaseUtil.getRawColumn("account", column);
    }
}
