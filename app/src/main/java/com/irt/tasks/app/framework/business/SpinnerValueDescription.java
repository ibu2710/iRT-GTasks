package com.irt.tasks.app.framework.business;

public class SpinnerValueDescription {
    public String description = "";
    public int value = 0;

    public SpinnerValueDescription(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public String toString() {
        return this.description;
    }
}
