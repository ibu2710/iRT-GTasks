package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.Task;
import java.util.List;

public interface RemoteDatastoreDao {
    void delete(Task task);

    Task get(String str);

    List<Task> getAll();

    void save(Task task);

    void update(Task task);
}
