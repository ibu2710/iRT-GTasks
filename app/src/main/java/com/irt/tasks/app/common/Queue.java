package com.irt.tasks.app.common;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

public class Queue {
    private List theList = Collections.synchronizedList(new LinkedList());

    public void add(Object item) {
        this.theList.add(item);
    }

    public Object remove() {
        if (!isEmpty()) {
            return this.theList.remove(0);
        }
        throw new NoSuchElementException();
    }

    public Object front() {
        if (isEmpty()) {
            return null;
        }
        return this.theList.get(0);
    }

    public boolean isEmpty() {
        return this.theList.size() == 0;
    }
}
