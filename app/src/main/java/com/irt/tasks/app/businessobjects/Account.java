package com.irt.tasks.app.businessobjects;

import com.irt.tasks.app.framework.business.IrtTasksBo;

public class Account implements IrtTasksBo {
    public static final String AUTO_SYNC = "autoSync";
    public static final String DOMAIN = "domain";
    public static final String EMAIL = "email";
    public static final String ENABLED = "enable";
    public static final String FONT_BACKGROUND_COLOR = "fontBgColor";
    public static final String FONT_COLOR = "fontColor";
    public static final String FONT_STYLE = "fontStyle";
    public static final String IS_NEW_GOOGLE_ID = "isNewGoogleId";
    public static final String NOTE = "note";
    public static final String PASSWORD = "password";
    public static final String ROW_ID = "id";
    public static final String SORT_POSITION = "sortPosition";
    private Boolean autoSync = Boolean.valueOf(true);
    private String domain;
    private String email;
    private Boolean enable = Boolean.valueOf(true);
    private Integer fontBgColor = Integer.valueOf(0);
    private Integer fontColor = Integer.valueOf(0);
    private Integer fontStyle = Integer.valueOf(0);
    private Long id;
    private Boolean isNewGoogleId = Boolean.valueOf(false);
    private String note;
    private String password;
    private Integer sortPosition = Integer.valueOf(0);

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDomain() {
        return this.domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Boolean isAutoSync() {
        return this.autoSync;
    }

    public void setAutoSync(Boolean autoSync) {
        this.autoSync = autoSync;
    }

    public Boolean isEnable() {
        return this.enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Integer getFontStyle() {
        return this.fontStyle;
    }

    public void setFontStyle(Integer fontStyle) {
        this.fontStyle = fontStyle;
    }

    public Integer getFontColor() {
        return this.fontColor;
    }

    public void setFontColor(Integer fontColor) {
        this.fontColor = fontColor;
    }

    public Integer getFontBgColor() {
        return this.fontBgColor;
    }

    public void setFontBgColor(Integer fontBgColor) {
        this.fontBgColor = fontBgColor;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSortPosition() {
        return this.sortPosition;
    }

    public void setSortPosition(Integer sortPosition) {
        this.sortPosition = sortPosition;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean isNewGoogleId() {
        return this.isNewGoogleId;
    }

    public void setNewGoogleId(Boolean newGoogleId) {
        this.isNewGoogleId = newGoogleId;
    }
}
