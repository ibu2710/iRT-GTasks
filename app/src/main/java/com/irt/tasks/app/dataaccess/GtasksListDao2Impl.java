package com.irt.tasks.app.dataaccess;

import com.google.api.services.tasks.Tasks;
import com.google.api.services.tasks.model.TaskList;
import com.google.api.services.tasks.model.TaskLists;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GtasksListDao2Impl implements GtasksListDao {
    ConfigManager config;

    public List<TasksList> getAllTasksLists() {
        this.config = ConfigManager.getInstance();
        List<TasksList> tasksLists = new ArrayList();
        Tasks service = this.config.getGoogleTaskService2();
        try {
            TaskLists gTasksLists = (TaskLists) service.tasklists().list().execute();
            TaskList defGTaskList = getDefaultGoogleTasksList(service);
            if (service != null && gTasksLists.size() > 0) {
                for (TaskList googleTaskListInfo : gTasksLists.getItems()) {
                    tasksLists.add(buildTasksList(ArchitectureContext.getAccount(), defGTaskList.getId(), googleTaskListInfo));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tasksLists;
    }

    public TasksList getTasksList(String tasksListId) {
        if (!StringUtil.isNullOrEmpty(tasksListId)) {
            this.config = ConfigManager.getInstance();
            Tasks service = this.config.getGoogleTaskService2();
            try {
                return buildTasksList(ArchitectureContext.getAccount(), getDefaultGoogleTasksList(service).getId(), (TaskList) service.tasklists().get(tasksListId).execute());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void deleteTasksList(TasksList tasksList) {
        this.config = ConfigManager.getInstance();
        try {
            this.config.getGoogleTaskService2().tasklists().delete(tasksList.getGtasksListId()).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void renameTasksList(TasksList tasksList) {
        this.config = ConfigManager.getInstance();
        Tasks service = this.config.getGoogleTaskService2();
        try {
            TaskList gTaskList = (TaskList) service.tasklists().get(tasksList.getGtasksListId()).execute();
            gTaskList.setTitle(tasksList.getName());
            service.tasklists().update(gTaskList.getId(), gTaskList).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void insertTasksList(TasksList tasksList) {
        this.config = ConfigManager.getInstance();
        TaskList gTaskList = new TaskList();
        gTaskList.setTitle(tasksList.getName());
        try {
            tasksList.setGtasksListId(((TaskList) this.config.getGoogleTaskService2().tasklists().insert(gTaskList).execute()).getId());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private TasksList buildTasksList(Account account, String activeTaskGTasksListId, TaskList googleTaskListInfo) {
        TasksList tasksList = new TasksList();
        tasksList.setName(googleTaskListInfo.getTitle());
        tasksList.setGtasksListId(googleTaskListInfo.getId());
        tasksList.setAccountId(account.getId());
        tasksList.setActiveTasksList(Boolean.valueOf(googleTaskListInfo.getId().equals(activeTaskGTasksListId)));
        return tasksList;
    }

    private TaskList getDefaultGoogleTasksList(Tasks service) throws IOException {
        return (TaskList) service.tasklists().get("@default").execute();
    }
}
