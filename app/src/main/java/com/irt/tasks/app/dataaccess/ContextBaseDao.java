package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.ContextBase;
import java.util.List;

public interface ContextBaseDao {
    void delete(ContextBase contextBase);

    List<ContextBase> getAll();

    List<ContextBase> getAllExcludingSpecificContext(Long l);

    boolean isNameUsed(String str, Long l);

    void saveOrUpdate(ContextBase contextBase);
}
