package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.ContextIncludes;
import java.util.List;

public interface ContextIncludesDao {
    int deleteAllByContextBaseId(Long l);

    List<ContextIncludes> getAllByContextBaseId(Long l);

    List<Long> getAllDistinctContextBasesIds();

    void save(ContextIncludes contextIncludes);
}
