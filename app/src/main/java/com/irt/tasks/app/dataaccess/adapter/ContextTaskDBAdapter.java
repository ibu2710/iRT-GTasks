package com.irt.tasks.app.dataaccess.adapter;

import android.database.Cursor;

public interface ContextTaskDBAdapter extends DBAdapter {
    int deleteAllWhereTaskDoesNotExist();

    int deleteAllbyContextBaseId(Long l);

    int deleteAllbyTaskId(Long l);

    Cursor getAllByContextBaseId(Long l);

    Cursor getAllByTaskId(Long l);

    Cursor getAllDistinctContextBasesIds();
}
