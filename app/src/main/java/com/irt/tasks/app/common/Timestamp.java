package com.irt.tasks.app.common;

import java.io.Serializable;

public final class Timestamp implements Serializable {
    private Object value;

    public static final Timestamp create(Object value) throws AssertFailedException {
        return new Timestamp(value);
    }

    public Object getValue() {
        return this.value;
    }

    public String toString() {
        return "Timestamp: " + this.value;
    }

    private Timestamp(Object value) throws AssertFailedException {
        Assert.notNull(value, "value");
        this.value = value;
    }
}
