package com.irt.tasks.app.businessprocess;

import android.util.Log;
import com.irt.tasks.app.businessobjects.ImportExportFile;
import com.irt.tasks.app.businessobjects.ImportExportFile.FileDateDescendingComparator;
import com.irt.tasks.app.businessobjects.ImportExportFile.FilenameComparator;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.CSVReader;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.dataaccess.DropboxDaoImpl;
import com.irt.tasks.app.dataaccess.FileDao;
import com.irt.tasks.app.dataaccess.SdCardDaoImpl;
import com.irt.tasks.app.dataaccess.TaskDao;
import com.irt.tasks.app.dataaccess.TaskDaoImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExternalApplicationsBpImpl implements ExternalApplicationsBp {
    public static final int BONSAI_CSV_NUMBER_OF_FIELDS = 15;
    public static final int ERROR_BONSAI_CSV_NUMBER_OF_FIELDS_NOT_CORRECT = 2;
    public static final int ERROR_REMOTE_FILE_NOT_ACCESSIBLE = 1;
    public static final int NO_ERROR = 0;
    FileDao fileDao;
    TaskBp taskBp = new TaskBpImpl();
    TaskDao taskDao = new TaskDaoImpl();
    TasksListBp tasksListBp = new TasksListBpImpl();

    private FileDao getFileDao() {
        if (ConfigManager.getInstance().isDropboxMode()) {
            return new DropboxDaoImpl();
        }
        return new SdCardDaoImpl();
    }

    private String getImportExportDirectory() {
        if (ConfigManager.getInstance().isDropboxMode()) {
            return Constants.DROPBOX_BONSAI_RELATIVE_DIRECTORY;
        }
        return AppUtil.getBonsaiSdCardDirectory();
    }

    public boolean exportToRemoteServer(TasksList tasksList, String toRemoteFilename, int dateFormat, boolean isTaskIdInContact) {
        this.fileDao = getFileDao();
        ExternalApplicationsBpHelper.writeTaskToBonsaiCsvFile(this.taskBp.getTasksByLevel(null, 20, false, tasksList, true), AppUtil.getBonsaiDateFormat(dateFormat), isTaskIdInContact);
        try {
            File file = new File(ExternalApplicationsBpHelper.getTempFilePath());
            this.fileDao.putFileOverwrite(ExternalApplicationsBpHelper.getTempFilePath(), getImportExportDirectory() + toRemoteFilename);
            file.delete();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return false;
        }
    }

    public int importReplaceListFromRemoteServer(TasksList tasksList, String fromRemoteFilename) {
        ConfigManager config = ConfigManager.getInstance();
        List<Task> originalTasks = this.taskDao.getAllTasksByTasksListId(tasksList.getId(), null, false, false, false);
        int result = performImportFromRemoteServer(tasksList, fromRemoteFilename, originalTasks, null);
        if (result == 0) {
            for (Task task : originalTasks) {
                Log.i(Constants.TAG, "Deleting task ...[" + task.getName() + "]");
                this.taskDao.delete(task);
            }
            AppUtil.scheduleAllAlarms(config.getContext());
        }
        return result;
    }

    private void setDecoratorsForImportedTask(Task importedTask, List<Task> originalTasks) {
        for (Task task : originalTasks) {
            if (importedTask.getId() != null && task.getId().equals(importedTask.getId())) {
                importedTask.setDecoratorsForGoogleFromTask(task);
                importedTask.setDecoratorsExtra(task);
                originalTasks.remove(task);
                return;
            }
        }
        if (importedTask.getId() != null) {
            Log.i(Constants.TAG, "************* TAMPERRED importedTask ...[" + importedTask.toString() + "]");
            importedTask.setId(null);
        }
    }

    public int importAppendToListFromRemoteServer(TasksList tasksList, String fromRemoteFilename) {
        return performImportFromRemoteServer(tasksList, fromRemoteFilename, null, getLastTaskWithParentZeroId(this.taskDao.getAllTasksByTasksListId(tasksList.getId(), null, false, false, false)));
    }

    private Task getLastTaskWithParentZeroId(List<Task> originalTasks) {
        Task lastTaskWithParentZero = null;
        for (Task task : originalTasks) {
            if (task.getParentTaskId().equals(Long.valueOf(0))) {
                lastTaskWithParentZero = task;
            }
        }
        return lastTaskWithParentZero;
    }

    public int importToNewListFromRemoteServer(Long accoundId, String newTasksListName, String fromRemoteFilename) {
        TasksList newTasksList = createNewTasksList(accoundId, newTasksListName);
        this.tasksListBp.saveOrUpdateWithTimeStamp(newTasksList);
        return performImportFromRemoteServer(newTasksList, fromRemoteFilename, null, null);
    }

    public int performImportFromRemoteServer(TasksList tasksList, String fromRemoteFilename, List<Task> originalTasks, Task previousTask) {
        this.fileDao = getFileDao();
        try {
            InputStream inputStream = this.fileDao.getFileStream(getImportExportDirectory() + fromRemoteFilename);
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            if (previousTask == null) {
                previousTask = createStarterTask();
            }
            Map<Integer, Task> levelToTaskMap = new HashMap();
            CSVReader csvReader = new CSVReader(r);
            while (true) {
                String[] taskFields = csvReader.readNext();
                if (taskFields == null) {
                    inputStream.close();
                    this.fileDao.closeFile();
                    csvReader.close();
                    return 0;
                } else if (isNumberOfBonsaiFieldsFromCsvValid(taskFields)) {
                    return 2;
                } else {
                    previousTask = performSaveTaskFromBonsaiCsv(tasksList, previousTask, levelToTaskMap, taskFields, originalTasks);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return 1;
        }
    }

    public boolean exportBranchToRemoteServer(Long tasksId, String toRemoteFilename) {
        return false;
    }

    public List<String> getListOfRemoteFilesForImport() {
        return null;
    }

    public List<String> getRemoteFilesListForImport() {
        ConfigManager config = ConfigManager.getInstance();
        this.fileDao = getFileDao();
        List<ImportExportFile> importedExportedfiles = this.fileDao.getListOfFiles(getImportExportDirectory());
        if (importedExportedfiles == null) {
            importedExportedfiles = new ArrayList();
        }
        if (config.isSortFilenamesByDate()) {
            Collections.sort(importedExportedfiles, new FileDateDescendingComparator());
        } else {
            Collections.sort(importedExportedfiles, new FilenameComparator());
        }
        List<String> files = new ArrayList();
        for (ImportExportFile importExportFile : importedExportedfiles) {
            files.add(importExportFile.getFilename());
        }
        return files;
    }

    private boolean isNumberOfBonsaiFieldsFromCsvValid(String[] taskFields) {
        return taskFields.length != 15;
    }

    private Task performSaveTaskFromBonsaiCsv(TasksList newTasksList, Task previousTask, Map<Integer, Task> levelToTaskMap, String[] taskFields, List<Task> originalTasks) {
        boolean isReplaceTheExistingList = originalTasks != null;
        Task task = new Task();
        ExternalApplicationsBpHelper.bonsaiCsvToTask(newTasksList, previousTask, levelToTaskMap, taskFields, task, isReplaceTheExistingList);
        if (isReplaceTheExistingList) {
            setDecoratorsForImportedTask(task, originalTasks);
        }
        this.taskDao.saveOrUpdateWithTimeStamp(task);
        levelToTaskMap.put(task.getLevel(), task);
        return task;
    }

    private Task createStarterTask() {
        Task previousTask = new Task();
        previousTask.setId(Long.valueOf(0));
        previousTask.setParentTaskId(Long.valueOf(0));
        previousTask.setPriorSiblingTaskId(Long.valueOf(0));
        previousTask.setLevel(Integer.valueOf(0));
        return previousTask;
    }

    private TasksList createNewTasksList(Long accoundId, String newTasksListName) {
        TasksList newTasksList = new TasksList();
        newTasksList.setAccountId(accoundId);
        newTasksList.setName(newTasksListName);
        newTasksList.setAutoSync(Boolean.valueOf(false));
        newTasksList.setNote("Imported from Bonsai Outliner.");
        return newTasksList;
    }
}
