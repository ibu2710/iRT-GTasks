package com.irt.tasks.app.common;

public final class TimeUtil {
    public static long secondsToMilliseconds(long seconds) {
        return 1000 * seconds;
    }

    public static long millisecondsToSeconds(long millis) {
        return millis / 1000;
    }

    public static long minutesToMilliseconds(long minutes) {
        return secondsToMilliseconds(60 * minutes);
    }

    public static long minutesToSeconds(long minutes) {
        return 60 * minutes;
    }

    public static long millisecondsToMinutes(long millis) {
        return millisecondsToSeconds(millis) / 60;
    }

    public static long hoursToMilliseconds(long hours) {
        return minutesToMilliseconds(60 * hours);
    }

    public static long hoursToMinutes(long hours) {
        return 60 * hours;
    }

    public static long millisecondsToHours(long millis) {
        return millisecondsToMinutes(millis) / 60;
    }

    public static long daysToMilliseconds(long hours) {
        return hoursToMilliseconds(24 * hours);
    }

    public static long millisecondsToDays(long millis) {
        return millisecondsToHours(millis) / 24;
    }

    public static long weeksToMilliseconds(long weeks) {
        return daysToMilliseconds(7 * weeks);
    }

    public static long millisecondsToWeeks(long millis) {
        return millisecondsToDays(millis) / 7;
    }

    private TimeUtil() {
    }
}
