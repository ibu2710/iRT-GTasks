package com.irt.tasks.app.framework.business;

public class ErrorTypes {

    public enum ERROR {
        NO_ERROR,
        ERROR_PARENT_PASTED_ON_CHILD,
        ERROR_NO_SOURCE_SELECTED
    }

    public static String getErrorMessage(ERROR errorType) {
        switch (errorType) {
            case ERROR_PARENT_PASTED_ON_CHILD:
                return "Pasting on task's own child is not valid.";
            case ERROR_NO_SOURCE_SELECTED:
                return "You must select a task/branch to copy.";
            default:
                return "Unknown error";
        }
    }
}
