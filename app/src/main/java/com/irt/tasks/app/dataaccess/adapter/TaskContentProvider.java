package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.framework.config.ConfigManager;

public class TaskContentProvider extends ContentProvider {
    public static final String AUTHORITY = "com.irt.tasks.app.dataaccess.adapter.taskcontentprovider";
    private static final int LEGACY_TASKS = 3;
    private static final int TASK = 1;
    private static final int TASKS = 2;
    private static final UriMatcher sUriMatcher = new UriMatcher(-1);
    private ConfigManager config;
    private DatabaseHelper dbHelper;
    private TaskBp taskBp = new TaskBpImpl();

    static {
        sUriMatcher.addURI(AUTHORITY, "task/#", 1);
        sUriMatcher.addURI(AUTHORITY, "tasks", 2);
        sUriMatcher.addURI(AUTHORITY, null, 3);
    }

    public boolean onCreate() {
        this.dbHelper = new DatabaseHelper(getContext());
        return this.dbHelper != null;
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        this.config = ConfigManager.getInstance();
        SQLiteQueryBuilder querryBuilder = new SQLiteQueryBuilder();
        SQLiteDatabase database = this.dbHelper.getReadableDatabase();
        querryBuilder.setTables("task");
        switch (sUriMatcher.match(uri)) {
            case 1:
                Cursor mCursor = database.query(true, "task", projection, "_id=" + this.taskBp.get(Long.valueOf(getId(uri))).getId(), null, null, null, null, null);
                if (mCursor == null) {
                    return mCursor;
                }
                mCursor.moveToFirst();
                return mCursor;
            case 2:
            case 3:
                Cursor cursor = querryBuilder.query(database, projection, selection + " AND " + "inTrashBin" + " = 0 AND " + "deleted" + " = 0 " + (" AND tasks_list_id IN (" + TaskDBAdapterHelper.getVisibleTasksList(this.config.isShowInvisibleAccounts().booleanValue()) + ") "), selectionArgs, null, null, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), uri);
                return cursor;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        ConfigManager config = ConfigManager.getInstance();
        SQLiteDatabase database = this.dbHelper.getWritableDatabase();
        switch (sUriMatcher.match(uri)) {
            case 1:
                Task task = this.taskBp.get(Long.valueOf(getId(uri)));
                task.setSnoozeDateTime(Long.valueOf(0));
                AppUtil.clearNotification(config.getContext(), task);
                this.taskBp.setTaskAsDoneAndAdjustAlarm(task, null);
                values.put(TaskDBAdapterImpl.SNOOZE_DATE_TIME, Long.valueOf(0));
                long todayMillis = AppUtil.getTodayDateTimeMilisecond();
                values.put(TaskDBAdapterImpl.LAST_CONTENT_MODIFIED_DATE, Long.valueOf(todayMillis));
                values.put("last_modified", Long.valueOf(todayMillis));
                if (task.isRecurringTask()) {
                    values.put("completed", task.isCompleted());
                    values.put(TaskDBAdapterImpl.DUE_DATE, task.getDueDate());
                    values.put(TaskDBAdapterImpl.IS_FLOAT, task.isFloat());
                    values.put(TaskDBAdapterImpl.REMINDER_DATE_TIME, task.getReminderDateTime());
                    values.put(TaskDBAdapterImpl.SNOOZE_DATE_TIME, Long.valueOf(0));
                }
                int count = database.update("task", values, "_id = " + getId(uri), selectionArgs);
                AppUtil.setLastApplicationModifiedDate();
                getContext().getContentResolver().notifyChange(uri, null);
                return count;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    private long getId(Uri uri) {
        String lastPathSegment = uri.getLastPathSegment();
        if (!StringUtil.isNullOrEmpty(lastPathSegment)) {
            try {
                return Long.parseLong(lastPathSegment);
            } catch (Exception e) {
            }
        }
        return -1;
    }
}
