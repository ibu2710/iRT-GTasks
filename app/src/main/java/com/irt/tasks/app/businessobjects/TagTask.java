package com.irt.tasks.app.businessobjects;

import com.irt.tasks.app.framework.business.IrtTasksBo;

public class TagTask implements IrtTasksBo {
    public static final String TAG_ID = "tagId";
    public static final String TASK_ID = "taskId";
    private Long tagId;
    private Long taskId;

    public TagTask(Long tagId, Long taskId) {
        this.tagId = tagId;
        this.taskId = taskId;
    }

    public Long getTagId() {
        return this.tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public Long getTaskId() {
        return this.taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
}
