package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.TasksList;
import java.util.List;

public interface GtasksListDao {
    void deleteTasksList(TasksList tasksList);

    List<TasksList> getAllTasksLists();

    TasksList getTasksList(String str);

    void insertTasksList(TasksList tasksList);

    void renameTasksList(TasksList tasksList);
}
