package com.irt.tasks.app.common;

import java.util.Collection;
import java.util.Map;

public final class Assert {
    public static void isNull(Object arg, String argName) {
        if (arg != null) {
            throw new AssertFailedException("The value of \"" + argName + "\" must be null");
        }
    }

    public static void notNull(Object arg, String argName) {
        if (arg == null) {
            throw new AssertFailedException("The value of \"" + argName + "\" is null");
        }
    }

    public static void isTrue(boolean condition, String message) {
        if (!condition) {
            throw new AssertFailedException(message);
        }
    }

    public static void isFalse(boolean condition, String message) {
        if (condition) {
            throw new AssertFailedException(message);
        }
    }

    public static void notNullOrEmpty(Collection collection, String argName) {
        notNull(collection, argName);
        if (collection.isEmpty()) {
            throw new AssertFailedException("The \"" + argName + "\" Collection is empty");
        }
    }

    public static void notNullOrEmpty(Map map, String argName) {
        notNull(map, argName);
        if (map.isEmpty()) {
            throw new AssertFailedException("The \"" + argName + "\" Map is empty");
        }
    }

    public static void notNullOrEmpty(Object[] array, String argName) {
        notNull(array, argName);
        if (array.length == 0) {
            throw new AssertFailedException("The \"" + argName + "\" array is empty");
        }
    }

    public static void notNullOrEmpty(String string, String argName) {
        notNull(string, argName);
        if (string.trim().length() == 0) {
            throw new AssertFailedException("The \"" + argName + "\" String is empty");
        }
    }

    public static void validDouble(Object object, String argName, SignEnum allow) {
        notNull(object, argName);
        String value = object.toString();
        try {
            checkAllowFlags(new Double(value).compareTo(Globals.DOUBLE_ZERO), allow, argName, value);
        } catch (NumberFormatException e) {
            throw new AssertFailedException("Unable to format as a Double type. fieldName: " + argName + " value: \"" + value + Globals.QUOTE);
        }
    }

    public static void validLong(Object object, String argName, SignEnum allow) {
        notNull(object, argName);
        String value = object.toString();
        try {
            checkAllowFlags(new Long(value).compareTo(Globals.LONG_ZERO), allow, argName, value);
        } catch (NumberFormatException e) {
            throw new AssertFailedException("Unable to format as a Long type. fieldName: " + argName + " value: \"" + value + Globals.QUOTE);
        }
    }

    public static void validInteger(Object object, String argName, SignEnum allow) {
        notNull(object, argName);
        String value = object.toString();
        try {
            checkAllowFlags(new Integer(value).compareTo(Globals.INTEGER_ZERO), allow, argName, value);
        } catch (NumberFormatException e) {
            throw new AssertFailedException("Unable to format as an Integer type. fieldName: " + argName + " value: \"" + value + Globals.QUOTE);
        }
    }

    private static void checkAllowFlags(int compare, SignEnum allow, String argName, String value) {
        if (compare < 0 && !allow.hasSetting(SignEnum.ALLOW_NEG)) {
            throw new AssertFailedException("Negative values not allowed. fieldName: " + argName + " value: " + value);
        } else if (compare == 0 && !allow.hasSetting(SignEnum.ALLOW_ZER)) {
            throw new AssertFailedException("Zero values not allowed. fieldName: " + argName + " value: " + value);
        } else if (compare > 0 && !allow.hasSetting(SignEnum.ALLOW_POS)) {
            throw new AssertFailedException("Positive values not allowed. fieldName: " + argName + " value: " + value);
        }
    }

    private Assert() {
    }
}
