package com.irt.tasks.app.businessobjects;

import com.irt.tasks.app.framework.business.IrtTasksBo;

public class Reminder implements IrtTasksBo {
    public static final String DATE_TIME = "dateTime";
    public static final String DAYS_EARLIER = "daysEarlier";
    public static final String ENABLED = "enabled";
    public static final String REPEAT_FROM_COMPLETION_DATE = "repeatFromCompletionDate";
    public static final String REPEAT_RULE = "repeatRule";
    public static final String TASK_ID = "taskId";
    private Long dateTime;
    private Integer daysEarlier = Integer.valueOf(0);
    private Boolean enabled = Boolean.valueOf(true);
    private Boolean repeatFromCompletionDate = Boolean.valueOf(false);
    private String repeatRule;
    private Long taskId;

    public Long getTaskId() {
        return this.taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getDaysEarlier() {
        return this.daysEarlier;
    }

    public void setDaysEarlier(Integer daysEarlier) {
        this.daysEarlier = daysEarlier;
    }

    public Long getDateTime() {
        return this.dateTime;
    }

    public void setDateTime(Long dateTime) {
        this.dateTime = dateTime;
    }

    public Boolean isRepeatFromCompletionDate() {
        return this.repeatFromCompletionDate;
    }

    public void setRepeatFromCompletionDate(Boolean repeatFromCompletionDate) {
        this.repeatFromCompletionDate = repeatFromCompletionDate;
    }

    public String getRepeatRule() {
        return this.repeatRule;
    }

    public void setRepeatRule(String repeatRule) {
        this.repeatRule = repeatRule;
    }
}
