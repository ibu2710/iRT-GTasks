package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.irt.tasks.app.businessobjects.TaskState;
import com.irt.tasks.app.framework.business.IrtTasksBo;

public class TaskStateDBAdapterImpl implements TaskStateDBAdapter {
    public static final String DATABASE_CREATE = "create table task_state (tasks_list_id integer not null, task_str_id text not null, type integer null); ";
    public static final String DATABASE_TABLE = "task_state";
    public static final String INDEX_ON_TASK_LIST_ID = "CREATE INDEX idx_task_state_tasks_list_id ON task_state (tasks_list_id) ";
    public static final String TASKS_LIST_ID = "tasks_list_id";
    public static final String TASK_STR_ID = "task_str_id";
    public static final String TYPE = "type";
    private static final String[] ALL_COLUMNS = new String[]{"tasks_list_id", TASK_STR_ID, "type"};
    private DatabaseHelper DBHelper;
    private final Context context;
    private SQLiteDatabase db;

    public TaskStateDBAdapterImpl(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(this.context);
    }

    public TaskStateDBAdapter open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.DBHelper.close();
    }

    public boolean delete(long rowId) {
        throw new RuntimeException("not implemented");
    }

    public Cursor get(long rowId) throws SQLException {
        throw new RuntimeException("not implemented");
    }

    public Cursor getAll() {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, null, null, null, null, null);
    }

    public long insert(IrtTasksBo businessObject) {
        return this.db.insert(DATABASE_TABLE, null, populateBusinessObjectContentValues((TaskState) businessObject));
    }

    public boolean update(IrtTasksBo businessObject) {
        TaskState taskState = (TaskState) businessObject;
        return this.db.update(DATABASE_TABLE, populateBusinessObjectContentValues(taskState), new StringBuilder().append("tasks_list_id=").append(taskState.getTasksListId()).toString(), null) > 0;
    }

    private ContentValues populateBusinessObjectContentValues(TaskState taskState) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("tasks_list_id", taskState.getTasksListId());
        contentValues.put(TASK_STR_ID, taskState.getTaskStrId());
        contentValues.put("type", taskState.getType());
        return contentValues;
    }

    public Cursor getAllByTasksListId(Long tasksListId) {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, "tasks_list_id = " + tasksListId, null, null, null, null);
    }

    public int deleteAllByTasksListId(Long tasksListId) {
        return this.db.delete(DATABASE_TABLE, "tasks_list_id=" + tasksListId, null);
    }
}
