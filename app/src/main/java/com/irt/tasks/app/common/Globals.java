package com.irt.tasks.app.common;

import java.math.BigDecimal;
import java.math.BigInteger;

public final class Globals {
    public static final BigDecimal BIGDECIMAL_ZERO = new BigDecimal("0");
    public static final BigInteger BIGINTEGER_ZERO = new BigInteger("0");
    public static final String COMMA = ",";
    public static final Double DOUBLE_ZERO = new Double(0.0d);
    public static final String EMPTY_STRING = "";
    public static final String FILE_SEPARATOR = System.getProperty("file.separator");
    public static final String FORWARDSLASH = "/";
    public static final Integer INTEGER_ZERO = new Integer(0);
    public static final Long LONG_ZERO = new Long(0);
    public static final String NEW_LINE = System.getProperty("line.separator");
    public static final String PATH_SEPARATOR = System.getProperty("path.separator");
    public static final String PERIOD = ".";
    public static final String QUOTE = "\"";
    public static final String SPACE = " ";
    public static final String TAB = "\t";
    public static final String UNDERSCORE = "_";

    private Globals() {
    }
}
