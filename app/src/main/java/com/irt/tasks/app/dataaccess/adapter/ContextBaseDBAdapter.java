package com.irt.tasks.app.dataaccess.adapter;

public interface ContextBaseDBAdapter extends DBAdapter {
    boolean isNameUsed(String str, Long l);
}
