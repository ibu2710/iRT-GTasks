package com.irt.tasks.app.common;

public final class NumberUtil {
    public static final int parseInteger(String string) {
        Assert.notNull(string, "string");
        return Integer.parseInt(string);
    }

    public static final int parseInteger(String string, int defaultValue) {
        return parseIntegerDefaultIfNull(string, defaultValue);
    }

    public static final int parseIntegerDefaultIfNull(String string, int defaultValue) {
        return string == null ? defaultValue : parseInteger(string);
    }

    public static final int parseIntegerDefaultIfInvalid(String string, int defaultValue) {
        if (string != null) {
            try {
                defaultValue = parseInteger(string);
            } catch (NumberFormatException e) {
            }
        }
        return defaultValue;
    }

    public static final boolean isValidInteger(String string) {
        try {
            Integer.valueOf(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private NumberUtil() {
    }
}
