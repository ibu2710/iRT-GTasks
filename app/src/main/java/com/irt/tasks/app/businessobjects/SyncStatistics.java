package com.irt.tasks.app.businessobjects;

import java.util.List;

public class SyncStatistics {
    private long localTasksTotal = 0;
    private long totalAccounts = 0;
    private long totalGTasksLists = 0;
    private long totalLocalDeletes = 0;
    private long totalLocalInserts = 0;
    private long totalLocalUpdates = 0;
    private long totalTasksLists = 0;
    private long totalWebDeletes = 0;
    private long totalWebInserts = 0;
    private long totalWebUpdates = 0;
    private long webTasksTotal = 0;

    public void incAccountsTotal() {
        this.totalAccounts++;
    }

    public void incGTasksListsTotal() {
        this.totalGTasksLists++;
    }

    public void decGTasksListsTotal() {
        this.totalGTasksLists--;
    }

    public void incTasksListsTotal() {
        this.totalTasksLists++;
    }

    public void decTasksListsTotal() {
        this.totalTasksLists--;
    }

    public void addToWebTasksTotal(List<Task> tasks) {
        this.webTasksTotal += (long) tasks.size();
    }

    public void addToLocalTasksTotal(List<Task> tasks) {
        this.localTasksTotal += (long) tasks.size();
    }

    public void addToWebDeletesTotal(int numOfDeletes) {
        this.totalWebDeletes += (long) numOfDeletes;
    }

    public void addToLocalDeletesTotal(int numOfDeletes) {
        this.totalLocalDeletes += (long) numOfDeletes;
    }

    public void incWebUpdatesTotal() {
        this.totalWebUpdates++;
    }

    public void incWebInsertsTotal() {
        this.totalWebInserts++;
    }

    public void incLocalUpdatesTotal() {
        this.totalLocalUpdates++;
    }

    public void incLocalInsertsTotal() {
        this.totalLocalInserts++;
    }

    public void decreaseWebUpdatesWithDoublecountInserts(int doubleCountInserts) {
        this.totalWebUpdates -= (long) doubleCountInserts;
    }

    public long getWebTasksTotal() {
        return this.webTasksTotal;
    }

    public long getLocalTasksTotal() {
        return this.localTasksTotal;
    }

    public long getTotalWebUpdates() {
        return this.totalWebUpdates;
    }

    public long getTotalWebInserts() {
        return this.totalWebInserts;
    }

    public long getTotalLocalUpdates() {
        return this.totalLocalUpdates;
    }

    public long getTotalLocalInserts() {
        return this.totalLocalInserts;
    }

    public long getTotalWebDeletes() {
        return this.totalWebDeletes;
    }

    public long getTotalLocalDeletes() {
        return this.totalLocalDeletes;
    }

    public String reportStatistics() {
        return "\nSynced " + this.totalAccounts + " account/s; " + this.totalGTasksLists + " Glists; " + this.totalTasksLists + " local lists";
    }

    public String reportDetailedStatistics() {
        return "\nSynced " + this.webTasksTotal + " GTasks with " + this.localTasksTotal + " local tasks" + "\nGTasks have " + this.totalWebUpdates + " updates; " + this.totalWebInserts + " inserts; " + this.totalWebDeletes + " deletes" + "\nLocTasks have " + this.totalLocalUpdates + " updates; " + this.totalLocalInserts + " inserts; " + this.totalLocalDeletes + " deletes";
    }
}
