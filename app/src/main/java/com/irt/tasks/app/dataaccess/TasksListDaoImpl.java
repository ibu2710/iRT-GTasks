package com.irt.tasks.app.dataaccess;

import android.database.Cursor;
import android.database.SQLException;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.adapter.TasksListDBAdapter;
import com.irt.tasks.app.dataaccess.adapter.TasksListDBAdapterImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.ArrayList;
import java.util.List;

public class TasksListDaoImpl implements TasksListDao {
    ConfigManager config;

    public void delete(TasksList tasksList) {
        TasksListDBAdapter dBAdapter = openDbAdapter();
        dBAdapter.delete(tasksList.getId().longValue());
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    public int deleteTasksListInTrashBin() {
        TasksListDBAdapter dBAdapter = openDbAdapter();
        int deletedRecords = dBAdapter.deleteTasksListInTrashBin();
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
        return deletedRecords;
    }

    public void forceDeleteByAccountId(Long accountId) {
        TasksListDBAdapter dBAdapter = openDbAdapter();
        dBAdapter.forceDeleteByAccountId(accountId);
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    public TasksList get(Long tasksListId) throws SQLException {
        TasksListDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.get(tasksListId.longValue());
        TasksList tasksList = null;
        if (cursor.getCount() > 0) {
            tasksList = buildTasksList(cursor);
        }
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasksList;
    }

    public List<TasksList> getAllByAccountId(Long accountId) {
        TasksListDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllByAccountId(accountId);
        List<TasksList> tasksLists = buildTasksLists(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasksLists;
    }

    public List<TasksList> getAll() {
        TasksListDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAll();
        List<TasksList> tasksLists = buildTasksLists(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasksLists;
    }

    public List<TasksList> getAllIncludingDeletedAndTrashBin() {
        TasksListDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllIncludingDeletedAndTrashBin();
        List<TasksList> tasksLists = buildTasksLists(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasksLists;
    }

    public void saveOrUpdate(TasksList tasksList) {
        TasksListDBAdapter dBAdapter = openDbAdapter();
        tasksList.setLastModified(Long.valueOf(AppUtil.getTodayDateTimeMilisecond()));
        if (tasksList.getId() == null) {
            tasksList.setGtasksListName(tasksList.getName());
            tasksList.setId(Long.valueOf(dBAdapter.insert(tasksList)));
        } else {
            if (StringUtil.isNullOrEmpty(tasksList.getGtasksListId())) {
                tasksList.setGtasksListName(tasksList.getName());
            }
            dBAdapter.update(tasksList);
        }
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    public void saveOrUpdateWithTimeStamp(TasksList tasksList) {
        tasksList.setLastModified(Long.valueOf(AppUtil.getTodayDateTimeMilisecond()));
        saveOrUpdate(tasksList);
        AppUtil.setLastApplicationModifiedDate();
    }

    public List<TasksList> getAllByAccountIdExcludeMarkedAsDelete(Long accountId) {
        TasksListDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllByAccountIdExcludeMarkedAsDelete(accountId);
        List<TasksList> tasksLists = buildTasksLists(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasksLists;
    }

    public List<TasksList> getAllSortedExcludeMarkedAsDelete(String orderBy) {
        this.config = ConfigManager.getInstance();
        TasksListDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllSortedExcludeMarkedAsDelete(orderBy, this.config.isShowInvisibleAccounts().booleanValue(), 2 == this.config.getTasksListSort().intValue());
        List<TasksList> tasksLists = buildTasksLists(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasksLists;
    }

    private List<TasksList> buildTasksLists(Cursor cursor) {
        List<TasksList> tasksLists = new ArrayList();
        while (cursor.moveToNext()) {
            tasksLists.add(buildTasksList(cursor));
        }
        return tasksLists;
    }

    private TasksList buildTasksList(Cursor cursor) {
        Long id = Long.valueOf(cursor.getLong(0));
        String name = cursor.getString(1);
        String gtasksListName = cursor.getString(2);
        String gtasksListId = cursor.getString(3);
        Boolean deleted = AppUtil.isNumberOne(cursor.getInt(4));
        Long lastModified = Long.valueOf(cursor.getLong(5));
        Integer sortPosition = Integer.valueOf(cursor.getInt(6));
        Long accountId = Long.valueOf(cursor.getLong(7));
        Integer fontStyle = Integer.valueOf(cursor.getInt(8));
        Integer fontColor = Integer.valueOf(cursor.getInt(9));
        Integer fontBgColor = Integer.valueOf(cursor.getInt(10));
        Boolean inTrashBin = AppUtil.isNumberOne(cursor.getInt(11));
        Boolean autoSync = AppUtil.isNumberOne(cursor.getInt(12));
        Boolean visible = AppUtil.isNumberOne(cursor.getInt(13));
        Long taskLastModified = Long.valueOf(cursor.getLong(14));
        String note = cursor.getString(15);
        Boolean highlight = AppUtil.isNumberOne(cursor.getInt(16));
        Boolean colorTasks = AppUtil.isNumberOne(cursor.getInt(17));
        Boolean activeTasksList = AppUtil.isNumberOne(cursor.getInt(18));
        Boolean showNote = AppUtil.isNumberOne(cursor.getInt(19));
        Boolean showDueDate = AppUtil.isNumberOne(cursor.getInt(20));
        Integer newTaskPosition = Integer.valueOf(cursor.getInt(21));
        Boolean excludeFromDelete = AppUtil.isNumberOne(cursor.getInt(22));
        Boolean secure = AppUtil.isNumberOne(cursor.getInt(23));
        TasksList tasksList = new TasksList();
        tasksList.setId(id);
        tasksList.setName(name);
        tasksList.setGtasksListName(gtasksListName);
        tasksList.setGtasksListId(gtasksListId);
        tasksList.setDeleted(deleted);
        tasksList.setLastModified(lastModified);
        tasksList.setSortPosition(sortPosition);
        tasksList.setFontStyle(fontStyle);
        tasksList.setFontColor(fontColor);
        tasksList.setFontBgColor(fontBgColor);
        tasksList.setAccountId(accountId);
        tasksList.setInTrashBin(inTrashBin);
        tasksList.setAutoSync(autoSync);
        tasksList.setVisible(visible);
        tasksList.setTaskLastModified(taskLastModified);
        tasksList.setNote(note);
        tasksList.setHighlight(highlight);
        tasksList.setColorTasks(colorTasks);
        tasksList.setActiveTasksList(activeTasksList);
        tasksList.setShowNote(showNote);
        tasksList.setShowDueDate(showDueDate);
        tasksList.setNewTaskPosition(newTaskPosition);
        tasksList.setExcludeFromDelete(excludeFromDelete);
        tasksList.setSecure(secure);
        return tasksList;
    }

    private TasksListDBAdapter openDbAdapter() {
        this.config = ConfigManager.getInstance();
        TasksListDBAdapter dBAdapter = new TasksListDBAdapterImpl(this.config.getContext());
        dBAdapter.open();
        return dBAdapter;
    }

    private void closeDbAdapter(TasksListDBAdapter dBAdapter) {
        dBAdapter.close();
    }
}
