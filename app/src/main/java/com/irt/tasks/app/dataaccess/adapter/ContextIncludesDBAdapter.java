package com.irt.tasks.app.dataaccess.adapter;

import android.database.Cursor;

public interface ContextIncludesDBAdapter extends DBAdapter {
    int deleteAllbyContextBaseId(Long l);

    Cursor getAllByContextBaseId(Long l);

    Cursor getAllDistinctContextBasesIds();
}
