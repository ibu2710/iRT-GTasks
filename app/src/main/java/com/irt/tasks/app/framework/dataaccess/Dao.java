package com.irt.tasks.app.framework.dataaccess;

import android.content.ContentResolver;
import com.irt.tasks.app.framework.config.ConfigManager;

public interface Dao {
    public static final String TAG = "IrtCalendar";
    public static final ContentResolver contentResolver = ConfigManager.getInstance().getContext().getContentResolver();
}
