package com.irt.tasks.app.common;

import android.graphics.Color;
import android.net.Uri;

public class Constants {
    public static final String ACTION_ALARM = "com.irt.tasks.action.IRT_GTASKS_OUTLINER_ALARM";
    public static final String ACTION_AUTO_SYNC = "com.irt.tasks.action.IRT_GTASKS_OUTLINER_AUTO_SYNC";
    public static final String ACTION_DROPBOX_AUTO_SYNC = "com.irt.tasks.action.IRT_GTASKS_OUTLINER_DROPBOX_AUTO_SYNC";
    public static final String ACTION_UPDATE_LABEL = "com.irt.calendar.app.action.IRT_CALENDAR_UPDATE_WIDGET_LABEL";
    public static final int AUTO_SELECTION = 0;
    public static final String BONSAI_SD_CARD_DIRECTORY = "/irt_gtasks_outliner/bonsai/";
    public static final String CALENDAR_PACKAGE_ANDROID_FROYO_UP = "com.google.android.calendar";
    public static final String CALENDAR_PACKAGE_ANDROID_MR1_DOWN = "com.android.calendar";
    public static final String CALENDAR_PACKAGE_FROYO_HTC = "com.google.htc.calendar";
    public static final String CALENDAR_PACKAGE_HTC = "com.htc.calendar";
    public static final String COLOR_BLUISH_DAY_HEADER = "#688CD9";
    public static final String COLOR_BLUISH_GRID_LINE = "#2952A3";
    public static final String COLOR_BLUISH_MAIN_LETTER = "#2952A3";
    public static final String COLOR_BLUISH_SATURDAY = "#E6ECF9";
    public static final String COLOR_BLUISH_WEEK_HEADER = "#A7BCE9";
    public static final String COLOR_BLUISH_WEEK_SELECTOR = "#BBCCEE";
    public static final String COLOR_REDISH_SUNDAY = "#FFEAEA";
    public static final int COLOR_SELECTED_TASK_YELLOW = Color.parseColor("#FFFF75");
    public static final int COLOR_TODAY_BLUE = Color.parseColor("#E8F4FF");
    public static final int COLOR_TODAY_GRAY = Color.parseColor("#F3F3F3");
    public static final int COLOR_TODAY_GREEN = Color.parseColor("#E6FFE6");
    public static final int COLOR_TODAY_PURPLE = Color.parseColor("#FFE8FF");
    public static final int COLOR_TODAY_YELLOW = Color.parseColor("#FFFFCE");
    public static final int COLOR_YELLOWISH_MONTH_WEEK_SELECTED_DAY_HIGHLIGHT = Color.parseColor("#FFFF5E");
    public static final Uri CONTENT_URI_ECLAIR_MR1_DOWN = Uri.parse("content://calendar/calendars");
    public static final Uri CONTENT_URI_FROYO_UP = Uri.parse("content://com.android.calendar/calendars");
    public static final String DATE_FORMAT_DDDOTMMDOTYY = "dd.MM.yy";
    public static final String DATE_FORMAT_DDDOTMMDOTYYYY = "dd.MM.yyyy";
    public static final String DATE_FORMAT_DDMMYY = "dd/MM/yy";
    public static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";
    public static final String DATE_FORMAT_HHMMSSA = "hh:mm:ss a";
    public static final String DATE_FORMAT_HMMSSA = "h:mm:ss a";
    public static final String DATE_FORMAT_KKMMSSA = "kk:mm:ss";
    public static final String DATE_FORMAT_KMMSSA = "k:mm:ss";
    public static final String DATE_FORMAT_MMDDYY = "MM/dd/yy";
    public static final String DATE_FORMAT_MMDDYYYY = "MM/dd/yyyy";
    public static final String DATE_FORMAT_MMDDYYYYE = "MM/dd/yyyy E";
    public static final String DATE_FORMAT_MMDDYYYYHHMMSSA = "MM/dd/yyyy hh:mm:ss a";
    public static final String DATE_FORMAT_MMDDYYYYKKMMSS = "MM/dd/yyyy kk:mm:ss";
    public static final String DATE_FORMAT_MMMDYYYY = "MMM d, yyyy";
    public static final String DATE_FORMAT_MMMDYYYYHMMA = "MMM d, yyyy, h:mm a";
    public static final String DATE_FORMAT_MMMDYYYYKMM = "MMM d, yyyy, k:mm";
    public static final String DATE_FORMAT_MM_DD_YY_HH_MM = "MMddyyhhmm";
    public static final String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String DATE_FORMAT_YYYY_MM_DD_KK_MM_SS = "yyyy-MM-dd-kkmmss";
    public static final String DATE_FORMAT_YY_MM_DD = "yy-MM-dd";
    public static final int DATE_PICKER_TASK_EDIT = 1;
    public static final String DONE_COLOR = "#AAA6AC";
    public static final String DROPBOX_BONSAI_RELATIVE_DIRECTORY = "/bonsai/";
    public static final String DROPBOX_DATABASE_META_DATA_RELATIVE_FILE_PATH = "/irttasks_metadata";
    public static final String DROPBOX_DATABASE_RELATIVE_FILE_PATH = "/irttasks.db";
    public static final String DROPBOX_DATABASE_SYNC_META_DATA_RELATIVE_FILE_PATH = "/irttasks_sync_metadata";
    public static final String DROPBOX_DATABASE_SYNC_RELATIVE_FILE_PATH = "/irttasks_sync.db";
    public static final int ECLAIR = 5;
    public static final int ECLAIR_MR1 = 7;
    public static final int EDIT_COLOR_COPY = Color.parseColor("#66CCFF");
    public static final int EDIT_COLOR_CUT = Color.parseColor("#FF2F2F");
    public static final String END_OF_DAY_TIME = " 23:59:59";
    public static final int EVENT_TITLE_STYLE_BOLD = 1;
    public static final int EVENT_TITLE_STYLE_NORMAL = 0;
    public static final String FALSE = "false";
    public static final int GOOGLE_BLUE_COLOR = -14069085;
    public static final Long HOME_TASKS_LIST_ID = Long.valueOf(0);
    public static final String ICON_APPLE = "rt_food_apple";
    public static final String ICON_BLANK = "rt_blank";
    public static final String INTENT_CALENDAR_TASK_ID = "calendarTaskId";
    public static final String INTENT_CALENDAR_TASK_LIST_ID = "calendarTaskListId";
    public static final String INTENT_FROM_CALENDAR = "fromCalendar";
    public static final String INTENT_FROM_CALENDAR_FOR_LIST = "fromCalendarForList";
    public static final String INTENT_FROM_VIEW_TASK = "fromViewTask";
    public static final String INTENT_IS_HIDE_AND_OR = "isHideAndOr";
    public static final String INTENT_IS_SHOW_ANY = "isShowAny";
    public static final String INTENT_SELECTED_DATE = "calendarSelectedDate";
    public static final String INTENT_TASK_ID = "taskId";
    public static final String INTENT_TASK_LIST_ID = "taskListId";
    public static final String INTENT_TYPE_VND_ANDROID_CURSOR_ITEM = "vnd.android.cursor.item";
    public static final String INTENT_TYPE_VND_COM_GOOGLE_CURSOR_ITEM = "vnd.com.google.cursor.item";
    public static final String INTENT_TYPE_VND_HTC_CURSOR_ITEM = "vnd.htc.cursor.item";
    public static final int INVALID_POSITION = -1;
    public static final Long INVALID_TASK_ID = Long.valueOf(-1);
    public static final String IRT_WEB_SOLUTIONS_EMAIL = "support@iRTWebSolutions.com";
    public static final String IS_CHILD_TASK = "isChild";
    public static final String IS_FROM_AGENDA_VIEW = "isFromAgendaView";
    public static final String IS_FROM_FILTER_VIEW = "isFromFilterView";
    public static final String IS_FROM_VIEW = "isFromView";
    public static final String IS_NEW_TASK = "isNewTask";
    public static final String IS_PLUS_NEW_TASK = "isPlusNewTask";
    public static final String IS_SEARCH_FROM_HOME = "isSearchFromHome";
    public static final String IS_SEARCH_LIST_PREFERENCES_NAME = "isSearchList";
    public static final String IS_VIEW_FROM_HOME = "isViewFromHome";
    public static final int JELLY_BEAN_MR1 = 17;
    public static final int MAX_TASKS_LEVEL = 20;
    public static final int MIN_TASKS_LEVEL = 1;
    public static final String NEW_TASK_ID = "newTaskId";
    public static final String NO = "N";
    public static final String NOTIFICATION_SPEECH = "notificationSpeech";
    public static final String NO_CONTEXT = "@no_context";
    public static final String NO_CONTEXT_INDICATOR = "zzzzzzz";
    public static final long NO_FLOATING_EVENT_LAST_RUN_DATE_MILLIS = 0;
    public static final Long NO_SIBLING_ID = Long.valueOf(0);
    public static final int ONE = 1;
    public static final int PAGE_BACKGROUND_COLOR_GREY = 1;
    public static final int PAGE_BACKGROUND_COLOR_WHITE = 0;
    public static final String PREFERENCES_NAME = "MyPrefsFile";
    public static final int REQUEST_ID_ADD_TASK = 1;
    public static final int REQUEST_ID_EDIT_TASK = 2;
    public static final int REQUEST_ID_END_ON_DATE_GOTO = 3;
    public static final int SATURDAY_INDEX = 6;
    public static final String SD_CARD_MAIN_DIRECTORY = "/irt_gtasks_outliner";
    public static final int SEARCH_SCOPE_PREF_TITLE = 0;
    public static final int SEARCH_SCOPE_PREF_TITLE_LOCATION = 1;
    public static final int SEARCH_SCOPE_PREF_TITLE_LOCATION_DESCRIPTION = 2;
    public static final String SELECTED_DATE = "selectedDate";
    public static final String SELECTED_DATE_LONG_KEY = "selectedDateLong";
    public static final int SNOOZE_MININMUM_MINUTES = 1;
    public static final int SNOOZE_MININMUM_MINUTES_BEFORE_EMAIL_IS_ALLOWED = 780;
    public static final int SUNDAY_INDEX = 0;
    public static final String SYNC_TASKS_LIST_NAME_AT_START = "Syncing...";
    public static final String TAG = "iRTGTasks";
    public static final Long TASKS_LIST_AUTO_ID = Long.valueOf(-1);
    public static final Long TASK_COLLAPSE_INDICATOR = Long.valueOf(-1);
    public static final Long TASK_EXPAND_INDICATOR = Long.valueOf(1);
    public static final String TASK_LIST = "tasks-list";
    public static final String TIME_AND_TITLE_FORMAT_1 = "time_and_title_format_1";
    public static final String TIME_AND_TITLE_FORMAT_2 = "time_and_title_format_2";
    public static final String TIME_AND_TITLE_FORMAT_3 = "time_and_title_format_3";
    public static final String TIME_AND_TITLE_FORMAT_4 = "time_and_title_format_4";
    public static final String TIME_REMAINING_FORMAT_1 = "time_remaining_format_1";
    public static final String TIME_REMAINING_FORMAT_2 = "time_remaining_format_2";
    public static final String TIME_REMAINING_FORMAT_3 = "time_remaining_format_3";
    public static final String TRUE = "true";
    public static final String URI_PREFIX_ECLAIR_FROYO_UP = "content://com.android.calendar/";
    public static final String URI_PREFIX_ECLAIR_MR1_DOWN = "content://calendar/";
    public static final int WINDOW_TITLE_MAX_LENGTH = 35;
    public static final String YES = "Y";
    public static final String encoding = "utf-8";
    public static final String mimeType = "text/html";

    public enum ACTIONS {
        TOP_SWIPE_LEFT,
        TOP_SWIPE_RIGHT,
        MIDDLE_SWIPE_LEFT,
        MIDDLE_SWIPE_RIGHT,
        BOTTOM_SWIPE_LEFT,
        BOTTOM_SWIPE_RIGHT,
        TAP_LEFT,
        TAP_RIGHT,
        DOUBLE_TAP_LEFT,
        DOUBLE_TAP_RIGHT,
        LONG_PRESS_LEFT,
        LONG_PRESS_RIGHT
    }

    public enum FILTER_PARAMETER_TYPES {
        CONTEXT,
        TAG,
        PROJECT
    }

    public enum HORIZONTAL_DIRECTION {
        LEFT,
        RIGHT
    }

    public enum UNIQUE_NAME_ERROR {
        NO_ERROR,
        BLANK,
        DUPLICATE
    }

    public enum VERTICAL_VIEW_SECTIONS {
        TOP,
        MIDDLE,
        BOTTOM
    }
}
