package com.irt.tasks.app.dataaccess.adapter;

import com.irt.tasks.app.common.Globals;

public class DatabaseUtil {
    public static String getRawColumns(String tableName, String[] ALL_COLUMNS) {
        String rawColumns = "";
        for (String column : ALL_COLUMNS) {
            rawColumns = rawColumns + getRawColumn(tableName, column) + Globals.COMMA;
        }
        return rawColumns.substring(0, rawColumns.lastIndexOf(Globals.COMMA));
    }

    public static String getRawColumn(String tableName, String column) {
        return tableName + Globals.PERIOD + column;
    }
}
