package com.irt.tasks.app.common;

import java.util.StringTokenizer;

public final class StringUtil {
    private static final char APOSTROPHY = '\'';
    private static final String EXCLAMATION_TOKEN = "!";
    private static final String NATIVE_PATH_SEPARATOR = "\\";
    private static final char PERIOD_SEPARATOR = '.';
    private static final String PERIOD_TOKEN = ".";
    private static final String QUESTION_TOKEN = "?";
    private static final String URI_FILE_PREFIX = "file";
    private static final String URI_HTTP_PREFIX = "http";
    private static final String URI_PATH_SEPARATOR = "/";
    private static final String URI_SEPARATOR_PREFIX = ":///";

    public static final String capitalizeEveryWord(String string) {
        Assert.notNull(string, "string");
        char prevCh = PERIOD_SEPARATOR;
        StringBuffer capString = new StringBuffer();
        for (int i = 0; i < string.length(); i++) {
            char ch = string.charAt(i);
            if (!Character.isLetter(ch) || Character.isLetter(prevCh) || prevCh == APOSTROPHY) {
                capString.append(Character.toLowerCase(ch));
            } else {
                capString.append(Character.toUpperCase(ch));
            }
            prevCh = ch;
        }
        return capString.toString();
    }

    public static final String capitalizeFirstWord(String string) {
        Assert.notNull(string, "string");
        String str = "";
        str = changeCapitalization(new StringTokenizer(string.toLowerCase(), ".", true));
        StringTokenizer questToken = new StringTokenizer(str, QUESTION_TOKEN, true);
        if (questToken.countTokens() > 1) {
            str = changeCapitalization(questToken);
        }
        StringTokenizer exclToken = new StringTokenizer(str, EXCLAMATION_TOKEN, true);
        if (exclToken.countTokens() > 1) {
            return changeCapitalization(exclToken);
        }
        return str;
    }

    public static final long count(String input, char countChar) {
        Assert.notNull(input, "input");
        long count = 0;
        char[] inputChar = input.toCharArray();
        for (char c : inputChar) {
            if (c == countChar) {
                count++;
            }
        }
        return count;
    }

    public static final String escapeHtml(String input) {
        Assert.notNull(input, "input");
        StringBuffer out = new StringBuffer();
        char[] in = input.toCharArray();
        for (int i = 0; i < in.length; i++) {
            boolean requiresHtmlEscape = in[i] > '~' || (in[i] < ' ' && (in[i] < '\t' || in[i] > '\r'));
            if (requiresHtmlEscape) {
                out.append("&#" + in[i] + ";");
            } else {
                out.append(in[i]);
            }
        }
        return out.toString();
    }

    public static final String getExtension(String string) {
        Assert.notNull(string, "string");
        String returnString = "";
        int index = string.lastIndexOf(".");
        if (index != -1) {
            returnString = string.substring(index + 1);
        }
        return returnString.toLowerCase();
    }

    public static final String getStringDiff(String inputString, String compareString) {
        Assert.notNull(inputString, "inputString");
        if (compareString == null) {
            return inputString;
        }
        if (inputString.length() > compareString.length()) {
            return getDiff(compareString, inputString);
        }
        return getDiff(inputString, compareString);
    }

    public static final String defaultIfEmpty(String string, String strDefault) {
        return isNullOrEmpty(string) ? strDefault : string;
    }

    public static final String defaultIfNull(String string, String strDefault) {
        return string == null ? strDefault : string;
    }

    public static final String leftPad(String inputString, char padChar, int paddedStringLength) {
        return pad(inputString, paddedStringLength, padChar, false);
    }

    public static final String replace(String string, String replaceThis, String withThis) {
        Assert.notNull(string, "string");
        if (replaceThis == null) {
            return string;
        }
        if (withThis == null) {
            withThis = "";
        }
        if (replaceThis.equals(withThis)) {
            return string;
        }
        int len = string.length();
        int replaceLen = replaceThis.length();
        StringBuffer sb = new StringBuffer(len);
        String newString = string;
        while (true) {
            int pos = newString.indexOf(replaceThis);
            if (pos != -1) {
                sb.append(newString.substring(0, pos) + withThis);
                newString = newString.substring(pos + replaceLen);
            } else {
                sb.append(newString);
                return sb.toString();
            }
        }
    }

    public static final String replaceFirstOccurence(String string, String replaceThis, String withThis) {
        Assert.notNull(string, "string");
        if (replaceThis == null) {
            return string;
        }
        if (withThis == null) {
            withThis = "";
        }
        String newString = string;
        StringBuffer sb = new StringBuffer(string.length());
        int pos = newString.indexOf(replaceThis);
        if (pos != -1) {
            int replaceLen = replaceThis.length();
            sb.append(newString.substring(0, pos) + withThis);
            newString = newString.substring(pos + replaceLen);
        }
        sb.append(newString);
        return sb.toString();
    }

    public static final String reverse(String stringToReverse) {
        Assert.notNull(stringToReverse, "stringToReverse");
        return new StringBuffer(stringToReverse).reverse().toString();
    }

    public static final String rightPad(String inputString, char padChar, int paddedStringLength) {
        return pad(inputString, paddedStringLength, padChar, true);
    }

    public static final String remove(String s, String stripThis) {
        return replace(s, stripThis, null);
    }

    public static final String emptyIfNull(String s) {
        return s == null ? "" : s;
    }

    public static final String nullIfEmpty(String s) {
        return (s == null || s.length() == 0) ? null : s;
    }

    public static final String getValidUriString(String qualifiedUri, String baseUri) {
        Assert.notNull(baseUri, "baseUri");
        Assert.notNullOrEmpty(qualifiedUri, "qualifiedUri");
        String newQualifiedUri = validateUriPrefix(replace(qualifiedUri, NATIVE_PATH_SEPARATOR, "/"));
        if (newQualifiedUri.startsWith(URI_HTTP_PREFIX) || newQualifiedUri.startsWith(URI_FILE_PREFIX)) {
            return newQualifiedUri;
        }
        String newBaseUri = replace(baseUri, NATIVE_PATH_SEPARATOR, "/");
        StringBuffer uri = new StringBuffer();
        uri.append(URI_FILE_PREFIX);
        uri.append(URI_SEPARATOR_PREFIX);
        return uri.toString().concat(validateLocalPrefix(newQualifiedUri, newBaseUri));
    }

    public static final boolean isNullOrEmpty(String string) {
        return string == null || string.trim().length() == 0;
    }

    public static final String getBaseUri(String uri) {
        Assert.notNull(uri, "uri");
        int lastSlash = uri.lastIndexOf("/");
        if (lastSlash == -1) {
            return "";
        }
        return uri.substring(0, lastSlash + 1);
    }

    private static final String getDiff(String baseStr, String compareStr) {
        int cutPos = -1;
        for (int i = 0; i < baseStr.length(); i++) {
            if (baseStr.charAt(i) != compareStr.charAt(i)) {
                cutPos = i;
                break;
            }
        }
        if (cutPos > 0) {
            return compareStr.substring(cutPos, compareStr.length());
        }
        return compareStr.substring(baseStr.length(), compareStr.length());
    }

    private static final String changeCapitalization(StringTokenizer st) {
        char prevCh = PERIOD_SEPARATOR;
        boolean fl = false;
        StringBuffer capString = new StringBuffer();
        while (st.hasMoreElements()) {
            String el = (String) st.nextElement();
            for (int i = 0; i < el.length(); i++) {
                char ch = el.charAt(i);
                if (!Character.isLetter(ch) || Character.isLetter(prevCh) || fl) {
                    capString.append(ch);
                } else {
                    capString.append(Character.toUpperCase(ch));
                    fl = true;
                }
                prevCh = ch;
            }
            fl = false;
        }
        return capString.toString();
    }

    private static final String pad(String input, int numChars, char padChar, boolean padRight) {
        String inputString;
        if (input == null) {
            inputString = "";
        } else {
            inputString = input;
        }
        String result = inputString;
        if (inputString.length() >= numChars) {
            return result;
        }
        int numPad = numChars - inputString.length();
        char[] paddedChars = new char[numPad];
        for (int i = 0; i < numPad; i++) {
            paddedChars[i] = padChar;
        }
        String pad = new String(paddedChars);
        return padRight ? inputString + pad : pad + inputString;
    }

    private static final String validateHttpPrefix(String qualifiedUri) {
        if (qualifiedUri.substring(0, 4).equalsIgnoreCase(URI_HTTP_PREFIX)) {
            return URI_HTTP_PREFIX + qualifiedUri.substring(4, qualifiedUri.length());
        }
        return qualifiedUri;
    }

    private static final String validateLocalPrefix(String qualifiedUri, String localUriPrefix) {
        String newQualifiedUri = qualifiedUri;
        if (qualifiedUri.length() < localUriPrefix.length() || !qualifiedUri.substring(0, localUriPrefix.length()).equalsIgnoreCase(localUriPrefix)) {
            return localUriPrefix.concat(qualifiedUri);
        }
        return newQualifiedUri;
    }

    private static final String validateUriPrefix(String qualifiedUri) {
        if (qualifiedUri.substring(0, 4).equalsIgnoreCase(URI_FILE_PREFIX)) {
            return URI_FILE_PREFIX + qualifiedUri.substring(4, qualifiedUri.length());
        }
        return validateHttpPrefix(qualifiedUri);
    }

    private StringUtil() {
    }
}
