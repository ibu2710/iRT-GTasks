package com.irt.tasks.app.framework.trial;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.text.format.DateFormat;
import com.irt.tasks.R;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.Calendar;
import java.util.Date;

public class TrialSpecificUtility {
    static final String APPLICATION_CODE_PARAM = "application_code";
    static final int APPLICATION_ID_IRT_GTASKS_OUTLINER = 2;
    static final String DEVICE_ID_PARAM = "device_id";
    private static final int EXPIRATION_GRACE_PERIOD_IN_DAYS = 10;

    private static boolean isExpired() {
        return AppUtil.getTodayDateTime().getTime() >= getExpirationDate().getTime();
    }

    private static boolean isFinalExpiration() {
        return AppUtil.getTodayDateTime().getTime() >= getExpirationDate().getTime() + 864000000;
    }

    public static void checkTrialExpiration(Context context) {
        if (!ConfigManager.getInstance().isProVersion() && isExpired()) {
            new Builder(context).setTitle("Trial Expiration").setMessage("Beta version has expired. Thank you for trying iRT GTasks Outliner.\n\nIf you want to continue using this application, please update iRT GTasks Outliner from the Android Market.").setPositiveButton(R.string.alert_dialog_ok, new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (TrialSpecificUtility.isFinalExpiration()) {
                        System.exit(0);
                    }
                }
            }).show();
        }
    }

    public static boolean isTrial() {
        return true;
    }

    public static String getTrialEndDate() {
        return formatExpirationDate(getExpirationDate());
    }

    private static String formatExpirationDate(Date expirationDate) {
        return DateFormat.format(Constants.DATE_FORMAT_MMMDYYYY, expirationDate).toString();
    }

    private static Date getDefiniteExpirationDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(2218, 11, 31);
        return cal.getTime();
    }

    private static Date getExpirationDate() {
        return getDefiniteExpirationDate();
    }

    private static Date getFinalCheckedExpirationDate(ConfigManager config) {
        if (getDefiniteExpirationDate().getTime() < config.getExpirationDateMillis().longValue()) {
            config.setExpirationDateMillis(Long.valueOf(getDefiniteExpirationDate().getTime()));
        }
        return new Date(config.getExpirationDateMillis().longValue());
    }
}
