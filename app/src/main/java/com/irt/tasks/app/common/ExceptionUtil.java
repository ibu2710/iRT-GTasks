package com.irt.tasks.app.common;

import java.io.PrintWriter;
import java.io.StringWriter;

public final class ExceptionUtil {
    public static final String getStackTrace(Throwable exception) {
        Assert.notNull(exception, "exception");
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        pw.close();
        return sw.toString();
    }

    public static Throwable getRootCause(Throwable t) {
        Assert.notNull(t, "t");
        Throwable rootCause = t;
        while (rootCause.getCause() != null && rootCause.getCause() != rootCause) {
            rootCause = rootCause.getCause();
        }
        return rootCause;
    }

    public static String getRootCauseClassNameAndMessage(Throwable t) {
        Throwable rootCause = getRootCause(t);
        return className(rootCause) + ": " + rootCause.getMessage();
    }

    private static String className(Object callerClass) {
        String className = "";
        if (callerClass == null) {
            return className;
        }
        String fullClassName = callerClass.getClass().getName();
        if (fullClassName != null) {
            return fullClassName.substring(fullClassName.lastIndexOf(46) + 1);
        }
        return className;
    }

    private ExceptionUtil() {
    }
}
