package com.irt.tasks.app.dataaccess;

import android.database.Cursor;
import com.irt.tasks.app.businessobjects.Project;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.dataaccess.adapter.ProjectDBAdapter;
import com.irt.tasks.app.dataaccess.adapter.ProjectDBAdapterImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.filter.TasksFilterManager;
import java.util.ArrayList;
import java.util.List;

public class ProjectDaoImpl implements ProjectDao {
    private ConfigManager config;
    TaskDao taskDao = new TaskDaoImpl();

    public List<Project> getAll() {
        ProjectDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAll();
        List<Project> projects = buildProjects(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return projects;
    }

    public Project get(Long projectId) {
        ProjectDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.get(projectId.longValue());
        Project project = null;
        if (cursor != null) {
            project = buildProject(cursor);
        }
        cursor.close();
        closeDbAdapter(dBAdapter);
        return project;
    }

    public boolean isTitleUsed(String title, Long projectId) {
        ProjectDBAdapter dBAdapter = openDbAdapter();
        boolean isTitleUsed = dBAdapter.isTitleUsed(title, projectId);
        closeDbAdapter(dBAdapter);
        return isTitleUsed;
    }

    public void saveOrUpdate(Project project) {
        ProjectDBAdapter dBAdapter = openDbAdapter();
        if (project.getId() == null) {
            project.setId(Long.valueOf(dBAdapter.insert(project)));
        } else {
            dBAdapter.update(project);
        }
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    public void delete(Project project) {
        TasksFilterManager.getInstance().resetProjectId(project.getId());
        this.taskDao.resetTasksProject(project.getId());
        ProjectDBAdapter dBAdapter = openDbAdapter();
        dBAdapter.delete(project.getId().longValue());
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    private List<Project> buildProjects(Cursor cursor) {
        List<Project> projects = new ArrayList();
        while (cursor.moveToNext()) {
            projects.add(buildProject(cursor));
        }
        return projects;
    }

    private Project buildProject(Cursor cursor) {
        Long id = Long.valueOf(cursor.getLong(0));
        String title = cursor.getString(1);
        String description = cursor.getString(2);
        Project project = new Project();
        project.setId(id);
        project.setTitle(title);
        project.setDescription(description);
        return project;
    }

    private ProjectDBAdapter openDbAdapter() {
        this.config = ConfigManager.getInstance();
        ProjectDBAdapter dBAdapter = new ProjectDBAdapterImpl(this.config.getContext());
        dBAdapter.open();
        return dBAdapter;
    }

    private void closeDbAdapter(ProjectDBAdapter dBAdapter) {
        dBAdapter.close();
    }
}
