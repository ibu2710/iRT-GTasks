package com.irt.tasks.app.businessprocess;

import android.content.Context;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.AccountDao;
import com.irt.tasks.app.dataaccess.AccountDaoImpl;
import com.irt.tasks.app.dataaccess.TaskDao;
import com.irt.tasks.app.dataaccess.TaskDaoImpl;
import com.irt.tasks.app.dataaccess.TasksListDao;
import com.irt.tasks.app.dataaccess.TasksListDaoImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.ArrayList;
import java.util.List;

public class AccountBpImpl implements AccountBp {
    AccountDao accountDao = new AccountDaoImpl();
    TaskDao taskDao = new TaskDaoImpl();
    TasksListDao tasksListDao = new TasksListDaoImpl();

    public List<Account> getAllAccountsFromSystem(Context context) {
        return new ArrayList();
    }

    public List<Account> getAll() {
        return this.accountDao.getAll();
    }

    public List<Account> getAllVisibleAccounts() {
        return this.accountDao.getAllVisible();
    }

    public void saveOrUpdate(Account account) {
        this.accountDao.saveOrUpdate(account);
        AppUtil.scheduleAllAlarms(ConfigManager.getInstance().getContext());
    }

    public Boolean isValidGoogleAccount(Account account, boolean isNew) {
        if (StringUtil.isNullOrEmpty(account.getEmail()) || StringUtil.isNullOrEmpty(account.getPassword())) {
            return Boolean.valueOf(false);
        }
        if (isNew) {
            for (Account account2 : this.accountDao.getAll()) {
                if (account2.getEmail().trim().toLowerCase().equals(account.getEmail().trim().toLowerCase())) {
                    return Boolean.valueOf(false);
                }
            }
        }
        return Boolean.valueOf(ConfigManager.getInstance().isValidGoogleAccount(account));
    }

    public void deleteAccount(Account account) {
        forceDeleteTasksByTasksListIds(account);
        this.tasksListDao.forceDeleteByAccountId(account.getId());
        this.accountDao.delete(account);
        AppUtil.scheduleAllAlarms(ConfigManager.getInstance().getContext());
    }

    private void forceDeleteTasksByTasksListIds(Account account) {
        for (TasksList tasksList2 : this.tasksListDao.getAllByAccountId(account.getId())) {
            this.taskDao.forceDeleteByTaskListId(tasksList2.getId());
        }
    }

    public Account get(Long accountId) {
        return this.accountDao.get(accountId);
    }
}
