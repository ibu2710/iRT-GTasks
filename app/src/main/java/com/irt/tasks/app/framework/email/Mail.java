package com.irt.tasks.app.framework.email;

import com.irt.tasks.app.common.Constants;
import java.util.Date;
import java.util.Properties;
import javax.activation.CommandMap;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.activation.MailcapCommandMap;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

public class Mail extends Authenticator {
    private boolean _auth;
    private String _body;
    private boolean _debuggable;
    private String _from;
    private String _host;
    private Multipart _multipart;
    private String _pass;
    private String _port;
    private String _sport;
    private String _subject;
    private String[] _to;
    private String _user;

    public Mail() {
        this._host = "smtp.gmail.com";
        this._port = "465";
        this._sport = "465";
        this._user = "";
        this._pass = "";
        this._from = "";
        this._subject = "";
        this._body = "";
        this._debuggable = false;
        this._auth = true;
        this._multipart = new MimeMultipart();
        MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
        mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
        mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
        mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
        mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
        mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
        CommandMap.setDefaultCommandMap(mc);
    }

    public Mail(String user, String pass) {
        this();
        this._user = user;
        this._pass = pass;
    }

    public boolean send() throws Exception {
        Properties props = _setProperties();
        if (this._user.equals("") || this._pass.equals("") || this._to.length <= 0 || this._from.equals("") || this._subject.equals("") || this._body.equals("")) {
            return false;
        }
        MimeMessage msg = new MimeMessage(Session.getInstance(props, this));
        msg.setFrom(new InternetAddress(this._from));
        Address[] addressTo = new InternetAddress[this._to.length];
        for (int i = 0; i < this._to.length; i++) {
            addressTo[i] = new InternetAddress(this._to[i]);
        }
        msg.setRecipients(RecipientType.TO, addressTo);
        msg.setSubject(this._subject);
        msg.setSentDate(new Date());
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText(this._body);
        this._multipart.addBodyPart(messageBodyPart);
        msg.setContent(this._multipart);
        Transport.send(msg);
        return true;
    }

    public void addAttachment(String filename) throws Exception {
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setDataHandler(new DataHandler(new FileDataSource(filename)));
        messageBodyPart.setFileName(filename);
        this._multipart.addBodyPart(messageBodyPart);
    }

    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(this._user, this._pass);
    }

    private Properties _setProperties() {
        Properties props = new Properties();
        props.put("mail.smtp.host", this._host);
        if (this._debuggable) {
            props.put("mail.debug", Constants.TRUE);
        }
        if (this._auth) {
            props.put("mail.smtp.auth", Constants.TRUE);
        }
        props.put("mail.smtp.port", this._port);
        props.put("mail.smtp.socketFactory.port", this._sport);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", Constants.FALSE);
        return props;
    }

    public String getBody() {
        return this._body;
    }

    public void setBody(String _body) {
        this._body = _body;
    }

    public void setTo(String[] toAddress) {
        this._to = toAddress;
    }

    public void setFrom(String fromAddress) {
        this._from = fromAddress;
    }

    public void setSubject(String subject) {
        this._subject = subject;
    }
}
