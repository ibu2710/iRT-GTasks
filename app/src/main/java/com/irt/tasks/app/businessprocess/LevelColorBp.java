package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.LevelColor;
import com.irt.tasks.app.businessobjects.TasksList;
import java.util.List;

public interface LevelColorBp {
    List<LevelColor> copyLevelColorFromTasksLists(Long l, Long l2);

    void createLevelColorsForTasksList(Long l);

    void deleteAllbyTasksListId(Long l);

    List<LevelColor> getAllByTasksListId(Long l);

    List<TasksList> getAllLevelColorTasksLists();

    void saveOrUpdate(LevelColor levelColor);
}
