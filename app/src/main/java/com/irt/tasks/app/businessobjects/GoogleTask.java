package com.irt.tasks.app.businessobjects;

import com.irt.tasks.app.framework.business.IrtTasksBo;

public class GoogleTask implements IrtTasksBo {
    private Long completed;
    private Boolean deleted;
    private Long due;
    private String etag;
    private Boolean hidden;
    private String id;
    private String notes;
    private String parent;
    private String position;
    private String selfLink;
    private String status;
    private String syncResult;
    private String syncStatus;
    private String taskListId;
    private String title;
    private Long updated;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEtag() {
        return this.etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getUpdated() {
        return this.updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public String getSelfLink() {
        return this.selfLink;
    }

    public void setSelfLink(String selfLink) {
        this.selfLink = selfLink;
    }

    public String getParent() {
        return this.parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getPosition() {
        return this.position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getDue() {
        return this.due;
    }

    public void setDue(Long due) {
        this.due = due;
    }

    public Long getCompleted() {
        return this.completed;
    }

    public void setCompleted(Long completed) {
        this.completed = completed;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getHidden() {
        return this.hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getTaskListId() {
        return this.taskListId;
    }

    public void setTaskListId(String taskListId) {
        this.taskListId = taskListId;
    }

    public String getSyncStatus() {
        return this.syncStatus;
    }

    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

    public String getSyncResult() {
        return this.syncResult;
    }

    public void setSyncResult(String syncResult) {
        this.syncResult = syncResult;
    }
}
