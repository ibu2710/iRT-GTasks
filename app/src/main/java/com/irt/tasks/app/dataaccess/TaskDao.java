package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.framework.dataaccess.Dao;
import java.util.List;

public interface TaskDao extends Dao {
    int countTasksByTasksListId(Long l, boolean z);

    void delete(Task task);

    int deleteTasksInTrashBin();

    void forceDelete(Long l);

    void forceDeleteByTaskListId(Long l);

    Task get(Long l);

    List<Task> getAll();

    List<Task> getAllTasksByTasksListId(Long l, String str, boolean z, boolean z2, boolean z3);

    List<Task> getAllTasksByTasksListIdWithDeleted(Long l);

    List<Task> getAllTasksDeletedOnlyByTasksListId(Long l);

    List<Task> getAllTasksForGoogleSync(TasksList tasksList);

    List<Task> getAllTasksTrashBinOnlyByTasksListId(Long l);

    List<Task> getAllTasksWithEligibleAlarms();

    List<Task> getAllTasksWithEligibleAlarmsIncludingInvisibleAccounts();

    List<Task> getAllVisibleTasks(String str, boolean z, boolean z2, boolean z3);

    List<Task> getAllVisibleTasksDeletedOnly();

    List<Task> getAllVisibleTasksTrashBinOnly();

    List<Task> getFilteredVisibleTasks(Long l);

    List<Task> getQuickAllVisibleTasks(String str, String str2);

    List<Task> getTasksMarkedForDeletionButNotInTrashBin(Long l);

    boolean resetTasksProject(Long l);

    void saveOrUpdate(Task task);

    void saveOrUpdateNoUpdateToLastApplicationModifiedDate(Task task);

    void saveOrUpdateWithTimeStamp(Task task);

    List<Task> searchAllExcludeDeleteAndTrashBin(String str);
}
