package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.ImportExportFile;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface FileDao {
    void closeFile();

    long getFileLastModifiedDateMillis(String str);

    InputStream getFileStream(String str) throws IOException;

    List<ImportExportFile> getListOfFiles(String str);

    void putFileOverwrite(String str, String str2) throws Throwable;

    boolean removeSyncMetadataFileListener();

    boolean setSyncMetadataFileListener();
}
