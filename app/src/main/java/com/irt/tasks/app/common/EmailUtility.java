package com.irt.tasks.app.common;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.framework.email.Mail;
import java.util.regex.Pattern;

public class EmailUtility {
    public static final String[] INDENTS = new String[20];
    private static final String INDENT_STR = "     ";

    public static void sendEmail(String[] recipients, String subject, String bodyText, Context context) {
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.setType("text/plain");
        emailIntent.putExtra("android.intent.extra.EMAIL", recipients);
        emailIntent.putExtra("android.intent.extra.SUBJECT", subject);
        emailIntent.putExtra("android.intent.extra.TEXT", bodyText);
        context.startActivity(Intent.createChooser(emailIntent, "Share"));
    }

    public static void send(String subject, String bodyText, Context context) {
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.setType("text/plain");
        emailIntent.putExtra("android.intent.extra.SUBJECT", subject);
        emailIntent.putExtra("android.intent.extra.TEXT", bodyText);
        context.startActivity(Intent.createChooser(emailIntent, "Send contents"));
    }

    public static boolean sendEmailUsingJavaMail(String fromEmailAddress, String password, String[] recipients, String subject, String bodyText, String attachmentFilePath, String fromName) throws Exception {
        Mail mail = new Mail(fromEmailAddress, password);
        mail.setTo(recipients);
        if (StringUtil.isNullOrEmpty(fromName)) {
            mail.setFrom(fromEmailAddress);
        } else {
            mail.setFrom(fromName + " <" + fromEmailAddress + ">");
        }
        mail.setSubject(subject);
        mail.setBody(bodyText);
        if (!StringUtil.isNullOrEmpty(attachmentFilePath)) {
            mail.addAttachment(attachmentFilePath);
        }
        try {
            return mail.send();
        } catch (Exception e) {
            Log.e("MailApp", "Could not send email", e);
            throw e;
        }
    }

    public static boolean isEmailValid(String email) {
        if (Pattern.compile("^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$", 2).matcher(email).matches()) {
            return true;
        }
        return false;
    }

    public static String getTaskNumberSpacer(Task task) {
        if (task.getTaskNumber().intValue() > 999) {
            return "    ";
        }
        if (task.getTaskNumber().intValue() > 99) {
            return "   ";
        }
        if (task.getTaskNumber().intValue() > 9) {
            return " ";
        }
        return " ";
    }

    public static String getCheckTask(Task task) {
        if (task.isCompleted().booleanValue()) {
            return "✓";
        }
        return "";
    }

    public static String getExpandedTask(Task task, boolean isTaskExpanded) {
        if (!task.isParent() || isTaskExpanded) {
            return "";
        }
        return "+";
    }

    public static void initIndentions() {
        INDENTS[0] = " ";
        for (int i = 1; i < 20; i++) {
            INDENTS[i] = INDENTS[i - 1] + INDENT_STR;
        }
    }
}
