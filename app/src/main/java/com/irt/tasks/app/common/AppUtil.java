package com.irt.tasks.app.common;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.SQLException;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Environment;
import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import com.irt.tasks.R;
import com.irt.tasks.app.businessobjects.DatabaseMetadata;
import com.irt.tasks.app.businessobjects.Devices;
import com.irt.tasks.app.businessobjects.LevelState;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.businessprocess.SyncTasksHelper;
import com.irt.tasks.app.businessprocess.TaskBp;
import com.irt.tasks.app.businessprocess.TaskBpImpl;
import com.irt.tasks.app.common.Constants.VERTICAL_VIEW_SECTIONS;
import com.irt.tasks.app.dataaccess.DropboxDaoImpl;
import com.irt.tasks.app.dataaccess.FileDao;
import com.irt.tasks.app.dataaccess.TaskDao;
import com.irt.tasks.app.dataaccess.TaskDaoImpl;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.business.IrtTasksBo;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.admin.AdminDatabaseActivity;
import com.irt.tasks.ui.customrepeat.CustomRepeatHelper;
import com.irt.tasks.ui.receiver.AlarmSetterHelper;
import com.irt.tasks.ui.task.EditTaskHelper;
import com.irt.tasks.ui.taskslist.AbstractMainListActivity;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class AppUtil {
    private static final String DOT_EDIT_EVENT = ".EditEvent";
    private static final String IRT_CALENDAR_PACKAGE = "com.irt.calendar.app.ui";
    private static final String IRT_CALENDAR_PRO_KEY_PACKAGE = "com.irt.calendar.pro.key";
    private static final String IRT_CALENDAR_PRO_TRIAL_PACKAGE = "com.irt.calendar.app.trial";
    private static final String SD_CARD_MOUNTED_STATUS = "mounted";
    private static final String SECURITY_PACKAGE_NAME = "com.irt.tasks.key";
    public static final int YEAR_DAYS_THRESHHOLD = 300;

    private static class ResetAllAlarms extends AsyncTask<Void, Void, Void> {
        Context mContext;

        public ResetAllAlarms(Context context) {
            this.mContext = context;
        }

        protected Void doInBackground(Void... nothing) {
            ConfigManager config = ConfigManager.getInstance();
            TaskDao taskDao = new TaskDaoImpl();
            TaskBp taskBp = new TaskBpImpl();
            boolean origIsShowInvisibleAccounts = config.isShowInvisibleAccounts().booleanValue();
            for (Task task : taskBp.getAllTasksWithEligibleAlarmsIncludingInvisibleAccounts()) {
                AlarmSetterHelper.flushAlarm(this.mContext, task);
            }
            if (config.isEnableReminder()) {
                List<Task> tasks = taskBp.getAllTasksWithEligibleAlarms();
                long todayMillis = AppUtil.getTodayDateTimeMilisecond();
                Calendar next5thMinuteCal = AppUtil.getTheNext5thMinute();
                next5thMinuteCal.set(Calendar.SECOND, 0);
                for (Task task2 : tasks) {
                    boolean isSnoozeBeforeToday = task2.getSnoozeDateTime().longValue() > 0 && todayMillis > task2.getSnoozeDateTime().longValue();
                    AlarmSetterHelper.performScheduleAlarm(this.mContext, task2, true, todayMillis, next5thMinuteCal.getTimeInMillis(), false);
                    if (isSnoozeBeforeToday) {
                        taskDao.saveOrUpdateNoUpdateToLastApplicationModifiedDate(task2);
                        next5thMinuteCal.add(Calendar.MINUTE, 1);
                    }
                }
            }
            return null;
        }
    }

    public static void logDebug(String message) {
        if (ConfigManager.getInstance().isDebug()) {
            Log.d(Constants.TAG, message);
        }
    }

    public static void setDropboxMetadata(FileDao fileDao, DatabaseMetadata dropboxDatabaseMetadata, String databaseMetadataRelativeFilePath) {
        String jsonMetadata = "";
        try {
            InputStream inputStream = fileDao.getFileStream(databaseMetadataRelativeFilePath);
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            while (true) {
                String record = r.readLine();
                if (record == null) {
                    break;
                }
                jsonMetadata = record;
            }
            inputStream.close();
            fileDao.closeFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        dropboxDatabaseMetadata.setObjFromJsonString(jsonMetadata);
    }

    public static void copyLocalDatabaseAndMetadataToDropbox(FileDao fileDao, DatabaseMetadata localDatabaseMetadata, File localDatabaseFile, String databaseRelativeFilePath, String databaseMetadataRelativeFilePath) {
        logDebug("[" + Devices.getDeviceName() + "] is copying current database to dropbox, metadata = " + localDatabaseMetadata.toJsonString());
        try {
            fileDao.putFileOverwrite(getDatabaseFilePath(), databaseRelativeFilePath);
            copyLocalMetadataToDropbox(fileDao, localDatabaseMetadata, localDatabaseFile, databaseMetadataRelativeFilePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public static void copyLocalMetadataToDropbox(FileDao fileDao, DatabaseMetadata localDatabaseMetadata, File localDatabaseFile, String databaseMetadataRelativeFilePath) {
        String jsonMetaData = localDatabaseMetadata.toJsonString();
        logDebug("[" + Devices.getDeviceName() + "] is copying current database metadata to dropbox, metadata = " + jsonMetaData);
        try {
            File metaDataFile = new File(getLocalMetadataFilePath(databaseMetadataRelativeFilePath));
            localDatabaseFile.getParentFile().mkdirs();
            PrintWriter out = new PrintWriter(metaDataFile);
            out.print(jsonMetaData);
            out.close();
            fileDao.putFileOverwrite(getLocalMetadataFilePath(databaseMetadataRelativeFilePath), databaseMetadataRelativeFilePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private static String getLocalMetadataFilePath(String databaseMetadataRelativeFilePath) {
        return getApplicationFilesPath() + databaseMetadataRelativeFilePath;
    }

    public static File setLocalDatabaseMetadata(DatabaseMetadata localDatabaseMetadata) {
        ConfigManager config = ConfigManager.getInstance();
        localDatabaseMetadata.setDatatabaseName(Constants.DROPBOX_DATABASE_RELATIVE_FILE_PATH);
        localDatabaseMetadata.setLastModifiedDateMillis(config.getLastAppModificationDateMillis());
        File databasefile = new File(getDatabaseFilePath());
        localDatabaseMetadata.setCreationDateMillis(config.getDatabaseCreationDateMillis());
        localDatabaseMetadata.setFileSize(Long.valueOf(databasefile.length()));
        localDatabaseMetadata.setDeviceName(Devices.getDeviceName());
        return databasefile;
    }

    public static void showMessage(Context context, int icon, String title, String message) {
        new Builder(context).setIcon(icon).setTitle(title).setMessage(message).setPositiveButton(R.string.alert_dialog_ok, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }

    public static String forceCopyFromDropBox(String fromFilename, String toDirectory, String toFilename) throws Throwable {
        FileOutputStream to;
        IOException e;
        Throwable th;
        FileDao fileDao = new DropboxDaoImpl();
        File toFile = new File(toDirectory);
        if (!toFile.exists()) {
            toFile.mkdirs();
        }
        if (toFile.isDirectory()) {
            toFile = new File(toFile, toFilename);
        }
        String toFileFullPath = toFile.getAbsolutePath();
        String parent = toFile.getParent();
        if (parent == null) {
            parent = System.getProperty("user.dir");
        }
        File dir = new File(parent);
        if (!dir.exists()) {
            throw new IOException("FileCopy: destination directory doesn't exist: " + parent);
        } else if (dir.isFile()) {
            throw new IOException("FileCopy: destination is not a directory: " + parent);
        } else if (dir.canWrite()) {
            InputStream from = null;
            FileOutputStream to2 = null;
            try {
                FileDao fileDao2 = new DropboxDaoImpl();
                try {
                    from = fileDao2.getFileStream(fromFilename);
                    to = new FileOutputStream(toFile);
                } catch (IOException e2) {
                    e = e2;
                    fileDao = fileDao2;
                    try {
                        throw new IOException("ERROR: Dropbox not readable: " + e.getCause());
                    } catch (Throwable th2) {
                        th = th2;
                        if (from != null) {
                            try {
                                from.close();
                                fileDao.closeFile();
                            } catch (IOException e3) {
                            }
                        }
                        if (to2 != null) {
                            try {
                                to2.close();
                            } catch (IOException e4) {
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    fileDao = fileDao2;
                    if (from != null) {
                        from.close();
                        fileDao.closeFile();
                    }
                    if (to2 != null) {
                        to2.close();
                    }
                    throw th;
                }
                try {
                    byte[] buffer = new byte[4096];
                    while (true) {
                        int bytesRead = from.read(buffer);
                        if (bytesRead == -1) {
                            break;
                        }
                        to.write(buffer, 0, bytesRead);
                    }
                    if (from != null) {
                        try {
                            from.close();
                            fileDao2.closeFile();
                        } catch (IOException e5) {
                        }
                    }
                    if (to != null) {
                        try {
                            to.close();
                        } catch (IOException e6) {
                        }
                    }
                    return toFileFullPath;
                } catch (IOException e7) {
                    e = e7;
                    to2 = to;
                    fileDao = fileDao2;
                    throw new IOException("ERROR: Dropbox not readable: " + e.getCause());
                } catch (Throwable th4) {
                    th = th4;
                    to2 = to;
                    fileDao = fileDao2;
                    if (from != null) {
                        from.close();
                        fileDao.closeFile();
                    }
                    if (to2 != null) {
                        to2.close();
                    }
                    throw th;
                }
            } catch (IOException e8) {
                e = e8;
                throw new IOException("ERROR: Dropbox not readable: " + e.getCause());
            }
        } else {
            throw new IOException("FileCopy: destination directory is unwriteable: " + parent);
        }
    }

    public static String getApplicationFilesPath() {
        return Environment.getDataDirectory().getAbsolutePath() + "/data/" + getPackageName() + "/files";
    }

    public static Date getInstallTime(PackageManager packageManager, String packageName) {
        return firstNonNull(installTimeFromPackageManager(packageManager, packageName), apkUpdateTime(packageManager, packageName));
    }

    public static final String getDatabaseFilePath() {
        return Environment.getDataDirectory().getAbsolutePath() + AdminDatabaseActivity.DATABASE_FILE_PATH;
    }

    public static String formatLastDeviceUpdateDate(long deviceLastBackupMillis) {
        if (ConfigManager.getInstance().is24HourFormat()) {
            return DateUtil.format(new Date(deviceLastBackupMillis), "EEE MMM d kk:mm:ss z yyyy");
        }
        return DateUtil.format(new Date(deviceLastBackupMillis), "EEE MMM d h:mm:ss a z yyyy");
    }

    public static void setMenuImage(Context context) {
        final Activity activity = (Activity) context;
        ImageView menuImageView = (ImageView) activity.findViewById(R.id.menu);
        if (ConfigManager.getInstance().isShowMenuButton()) {
            menuImageView.setVisibility(View.VISIBLE);
            menuImageView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    activity.openOptionsMenu();
                }
            });
            return;
        }
        menuImageView.setVisibility(View.GONE);
    }

    public static HashMap<String, String> getHashAlarm() {
        return getHashAlarm(null);
    }

    public static HashMap<String, String> getHashAlarm(String utteranceId) {
        ConfigManager config = ConfigManager.getInstance();
        HashMap<String, String> myHashAlarm = new HashMap();
        if (config.isDeviceAPhone()) {
            myHashAlarm.put("streamType", String.valueOf(2));
        } else {
            myHashAlarm.put("streamType", String.valueOf(3));
        }
        if (!StringUtil.isNullOrEmpty(utteranceId)) {
            myHashAlarm.put("utteranceId", utteranceId);
        }
        return myHashAlarm;
    }

    public static String getFinalAutoSnoozeDescrtiptionForTask(Context context, Task task) {
        if (task.getAutoSnoozeDuration().intValue() != 1) {
            return getStringFromArrayValueBasedOnValue(context, R.array.task_auto_snooze_duration, R.array.task_auto_snooze_duration_values, task.getAutoSnoozeDuration().intValue());
        }
        return getAutoSnoozeDurationDescription(context);
    }

    public static void clearSnooze(Context context, TaskDao taskDao, Task task) {
        boolean isValidAlarm;
        clearNotification(context, task);
        if (task.getSnoozeDateTime().longValue() > 0) {
            isValidAlarm = true;
        } else {
            isValidAlarm = false;
        }
        task.setSnoozeDateTime(Long.valueOf(0));
        AlarmSetterHelper.scheduleAlarm(context, task, false);
        if (isValidAlarm) {
            taskDao.saveOrUpdate(task);
        }
    }

    public static boolean isAutoSnoozeOff(Task task) {
        return getAutoSnoozeDurationMinutes(task) == 0;
    }

    public static String getStringFromArrayValueBasedOnValue(Context context, int descriptionResource, int valueResource, int value) {
        return getStringFromArrayValueBasedOnPosition(context, descriptionResource, getPositionFromArrayValue(context, valueResource, value));
    }

    public static void clearNotification(Context context, Task task) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (task.getId() != null) {
            notificationManager.cancel(task.getId().intValue());
        }
    }

    public static String getAutoSnoozeDurationDescription(Context context) {
        return context.getResources().getStringArray(R.array.auto_snooze_duration)[getPositionFromArrayValue(context, R.array.auto_snooze_duration_values, ConfigManager.getInstance().getAutoSnoozeDuration().intValue())];
    }

    public static String getStringFromArrayValueBasedOnPosition(Context context, int resourceArrayId, int position) {
        return context.getResources().getStringArray(resourceArrayId)[position];
    }

    public static int getPositionFromArrayValue(Context context, int resourceArrayId, int arrayValue) {
        String[] valueArray = context.getResources().getStringArray(resourceArrayId);
        String arrayValueString = String.valueOf(arrayValue);
        for (int i = 0; i < valueArray.length; i++) {
            if (arrayValueString.equals(valueArray[i])) {
                return i;
            }
        }
        return 0;
    }

    public static String getNextAlarmFormat(boolean isIncludeDay) {
        ConfigManager config = ConfigManager.getInstance();
        String dayFormat = "";
        if (isIncludeDay) {
            dayFormat = ", EE";
        }
        if (config.is24HourFormat()) {
            return Constants.DATE_FORMAT_MMMDYYYYKMM + dayFormat;
        }
        return Constants.DATE_FORMAT_MMMDYYYYHMMA + dayFormat;
    }

    public static String formatTimeBeforeNextAlarm(double _minutesBeforeNextReminder) {
        StringBuffer timeBeforeNextEvent = new StringBuffer();
        if (_minutesBeforeNextReminder > 0.0d) {
            long totalHoursBeforeNextReminder = Math.round(Math.floor(_minutesBeforeNextReminder / 60.0d));
            long daysBeforeNextReminder = Math.round(Math.floor((double) (totalHoursBeforeNextReminder / 24)));
            long hoursBeforeNextReminder = (long) Math.round((float) (totalHoursBeforeNextReminder % 24));
            long minutesBeforeNextReminder = Math.round(_minutesBeforeNextReminder % 60.0d);
            if (daysBeforeNextReminder > 0) {
                timeBeforeNextEvent.append(daysBeforeNextReminder + formatPlural("day", daysBeforeNextReminder));
            }
            if (hoursBeforeNextReminder > 0 || daysBeforeNextReminder > 0) {
                timeBeforeNextEvent.append(hoursBeforeNextReminder + formatPlural("hr", hoursBeforeNextReminder));
            }
            timeBeforeNextEvent.append(minutesBeforeNextReminder + " min ");
        }
        return timeBeforeNextEvent.toString();
    }

    private static String formatPlural(String string, long number) {
        if (number > 1 || number < -1) {
            return " " + string + "s ";
        }
        return " " + string + " ";
    }

    public static SpannableStringBuilder getDueDateDescription(Task task, String dateFormat, boolean isPreferenceBasedFormat) {
        if (task.isCompleted().booleanValue() && !isPreferenceBasedFormat) {
            return new SpannableStringBuilder("Task completed");
        }
        SpannableStringBuilder dueDateDescription = new SpannableStringBuilder("Due ");
        dueDateDescription.append(getDueDateDescription(task.getDueDate(), !StringUtil.isNullOrEmpty(dateFormat)));
        if (isPreferenceBasedFormat) {
            dueDateDescription.append(formatDateByPreference(new Date(task.getDueDate().longValue())));
        } else {
            dueDateDescription.append(DateUtil.format(new Date(task.getDueDate().longValue()), dateFormat));
        }
        if (task.getDueDate().longValue() >= getTodayDateOnly().getTime()) {
            return dueDateDescription;
        }
        //dueDateDescription.setSpan(new ForegroundColorSpan(R.color.title_background), 0, dueDateDescription.length(), 0);
        return dueDateDescription;
    }

    public static String getDueDateDescription(Long dueDateMillis, boolean isIncludeComma) {
        String dueDysDescription;
        Calendar dueDateCal = Calendar.getInstance();
        dueDateCal.setTimeInMillis(dueDateMillis.longValue());
        long daysDiff = (dueDateCal.getTimeInMillis() - getTodayDateOnlyCal().getTimeInMillis()) / ElapsedTime.DAY_IN_MILLIS;
        if (daysDiff >= 0) {
            if (daysDiff == 0) {
                dueDysDescription = "Today";
            } else if (daysDiff == 1) {
                dueDysDescription = "Tomorrow";
            } else {
                dueDysDescription = "in " + daysDiff + " days";
            }
        } else if (daysDiff == -1) {
            dueDysDescription = "Yesterday";
        } else {
            dueDysDescription = Math.abs(daysDiff) + " days ago";
        }
        if (isIncludeComma) {
            return dueDysDescription + ", ";
        }
        return dueDysDescription;
    }

    public static boolean isSnoozeEmailAllowed(Task task) {
        if (task.getSnoozeDateTime().longValue() != 0 && getAutoSnoozeDurationMinutes(task) < Constants.SNOOZE_MININMUM_MINUTES_BEFORE_EMAIL_IS_ALLOWED) {
            return false;
        }
        return true;
    }

    public static int getAutoSnoozeDurationMinutes(Task task) {
        ConfigManager config = ConfigManager.getInstance();
        if (task.getAutoSnoozeDuration().intValue() == 1) {
            return config.getAutoSnoozeDuration().intValue();
        }
        if (task.getAutoSnoozeDuration().intValue() > 1) {
            return task.getAutoSnoozeDuration().intValue();
        }
        return 0;
    }

    public static boolean isExternalStorageMounted() {
        return SD_CARD_MOUNTED_STATUS.equals(Environment.getExternalStorageState());
    }

    public static String getBonsaiSdCardDirectory() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.BONSAI_SD_CARD_DIRECTORY;
    }

    public static String getSdCardMainDirectory() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.SD_CARD_MAIN_DIRECTORY;
    }

    public static void turnOffAutoKeyboardPopUp(Context context) {
        ((Activity) context).getWindow().setSoftInputMode(3);
    }

    public static String getBonsaiDateFormat(int dateFormat) {
        switch (dateFormat) {
            case 0:
                return "M/d/yyyy";
            default:
                return "M/d/yyyy";
        }
    }

    public static String getNextDateIsOnWording(Task task) {
        if (task.isRecurringTask()) {
            return "; the next date is on " + DateUtil.format(new Date(CustomRepeatHelper.getNextDueDate(task)), "MMMM dd, yyyy E");
        }
        return "";
    }

    public static void calculateReminderDateTime(Long selectedDueDateMillis, Task task, boolean forceCalc) {
        Calendar calReminder = Calendar.getInstance();
        calReminder.setTimeInMillis(task.getReminderDateTime().longValue());
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(selectedDueDateMillis.longValue());
        int hour = calReminder.get(Calendar.HOUR_OF_DAY);
        int minute = calReminder.get(Calendar.MINUTE);
        if ((!task.isReminderEnabled().booleanValue() && hour == 19 && minute == 0) || forceCalc) {
            Calendar next5thMinuteCal = getTheNext5thMinute();
            if (getDateOnly(cal.getTime()).getTime() < getDateOnly(next5thMinuteCal.getTime()).getTime()) {
                hour = 0;
                minute = 0;
            } else {
                hour = next5thMinuteCal.get(Calendar.HOUR_OF_DAY);
                minute = next5thMinuteCal.get(Calendar.MINUTE);
            }
        }
        cal.add(Calendar.DAY_OF_YEAR, -task.getReminderDaysEarlier().intValue());
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        task.setReminderDateTime(Long.valueOf(cal.getTimeInMillis()));
    }

    private static Calendar getTheNext5thMinute() {
        Calendar todayCal = Calendar.getInstance();
        todayCal.setTime(getTodayDateTime());
        int minute = todayCal.get(Calendar.MINUTE);
        if (minute % 5 == 0) {
            todayCal.add(Calendar.MINUTE, 1);
            minute = todayCal.get(Calendar.MINUTE);
        }
        for (int i = 0; i < 10 && minute % 5 != 0; i++) {
            todayCal.add(Calendar.MINUTE, 1);
            minute = todayCal.get(Calendar.MINUTE);
        }
        return todayCal;
    }

    public static boolean validateExtraEmails(EditText extraEmailEditText, String extraEmails) {
        boolean isFirstEmailAddressFound = false;
        String cleanExtraEmails = "";
        for (String emailAddress : extraEmails.split(Globals.COMMA)) {
            String emailAddress2 = null;
            if (!StringUtil.isNullOrEmpty(emailAddress)) {
                emailAddress2 = emailAddress.trim();
            }
            if (!EmailUtility.isEmailValid(emailAddress2)) {
                return false;
            }
            String delimiter = "";
            if (isFirstEmailAddressFound) {
                delimiter = Globals.COMMA;
            }
            cleanExtraEmails = cleanExtraEmails + delimiter + emailAddress2;
            isFirstEmailAddressFound = true;
        }
        extraEmailEditText.setText(cleanExtraEmails);
        return true;
    }

    public static void setLastApplicationModifiedDate() {
        setLastApplicationModifiedDate(getTodayDateTimeMilisecond());
    }

    public static void setLastApplicationModifiedDate(long dateMillis) {
        ConfigManager.getInstance().setLastAppModificationDateMillis(Long.valueOf(dateMillis));
    }

    public static void setTaskAlarm(Context context, Task task) {
        if (task.isCompleted().booleanValue() || !task.isReminderEnabled().booleanValue()) {
            AlarmSetterHelper.scheduleAlarm(context, task, false);
            return;
        }
        long todayMillis = getTodayDateTimeMilisecond();
        if (todayMillis < task.getReminderDateTime().longValue() || todayMillis < task.getSnoozeDateTime().longValue()) {
            AlarmSetterHelper.scheduleAlarm(context, task, true);
        } else {
            AlarmSetterHelper.scheduleAlarm(context, task, false);
        }
    }

    public static String getReminderDaysEarlier(Long reminderDateTimeLong, Long dueDate) {
        String daysEarlierStr = "";
        Calendar reminderDateCal = Calendar.getInstance();
        reminderDateCal.setTimeInMillis(reminderDateTimeLong.longValue());
        Calendar dueDateCal = Calendar.getInstance();
        dueDateCal.setTimeInMillis(dueDate.longValue());
        int daysEarlier = Math.abs(dueDateCal.get(Calendar.DAY_OF_YEAR) - reminderDateCal.get(Calendar.DAY_OF_YEAR));
        daysEarlierStr = daysEarlier + " days earlier ";
        if (daysEarlier == 1) {
            return daysEarlier + " day earlier ";
        }
        if (daysEarlier == 0) {
            return "On due date ";
        }
        return daysEarlierStr;
    }

    public static void scheduleAllAlarms(Context context) {
        new ResetAllAlarms(context).execute(new Void[0]);
    }

    public static String getReminderTimeFormat() {
        if (ConfigManager.getInstance().is24HourFormat()) {
            return "k:mm";
        }
        return "h:mm a";
    }

    public static boolean isWifiConnected() {
        return ((ConnectivityManager) ConfigManager.getInstance().getContext().getSystemService(Context.CONNECTIVITY_SERVICE)).getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
    }

    public static boolean isOnline() {
        NetworkInfo netInfo = ((ConnectivityManager) ConfigManager.getInstance().getContext().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (netInfo == null || !netInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public static boolean isZoomSet(LevelState levelState) {
        return !StringUtil.isNullOrEmpty(levelState.getZoomTaskIdStr());
    }

    public static void delay(int millisec) {
        try {
            Thread.sleep((long) millisec);
        } catch (InterruptedException ie) {
            System.out.println(ie.getMessage());
        }
    }

    public static String getMaxWindowTitle(String windowTitle) {
        if (StringUtil.isNullOrEmpty(windowTitle) || windowTitle.length() <= 35) {
            return windowTitle;
        }
        return windowTitle.substring(0, 35) + "...";
    }

    public static String getCurrentlySyncedTasksList() {
        return getMaxWindowTitle(ConfigManager.getInstance().getCurrentTasksListBeingSynced());
    }

    public static void refreshDisplay() {
        ConfigManager config = ConfigManager.getInstance();
        if (config.getCurrentContext() != null && (config.getCurrentContext() instanceof AbstractMainListActivity)) {
            ((AbstractMainListActivity) config.getCurrentContext()).refreshDisplay();
        }
    }

    public static List<Task> getMaxSortedTasksForListIncludeCompleted(TasksList tasksList, List<Task> tasks) {
        ConfigManager config = ConfigManager.getInstance();
        boolean isHideCompletedTasks = config.isHideCompletedTasks();
        config.setHideCompletedTasks(false);
        tasks = getMaxSortedTasksForList(tasksList, tasks);
        config.setHideCompletedTasks(isHideCompletedTasks);
        return tasks;
    }

    public static boolean isOfflineAccount(Long accountId) {
        return DatabaseHelper.OFFLINE_ACCOUNT_ID.equals(accountId);
    }

    public static void showGenericMessage(Context context, String title, String message) {
        new Builder(context).setTitle(title).setMessage(message).setPositiveButton("OK", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }

    public static void showGenericWarningMessage(Context context, String title, String message) {
        new Builder(context).setTitle(title).setMessage(message).setPositiveButton("OK", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }

    public static List<Task> getTaskChildren(Task sourceTask, List<Task> tasks) {
        List<Task> taskChildren = new ArrayList();
        for (Task task : tasks) {
            if (task.getParentTaskId().equals(sourceTask.getId())) {
                taskChildren.add(task);
            }
        }
        return taskChildren;
    }

    public static List<Task> getMaxSortedTasksForList(TasksList taskList, List<Task> tasks) {
        SyncTasksHelper syncTasksHelper = new SyncTasksHelper();
        syncTasksHelper.setLevelToDisplay(20);
        return syncTasksHelper.sortTasks(tasks, null, taskList, false, false);
    }

    public static String formatDateByPreference(Date date) {
        ConfigManager config = ConfigManager.getInstance();
        switch (config.getDateFormat().intValue()) {
            case 0:
                return DateFormat.getDateFormat(config.getContext()).format(date);
            case 1:
                return DateUtil.format(date, Constants.DATE_FORMAT_MMDDYYYY);
            case 2:
                return DateUtil.format(date, Constants.DATE_FORMAT_DDMMYYYY);
            case 3:
                return DateUtil.format(date, Constants.DATE_FORMAT_DDDOTMMDOTYYYY);
            case 4:
                return DateUtil.format(date, Constants.DATE_FORMAT_YYYY_MM_DD);
            default:
                return DateFormat.getDateFormat(config.getContext()).format(date);
        }
    }

    public static String formatDateByPreferenceYY(Date date) {
        ConfigManager config = ConfigManager.getInstance();
        java.text.DateFormat df = java.text.DateFormat.getDateInstance(3);
        switch (config.getDateFormat().intValue()) {
            case 0:
                return df.format(date);
            case 1:
                return DateUtil.format(date, Constants.DATE_FORMAT_MMDDYY);
            case 2:
                return DateUtil.format(date, Constants.DATE_FORMAT_DDMMYY);
            case 3:
                return DateUtil.format(date, Constants.DATE_FORMAT_DDDOTMMDOTYY);
            case 4:
                return DateUtil.format(date, Constants.DATE_FORMAT_YYYY_MM_DD);
            default:
                return df.format(date);
        }
    }

    public static String formatTimeByPreference(Date date) {
        return DateUtil.format(date, ConfigManager.getInstance().is24HourFormat() ? Constants.DATE_FORMAT_KKMMSSA : Constants.DATE_FORMAT_HMMSSA);
    }

    public static Calendar getFirstDayOfMonthCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        if (isStartDaySaturdayAndDayIsSunday(cal)) {
            cal.add(Calendar.DAY_OF_YEAR, -6);
        } else {
            cal.add(Calendar.DAY_OF_YEAR, ((-cal.get(Calendar.DAY_OF_WEEK)) + 1) + getStartDayOffset());
        }
        return cal;
    }

    public static boolean isStartDaySaturdayAndDayIsSunday(Calendar cal) {
        if (ConfigManager.getInstance().getStartDay().intValue() == 1 && cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return true;
        }
        return false;
    }

    public static int getStartDayOffset() {
        return ConfigManager.getInstance().getStartDay().intValue();
    }

    public static int getAdjustedPosition(int position) {
        ConfigManager config = ConfigManager.getInstance();
        if (config.getStartDay().intValue() == 1 && position == 6) {
            return 0;
        }
        if (config.getStartDay().intValue() == 1) {
            return position + 1;
        }
        return position;
    }

    public static int getGreenColorStatus() {
        return Color.parseColor("#008400");
    }

    public static int getRedColorStatus() {
        return Color.parseColor("#D73600");
    }

    public static int getFontAutoColor(Task task) {
        if (!task.isTasksListAsTask()) {
            ConfigManager config = ConfigManager.getInstance();
            int taskLevel = task.getLevel().intValue();
            if (config.getLastSelectedTasksList().equals(DatabaseHelper.ALL_TASKS_LIST_ID)) {
                taskLevel--;
            }
            int fontColor = config.getLevelColor(task.getTasksListId(), taskLevel).getFontColor().intValue();
            if (fontColor != 0 && task.getTasksList().isColorTasks().booleanValue()) {
                return EditTaskHelper.getFontColor(fontColor, fontColor);
            }
        }
        return Color.parseColor("#111111");
    }

    public static void setFontBackgroundColor(SpannableStringBuilder fontColorSample, Task task) {
        if (task.getFontBgColor().intValue() != 0) {
            fontColorSample.setSpan(new BackgroundColorSpan(EditTaskHelper.getFontColor(task.getFontBgColor().intValue(), -1)), 0, fontColorSample.length(), 0);
        }
    }

    public static void setFontForegroundColor(SpannableStringBuilder fontColorSample, Task task) {
        fontColorSample.setSpan(new ForegroundColorSpan(EditTaskHelper.getFontColor(task.getFontColor().intValue(), getFontAutoColor(task))), 0, fontColorSample.length(), 0);
    }

    public static void setFontStyle(SpannableStringBuilder fontColorSample, Task task) {
        fontColorSample.setSpan(new StyleSpan(getFontAutoSytle(task)), 0, fontColorSample.length(), 0);
    }

    private static int getFontAutoSytle(Task task) {
        if (!task.isTasksListAsTask() && task.getFontStyle().intValue() == 0) {
            ConfigManager config = ConfigManager.getInstance();
            int taskLevel = task.getLevel().intValue();
            if (config.getLastSelectedTasksList().equals(DatabaseHelper.ALL_TASKS_LIST_ID)) {
                taskLevel--;
            }
            int fontStyle = config.getLevelColor(task.getTasksListId(), taskLevel).getFontStyle().intValue();
            if (fontStyle != 0 && task.getTasksList().isColorTasks().booleanValue()) {
                return EditTaskHelper.getFontStyle(fontStyle);
            }
        }
        return EditTaskHelper.getFontStyle(task.getFontStyle().intValue());
    }

    public static boolean isHide(Task task) {
        return ConfigManager.getInstance().isHideCompletedTasks() && task.isCompleted().booleanValue();
    }

    public static void formatTaskNameForDone(SpannableStringBuilder taskName, Task task) {
        if (task.isCompleted().booleanValue()) {
            taskName.setSpan(new StrikethroughSpan(), 0, taskName.length(), 0);
            taskName.setSpan(new ForegroundColorSpan(Color.parseColor(Constants.DONE_COLOR)), 0, taskName.length(), 0);
            return;
        }
        taskName.setSpan(new ForegroundColorSpan(getFontAutoColor(task)), 0, taskName.length(), 0);
    }

    public static boolean isExpanded(Task task, Map<String, Long> expandedTasks, Long expandedOrCollapsed) {
        boolean isExpanded = expandedTasks.containsKey(getExpandedTaskId(task));
        if (isExpanded) {
            return ((Long) expandedTasks.get(getExpandedTaskId(task))).equals(expandedOrCollapsed);
        }
        return isExpanded;
    }

    public static String getExpandedTaskId(Task task) {
        if (task.isTasksListAsTask()) {
            return Constants.TASK_LIST + task.getTasksListId();
        }
        return task.getId() + "";
    }

    public static Long getExpandOrCollapseIndicator(int taskLevel, int levelToDisplay) {
        ConfigManager config = ConfigManager.getInstance();
        if (taskLevel >= levelToDisplay) {
            return Constants.TASK_EXPAND_INDICATOR;
        }
        return Constants.TASK_COLLAPSE_INDICATOR;
    }

    public static boolean isTodaySelected(Date selectedDate) {
        if (selectedDate == null || getTodayDateOnly().getTime() != selectedDate.getTime()) {
            return false;
        }
        return true;
    }

    public static String getVersionName() {
        try {
            return ConfigManager.getInstance().getContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static boolean isRightTap(float locationOnWidthTapped, View view) {
        if (locationOnWidthTapped > ((float) (view.getMeasuredWidth() / 2))) {
            return true;
        }
        return false;
    }

    public static VERTICAL_VIEW_SECTIONS getVerticalScreenSection(float locationOnHeightTapped, View view) {
        float oneThirdSection = (float) (view.getMeasuredHeight() / 3);
        float middleSectionBottom = oneThirdSection * 2.0f;
        if (locationOnHeightTapped <= oneThirdSection) {
            return VERTICAL_VIEW_SECTIONS.TOP;
        }
        if (locationOnHeightTapped <= oneThirdSection || locationOnHeightTapped > middleSectionBottom) {
            return VERTICAL_VIEW_SECTIONS.BOTTOM;
        }
        return VERTICAL_VIEW_SECTIONS.MIDDLE;
    }

    public static boolean isLandscapeMode(Context context) {
        return context.getResources().getConfiguration().orientation == 2;
    }

    public static void throwDaoNotImplmentedException() {
        throw new SQLException("DAO Method not implemented");
    }

    public static long getDaysDiffLongAccurate(long startDateMillis, long endDateMillis) {
        return (getDateOnly(new Date(endDateMillis)).getTime() - getDateOnly(new Date(startDateMillis)).getTime()) / ElapsedTime.DAY_IN_MILLIS;
    }

    public static long getDaysDiffLong(long startDateMillis, long endDateMillis) {
        Calendar beginDateDateOnlyCal = Calendar.getInstance();
        beginDateDateOnlyCal.setTimeInMillis(startDateMillis);
        Calendar endDateDateOnlyCal = Calendar.getInstance();
        endDateDateOnlyCal.setTimeInMillis(endDateMillis);
        int daysDiff = endDateDateOnlyCal.get(Calendar.DAY_OF_YEAR) - beginDateDateOnlyCal.get(Calendar.DAY_OF_YEAR);
        if (daysDiff > 300 || daysDiff < -300) {
            return getDaysDiffLongAccurate(startDateMillis, endDateMillis);
        }
        return (long) daysDiff;
    }

    public static int getDayDiffInt(Calendar beginDateDateCal, Calendar endDateDateCal) {
        int daysDiff = endDateDateCal.get(Calendar.DAY_OF_YEAR) - beginDateDateCal.get(Calendar.DAY_OF_YEAR);
        if (daysDiff > 300 || daysDiff < -300) {
            return new Long(getDaysDiffLongAccurate(beginDateDateCal.getTimeInMillis(), endDateDateCal.getTimeInMillis())).intValue();
        }
        return daysDiff;
    }

    public static String getCalendarAndroidPackage() {
        return isSdkHigherThanEclairMr1() ? Constants.CALENDAR_PACKAGE_ANDROID_FROYO_UP : Constants.CALENDAR_PACKAGE_ANDROID_MR1_DOWN;
    }

    public static String getCalendarUriPrefix() {
        return isSdkHigherThanEclairMr1() ? Constants.URI_PREFIX_ECLAIR_FROYO_UP : Constants.URI_PREFIX_ECLAIR_MR1_DOWN;
    }

    public static boolean isSdkLowerThanEclair() {
        return VERSION.SDK_INT < 5;
    }

    public static boolean isSdkHigherThanEclairMr1() {
        return VERSION.SDK_INT > 7;
    }

    public static boolean checkForProVersion() {
        ConfigManager config = ConfigManager.getInstance();
        config.setProVersion(isProVersion(config.getContext()));
        return config.isProVersion();
    }

    private static boolean isProVersion(Context context) {
        boolean isProVersion;
        if (context.getPackageManager().checkSignatures(context.getPackageName(), IRT_CALENDAR_PRO_KEY_PACKAGE) == 0) {
            isProVersion = true;
        } else {
            isProVersion = false;
        }
        if (isProVersion || context.getPackageManager().checkSignatures(context.getPackageName(), SECURITY_PACKAGE_NAME) == 0) {
            return true;
        }
        return false;
    }

    public static boolean isIRTCalendarInstalled() {
        ConfigManager config = ConfigManager.getInstance();
        if ((isIRTCalendarAvailable(config.getContext()) && isIRTCalendarProKeyAvailable(config.getContext())) || isIRTCalendarProTrialAvailable(config.getContext())) {
            config.setIRTCalendarAvailable(true);
        } else {
            config.setIRTCalendarAvailable(false);
        }
        return config.isIRTCalendarAvailable();
    }

    public static boolean isIRTCalendarAvailable(Context context) {
        return context.getPackageManager().checkSignatures(context.getPackageName(), IRT_CALENDAR_PACKAGE) == 0;
    }

    private static boolean isIRTCalendarProKeyAvailable(Context context) {
        return true;
    }

    public static boolean isIRTCalendarProTrialAvailable(Context context) {
        return context.getPackageManager().checkSignatures(context.getPackageName(), IRT_CALENDAR_PRO_TRIAL_PACKAGE) == 0;
    }

    public static String getPackageName() {
        return ConfigManager.getInstance().getContext().getPackageName();
    }

    public static String getAppName() {
        return ConfigManager.getInstance().getContext().getString(R.string.app_name);
    }

    public static int getTimeZoneOffset() {
        TimeZone timeZone = TimeZone.getDefault();
        return timeZone.getRawOffset() + timeZone.getDSTSavings();
    }

    public static long getTodayDateTimeMilisecond() {
        return getDate().getTime();
    }

    public static MenuItem getMenuItem(Menu menu, int menuItemId) {
        for (int i = 0; i < menu.size(); i++) {
            MenuItem menuItem = menu.getItem(i);
            if (menuItem.getItemId() == menuItemId) {
                return menuItem;
            }
        }
        return null;
    }

    public static Date getTodayDateTime() {
        return getDate();
    }

    public static Timestamp getDateOnly(Date date) {
        return new Timestamp(DateUtil.parseDate(DateUtil.format(date, Constants.DATE_FORMAT_MMDDYYYY), Constants.DATE_FORMAT_MMDDYYYY).getTime());
    }

    public static Calendar getTodayDateOnlyCal() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getTodayDateOnly());
        return cal;
    }

    public static boolean isDateInRange(Date beginDate, Date endDate, Date dateToTest) {
        long beginDateMilisecond = beginDate.getTime();
        long endDateMilisecond = endDate.getTime();
        long dateToTestMilisecond = dateToTest.getTime();
        return dateToTestMilisecond >= beginDateMilisecond && dateToTestMilisecond <= endDateMilisecond;
    }

    public static Timestamp getTodayDateOnly() {
        return getDateOnly(getDate());
    }

    public static Date addNYears(Date date, int years) {
        Calendar cal = setTimeToLatestDayTime(date);
        cal.add(Calendar.YEAR, years);
        return cal.getTime();
    }

    public static Date setTimeToEndOfDay(Date date) {
        return setTimeToLatestDayTime(date).getTime();
    }

    public static Boolean isYes(String value) {
        return Boolean.valueOf(Constants.YES.equals(value));
    }

    public static String getBooleanAsYorN(Boolean value) {
        return value.booleanValue() ? Constants.YES : Constants.NO;
    }

    public static Boolean isNumberOne(int value) {
        boolean z = true;
        if (1 != value) {
            z = false;
        }
        return Boolean.valueOf(z);
    }

    public static Integer getBooleanAs1or0(Boolean value) {
        return Integer.valueOf(value.booleanValue() ? 1 : 0);
    }

    public static long convertMinutesToMilliseconds(int minutes) {
        return (long) ((minutes * 60) * 1000);
    }

    public static void logDebugClazz(Class clazz, String objectName, Object value) {
        logDebug("RBT???..." + clazz.getSimpleName() + "..." + objectName + " = " + value);
    }

    private static Calendar setTimeToLatestDayTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR, 11);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal;
    }

    private static Date getDate() {
        return new Date();
    }

    private static Date apkUpdateTime(PackageManager packageManager, String packageName) {
        try {
            File apkFile = new File(packageManager.getApplicationInfo(packageName, 0).sourceDir);
            if (apkFile.exists()) {
                return new Date(apkFile.lastModified());
            }
            return null;
        } catch (NameNotFoundException e) {
            return null;
        }
    }

    private static Date installTimeFromPackageManager(PackageManager packageManager, String packageName) {
        try {
            return new Date(PackageInfo.class.getField("firstInstallTime").getLong(packageManager.getPackageInfo(packageName, 0)));
        } catch (NameNotFoundException e) {
            return null;
        } catch (IllegalAccessException e2) {
            return null;
        } catch (NoSuchFieldException e3) {
            return null;
        } catch (IllegalArgumentException e4) {
            return null;
        } catch (SecurityException e5) {
            return null;
        }
    }

    private static Date firstNonNull(Date... dates) {
        for (Date date : dates) {
            if (date != null) {
                return date;
            }
        }
        return null;
    }
}
