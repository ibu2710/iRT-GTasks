package com.irt.tasks.app.common;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public final class IoUtil {
    public static final String getContentsAsString(Reader reader) throws IOException {
        Assert.notNull(reader, "reader");
        char[] cbuf = new char[4096];
        StringBuffer sb = new StringBuffer();
        while (true) {
            int len = reader.read(cbuf, 0, cbuf.length);
            if (len == -1) {
                return sb.toString();
            }
            sb.append(cbuf, 0, len);
        }
    }

    public static final String getContentsAsString(InputStream inputStream) throws IOException {
        Assert.notNull(inputStream, "inputStream");
        return getContentsAsString(new InputStreamReader(inputStream));
    }

    public static final byte[] getContentsAsBytes(InputStream inputStream) throws IOException {
        Assert.notNull(inputStream, "inputStream");
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        while (true) {
            int b = inputStream.read();
            if (b == -1) {
                return byteOut.toByteArray();
            }
            byteOut.write(b);
        }
    }

    private IoUtil() {
    }
}
