package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.todoroo.gtasks.GoogleLoginException;
import com.todoroo.gtasks.GoogleTaskTask;
import com.todoroo.gtasks.actions.Action;
import com.todoroo.gtasks.actions.Actions;
import com.todoroo.gtasks.actions.ListAction;
import com.todoroo.gtasks.actions.ListActions;
import com.todoroo.gtasks.actions.ListActions.TaskCreator;
import com.todoroo.gtasks.actions.ListActions.TaskModifier;
import com.todoroo.gtasks.actions.TaskCreationListAction;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GtaskDaoImpl implements GtaskDao {
    private static final int DELAY_PER_SUB_BATCH_IN_MILLIS = 1000;
    private static final int SUB_BATCH_SIZE_DELETE = 30;
    private static final int SUB_BATCH_SIZE_INSERT = 30;
    private static final int SUB_BATCH_SIZE_UPDATE = 15;
    private static final Actions actions = new Actions();
    private static final ListActions listActions = new ListActions();
    ConfigManager config;
    TaskDao taskDao = new TaskDaoImpl();

    public List<Task> getAllTasksByTasksListId(String tasksListId) {
        this.config = ConfigManager.getInstance();
        List<Task> tasks = new ArrayList();
        try {
            buildTasks(tasks, new HashMap(), this.config.getGoogleTaskService().getTasks(tasksListId));
            return tasks;
        } catch (GoogleLoginException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        } catch (JSONException e2) {
            e2.printStackTrace();
            throw new RuntimeException(e2.getMessage());
        } catch (IOException e3) {
            e3.printStackTrace();
            throw new RuntimeException(e3.getMessage());
        }
    }

    public void batchUpdateTasks(List<Task> tasks, SubBatchType subBatchType) {
        this.config = ConfigManager.getInstance();
        List<Task> subBatchTasks = new ArrayList();
        int subBatchSize = getSubBatchSize(subBatchType);
        int submitCounter = 1;
        long totalRecords = (long) tasks.size();
        int subBatchCounter = 0;
        for (int i = 0; i < tasks.size(); i++) {
            subBatchTasks.add((Task) tasks.get(i));
            if (subBatchCounter < subBatchSize) {
                subBatchCounter++;
            } else {
                if (this.config.isDebug()) {
                    logBatchInfo(totalRecords, subBatchSize, submitCounter);
                    submitCounter++;
                }
                contolledPerformanceOfBatchUpdate(subBatchTasks);
                subBatchTasks.clear();
                subBatchCounter = 0;
                delay(1000);
            }
        }
        if (subBatchCounter > 0) {
            contolledPerformanceOfBatchUpdate(subBatchTasks);
        }
    }

    private void delay(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    private void contolledPerformanceOfBatchUpdate(List<Task> tasks) {
        try {
            performBatchUpdateTasks(tasks);
            for (Task task2 : tasks) {
                if (task2.isDeleted().booleanValue()) {
                    task2.setInTrashBin(Boolean.valueOf(true));
                } else {
                    Task taskFromDB = this.taskDao.get(task2.getId());
                    if (taskFromDB != null) {
                        taskFromDB.setGtaskId(task2.getGtaskId());
                        taskFromDB.setMoved(Boolean.valueOf(false));
                        task2 = taskFromDB;
                    } else {
                        return;
                    }
                }
                this.taskDao.saveOrUpdate(task2);
            }
        } catch (GoogleLoginException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        } catch (JSONException e2) {
            e2.printStackTrace();
            throw new RuntimeException(e2.getMessage());
        } catch (IOException e3) {
            e3.printStackTrace();
            throw new RuntimeException(e3.getMessage());
        }
    }

    private void performBatchUpdateTasks(List<Task> tasks) throws JSONException, IOException, GoogleLoginException {
        this.config = ConfigManager.getInstance();
        if (tasks.size() > 0) {
            Task task;
            Map<Long, ListAction> newTasksMap = new HashMap();
            int numberOfMoves = countNumberOfBatchMoves(tasks);
            String gtaskListId = ((Task) tasks.get(0)).getGtasksListId();
            ListAction[] updateActions = new ListAction[(tasks.size() + numberOfMoves)];
            int actionsCounter = 0;
            for (int i = 0; i < tasks.size(); i++) {
                task = (Task) tasks.get(i);
                if (StringUtil.isNullOrEmpty(task.getGtaskId())) {
                    updateActions[actionsCounter] = ((TaskCreator) ((TaskCreator) ((TaskCreator) ((TaskCreator) ((TaskCreator) listActions.createTask(task.getName()).completed(task.isCompleted().booleanValue())).taskDate(task.getDueDate().longValue())).notes(task.getNote())).parentId(task.getGtasksListId())).deleted(false)).done();
                    newTasksMap.put(task.getId(), updateActions[actionsCounter]);
                    if (this.config.isDebug()) {
                        System.out.println("Inserting to web [" + task.getName() + "] isDelete = " + task.isDeleted());
                    }
                } else {
                    updateActions[actionsCounter] = ((TaskModifier) ((TaskModifier) ((TaskModifier) ((TaskModifier) listActions.modifyTask(task.getGtaskId()).name(task.getName()).deleted(task.isDeleted().booleanValue())).completed(task.isCompleted().booleanValue())).taskDate(task.getDueDate().longValue())).notes(task.getNote())).done();
                    if (this.config.isDebug()) {
                        System.out.println("Updating web [" + task.getName() + "] isDelete = " + task.isDeleted());
                    }
                }
                actionsCounter++;
                if (!task.isDeleted().booleanValue() && task.isMoved().booleanValue()) {
                    if (this.config.isDebug()) {
                        System.out.println("Updating web move ...[" + task.getName() + "] isMoved = " + task.isMoved());
                    }
                    updateActions[actionsCounter] = listActions.move(task.getGtaskId(), task.getParentGtaskId(), task.getPriorSiblingGtaskId());
                    actionsCounter++;
                }
            }
            performUpdateOrInsertTak(gtaskListId, updateActions);
            if (newTasksMap.size() > 0) {
                for (Task task2 : tasks) {
                    if (newTasksMap.containsKey(task2.getId())) {
                        task2.setGtaskId(((TaskCreationListAction) ((ListAction) newTasksMap.get(task2.getId()))).getNewId());
                    }
                }
            }
        }
    }

    private int countNumberOfBatchMoves(List<Task> tasks) {
        int numberOfBatchMoves = 0;
        for (Task task : tasks) {
            if (!task.isDeleted().booleanValue() && task.isMoved().booleanValue()) {
                numberOfBatchMoves++;
            }
        }
        return numberOfBatchMoves;
    }

    private void performUpdateTask(Task task) throws JSONException, IOException, GoogleLoginException {
        ListAction modificationAction = ((TaskModifier) ((TaskModifier) ((TaskModifier) ((TaskModifier) listActions.modifyTask(task.getGtaskId()).name(task.getName()).deleted(task.isDeleted().booleanValue())).completed(task.isCompleted().booleanValue())).taskDate(task.getDueDate().longValue())).notes(task.getNote())).done();
        performUpdateOrInsertTak(task.getGtasksListId(), new ListAction[]{modificationAction});
    }

    private void performInsertTask(Task task) throws JSONException, IOException, GoogleLoginException {
        TaskCreationListAction createTaskAction = ((TaskCreator) ((TaskCreator) ((TaskCreator) ((TaskCreator) ((TaskCreator) listActions.createTask(task.getName()).completed(task.isCompleted().booleanValue())).taskDate(task.getDueDate().longValue())).notes(task.getNote())).parentId(task.getGtasksListId())).deleted(false)).done();
        performUpdateOrInsertTak(task.getGtasksListId(), new ListAction[]{createTaskAction});
        task.setGtaskId(createTaskAction.getNewId());
    }

    public void moveTaskFromListToList(Task task, TasksList sourceList, TasksList destList, Task destParent) {
        try {
            String str;
            this.config = ConfigManager.getInstance();
            String gtaskId = task.getGtaskId();
            String gtasksListId = sourceList.getGtasksListId();
            String gtasksListId2 = destList.getGtasksListId();
            if (destParent == null) {
                str = null;
            } else {
                str = destParent.getGtaskId();
            }
            Action moveTaskAction = actions.moveTask(gtaskId, gtasksListId, gtasksListId2, str);
            this.config.getGoogleTaskService().executeActions(moveTaskAction);
        } catch (JSONException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        } catch (GoogleLoginException e2) {
            e2.printStackTrace();
            throw new RuntimeException(e2.getMessage());
        } catch (IOException e3) {
            e3.printStackTrace();
            throw new RuntimeException(e3.getMessage());
        }
    }

    public void moveTaskWithinSameList(Task task) {
        try {
            System.out.println("Moving [" + task.getName() + "] to parent = [" + task.getParentGtaskId() + "], Prior Sibling = [" + task.getPriorSiblingGtaskId() + "]");
            ListAction moveTaskListAction = listActions.move(task.getGtaskId(), task.getParentGtaskId(), task.getPriorSiblingGtaskId());
            performUpdateOrInsertTak(task.getGtasksListId(), new ListAction[]{moveTaskListAction});
        } catch (JSONException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        } catch (GoogleLoginException e2) {
            e2.printStackTrace();
            throw new RuntimeException(e2.getMessage());
        } catch (IOException e3) {
            e3.printStackTrace();
            throw new RuntimeException(e3.getMessage());
        }
    }

    private void performUpdateOrInsertTak(String gtasksListId, ListAction[] listAction) throws JSONException, IOException, GoogleLoginException {
        this.config = ConfigManager.getInstance();
        this.config.getGoogleTaskService().executeListActions(gtasksListId, listAction);
    }

    private void buildTasks(List<Task> tasks, Map<String, String> childTaskIdsMap, List<GoogleTaskTask> googleTasks) {
        if (googleTasks != null && googleTasks.size() > 0) {
            String priorGtaskId = null;
            for (int i = 0; i < googleTasks.size(); i++) {
                Task task = new Task();
                buildTask(childTaskIdsMap, priorGtaskId, task, (GoogleTaskTask) googleTasks.get(i), tasks);
                tasks.add(task);
                priorGtaskId = task.getGtaskId();
            }
        }
    }

    private void buildTask(Map<String, String> childTaskIdsMap, String priorGtaskId, Task task, GoogleTaskTask googleTask, List<Task> tasks) {
        addChildGTaskIdsToMap(childTaskIdsMap, googleTask);
        task.setWebTask(true);
        task.setName(googleTask.getName());
        task.setGtaskId(googleTask.getId());
        task.setGtasksListId(googleTask.getList_id());
        task.setNote(googleTask.getNotes());
        task.setCompleted(Boolean.valueOf(googleTask.isCompleted()));
        task.setDeleted(Boolean.valueOf(googleTask.isDeleted()));
        task.setLastModified(Long.valueOf(googleTask.getLast_modified()));
        task.setDueDate(Long.valueOf(googleTask.getTask_date()));
        task.setPriorSiblingGtaskId(priorGtaskId);
        task.setParentGtaskId(getParentGTaskId(childTaskIdsMap, googleTask));
        if (priorGtaskId != null && priorGtaskId.equals(task.getParentGtaskId())) {
            task.setPriorSiblingGtaskId(null);
        }
        if (!StringUtil.isNullOrEmpty(task.getPriorSiblingGtaskId())) {
            Task priorSiblingTask = null;
            for (Task task2 : tasks) {
                if (StringUtil.isNullOrEmpty(task2.getParentGtaskId()) && StringUtil.isNullOrEmpty(task.getParentGtaskId())) {
                    priorSiblingTask = task2;
                } else if (!StringUtil.isNullOrEmpty(task2.getParentGtaskId()) && task2.getParentGtaskId().equals(task.getParentGtaskId())) {
                    priorSiblingTask = task2;
                }
            }
            if (priorSiblingTask != null) {
                task.setPriorSiblingGtaskId(priorSiblingTask.getGtaskId());
            }
        }
    }

    private String getParentGTaskId(Map<String, String> childTaskIdsMap, GoogleTaskTask googleTask) {
        if (childTaskIdsMap.containsKey(googleTask.getId())) {
            return (String) childTaskIdsMap.get(googleTask.getId());
        }
        return null;
    }

    private void addChildGTaskIdsToMap(Map<String, String> childTaskIdsMap, GoogleTaskTask googleTask) {
        String[] childIds = googleTask.getChild_ids();
        if (childIds != null && childIds.length > 0) {
            for (String childId : childIds) {
                childTaskIdsMap.put(childId, googleTask.getId());
            }
        }
    }

    private int getSubBatchSize(SubBatchType subBatchType) {
        switch (subBatchType) {
            case DELETE:
            case INSERT:
                return 30;
            case UPDATE:
                return 15;
            default:
                return 0;
        }
    }

    private void logBatchInfo(long totalRecords, int subBatchSize, int submitCounter) {
        System.out.println("*****************************************************************************");
        System.out.println("Submiting contolledPerformanceOfBatchUpdate...." + submitCounter + " of " + (totalRecords / ((long) subBatchSize)) + " with " + subBatchSize + " records per batch" + " / " + AppUtil.getTodayDateTime());
        System.out.println("*****************************************************************************");
    }
}
