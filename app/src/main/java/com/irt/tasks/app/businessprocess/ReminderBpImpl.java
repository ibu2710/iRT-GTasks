package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.Reminder;
import com.irt.tasks.app.dataaccess.ReminderDao;
import com.irt.tasks.app.dataaccess.ReminderDaoImpl;
import java.util.List;

public class ReminderBpImpl implements ReminderBp {
    private ReminderDao reminderDao = new ReminderDaoImpl();

    public List<Reminder> getAll() {
        return this.reminderDao.getAll();
    }

    public List<Reminder> getAllByTasksListId(Long tasksListId) {
        return this.reminderDao.getAllByTasksListId(tasksListId);
    }

    public void saveOrUpdate(Reminder reminder) {
        this.reminderDao.saveOrUpdate(reminder);
    }
}
