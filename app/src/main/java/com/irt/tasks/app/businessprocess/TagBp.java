package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.Tag;
import com.irt.tasks.app.businessobjects.TagTask;
import com.irt.tasks.app.common.Constants.UNIQUE_NAME_ERROR;
import java.util.List;

public interface TagBp {
    int deleteAllTagTasksByTagId(Long l);

    int deleteAllTagTasksByTaskId(Long l);

    void deleteTag(Tag tag);

    List<Long> getAllDistinctTagIdsForTagTasks();

    List<TagTask> getAllTagTasksByTagId(Long l);

    List<TagTask> getAllTagTasksByTaskId(Long l);

    List<Tag> getAllTags();

    UNIQUE_NAME_ERROR isValidTagName(Tag tag);

    void saveOrUpdateTag(Tag tag);

    void saveTagTask(TagTask tagTask);
}
