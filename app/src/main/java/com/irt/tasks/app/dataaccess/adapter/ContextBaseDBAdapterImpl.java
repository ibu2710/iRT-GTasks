package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.irt.tasks.app.businessobjects.ContextBase;
import com.irt.tasks.app.framework.business.IrtTasksBo;

public class ContextBaseDBAdapterImpl implements ContextBaseDBAdapter {
    private static final String[] ALL_COLUMNS = new String[]{"_id", "name"};
    public static final String DATABASE_CREATE = "create table context_base (_id integer primary key autoincrement, name text null);";
    public static final String DATABASE_TABLE = "context_base";
    public static final String INDEX_UNIQUE_ON_NAME = "CREATE UNIQUE INDEX idx_unique_context_base_name ON context_base (name) ";
    public static final String NAME = "name";
    public static final String ROW_ID = "_id";
    private DatabaseHelper DBHelper;
    private final Context context;
    private SQLiteDatabase db;

    public ContextBaseDBAdapterImpl(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(this.context);
    }

    public ContextBaseDBAdapterImpl open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.DBHelper.close();
    }

    public boolean delete(long rowId) {
        return this.db.delete(DATABASE_TABLE, new StringBuilder().append("_id=").append(rowId).toString(), null) > 0;
    }

    public Cursor get(long rowId) throws SQLException {
        Cursor mCursor = this.db.query(true, DATABASE_TABLE, ALL_COLUMNS, "_id=" + rowId, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public boolean isNameUsed(String name, Long contextBaseId) {
        String idCriteria = " ";
        if (contextBaseId != null) {
            idCriteria = " AND _id != " + contextBaseId;
        }
        Cursor mCursor = this.db.query(true, DATABASE_TABLE, ALL_COLUMNS, "lower(name)= '" + name.toLowerCase().trim() + "' " + idCriteria, null, null, null, null, null);
        if (mCursor == null) {
            return false;
        }
        if (mCursor.getCount() > 0) {
            return true;
        }
        return false;
    }

    public Cursor getAll() {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, null, null, null, null, "lower(name)");
    }

    public long insert(IrtTasksBo businessObject) {
        return this.db.insert(DATABASE_TABLE, null, populateBusinessObjectContentValues((ContextBase) businessObject));
    }

    public boolean update(IrtTasksBo businessObject) {
        ContextBase contextBase = (ContextBase) businessObject;
        return this.db.update(DATABASE_TABLE, populateBusinessObjectContentValues(contextBase), new StringBuilder().append("_id=").append(contextBase.getId()).toString(), null) > 0;
    }

    private ContentValues populateBusinessObjectContentValues(ContextBase contextBase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", contextBase.getName());
        return contentValues;
    }
}
