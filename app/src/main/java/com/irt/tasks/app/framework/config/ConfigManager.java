package com.irt.tasks.app.framework.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.tasks.Tasks;
import com.google.api.services.tasks.Tasks.Builder;
import com.google.api.services.tasks.TasksScopes;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessobjects.LevelColor;
import com.irt.tasks.app.businessobjects.LevelState;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.LevelColorDaoImpl;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.ui.preferences.TasksPreferences;
import com.irt.tasks.ui.sync.DropboxSyncAdapter;
import com.todoroo.gtasks.GoogleConnectionManager;
import com.todoroo.gtasks.GoogleLoginException;
import com.todoroo.gtasks.GoogleTaskService;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;

public class ConfigManager {
    private static final int NOT_VALID_SCREEN_HEIGHT = 300;
    private static final int NOT_VALID_SCREEN_WIDTH = 200;
    private static ConfigManager instance;
    private Context context;
    private Context currentContext;
    private String currentTasksListBeingSynced = "";
    private Editor editor;
    private Map<String, GoogleTaskService> googleTaskServiceMap = new HashMap();
    private boolean isDropboxFileSentFromThisDevice = false;
    private boolean isDropboxSyncThisTimeOnly = false;
    private boolean isIRTCalendarAvailable = false;
    private boolean isPreferenceAccessed = false;
    private boolean isProVersion = false;
    private boolean isSyncing = false;
    private Boolean isWifiConnected = null;
    private Map<Long, List<LevelColor>> levelColorMap = new HashMap();
    private Map<Long, LevelState> levelStateMap = new HashMap();
    private int screenHeight;
    private int screenWidth;
    private SharedPreferences sharedPreferences;
    private Map<Long, Map<String, Long>> taskStateMap = new HashMap();

    public boolean isLevelColorAvailable(Long tasksListId) {
        return this.levelColorMap.containsKey(tasksListId);
    }

    public LevelColor getLevelColor(Long tasksListId, int level) {
        List<LevelColor> levelColors;
        if (this.levelColorMap.containsKey(tasksListId)) {
            levelColors = (List) this.levelColorMap.get(tasksListId);
            if (levelColors.size() > 0 && level > 0) {
                return (LevelColor) levelColors.get(level - 1);
            }
        }
        if (isDebug()) {
            System.out.println("getLevelColor... getting from DB..." + tasksListId);
        }
        levelColors = new LevelColorDaoImpl().getAllByTasksListId(tasksListId);
        addLevelColorToMap(tasksListId, levelColors);
        if (levelColors.size() > 0 && level > 0) {
            return (LevelColor) levelColors.get(level - 1);
        }
        return new LevelColor();
    }

    public void addLevelColorToMap(Long tasksListId, List<LevelColor> levelColors) {
        if (this.levelColorMap.containsKey(tasksListId)) {
            this.levelColorMap.remove(tasksListId);
        }
        this.levelColorMap.put(tasksListId, levelColors);
    }

    public LevelState getLevelState(Long tasksListId) {
        return (LevelState) this.levelStateMap.get(tasksListId);
    }

    public void addLevelState(LevelState levelState) {
        this.levelStateMap.put(levelState.getTasksListId(), levelState);
    }

    public ConfigManager(Context context) {
        this.context = context;
    }

    private void initScreenHeightAndWidth() {
        switch (getScreenSize()) {
            case 0:
                autoScreenHeightAndWidthCalc();
                return;
            case 1:
                this.screenHeight = 480;
                this.screenWidth = ScreenSizes.MYTOUCH_3G_HVGA_WIDTH;
                return;
            case 2:
                this.screenHeight = ScreenSizes.NEXUS_ONE_WVGA_HEIGHT;
                this.screenWidth = 480;
                return;
            case 3:
                this.screenHeight = ScreenSizes.DROID_FWVGA_HEIGHT;
                this.screenWidth = 480;
                return;
            default:
                setDefaultScreenSize();
                return;
        }
    }

    private void autoScreenHeightAndWidthCalc() {
        DisplayMetrics dm = new DisplayMetrics();
        ((WindowManager) this.context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(dm);
        this.screenHeight = dm.heightPixels;
        this.screenWidth = dm.widthPixels;
        if (this.screenWidth > this.screenHeight) {
            this.screenHeight = dm.widthPixels;
            this.screenWidth = dm.heightPixels;
        }
        if (this.screenHeight < 300 || this.screenWidth < 200) {
            setDefaultScreenSize();
        }
    }

    private void setDefaultScreenSize() {
        this.screenHeight = ScreenSizes.DROID_FWVGA_HEIGHT;
        this.screenWidth = 480;
    }

    public static ConfigManager getInstance(Context context) {
        if (instance == null) {
            instance = new ConfigManager(context);
            instance.setSharedPreferences(PreferenceManager.getDefaultSharedPreferences(context));
            instance.setSharedPreferencesEditor(instance.getSharedPreferences().edit());
            instance.setScreenSize();
        }
        return instance;
    }

    public Context getCurrentContext() {
        return this.currentContext;
    }

    public void setCurrentContext(Context currentContext) {
        this.currentContext = currentContext;
    }

    public boolean isPreferenceAccessed() {
        return this.isPreferenceAccessed;
    }

    public void setPreferenceAccessed(boolean isPreferenceAccessed) {
        this.isPreferenceAccessed = isPreferenceAccessed;
    }

    public boolean isSyncing() {
        return this.isSyncing;
    }

    public void setSyncing(boolean isSyncing) {
        this.isSyncing = isSyncing;
    }

    public String getCurrentTasksListBeingSynced() {
        return this.currentTasksListBeingSynced;
    }

    public void setCurrentTasksListBeingSynced(String currentTasksListBeingSynced) {
        this.currentTasksListBeingSynced = currentTasksListBeingSynced;
    }

    public GoogleTaskService getGoogleTaskService() {
        Account account = ArchitectureContext.getAccount();
        if (((GoogleTaskService) this.googleTaskServiceMap.get(account.getEmail())) == null) {
            setGoogleTaskService(account);
        }
        return (GoogleTaskService) this.googleTaskServiceMap.get(account.getEmail());
    }

    public void setGoogleTaskService(Account account) {
        if (!DatabaseHelper.OFFLINE_ACCOUNT_ID.equals(account.getId())) {
            if (this.googleTaskServiceMap.containsKey(account.getEmail())) {
                this.googleTaskServiceMap.remove(account.getEmail());
            }
            this.googleTaskServiceMap.put(account.getEmail(), new GoogleTaskService(new GoogleConnectionManager(getAuthenticationToken(account))));
        }
    }

    public String getAuthenticationToken(Account account) {
        try {
            return getGoogleAccountCredential(account).getToken();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (GoogleAuthException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public Tasks getGoogleTaskService2() {
        return getGoogleTaskService2(getGoogleAccountCredential(ArchitectureContext.getAccount()));
    }

    public GoogleAccountCredential getGoogleAccountCredential(Account account) {
        GoogleAccountCredential credential = GoogleAccountCredential.usingOAuth2(getContext(), Collections.singleton(TasksScopes.TASKS));
        credential.setSelectedAccountName(account.getEmail());
        return credential;
    }

    public Tasks getGoogleTaskService2(GoogleAccountCredential credential) {
        return new Builder(AndroidHttp.newCompatibleTransport(), GsonFactory.getDefaultInstance(), credential).setApplicationName("Google-TasksAndroidSample/1.0").build();
    }

    public boolean isValidGoogleAccount(Account account) {
        GoogleConnectionManager googleConnectionManager = new GoogleConnectionManager(account.getEmail(), account.getPassword(), StringUtil.isNullOrEmpty(account.getDomain()));
        String token = null;
        try {
            googleConnectionManager.authenticate(false);
            googleConnectionManager.get();
            token = googleConnectionManager.getToken();
        } catch (GoogleLoginException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (JSONException e3) {
            e3.printStackTrace();
        }
        if (StringUtil.isNullOrEmpty(token)) {
            return false;
        }
        return true;
    }

    public int getScreenSize() {
        return getSharedPereferencesInt(TasksPreferences.SCREEN_SIZE_PREF_KEY);
    }

    public void setScreenSize(Integer screenSize) {
        setSharedPreferencesInteger(TasksPreferences.SCREEN_SIZE_PREF_KEY, screenSize);
    }

    public void setScreenSize() {
        initScreenHeightAndWidth();
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public void setSharedPreferences(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public SharedPreferences getSharedPreferences() {
        return this.sharedPreferences;
    }

    public void setSharedPreferencesEditor(Editor editor) {
        this.editor = editor;
    }

    public static ConfigManager getInstance() {
        return instance;
    }

    public Context getContext() {
        return this.context;
    }

    public boolean isProVersion() {
        return this.isProVersion;
    }

    public void setProVersion(boolean isProVersion) {
        this.isProVersion = isProVersion;
    }

    public boolean isIRTCalendarAvailable() {
        return this.isIRTCalendarAvailable;
    }

    public void setIRTCalendarAvailable(boolean isIRTCalendarAvailable) {
        this.isIRTCalendarAvailable = isIRTCalendarAvailable;
    }

    public Map<String, Long> getTaskStateMapByTasksListId(Long tasksListId) {
        return (Map) this.taskStateMap.get(tasksListId);
    }

    public void addTaskStateMap(Long tasksListId, Map<String, Long> taskStateMap) {
        this.taskStateMap.put(tasksListId, taskStateMap);
    }

    public boolean is24HourFormat() {
        switch (getTimeFormat().intValue()) {
            case 0:
                return DateFormat.is24HourFormat(this.context);
            case 1:
                return false;
            default:
                return true;
        }
    }

    public Long getExpirationDateMillis() {
        return Long.valueOf(getSharedPereferencesLong(TasksPreferences.EXPIRATION_DATE_MILLIS_KEY));
    }

    public void setExpirationDateMillis(Long expirationDateMillis) {
        setSharedPreferencesLong(TasksPreferences.EXPIRATION_DATE_MILLIS_KEY, expirationDateMillis);
    }

    public Long getDropboxLastBackupDateFromThisDeviceMillis() {
        return Long.valueOf(getSharedPereferencesLong(TasksPreferences.DROPBOX_LAST_BACKUP_DATE_FROM_THIS_DEVICE_MILLIS_KEY));
    }

    public void setDropboxLastBackupDateFromThisDeviceMillis(Long lastBackupDateFromThisDeviceMillis) {
        setSharedPreferencesLong(TasksPreferences.DROPBOX_LAST_BACKUP_DATE_FROM_THIS_DEVICE_MILLIS_KEY, lastBackupDateFromThisDeviceMillis);
    }

    public Long getLastAppModificationDateMillis() {
        return Long.valueOf(getSharedPereferencesLong(TasksPreferences.LAST_APP_MODIFCATION_DATE_MILLIS_KEY));
    }

    public void setLastAppModificationDateMillis(Long lastAppModificationDateMillis) {
        setDataEditInitiated(Boolean.valueOf(true));
        setSharedPreferencesLong(TasksPreferences.LAST_APP_MODIFCATION_DATE_MILLIS_KEY, lastAppModificationDateMillis);
    }

    public void setLastAppModificationDateMillisWithNoDataEditInitiated(Long lastAppModificationDateMillis) {
        setSharedPreferencesLong(TasksPreferences.LAST_APP_MODIFCATION_DATE_MILLIS_KEY, lastAppModificationDateMillis);
    }

    public Long getDatabaseCreationDateMillis() {
        Long databaseCreationDateMillis = Long.valueOf(getSharedPereferencesLong(TasksPreferences.LOCAL_DATABASE_CREATION_DATE_PREF_KEY));
        if (databaseCreationDateMillis == null || databaseCreationDateMillis.longValue() == 0) {
            setDatabaseCreationDateMillis(Long.valueOf(AppUtil.getInstallTime(this.context.getPackageManager(), AppUtil.getPackageName()).getTime()));
        }
        return Long.valueOf(getSharedPereferencesLong(TasksPreferences.LOCAL_DATABASE_CREATION_DATE_PREF_KEY));
    }

    public void setDatabaseCreationDateMillis(Long lastAppModificationDateMillis) {
        setSharedPreferencesLong(TasksPreferences.LOCAL_DATABASE_CREATION_DATE_PREF_KEY, lastAppModificationDateMillis);
    }

    public Long getDropboxSyncDateMillis() {
        return Long.valueOf(getSharedPereferencesLong(TasksPreferences.DROPBOX_SYNC_DATE_PREF_KEY));
    }

    public void setDropboxSyncDateMillis(Long lastAppModificationDateMillis) {
        setSharedPreferencesLong(TasksPreferences.DROPBOX_SYNC_DATE_PREF_KEY, lastAppModificationDateMillis);
    }

    public Long getSDCardLastBackupDateFromThisDeviceMillis() {
        return Long.valueOf(getSharedPereferencesLong(TasksPreferences.SD_CARD_LAST_BACKUP_DATE_FROM_THIS_DEVICE_MILLIS_KEY));
    }

    public void setSDCardLastBackupDateFromThisDeviceMillis(Long lastBackupDateFromThisDeviceMillis) {
        setSharedPreferencesLong(TasksPreferences.SD_CARD_LAST_BACKUP_DATE_FROM_THIS_DEVICE_MILLIS_KEY, lastBackupDateFromThisDeviceMillis);
    }

    public boolean isNeedToInitializePreference(String newPreferenceKey) {
        return !this.sharedPreferences.contains(newPreferenceKey);
    }

    public void setApplicationVersion(String applicationVersion) {
        setSharedPreferencesString(TasksPreferences.APP_VERSION_PREF_KEY, applicationVersion);
    }

    public String getApplicationVersion() {
        return getSharedPereferencesString(TasksPreferences.APP_VERSION_PREF_KEY);
    }

    public void setJsonTasksFilterObject(String jsonTasksFilterObject) {
        setSharedPreferencesString(TasksPreferences.JSON_TASKS_FILTER_OBJECT_PREF_KEY, jsonTasksFilterObject);
    }

    public String getJsonTasksFilterObject() {
        return getSharedPereferencesString(TasksPreferences.JSON_TASKS_FILTER_OBJECT_PREF_KEY);
    }

    public void setLastSyncInfo(String lastSyncInfo) {
        setSharedPreferencesString(TasksPreferences.LAST_SYNC_INFO_PREF_KEY, lastSyncInfo);
    }

    public String getLastSyncInfo() {
        return getSharedPereferencesString(TasksPreferences.LAST_SYNC_INFO_PREF_KEY);
    }

    public void setLastSyncInfoDetails(String lastSyncInfoDetails) {
        setSharedPreferencesString(TasksPreferences.LAST_SYNC_INFO_DETAILS_PREF_KEY, lastSyncInfoDetails);
    }

    public String getLastSyncInfoDetails() {
        return getSharedPereferencesString(TasksPreferences.LAST_SYNC_INFO_DETAILS_PREF_KEY);
    }

    public Integer getTopLeftRightSwipeAction() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.TOP_LEFT_RIGHT_SWIPE_LIST_PREF_KEY));
    }

    public Integer getTasksListSort() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.TASKS_LIST_SORT_PREF_KEY));
    }

    public void setTasksListSort(Integer tasksListSort) {
        setSharedPreferencesInteger(TasksPreferences.TASKS_LIST_SORT_PREF_KEY, tasksListSort);
    }

    public Integer getBonsaiDateFormat() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.BONSAI_DATE_FORMAT_LIST_PREF_KEY));
    }

    public void setBonsaiDateFormat(Integer bonsaiDateFormat) {
        setSharedPreferencesInteger(TasksPreferences.BONSAI_DATE_FORMAT_LIST_PREF_KEY, bonsaiDateFormat);
    }

    public Long getLastSelectedTasksList() {
        return Long.valueOf(getSharedPereferencesLong(TasksPreferences.LAST_SELECTED_TASKS_LIST_PREF_KEY));
    }

    public void setLastSelectedTasksList(Long tasksListSort) {
        setSharedPreferencesLong(TasksPreferences.LAST_SELECTED_TASKS_LIST_PREF_KEY, tasksListSort);
    }

    public Integer getPanelToShow() {
        Integer panelToShow = Integer.valueOf(getSharedPereferencesInt(TasksPreferences.PANEL_TO_SHOW_LIST_PREF_KEY));
        if (panelToShow != null) {
            return panelToShow;
        }
        panelToShow = Integer.valueOf(0);
        setPanelToShow(panelToShow);
        return panelToShow;
    }

    public void setPanelToShow(Integer tasksListSort) {
        setSharedPreferencesInteger(TasksPreferences.PANEL_TO_SHOW_LIST_PREF_KEY, tasksListSort);
    }

    public Boolean isShowInvisibleAccounts() {
        return Boolean.valueOf(getSharedPreferencesBoolean(TasksPreferences.SHOW_INVISIBLE_ACCOUNTS_PREF_KEY, false));
    }

    public void setShowInvisibleAccounts(Boolean tasksListSort) {
        setSharedPreferencesBoolean(TasksPreferences.SHOW_INVISIBLE_ACCOUNTS_PREF_KEY, tasksListSort.booleanValue());
    }

    public Boolean isTutorialsTasksListsCreated() {
        return Boolean.valueOf(getSharedPreferencesBoolean(TasksPreferences.TUTORIALS_TASKS_LISTS_CREATED_KEY, false));
    }

    public void setTutorialsTasksListsCreated(Boolean isTutorialsTasksListsCreated) {
        setSharedPreferencesBoolean(TasksPreferences.TUTORIALS_TASKS_LISTS_CREATED_KEY, isTutorialsTasksListsCreated.booleanValue());
    }

    public Integer getMiddleLeftRightSwipeAction() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.MIDDLE_LEFT_RIGHT_SWIPE_LIST_PREF_KEY));
    }

    public Integer getBottomLeftRightSwipeAction() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.BOTTOM_LEFT_RIGHT_SWIPE_LIST_PREF_KEY));
    }

    public Integer getLeftTapAction() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.LEFT_TAP_LIST_PREF_KEY));
    }

    public Integer getRightTapAction() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.RIGHT_TAP_LIST_PREF_KEY));
    }

    public void setRightTapAction(Integer rightTapAction) {
        setSharedPreferencesInteger(TasksPreferences.RIGHT_TAP_LIST_PREF_KEY, rightTapAction);
    }

    public Integer getLeftDoubleTapAction() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.LEFT_DOUBLE_TAP_LIST_PREF_KEY));
    }

    public Integer getRightDoubleTapAction() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.RIGHT_DOUBLE_TAP_LIST_PREF_KEY));
    }

    public Integer getLeftLongPressAction() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.LEFT_LONG_PRESS_LIST_PREF_KEY));
    }

    public Integer getRightLongPressAction() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.RIGHT_LONG_PRESS_LIST_PREF_KEY));
    }

    public Integer getStartDay() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.START_DAY_LIST_PREF_KEY));
    }

    public Integer getTaskNameFontSize() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.TASK_NAME_FONT_SIZE_LIST_PREF_KEY));
    }

    public void setTaskNameFontSize(Integer taskNameFontSize) {
        setSharedPreferencesInteger(TasksPreferences.TASK_NAME_FONT_SIZE_LIST_PREF_KEY, taskNameFontSize);
    }

    public Integer getTaskNoteFontSize() {
        if (getSharedPereferencesInt(TasksPreferences.TASK_NOTE_FONT_SIZE_LIST_PREF_KEY) == 0) {
            setTaskNoteFontSize(Integer.valueOf(19));
        }
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.TASK_NOTE_FONT_SIZE_LIST_PREF_KEY));
    }

    public void setTaskNoteFontSize(Integer taskNoteFontSize) {
        setSharedPreferencesInteger(TasksPreferences.TASK_NOTE_FONT_SIZE_LIST_PREF_KEY, taskNoteFontSize);
    }

    public Integer getDropboxSyncAfterLastEditIdleSeconds() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.DROPBOX_SYNC_AFTER_LAST_EDIT_IDLE_LIST_PREF_KEY));
    }

    public Integer getDropboxSyncIconType() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.DROPBOX_SYNC_ICON_LIST_PREF_KEY));
    }

    public Integer getDropboxDelayInReceivingFilesFromOtherDevicesSeconds() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.f73x6debeda0));
    }

    public boolean isShowDoneCheckbox() {
        return getSharedPreferencesBoolean(TasksPreferences.SHOW_DONE_CHECKBOX_CHECKBOX_PREF_KEY, false);
    }

    public boolean isDropboxSyncThisTimeOnly() {
        return this.isDropboxSyncThisTimeOnly;
    }

    public void setDropboxSyncThisTimeOnly(Boolean isDropboxSyncThisTimeOnly) {
        this.isDropboxSyncThisTimeOnly = isDropboxSyncThisTimeOnly.booleanValue();
    }

    public boolean isDropboxFileSentFromThisDevice() {
        return this.isDropboxFileSentFromThisDevice;
    }

    public void setDropboxFileSentFromThisDevice(Boolean isDropboxFileSentFromThisDevice) {
        this.isDropboxFileSentFromThisDevice = isDropboxFileSentFromThisDevice.booleanValue();
    }

    public boolean isWifiConnected() {
        if (this.isWifiConnected == null) {
            this.isWifiConnected = Boolean.valueOf(AppUtil.isWifiConnected());
        }
        return this.isWifiConnected.booleanValue();
    }

    public void setWifiConnected(Boolean isWifiConnected) {
        this.isWifiConnected = isWifiConnected;
    }

    public String getDropboxSyncStatus() {
        return getSharedPereferencesString(TasksPreferences.DROPBOX_SYNC_STATUS_PREF_KEY);
    }

    public void setDropboxSyncStatus(String dropboxSyncStatus) {
        setSharedPreferencesString(TasksPreferences.DROPBOX_SYNC_STATUS_PREF_KEY, dropboxSyncStatus);
    }

    public boolean isDataEditInitiated() {
        return Boolean.valueOf(getSharedPreferencesBoolean(TasksPreferences.IS_DATA_EDIT_INITIATED_PREF_KEY, false)).booleanValue();
    }

    public void setDataEditInitiated(Boolean isDataEditInitiated) {
        if (!isDropboxAutoSync()) {
            isDataEditInitiated = Boolean.valueOf(false);
        }
        if (isDataEditInitiated.booleanValue()) {
            setDropboxSyncStatus(DropboxSyncAdapter.SYNCING_STATUS_SOURCE_DATA_MODIFIED);
        } else {
            setDropboxSyncStatus(DropboxSyncAdapter.SYNCING_STATUS_COMPLETE);
        }
        setSharedPreferencesBoolean(TasksPreferences.IS_DATA_EDIT_INITIATED_PREF_KEY, isDataEditInitiated.booleanValue());
    }

    public boolean isDragAndDropMode() {
        return Boolean.valueOf(getSharedPreferencesBoolean(TasksPreferences.DRAG_AND_DROP_CHECKBOX_PREF_KEY, false)).booleanValue();
    }

    public void setDragAndDropMode(Boolean isDragAndDropMode) {
        setSharedPreferencesBoolean(TasksPreferences.DRAG_AND_DROP_CHECKBOX_PREF_KEY, isDragAndDropMode.booleanValue());
    }

    public boolean isBackupInitializedAfterInstall() {
        return Boolean.valueOf(getSharedPreferencesBoolean(TasksPreferences.BACKUP_INITIALIZED_AFTER_INSTALL_CHECKBOX_PREF_KEY, false)).booleanValue();
    }

    public void setBackupInitializedAfterInstall(Boolean isBackupInitializedAfterInstall) {
        setSharedPreferencesBoolean(TasksPreferences.BACKUP_INITIALIZED_AFTER_INSTALL_CHECKBOX_PREF_KEY, isBackupInitializedAfterInstall.booleanValue());
    }

    public boolean isViewFromHome() {
        Boolean isViewFromHome = Boolean.valueOf(getSharedPreferencesBoolean(TasksPreferences.VIEW_FROM_HOME_CHECKBOX_PREF_KEY, false));
        if (isViewFromHome == null) {
            isViewFromHome = Boolean.valueOf(false);
            setViewFromHome(isViewFromHome);
        }
        return isViewFromHome.booleanValue();
    }

    public void setViewFromHome(Boolean isViewFromHome) {
        setSharedPreferencesBoolean(TasksPreferences.VIEW_FROM_HOME_CHECKBOX_PREF_KEY, isViewFromHome.booleanValue());
    }

    public boolean isShowTasksListStatistics() {
        return Boolean.valueOf(getSharedPreferencesBoolean(TasksPreferences.SHOW_TASKS_LIST_STATISTICS_CHECKBOX_PREF_KEY, true)).booleanValue();
    }

    public void setShowTasksListStatistics(Boolean isShowTasksListStatistics) {
        setSharedPreferencesBoolean(TasksPreferences.SHOW_TASKS_LIST_STATISTICS_CHECKBOX_PREF_KEY, isShowTasksListStatistics.booleanValue());
    }

    public boolean isShowTasksContexts() {
        return Boolean.valueOf(getSharedPreferencesBoolean(TasksPreferences.SHOW_TASKS_CONTEXTS_CHECKBOX_PREF_KEY, true)).booleanValue();
    }

    public void setShowTasksContexts(Boolean isShowTasksContexts) {
        setSharedPreferencesBoolean(TasksPreferences.SHOW_TASKS_CONTEXTS_CHECKBOX_PREF_KEY, isShowTasksContexts.booleanValue());
    }

    public boolean isReminderEmailValidated() {
        return Boolean.valueOf(getSharedPreferencesBoolean(TasksPreferences.REMINDER_EMAIL_VALIDATED_CHECKBOX_PREF_KEY, false)).booleanValue();
    }

    public void setReminderEmailValidated(Boolean isReminderEmailValidated) {
        setSharedPreferencesBoolean(TasksPreferences.REMINDER_EMAIL_VALIDATED_CHECKBOX_PREF_KEY, isReminderEmailValidated.booleanValue());
    }

    public boolean isTalkingCalendarInitialized() {
        return Boolean.valueOf(getSharedPreferencesBoolean(TasksPreferences.TALKING_CALENDAR_INITIALIZED_CHECKBOX_PREF_KEY, false)).booleanValue();
    }

    public void setTalkingCalendarInitialized(Boolean isTalkingCalendarInitialized) {
        setSharedPreferencesBoolean(TasksPreferences.TALKING_CALENDAR_INITIALIZED_CHECKBOX_PREF_KEY, isTalkingCalendarInitialized.booleanValue());
    }

    public boolean isDeviceAPhone() {
        return ((TelephonyManager) this.context.getSystemService("phone")).getPhoneType() > 0;
    }

    public boolean isShowQuestionMarkMessage() {
        return getSharedPreferencesBoolean(TasksPreferences.SHOW_QUESTION_MARK_MESSAGE_KEY, false);
    }

    public void setShowQuestionMarkMessage(Boolean isShowQuestionMarkMessage) {
        setSharedPreferencesBoolean(TasksPreferences.SHOW_QUESTION_MARK_MESSAGE_KEY, isShowQuestionMarkMessage.booleanValue());
    }

    public boolean isShowDropboxCloudHelpMessage() {
        return getSharedPreferencesBoolean(TasksPreferences.SHOW_DROPBOX_CLOUD_HELP_MESSAGE_KEY, true);
    }

    public void setShowDropboxCloudHelpMessage(Boolean isShowDropboxCloudMessage) {
        setSharedPreferencesBoolean(TasksPreferences.SHOW_DROPBOX_CLOUD_HELP_MESSAGE_KEY, isShowDropboxCloudMessage.booleanValue());
    }

    public boolean isSwipeMoveLeftRightOnDragAndDropMode() {
        return getSharedPreferencesBoolean(TasksPreferences.SWIPE_MOVE_LEFT_RIGHT_ON_DRAG_AND_DROP_PREF_KEY, false);
    }

    public boolean isDropboxMode() {
        return getSharedPreferencesBoolean(TasksPreferences.DROPBOX_MODE, false);
    }

    public void setDropboxMode(boolean isDropboxMode) {
        setSharedPreferencesBoolean(TasksPreferences.DROPBOX_MODE, isDropboxMode);
    }

    public boolean isShowTaskNumbers() {
        return getSharedPreferencesBoolean(TasksPreferences.SHOW_TASK_NUMBERS_CHECKBOX_PREF_KEY, false);
    }

    public void setShowTaskNumbers(boolean isShowTaskNumbers) {
        setSharedPreferencesBoolean(TasksPreferences.SHOW_TASK_NUMBERS_CHECKBOX_PREF_KEY, isShowTaskNumbers);
    }

    public boolean isHideCompletedTasks() {
        return getSharedPreferencesBoolean(TasksPreferences.HIDE_COMPLETED_TASKS_CHECKBOX_PREF_KEY, false);
    }

    public void setHideCompletedTasks(boolean isHideCompletedTasks) {
        setSharedPreferencesBoolean(TasksPreferences.HIDE_COMPLETED_TASKS_CHECKBOX_PREF_KEY, isHideCompletedTasks);
    }

    public boolean isShowMenuButton() {
        return getSharedPreferencesBoolean(TasksPreferences.SHOW_MENU_BUTTON_CHECKBOX_PREF_KEY, false);
    }

    public boolean isDebug() {
        return getSharedPreferencesBoolean(TasksPreferences.DEBUG_CHECKBOX_PREF_KEY, false);
    }

    public boolean isShowFloatsOnAgenda() {
        return getSharedPreferencesBoolean(TasksPreferences.SHOW_FLOATS_ON_AGENDA_CHECKBOX_PREF_KEY, false);
    }

    public boolean isShowAllTasksOnFilter() {
        return getSharedPreferencesBoolean(TasksPreferences.SHOW_ALL_TASKS_ON_FILTER_CHECKBOX_PREF_KEY, true);
    }

    public void setShowDoneCheckbox(boolean isShowDoneCheckbox) {
        setSharedPreferencesBoolean(TasksPreferences.SHOW_DONE_CHECKBOX_CHECKBOX_PREF_KEY, isShowDoneCheckbox);
    }

    public Integer getHighlightStyle() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.HIGHLIGHT_STYLE_LIST_PREF_KEY));
    }

    public boolean isCreateExtraBackup() {
        return getSharedPreferencesBoolean(TasksPreferences.CREATE_EXTRA_BACKUP_CHECKBOX_KEY, false);
    }

    public boolean isLinkifyNotes() {
        return getSharedPreferencesBoolean(TasksPreferences.LINKIFY_NOTES_CHECKBOX_PREF_KEY, false);
    }

    public boolean isStickyView() {
        return getSharedPreferencesBoolean(TasksPreferences.STICKY_VIEW_CHECKBOX_KEY, false);
    }

    public boolean isLockExportType() {
        return getSharedPreferencesBoolean(TasksPreferences.LOCK_EXPORT_TYPE_CHECKBOX_PREF_KEY, false);
    }

    public boolean isLockDateFormat() {
        return getSharedPreferencesBoolean(TasksPreferences.LOCK_DATE_FORMAT_CHECKBOX_PREF_KEY, false);
    }

    public boolean isSortFilenamesByDate() {
        return getSharedPreferencesBoolean(TasksPreferences.SORT_FILENAMES_BY_DATE_CHECKBOX_PREF_KEY, false);
    }

    public boolean isEnableReminderSound() {
        return getSharedPreferencesBoolean(TasksPreferences.REMINDER_ENABLE_SOUND_CHECKBOX_KEY, false);
    }

    public boolean isEnableTextToSpeech() {
        return getSharedPreferencesBoolean(TasksPreferences.ENABLE_TEXT_TO_SPEECH_CHECKBOX_KEY, false);
    }

    public void setEnableTextToSpeech(boolean isEnableTextToSpeech) {
        setSharedPreferencesBoolean(TasksPreferences.ENABLE_TEXT_TO_SPEECH_CHECKBOX_KEY, isEnableTextToSpeech);
    }

    public boolean isEnableReminder() {
        return getSharedPreferencesBoolean(TasksPreferences.ENABLE_REMINDER_CHECKBOX_KEY, false);
    }

    public void setEnableReminder(boolean isEnableReminder) {
        setSharedPreferencesBoolean(TasksPreferences.ENABLE_REMINDER_CHECKBOX_KEY, isEnableReminder);
    }

    public Integer getReminderSnoozeDuration() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.REMINDER_SNOOZE_DURATION_LIST_PREF_KEY));
    }

    public boolean isAutoSync() {
        return getSharedPreferencesBoolean(TasksPreferences.AUTO_SYNC_CHECKBOX_PREF_KEY, false);
    }

    public void setAutoSync(boolean isAutoSync) {
        setSharedPreferencesBoolean(TasksPreferences.AUTO_SYNC_CHECKBOX_PREF_KEY, isAutoSync);
    }

    public boolean isDropboxAutoSync() {
        return getSharedPreferencesBoolean(TasksPreferences.DROPBOX_AUTO_SYNC_CHECKBOX_PREF_KEY, false);
    }

    public void setDropboxAutoSync(boolean isAutoSync) {
        setSharedPreferencesBoolean(TasksPreferences.DROPBOX_AUTO_SYNC_CHECKBOX_PREF_KEY, isAutoSync);
    }

    public boolean isDropboxAutoSyncOnWifiOnly() {
        return getSharedPreferencesBoolean(TasksPreferences.DROPBOX_AUTO_SYNC_ON_WIFI_ONLY_CHECKBOX_PREF_KEY, false);
    }

    public Integer getAutoSyncStartTime() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.AUTO_SYNC_START_TIME_LIST_PREF_KEY));
    }

    public void setAutoSyncStartTime(Integer autoSyncStartTime) {
        setSharedPreferencesInteger(TasksPreferences.AUTO_SYNC_START_TIME_LIST_PREF_KEY, autoSyncStartTime);
    }

    public Integer getAutoSyncEndTime() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.AUTO_SYNC_END_TIME_LIST_PREF_KEY));
    }

    public void setAutoSyncEndTime(Integer autoSyncEndTime) {
        setSharedPreferencesInteger(TasksPreferences.AUTO_SYNC_END_TIME_LIST_PREF_KEY, autoSyncEndTime);
    }

    public Integer getAutoSyncFrequency() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.AUTO_SYNC_FREQUENCY_LIST_PREF_KEY));
    }

    public void setAutoSyncFrequency(Integer autoSyncFrequency) {
        setSharedPreferencesInteger(TasksPreferences.AUTO_SYNC_FREQUENCY_LIST_PREF_KEY, autoSyncFrequency);
    }

    public Integer getNoteVisibleLines() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.NOTE_VISIBLE_LINES_KEY));
    }

    public boolean isPaneledDragAndDrop() {
        return getSharedPreferencesBoolean(TasksPreferences.PANELED_DRAG_AND_DROP_CHECKBOX_PREF_KEY, false);
    }

    public Integer getTimeFormat() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.TIME_FORMAT_LIST_PREF_KEY));
    }

    public Integer getDateFormat() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.DATE_FORMAT_LIST_PREF_KEY));
    }

    public Integer getAutoSnoozeDuration() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.AUTO_SNOOZE_DURATION_LIST_PREF_KEY));
    }

    public Integer getManualSnoozeDurationDefault() {
        return Integer.valueOf(getSharedPereferencesInt(TasksPreferences.MANUAL_SNOOZE_DURATION_DEFAULT_LIST_PREF_KEY));
    }

    public Long getTasksListForNewTaskPlus() {
        return Long.valueOf(getSharedPereferencesLong(TasksPreferences.TASKS_LIST_FOR_NEW_TASK_PLUS_LIST_PREF_KEY));
    }

    public void setTasksListForNewTaskPlus(Long tasksListId) {
        setSharedPreferencesLong(TasksPreferences.TASKS_LIST_FOR_NEW_TASK_PLUS_LIST_PREF_KEY, tasksListId);
    }

    public String getDefaultEmailRecipient() {
        return getSharedPereferencesString(TasksPreferences.DEFAULT_EMAIL_RECIPIENT_EDIT_TEXT_KEY);
    }

    public void setDefaultEmailRecipient(String defaultEmailRecipient) {
        setSharedPreferencesString(TasksPreferences.DEFAULT_EMAIL_RECIPIENT_EDIT_TEXT_KEY, defaultEmailRecipient);
    }

    public boolean isEnableReminderEmail() {
        return getSharedPreferencesBoolean(TasksPreferences.ENABLE_REMINDER_EMAIL_CHECKBOX_PREF_KEY, false);
    }

    public void setEnableReminderEmail(boolean isEnableReminderEmail) {
        setSharedPreferencesBoolean(TasksPreferences.ENABLE_REMINDER_EMAIL_CHECKBOX_PREF_KEY, isEnableReminderEmail);
    }

    private boolean getSharedPreferencesBoolean(String key, boolean defaultValue) {
        return this.sharedPreferences.getBoolean(key, defaultValue);
    }

    private String getSharedPereferencesString(String key) {
        return this.sharedPreferences.getString(key, null);
    }

    private int getSharedPereferencesInt(String key) {
        return new Integer(this.sharedPreferences.getString(key, "0")).intValue();
    }

    private long getSharedPereferencesLong(String key) {
        return new Long(this.sharedPreferences.getString(key, "0")).longValue();
    }

    private void setSharedPreferencesString(String key, String item) {
        this.editor.putString(key, item);
        this.editor.commit();
    }

    private void setSharedPreferencesInteger(String key, Integer item) {
        setSharedPreferencesString(key, item + "");
        this.editor.commit();
    }

    private void setSharedPreferencesLong(String key, Long item) {
        setSharedPreferencesString(key, item + "");
        this.editor.commit();
    }

    private void setSharedPreferencesBoolean(String key, boolean item) {
        this.editor.putBoolean(key, item);
        this.editor.commit();
    }
}
