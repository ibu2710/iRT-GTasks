package com.irt.tasks.app.dataaccess;

import android.database.Cursor;
import com.irt.tasks.app.businessobjects.ContextIncludes;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.dataaccess.adapter.ContextIncludesDBAdapter;
import com.irt.tasks.app.dataaccess.adapter.ContextIncludesDBAdapterImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.ArrayList;
import java.util.List;

public class ContextIncludesDaoImpl implements ContextIncludesDao {
    private ConfigManager config;

    public List<ContextIncludes> getAllByContextBaseId(Long contextBaseId) {
        ContextIncludesDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllByContextBaseId(contextBaseId);
        List<ContextIncludes> contextIncludes = buildContextIncludess(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return contextIncludes;
    }

    public int deleteAllByContextBaseId(Long contextBaseId) {
        ContextIncludesDBAdapter dBAdapter = openDbAdapter();
        int deletes = dBAdapter.deleteAllbyContextBaseId(contextBaseId);
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
        return deletes;
    }

    public void save(ContextIncludes contextIncludes) {
        ContextIncludesDBAdapter dBAdapter = openDbAdapter();
        dBAdapter.insert(contextIncludes);
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    public List<Long> getAllDistinctContextBasesIds() {
        ContextIncludesDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllDistinctContextBasesIds();
        List<Long> contextBasesIds = new ArrayList();
        while (cursor.moveToNext()) {
            contextBasesIds.add(Long.valueOf(cursor.getLong(0)));
        }
        cursor.close();
        closeDbAdapter(dBAdapter);
        return contextBasesIds;
    }

    private List<ContextIncludes> buildContextIncludess(Cursor cursor) {
        List<ContextIncludes> contextIncludess = new ArrayList();
        while (cursor.moveToNext()) {
            contextIncludess.add(buildContextIncludes(cursor));
        }
        return contextIncludess;
    }

    private ContextIncludes buildContextIncludes(Cursor cursor) {
        Long contextId = Long.valueOf(cursor.getLong(0));
        Long includedContextId = Long.valueOf(cursor.getLong(1));
        ContextIncludes contextIncludes = new ContextIncludes(contextId,includedContextId);
        contextIncludes.setContextId(contextId);
        contextIncludes.setIncludedContextId(includedContextId);
        return contextIncludes;
    }

    private ContextIncludesDBAdapter openDbAdapter() {
        this.config = ConfigManager.getInstance();
        ContextIncludesDBAdapter dBAdapter = new ContextIncludesDBAdapterImpl(this.config.getContext());
        dBAdapter.open();
        return dBAdapter;
    }

    private void closeDbAdapter(ContextIncludesDBAdapter dBAdapter) {
        dBAdapter.close();
    }
}
