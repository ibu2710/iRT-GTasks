package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.AccountDao;
import com.irt.tasks.app.dataaccess.AccountDaoImpl;
import com.irt.tasks.app.dataaccess.ContextTaskDao;
import com.irt.tasks.app.dataaccess.ContextTaskDaoImpl;
import com.irt.tasks.app.dataaccess.GtaskDao;
import com.irt.tasks.app.dataaccess.GtaskDao2Impl;
import com.irt.tasks.app.dataaccess.GtasksListDao;
import com.irt.tasks.app.dataaccess.GtasksListDao2Impl;
import com.irt.tasks.app.dataaccess.LevelColorDao;
import com.irt.tasks.app.dataaccess.LevelColorDaoImpl;
import com.irt.tasks.app.dataaccess.TagTaskDao;
import com.irt.tasks.app.dataaccess.TagTaskDaoImpl;
import com.irt.tasks.app.dataaccess.TaskDao;
import com.irt.tasks.app.dataaccess.TaskDaoImpl;
import com.irt.tasks.app.dataaccess.TasksListDao;
import com.irt.tasks.app.dataaccess.TasksListDaoImpl;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.dataaccess.adapter.TasksListDBAdapterImpl;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.ArrayList;
import java.util.List;

public class TasksListBpImpl implements TasksListBp {
    AccountDao accountDao = new AccountDaoImpl();
    ContextTaskDao contextTaskDao = new ContextTaskDaoImpl();
    GtaskDao gtaskDao = new GtaskDao2Impl();
    GtasksListDao gtasksListDao = new GtasksListDao2Impl();
    LevelColorDao levelColorDao = new LevelColorDaoImpl();
    TagTaskDao tagTaskDao = new TagTaskDaoImpl();
    TaskDao taskDao = new TaskDaoImpl();
    TasksListDao tasksListDao = new TasksListDaoImpl();

    public List<TasksList> getAllExcludeMarkedAsDelete() {
        return this.tasksListDao.getAllByAccountIdExcludeMarkedAsDelete(null);
    }

    public void copyListAsBranch(Long sourceTaskListId, TasksList targetTasksList) {
        ConfigManager config = ConfigManager.getInstance();
        TasksList sourceTasksList = this.tasksListDao.get(sourceTaskListId);
        List<Task> sortedSourceTasks = AppUtil.getMaxSortedTasksForListIncludeCompleted(sourceTasksList, this.taskDao.getAllTasksByTasksListId(sourceTaskListId, null, false, false, false));
        List<Task> targetTasks = AppUtil.getMaxSortedTasksForListIncludeCompleted(targetTasksList, this.taskDao.getAllTasksByTasksListId(targetTasksList.getId(), null, false, false, false));
        Task firstTaskOnTarget = null;
        if (targetTasks.size() > 0) {
            firstTaskOnTarget = (Task) targetTasks.get(0);
        }
        Task firstSourceTask = new Task();
        firstSourceTask.setTasksListId(targetTasksList.getId());
        firstSourceTask.setName(sourceTasksList.getName());
        this.taskDao.saveOrUpdateWithTimeStamp(firstSourceTask);
        if (firstTaskOnTarget != null) {
            firstTaskOnTarget.setPriorSiblingTaskId(firstSourceTask.getId());
            firstTaskOnTarget.setMoved(Boolean.valueOf(true));
            this.taskDao.saveOrUpdateWithTimeStamp(firstTaskOnTarget);
        }
        updateSourceTasksParentsWithFirstTaskOnList(sortedSourceTasks, firstSourceTask);
        performCopyTasks(targetTasksList, sortedSourceTasks);
        AppUtil.scheduleAllAlarms(config.getContext());
    }

    private void updateSourceTasksParentsWithFirstTaskOnList(List<Task> sortedSourceTasks, Task firstSourceTask) {
        for (Task task : sortedSourceTasks) {
            if (task.getParentTaskId().equals(Long.valueOf(0))) {
                task.setParentTaskId(firstSourceTask.getId());
            }
        }
    }

    public void copyList(Long sourceTaskListId, Long targetAccountId) {
        ConfigManager config = ConfigManager.getInstance();
        TasksList tasksList = this.tasksListDao.get(sourceTaskListId);
        List<Task> tasks = AppUtil.getMaxSortedTasksForListIncludeCompleted(tasksList, this.taskDao.getAllTasksByTasksListId(sourceTaskListId, null, false, false, false));
        tasksList.setId(null);
        tasksList.setGtasksListId(null);
        tasksList.setAutoSync(Boolean.valueOf(true));
        tasksList.setName("Copy of " + tasksList.getName());
        tasksList.setAccountId(targetAccountId);
        tasksList.setActiveTasksList(Boolean.valueOf(false));
        this.tasksListDao.saveOrUpdate(tasksList);
        performCopyTasks(tasksList, tasks);
        AppUtil.scheduleAllAlarms(config.getContext());
    }

    private void performCopyTasks(TasksList taskList, List<Task> sortedTasks) {
        for (int i = 0; i < sortedTasks.size(); i++) {
            Task task = (Task) sortedTasks.get(i);
            Long origId = task.getId();
            task.setId(null);
            task.setGtaskId(null);
            task.setTasksListId(taskList.getId());
            this.taskDao.saveOrUpdate(task);
            updateParentAndSibling(origId, task.getId(), sortedTasks);
        }
    }

    private void updateParentAndSibling(Long origId, Long newId, List<Task> tasks) {
        for (Task task : tasks) {
            if (origId.equals(task.getParentTaskId())) {
                task.setParentTaskId(newId);
            }
            if (origId.equals(task.getPriorSiblingTaskId())) {
                task.setPriorSiblingTaskId(newId);
            }
        }
    }

    public List<TasksList> getAllTasksListBySortOrder() {
        switch (ConfigManager.getInstance().getTasksListSort().intValue()) {
            case 0:
                return this.tasksListDao.getAllSortedExcludeMarkedAsDelete("name");
            case 1:
                return sortTasksListByAccount();
            case 2:
                return this.tasksListDao.getAllSortedExcludeMarkedAsDelete(TasksListDBAdapterImpl.TASK_LAST_MODIFIED);
            case 3:
                return this.tasksListDao.getAllSortedExcludeMarkedAsDelete("sort_position");
            default:
                return this.tasksListDao.getAllSortedExcludeMarkedAsDelete("name");
        }
    }

    private List<TasksList> sortTasksListByAccount() {
        List<TasksList> tasksLists = new ArrayList();
        for (Account account : this.accountDao.getAllVisible()) {
            tasksLists.addAll(this.tasksListDao.getAllByAccountIdExcludeMarkedAsDelete(account.getId()));
        }
        return tasksLists;
    }

    public void restoreDeletedTasksList(Long tasksListId, String tasksListName) {
        TasksList tasksList = this.tasksListDao.get(tasksListId);
        tasksList.setDeleted(Boolean.valueOf(false));
        tasksList.setName(tasksListName);
        tasksList.setGtasksListName(tasksListName);
        this.tasksListDao.saveOrUpdate(tasksList);
        for (Task task : this.taskDao.getAllTasksByTasksListIdWithDeleted(tasksListId)) {
            task.setDeleted(Boolean.valueOf(false));
            this.taskDao.saveOrUpdate(task);
        }
        AppUtil.scheduleAllAlarms(ConfigManager.getInstance().getContext());
    }

    public void deleteTasksList(Long tasksListId, boolean isSendToTrashBin) {
        TasksList tasksList = this.tasksListDao.get(tasksListId);
        Long accountId = tasksList.getAccountId();
        boolean isTasksListInTrashBin = false;
        List<Task> tasks = this.taskDao.getAllTasksByTasksListIdWithDeleted(tasksListId);
        tasksList.setDeleted(Boolean.valueOf(true));
        tasksList.setAutoSync(Boolean.valueOf(true));
        if (DatabaseHelper.OFFLINE_ACCOUNT_ID.equals(accountId) || StringUtil.isNullOrEmpty(tasksList.getGtasksListId()) || isSendToTrashBin) {
            tasksList.setInTrashBin(Boolean.valueOf(true));
            isTasksListInTrashBin = true;
        }
        this.tasksListDao.saveOrUpdate(tasksList);
        for (Task task : tasks) {
            if (isTasksListInTrashBin) {
                task.setInTrashBin(Boolean.valueOf(true));
            }
            task.setDeleted(Boolean.valueOf(true));
            this.taskDao.saveOrUpdate(task);
        }
        AppUtil.scheduleAllAlarms(ConfigManager.getInstance().getContext());
    }

    public int clearTrashBin() {
        int totalRecordsDeleted = this.taskDao.deleteTasksInTrashBin() + this.tasksListDao.deleteTasksListInTrashBin();
        deleteLevelColor();
        this.contextTaskDao.deleteAllWhereTaskDoesNotExist();
        this.tagTaskDao.deleteAllWhereTaskDoesNotExist();
        return totalRecordsDeleted;
    }

    private void deleteLevelColor() {
        ConfigManager config = ConfigManager.getInstance();
        List<TasksList> tasksLists = this.tasksListDao.getAllIncludingDeletedAndTrashBin();
        for (Long levelColorTasksListId : this.levelColorDao.getAllDistinctTasksListIds()) {
            if (!isTasksListUsed(levelColorTasksListId, tasksLists)) {
                if (config.isDebug()) {
                    System.out.println("Deleting levelColorTasksListId " + levelColorTasksListId);
                }
                this.levelColorDao.deleteAllbyTasksListId(levelColorTasksListId);
            }
        }
    }

    private boolean isTasksListUsed(Long levelColorTasksListId, List<TasksList> tasksLists) {
        for (TasksList tasksList : tasksLists) {
            if (tasksList.getId().equals(levelColorTasksListId)) {
                return true;
            }
        }
        return false;
    }

    public void saveOrUpdateWithTimeStamp(TasksList tasksList) {
        tasksList.setTaskLastModified(Long.valueOf(AppUtil.getTodayDateTimeMilisecond()));
        this.tasksListDao.saveOrUpdateWithTimeStamp(tasksList);
        AppUtil.scheduleAllAlarms(ConfigManager.getInstance().getContext());
    }

    public TasksList get(Long tasksListId) {
        return this.tasksListDao.get(tasksListId);
    }

    public void saveOrUpdate(TasksList tasksList) {
        this.tasksListDao.saveOrUpdate(tasksList);
    }

    public void overwriteGoogleTasks(TasksList tasksList) {
        SyncBp syncBp = new SyncBpImpl();
        if (!DatabaseHelper.ALL_TASKS_LIST_ID.equals(tasksList.getId())) {
            Account account = this.accountDao.get(tasksList.getAccountId());
            if (account.isNewGoogleId().booleanValue()) {
                ArchitectureContext.setAccount(account);
                this.gtasksListDao.getTasksList(tasksList.getGtasksListId());
                this.gtasksListDao.deleteTasksList(tasksList);
                tasksList.setGtasksListId(null);
                this.tasksListDao.saveOrUpdate(tasksList);
                for (Task task : this.taskDao.getAllTasksByTasksListId(tasksList.getId(), null, false, false, false)) {
                    task.setGtaskId(null);
                    this.taskDao.saveOrUpdate(task);
                }
                syncBp.syncWithGoogleTasks(tasksList);
            }
        }
    }

    public void emptyTasksListAndSyncWithGoogle(TasksList tasksList) {
        SyncBp syncBp = new SyncBpImpl();
        if (!DatabaseHelper.ALL_TASKS_LIST_ID.equals(tasksList.getId())) {
            performEmtpyingofTasksListAndSyncWithGoogle(tasksList, syncBp);
            performEmtpyingofTasksListAndSyncWithGoogle(tasksList, syncBp);
        }
    }

    private void performEmtpyingofTasksListAndSyncWithGoogle(TasksList tasksList, SyncBp syncBp) {
        ArchitectureContext.setAccount(this.accountDao.get(tasksList.getAccountId()));
        for (Task task : this.taskDao.getAllTasksByTasksListId(tasksList.getId(), null, false, false, false)) {
            task.setDeleted(Boolean.valueOf(true));
            this.taskDao.saveOrUpdate(task);
        }
        syncBp.syncWithGoogleTasks(tasksList);
    }

    public void overwriteLocalTasks(TasksList tasksList) {
        SyncBp syncBp = new SyncBpImpl();
        if (!DatabaseHelper.ALL_TASKS_LIST_ID.equals(tasksList.getId())) {
            String gtasksListId = tasksList.getGtasksListId();
            Long tasksListId = tasksList.getId();
            Account account = this.accountDao.get(tasksList.getAccountId());
            if (account.isNewGoogleId().booleanValue()) {
                ArchitectureContext.setAccount(account);
                TasksList tasksListFromGoole = this.gtasksListDao.getTasksList(gtasksListId);
                syncBp.updateTasksListNameAndLocalGtasksListName(tasksListFromGoole, tasksList);
                tasksList.setGtasksListId(tasksListFromGoole.getGtasksListId());
                tasksList.setId(null);
                List<Task> gtasks = this.gtaskDao.getAllTasksByTasksListId(gtasksListId);
                List<Task> localTasks = this.taskDao.getAllTasksByTasksListId(tasksListId, null, false, false, false);
                for (Task gtask : gtasks) {
                    Task task = getTaskByGTaskId(gtask, localTasks);
                    if (task != null) {
                        gtask.setDecoratorsForGoogleFromTask(task);
                        gtask.setLastContentModifiedDate(gtask.getLastModified());
                        localTasks.remove(task);
                    }
                }
                deleteTasksList(tasksListId, true);
                this.tasksListDao.saveOrUpdate(tasksList);
                Task prevTask = new Task();
                for (Task gtask2 : gtasks) {
                    gtask2.setTasksListId(tasksList.getId());
                    if (StringUtil.isNullOrEmpty(gtask2.getParentGtaskId())) {
                        gtask2.setParentTaskId(Long.valueOf(0));
                    } else if (gtask2.getParentGtaskId().equals(prevTask.getGtaskId())) {
                        gtask2.setParentTaskId(prevTask.getId());
                    } else if (gtask2.getParentGtaskId().equals(prevTask.getParentGtaskId())) {
                        gtask2.setParentTaskId(prevTask.getParentTaskId());
                    }
                    if (StringUtil.isNullOrEmpty(gtask2.getPriorSiblingGtaskId())) {
                        gtask2.setPriorSiblingTaskId(Constants.NO_SIBLING_ID);
                    } else if (gtask2.getPriorSiblingGtaskId().equals(prevTask.getGtaskId())) {
                        gtask2.setPriorSiblingTaskId(prevTask.getId());
                    }
                    this.taskDao.saveOrUpdate(gtask2);
                    prevTask = gtask2;
                }
                ConfigManager config = ConfigManager.getInstance();
                config.setLastSelectedTasksList(tasksList.getId());
                AppUtil.scheduleAllAlarms(config.getContext());
            }
        }
    }

    private Task getTaskByGTaskId(Task gtask, List<Task> localTasks) {
        for (Task task : localTasks) {
            if (gtask.getGtaskId().equals(task.getGtaskId())) {
                return task;
            }
        }
        return null;
    }

    public void createTutorialTaskLists(Long accountId) {
        Long accountIdToUse = accountId;
        if (accountIdToUse == null) {
            accountIdToUse = DatabaseHelper.OFFLINE_ACCOUNT_ID;
        }
        TasksListBpHelper.createInboxTasksList(accountIdToUse);
        TasksListBpHelper.createLongPressTasksList(accountIdToUse);
        TasksListBpHelper.createTasksTreeTutorialTasksList(accountIdToUse);
    }
}
