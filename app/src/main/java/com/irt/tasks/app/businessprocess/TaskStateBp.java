package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.LevelState;
import java.util.Map;

public interface TaskStateBp {
    LevelState getLevelSate(Long l, String str);

    Map<String, Long> getTaskStateMapByTasksListId(Long l);

    void saveOrUpdateLevelState(LevelState levelState);

    void saveTaskStates(Long l);
}
