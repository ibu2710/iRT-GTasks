package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.common.StringUtil;

public class GoogleTaskContentProvider extends ContentProvider {
    public static final String AUTHORITY = "com.irt.tasks.app.dataaccess.adapter.googletaskcontentprovider";
    public static final String COMPLETED = "completed";
    private static final int COMPLEX_TASK_QUERY = 3;
    public static final String DATABASE_CREATE = "create table task (id text not null, etag  text not null, title text null, updated integer default 0 not null, selfLink text not null, parent text null, position text not null, notes text null, status text not null, due integer null, completed integer null, deleted integer default 0 not null, hidden integer default 0 not null, taskListId text not null, sync_status text null, sync_result text null);";
    public static final String DATABASE_TABLE = "task";
    public static final String DELETED = "deleted";
    public static final String DUE = "due";
    public static final String ETAG = "etag";
    public static final String HIDDEN = "hidden";
    public static final String ID = "id";
    public static final String INDEX_ON_TASK_LIST_ID = "CREATE INDEX idx_task_taskListId ON task (taskListId) ";
    public static final String INDEX_UNIQUE_ON_ID = "CREATE UNIQUE INDEX idx__unique_task_id ON task (id) ";
    public static final String NOTES = "notes";
    public static final String PARENT = "parent";
    public static final String POSITION = "position";
    public static final String SELF_LINK = "selfLink";
    public static final String STATUS = "status";
    public static final String SYNC_RESULT = "sync_result";
    public static final String SYNC_STATUS = "sync_status";
    private static final int TASK = 1;
    private static final int TASKS = 2;
    public static final String TASK_LIST_ID = "taskListId";
    public static final String TITLE = "title";
    public static final String UPDATED = "updated";
    private static final UriMatcher sUriMatcher = new UriMatcher(-1);
    private SQLiteDatabase database;
    private GoogleDatabaseHelper dbHelper;
    public static final String[] ALL_COLUMNS = new String[]{"id", ETAG, "title", UPDATED, SELF_LINK, "parent", "position", NOTES, "status", DUE, "completed", "deleted", HIDDEN, "taskListId", SYNC_STATUS, SYNC_RESULT};

    static {
        sUriMatcher.addURI(AUTHORITY, "task/*", 1);
        sUriMatcher.addURI(AUTHORITY, "tasks", 2);
        sUriMatcher.addURI(AUTHORITY, null, 3);
    }

    public boolean onCreate() {
        this.dbHelper = new GoogleDatabaseHelper(getContext());
        return this.dbHelper != null;
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        this.database = this.dbHelper.getReadableDatabase();
        switch (sUriMatcher.match(uri)) {
            case 1:
                Cursor mCursor = this.database.query(true, "task", projection, "id=\"" + getIdStr(uri) + Globals.QUOTE, null, null, null, null, null);
                if (mCursor == null) {
                    return mCursor;
                }
                mCursor.moveToFirst();
                return mCursor;
            case 2:
                return this.database.query("task", projection, selection, selectionArgs, null, null, sortOrder);
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues values) {
        this.database = this.dbHelper.getWritableDatabase();
        if (this.database.insert("task", null, values) < 0) {
            AppUtil.logDebug("Insert failed for Google Task.");
        }
        return uri;
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        this.database = this.dbHelper.getWritableDatabase();
        switch (sUriMatcher.match(uri)) {
            case 1:
                int count = this.database.delete("task", "id=\"" + getIdStr(uri) + Globals.QUOTE, null);
                getContext().getContentResolver().notifyChange(uri, null);
                return count;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        this.database = this.dbHelper.getWritableDatabase();
        switch (sUriMatcher.match(uri)) {
            case 1:
                int count = this.database.update("task", values, "id = \"" + getIdStr(uri) + Globals.QUOTE, selectionArgs);
                getContext().getContentResolver().notifyChange(uri, null);
                return count;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    private String getIdStr(Uri uri) {
        String lastPathSegment = uri.getLastPathSegment();
        return !StringUtil.isNullOrEmpty(lastPathSegment) ? lastPathSegment : null;
    }
}
