package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.irt.tasks.app.businessobjects.Email;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.framework.business.IrtTasksBo;
import com.irt.tasks.app.framework.security.AndroidCrypto;
import com.irt.tasks.app.framework.security.SecurityKey;

public class EmailDBAdapterImpl implements EmailDBAdapter {
    public static final String SIGNATURE = "signature";
    private static final String[] ALL_COLUMNS = new String[]{"_id", "name", "email", "password", "extra_emails", SIGNATURE};
    public static final String DATABASE_CREATE = "create table email (_id integer primary key autoincrement, name text null, email text not null, password text not null, extra_emails text null, signature  text default 'Sent from my iRT GTasks Outliner' null);";
    public static final String DATABASE_TABLE = "email";
    public static final String EMAIL = "email";
    public static final String EXTRA_EMAILS = "extra_emails";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String ROW_ID = "_id";
    private DatabaseHelper DBHelper;
    private final Context context;
    private SQLiteDatabase db;

    public EmailDBAdapterImpl(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(this.context);
    }

    public EmailDBAdapterImpl open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.DBHelper.close();
    }

    public boolean delete(long rowId) {
        return this.db.delete("email", new StringBuilder().append("_id=").append(rowId).toString(), null) > 0;
    }

    public Cursor get(long rowId) throws SQLException {
        Cursor mCursor = this.db.query(true, "email", ALL_COLUMNS, "_id=" + rowId, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor getAll() {
        return this.db.query("email", ALL_COLUMNS, null, null, null, null, null);
    }

    public long insert(IrtTasksBo businessObject) {
        return this.db.insert("email", null, populateBusinessObjectContentValues((Email) businessObject));
    }

    public boolean update(IrtTasksBo businessObject) {
        Email email = (Email) businessObject;
        return this.db.update("email", populateBusinessObjectContentValues(email), new StringBuilder().append("_id=").append(email.getId()).toString(), null) > 0;
    }

    private ContentValues populateBusinessObjectContentValues(Email email) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", email.getName());
        contentValues.put("email", email.getEmail());
        contentValues.put("password", encryptPassword(email.getPassword()));
        contentValues.put("extra_emails", email.getExtraEmail());
        contentValues.put(SIGNATURE, email.getSignature());
        return contentValues;
    }

    private String encryptPassword(String clearPassword) {
        if (StringUtil.isNullOrEmpty(clearPassword)) {
            return null;
        }
        try {
            return AndroidCrypto.encrypt(SecurityKey.ENCRYPTION_MASTER_KEY, clearPassword);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
