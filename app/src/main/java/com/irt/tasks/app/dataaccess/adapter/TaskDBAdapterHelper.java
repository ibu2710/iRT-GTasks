package com.irt.tasks.app.dataaccess.adapter;

public class TaskDBAdapterHelper {
    public static String getVisibleTasksList(boolean isShowInvisible) {
        String invisibleFilter = getListRawColumn("visible") + " = 1 AND " + getAccountRawColumn("enable") + " = 1 " + " AND ";
        if (isShowInvisible) {
            invisibleFilter = "";
        }
        return "SELECT " + getListRawColumn("_id") + " from " + TasksListDBAdapterImpl.DATABASE_TABLE + " JOIN " + "account" + " ON " + getListRawColumn(TasksListDBAdapterImpl.ACCOUNT_ID) + " = " + getAccountRawColumn("_id") + " WHERE " + invisibleFilter + getListRawColumn("inTrashBin") + " = 0 " + "AND " + getListRawColumn("deleted") + " = 0 ";
    }

    public static String getListRawColumn(String column) {
        return DatabaseUtil.getRawColumn(TasksListDBAdapterImpl.DATABASE_TABLE, column);
    }

    private static String getAccountRawColumn(String column) {
        return DatabaseUtil.getRawColumn("account", column);
    }

    public static String getContextBaseRawColumn(String column) {
        return DatabaseUtil.getRawColumn(ContextBaseDBAdapterImpl.DATABASE_TABLE, column);
    }

    public static String getContextTaskRawColumn(String column) {
        return DatabaseUtil.getRawColumn(ContextTaskDBAdapterImpl.DATABASE_TABLE, column);
    }

    public static String getContextIncludesRawColumn(String column) {
        return DatabaseUtil.getRawColumn(ContextIncludesDBAdapterImpl.DATABASE_TABLE, column);
    }

    public static String getTaskRawColumn(String column) {
        return DatabaseUtil.getRawColumn("task", column);
    }
}
