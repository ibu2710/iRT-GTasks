package com.irt.tasks.app.dataaccess.adapter;

import android.database.Cursor;

public interface TagTaskDBAdapter extends DBAdapter {
    int deleteAllWhereTaskDoesNotExist();

    int deleteAllbyTagId(Long l);

    int deleteAllbyTaskId(Long l);

    Cursor getAllByTagId(Long l);

    Cursor getAllByTaskId(Long l);

    Cursor getAllDistinctTagIds();
}
