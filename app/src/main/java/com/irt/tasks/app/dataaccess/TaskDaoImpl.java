package com.irt.tasks.app.dataaccess;

import android.database.Cursor;
import android.database.SQLException;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.adapter.TaskDBAdapter;
import com.irt.tasks.app.dataaccess.adapter.TaskDBAdapterImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.receiver.AlarmSetterHelper;
import java.util.ArrayList;
import java.util.List;

public class TaskDaoImpl implements TaskDao {
    ConfigManager config;

    public List<Task> getQuickAllVisibleTasks(String query, String orderBy) {
        TaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getQuickAllVisibleTasks(query, orderBy);
        List<Task> tasks = buildQuickTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasks;
    }

    public List<Task> getFilteredVisibleTasks(Long tasksListId) {
        TaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getFilteredVisibleTasks(tasksListId);
        List<Task> tasks = buildFilteredTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasks;
    }

    public void forceDeleteByTaskListId(Long taskListId) {
        TaskDBAdapter dBAdapter = openDbAdapter();
        dBAdapter.forceDeleteByTaskListId(taskListId);
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    public void forceDelete(Long taskId) {
        TaskDBAdapter dBAdapter = openDbAdapter();
        dBAdapter.delete(taskId.longValue());
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    public void delete(Task task) {
        if (StringUtil.isNullOrEmpty(task.getGtaskId())) {
            if (this.config.isDebug()) {
                System.out.println("sending to trash bin " + task.getName());
            }
            task.setInTrashBin(Boolean.valueOf(true));
        }
        if (this.config.isDebug()) {
            System.out.println("marking for delete " + task.getName());
        }
        task.setDeleted(Boolean.valueOf(true));
        task.setReminderEnabled(Boolean.valueOf(false));
        if (!(task.getSnoozeDateTime().equals(Long.valueOf(0)) && task.getReminderDateTime().equals(Long.valueOf(0)))) {
            ConfigManager config = ConfigManager.getInstance();
            AppUtil.clearNotification(config.getContext(), task);
            task.setReminderDateTime(Long.valueOf(0));
            task.setSnoozeDateTime(Long.valueOf(0));
            AlarmSetterHelper.scheduleAlarm(config.getContext(), task, false);
        }
        saveOrUpdateWithTimeStamp(task);
        AppUtil.setLastApplicationModifiedDate();
    }

    public Task get(Long taskId) throws SQLException {
        TaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.get(taskId.longValue());
        Task task = null;
        if (cursor != null) {
            task = buildTask(cursor);
        }
        cursor.close();
        closeDbAdapter(dBAdapter);
        return task;
    }

    public List<Task> getAll() {
        TaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAll();
        List<Task> tasks = buildTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasks;
    }

    public void saveOrUpdateWithTimeStamp(Task task) {
        task.setLastModified(Long.valueOf(AppUtil.getTodayDateTimeMilisecond()));
        saveOrUpdate(task);
        AppUtil.setLastApplicationModifiedDate();
    }

    public void saveOrUpdate(Task task) {
        saveOrUpdate(task, true);
    }

    public void saveOrUpdateNoUpdateToLastApplicationModifiedDate(Task task) {
        saveOrUpdate(task, false);
    }

    public void saveOrUpdate(Task task, boolean isUpdateLastApplicationModifiedDate) {
        this.config = ConfigManager.getInstance();
        TaskDBAdapter dBAdapter = openDbAdapter();
        if (task.getId() == null) {
            if (task.getLastModified().equals(Long.valueOf(0))) {
                task.setCreationDate(Long.valueOf(AppUtil.getTodayDateTimeMilisecond()));
            } else {
                task.setCreationDate(task.getLastModified());
            }
            task.setLastContentModifiedDate(task.getCreationDate());
            task.setId(Long.valueOf(dBAdapter.insert(task)));
            if (this.config.isDebug()) {
                System.out.println("Inserted Local DB [" + task.getName() + "]/id " + task.getId() + Globals.FORWARDSLASH + task.getParentTaskId() + Globals.FORWARDSLASH + task.getPriorSiblingTaskId() + Globals.FORWARDSLASH + task.getTasksListId());
            }
        } else {
            if (this.config.isDebug()) {
                System.out.println("Updating Local DB [" + task.getName() + "]/id " + task.getId() + Globals.FORWARDSLASH + task.getParentTaskId() + Globals.FORWARDSLASH + task.getPriorSiblingTaskId());
            }
            dBAdapter.update(task);
        }
        closeDbAdapter(dBAdapter);
        if (isUpdateLastApplicationModifiedDate) {
            AppUtil.setLastApplicationModifiedDate();
        }
    }

    public List<Task> getAllTasksByTasksListId(Long tasksListId, String orderBy, boolean isCompletedOnly, boolean isDueDateOnly, boolean isSnoozeOnly) {
        TaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllTasksByTasksListId(tasksListId, orderBy, isCompletedOnly, isDueDateOnly, isSnoozeOnly);
        List<Task> tasks = buildTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasks;
    }

    public List<Task> getAllVisibleTasksDeletedOnly() {
        return getAllVisibleTasksDeletedOrTashBinOnly(true, false);
    }

    public List<Task> getAllVisibleTasksTrashBinOnly() {
        return getAllVisibleTasksDeletedOrTashBinOnly(false, true);
    }

    private List<Task> getAllVisibleTasksDeletedOrTashBinOnly(boolean isDeletedOnly, boolean isTrashBinOnly) {
        this.config = ConfigManager.getInstance();
        TaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllVisibleTasksDeletedOrTashBinOnly(isDeletedOnly, isTrashBinOnly, this.config.isShowInvisibleAccounts().booleanValue());
        List<Task> tasks = buildTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasks;
    }

    public List<Task> getAllTasksDeletedOnlyByTasksListId(Long tasksListId) {
        return getAllTasksDeletedOrTrashBinByTasksListId(tasksListId, true, false);
    }

    public List<Task> getAllTasksTrashBinOnlyByTasksListId(Long tasksListId) {
        return getAllTasksDeletedOrTrashBinByTasksListId(tasksListId, false, true);
    }

    private List<Task> getAllTasksDeletedOrTrashBinByTasksListId(Long tasksListId, boolean isDeletedOnly, boolean isTrashBinOnly) {
        TaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllTasksDeletedOrTrashBinByTasksListId(tasksListId, isDeletedOnly, isTrashBinOnly);
        List<Task> tasks = buildTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasks;
    }

    public List<Task> getAllTasksByTasksListIdWithDeleted(Long tasksListId) {
        TaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllTasksByTasksListIdWithDeleted(tasksListId);
        List<Task> tasks = buildTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasks;
    }

    public List<Task> getAllTasksForGoogleSync(TasksList tasksList) {
        List<Task> tasks = AppUtil.getMaxSortedTasksForListIncludeCompleted(tasksList, getAllTasksByTasksListIdWithDeleted(tasksList.getId()));
        populateParentAndSiblingGIds(tasks, tasksList.getGtasksListId());
        return tasks;
    }

    public int deleteTasksInTrashBin() {
        TaskDBAdapter dBAdapter = openDbAdapter();
        int totalRecordsDeleted = dBAdapter.deleteTasksInTrashBin();
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
        return totalRecordsDeleted;
    }

    public boolean resetTasksProject(Long projectId) {
        TaskDBAdapter dBAdapter = openDbAdapter();
        boolean success = dBAdapter.resetTasksProject(projectId);
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
        return success;
    }

    public List<Task> searchAllExcludeDeleteAndTrashBin(String queryString) {
        TaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.searchAllExcludeDeleteAndTrashBin(queryString);
        List<Task> tasks = buildTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasks;
    }

    public int countTasksByTasksListId(Long tasksListId, boolean isCompleted) {
        TaskDBAdapter dBAdapter = openDbAdapter();
        int totalTasks = dBAdapter.countTasksByTasksListId(tasksListId, isCompleted);
        closeDbAdapter(dBAdapter);
        return totalTasks;
    }

    public List<Task> getAllVisibleTasks(String orderBy, boolean isCompleted, boolean isDueDateOnly, boolean isSnoozeOnly) {
        this.config = ConfigManager.getInstance();
        TaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllVisibleTasks(orderBy, isCompleted, this.config.isShowInvisibleAccounts().booleanValue(), isDueDateOnly, isSnoozeOnly);
        List<Task> tasks = buildTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasks;
    }

    public List<Task> getAllTasksWithEligibleAlarms() {
        TaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllTasksWithEligibleAlarms();
        List<Task> tasks = buildTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasks;
    }

    public List<Task> getAllTasksWithEligibleAlarmsIncludingInvisibleAccounts() {
        TaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllTasksWithEligibleAlarmsIncludingInvisibleAccounts();
        List<Task> tasks = buildTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasks;
    }

    public List<Task> getTasksMarkedForDeletionButNotInTrashBin(Long tasksListId) {
        TaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getTasksMarkedForDeletionButNotInTrashBin(tasksListId);
        List<Task> tasks = buildTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasks;
    }

    private void populateParentAndSiblingGIds(List<Task> tasks, String gtasksListId) {
        for (Task task : tasks) {
            task.setGtasksListId(gtasksListId);
            task.setParentGtaskId(getGtaskId(task.getParentTaskId(), tasks));
            task.setPriorSiblingGtaskId(getGtaskId(task.getPriorSiblingTaskId(), tasks));
        }
    }

    private String getGtaskId(Long taskId, List<Task> tasks) {
        for (Task task : tasks) {
            if (task.getId().equals(taskId)) {
                return task.getGtaskId();
            }
        }
        return null;
    }

    private List<Task> buildTasks(Cursor cursor) {
        List<Task> tasks = new ArrayList();
        while (cursor.moveToNext()) {
            tasks.add(buildTask(cursor));
        }
        return tasks;
    }

    private Task buildTask(Cursor cursor) {
        return performBuildTask(cursor);
    }

    private List<Task> buildFilteredTasks(Cursor cursor) {
        List<Task> tasks = new ArrayList();
        while (cursor.moveToNext()) {
            tasks.add(buildFilteredTask(cursor));
        }
        return tasks;
    }

    private Task buildFilteredTask(Cursor cursor) {
        Task task = performBuildTask(cursor);
        String tasksListName = cursor.getString(39);
        String contextName = cursor.getString(40);
        task.setTasksListName(tasksListName);
        task.setContextName(contextName);
        return task;
    }

    private List<Task> buildQuickTasks(Cursor cursor) {
        List<Task> tasks = new ArrayList();
        while (cursor.moveToNext()) {
            tasks.add(buildQuickTask(cursor));
        }
        return tasks;
    }

    private Task buildQuickTask(Cursor cursor) {
        Task task = performBuildTask(cursor);
        task.setTasksListName(cursor.getString(39));
        return task;
    }

    private Task performBuildTask(Cursor cursor) {
        Long id = Long.valueOf(cursor.getLong(0));
        String gtaskId = cursor.getString(1);
        Long tasksListId = Long.valueOf(cursor.getLong(2));
        Long parentTaskId = Long.valueOf(cursor.getLong(3));
        Long priorSiblingTaskId = Long.valueOf(cursor.getLong(4));
        String name = cursor.getString(5);
        String note = cursor.getString(6);
        Long dueDate = Long.valueOf(cursor.getLong(7));
        Long lastModified = Long.valueOf(cursor.getLong(8));
        Boolean deleted = AppUtil.isNumberOne(cursor.getInt(9));
        Boolean completed = AppUtil.isNumberOne(cursor.getInt(10));
        Boolean inTrashBin = AppUtil.isNumberOne(cursor.getInt(11));
        Boolean moved = AppUtil.isNumberOne(cursor.getInt(12));
        Boolean highlight = AppUtil.isNumberOne(cursor.getInt(13));
        Integer fontStyle = Integer.valueOf(cursor.getInt(14));
        Integer fontColor = Integer.valueOf(cursor.getInt(15));
        Integer fontBgColor = Integer.valueOf(cursor.getInt(16));
        Long syncTime = Long.valueOf(cursor.getLong(17));
        Boolean showNote = AppUtil.isNumberOne(cursor.getInt(18));
        Long creationDate = Long.valueOf(cursor.getLong(19));
        Long lastContentModifiedDate = Long.valueOf(cursor.getLong(20));
        Integer priority = Integer.valueOf(cursor.getInt(21));
        Boolean reminderEnabled = AppUtil.isNumberOne(cursor.getInt(22));
        Integer reminderDaysEarlier = Integer.valueOf(cursor.getInt(23));
        Long reminderDateTime = Long.valueOf(cursor.getLong(24));
        Boolean reminderRepeatFromCompletionDate = AppUtil.isNumberOne(cursor.getInt(25));
        String reminderRepeatRule = cursor.getString(26);
        Boolean notifyByEmail = AppUtil.isNumberOne(cursor.getInt(27));
        Boolean notifyExtraEmails = AppUtil.isNumberOne(cursor.getInt(28));
        String extraEmails = cursor.getString(29);
        Boolean isfloat = AppUtil.isNumberOne(cursor.getInt(30));
        Boolean excludeFromDelete = AppUtil.isNumberOne(cursor.getInt(31));
        String iconName = cursor.getString(32);
        Long snoozeDateTime = Long.valueOf(cursor.getLong(33));
        Integer autoSnoozeDuration = Integer.valueOf(cursor.getInt(34));
        Long completionDate = Long.valueOf(cursor.getLong(35));
        Boolean starred = AppUtil.isNumberOne(cursor.getInt(36));
        Long projectId = Long.valueOf(cursor.getLong(37));
        Integer status = Integer.valueOf(cursor.getInt(38));
        Task task = new Task();
        task.setId(id);
        task.setGtaskId(gtaskId);
        task.setTasksListId(tasksListId);
        task.setParentTaskId(parentTaskId);
        task.setPriorSiblingTaskId(priorSiblingTaskId);
        task.setName(name);
        task.setNote(note);
        task.setDueDate(dueDate);
        task.setLastModified(lastModified);
        task.setDeleted(deleted);
        task.setCompleted(completed);
        task.setInTrashBin(inTrashBin);
        task.setMoved(moved);
        task.setHighlight(highlight);
        task.setFontStyle(fontStyle);
        task.setFontColor(fontColor);
        task.setFontBgColor(fontBgColor);
        task.setSyncTime(syncTime);
        task.setShowNote(showNote);
        task.setCreationDate(creationDate);
        task.setLastContentModifiedDate(lastContentModifiedDate);
        task.setPriority(priority);
        task.setReminderEnabled(reminderEnabled);
        task.setReminderDaysEarlier(reminderDaysEarlier);
        task.setReminderDateTime(reminderDateTime);
        task.setReminderRepeatFromCompletionDate(reminderRepeatFromCompletionDate);
        task.setReminderRepeatRule(reminderRepeatRule);
        task.setNotifyByEmail(notifyByEmail);
        task.setNotifyExtraEmails(notifyExtraEmails);
        task.setExtraEmails(extraEmails);
        task.setIsFloat(isfloat);
        task.setExcludeFromDelete(excludeFromDelete);
        task.setIconName(iconName);
        task.setSnoozeDateTime(snoozeDateTime);
        task.setAutoSnoozeDuration(autoSnoozeDuration);
        task.setCompletionDate(completionDate);
        task.setStarred(starred);
        task.setProjectId(projectId);
        task.setStatus(status);
        return task;
    }

    private TaskDBAdapter openDbAdapter() {
        this.config = ConfigManager.getInstance();
        TaskDBAdapter dBAdapter = new TaskDBAdapterImpl(this.config.getContext());
        dBAdapter.open();
        return dBAdapter;
    }

    private void closeDbAdapter(TaskDBAdapter dBAdapter) {
        dBAdapter.close();
    }
}
