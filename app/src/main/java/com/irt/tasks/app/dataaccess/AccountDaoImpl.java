package com.irt.tasks.app.dataaccess;

import android.database.Cursor;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.adapter.AccountDBAdapter;
import com.irt.tasks.app.dataaccess.adapter.AccountDBAdapterImpl;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.app.framework.security.AndroidCrypto;
import com.irt.tasks.app.framework.security.SecurityKey;
import java.util.ArrayList;
import java.util.List;

public class AccountDaoImpl implements AccountDao {
    ConfigManager config;

    public void delete(Account account) {
        if (!DatabaseHelper.OFFLINE_ACCOUNT_ID.equals(account.getId())) {
            AccountDBAdapter dBAdapter = openDbAdapter();
            dBAdapter.delete(account.getId().longValue());
            closeDbAdapter(dBAdapter);
            AppUtil.setLastApplicationModifiedDate();
        }
    }

    public Account get(Long accountId) {
        Account account = null;
        AccountDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.get(accountId.longValue());
        if (cursor != null) {
            account = buildAccount(cursor);
        }
        cursor.close();
        closeDbAdapter(dBAdapter);
        return account;
    }

    public List<Account> getAll() {
        AccountDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAll();
        List<Account> accounts = buildAccounts(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return accounts;
    }

    public void saveOrUpdate(Account account) {
        AccountDBAdapter dBAdapter = openDbAdapter();
        if (account.getId() == null) {
            account.setNewGoogleId(Boolean.valueOf(true));
            account.setId(Long.valueOf(dBAdapter.insert(account)));
        } else {
            dBAdapter.update(account);
        }
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    public List<Account> getAllVisible() {
        this.config = ConfigManager.getInstance();
        AccountDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllVisible(this.config.isShowInvisibleAccounts().booleanValue());
        List<Account> accounts = buildAccounts(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return accounts;
    }

    public List<Account> getAllAutoSync() {
        AccountDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllAutoSync();
        List<Account> accounts = buildAccounts(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return accounts;
    }

    private List<Account> buildAccounts(Cursor cursor) {
        List<Account> accounts = new ArrayList();
        while (cursor.moveToNext()) {
            accounts.add(buildAccount(cursor));
        }
        return accounts;
    }

    private Account buildAccount(Cursor cursor) {
        Long id = Long.valueOf(cursor.getLong(0));
        String email = cursor.getString(1);
        String password = cursor.getString(2);
        String domain = cursor.getString(3);
        Boolean isAutoSync = AppUtil.isNumberOne(cursor.getInt(4));
        Boolean isEnable = AppUtil.isNumberOne(cursor.getInt(5));
        String note = cursor.getString(6);
        Integer sortPosition = Integer.valueOf(cursor.getInt(7));
        Integer fontStyle = Integer.valueOf(cursor.getInt(8));
        Integer fontColor = Integer.valueOf(cursor.getInt(9));
        Integer fontBgColor = Integer.valueOf(cursor.getInt(10));
        Boolean isNewGoogleId = AppUtil.isNumberOne(cursor.getInt(11));
        Account account = new Account();
        account.setId(id);
        account.setEmail(email);
        account.setDomain(domain);
        account.setAutoSync(isAutoSync);
        account.setEnable(isEnable);
        account.setNote(note);
        account.setSortPosition(sortPosition);
        account.setFontStyle(fontStyle);
        account.setFontColor(fontColor);
        account.setFontBgColor(fontBgColor);
        account.setNewGoogleId(isNewGoogleId);
        setAccountPassword(password, account);
        return account;
    }

    private void setAccountPassword(String password, Account account) {
        if (StringUtil.isNullOrEmpty(password)) {
            account.setPassword(null);
            return;
        }
        try {
            account.setPassword(AndroidCrypto.decrypt(SecurityKey.ENCRYPTION_MASTER_KEY, password, false));
        } catch (Exception e) {
            try {
                account.setPassword(AndroidCrypto.decrypt(SecurityKey.ENCRYPTION_MASTER_KEY, password, true));
                System.out.println("Saving account for Android compatibility [" + account.getEmail() + "]");
                saveOrUpdate(account);
            } catch (Exception e2) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }

    private AccountDBAdapter openDbAdapter() {
        this.config = ConfigManager.getInstance();
        AccountDBAdapter dBAdapter = new AccountDBAdapterImpl(this.config.getContext());
        dBAdapter.open();
        return dBAdapter;
    }

    private void closeDbAdapter(AccountDBAdapter dBAdapter) {
        dBAdapter.close();
    }
}
