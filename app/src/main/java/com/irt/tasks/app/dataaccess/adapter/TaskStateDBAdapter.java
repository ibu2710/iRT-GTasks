package com.irt.tasks.app.dataaccess.adapter;

import android.database.Cursor;

public interface TaskStateDBAdapter extends DBAdapter {
    int deleteAllByTasksListId(Long l);

    Cursor getAllByTasksListId(Long l);
}
