package com.irt.tasks.app.businessobjects;

import com.irt.tasks.app.framework.business.IrtTasksBo;

public class RemoteTask implements IrtTasksBo {
    private Integer fontBgColor = Integer.valueOf(0);
    private Integer fontColor = Integer.valueOf(0);
    private Integer fontStyle = Integer.valueOf(0);
    private Boolean highlight = Boolean.valueOf(false);
    private String taskId;

    public String getTaskId() {
        return this.taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public Boolean getHighlight() {
        return this.highlight;
    }

    public void setHighlight(Boolean highlight) {
        this.highlight = highlight;
    }

    public Integer getFontStyle() {
        return this.fontStyle;
    }

    public void setFontStyle(Integer fontStyle) {
        this.fontStyle = fontStyle;
    }

    public Integer getFontColor() {
        return this.fontColor;
    }

    public void setFontColor(Integer fontColor) {
        this.fontColor = fontColor;
    }

    public Integer getFontBgColor() {
        return this.fontBgColor;
    }

    public void setFontBgColor(Integer fontBgColor) {
        this.fontBgColor = fontBgColor;
    }
}
