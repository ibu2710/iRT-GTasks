package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.TasksList;

public interface SyncBp {
    boolean syncWithGoogleTasks();

    boolean syncWithGoogleTasks(TasksList tasksList);

    void updateTasksListNameAndLocalGtasksListName(TasksList tasksList, TasksList tasksList2);
}
