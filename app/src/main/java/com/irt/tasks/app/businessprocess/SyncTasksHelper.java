package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.SyncStatistics;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.GtaskDao;
import com.irt.tasks.app.dataaccess.GtaskDao.SubBatchType;
import com.irt.tasks.app.dataaccess.GtaskDao2Impl;
import com.irt.tasks.app.dataaccess.TaskDao;
import com.irt.tasks.app.dataaccess.TaskDaoImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SyncTasksHelper {
    private static final String GOOGLE_TASK_START_ID = null;
    private static final String GOOGLE_TASK_START_PARENT_ID = null;
    private static final String GOOGLE_TASK_START_PRIOR_SIBLING_ID = null;
    static final String[] INDENTS = new String[20];
    private static final String INDENtT_STR = "  ";
    static ConfigManager config;
    GtaskDao gtaskDao = new GtaskDao2Impl();
    int levelToDisplay = 20;
    private Long mSyncId = Long.valueOf(0);
    TaskBp taskBp = new TaskBpImpl();
    TaskDao taskDao = new TaskDaoImpl();

    public void initIndentions() {
        INDENTS[0] = "  ";
        for (int i = 1; i < 20; i++) {
            INDENTS[i] = INDENTS[i - 1] + "  ";
        }
    }

    private Long getSyncId() {
        Long l = this.mSyncId;
        this.mSyncId = Long.valueOf(this.mSyncId.longValue() + 1);
        return this.mSyncId;
    }

    private void resetSyncId() {
        this.mSyncId = Long.valueOf(0);
    }

    public static List<Task> syncTasksInList(TasksList tasksList, List<Task> webTasks, List<Task> localTasks, SyncStatistics syncStat) {
        config = ConfigManager.getInstance();
        SyncTasksHelper syncTasksHelper = new SyncTasksHelper();
        syncTasksHelper.initIndentions();
        return syncTasksHelper.syncTasks(tasksList, webTasks, localTasks, syncStat);
    }

    private List<Task> syncTasks(TasksList tasksList, List<Task> webTasks, List<Task> localTasks, SyncStatistics syncStat) {
        ConfigManager config = ConfigManager.getInstance();
        for (Task webTask : webTasks) {
            webTask.setTasksListId(tasksList.getId());
            updateWebIdWithTaskIds(localTasks, webTask);
        }
        if (config.isDebug()) {
            drawTree(webTasks, "WEB ORIG TASK LOOP with updated local Task IDs - " + tasksList.getName());
            drawTree(localTasks, "LOCAL ORIG TASK LOOP - " + tasksList.getName());
        }
        List<Task> syncTasks = new ArrayList();
        List<Task> list = webTasks;
        List<Task> list2 = localTasks;
        TasksList tasksList2 = tasksList;
        syncTasksRecursively(getStarterParentTask(), list, list2, tasksList2, syncTasks, new ArrayList(webTasks), new ArrayList(localTasks), syncStat);
        if (config.isDebug()) {
            drawTree(syncTasks, "AFTER THE VIRTUAL SYNC - " + tasksList.getName());
        }
        saveOrUpdateSyncedTasks(syncTasks, syncStat);
        if (config.isDebug()) {
            drawTree(syncTasks, "SYNC COMPLETED - " + tasksList.getName());
        }
        resetSyncId();
        return syncTasks;
    }

    private void syncTasksRecursively(Task parentTask, List<Task> webTasks, List<Task> localTasks, TasksList tasksList, List<Task> syncTasks, List<Task> webTasksReference, List<Task> localTasksReference, SyncStatistics syncStat) {
        boolean isContinueWithLoop = true;
        while (isContinueWithLoop) {
            List<Task> childrenTasks = getChildrenTasksForSync(parentTask, webTasks, localTasks, webTasksReference, localTasksReference);
            Task prevTask = null;
            for (int i = 0; i < childrenTasks.size(); i++) {
                Task task = (Task) childrenTasks.get(i);
                setSyncedIds(parentTask, prevTask, i, task);
                syncTasks.add(task);
                syncTasksRecursively(task, webTasks, localTasks, tasksList, syncTasks, webTasksReference, localTasksReference, syncStat);
                prevTask = task;
            }
            if (isMoreTasksToProcess(parentTask, webTasks, localTasks)) {
                parentTask = voteForParentToFixBrokenLink(webTasks, localTasks, webTasksReference, localTasksReference);
            } else {
                isContinueWithLoop = false;
            }
        }
    }

    private List<Task> getChildrenTasksForSync(Task parentTask, List<Task> webTasks, List<Task> localTasks, List<Task> webTasksReference, List<Task> localTasksReference) {
        if (config.isDebug()) {
            System.out.println("Getting children tasks for sync, The parent task = [" + parentTask.getId() + "]" + parentTask.getName());
        }
        List<Task> childrenTasksForSync = new ArrayList();
        List<Task> webTaskChildren = getTaskChildrenWhereParentIsWebOrLocal(parentTask, webTasks, false);
        List<Task> localTaskChildren = getTaskChildrenWhereParentIsWebOrLocal(parentTask, localTasks, true);
        webTasks.removeAll(webTaskChildren);
        List<Task> localTaskChildrenBuffer = new ArrayList(localTaskChildren);
        List<Task> localTasksBuffer = new ArrayList(localTaskChildren);
        for (Task webTask : webTaskChildren) {
            Task localTask = getLocalTaskByWebId(webTask, localTasksReference);
            if (localTask == null || localTask.isDeleted().booleanValue()) {
                if (config.isDebug()) {
                    System.out.println("New web task, local task need to be added [" + webTask.getName() + "]");
                }
                webTask.setTaskNeedUpdate(true);
                childrenTasksForSync.add(webTask);
            } else if (webTask.getLastModified().longValue() > localTask.getLastModified().longValue()) {
                if (config.isDebug()) {
                    System.out.println("Local update needed for [" + webTask.getName() + "]");
                }
                webTask.setDecoratorsForGoogleFromTask(localTask);
                webTask.setTaskNeedUpdate(true);
                childrenTasksForSync.add(webTask);
            } else if (webTask.getLastModified().longValue() >= localTask.getLastModified().longValue()) {
                if (config.isDebug()) {
                    System.out.println("@004 Web and Local task are the same [" + localTask.getName() + "] they would still be included in the children for sync, in case we want to save all to a resultant DB");
                }
                childrenTasksForSync.add(localTask);
            } else if (!isSkipTask(parentTask, localTask)) {
                if (config.isDebug()) {
                    System.out.println("Web update needed for [" + localTask.getName() + "]");
                }
                localTask.setGtaskNeedUpdate(true);
                childrenTasksForSync.add(localTask);
            } else if (config.isDebug()) {
                System.out.println("@003 Skipping local task [" + localTask.getName() + "] parent ID [" + localTask.getParentTaskId() + "] is not the same as what is currently being processed");
            }
            if (localTask != null) {
                localTaskChildren.remove(localTask);
            }
        }
        for (Task task : localTaskChildren) {
            if (!(task.isDeleted().booleanValue() || isExcludeTaskWithGtaskId(task) || isWebEquivalentTaskLater(task, webTasksReference) || isSkipTask(parentTask, task))) {
                int taskPosition = getTaskPosition(task, localTasksBuffer);
                task.setGtaskNeedUpdate(true);
                task.setMoved(Boolean.valueOf(true));
                if (taskPosition > childrenTasksForSync.size()) {
                    if (config.isDebug()) {
                        System.out.println("@005 New local task, web task need to be added [" + task.getName() + "] adding to end of childrenTasksForSync");
                    }
                    childrenTasksForSync.add(task);
                } else {
                    if (config.isDebug()) {
                        System.out.println("@006 New local task, web task need to be added [" + task.getName() + "] maintain it's correct position in childrenTasksForSync");
                    }
                    childrenTasksForSync.add(taskPosition, task);
                }
            }
        }
        localTasks.removeAll(localTaskChildrenBuffer);
        sortChildren(childrenTasksForSync);
        return childrenTasksForSync;
    }

    private void sortChildren(List<Task> childrenTasksForSync) {
        int taskSize = childrenTasksForSync.size();
        int i = 0;
        while (i < taskSize - 1) {
            if (!((Task) childrenTasksForSync.get(i)).isWebTask()) {
                int j = i + 1;
                while (j < taskSize) {
                    if (!((Task) childrenTasksForSync.get(j)).isWebTask() && ((Task) childrenTasksForSync.get(i)).getTaskNumber().intValue() > ((Task) childrenTasksForSync.get(j)).getTaskNumber().intValue()) {
                        Task taskBuff = (Task) childrenTasksForSync.get(i);
                        childrenTasksForSync.add(i, childrenTasksForSync.get(j));
                        childrenTasksForSync.remove(i + 1);
                        childrenTasksForSync.add(j, taskBuff);
                        childrenTasksForSync.remove(j + 1);
                    }
                    j++;
                }
            }
            i++;
        }
    }

    private boolean isSkipTask(Task parentTask, Task localTask) {
        if (config.isDebug()) {
            System.out.println("Test if we need to skip this task...");
        }
        return !localTask.getParentTaskId().equals(parentTask.getId());
    }

    private boolean isWebEquivalentTaskLater(Task task, List<Task> webTasks) {
        for (Task task2 : webTasks) {
            if (task.getId().equals(task2.getId())) {
                if (task2.getLastModified().longValue() > task.getLastModified().longValue()) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    private int getTaskPosition(Task task, List<Task> localTasksBuffer) {
        int index = 0;
        for (Task task2 : localTasksBuffer) {
            if (task2.getId().equals(task.getId())) {
                return index;
            }
            if (!task.isDeleted().booleanValue()) {
                index++;
            }
        }
        return 0;
    }

    private boolean isExcludeTaskWithGtaskId(Task task) {
        return task.getGtaskId() != null && task.getLastModified().equals(task.getSyncTime());
    }

    private List<Task> getTaskChildrenWhereParentIsWebOrLocal(Task parentTask, List<Task> tasks, boolean isTasksLocal) {
        List<Task> taskChildren = new ArrayList();
        if (isTasksLocal) {
            for (Task task : tasks) {
                if (task.getParentTaskId().equals(parentTask.getId())) {
                    taskChildren.add(task);
                }
            }
        } else if (parentTask.isWebTask()) {
            for (Task webTask : tasks) {
                if (StringUtil.isNullOrEmpty(webTask.getParentGtaskId()) && StringUtil.isNullOrEmpty(parentTask.getGtaskId())) {
                    taskChildren.add(webTask);
                } else if (!StringUtil.isNullOrEmpty(webTask.getParentGtaskId()) && webTask.getParentGtaskId().equals(parentTask.getGtaskId())) {
                    taskChildren.add(webTask);
                }
            }
        } else if (parentTask.isStarterParentTask()) {
            for (Task webTask2 : tasks) {
                if (StringUtil.isNullOrEmpty(webTask2.getParentGtaskId()) && parentTask.getId().equals(webTask2.getParentTaskId())) {
                    taskChildren.add(webTask2);
                }
            }
        } else {
            for (Task webTask22 : tasks) {
                if (!StringUtil.isNullOrEmpty(webTask22.getParentGtaskId()) && parentTask.getId().equals(webTask22.getParentTaskId())) {
                    taskChildren.add(webTask22);
                }
            }
        }
        return taskChildren;
    }

    private Task getStarterParentTask() {
        Task starterTask = new Task();
        starterTask.setStarterParentTask(true);
        starterTask.setId(Long.valueOf(0));
        starterTask.setParentTaskId(Long.valueOf(0));
        starterTask.setPriorSiblingTaskId(Constants.NO_SIBLING_ID);
        starterTask.setSyncId(Long.valueOf(0));
        starterTask.setSyncParentId(Long.valueOf(0));
        starterTask.setSyncPriorSiblingId(Constants.NO_SIBLING_ID);
        starterTask.setGtaskId(GOOGLE_TASK_START_ID);
        starterTask.setParentGtaskId(GOOGLE_TASK_START_PARENT_ID);
        starterTask.setPriorSiblingGtaskId(GOOGLE_TASK_START_PRIOR_SIBLING_ID);
        return starterTask;
    }

    private void updateWebIdWithTaskIds(List<Task> localTasksReference, Task webTask) {
        webTask.setId(getTaskIdByGtaskIdForWebFromLocalRef(webTask.getGtaskId(), localTasksReference));
        webTask.setParentTaskId(getTaskIdByGtaskIdForWebFromLocalRef(webTask.getParentGtaskId(), localTasksReference));
        webTask.setPriorSiblingTaskId(getTaskIdByGtaskIdForWebFromLocalRef(webTask.getPriorSiblingGtaskId(), localTasksReference));
    }

    private Long getTaskIdByGtaskIdForWebFromLocalRef(String gtaskId, List<Task> localTasks) {
        if (StringUtil.isNullOrEmpty(gtaskId)) {
            return null;
        }
        for (Task task : localTasks) {
            if (gtaskId.equals(task.getGtaskId())) {
                return task.getId();
            }
        }
        return null;
    }

    private void saveToDB(Task task) {
        this.taskDao.saveOrUpdate(task);
    }

    private Task getLocalTaskByWebId(Task webTask, List<Task> localTasks) {
        for (Task task : localTasks) {
            if (task.getId().equals(webTask.getId())) {
                return task;
            }
        }
        return null;
    }

    private Task getTaskByWebId(String gtaskId, List<Task> tasks) {
        for (Task task : tasks) {
            if (gtaskId.equals(task.getId())) {
                return task;
            }
        }
        return null;
    }

    private Task getTaskByTaskId(Long taskId, List<Task> tasks) {
        for (Task task : tasks) {
            if (taskId.equals(task.getId())) {
                return task;
            }
        }
        return null;
    }

    public List<Task> sortTasks(List<Task> tasks, Map<String, Long> expandedTasks, TasksList taskList, boolean isIncludeTaskPath, boolean isOneTasksList) {
        config = ConfigManager.getInstance();
        List<Task> sortedTasks = new ArrayList();
        List<Task> rootTasks = getRootTasks(tasks);
        if (rootTasks.size() > 0) {
            tasks.removeAll(rootTasks);
            int taskCounter = 1;
            int initialLevel = 2;
            if (isOneTasksList) {
                initialLevel = 1;
            }
            for (Task task : rootTasks) {
                if (sortRecursively(task, initialLevel, tasks, sortedTasks, expandedTasks, taskCounter, taskList, new Task(), isIncludeTaskPath)) {
                    taskCounter++;
                }
            }
        } else if (config.isDebug()) {
            System.out.println("Can't find root task");
        }
        if (!(config.isSyncing() || this.levelToDisplay < 20 || config.isHideCompletedTasks())) {
            for (Task task2 : tasks) {
                System.out.println("*** Restoring orphaned task [" + task2 + "]" + task2.getTasksListId());
                task2.setParentTaskId(Long.valueOf(0));
                task2.setPriorSiblingTaskId(Constants.NO_SIBLING_ID);
                this.taskBp.saveOrUpdate(task2, null);
            }
        }
        return sortedTasks;
    }

    private List<Task> getListTasks(List<Task> tasks) {
        List<Task> listTasks = new ArrayList();
        for (Task task : tasks) {
            if (task.isTasksListAsTask()) {
                listTasks.add(task);
            }
        }
        return listTasks;
    }

    private List<Task> getRootTasks(List<Task> tasks) {
        List<Task> rootTasks = new ArrayList();
        for (Task task : tasks) {
            if (task.getParentTaskId().equals(Long.valueOf(0))) {
                rootTasks.add(task);
            }
        }
        Task dummyTask = new Task();
        dummyTask.setId(Long.valueOf(0));
        return sortChildren(rootTasks, dummyTask);
    }

    private boolean sortRecursively(Task task, int level, List<Task> tasks, List<Task> sortedTasks, Map<String, Long> expandedTasks, int taskNumber, TasksList tasksList, Task parentTask, boolean isIncludeTaskPath) {
        setListNameAndTaskPath(task, tasksList, parentTask, isIncludeTaskPath);
        task.setLevel(Integer.valueOf(level));
        task.setTaskNumber(Integer.valueOf(taskNumber));
        if (AppUtil.isHide(task) || isTaskCollapsed(task, expandedTasks) || (level > this.levelToDisplay && !isTaskExpanded(task, expandedTasks))) {
            return false;
        }
        task.setIndention(INDENTS[level]);
        task.setTasksList(tasksList);
        sortedTasks.add(task);
        task.setParent(isTaskAParent(task, tasks));
        if (task.isParent() && level < 20) {
            List<Task> childredTasks = getChildrenTasks(task, tasks);
            tasks.removeAll(childredTasks);
            int taskCounter = 1;
            for (Task childTask : childredTasks) {
                if (sortRecursively(childTask, level + 1, tasks, sortedTasks, expandedTasks, taskCounter, tasksList, task, isIncludeTaskPath)) {
                    taskCounter++;
                }
            }
        }
        return true;
    }

    private void setListNameAndTaskPath(Task task, TasksList tasksList, Task parentTask, boolean isIncludeTaskPath) {
        if (isIncludeTaskPath) {
            if (tasksList != null) {
                task.setTasksListName(tasksList.getName());
            } else {
                task.setTasksListName("");
            }
            setTaskPath(task, parentTask);
        }
    }

    private void setTaskPath(Task task, Task parentTask) {
        String parentTaskName = parentTask.getName();
        String taskPath = parentTask.getTaskPath();
        if (StringUtil.isNullOrEmpty(parentTaskName)) {
            task.setTaskPath(">");
        } else if (parentTaskName.length() > 11) {
            task.setTaskPath(taskPath + parentTaskName.substring(0, 11) + "...>");
        } else {
            task.setTaskPath(taskPath + parentTaskName + ">");
        }
    }

    private boolean isTaskCollapsed(Task task, Map<String, Long> expandedTasks) {
        return isTaskExpandedOrCollapsed(task, expandedTasks, Constants.TASK_COLLAPSE_INDICATOR);
    }

    private boolean isTaskExpanded(Task task, Map<String, Long> expandedTasks) {
        return isTaskExpandedOrCollapsed(task, expandedTasks, Constants.TASK_EXPAND_INDICATOR);
    }

    private boolean isTaskExpandedOrCollapsed(Task task, Map<String, Long> expandedTasks, Long expandedOrCollapse) {
        if (expandedTasks == null) {
            return false;
        }
        boolean isTaskExpanded;
        if (task.getParentTaskId().equals(Long.valueOf(0))) {
            String taskListIdStr = Constants.TASK_LIST + task.getTasksListId();
            isTaskExpanded = expandedTasks.containsKey(taskListIdStr);
            if (isTaskExpanded) {
                return ((Long) expandedTasks.get(taskListIdStr)).equals(expandedOrCollapse);
            }
            return isTaskExpanded;
        }
        String parentTaskIdStr = task.getParentTaskId() + "";
        isTaskExpanded = expandedTasks.containsKey(parentTaskIdStr);
        if (isTaskExpanded) {
            return ((Long) expandedTasks.get(parentTaskIdStr)).equals(expandedOrCollapse);
        }
        return isTaskExpanded;
    }

    private boolean isTaskAParent(Task task, List<Task> tasks) {
        for (Task childTask : tasks) {
            if (childTask.getParentTaskId().equals(task.getId())) {
                return true;
            }
        }
        return false;
    }

    private List<Task> getChildrenTasks(Task task, List<Task> tasks) {
        List<Task> childrenTasks = new ArrayList();
        for (Task childTask : tasks) {
            if (childTask.getParentTaskId().equals(task.getId())) {
                childrenTasks.add(childTask);
            }
        }
        return sortChildren(childrenTasks, task);
    }

    private List<Task> sortChildren(List<Task> childrenTasks, Task parentTask) {
        List<Task> sortedChildrenTasks = new ArrayList();
        Task dummyTask = new Task();
        dummyTask.setId(Long.valueOf(0));
        int childrenTasksSize = childrenTasks.size();
        for (int i = 0; i < childrenTasksSize; i++) {
            Task sibling = getNextSibling(childrenTasks, dummyTask, parentTask);
            childrenTasks.remove(sibling);
            sortedChildrenTasks.add(sibling);
            dummyTask = sibling;
        }
        return sortedChildrenTasks;
    }

    private Task getNextSibling(List<Task> childrenTasks, Task priorSibling, Task parentTask) {
        for (Task task : childrenTasks) {
            if (task.getPriorSiblingTaskId().equals(priorSibling.getId())) {
                return task;
            }
        }
        Task fixedSiblingTask = (Task) childrenTasks.get(0);
        fixedSiblingTask.setPriorSiblingTaskId(priorSibling.getId());
        fixedSiblingTask.setParentTaskId(parentTask.getId());
        System.out.println("****==========" + fixedSiblingTask.getName() + "[" + parentTask.getId() + Globals.FORWARDSLASH + priorSibling.getId() + "]==================== Fixing Next Sibling ***");
        if (!config.isSyncing()) {
            saveToDB(fixedSiblingTask);
        }
        return fixedSiblingTask;
    }

    public void drawTree(List<Task> localTasks, String label) {
        config = ConfigManager.getInstance();
        if (config.isDebug()) {
            performDrawTree(localTasks, label);
            drawTasksThatNeedUpdate(localTasks);
        }
    }

    private void drawTasksThatNeedUpdate(List<Task> localTasks) {
        System.out.println("----- Start to list task/s that need local DB or Web update ------");
        for (Task task : localTasks) {
            if (task.isTaskNeedUpdate()) {
                System.out.println("[" + task.getLastModified() + "]" + task.getName() + " -- local DB needs update");
            }
            if (task.isGtaskNeedUpdate()) {
                System.out.println("[" + task.getLastModified() + "]" + task.getName() + " -- Web needs update");
            }
        }
        System.out.println("----- End list of task/s that need local DB or Web update ------");
    }

    private void performDrawTree(List<Task> localTasks, String label) {
        System.out.println("******************* START of " + label + " Tree ********************");
        for (Task task : localTasks) {
            String str;
            PrintStream printStream = System.out;
            StringBuilder append = new StringBuilder().append(INDENTS[task.getLevel().intValue()]);
            if (task.isDeleted().booleanValue()) {
                str = "-- delete -- " + getTruncatedTaskName(task.getName());
            } else {
                str = getTruncatedTaskName(task.getName());
            }
            printStream.println(append.append(str).append("[").append(task.getSyncId()).append("] ").append("[").append(task.getSyncParentId()).append("] ").append("[").append(task.getSyncPriorSiblingId()).append("] - ").append("[").append(task.getId()).append("] ").append("[").append(task.getParentTaskId()).append(Globals.COMMA).append(task.getPriorSiblingTaskId()).append("] - ").append(task.getGtaskId()).append(" [").append(task.getParentGtaskId()).append(Globals.COMMA).append(task.getPriorSiblingGtaskId()).append("] ").append(task.isTaskNeedUpdate() ? " -- local DB needs update" : " ").append(task.isGtaskNeedUpdate() ? " -- Web needs update" : "").append(task.isDeleted().booleanValue() ? " -- is Deleted" : "").append(task.isInTrashBin().booleanValue() ? " -- In TRASH Bin" : "").append(task.getName()).toString());
        }
        System.out.println("******************* END of " + label + " Tree ********************");
    }

    private String getTruncatedTaskName(String taskName) {
        if (StringUtil.isNullOrEmpty(taskName) || taskName.length() <= 3) {
            return taskName;
        }
        return taskName.substring(0, 2);
    }

    public int getLevelToDisplay() {
        return this.levelToDisplay;
    }

    public void setLevelToDisplay(int levelToDisplay) {
        this.levelToDisplay = levelToDisplay;
    }

    private void saveOrUpdateSyncedTasks(List<Task> syncTasks, SyncStatistics syncStat) {
        performEfficientSaveOrUpdateSyncedTasks(syncTasks, syncStat);
    }

    private void performEfficientSaveOrUpdateSyncedTasks(List<Task> syncTasks, SyncStatistics syncStat) {
        for (Task task : syncTasks) {
            if (task.isTaskNeedUpdate()) {
                boolean isNewTask = task.getId() == null;
                saveToDB(task);
                if (isNewTask) {
                    syncStat.incLocalInsertsTotal();
                    if (config.isDebug()) {
                        System.out.println("We have a new task for DB [" + task + "]");
                    }
                    updateIds(task, syncTasks);
                } else {
                    syncStat.incLocalUpdatesTotal();
                }
            }
        }
        List<Task> newWebTasks = new ArrayList();
        for (Task task2 : syncTasks) {
            if (task2.isGtaskNeedUpdate() && StringUtil.isNullOrEmpty(task2.getGtaskId())) {
                syncStat.incWebInsertsTotal();
                task2.setMoved(Boolean.valueOf(false));
                newWebTasks.add(task2);
            }
        }
        this.gtaskDao.batchUpdateTasks(newWebTasks, SubBatchType.INSERT);
        for (Task task22 : newWebTasks) {
            task22.setGtaskNeedUpdate(true);
            task22.setMoved(Boolean.valueOf(true));
            updateGIds(task22, syncTasks);
        }
        List<Task> webTasks = new ArrayList();
        for (Task task222 : syncTasks) {
            if (task222.isGtaskNeedUpdate()) {
                webTasks.add(task222);
                syncStat.incWebUpdatesTotal();
            }
        }
        syncStat.decreaseWebUpdatesWithDoublecountInserts(newWebTasks.size());
        this.gtaskDao.batchUpdateTasks(webTasks, SubBatchType.UPDATE);
        if (webTasks.size() > 0) {
            List<Task> latestWebTasks = this.gtaskDao.getAllTasksByTasksListId(((Task) webTasks.get(0)).getGtasksListId());
            for (Task task2222 : webTasks) {
                setWebTaskLastModifiedDate(task2222, latestWebTasks);
                Task localTask = this.taskDao.get(task2222.getId());
                localTask.setLastModified(task2222.getLastModified());
                saveToDB(localTask);
            }
        }
    }

    private void setWebTaskLastModifiedDate(Task task, List<Task> latestWebTasks) {
        for (Task taskBuf : latestWebTasks) {
            if (task.getGtaskId().equals(taskBuf.getGtaskId())) {
                if (config.isDebug()) {
                    System.out.println("Updating last modified date of webTask for local DB update [" + task.getId() + "][" + taskBuf.getName() + "][" + taskBuf.getLastModified() + "]");
                }
                task.setLastModified(taskBuf.getLastModified());
            }
        }
    }

    private void updateIds(Task task, List<Task> syncTasks) {
        for (Task task2 : syncTasks) {
            if (task.getSyncId().equals(task2.getSyncParentId())) {
                if (config.isDebug()) {
                    System.out.println("We update parent id on DB  [" + task2 + ']');
                }
                task2.setParentTaskId(task.getId());
                task2.setTaskNeedUpdate(true);
            }
            if (task.getSyncId().equals(task2.getSyncPriorSiblingId())) {
                if (config.isDebug()) {
                    System.out.println("We update prior sibling on DB [" + task2 + ']');
                }
                task2.setPriorSiblingTaskId(task.getId());
                task2.setTaskNeedUpdate(true);
            }
        }
    }

    private void updateGIds(Task task, List<Task> syncTasks) {
        for (Task task2 : syncTasks) {
            if (task.getSyncId().equals(task2.getSyncParentId())) {
                task2.setParentGtaskId(task.getGtaskId());
                task2.setMoved(Boolean.valueOf(true));
                task2.setGtaskNeedUpdate(true);
            }
            if (task.getSyncId().equals(task2.getSyncPriorSiblingId())) {
                task2.setPriorSiblingGtaskId(task.getGtaskId());
                task2.setMoved(Boolean.valueOf(true));
                task2.setGtaskNeedUpdate(true);
            }
        }
    }

    private void setSyncedIds(Task parentTask, Task prevTask, int i, Task task) {
        task.setSyncId(getSyncId());
        task.setSyncParentId(parentTask.getSyncId());
        setSyncedParentIds(parentTask, task);
        if (i == 0) {
            task.setSyncPriorSiblingId(Constants.NO_SIBLING_ID);
            setSyncedPriorSiblingIdForNoSibling(task);
            return;
        }
        task.setSyncPriorSiblingId(prevTask.getSyncId());
        setSyncedPriorSiblingIds(prevTask, task);
    }

    private void setSyncedPriorSiblingIdForNoSibling(Task task) {
        if (!task.getPriorSiblingTaskId().equals(Constants.NO_SIBLING_ID)) {
            task.setTaskNeedUpdate(true);
            if (config.isDebug()) {
                System.out.println("@007 Setting Local Prior Sibling Id to No Sibling [" + task.getName() + "] Local task need update.");
            }
        }
        task.setPriorSiblingTaskId(Constants.NO_SIBLING_ID);
        if (!StringUtil.isNullOrEmpty(task.getPriorSiblingGtaskId())) {
            setGtaskNeedsUpdateAndMoved(task);
            if (config.isDebug()) {
                System.out.println("@008 Setting Web Prior Sibling Id to No Sibling [" + task.getName() + "] Web task needs update and moved.");
            }
        }
        task.setPriorSiblingGtaskId(null);
    }

    private void setSyncedPriorSiblingIds(Task prevTask, Task task) {
        if (task.getPriorSiblingGtaskId() == null && prevTask.getGtaskId() != null) {
            setGtaskNeedsUpdateAndMoved(task);
            if (config.isDebug()) {
                System.out.println("@009 Setting  Web Prior Sibling Id [" + task.getName() + "] Web task needs update and moved.");
            }
        } else if (!(task.getPriorSiblingGtaskId() == null || task.getPriorSiblingGtaskId().equals(prevTask.getGtaskId()))) {
            if (config.isDebug()) {
                System.out.println("@010 Setting  Web Prior Sibling Id [" + task.getName() + "] Web task needs update and moved.");
            }
            setGtaskNeedsUpdateAndMoved(task);
        }
        task.setPriorSiblingGtaskId(prevTask.getGtaskId());
        if (task.getPriorSiblingTaskId().equals(Long.valueOf(0)) && prevTask.getId() != null) {
            if (config.isDebug()) {
                System.out.println("@011 Setting Local Prior Sibling Id [" + task.getName() + "] local task needs update.");
            }
            task.setTaskNeedUpdate(true);
        } else if (!(task.getPriorSiblingTaskId().equals(Long.valueOf(0)) || task.getPriorSiblingTaskId().equals(prevTask.getId()))) {
            if (config.isDebug()) {
                System.out.println("@012 Setting Local Prior Sibling Id [" + task.getName() + "] local task needs update.");
            }
            task.setTaskNeedUpdate(true);
        }
        task.setPriorSiblingTaskId(prevTask.getId());
    }

    private void setSyncedParentIds(Task parentTask, Task task) {
        if (task.getParentGtaskId() == null && parentTask.getGtaskId() != null) {
            if (config.isDebug()) {
                System.out.println("@061 Setting Web task Parent GTasks Id [" + task.getName() + "] Web task needs update and moved.");
            }
            setGtaskNeedsUpdateAndMoved(task);
        } else if (!(task.getParentGtaskId() == null || task.getParentGtaskId().equals(parentTask.getGtaskId()))) {
            if (config.isDebug()) {
                System.out.println("@062 Setting Web task Parent GTasks Id [" + task.getName() + "] Web task needs update and moved.");
            }
            setGtaskNeedsUpdateAndMoved(task);
        }
        task.setParentGtaskId(parentTask.getGtaskId());
        if (!task.getParentTaskId().equals(parentTask.getId())) {
            if (config.isDebug()) {
                System.out.println("@063 Setting local task Parent Tasks Id [" + task.getName() + "] local task needs update.");
            }
            task.setTaskNeedUpdate(true);
        } else if (!(task.getParentTaskId().equals(Long.valueOf(0)) || task.getParentTaskId().equals(parentTask.getId()))) {
            if (config.isDebug()) {
                System.out.println("@064 Setting local task Parent Tasks Id [" + task.getName() + "] local task needs update.");
            }
            task.setTaskNeedUpdate(true);
        }
        task.setParentTaskId(parentTask.getId());
    }

    private boolean isMoreTasksToProcess(Task parentTask, List<Task> webTasks, List<Task> localTasks) {
        return (webTasks.size() > 0 || localTasks.size() > 0) && parentTask.isStarterParentTask();
    }

    private Task voteForParentToFixBrokenLink(List<Task> webTasks, List<Task> localTasks, List<Task> webTasksReference, List<Task> localTasksReference) {
        Task votedParent;
        boolean isVotedChildFromWeb = false;
        Task webNextVotedChild = null;
        if (webTasks.size() > 0) {
            webNextVotedChild = (Task) webTasks.get(0);
        }
        Task localNextVotedChild = null;
        if (localTasks.size() > 0) {
            localNextVotedChild = (Task) localTasks.get(0);
        }
        if (webNextVotedChild == null) {
            isVotedChildFromWeb = false;
            votedParent = getTaskByTaskId(localNextVotedChild.getParentTaskId(), localTasksReference);
        } else if (localNextVotedChild == null) {
            isVotedChildFromWeb = true;
            votedParent = getTaskByWebId(webNextVotedChild.getParentGtaskId(), webTasksReference);
        } else if (webNextVotedChild.getLastModified().longValue() > localNextVotedChild.getLastModified().longValue()) {
            votedParent = getTaskByWebId(webNextVotedChild.getParentGtaskId(), webTasksReference);
        } else {
            isVotedChildFromWeb = false;
            votedParent = getTaskByTaskId(localNextVotedChild.getParentTaskId(), localTasksReference);
        }
        System.out.println("***** Fixing broken link injecting Parent [" + votedParent.getName() + "] from " + (isVotedChildFromWeb ? "web" : "local"));
        return votedParent;
    }

    private void setGtaskNeedsUpdateAndMoved(Task task) {
        task.setGtaskNeedUpdate(true);
        task.setMoved(Boolean.valueOf(true));
    }
}
