package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.ContextTask;
import java.util.List;

public interface ContextTaskDao {
    int deleteAllByContextBaseId(Long l);

    int deleteAllByTaskId(Long l);

    int deleteAllWhereTaskDoesNotExist();

    List<ContextTask> getAllByContextBaseId(Long l);

    List<ContextTask> getAllByTaskId(Long l);

    List<Long> getAllDistinctContextBasesIds();

    void save(ContextTask contextTask);
}
