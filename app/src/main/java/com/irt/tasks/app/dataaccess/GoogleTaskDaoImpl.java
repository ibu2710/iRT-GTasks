package com.irt.tasks.app.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import com.irt.tasks.app.businessobjects.GoogleTask;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.dataaccess.adapter.GoogleTaskContentProvider;
import java.util.ArrayList;
import java.util.List;

public class GoogleTaskDaoImpl implements GoogleTaskDao {
    private static final int MAX_GET_TASK_CURSOR_RETRIES_ = 20;
    private static final String URI_GOOGLE_TASK = "content://com.irt.tasks.app.dataaccess.adapter.googletaskcontentprovider";

    public void save(GoogleTask task) {
        contentResolver.insert(Uri.parse("content://com.irt.tasks.app.dataaccess.adapter.googletaskcontentprovider/task"), taskToContentValues(task));
    }

    public void saveOrUpdate(GoogleTask task) {
        Uri taskUri = getUriWithTaskId(task.getId());
        ContentValues taskContentValues = taskToContentValues(task);
        if (get(task.getId()) != null) {
            contentResolver.update(taskUri, taskContentValues, null, null);
        } else {
            contentResolver.insert(taskUri, taskContentValues);
        }
    }

    public void delete(GoogleTask task) {
        contentResolver.delete(getUriWithTaskId(task.getId()), null, null);
    }

    public List<GoogleTask> getAll() {
        return getTasks(null);
    }

    public GoogleTask get(String taskId) {
        Cursor cursor = getTaskCursor(getUriWithTaskId(taskId));
        GoogleTask task = null;
        if (cursor != null && cursor.moveToFirst()) {
            task = buildTask(cursor);
        }
        cursor.close();
        return task;
    }

    private Uri getUriWithTaskId(String taskId) {
        return Uri.parse("content://com.irt.tasks.app.dataaccess.adapter.googletaskcontentprovider/task/" + taskId);
    }

    private List<GoogleTask> getTasks(String extraCriteria) {
        Cursor cursor = getTaskCursor(Uri.parse("content://com.irt.tasks.app.dataaccess.adapter.googletaskcontentprovider/tasks"));
        List<GoogleTask> tasks = buildTasks(cursor);
        cursor.close();
        return tasks;
    }

    private Cursor getTaskCursor(Uri uri) {
        Cursor cursor = null;
        int retryCounter = 0;
        boolean isRetry;
        do {
            try {
                cursor = contentResolver.query(uri, GoogleTaskContentProvider.ALL_COLUMNS, null, null, null);
                break;
            } catch (RuntimeException e) {
                isRetry = isRetry(retryCounter, e);
                retryCounter++;
                if (!isRetry) {
                }
            }
        } while (isRetry);
        return cursor;
    }

    private boolean isRetry(int retryCounter, RuntimeException e) {
        System.out.println("Encountered " + e.getCause());
        System.out.println("Retry counter: [" + retryCounter + "], max retry: [" + 20 + "]");
        boolean isRetry = retryCounter <= 20;
        if (isRetry) {
            System.out.println("Retrying GoogleTask Content Provider access....");
            return isRetry;
        }
        System.out.println("Retry exhausted....");
        throw e;
    }

    private List<GoogleTask> buildTasks(Cursor cursor) {
        List<GoogleTask> tasks = new ArrayList();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                tasks.add(buildTask(cursor));
            }
        }
        return tasks;
    }

    private GoogleTask buildTask(Cursor cursor) {
        String id = cursor.getString(0);
        String etag = cursor.getString(1);
        String title = cursor.getString(2);
        Long updated = Long.valueOf(cursor.getLong(3));
        String selfLink = cursor.getString(4);
        String parent = cursor.getString(5);
        String position = cursor.getString(6);
        String notes = cursor.getString(7);
        String status = cursor.getString(8);
        Long due = Long.valueOf(cursor.getLong(9));
        Long completed = Long.valueOf(cursor.getLong(10));
        Boolean deleted = AppUtil.isNumberOne(cursor.getInt(11));
        Boolean hidden = AppUtil.isNumberOne(cursor.getInt(12));
        String taskListId = cursor.getString(13);
        String syncStatus = cursor.getString(14);
        String syncResult = cursor.getString(15);
        GoogleTask task = new GoogleTask();
        task.setId(id);
        task.setEtag(etag);
        task.setTitle(title);
        task.setUpdated(updated);
        task.setSelfLink(selfLink);
        task.setParent(parent);
        task.setPosition(position);
        task.setNotes(notes);
        task.setStatus(status);
        task.setDue(due);
        task.setCompleted(completed);
        task.setDeleted(deleted);
        task.setHidden(hidden);
        task.setTaskListId(taskListId);
        task.setSyncStatus(syncStatus);
        task.setSyncResult(syncResult);
        return task;
    }

    private ContentValues taskToContentValues(GoogleTask task) {
        ContentValues taskContentValues = new ContentValues();
        taskContentValues.put("id", task.getId());
        taskContentValues.put(GoogleTaskContentProvider.ETAG, task.getEtag());
        taskContentValues.put("title", task.getTitle());
        taskContentValues.put(GoogleTaskContentProvider.UPDATED, task.getUpdated());
        taskContentValues.put(GoogleTaskContentProvider.SELF_LINK, task.getSelfLink());
        taskContentValues.put("parent", task.getParent());
        taskContentValues.put("position", task.getPosition());
        taskContentValues.put(GoogleTaskContentProvider.NOTES, task.getNotes());
        taskContentValues.put("status", task.getStatus());
        taskContentValues.put(GoogleTaskContentProvider.DUE, task.getDue());
        taskContentValues.put("completed", task.getCompleted());
        taskContentValues.put("deleted", task.getDeleted());
        taskContentValues.put(GoogleTaskContentProvider.HIDDEN, task.getHidden());
        taskContentValues.put("taskListId", task.getTaskListId());
        taskContentValues.put(GoogleTaskContentProvider.SYNC_STATUS, task.getSyncStatus());
        taskContentValues.put(GoogleTaskContentProvider.SYNC_RESULT, task.getSyncResult());
        return taskContentValues;
    }
}
