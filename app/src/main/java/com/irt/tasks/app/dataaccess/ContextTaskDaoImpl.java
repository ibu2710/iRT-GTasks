package com.irt.tasks.app.dataaccess;

import android.database.Cursor;
import com.irt.tasks.app.businessobjects.ContextTask;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.dataaccess.adapter.ContextTaskDBAdapter;
import com.irt.tasks.app.dataaccess.adapter.ContextTaskDBAdapterImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.ArrayList;
import java.util.List;

public class ContextTaskDaoImpl implements ContextTaskDao {
    private ConfigManager config;

    public List<ContextTask> getAllByContextBaseId(Long contextBaseId) {
        ContextTaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllByContextBaseId(contextBaseId);
        List<ContextTask> contextTasks = buildContextTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return contextTasks;
    }

    public List<ContextTask> getAllByTaskId(Long taskId) {
        ContextTaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllByTaskId(taskId);
        List<ContextTask> contextTasks = buildContextTasks(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return contextTasks;
    }

    public int deleteAllByContextBaseId(Long contextBaseId) {
        ContextTaskDBAdapter dBAdapter = openDbAdapter();
        int deletes = dBAdapter.deleteAllbyContextBaseId(contextBaseId);
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
        return deletes;
    }

    public int deleteAllByTaskId(Long taskId) {
        ContextTaskDBAdapter dBAdapter = openDbAdapter();
        int deletes = dBAdapter.deleteAllbyTaskId(taskId);
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
        return deletes;
    }

    public int deleteAllWhereTaskDoesNotExist() {
        ContextTaskDBAdapter dBAdapter = openDbAdapter();
        int deletes = dBAdapter.deleteAllWhereTaskDoesNotExist();
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
        return deletes;
    }

    public void save(ContextTask contextTask) {
        ContextTaskDBAdapter dBAdapter = openDbAdapter();
        dBAdapter.insert(contextTask);
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    public List<Long> getAllDistinctContextBasesIds() {
        ContextTaskDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllDistinctContextBasesIds();
        List<Long> contextBasesIds = new ArrayList();
        while (cursor.moveToNext()) {
            contextBasesIds.add(Long.valueOf(cursor.getLong(0)));
        }
        cursor.close();
        closeDbAdapter(dBAdapter);
        return contextBasesIds;
    }

    private List<ContextTask> buildContextTasks(Cursor cursor) {
        List<ContextTask> contextTasks = new ArrayList();
        while (cursor.moveToNext()) {
            contextTasks.add(buildContextTask(cursor));
        }
        return contextTasks;
    }

    private ContextTask buildContextTask(Cursor cursor) {
        Long contextId = Long.valueOf(cursor.getLong(0));
        Long taskId = Long.valueOf(cursor.getLong(1));
        ContextTask contextTask = new ContextTask(contextId,taskId);
        contextTask.setContextId(contextId);
        contextTask.setTaskId(taskId);
        return contextTask;
    }

    private ContextTaskDBAdapter openDbAdapter() {
        this.config = ConfigManager.getInstance();
        ContextTaskDBAdapter dBAdapter = new ContextTaskDBAdapterImpl(this.config.getContext());
        dBAdapter.open();
        return dBAdapter;
    }

    private void closeDbAdapter(ContextTaskDBAdapter dBAdapter) {
        dBAdapter.close();
    }
}
