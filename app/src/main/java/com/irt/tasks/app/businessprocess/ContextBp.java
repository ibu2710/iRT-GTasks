package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.ContextBase;
import com.irt.tasks.app.businessobjects.ContextIncludes;
import com.irt.tasks.app.businessobjects.ContextTask;
import com.irt.tasks.app.common.Constants.UNIQUE_NAME_ERROR;
import java.util.List;

public interface ContextBp {
    int deleteAllConextTasksbyContextBaseId(Long l);

    int deleteAllConextTasksbyTaskId(Long l);

    int deleteAllContextIncludesByContextBaseId(Long l);

    void deleteContext(ContextBase contextBase);

    List<ContextIncludes> getAllContextIncludesByContextBaseId(Long l);

    List<ContextTask> getAllContextTasksByContextBaseId(Long l);

    List<ContextTask> getAllContextTasksByTaskId(Long l);

    List<ContextBase> getAllContexts();

    List<ContextBase> getAllContextsExcludingSpecificContext(Long l);

    List<Long> getAllDistinctContextBasesIdsForContextIncludes();

    List<Long> getAllDistinctContextBasesIdsForContextTasks();

    UNIQUE_NAME_ERROR isValidContextName(ContextBase contextBase);

    void saveContextIncludes(ContextIncludes contextIncludes);

    void saveContextTask(ContextTask contextTask);

    void saveOrUpdateContext(ContextBase contextBase);
}
