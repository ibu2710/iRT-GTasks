package com.irt.tasks.app.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public final class DateUtil {
    public static String format(Date date, String dateFormat) {
        Assert.notNull(date, "date");
        Assert.notNull(dateFormat, "dateFormat");
        return new SimpleDateFormat(dateFormat).format(date);
    }

    public static String format(Calendar calendar, String dateFormat) {
        Assert.notNull(calendar, "calendar");
        Assert.notNull(dateFormat, "dateFormat");
        return new SimpleDateFormat(dateFormat).format(calendar.getTime());
    }

    public static boolean isValidDate(String dateString, String dateFormat) {
        Assert.notNull(dateString, "dateString");
        Assert.notNull(dateFormat, "dateFormat");
        DateFormat df = new SimpleDateFormat(dateFormat);
        df.setLenient(false);
        try {
            df.parse(dateString);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public static Date parseDate(String dateString, String dateFormat) {
        Assert.notNull(dateString, "dateString");
        Assert.notNull(dateFormat, "dateFormat");
        DateFormat df = new SimpleDateFormat(dateFormat);
        df.setLenient(false);
        try {
            return df.parse(dateString);
        } catch (ParseException e) {
            throw new AssertFailedException(e.getMessage());
        }
    }

    public static Date parseDate(String dateString, String dateFormat, Date defaultDate) {
        return parseDateDefaultIfNull(dateString, dateFormat, defaultDate);
    }

    public static Date parseDateDefaultIfNull(String dateString, String dateFormat, Date defaultDate) {
        Assert.notNull(dateFormat, "dateFormat");
        return dateString == null ? defaultDate : parseDate(dateString, dateFormat);
    }

    public static Date parseDateDefaultIfInvalid(String dateString, String dateFormat, Date defaultDate) {
        Assert.notNull(dateFormat, "dateFormat");
        if (dateString != null) {
            DateFormat df = new SimpleDateFormat(dateFormat);
            df.setLenient(false);
            try {
                defaultDate = df.parse(dateString);
            } catch (ParseException e) {
            }
        }
        return defaultDate;
    }

    public static Calendar toCalendar(Date date) {
        Assert.notNull(date, "date");
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar;
    }

    private DateUtil() {
    }
}
