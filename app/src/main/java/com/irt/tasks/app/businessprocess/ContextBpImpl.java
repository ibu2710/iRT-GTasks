package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.ContextBase;
import com.irt.tasks.app.businessobjects.ContextIncludes;
import com.irt.tasks.app.businessobjects.ContextTask;
import com.irt.tasks.app.common.Constants.UNIQUE_NAME_ERROR;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.ContextBaseDao;
import com.irt.tasks.app.dataaccess.ContextBaseDaoImpl;
import com.irt.tasks.app.dataaccess.ContextIncludesDao;
import com.irt.tasks.app.dataaccess.ContextIncludesDaoImpl;
import com.irt.tasks.app.dataaccess.ContextTaskDao;
import com.irt.tasks.app.dataaccess.ContextTaskDaoImpl;
import java.util.List;

public class ContextBpImpl implements ContextBp {
    private ContextBaseDao contextBaseDao = new ContextBaseDaoImpl();
    private ContextIncludesDao contextIncludesDao = new ContextIncludesDaoImpl();
    private ContextTaskDao contextTaskDao = new ContextTaskDaoImpl();

    public List<ContextBase> getAllContexts() {
        return this.contextBaseDao.getAll();
    }

    public List<ContextBase> getAllContextsExcludingSpecificContext(Long contextId) {
        return this.contextBaseDao.getAllExcludingSpecificContext(contextId);
    }

    public void saveOrUpdateContext(ContextBase contextBase) {
        this.contextBaseDao.saveOrUpdate(contextBase);
    }

    public void deleteContext(ContextBase contextBase) {
        this.contextIncludesDao.deleteAllByContextBaseId(contextBase.getId());
        this.contextTaskDao.deleteAllByContextBaseId(contextBase.getId());
        this.contextBaseDao.delete(contextBase);
    }

    public UNIQUE_NAME_ERROR isValidContextName(ContextBase contextBase) {
        if (StringUtil.isNullOrEmpty(contextBase.getName())) {
            return UNIQUE_NAME_ERROR.BLANK;
        }
        if (this.contextBaseDao.isNameUsed(contextBase.getName(), contextBase.getId())) {
            return UNIQUE_NAME_ERROR.DUPLICATE;
        }
        return UNIQUE_NAME_ERROR.NO_ERROR;
    }

    public List<ContextIncludes> getAllContextIncludesByContextBaseId(Long contextBaseId) {
        return this.contextIncludesDao.getAllByContextBaseId(contextBaseId);
    }

    public int deleteAllContextIncludesByContextBaseId(Long contextBaseId) {
        return this.contextIncludesDao.deleteAllByContextBaseId(contextBaseId);
    }

    public List<Long> getAllDistinctContextBasesIdsForContextIncludes() {
        return this.contextIncludesDao.getAllDistinctContextBasesIds();
    }

    public void saveContextIncludes(ContextIncludes contextIncludes) {
        this.contextIncludesDao.save(contextIncludes);
    }

    public List<ContextTask> getAllContextTasksByContextBaseId(Long contextBaseId) {
        return this.contextTaskDao.getAllByContextBaseId(contextBaseId);
    }

    public int deleteAllConextTasksbyContextBaseId(Long contextBaseId) {
        return this.contextTaskDao.deleteAllByContextBaseId(contextBaseId);
    }

    public int deleteAllConextTasksbyTaskId(Long taskId) {
        return this.contextTaskDao.deleteAllByTaskId(taskId);
    }

    public List<Long> getAllDistinctContextBasesIdsForContextTasks() {
        return this.contextTaskDao.getAllDistinctContextBasesIds();
    }

    public void saveContextTask(ContextTask contextTask) {
        this.contextTaskDao.save(contextTask);
    }

    public List<ContextTask> getAllContextTasksByTaskId(Long taskId) {
        return this.contextTaskDao.getAllByTaskId(taskId);
    }
}
