package com.irt.tasks.app.businessprocess;

import android.text.format.DateFormat;
import com.irt.tasks.app.businessobjects.Account;
import com.irt.tasks.app.businessobjects.SyncStatistics;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.AccountDao;
import com.irt.tasks.app.dataaccess.AccountDaoImpl;
import com.irt.tasks.app.dataaccess.GtaskDao;
import com.irt.tasks.app.dataaccess.GtaskDao.SubBatchType;
import com.irt.tasks.app.dataaccess.GtaskDao2Impl;
import com.irt.tasks.app.dataaccess.GtasksListDao;
import com.irt.tasks.app.dataaccess.GtasksListDao2Impl;
import com.irt.tasks.app.dataaccess.TaskDao;
import com.irt.tasks.app.dataaccess.TaskDaoImpl;
import com.irt.tasks.app.dataaccess.TasksListDao;
import com.irt.tasks.app.dataaccess.TasksListDaoImpl;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.framework.business.ArchitectureContext;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SyncBpImpl implements SyncBp {
    private static final int MAX_SYNC_RETRY = 3;
    AccountDao accountDao = new AccountDaoImpl();
    ConfigManager config;
    GtaskDao gtasksDao = new GtaskDao2Impl();
    GtasksListDao gtasksListDao = new GtasksListDao2Impl();
    TaskBp taskBp = new TaskBpImpl();
    TaskDao taskDao = new TaskDaoImpl();
    TasksListBp tasksListBp = new TasksListBpImpl();
    TasksListDao tasksListDao = new TasksListDaoImpl();

    public synchronized boolean syncWithGoogleTasks() {
        return syncWithGoogleTasks(null);
    }

    public boolean syncWithGoogleTasks(TasksList tasksListToSync) {
        if (AppUtil.isOnline()) {
            boolean isSuccessful = false;
            String errorMessage = "";
            int i = 0;
            while (i < 3) {
                try {
                    performGoogleSync(tasksListToSync);
                    isSuccessful = true;
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                    errorMessage = e.getMessage();
                    i++;
                }
            }
            AppUtil.scheduleAllAlarms(ConfigManager.getInstance().getContext());
            if (isSuccessful) {
                return true;
            }
            return false;
        }
        this.config = ConfigManager.getInstance();
        System.out.println("iRT GTasks Outliner: Network not available...");
        this.config.setSyncing(false);
        return false;
    }

    private void performGoogleSync(TasksList tasksListToSync) {
        boolean isSyncSpecifiedList = true;
        this.config = ConfigManager.getInstance();
        if (!this.config.isSyncing()) {
            this.config.setSyncing(true);
            this.config.setCurrentTasksListBeingSynced(Constants.SYNC_TASKS_LIST_NAME_AT_START);
            if (tasksListToSync == null) {
                isSyncSpecifiedList = false;
            }
            if (isSyncSpecifiedList) {
                try {
                    if (DatabaseHelper.ALL_TASKS_LIST_ID.equals(tasksListToSync.getId())) {
                        isSyncSpecifiedList = false;
                    }
                } catch (Throwable th) {
                    this.config.setSyncing(false);
                    this.config.setCurrentTasksListBeingSynced(null);
                }
            }
            Long startTimeMillis = Long.valueOf(AppUtil.getTodayDateTimeMilisecond());
            String syncName = isSyncSpecifiedList ? "Synced ONE: " + tasksListToSync.getName() : "Synced ALL Auto-sync enabled lists";
            this.config.setLastSyncInfo(syncName);
            this.config.setLastSyncInfoDetails(DateFormat.format(Constants.DATE_FORMAT_MMMDYYYYHMMA, new Date(startTimeMillis.longValue())) + ", still SYNCING...");
            if (this.config.isDebug()) {
                System.out.println(" ***************************************************");
                System.out.println("-");
                System.out.println("-");
                System.out.println("-");
                System.out.println("-");
                System.out.println(" ******** START Syncing.... +" + new Date(startTimeMillis.longValue()));
                System.out.println("-");
                System.out.println("-");
                System.out.println("-");
                System.out.println("-");
                System.out.println(" ***************************************************");
            }
            SyncStatistics syncStat = new SyncStatistics();
            if (isSyncSpecifiedList) {
                performSyncWithSpecificList(tasksListToSync, syncStat);
            } else {
                performSyncAll(syncStat);
            }
            Long endTimeMillis = Long.valueOf(AppUtil.getTodayDateTimeMilisecond());
            if (this.config.isDebug()) {
                System.out.println(" ***************************************************");
                System.out.println(" ******** COMPLETED Syncing in " + ((endTimeMillis.longValue() - startTimeMillis.longValue()) / 1000) + " seconds at " + new Date(endTimeMillis.longValue()));
                System.out.println(" ***************************************************");
            }
            this.config.setLastSyncInfo(syncName + syncStat.reportStatistics());
            this.config.setLastSyncInfoDetails(DateFormat.format(Constants.DATE_FORMAT_MMMDYYYYHMMA, new Date(startTimeMillis.longValue())) + ", for " + ((endTimeMillis.longValue() - startTimeMillis.longValue()) / 1000) + " seconds" + syncStat.reportDetailedStatistics());
            if (this.config.isDebug()) {
                System.out.println("Google Tasks Sync details:");
                System.out.println(this.config.getLastSyncInfoDetails());
                System.out.println(" ***************************************************");
                System.out.println("Synced Accounts and Lists:");
                System.out.println(this.config.getLastSyncInfo());
                System.out.println(" ***************************************************");
            }
            this.config.setSyncing(false);
            this.config.setCurrentTasksListBeingSynced(null);
        }
    }

    private void performSyncWithSpecificList(TasksList tasksListToSync, SyncStatistics syncStat) {
        Account account = this.accountDao.get(tasksListToSync.getAccountId());
        if (account.isNewGoogleId().booleanValue()) {
            ArchitectureContext.setAccount(account);
            syncStat.incAccountsTotal();
            TasksList gtasksList = this.gtasksListDao.getTasksList(tasksListToSync.getGtasksListId());
            List<TasksList> gtasksLists = new ArrayList();
            if (gtasksList != null) {
                gtasksLists.add(gtasksList);
            }
            List<TasksList> tasksLists = new ArrayList();
            tasksLists.add(tasksListToSync);
            syncTasksListsAndGTasksLists(gtasksLists, tasksLists, syncStat);
            TasksList freshTasksList = this.tasksListDao.get(tasksListToSync.getId());
            if (freshTasksList != null) {
                tasksLists.clear();
                tasksLists.add(freshTasksList);
                syncTasksAndGTasks(tasksLists, syncStat);
            }
        }
    }

    private void performSyncAll(SyncStatistics syncStat) {
        for (Account account : this.accountDao.getAllAutoSync()) {
            if (account.isNewGoogleId().booleanValue()) {
                ArchitectureContext.setAccount(account);
                syncStat.incAccountsTotal();
                syncTasksListsAndGTasksLists(this.gtasksListDao.getAllTasksLists(), this.tasksListDao.getAllByAccountId(account.getId()), syncStat);
                syncTasksAndGTasks(this.tasksListDao.getAllByAccountIdExcludeMarkedAsDelete(account.getId()), syncStat);
            }
        }
    }

    private void syncTasksAndGTasks(List<TasksList> tasksLists, SyncStatistics syncStat) {
        for (TasksList tasksList : tasksLists) {
            if (tasksList.isAutoSync().booleanValue()) {
                updateSyncedTasksList(tasksList);
                List<Task> gtasksForList = loadGoogleTasksFromWeb(tasksList, syncStat);
                syncStat.addToWebTasksTotal(gtasksForList);
                List<Task> tasksForList = loadLocalTasks(tasksList, gtasksForList, syncStat);
                syncStat.addToLocalTasksTotal(tasksForList);
                SyncTasksHelper.syncTasksInList(tasksList, gtasksForList, tasksForList, syncStat);
            }
        }
    }

    private List<Task> loadLocalTasks(TasksList tasksList, List<Task> gtasksForList, SyncStatistics syncStat) {
        this.config = ConfigManager.getInstance();
        List<Task> tasksForList = this.taskDao.getAllTasksForGoogleSync(tasksList);
        int numberOfLocalDeletes = deleteLocalTasks(tasksForList, gtasksForList);
        syncStat.addToLocalDeletesTotal(numberOfLocalDeletes);
        if (numberOfLocalDeletes <= 0) {
            return tasksForList;
        }
        if (this.config.isDebug()) {
            System.out.println("Reloading local data data.");
        }
        return this.taskDao.getAllTasksForGoogleSync(tasksList);
    }

    private int deleteLocalTasks(List<Task> tasksForList, List<Task> gtasksForList) {
        List<Task> localTasksToDelete = getLocalTasksToDelete(tasksForList, gtasksForList);
        int tasksForDeletionCount = localTasksToDelete.size();
        for (Task task : localTasksToDelete) {
            this.taskBp.delete(task);
            task.setInTrashBin(Boolean.valueOf(true));
            this.taskDao.saveOrUpdateWithTimeStamp(task);
        }
        return tasksForDeletionCount;
    }

    private List<Task> getLocalTasksToDelete(List<Task> tasksForList, List<Task> gtasksForList) {
        List<Task> localTasksToDelete = new ArrayList();
        for (Task task : tasksForList) {
            if (!StringUtil.isNullOrEmpty(task.getGtaskId()) && getTaskFromWeb(task, gtasksForList) == null) {
                localTasksToDelete.add(task);
            }
        }
        return localTasksToDelete;
    }

    private List<Task> loadGoogleTasksFromWeb(TasksList tasksList, SyncStatistics syncStat) {
        this.config = ConfigManager.getInstance();
        List<Task> gtasksForList = this.gtasksDao.getAllTasksByTasksListId(tasksList.getGtasksListId());
        int numberOfWebDeletes = deleteWebTasks(tasksList, gtasksForList);
        syncStat.addToWebDeletesTotal(numberOfWebDeletes);
        if (numberOfWebDeletes <= 0) {
            return gtasksForList;
        }
        if (this.config.isDebug()) {
            System.out.println("Reloading web data.");
        }
        return this.gtasksDao.getAllTasksByTasksListId(tasksList.getGtasksListId());
    }

    private int deleteWebTasks(TasksList tasksList, List<Task> gtasksForList) {
        List<Task> filterWebTasksToDelete = filterWebTasksToDelete(this.taskDao.getTasksMarkedForDeletionButNotInTrashBin(tasksList.getId()), gtasksForList, tasksList.getGtasksListId());
        int tasksForDeletionCount = filterWebTasksToDelete.size();
        this.gtasksDao.batchUpdateTasks(filterWebTasksToDelete, SubBatchType.DELETE);
        return tasksForDeletionCount;
    }

    private List<Task> filterWebTasksToDelete(List<Task> tasksForWebDeletion, List<Task> gtasksForList, String gTasksListId) {
        List<Task> filteredWebTasksToDelete = new ArrayList();
        for (Task task : tasksForWebDeletion) {
            Task gtaskFromWeb = getTaskFromWeb(task, gtasksForList);
            if (gtaskFromWeb == null || gtaskFromWeb.getLastModified().longValue() > task.getLastModified().longValue()) {
                task.setInTrashBin(Boolean.valueOf(true));
                this.taskDao.saveOrUpdate(task);
            } else {
                task.setGtasksListId(gTasksListId);
                filteredWebTasksToDelete.add(task);
            }
        }
        return filteredWebTasksToDelete;
    }

    private Task getTaskFromWeb(Task task, List<Task> gtasksForList) {
        for (Task task2 : gtasksForList) {
            if (task2.getGtaskId().equals(task.getGtaskId())) {
                return task2;
            }
        }
        return null;
    }

    private void syncTasksListsAndGTasksLists(List<TasksList> gtasksLists, List<TasksList> tasksLists, SyncStatistics syncStat) {
        for (TasksList gtasksList : gtasksLists) {
            syncStat.incGTasksListsTotal();
            TasksList tasksList = getTaskListByGtasksList(gtasksList, tasksLists);
            if (tasksList == null) {
                this.tasksListDao.saveOrUpdate(gtasksList);
            } else if (!tasksList.isAutoSync().booleanValue() && !tasksList.isDeleted().booleanValue()) {
                syncStat.decGTasksListsTotal();
                syncStat.decTasksListsTotal();
            } else if (tasksList.isDeleted().booleanValue()) {
                if (tasksList.isDeleted().booleanValue()) {
                    if (getGtasksListTasksLastModifiedMillis(gtasksList).longValue() > tasksList.getLastModified().longValue() || !gtasksList.getName().equals(tasksList.getGtasksListName())) {
                        this.tasksListBp.restoreDeletedTasksList(tasksList.getId(), gtasksList.getName());
                    } else {
                        this.gtasksListDao.deleteTasksList(gtasksList);
                        this.tasksListBp.deleteTasksList(tasksList.getId(), true);
                    }
                }
            } else if (StringUtil.isNullOrEmpty(gtasksList.getName()) || gtasksList.getName().equals(tasksList.getName())) {
                if (StringUtil.isNullOrEmpty(gtasksList.getName()) && StringUtil.isNullOrEmpty(tasksList.getGtasksListName()) && !StringUtil.isNullOrEmpty(tasksList.getName())) {
                    updateGtasksListName(gtasksList, tasksList);
                }
            } else if (gtasksList.getName().equals(tasksList.getGtasksListName())) {
                updateGtasksListName(gtasksList, tasksList);
            } else {
                updateTasksListNameAndLocalGtasksListName(gtasksList, tasksList);
                this.tasksListDao.saveOrUpdate(tasksList);
            }
            if (tasksList != null) {
                syncStat.incTasksListsTotal();
                tasksLists.remove(tasksList);
            }
        }
        for (TasksList tasksList2 : tasksLists) {
            if (tasksList2.isAutoSync().booleanValue()) {
                if (!tasksList2.getAccountId().equals(DatabaseHelper.OFFLINE_ACCOUNT_ID) && !tasksList2.isDeleted().booleanValue() && StringUtil.isNullOrEmpty(tasksList2.getGtasksListId())) {
                    this.gtasksListDao.insertTasksList(tasksList2);
                    this.tasksListDao.saveOrUpdate(tasksList2);
                    syncStat.incTasksListsTotal();
                } else if (!StringUtil.isNullOrEmpty(tasksList2.getGtasksListId())) {
                    syncStat.incTasksListsTotal();
                    this.tasksListBp.deleteTasksList(tasksList2.getId(), true);
                }
            }
        }
    }

    private void updateSyncedTasksList(TasksList tasksList) {
        this.config.setCurrentTasksListBeingSynced("Syncing: " + tasksList.getName());
    }

    private Long getGtasksListTasksLastModifiedMillis(TasksList gtasksList) {
        List<Task> gtasksForList = this.gtasksDao.getAllTasksByTasksListId(gtasksList.getGtasksListId());
        Long gtasksListTasksLastModifiedMillis = Long.valueOf(0);
        for (Task task : gtasksForList) {
            if (gtasksListTasksLastModifiedMillis.longValue() < task.getLastModified().longValue()) {
                gtasksListTasksLastModifiedMillis = task.getLastModified();
            }
        }
        return gtasksListTasksLastModifiedMillis;
    }

    public void updateTasksListNameAndLocalGtasksListName(TasksList gtasksList, TasksList tasksList) {
        tasksList.setName(gtasksList.getName());
        tasksList.setGtasksListName(gtasksList.getName());
        tasksList.setActiveTasksList(gtasksList.istActiveTasksList());
    }

    private void updateGtasksListName(TasksList gtasksList, TasksList tasksList) {
        gtasksList.setName(tasksList.getName());
        this.gtasksListDao.renameTasksList(gtasksList);
        tasksList.setGtasksListName(tasksList.getName());
        tasksList.setActiveTasksList(gtasksList.istActiveTasksList());
        this.tasksListDao.saveOrUpdate(tasksList);
    }

    private TasksList getTaskListByGtasksList(TasksList gtasksList, List<TasksList> tasksLists) {
        for (TasksList tasksList : tasksLists) {
            if (gtasksList.getGtasksListId().equals(tasksList.getGtasksListId())) {
                return tasksList;
            }
        }
        return null;
    }
}
