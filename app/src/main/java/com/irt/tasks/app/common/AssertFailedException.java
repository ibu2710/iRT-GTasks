package com.irt.tasks.app.common;

public final class AssertFailedException extends RuntimeException {
    public AssertFailedException(String message) {
        super(message);
    }
}
