package com.irt.tasks.app.dataaccess;

import android.database.Cursor;
import com.irt.tasks.app.businessobjects.Email;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.adapter.EmailDBAdapter;
import com.irt.tasks.app.dataaccess.adapter.EmailDBAdapterImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.app.framework.security.AndroidCrypto;
import com.irt.tasks.app.framework.security.SecurityKey;

public class EmailDaoImpl implements EmailDao {
    ConfigManager config;

    public Email get() {
        Email email;
        EmailDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAll();
        if (cursor == null || !cursor.moveToFirst()) {
            email = new Email();
        } else {
            email = buildEmail(cursor);
        }
        cursor.close();
        closeDbAdapter(dBAdapter);
        return email;
    }

    public void saveOrUpdate(Email email) {
        EmailDBAdapter dBAdapter = openDbAdapter();
        if (email.getId() == null) {
            email.setId(Long.valueOf(dBAdapter.insert(email)));
        } else {
            dBAdapter.update(email);
        }
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    private Email buildEmail(Cursor cursor) {
        Long id = Long.valueOf(cursor.getLong(0));
        String name = cursor.getString(1);
        String emailAddress = cursor.getString(2);
        String password = cursor.getString(3);
        String extraEmail = cursor.getString(4);
        String signature = cursor.getString(5);
        Email email = new Email();
        email.setId(id);
        email.setName(name);
        email.setEmail(emailAddress);
        email.setExtraEmail(extraEmail);
        email.setSignature(signature);
        setEmailPassword(password, email);
        return email;
    }

    private void setEmailPassword(String password, Email email) {
        if (StringUtil.isNullOrEmpty(password)) {
            email.setPassword(null);
            return;
        }
        try {
            email.setPassword(AndroidCrypto.decrypt(SecurityKey.ENCRYPTION_MASTER_KEY, password, false));
        } catch (Exception e) {
            try {
                email.setPassword(AndroidCrypto.decrypt(SecurityKey.ENCRYPTION_MASTER_KEY, password, true));
                System.out.println("Saving email for Android compatibility [" + email.getEmail() + "]");
                saveOrUpdate(email);
            } catch (Exception e2) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }

    private EmailDBAdapter openDbAdapter() {
        this.config = ConfigManager.getInstance();
        EmailDBAdapter dBAdapter = new EmailDBAdapterImpl(this.config.getContext());
        dBAdapter.open();
        return dBAdapter;
    }

    private void closeDbAdapter(EmailDBAdapter dBAdapter) {
        dBAdapter.close();
    }
}
