package com.irt.tasks.app.dataaccess;

import com.dropbox.sync.android.DbxAccountManager;
import com.dropbox.sync.android.DbxException;
import com.dropbox.sync.android.DbxException.Unauthorized;
import com.dropbox.sync.android.DbxFile;
import com.dropbox.sync.android.DbxFileInfo;
import com.dropbox.sync.android.DbxFileStatus;
import com.dropbox.sync.android.DbxFileSystem;
import com.dropbox.sync.android.DbxFileSystem.PathListener;
import com.dropbox.sync.android.DbxFileSystem.PathListener.Mode;
import com.dropbox.sync.android.DbxPath;
import com.irt.tasks.app.businessobjects.Devices;
import com.irt.tasks.app.businessobjects.ImportExportFile;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.sync.DropboxSyncAdapter;
import com.irt.tasks.ui.sync.DropboxSyncHelper;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DropboxDaoImpl implements FileDao {
    public static final String APP_KEY = "97neu1xlx9aatdn";
    public static final String SECRET_KEY = "z6n1jqhwa107faf";
    private ConfigManager config;
    private DbxFileSystem dbxFileSystem;
    private DbxFile file;
    private DbxAccountManager mAccountManager;
    private PathListener pathListener = new C17451();

    class C17451 implements PathListener {
        C17451() {
        }

        public void onPathChange(DbxFileSystem fs, DbxPath registeredPath, Mode registeredMode) {
            if (!DropboxDaoImpl.this.config.isDropboxAutoSync()) {
                return;
            }
            if ((!DropboxDaoImpl.this.config.isDropboxAutoSyncOnWifiOnly() || DropboxDaoImpl.this.config.isWifiConnected()) && !DropboxDaoImpl.this.config.isDropboxFileSentFromThisDevice()) {
                DropboxSyncHelper.broadCastSyncStatus(DropboxSyncAdapter.SYNCING_STATUS_STARTING_FROM_FILE_LISTENER);
                AppUtil.logDebug("-");
                AppUtil.logDebug("-");
                AppUtil.logDebug("-");
                AppUtil.logDebug("-");
                AppUtil.logDebug("********>>>>>>>[" + Devices.getDeviceName() + "] Running the Dropbox file listner in DropboxDaoImpl.onPathChange <<<<<<<<<<<<<<<<<*******");
                DropboxSyncHelper.backendRequestSyncWithDelay(DropboxDaoImpl.this.config.getDropboxDelayInReceivingFilesFromOtherDevicesSeconds().intValue());
            }
        }
    }

    public long getFileLastModifiedDateMillis(String remoteRelativeFilePath) {
        try {
            DbxPath path = new DbxPath(remoteRelativeFilePath);
            if (this.dbxFileSystem.exists(path) && this.dbxFileSystem.isFile(path)) {
                waitForLatestFile(remoteRelativeFilePath);
                return this.dbxFileSystem.getFileInfo(path).modifiedTime.getTime();
            }
            this.dbxFileSystem.listFolder(new DbxPath(Globals.FORWARDSLASH));
            if (!this.dbxFileSystem.exists(path) || !this.dbxFileSystem.isFile(path)) {
                return 0;
            }
            waitForLatestFile(remoteRelativeFilePath);
            return this.dbxFileSystem.getFileInfo(path).modifiedTime.getTime();
        } catch (Unauthorized unauthorized) {
            unauthorized.printStackTrace();
            return 0;
        } catch (DbxException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public InputStream getFileStream(String remoteFromRelativeFilePath) throws IOException {
        try {
            this.file = this.dbxFileSystem.open(new DbxPath(remoteFromRelativeFilePath));
            return this.file.getReadStream();
        } catch (IOException e2) {
            e2.printStackTrace();
            throw new IOException(e2.getMessage());
        }
    }

    public void putFileOverwrite(String localFromFilePath, String remoteToRelativeFilePath) throws IOException {
        File localFromFile = new File(localFromFilePath);
        validateLocalFile(localFromFilePath, localFromFile);
        try {
            DbxPath path = new DbxPath(remoteToRelativeFilePath);
            if (this.dbxFileSystem.exists(path)) {
                this.file = this.dbxFileSystem.open(path);
            } else {
                this.file = this.dbxFileSystem.create(path);
            }
            this.file.writeFromExistingFile(localFromFile, false);
            this.file.close();
            if (this.file != null) {
                this.file.close();
                this.file = null;
            }
        } catch (DbxException e) {
            e.printStackTrace();
            throw new IOException(e.getMessage());
        } catch (IOException e2) {
            e2.printStackTrace();
            throw new IOException(e2.getMessage());
        } catch (Throwable th) {
            if (this.file != null) {
                this.file.close();
                this.file = null;
            }
        }
    }

    public List<ImportExportFile> getListOfFiles(String remotDirectory) {
        try {
            List<ImportExportFile> arrayList = new ArrayList();
            DbxPath path = new DbxPath(remotDirectory);
            if (!this.dbxFileSystem.exists(path)) {
                this.dbxFileSystem.createFolder(path);
            } else if (!this.dbxFileSystem.isFolder(path)) {
                this.dbxFileSystem.delete(path);
                this.dbxFileSystem.createFolder(path);
            }
            for (DbxFileInfo fileInfo : this.dbxFileSystem.listFolder(path)) {
                if (!fileInfo.isFolder) {
                    ImportExportFile importExportFile = new ImportExportFile();
                    importExportFile.setFilename(fileInfo.path.getName());
                    importExportFile.setDateMillis(Long.valueOf(fileInfo.modifiedTime.getTime()));
                    arrayList.add(importExportFile);
                }
            }
            return arrayList;
        } catch (DbxException e) {
            e.printStackTrace();
            return new ArrayList();
        }
    }

    public void closeFile() {
        if (this.file != null) {
            this.file.close();
            this.file = null;
        }
    }

    public boolean setSyncMetadataFileListener() {
        String remoteRelativeFilePath = Constants.DROPBOX_DATABASE_SYNC_META_DATA_RELATIVE_FILE_PATH;
        this.dbxFileSystem.addPathListener(this.pathListener, new DbxPath(Constants.DROPBOX_DATABASE_SYNC_META_DATA_RELATIVE_FILE_PATH), Mode.PATH_ONLY);
        return false;
    }

    public boolean removeSyncMetadataFileListener() {
        this.dbxFileSystem.removePathListenerForAll(this.pathListener);
        return false;
    }

    private void waitForLatestFile(String pathStr) {
        try {
            this.file = this.dbxFileSystem.open(new DbxPath(pathStr));
            int counter = 0;
            if (!this.file.getSyncStatus().isLatest) {
                DbxFileStatus status = this.file.getNewerStatus();
                while (!status.isCached && counter <= 60) {
                    counter++;
                    AppUtil.delay(1000);
                    AppUtil.logDebug("[" + pathStr + "] isCached counter .... = " + counter);
                    status = this.file.getNewerStatus();
                }
                this.file.update();
            }
            this.file.readString();
            this.file.close();
        } catch (DbxException e) {
            if (this.file != null) {
                this.file.close();
            }
            e.printStackTrace();
        } catch (IOException e2) {
            if (this.file != null) {
                this.file.close();
            }
            e2.printStackTrace();
        }
    }

    public DropboxDaoImpl() {
        try {
            this.config = ConfigManager.getInstance();
            this.mAccountManager = DbxAccountManager.getInstance(this.config.getContext(), APP_KEY, SECRET_KEY);
            this.dbxFileSystem = DbxFileSystem.forAccount(this.mAccountManager.getLinkedAccount());
        } catch (Unauthorized unauthorized) {
            unauthorized.printStackTrace();
        }
    }

    private void validateLocalFile(String localFromFilePath, File localFromFile) throws IOException {
        if (!localFromFile.exists()) {
            throw new IOException("FileCopy: no such source file: " + localFromFilePath);
        } else if (!localFromFile.isFile()) {
            throw new IOException("FileCopy: can't copy directory: " + localFromFilePath);
        } else if (!localFromFile.canRead()) {
            throw new IOException("FileCopy: source file is unreadable: " + localFromFilePath);
        }
    }
}
