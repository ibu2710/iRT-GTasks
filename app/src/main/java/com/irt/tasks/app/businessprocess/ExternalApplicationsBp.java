package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.TasksList;
import java.util.List;

public interface ExternalApplicationsBp {
    boolean exportBranchToRemoteServer(Long l, String str);

    boolean exportToRemoteServer(TasksList tasksList, String str, int i, boolean z);

    List<String> getListOfRemoteFilesForImport();

    List<String> getRemoteFilesListForImport();

    int importAppendToListFromRemoteServer(TasksList tasksList, String str);

    int importReplaceListFromRemoteServer(TasksList tasksList, String str);

    int importToNewListFromRemoteServer(Long l, String str, String str2);
}
