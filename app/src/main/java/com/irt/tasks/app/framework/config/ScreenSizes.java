package com.irt.tasks.app.framework.config;

public class ScreenSizes {
    public static final int AUTO_SCREEN_SIZE = 0;
    public static final int DROID_FWVGA_HEIGHT = 854;
    public static final int DROID_FWVGA_SCREEN_SIZE = 3;
    public static final int DROID_FWVGA_WIDTH = 480;
    public static final int MYTOUCH_3G_HVGA_HEIGHT = 480;
    public static final int MYTOUCH_3G_HVGA_SCREEN_SIZE = 1;
    public static final int MYTOUCH_3G_HVGA_WIDTH = 320;
    public static final int NEXUS_ONE_WVGA_HEIGHT = 800;
    public static final int NEXUS_ONE_WVGA_SCREEN_SIZE = 2;
    public static final int NEXUS_ONE_WVGA_WIDTH = 480;

    public static final boolean isValidScreenSize(int width, int height) {
        if (width == MYTOUCH_3G_HVGA_WIDTH && height == 480) {
            return true;
        }
        if (width == 480 && height == NEXUS_ONE_WVGA_HEIGHT) {
            return true;
        }
        if (width == 480 && height == DROID_FWVGA_HEIGHT) {
            return true;
        }
        return false;
    }
}
