package com.irt.tasks.app.businessprocess;

import android.content.Context;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.framework.business.ErrorTypes.ERROR;
import com.irt.tasks.ui.panel.EditActionDetail;
import java.util.List;
import java.util.Map;

public interface TaskBp {
    void clearAllSnoozeTasks(TasksList tasksList, Context context);

    void copyBranchToList(Task task, TasksList tasksList);

    void copyTaskRecursively(Task task, Task task2, boolean z, List<Task> list, Long l, boolean z2);

    int countTasksByTasksListId(Long l, boolean z);

    void cutAndPasteBranch(Task task, Task task2, boolean z, boolean z2);

    void delete(Task task);

    void deleteCompleted(TasksList tasksList);

    void deleteRecursively(Task task, List<Task> list);

    int deleteTasksInTrashBin();

    void expandCollapseBranchAllChildren(Map<String, Long> map, Task task, List<Task> list, Integer num, boolean z);

    Task get(Long l);

    List<Task> getAllForView(int i, TasksList tasksList, String str);

    List<Task> getAllTasksWithCompletedAndSupportForAllTasks(TasksList tasksList, boolean z);

    List<Task> getAllTasksWithEligibleAlarms();

    List<Task> getAllTasksWithEligibleAlarmsIncludingInvisibleAccounts();

    List<Task> getSortedTasksAtMaxLevelIncludeCompletedTasks(TasksList tasksList);

    List<Task> getTaskChildrenAndGrandChildren(Task task, List<Task> list);

    List<Task> getTasksByLevel(Map<String, Long> map, int i, boolean z, TasksList tasksList, boolean z2);

    void moveDown(Task task, List<Task> list, Map<String, Long> map);

    void moveLeft(Task task, List<Task> list, Map<String, Long> map);

    void moveRight(Task task, List<Task> list, Map<String, Long> map);

    void moveUp(Task task, List<Task> list, Map<String, Long> map);

    void multipleDeletes(EditActionDetail editActionDetail, List<Task> list);

    ERROR multipleEditAndPaste(EditActionDetail editActionDetail, List<Task> list);

    void restoreTask(Task task, boolean z);

    void saveOrUpdate(Task task, TasksList tasksList);

    List<Task> searchAllTasks(String str, List<Task> list);

    void setTaskAsDoneAndAdjustAlarm(Task task, Context context);

    void sortPermanent(TasksList tasksList, int i);

    void uncheckCompleted(TasksList tasksList);

    ERROR validateCutAndPaste(Task task, Task task2, List<Task> list);
}
