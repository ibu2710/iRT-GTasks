package com.irt.tasks.app.dataaccess;

import android.database.Cursor;
import com.irt.tasks.app.businessobjects.ContextBase;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.dataaccess.adapter.ContextBaseDBAdapter;
import com.irt.tasks.app.dataaccess.adapter.ContextBaseDBAdapterImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.filter.TasksFilterManager;
import java.util.ArrayList;
import java.util.List;

public class ContextBaseDaoImpl implements ContextBaseDao {
    private ConfigManager config;
    private ContextIncludesDao contextIncludesDao = new ContextIncludesDaoImpl();
    private ContextTaskDao contextTaskDao = new ContextTaskDaoImpl();

    public List<ContextBase> getAll() {
        return performGetAll(null);
    }

    private List<ContextBase> performGetAll(Long excludedContexBaseId) {
        ContextBaseDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAll();
        List<ContextBase> contextBases = buildContextBases(cursor, excludedContexBaseId);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return contextBases;
    }

    public List<ContextBase> getAllExcludingSpecificContext(Long contextId) {
        return performGetAll(contextId);
    }

    public void saveOrUpdate(ContextBase contextBase) {
        ContextBaseDBAdapter dBAdapter = openDbAdapter();
        if (contextBase.getId() == null) {
            contextBase.setId(Long.valueOf(dBAdapter.insert(contextBase)));
        } else {
            dBAdapter.update(contextBase);
        }
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    public void delete(ContextBase contextBase) {
        TasksFilterManager.getInstance().removeContextId(contextBase.getId());
        this.contextIncludesDao.deleteAllByContextBaseId(contextBase.getId());
        this.contextTaskDao.deleteAllByContextBaseId(contextBase.getId());
        ContextBaseDBAdapter dBAdapter = openDbAdapter();
        dBAdapter.delete(contextBase.getId().longValue());
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    public boolean isNameUsed(String name, Long contextBaseId) {
        ContextBaseDBAdapter dBAdapter = openDbAdapter();
        boolean isNameUsed = dBAdapter.isNameUsed(name, contextBaseId);
        closeDbAdapter(dBAdapter);
        return isNameUsed;
    }

    private List<ContextBase> buildContextBases(Cursor cursor, Long excludedContextBaseId) {
        List<ContextBase> contextBases = new ArrayList();
        while (cursor.moveToNext()) {
            ContextBase contextBase = buildContextBase(cursor);
            if (!contextBase.getId().equals(excludedContextBaseId)) {
                contextBases.add(contextBase);
            }
        }
        return contextBases;
    }

    private ContextBase buildContextBase(Cursor cursor) {
        Long id = Long.valueOf(cursor.getLong(0));
        String name = cursor.getString(1);
        ContextBase contextBase = new ContextBase();
        contextBase.setId(id);
        contextBase.setName(name);
        return contextBase;
    }

    private ContextBaseDBAdapter openDbAdapter() {
        this.config = ConfigManager.getInstance();
        ContextBaseDBAdapter dBAdapter = new ContextBaseDBAdapterImpl(this.config.getContext());
        dBAdapter.open();
        return dBAdapter;
    }

    private void closeDbAdapter(ContextBaseDBAdapter dBAdapter) {
        dBAdapter.close();
    }
}
