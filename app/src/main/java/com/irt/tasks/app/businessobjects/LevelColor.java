package com.irt.tasks.app.businessobjects;

import com.irt.tasks.app.framework.business.IrtTasksBo;

public class LevelColor implements IrtTasksBo {
    public static final String FONT_COLOR = "fontColor";
    public static final String FONT_STYLE = "fontStyle";
    public static final String LEVEL = "level";
    public static final String ROW_ID = "id";
    public static final String TASKS_LIST_ID = "tasksListId";
    private Integer fontColor = Integer.valueOf(0);
    private Integer fontStyle = Integer.valueOf(0);
    private Long id;
    private Integer level;
    private Long tasksListId;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTasksListId() {
        return this.tasksListId;
    }

    public void setTasksListId(Long tasksListId) {
        this.tasksListId = tasksListId;
    }

    public Integer getLevel() {
        return this.level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getFontColor() {
        return this.fontColor;
    }

    public void setFontColor(Integer fontColor) {
        this.fontColor = fontColor;
    }

    public Integer getFontStyle() {
        return this.fontStyle;
    }

    public void setFontStyle(Integer fontStyle) {
        this.fontStyle = fontStyle;
    }
}
