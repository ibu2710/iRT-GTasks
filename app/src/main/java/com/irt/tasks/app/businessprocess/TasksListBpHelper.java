package com.irt.tasks.app.businessprocess;

import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.dataaccess.TaskDao;
import com.irt.tasks.app.dataaccess.TaskDaoImpl;
import com.irt.tasks.app.dataaccess.TasksListDao;
import com.irt.tasks.app.dataaccess.TasksListDaoImpl;
import com.irt.tasks.app.framework.config.ConfigManager;

public class TasksListBpHelper {
    public static final Long NO_PARENT_TAKS_ID = Long.valueOf(0);

    public static void createTasksTreeTutorialTasksList(Long accountIdToUse) {
        TasksList tasksList = createTasksList(accountIdToUse, "Sample, Tutorial on Tasks Tree - tap to proceed", "Tutorial on Tasks Tree basic navigation");
        Task task = createChildTask(tasksList.getId(), "4. Expand/collapse tasks one level at a time", "SWIPE left/right at middle of view", NO_PARENT_TAKS_ID, createChildTask(tasksList.getId(), "3. Change Font Size", "SWIPE left/right at top of view", NO_PARENT_TAKS_ID, createChildTask(tasksList.getId(), "2. Add NEW task", "TAP on blue Plus icon on lower right", NO_PARENT_TAKS_ID, createChildTask(tasksList.getId(), "1. Read Help for this view", "TAP on QUESTION MARK on Top Right", NO_PARENT_TAKS_ID, createChildTask(tasksList.getId(), "Follow the instructions below for basic navigation. READ the NOTES for each task.", "Have fun!", NO_PARENT_TAKS_ID, Constants.NO_SIBLING_ID, Integer.valueOf(2), Integer.valueOf(2), Integer.valueOf(0), Long.valueOf(0)).getId(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Long.valueOf(0)).getId(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Long.valueOf(0)).getId(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Long.valueOf(0)).getId(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Long.valueOf(0));
        Long priorSiblingId = task.getId();
        task = createChildTask(tasksList.getId(), "Another sample child task", null, createChildTask(tasksList.getId(), "Sample child task", null, task.getId(), Constants.NO_SIBLING_ID, Integer.valueOf(0), Integer.valueOf(3), Integer.valueOf(0), Long.valueOf(0)).getId(), Constants.NO_SIBLING_ID, Integer.valueOf(0), Integer.valueOf(2), Integer.valueOf(0), Long.valueOf(0));
        task = createChildTask(tasksList.getId(), "6. Expand/collapse ALL children for this branch", "LEFT Long press on this task", NO_PARENT_TAKS_ID, createChildTask(tasksList.getId(), "5. Expand/collapse tasks to minimum or maximum level", "SWIPE left/right at bottom of view", NO_PARENT_TAKS_ID, priorSiblingId, Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Long.valueOf(0)).getId(), Integer.valueOf(0), Integer.valueOf(2), Integer.valueOf(0), Long.valueOf(0));
        priorSiblingId = task.getId();
        task = createChildTask(tasksList.getId(), "Sample task", null, createChildTask(tasksList.getId(), "Another sample child task", null, createChildTask(tasksList.getId(), "Sample child task", null, task.getId(), Constants.NO_SIBLING_ID, Integer.valueOf(0), Integer.valueOf(3), Integer.valueOf(0), Long.valueOf(0)).getId(), Constants.NO_SIBLING_ID, Integer.valueOf(0), Integer.valueOf(2), Integer.valueOf(0), Long.valueOf(0)).getId(), Constants.NO_SIBLING_ID, Integer.valueOf(0), Integer.valueOf(3), Integer.valueOf(0), Long.valueOf(0));
        task = createChildTask(tasksList.getId(), "8. Expand/collapse this task. ", "LEFT single tap on this task", NO_PARENT_TAKS_ID, createChildTask(tasksList.getId(), "7. Show/hide the CHECKBOXES. ", "LEFT Double tap on this task", NO_PARENT_TAKS_ID, priorSiblingId, Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Long.valueOf(0)).getId(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Long.valueOf(0));
        priorSiblingId = task.getId();
        task = createChildTask(tasksList.getId(), "Another sample child task", null, createChildTask(tasksList.getId(), "Sample child task", null, task.getId(), Constants.NO_SIBLING_ID, Integer.valueOf(0), Integer.valueOf(3), Integer.valueOf(0), Long.valueOf(0)).getId(), Constants.NO_SIBLING_ID, Integer.valueOf(0), Integer.valueOf(2), Integer.valueOf(0), Long.valueOf(0));
        task = createChildTask(tasksList.getId(), "For more help,  tap on the question mark on the top right of this view", null, NO_PARENT_TAKS_ID, createChildTask(tasksList.getId(), "All these are configurable", "Menu - > More - > Preferences - > Navigation Preferences - > Gesture Preferences", NO_PARENT_TAKS_ID, createChildTask(tasksList.getId(), "11. EDIT this task", "RIGHT Single tap on this task", NO_PARENT_TAKS_ID, createChildTask(tasksList.getId(), "10. Show/hide the Drag and Drop icon and Plus panel", "RIGHT Double tap on this task", NO_PARENT_TAKS_ID, createChildTask(tasksList.getId(), "9. Show the task context menu", "RIGHT Long press on this task", NO_PARENT_TAKS_ID, priorSiblingId, Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Long.valueOf(0)).getId(), Integer.valueOf(2), Integer.valueOf(2), Integer.valueOf(7), Long.valueOf(0)).getId(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Long.valueOf(0)).getId(), Integer.valueOf(0), Integer.valueOf(12), Integer.valueOf(0), Long.valueOf(0)).getId(), Integer.valueOf(0), Integer.valueOf(9), Integer.valueOf(0), Long.valueOf(0));
    }

    public static void createInboxTasksList(Long accountIdToUse) {
        ConfigManager config = ConfigManager.getInstance();
        TasksList tasksList = createTasksList(accountIdToUse, "00 Inbox", "My Inbox. It would be good to use this as default for the Blue Plus icon.");
        config.setTasksListForNewTaskPlus(tasksList.getId());
        createChildTask(tasksList.getId(), "The Blue Plus icon on the Tasks List Home and Agenda views will use '00 Inbox' as the default Tasks List for new tasks", "This has been set on:\nMenu -> More -> Preferences -> Navigation Preferences -> Blue Plus Tasks List -> 00 Inbox", NO_PARENT_TAKS_ID, Constants.NO_SIBLING_ID, Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Long.valueOf(0));
    }

    public static void createLongPressTasksList(Long accountIdToUse) {
        createChildTask(createTasksList(accountIdToUse, "Sample, Long press on me to edit this list", "Sample tasks list for editing").getId(), "I'm a sample task", "I'm a sample task, try the Tutorial on Tasks Tree.", NO_PARENT_TAKS_ID, Constants.NO_SIBLING_ID, Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Long.valueOf(0));
    }

    public static void createRightDoubleTapTasksList(Long accountIdToUse) {
        createChildTask(createTasksList(accountIdToUse, "Sample, Right Double tap on me to see Due date view", "Sample tasks list to access Due date view").getId(), "I'm a sample task with due date", "A sample task for Due date view. Tap back button to go back.", NO_PARENT_TAKS_ID, Constants.NO_SIBLING_ID, Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Long.valueOf(AppUtil.getTodayDateOnly().getTime()));
    }

    public static void createLeftDoubleTapTasksList(Long accountIdToUse) {
        createChildTask(createTasksList(accountIdToUse, "Sample, Left Double tap on me to see Last modified view", "Sample tasks list to access Last modified view").getId(), "I'm a sample task", "A sample task for Last modified view. Tap back button to go back.", NO_PARENT_TAKS_ID, Constants.NO_SIBLING_ID, Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Long.valueOf(0));
    }

    private static Task createChildTask(Long tasksListId, String name, String note, Long parentTaskId, Long priorSiblingTaskId, Integer fontStyle, Integer fontColor, Integer fontBgColor, Long dueDate) {
        TaskDao taskDao = new TaskDaoImpl();
        Task task = new Task();
        task.setTasksListId(tasksListId);
        task.setName(name);
        task.setNote(note);
        task.setParentTaskId(parentTaskId);
        task.setPriorSiblingTaskId(priorSiblingTaskId);
        task.setFontStyle(fontStyle);
        task.setFontColor(fontColor);
        task.setFontBgColor(fontBgColor);
        task.setDueDate(dueDate);
        taskDao.saveOrUpdateWithTimeStamp(task);
        return task;
    }

    private static TasksList createTasksList(Long accountId, String name, String note) {
        TasksListDao tasksListDao = new TasksListDaoImpl();
        TasksList tasksList = new TasksList();
        tasksList.setAccountId(accountId);
        tasksList.setName(name);
        tasksList.setNote(note);
        tasksListDao.saveOrUpdateWithTimeStamp(tasksList);
        return tasksList;
    }
}
