package com.irt.tasks.app.businessprocess;

import android.content.Context;
import android.widget.Toast;
import com.irt.tasks.app.businessobjects.ContextTask;
import com.irt.tasks.app.businessobjects.LevelState;
import com.irt.tasks.app.businessobjects.TagTask;
import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.Task.FloatComparator;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.Globals;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.dataaccess.AccountDao;
import com.irt.tasks.app.dataaccess.AccountDaoImpl;
import com.irt.tasks.app.dataaccess.ContextTaskDao;
import com.irt.tasks.app.dataaccess.ContextTaskDaoImpl;
import com.irt.tasks.app.dataaccess.TagTaskDao;
import com.irt.tasks.app.dataaccess.TagTaskDaoImpl;
import com.irt.tasks.app.dataaccess.TaskDao;
import com.irt.tasks.app.dataaccess.TaskDaoImpl;
import com.irt.tasks.app.dataaccess.TasksListDao;
import com.irt.tasks.app.dataaccess.TasksListDaoImpl;
import com.irt.tasks.app.dataaccess.adapter.DatabaseHelper;
import com.irt.tasks.app.dataaccess.adapter.TaskDBAdapterImpl;
import com.irt.tasks.app.framework.business.ErrorTypes.ERROR;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.customrepeat.CustomRepeatHelper;
import com.irt.tasks.ui.panel.EditActionDetail;
import com.irt.tasks.ui.preferences.ArraysConstants;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class TaskBpImpl implements TaskBp {
    public static final int AGENDA_DAYS_AHEAD = 8;
    public static final int AGENDA_DAY_PRIOR = -1;
    AccountDao accountDao = new AccountDaoImpl();
    ContextTaskDao contextTaskDao = new ContextTaskDaoImpl();
    TagTaskDao tagTaskDao = new TagTaskDaoImpl();
    TaskDao taskDao = new TaskDaoImpl();
    TaskStateBp taskStateBp = new TaskStateBpImpl();
    TasksListBp tasksListBp = new TasksListBpImpl();
    TasksListDao tasksListDao = new TasksListDaoImpl();

    public void clearAllSnoozeTasks(TasksList tasksList, Context context) {
        for (Task task : getAllForView(1, tasksList, null)) {
            AppUtil.clearSnooze(context, this.taskDao, task);
        }
    }

    public void setTaskAsDoneAndAdjustAlarm(Task task, Context context) {
        if (task.isRecurringTask() && (CustomRepeatHelper.getUntil(task.getReminderRepeatRule()) == 0 || task.getDueDate().longValue() < CustomRepeatHelper.getUntil())) {
            task.setDueDate(Long.valueOf(CustomRepeatHelper.getNextDueDate(task)));
            if (task.getReminderDateTime().longValue() != 0) {
                AppUtil.calculateReminderDateTime(task.getDueDate(), task, false);
            }
            task.setCompleted(Boolean.valueOf(false));
            if (context != null) {
                Toast.makeText(context, "Due date set to " + AppUtil.formatDateByPreference(new Date(task.getDueDate().longValue())), Toast.LENGTH_LONG).show();
            }
        }
        if (task.isCompleted().booleanValue()) {
            long todayDateMillis = AppUtil.getTodayDateOnly().getTime();
            if (task.isFloat().booleanValue() && task.getDueDate().longValue() <= todayDateMillis) {
                task.setDueDate(Long.valueOf(todayDateMillis));
            }
            task.setIsFloat(Boolean.valueOf(false));
        }
        if (context == null) {
            AppUtil.setTaskAlarm(ConfigManager.getInstance().getContext(), task);
        } else {
            AppUtil.setTaskAlarm(context, task);
        }
    }

    public boolean areAllChildrenComplete(Task task) {
        return false;
    }

    public void sortPermanent(TasksList tasksList, int sortScope) {
        LevelState levelState = this.taskStateBp.getLevelSate(tasksList.getId(), null);
        List<Task> originalSortedTasks = getAllTasksWithCompletedAndSupportForAllTasks(tasksList, false);
        List<Task> sortedTasksByName = new ArrayList();
        Task parentTask = new Task();
        parentTask.setId(Long.valueOf(0));
        int zoomLevel = 0;
        if (sortScope != 2 && AppUtil.isZoomSet(levelState)) {
            levelState.setZoomTask(getTaskFromSortedTasks(new Long(levelState.getZoomTaskIdStr()), originalSortedTasks));
            parentTask = levelState.getZoomTask();
            zoomLevel = levelState.getZoomTask().getLevel().intValue();
        }
        int sortLevel = zoomLevel + 1;
        int maxSortLevel = zoomLevel + 1;
        switch (sortScope) {
            case 0:
                maxSortLevel = zoomLevel + 1;
                break;
            case 1:
            case 2:
                maxSortLevel = 20;
                break;
        }
        sortByTaskNameRecursively(parentTask, originalSortedTasks, sortedTasksByName, sortLevel, maxSortLevel);
        for (Task task2 : sortedTasksByName) {
            if (task2.isTaskNeedUpdate()) {
                task2.setMoved(Boolean.valueOf(true));
                this.taskDao.saveOrUpdateWithTimeStamp(task2);
            }
        }
    }

    private void sortByTaskNameRecursively(Task parentTask, List<Task> originalSortedTasks, List<Task> sortedTasksByName, int sortLevel, int maxSortLevel) {
        if (sortLevel <= maxSortLevel) {
            List<Task> childrenTasks = AppUtil.getTaskChildren(parentTask, originalSortedTasks);
            originalSortedTasks.remove(childrenTasks);
            sortByTaskName(childrenTasks);
            Task prevTask = null;
            for (int i = 0; i < childrenTasks.size(); i++) {
                Task task = (Task) childrenTasks.get(i);
                if (!task.getParentTaskId().equals(parentTask.getId())) {
                    task.isTaskNeedUpdate();
                    task.setParentTaskId(parentTask.getId());
                }
                if (i == 0) {
                    task.setTaskNeedUpdate(!task.getPriorSiblingTaskId().equals(Constants.NO_SIBLING_ID));
                    task.setPriorSiblingTaskId(Constants.NO_SIBLING_ID);
                } else if (!task.getPriorSiblingTaskId().equals(prevTask.getId())) {
                    task.setTaskNeedUpdate(true);
                    task.setPriorSiblingTaskId(prevTask.getId());
                }
                sortedTasksByName.add(task);
                sortByTaskNameRecursively(task, originalSortedTasks, sortedTasksByName, sortLevel + 1, maxSortLevel);
                prevTask = task;
            }
        }
    }

    private void sortByTaskName(List<Task> childrenTasks) {
        int taskSize = childrenTasks.size();
        for (int i = 0; i < taskSize - 1; i++) {
            for (int j = i + 1; j < taskSize; j++) {
                if (((Task) childrenTasks.get(i)).getName().compareToIgnoreCase(((Task) childrenTasks.get(j)).getName()) > 0) {
                    Task taskBuff = (Task) childrenTasks.get(i);
                    childrenTasks.add(i, childrenTasks.get(j));
                    childrenTasks.remove(i + 1);
                    childrenTasks.add(j, taskBuff);
                    childrenTasks.remove(j + 1);
                }
            }
        }
    }

    public void restoreTask(Task task, boolean isRestoreFromTrashBin) {
        this.taskDao.forceDelete(task.getId());
        task.setId(null);
        task.setMoved(Boolean.valueOf(true));
        task.setDeleted(Boolean.valueOf(false));
        task.setInTrashBin(Boolean.valueOf(false));
        task.setParentTaskId(Long.valueOf(0));
        task.setPriorSiblingTaskId(Constants.NO_SIBLING_ID);
        if (isRestoreFromTrashBin) {
            task.setGtaskId(null);
        }
        saveOrUpdate(task, null);
        AppUtil.setTaskAlarm(ConfigManager.getInstance().getContext(), task);
    }

    public void uncheckCompleted(TasksList tasksList) {
        List<Task> sortedTasks = getAllTasksWithCompletedAndSupportForAllTasks(tasksList, false);
        LevelState levelState = this.taskStateBp.getLevelSate(tasksList.getId(), null);
        if (AppUtil.isZoomSet(levelState)) {
            sortedTasks = getTaskChildrenAndGrandChildren(levelState.getZoomTask(), sortedTasks);
        }
        for (Task task : sortedTasks) {
            if (task.isCompleted().booleanValue()) {
                task.setLastContentModifiedDate(Long.valueOf(AppUtil.getTodayDateTimeMilisecond()));
                task.setCompleted(Boolean.valueOf(false));
                saveOrUpdate(task, null);
            }
        }
    }

    public void deleteCompleted(TasksList tasksList) {
        List<Task> sortedTasks = getAllTasksWithCompletedAndSupportForAllTasks(tasksList, false);
        LevelState levelState = this.taskStateBp.getLevelSate(tasksList.getId(), null);
        if (AppUtil.isZoomSet(levelState)) {
            sortedTasks = getTaskChildrenAndGrandChildren(levelState.getZoomTask(), sortedTasks);
        }
        performMultipleDeletes(sortedTasks, getCompetedTasks(sortedTasks));
        AppUtil.scheduleAllAlarms(ConfigManager.getInstance().getContext());
    }

    public List<Task> getAllTasksWithCompletedAndSupportForAllTasks(TasksList tasksList, boolean isIncludeTasksList) {
        ConfigManager config = ConfigManager.getInstance();
        boolean isHideCompletedTasks = config.isHideCompletedTasks();
        config.setHideCompletedTasks(false);
        List<Task> sortedTasks = getTasksByLevel(null, 20, false, tasksList, isIncludeTasksList);
        config.setHideCompletedTasks(isHideCompletedTasks);
        return sortedTasks;
    }

    private List<Task> getCompetedTasks(List<Task> sortedTasks) {
        List<Task> competedTasks = new ArrayList();
        for (Task task : sortedTasks) {
            if (task.isCompleted().booleanValue()) {
                competedTasks.add(task);
            }
        }
        return competedTasks;
    }

    public List<Task> getTasksByLevel(Map<String, Long> expandedTasks, int level, boolean includeTaskPath, TasksList tasksListToUse, boolean isIncludeTasksList) {
        ConfigManager config = ConfigManager.getInstance();
        List<Task> allTasksWithImbededTasksList = new ArrayList();
        SyncTasksHelper syncTasksHelper = new SyncTasksHelper();
        syncTasksHelper.initIndentions();
        syncTasksHelper.setLevelToDisplay(level);
        int listNumber = 0;
        boolean isOneList = true;
        List<TasksList> tasksLists;
        if (tasksListToUse == null || tasksListToUse.getId().equals(DatabaseHelper.ALL_TASKS_LIST_ID)) {
            isOneList = false;
            tasksLists = this.tasksListBp.getAllTasksListBySortOrder();
        } else {
            tasksLists = new ArrayList();
            tasksLists.add(tasksListToUse);
        }
        for (TasksList tasksList : tasksLists) {
            if (config.isShowInvisibleAccounts().booleanValue() || tasksList.isVisible().booleanValue()) {
                if (!isOneList && isIncludeTasksList) {
                    allTasksWithImbededTasksList.add(getTasksListAsTask(tasksList, listNumber + 1));
                }
                allTasksWithImbededTasksList.addAll(syncTasksHelper.sortTasks(this.taskDao.getAllTasksByTasksListId(tasksList.getId(), null, false, false, false), expandedTasks, tasksList, includeTaskPath, isOneList));
                listNumber++;
            }
        }
        return allTasksWithImbededTasksList;
    }

    public void saveOrUpdate(Task task, TasksList origTasksList) {
        setTasksListTaskLastModifiedTime(task);
        if (task.isTasksListAsTask()) {
            TasksList tasksList = this.tasksListDao.get(task.getTasksListId());
            tasksList.setName(task.getName());
            this.tasksListDao.saveOrUpdate(tasksList);
        } else if (origTasksList != null) {
            Task targetTask = new Task();
            Long origTaskId = task.getId();
            targetTask.setTasksListId(task.getTasksListId());
            targetTask.setTasksListAsTask(true);
            task.setTasksListId(origTasksList.getId());
            copyTaskRecursively(task, targetTask, false, getSortedTasksAtMaxLevelIncludeCompletedTasks(origTasksList), targetTask.getTasksListId(), true);
            deleteBranchByTaskIdAndTasksList(origTasksList, origTaskId);
        } else {
            boolean isNew;
            if (task.getId() == null) {
                isNew = true;
            } else {
                isNew = false;
            }
            this.taskDao.saveOrUpdateWithTimeStamp(task);
            if (isNew) {
                moveSiblingDown(task);
            }
        }
    }

    public List<Task> getSortedTasksAtMaxLevelIncludeCompletedTasks(TasksList origTasksList) {
        return AppUtil.getMaxSortedTasksForListIncludeCompleted(origTasksList, this.taskDao.getAllTasksByTasksListId(origTasksList.getId(), null, false, false, false));
    }

    private void deleteBranchByTaskIdAndTasksList(TasksList tasksList, Long taskId) {
        List<Task> sortedTasksAtMaxLevel = getSortedTasksAtMaxLevelIncludeCompletedTasks(tasksList);
        deleteRecursively(getTaskFromSortedTasks(taskId, sortedTasksAtMaxLevel), sortedTasksAtMaxLevel);
    }

    private Task getTaskFromSortedTasks(Long origTaskId, List<Task> sortedTasksAtMaxLevel) {
        for (Task task : sortedTasksAtMaxLevel) {
            if (task.getId().equals(origTaskId)) {
                return task;
            }
        }
        return null;
    }

    private void setTasksListTaskLastModifiedTime(Task task) {
        TasksList tasksList = this.tasksListDao.get(task.getTasksListId());
        tasksList.setTaskLastModified(Long.valueOf(AppUtil.getTodayDateTimeMilisecond()));
        this.tasksListDao.saveOrUpdate(tasksList);
    }

    public void delete(Task task) {
        moveSiblingsAndChildrenUp(task, false);
        this.taskDao.delete(task);
    }

    public int deleteTasksInTrashBin() {
        return this.taskDao.deleteTasksInTrashBin();
    }

    public void copyBranchToList(Task sourceTask, TasksList targetTasksList) {
        if (targetTasksList.getId() == null) {
            this.tasksListDao.saveOrUpdateWithTimeStamp(targetTasksList);
        }
        Task targetTask = new Task();
        targetTask.setTasksListId(targetTasksList.getId());
        targetTask.setTasksListAsTask(true);
        copyTaskRecursively(sourceTask, targetTask, false, getSortedTasksAtMaxLevelIncludeCompletedTasks(this.tasksListDao.get(sourceTask.getTasksListId())), targetTask.getTasksListId(), true);
    }

    public void cutAndPasteBranch(Task cutTask, Task pasteTask, boolean isTargetExpanded, boolean isTopOfList) {
        if (cutTask.getTasksListId().equals(pasteTask.getTasksListId())) {
            moveSiblingsAndChildrenUp(cutTask, true);
            if (isTopOfList) {
                pasteTask.setPriorSiblingTaskId(cutTask.getId());
                pasteTask.setMoved(Boolean.valueOf(true));
                this.taskDao.saveOrUpdateWithTimeStamp(pasteTask);
                cutTask.setParentTaskId(pasteTask.getParentTaskId());
                cutTask.setPriorSiblingTaskId(Constants.NO_SIBLING_ID);
                cutTask.setMoved(Boolean.valueOf(true));
                this.taskDao.saveOrUpdateWithTimeStamp(cutTask);
                return;
            }
            if (isTargetExpanded) {
                cutTask.setParentTaskId(pasteTask.getId());
                cutTask.setPriorSiblingTaskId(Long.valueOf(0));
            } else {
                cutTask.setParentTaskId(pasteTask.getParentTaskId());
                cutTask.setPriorSiblingTaskId(pasteTask.getId());
            }
            cutTask.setMoved(Boolean.valueOf(true));
            this.taskDao.saveOrUpdateWithTimeStamp(cutTask);
            moveSiblingDown(cutTask);
            return;
        }
        crossListCutAndPaste(cutTask, pasteTask, isTargetExpanded);
    }

    private void crossListCutAndPaste(Task cutTask, Task pasteTask, boolean isTargetExpanded) {
        Long origTaskId = cutTask.getId();
        TasksList origTasksList = this.tasksListDao.get(cutTask.getTasksListId());
        copyTaskRecursively(cutTask, pasteTask, isTargetExpanded, getTasksByLevel(null, 20, false, origTasksList, true), null, true);
        deleteBranchByTaskIdAndTasksList(origTasksList, origTaskId);
    }

    public List<Task> searchAllTasks(String queryString, List<Task> tasksToQuery) {
        List<Task> filteredTasks = new ArrayList();
        if (!StringUtil.isNullOrEmpty(queryString)) {
            for (Task task : tasksToQuery) {
                if (isQueryMatch(task, queryString)) {
                    filteredTasks.add(task);
                }
            }
        }
        return filteredTasks;
    }

    private boolean isQueryMatch(Task task, String queryString) {
        boolean isQueryMatch = false;
        if (!StringUtil.isNullOrEmpty(task.getName())) {
            isQueryMatch = task.getName().toLowerCase().contains(queryString.toLowerCase());
        }
        if (isQueryMatch || StringUtil.isNullOrEmpty(task.getNote())) {
            return isQueryMatch;
        }
        return task.getNote().toLowerCase().contains(queryString.toLowerCase());
    }

    public Task get(Long taskId) {
        return this.taskDao.get(taskId);
    }

    public int countTasksByTasksListId(Long tasksListId, boolean isCompleted) {
        return this.taskDao.countTasksByTasksListId(tasksListId, isCompleted);
    }

    public List<Task> getAllForView(int viewType, TasksList tasksList, String searchQuery) {
        ConfigManager config = ConfigManager.getInstance();
        List<Task> tasks = new ArrayList();
        switch (viewType) {
            case 0:
                return getQuickTasksForFiltersView(tasksList, DatabaseHelper.ORDER_ASC);
            case 1:
                return getQuickTasksForSnoozeView(tasksList, DatabaseHelper.ORDER_ASC);
            case 2:
                if (!isAllTasksList(tasksList)) {
                    tasks = getTasksForAlphabeticalView(tasksList, DatabaseHelper.ORDER_ASC);
                    break;
                }
                return getQuickTasksForAlphabeticalView(tasksList, DatabaseHelper.ORDER_ASC);
            case 3:
                if (isAllTasksList(tasksList)) {
                    return getQuickTasksForCompletedTasksView(tasksList, DatabaseHelper.ORDER_DESC);
                }
                return getTasksForCompletedView(tasksList, DatabaseHelper.ORDER_DESC);
            case 4:
                if (!isAllTasksList(tasksList)) {
                    tasks = getTasksForCreationDateView(tasksList, DatabaseHelper.ORDER_DESC);
                    break;
                }
                return getQuickTasksForCreationDateView(tasksList, DatabaseHelper.ORDER_DESC);
            case 5:
                if (!isAllTasksList(tasksList)) {
                    tasks = getTasksForDueDateView(tasksList, DatabaseHelper.ORDER_ASC);
                    break;
                }
                return getQuickTasksForDueDateView(tasksList, DatabaseHelper.ORDER_ASC);
            case 6:
                return zoomTasksFilter(tasksList, getTasksByLevel(null, 20, true, tasksList, false));
            case 7:
                if (!isAllTasksList(tasksList)) {
                    tasks = getTasksForLastModifiedView(tasksList, DatabaseHelper.ORDER_DESC);
                    break;
                }
                return getQuickTasksForLastModifiedView(tasksList, DatabaseHelper.ORDER_DESC);
            case 8:
                if (!isAllTasksList(tasksList)) {
                    tasks = getTasksForAlphabeticalView(tasksList, DatabaseHelper.ORDER_DESC);
                    break;
                }
                return getQuickTasksForAlphabeticalView(tasksList, DatabaseHelper.ORDER_DESC);
            case 9:
                if (isAllTasksList(tasksList)) {
                    return getQuickTasksForCompletedTasksView(tasksList, DatabaseHelper.ORDER_ASC);
                }
                return getTasksForCompletedView(tasksList, DatabaseHelper.ORDER_ASC);
            case 10:
                if (!isAllTasksList(tasksList)) {
                    tasks = getTasksForCreationDateView(tasksList, DatabaseHelper.ORDER_ASC);
                    break;
                }
                return getQuickTasksForCreationDateView(tasksList, DatabaseHelper.ORDER_ASC);
            case 11:
                if (!isAllTasksList(tasksList)) {
                    tasks = getTasksForDueDateView(tasksList, DatabaseHelper.ORDER_DESC);
                    break;
                }
                return getQuickTasksForDueDateView(tasksList, DatabaseHelper.ORDER_DESC);
            case 12:
                tasks = getTasksByLevel(null, 20, true, tasksList, false);
                Collections.reverse(tasks);
                break;
            case 13:
                if (!isAllTasksList(tasksList)) {
                    tasks = getTasksForLastModifiedView(tasksList, DatabaseHelper.ORDER_ASC);
                    break;
                }
                return getQuickTasksForLastModifiedView(tasksList, DatabaseHelper.ORDER_ASC);
            case 14:
                return getQuickTasksForSnoozeView(tasksList, DatabaseHelper.ORDER_DESC);
            case 15:
                return getListForDeletedOnlyView(tasksList);
            case 16:
                return getListForTrashBinOnlyView(tasksList);
            case 17:
                if (config.isShowFloatsOnAgenda()) {
                    return getQuickTasksForAgendaViewWithFloats(tasksList, DatabaseHelper.ORDER_ASC);
                }
                return getQuickTasksForAgendaView(tasksList, DatabaseHelper.ORDER_ASC);
            case ArraysConstants.VIEW_MODIFIED /*18*/:
                return getQuickTasksForModifiedView(tasksList, DatabaseHelper.ORDER_DESC);
            case 19:
                return getQuickTasksForCreatedView(tasksList, DatabaseHelper.ORDER_DESC);
            case 20:
                return searchAllTasks(searchQuery, zoomTasksFilter(tasksList, getTasksByLevel(null, 20, true, tasksList, true)));
        }
        return getTasksWithSortedInfo(tasks, tasksList);
    }

    private List<Task> zoomTasksFilter(TasksList tasksList, List<Task> tasks) {
        if (isAllTasksList(tasksList)) {
            return tasks;
        }
        LevelState levelState = this.taskStateBp.getLevelSate(tasksList.getId(), null);
        if (AppUtil.isZoomSet(levelState)) {
            return getTaskChildrenAndGrandChildren(get(new Long(levelState.getZoomTaskIdStr())), tasks);
        }
        return tasks;
    }

    private List<Task> getTasksForLastModifiedView(TasksList tasksList, String orderBy) {
        return getListForView(tasksList, TaskDBAdapterImpl.LAST_CONTENT_MODIFIED_DATE, orderBy, false, false, false);
    }

    private List<Task> getTasksForDueDateView(TasksList tasksList, String orderBy) {
        return sortForFloats(getListForView(tasksList, TaskDBAdapterImpl.DUE_DATE + orderBy + ", " + TaskDBAdapterImpl.REMINDER_ENABLED + ", " + TaskDBAdapterImpl.REMINDER_DATE_TIME + ", lower(" + "name" + ") ", DatabaseHelper.ORDER_ASC, false, true, false), DatabaseHelper.ORDER_ASC.equals(orderBy));
    }

    private List<Task> sortForFloats(List<Task> tasks, boolean isAscending) {
        List<Task> floatingTasks = new ArrayList();
        List<Task> pastDueDateTasks = new ArrayList();
        List<Task> futureDueDateTasks = new ArrayList();
        long todayDateMillis = AppUtil.getTodayDateOnly().getTime();
        for (Task task : tasks) {
            if (task.getDueDate().longValue() == todayDateMillis || (task.isFloat().booleanValue() && task.getDueDate().longValue() < todayDateMillis)) {
                boolean z = task.isFloat().booleanValue() && task.getDueDate().longValue() < todayDateMillis;
                task.setConsideredAsFloat(z);
                floatingTasks.add(task);
            } else if (task.getDueDate().longValue() < todayDateMillis) {
                pastDueDateTasks.add(task);
            } else {
                futureDueDateTasks.add(task);
            }
        }
        if (floatingTasks.size() == 0) {
            return tasks;
        }
        Collections.sort(floatingTasks, new FloatComparator());
        List<Task> finalTasks = new ArrayList();
        if (isAscending) {
            finalTasks.addAll(pastDueDateTasks);
            finalTasks.addAll(floatingTasks);
            finalTasks.addAll(futureDueDateTasks);
        } else {
            finalTasks.addAll(futureDueDateTasks);
            finalTasks.addAll(floatingTasks);
            finalTasks.addAll(pastDueDateTasks);
        }
        return finalTasks;
    }

    private List<Task> getTasksForCreationDateView(TasksList tasksList, String orderBy) {
        return getListForView(tasksList, TaskDBAdapterImpl.CREATION_DATE, orderBy, false, false, false);
    }

    private List<Task> getTasksForCompletedView(TasksList tasksList, String orderBy) {
        List<Task> tasks = getListForView(tasksList, TaskDBAdapterImpl.LAST_CONTENT_MODIFIED_DATE, orderBy, true, false, false);
        ConfigManager config = ConfigManager.getInstance();
        boolean isHideCompletedTasks = config.isHideCompletedTasks();
        config.setHideCompletedTasks(false);
        tasks = getTasksWithSortedInfo(tasks, tasksList);
        config.setHideCompletedTasks(isHideCompletedTasks);
        return tasks;
    }

    private List<Task> getQuickTasksForDueDateView(TasksList tasksList, String orderBy) {
        return sortForFloats(getQuickListForView(tasksList, "(due_date> 0 OR is_float = 1 ) AND completed = 0", "due_date " + orderBy + ", " + TaskDBAdapterImpl.REMINDER_ENABLED + ", " + TaskDBAdapterImpl.REMINDER_DATE_TIME + ", " + "lower(" + "task" + Globals.PERIOD + "name" + ")", true), DatabaseHelper.ORDER_ASC.equals(orderBy));
    }

    private List<Task> getQuickTasksForFiltersView(TasksList tasksList, String orderAsc) {
        return this.taskDao.getFilteredVisibleTasks(tasksList.getId());
    }

    private List<Task> getQuickTasksForAgendaView(TasksList tasksList, String orderBy) {
        ConfigManager config = ConfigManager.getInstance();
        Calendar cal = Calendar.getInstance();
        cal.setTime(AppUtil.getTodayDateOnly());
        cal.add(Calendar.DAY_OF_YEAR, -1);
        long startDate = cal.getTime().getTime();
        cal.add(Calendar.DAY_OF_YEAR, 8);
        long endDate = cal.getTime().getTime();
        return getQuickListForView(tasksList, "due_date >= " + startDate + " AND " + TaskDBAdapterImpl.DUE_DATE + " <= " + endDate + setCompletedFileter(config, ""), "due_date, reminder_enabled, reminder_date_time, lower(task.name)  " + orderBy, true);
    }

    private String setCompletedFileter(ConfigManager config, String completedFilter) {
        if (config.isHideCompletedTasks()) {
            return " AND completed = 0";
        }
        return completedFilter;
    }

    private List<Task> getQuickTasksForAgendaViewWithFloats(TasksList tasksList, String orderBy) {
        ConfigManager config = ConfigManager.getInstance();
        Calendar cal = Calendar.getInstance();
        cal.setTime(AppUtil.getTodayDateOnly());
        cal.add(Calendar.DAY_OF_YEAR, -1);
        long startDate = cal.getTime().getTime();
        cal.add(Calendar.DAY_OF_YEAR, 8);
        long endDate = cal.getTime().getTime();
        return sortForFloats(getQuickListForView(tasksList, "(due_date >= " + startDate + " OR " + TaskDBAdapterImpl.IS_FLOAT + " = 1 ) AND " + TaskDBAdapterImpl.DUE_DATE + " <= " + endDate + setCompletedFileter(config, ""), "due_date, reminder_enabled, reminder_date_time, lower(task.name)  " + orderBy, true), DatabaseHelper.ORDER_ASC.equals(orderBy));
    }

    private List<Task> getQuickTasksForLastModifiedView(TasksList tasksList, String orderBy) {
        return getQuickListForView(tasksList, " ", "last_content_modified_date " + orderBy, true);
    }

    private List<Task> getQuickTasksForAlphabeticalView(TasksList tasksList, String orderBy) {
        return getQuickListForView(tasksList, " ", "lower(task.name)  " + orderBy, true);
    }

    private List<Task> getQuickTasksForCreationDateView(TasksList tasksList, String orderBy) {
        return getQuickListForView(tasksList, " ", "creation_date " + orderBy, true);
    }

    private List<Task> getQuickTasksForModifiedView(TasksList tasksList, String orderBy) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(AppUtil.getTodayDateTime());
        long endDate = cal.getTime().getTime();
        cal.add(Calendar.DAY_OF_YEAR, -7);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return getQuickListForView(tasksList, "last_content_modified_date >= " + cal.getTime().getTime() + " AND " + TaskDBAdapterImpl.LAST_CONTENT_MODIFIED_DATE + " <= " + endDate, "last_content_modified_date " + orderBy, true);
    }

    private List<Task> getQuickTasksForCreatedView(TasksList tasksList, String orderBy) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(AppUtil.getTodayDateTime());
        long endDate = cal.getTime().getTime();
        cal.add(Calendar.DAY_OF_YEAR, -7);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return getQuickListForView(tasksList, "creation_date >= " + cal.getTime().getTime() + " AND " + TaskDBAdapterImpl.CREATION_DATE + " <= " + endDate, "creation_date " + orderBy, true);
    }

    private List<Task> getQuickTasksForSnoozeView(TasksList tasksList, String orderBy) {
        return getQuickListForView(tasksList, "snooze_date_time > 0", "snooze_date_time " + orderBy, true);
    }

    private List<Task> getQuickTasksForCompletedTasksView(TasksList tasksList, String orderBy) {
        return getQuickListForView(tasksList, "completed = 1", "last_content_modified_date " + orderBy, true);
    }

    private List<Task> getQuickListForView(TasksList tasksList, String query, String orderBy, boolean isAllTasks) {
        List<Task> tasks = new ArrayList();
        if (isAllTasks || isAllTasksList(tasksList)) {
            return this.taskDao.getQuickAllVisibleTasks(query, orderBy);
        }
        return tasks;
    }

    private List<Task> getTasksForAlphabeticalView(TasksList tasksList, String orderBy) {
        return getListForView(tasksList, "lower(name) ", orderBy, false, false, false);
    }

    private List<Task> getListForView(TasksList tasksList, String column, String order, boolean isCompletedOnly, boolean isDueDateOnly, boolean isSnoozeOnly) {
        if (isAllTasksList(tasksList)) {
            return this.taskDao.getAllVisibleTasks(column + order, isCompletedOnly, isDueDateOnly, isSnoozeOnly);
        }
        return this.taskDao.getAllTasksByTasksListId(tasksList.getId(), column + order, isCompletedOnly, isDueDateOnly, isSnoozeOnly);
    }

    private boolean isAllTasksList(TasksList tasksList) {
        return DatabaseHelper.ALL_TASKS_LIST_ID.equals(tasksList.getId());
    }

    private List<Task> getListForDeletedOnlyView(TasksList tasksList) {
        List<Task> tasks;
        if (isAllTasksList(tasksList)) {
            tasks = this.taskDao.getAllVisibleTasksDeletedOnly();
            setTasksListsOnTasks(tasks);
            return tasks;
        }
        tasks = this.taskDao.getAllTasksDeletedOnlyByTasksListId(tasksList.getId());
        setTasksListOnTask(tasksList, tasks);
        return tasks;
    }

    private List<Task> getListForTrashBinOnlyView(TasksList tasksList) {
        List<Task> tasks;
        if (isAllTasksList(tasksList)) {
            tasks = this.taskDao.getAllVisibleTasksTrashBinOnly();
            setTasksListsOnTasks(tasks);
            return tasks;
        }
        tasks = this.taskDao.getAllTasksTrashBinOnlyByTasksListId(tasksList.getId());
        setTasksListOnTask(tasksList, tasks);
        return tasks;
    }

    private void setTasksListOnTask(TasksList tasksList, List<Task> tasks) {
        for (Task task : tasks) {
            task.setTasksListName(tasksList.getName());
            task.setTaskPath("");
        }
    }

    private void setTasksListsOnTasks(List<Task> tasks) {
        List<TasksList> tasksLists = this.tasksListDao.getAllIncludingDeletedAndTrashBin();
        for (Task task : tasks) {
            TasksList tasksList = getTasksList(task.getTasksListId(), tasksLists);
            if (tasksList != null) {
                task.setTasksListName(tasksList.getName());
                task.setTaskPath("");
            } else {
                task.setTasksListName(null);
                task.setTaskPath("");
            }
        }
    }

    private TasksList getTasksList(Long tasksListId, List<TasksList> tasksLists) {
        for (TasksList tasksList : tasksLists) {
            if (tasksList.getId().equals(tasksListId)) {
                return tasksList;
            }
        }
        return null;
    }

    public void moveLeft(Task task, List<Task> sortedTasks, Map<String, Long> mExpandedTasks) {
        if (!task.getParentTaskId().equals(Long.valueOf(0))) {
            if (task.getPriorSiblingTaskId().equals(Constants.NO_SIBLING_ID)) {
                mExpandedTasks.remove(task.getParentTaskId());
            }
            convertNextSiblingsToChildren(task, sortedTasks);
            Task parentTask = this.taskDao.get(task.getParentTaskId());
            task.setParentTaskId(parentTask.getParentTaskId());
            task.setPriorSiblingTaskId(parentTask.getId());
            task.setMoved(Boolean.valueOf(true));
            this.taskDao.saveOrUpdateWithTimeStamp(task);
            moveSiblingDown(task);
        }
    }

    public void moveRight(Task task, List<Task> sortedTasks, Map<String, Long> mExpandedTasks) {
        if (!task.getPriorSiblingTaskId().equals(Constants.NO_SIBLING_ID)) {
            mExpandedTasks.put(task.getPriorSiblingTaskId() + "", Constants.TASK_EXPAND_INDICATOR);
            moveSiblingsAndChildrenUp(task, true);
            Task lastChildTaskOfParent = getLastChildTaskOfParent(task.getPriorSiblingTaskId(), sortedTasks);
            task.setParentTaskId(task.getPriorSiblingTaskId());
            if (lastChildTaskOfParent != null) {
                task.setPriorSiblingTaskId(lastChildTaskOfParent.getId());
            } else {
                task.setPriorSiblingTaskId(Constants.NO_SIBLING_ID);
            }
            task.setMoved(Boolean.valueOf(true));
            this.taskDao.saveOrUpdateWithTimeStamp(task);
        }
    }

    private Task getLastChildTaskOfParent(Long parentTaskId, List<Task> sortedTasks) {
        Task lastChildTaskOfParent = null;
        for (Task task : sortedTasks) {
            if (task.getParentTaskId().equals(parentTaskId)) {
                lastChildTaskOfParent = task;
            }
        }
        return lastChildTaskOfParent;
    }

    public void moveUp(Task task, List<Task> sortedTasks, Map<String, Long> map) {
        if (task.getParentTaskId().equals(Constants.NO_SIBLING_ID) && task.getPriorSiblingTaskId().equals(Constants.NO_SIBLING_ID)) {
            Task priorSiblingTask = null;
            int i = 0;
            while (i < sortedTasks.size()) {
                Task task2 = (Task) sortedTasks.get(i);
                if (!task2.getId().equals(task.getId()) || i <= 1) {
                    if ((!task2.isTasksListAsTask() && task2.getParentTaskId().equals(Long.valueOf(0))) || (task2.isTasksListAsTask() && !task2.getTasksListId().equals(task.getTasksListId()))) {
                        priorSiblingTask = task2;
                    }
                    i++;
                } else if (priorSiblingTask != null) {
                    cutAndPasteBranch(task, priorSiblingTask, false, false);
                    return;
                } else {
                    return;
                }
            }
        } else if (task.getPriorSiblingTaskId().equals(Constants.NO_SIBLING_ID)) {
            performMoveUp(task, this.taskDao.get(task.getParentTaskId()));
        } else {
            performMoveUp(task, this.taskDao.get(task.getPriorSiblingTaskId()));
        }
    }

    private void performMoveUp(Task task, Task parentTask) {
        if (!isFirstChildOfList(parentTask)) {
            performNormalMoveUp(task, parentTask);
        } else if (task.getPriorSiblingTaskId().equals(Constants.NO_SIBLING_ID)) {
            parentTask.setPriorSiblingTaskId(task.getId());
            parentTask.setMoved(Boolean.valueOf(true));
            this.taskDao.saveOrUpdateWithTimeStamp(parentTask);
            task.setParentTaskId(Long.valueOf(0));
            task.setPriorSiblingTaskId(Constants.NO_SIBLING_ID);
            task.setMoved(Boolean.valueOf(true));
            this.taskDao.saveOrUpdateWithTimeStamp(task);
        } else {
            cutAndPasteBranch(parentTask, task, false, false);
        }
    }

    private boolean isFirstChildOfList(Task task) {
        return task.getParentTaskId().equals(Long.valueOf(0)) && task.getPriorSiblingTaskId().equals(Constants.NO_SIBLING_ID);
    }

    private void performNormalMoveUp(Task task, Task parentTask) {
        if (parentTask.getPriorSiblingTaskId().equals(Constants.NO_SIBLING_ID)) {
            cutAndPasteBranch(task, this.taskDao.get(parentTask.getParentTaskId()), true, false);
        } else {
            cutAndPasteBranch(task, this.taskDao.get(parentTask.getPriorSiblingTaskId()), false, false);
        }
    }

    public void moveDown(Task task, List<Task> sortedTasks, Map<String, Long> map) {
        Task nextSibling = getNextSibling(task, sortedTasks);
        if (nextSibling != null) {
            cutAndPasteBranch(task, nextSibling, false, false);
            return;
        }
        Task nextParentSibling = getNextParentSibling(task, sortedTasks);
        if (nextParentSibling != null) {
            cutAndPasteBranch(task, nextParentSibling, false, false);
        }
    }

    public ERROR validateCutAndPaste(Task sourceTask, Task targetTask, List<Task> sortedTasks) {
        if (sourceTask == null || sourceTask.getId() == null) {
            return ERROR.ERROR_NO_SOURCE_SELECTED;
        }
        if (sourceTask.getId().equals(targetTask.getId())) {
            return ERROR.ERROR_PARENT_PASTED_ON_CHILD;
        }
        if (sourceTask.isParent()) {
            for (Task task : getTaskChildrenAndGrandChildren(sourceTask, sortedTasks)) {
                if (task.getId().equals(targetTask.getId())) {
                    return ERROR.ERROR_PARENT_PASTED_ON_CHILD;
                }
            }
        }
        return ERROR.NO_ERROR;
    }

    public ERROR multipleEditAndPaste(EditActionDetail editActionDetail, List<Task> sortedTasks) {
        Task task;
        List<Task> sourceTasks = getCleanTasksListForEditing(editActionDetail.getSelectedTasks(), sortedTasks);
        Task desitnationTask = editActionDetail.getDestinationTask();
        for (Task task2 : sourceTasks) {
            if (task2.getId().equals(desitnationTask.getId())) {
                return ERROR.ERROR_PARENT_PASTED_ON_CHILD;
            }
        }
        List<Task> childrenAndGrandChildrenTasks = new ArrayList();
        for (Task task22 : sourceTasks) {
            if (task22.isParent()) {
                childrenAndGrandChildrenTasks.addAll(getTaskChildrenAndGrandChildren(task22, sortedTasks));
            }
        }
        for (Task task222 : childrenAndGrandChildrenTasks) {
            if (task222.getId().equals(desitnationTask.getId())) {
                return ERROR.ERROR_PARENT_PASTED_ON_CHILD;
            }
        }
        Task prevTask;
        int i;
        switch (editActionDetail.getFirstEditAction()) {
            case CUT:
                prevTask = desitnationTask;
                for (i = 0; i < sourceTasks.size(); i++) {
                    task = (Task) sourceTasks.get(i);
                    if (i == 0) {
                        cutAndPasteBranch(task, desitnationTask, editActionDetail.isDestinationExpanded(), false);
                    } else {
                        cutAndPasteBranch(task, prevTask, false, false);
                    }
                    prevTask = task;
                }
                break;
            case COPY:
                prevTask = desitnationTask;
                for (i = 0; i < sourceTasks.size(); i++) {
                    task = (Task) sourceTasks.get(i);
                    if (i == 0) {
                        copyTaskRecursively(task, prevTask, editActionDetail.isDestinationExpanded(), sortedTasks, null, true);
                    } else {
                        copyTaskRecursively(task, prevTask, false, sortedTasks, null, true);
                    }
                    prevTask = task;
                }
                break;
        }
        AppUtil.scheduleAllAlarms(ConfigManager.getInstance().getContext());
        return ERROR.NO_ERROR;
    }

    public List<Task> getCleanTasksListForEditing(List<Task> tasksToClean, List<Task> sortedTasks) {
        List<Task> cleanTasks = getSubSortedTasks(tasksToClean, sortedTasks);
        List<Task> childrenAndGrandChildrenTasks = new ArrayList();
        for (Task task : cleanTasks) {
            if (task.isParent() && !isTaskInList(task, childrenAndGrandChildrenTasks)) {
                childrenAndGrandChildrenTasks.addAll(getTaskChildrenAndGrandChildren(task, sortedTasks));
            }
        }
        for (Task task2 : childrenAndGrandChildrenTasks) {
            removeTaskFromList(task2, cleanTasks);
        }
        return cleanTasks;
    }

    private List<Task> getSubSortedTasks(List<Task> tasksToClean, List<Task> sortedTasks) {
        List<Task> subSortedTasks = new ArrayList();
        for (Task task : sortedTasks) {
            if (isTaskInList(task, tasksToClean)) {
                subSortedTasks.add(task);
            }
        }
        return subSortedTasks;
    }

    private void removeTaskFromList(Task task, List<Task> sourceTasks) {
        for (Task task2 : sourceTasks) {
            if (task2.getId().equals(task.getId())) {
                sourceTasks.remove(task2);
                return;
            }
        }
    }

    public List<Task> getTaskChildrenAndGrandChildren(Task task, List<Task> sortedTasks) {
        List<Task> taskChildrenAndGrandChildrens = new ArrayList();
        getTaskChildrenAndGrandChildrensRecursively(task, sortedTasks, taskChildrenAndGrandChildrens);
        return taskChildrenAndGrandChildrens;
    }

    private void getTaskChildrenAndGrandChildrensRecursively(Task task, List<Task> sortedTasks, List<Task> taskChildrenAndGrandChildrens) {
        for (Task task2 : AppUtil.getTaskChildren(task, sortedTasks)) {
            taskChildrenAndGrandChildrens.add(task2);
            getTaskChildrenAndGrandChildrensRecursively(task2, sortedTasks, taskChildrenAndGrandChildrens);
        }
    }

    public void multipleDeletes(EditActionDetail editActionDetail, List<Task> sortedTasks) {
        performMultipleDeletes(sortedTasks, editActionDetail.getSelectedTasks());
        AppUtil.scheduleAllAlarms(ConfigManager.getInstance().getContext());
    }

    private void performMultipleDeletes(List<Task> sortedTasks, List<Task> tasksToDelete) {
        tasksToDelete = getCleanTasksListForEditing(tasksToDelete, sortedTasks);
        for (int i = tasksToDelete.size() - 1; i >= 0; i--) {
            deleteRecursively((Task) tasksToDelete.get(i), sortedTasks);
        }
    }

    private boolean isTaskInList(Task task, List<Task> tasks) {
        for (Task task2 : tasks) {
            if (task2.getId().equals(task.getId())) {
                return true;
            }
        }
        return false;
    }

    public void deleteRecursively(Task task, List<Task> sortedTasks) {
        boolean isTaskParent = task.isParent();
        List<Task> taskChildren = new ArrayList();
        if (isTaskParent) {
            taskChildren = AppUtil.getTaskChildren(task, sortedTasks);
        }
        delete(task);
        if (isTaskParent) {
            for (Task task2 : taskChildren) {
                deleteRecursively(task2, sortedTasks);
            }
        }
        AppUtil.scheduleAllAlarms(ConfigManager.getInstance().getContext());
    }

    public void expandCollapseBranchAllChildren(Map<String, Long> mExpandedTasks, Task task, List<Task> sortedTasks, Integer level, boolean isTaskExpanded) {
        if (task.isParent()) {
            if (isTaskExpanded) {
                if (task.getLevel().intValue() < level.intValue()) {
                    mExpandedTasks.put(AppUtil.getExpandedTaskId(task), Constants.TASK_COLLAPSE_INDICATOR);
                } else {
                    mExpandedTasks.remove(AppUtil.getExpandedTaskId(task));
                }
            } else if (task.getLevel().intValue() >= level.intValue()) {
                mExpandedTasks.put(AppUtil.getExpandedTaskId(task), Constants.TASK_EXPAND_INDICATOR);
            } else {
                mExpandedTasks.remove(AppUtil.getExpandedTaskId(task));
            }
            List<Task> tasks;
            if (task.isTasksListAsTask()) {
                tasks = getTaskChildrenAndGrandChildrenForList(task, sortedTasks);
            } else {
                tasks = getTaskChildrenAndGrandChildren(task, sortedTasks);
            }
            for (Task task2 : tasks) {
                if (task2.isParent()) {
                    if (isTaskExpanded) {
                        if (task2.getLevel().intValue() < level.intValue()) {
                            mExpandedTasks.put(AppUtil.getExpandedTaskId(task2), Constants.TASK_COLLAPSE_INDICATOR);
                        } else {
                            mExpandedTasks.remove(AppUtil.getExpandedTaskId(task2));
                        }
                    } else if (task2.getLevel().intValue() >= level.intValue()) {
                        mExpandedTasks.put(AppUtil.getExpandedTaskId(task2), Constants.TASK_EXPAND_INDICATOR);
                    } else {
                        mExpandedTasks.remove(AppUtil.getExpandedTaskId(task2));
                    }
                }
            }
        }
    }

    private List<Task> getTaskChildrenAndGrandChildrenForList(Task task, List<Task> sortedTasks) {
        List<Task> tasks = new ArrayList();
        for (Task task2 : sortedTasks) {
            if (task2.getTasksListId().equals(task.getTasksListId())) {
                tasks.add(task2);
            }
        }
        return tasks;
    }

    public void copyTaskRecursively(Task sourceTask, Task targetTask, boolean isTargetExpanded, List<Task> sortedTasks, Long crossTasksListId, boolean isFirstLoop) {
        Long sourceTaskId = sourceTask.getId();
        boolean isTargetTasksListAsTask = targetTask.isTasksListAsTask();
        boolean isSourceTaskParent = sourceTask.isParent();
        List<Task> sourceTaskChildren = new ArrayList();
        if (isSourceTaskParent) {
            sourceTaskChildren = AppUtil.getTaskChildren(sourceTask, sortedTasks);
        }
        sourceTask.setId(null);
        sourceTask.setGtaskId(null);
        if (crossTasksListId != null) {
            sourceTask.setTasksListId(crossTasksListId);
        } else {
            sourceTask.setTasksListId(targetTask.getTasksListId());
        }
        if (isTargetTasksListAsTask) {
            sourceTask.setParentTaskId(Long.valueOf(0));
            sourceTask.setPriorSiblingTaskId(Constants.NO_SIBLING_ID);
        } else if (isFirstLoop && isTargetExpanded) {
            sourceTask.setParentTaskId(targetTask.getId());
            sourceTask.setPriorSiblingTaskId(Constants.NO_SIBLING_ID);
        } else if (isTargetExpanded && sourceTask.getPriorSiblingTaskId().equals(Constants.NO_SIBLING_ID)) {
            sourceTask.setParentTaskId(targetTask.getId());
            sourceTask.setPriorSiblingTaskId(Constants.NO_SIBLING_ID);
        } else {
            sourceTask.setParentTaskId(targetTask.getParentTaskId());
            sourceTask.setPriorSiblingTaskId(targetTask.getId());
        }
        saveOrUpdate(sourceTask, null);
        copyContextTasks(sourceTask, sourceTaskId);
        copyTagTasks(sourceTask, sourceTaskId);
        if (isSourceTaskParent) {
            Task prevTask = sourceTask;
            for (Task task : sourceTaskChildren) {
                copyTaskRecursively(task, prevTask, prevTask.isParent(), sortedTasks, crossTasksListId, false);
                prevTask = task;
            }
        }
    }

    private void copyTagTasks(Task sourceTask, Long sourceTaskId) {
        for (TagTask tagTask : this.tagTaskDao.getAllTagTasksByTaskId(sourceTaskId)) {
            tagTask.setTaskId(sourceTask.getId());
            this.tagTaskDao.save(tagTask);
        }
    }

    private void copyContextTasks(Task sourceTask, Long sourceTaskId) {
        for (ContextTask contextTask : this.contextTaskDao.getAllByTaskId(sourceTaskId)) {
            contextTask.setTaskId(sourceTask.getId());
            this.contextTaskDao.save(contextTask);
        }
    }

    public List<Task> getAllTasksWithEligibleAlarms() {
        return this.taskDao.getAllTasksWithEligibleAlarms();
    }

    public List<Task> getAllTasksWithEligibleAlarmsIncludingInvisibleAccounts() {
        return this.taskDao.getAllTasksWithEligibleAlarmsIncludingInvisibleAccounts();
    }

    private Task getNextParentSibling(Task task, List<Task> sortedTasks) {
        for (Task task2 : sortedTasks) {
            if (!task2.getPriorSiblingTaskId().equals(Constants.NO_SIBLING_ID) && task2.getPriorSiblingTaskId().equals(task.getParentTaskId())) {
                return task2;
            }
        }
        return null;
    }

    private Task getNextSibling(Task task, List<Task> sortedTasks) {
        boolean isTaskFound = false;
        for (Task task2 : sortedTasks) {
            if (task2.getId().equals(task.getId())) {
                isTaskFound = true;
            }
            if (isTaskFound && task2.isTasksListAsTask()) {
                return task2;
            }
            if (task2.getPriorSiblingTaskId().equals(task.getId())) {
                return task2;
            }
        }
        return null;
    }

    private void convertNextSiblingsToChildren(Task task, List<Task> sortedTasks) {
        boolean isStartConvertingSiblings = false;
        for (Task task2 : sortedTasks) {
            if (task2.getPriorSiblingTaskId().equals(task.getId())) {
                task2.setParentTaskId(task.getId());
                task2.setPriorSiblingTaskId(Long.valueOf(0));
                task2.setMoved(Boolean.valueOf(true));
                this.taskDao.saveOrUpdateWithTimeStamp(task2);
                isStartConvertingSiblings = true;
            } else if (isStartConvertingSiblings && task2.getParentTaskId().equals(task.getParentTaskId())) {
                task2.setParentTaskId(task.getId());
                task2.setMoved(Boolean.valueOf(true));
                this.taskDao.saveOrUpdateWithTimeStamp(task2);
            }
        }
    }

    List<Task> getTasksWithSortedInfo(List<Task> tasks, TasksList tasksList) {
        ConfigManager config = ConfigManager.getInstance();
        LevelState levelState = null;
        boolean isZoomSet = false;
        if (isAllTasksList(tasksList)) {
            tasksList = DatabaseHelper.createTasksListCalledAll();
        } else {
            levelState = this.taskStateBp.getLevelSate(tasksList.getId(), null);
            isZoomSet = AppUtil.isZoomSet(levelState);
        }
        List<Task> sortedTasks = getTasksByLevel(null, 20, true, tasksList, true);
        if (isZoomSet) {
            sortedTasks = getTaskChildrenAndGrandChildren(get(new Long(levelState.getZoomTaskIdStr())), sortedTasks);
        }
        List<Task> tasksWithSortedInfo = new ArrayList();
        for (Task task : tasks) {
            if (!config.isHideCompletedTasks() || !task.isCompleted().booleanValue()) {
                Task taskWithInfo = taskWithSortedInfo(task, sortedTasks);
                if (taskWithInfo != null) {
                    tasksWithSortedInfo.add(taskWithInfo);
                }
            }
        }
        return tasksWithSortedInfo;
    }

    private Task taskWithSortedInfo(Task task, List<Task> sortedTasks) {
        for (Task task2 : sortedTasks) {
            if (task2.getId().equals(task.getId())) {
                task2.setConsideredAsFloat(task.isConsideredAsFloat());
                return task2;
            }
        }
        return null;
    }

    private void moveSiblingsAndChildrenUp(Task task, boolean isSiblingOnly) {
        for (Task currentTask : this.taskDao.getAllTasksByTasksListId(task.getTasksListId(), null, false, false, false)) {
            if (currentTask.getParentTaskId().equals(task.getParentTaskId()) && currentTask.getPriorSiblingTaskId().equals(task.getId())) {
                currentTask.setPriorSiblingTaskId(task.getPriorSiblingTaskId());
                currentTask.setMoved(Boolean.valueOf(true));
                this.taskDao.saveOrUpdateWithTimeStamp(currentTask);
            } else if (!isSiblingOnly) {
                if (currentTask.getParentTaskId().equals(task.getId()) && currentTask.getPriorSiblingTaskId().equals(Long.valueOf(0))) {
                    currentTask.setParentTaskId(task.getParentTaskId());
                    currentTask.setPriorSiblingTaskId(task.getPriorSiblingTaskId());
                    currentTask.setMoved(Boolean.valueOf(true));
                    this.taskDao.saveOrUpdateWithTimeStamp(currentTask);
                } else if (currentTask.getParentTaskId().equals(task.getId()) && !currentTask.getPriorSiblingTaskId().equals(Long.valueOf(0))) {
                    currentTask.setParentTaskId(task.getParentTaskId());
                    currentTask.setMoved(Boolean.valueOf(true));
                    this.taskDao.saveOrUpdateWithTimeStamp(currentTask);
                }
            }
        }
    }

    private void moveSiblingDown(Task task) {
        for (Task currentTask : this.taskDao.getAllTasksByTasksListId(task.getTasksListId(), null, false, false, false)) {
            if (!currentTask.getId().equals(task.getId()) && currentTask.getParentTaskId().equals(task.getParentTaskId()) && currentTask.getPriorSiblingTaskId().equals(task.getPriorSiblingTaskId())) {
                currentTask.setBothPriorSiblingTaskFromTaskId(task);
                currentTask.setMoved(Boolean.valueOf(true));
                this.taskDao.saveOrUpdateWithTimeStamp(currentTask);
                return;
            }
        }
    }

    private Task getTasksListAsTask(TasksList tasksList, int taskNumber) {
        Task listAsTask = new Task();
        listAsTask.setName(tasksList.getName());
        listAsTask.setId(Long.valueOf(0));
        listAsTask.setTasksListId(tasksList.getId());
        listAsTask.setTasksListAsTask(true);
        listAsTask.setIndention("");
        listAsTask.setParent(true);
        listAsTask.setLevel(Integer.valueOf(1));
        listAsTask.setTaskNumber(Integer.valueOf(taskNumber));
        return listAsTask;
    }
}
