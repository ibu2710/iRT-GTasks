package com.irt.tasks.app.common;

public final class Encrypt {
    private static final String CIPHER_STRING = "\rhJ*6k-@,F:^fz\"4L%}BSwd_!U{(y#Xa3t8M;c9Rbs~lWxTij<A$VPq>n[5\\p+Y|oO1uHZ r'm?NgQvD:72&/G).I\n]K0EC=e\t";
    private static final String USAGE = "Usage: Encrypt e <plaintext>\n or Encrypt d <encrypted>";

    public static String decrypt(String source) {
        Assert.notNull(source, "source");
        if (source.equals("")) {
            return "";
        }
        String keystr = source.substring(0, 3);
        String encstr = source.substring(3, source.length());
        int key = (new Integer(keystr).intValue() - 7) / 2;
        StringBuffer sb = new StringBuffer();
        int len = CIPHER_STRING.length();
        int addTo = len * 30;
        for (int i = 0; i < encstr.length(); i += 2) {
            sb.append(CIPHER_STRING.charAt((((Integer.parseInt(encstr.substring(i, i + 2)) + addTo) - (i / 2)) - key) % len));
        }
        return sb.toString();
    }

    public static String encrypt(String source) {
        Assert.notNull(source, "source");
        if (source.equals("")) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        int key = getKey();
        int len = CIPHER_STRING.length();
        sb.append((key * 2) + 7);
        for (int i = 0; i < source.length(); i++) {
            sb.append(fixWidth(((CIPHER_STRING.indexOf(source.charAt(i)) + key) + i) % len));
        }
        return sb.toString();
    }

    private static String fixWidth(int x) {
        if (x < 10) {
            return "0" + x;
        }
        return x + "";
    }

    private static int getKey() {
        int i = ((int) (255 & System.currentTimeMillis())) + 100;
        if (i % CIPHER_STRING.length() == 0) {
            return i + 5;
        }
        return i;
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println(USAGE);
        } else if (args[0].equalsIgnoreCase("e")) {
            System.out.println(encrypt(args[1]));
        } else if (args[0].equalsIgnoreCase("d")) {
            System.out.println(decrypt(args[1]));
        } else {
            System.out.println(USAGE);
        }
    }

    private Encrypt() {
    }
}
