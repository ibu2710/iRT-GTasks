package com.irt.tasks.app.framework.trial;

public class ExpirationDateHelper {
    static final int EXPIRATION_DAY = 31;
    static final int EXPIRATION_MONTH = 11;
    static final int EXPIRATION_YEAR = 2218;
}
