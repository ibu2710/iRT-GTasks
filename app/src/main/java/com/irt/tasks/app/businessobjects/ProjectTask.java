package com.irt.tasks.app.businessobjects;

import com.irt.tasks.app.framework.business.IrtTasksBo;

public class ProjectTask implements IrtTasksBo {
    public static final String PROJECT_ID = "projectId";
    public static final String TASK_ID = "taskId";
    private Long projectId;
    private Long taskId;

    public Long getProjectId() {
        return this.projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getTaskId() {
        return this.taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
}
