package com.irt.tasks.app.dataaccess;

import android.database.Cursor;
import com.irt.tasks.app.businessobjects.Tag;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.dataaccess.adapter.TagDBAdapter;
import com.irt.tasks.app.dataaccess.adapter.TagDBAdapterImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.filter.TasksFilterManager;
import java.util.ArrayList;
import java.util.List;

public class TagDaoImpl implements TagDao {
    private ConfigManager config;

    public List<Tag> getAll() {
        TagDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAll();
        List<Tag> tags = buildTags(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tags;
    }

    public void saveOrUpdate(Tag tag) {
        TagDBAdapter dBAdapter = openDbAdapter();
        if (tag.getId() == null) {
            tag.setId(Long.valueOf(dBAdapter.insert(tag)));
        } else {
            dBAdapter.update(tag);
        }
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    public void delete(Tag tag) {
        TasksFilterManager.getInstance().removeTagId(tag.getId());
        TagDBAdapter dBAdapter = openDbAdapter();
        dBAdapter.delete(tag.getId().longValue());
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    public boolean isNameUsed(String name, Long tagId) {
        TagDBAdapter dBAdapter = openDbAdapter();
        boolean isNameUsed = dBAdapter.isNameUsed(name, tagId);
        closeDbAdapter(dBAdapter);
        return isNameUsed;
    }

    private List<Tag> buildTags(Cursor cursor) {
        List<Tag> tags = new ArrayList();
        while (cursor.moveToNext()) {
            tags.add(buildTag(cursor));
        }
        return tags;
    }

    private Tag buildTag(Cursor cursor) {
        Long id = Long.valueOf(cursor.getLong(0));
        String name = cursor.getString(1);
        Tag tag = new Tag();
        tag.setId(id);
        tag.setName(name);
        return tag;
    }

    private TagDBAdapter openDbAdapter() {
        this.config = ConfigManager.getInstance();
        TagDBAdapter dBAdapter = new TagDBAdapterImpl(this.config.getContext());
        dBAdapter.open();
        return dBAdapter;
    }

    private void closeDbAdapter(TagDBAdapter dBAdapter) {
        dBAdapter.close();
    }
}
