package com.irt.tasks.app.businessprocess;

import android.os.Environment;

import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.CSVWriter;
import com.irt.tasks.app.common.Constants;
import com.irt.tasks.app.common.DateUtil;
import com.irt.tasks.app.common.Globals;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;



public class ExternalApplicationsBpHelper {
    public static final String APPLICATION_DIRECTORY = ("/data/" + AppUtil.getPackageName() + Globals.FORWARDSLASH);
    public static final String APPLICATION_FILES_DIRECTORY = (APPLICATION_DIRECTORY + "files/");
    public static final int BONSAI_FIELD_CATEGORY = 11;
    public static final int BONSAI_FIELD_COLUMN = 3;
    public static final int BONSAI_FIELD_COMPLETED_DATE = 10;
    public static final int BONSAI_FIELD_CONTACT = 13;
    public static final int BONSAI_FIELD_CREATION_DATE = 7;
    public static final int BONSAI_FIELD_DUE_DATE = 9;
    public static final int BONSAI_FIELD_ID = 1;
    public static final int BONSAI_FIELD_KEYWORDS = 12;
    public static final int BONSAI_FIELD_NAME = 0;
    public static final int BONSAI_FIELD_NOTES = 14;
    public static final int BONSAI_FIELD_PERCENT_COMPLETE = 4;
    public static final int BONSAI_FIELD_PRIORITY = 5;
    public static final int BONSAI_FIELD_START_DATE = 8;
    public static final int BONSAI_FIELD_UNKNOWN01 = 2;
    public static final int BONSAI_FIELD_UNKNOWN02 = 6;
    public static final String EXPORT_TEMP_FILE_NAME = "export.txt";

    public static void bonsaiCsvToTask(TasksList newTasksList, Task previousTask, Map<Integer, Task> levelToTaskMap, String[] taskFields, Task task, boolean isContactsTaskId) {
        boolean z = false;
        if (isContactsTaskId) {
            long taskId = getBonsaiLongField(taskFields[13]);
            if (taskId > 0) {
                task.setId(Long.valueOf(taskId));
            }
        }
        task.setTasksListId(newTasksList.getId());
        task.setName(getBonsaiStringField(taskFields[0]));
        setParentTaskIdAndSibling(task, previousTask, getBonsaiIntField(taskFields[3]), levelToTaskMap);
        if (getBonsaiIntField(taskFields[4]) == 100) {
            z = true;
        }
        task.setCompleted(Boolean.valueOf(z));
        task.setPriority(Integer.valueOf(getBonsaiIntField(taskFields[5])));
        task.setCreationDate(getBonsaiDateFieldMillis(taskFields[10]));
        task.setNote(getBonsaiStringField(taskFields[14]));
    }

    private static Long getBonsaiDateFieldMillis(String string) {
        try {
            return Long.valueOf(DateUtil.parseDate(getBonsaiStringField(string), Constants.DATE_FORMAT_DDMMYYYY).getTime());
        } catch (Exception e) {
            return Long.valueOf(0);
        }
    }

    private static void setParentTaskIdAndSibling(Task task, Task previousTask, int column, Map<Integer, Task> levelToTaskMap) {
        task.setLevel(Integer.valueOf(column));
        if (column == previousTask.getLevel().intValue()) {
            task.setParentTaskId(previousTask.getParentTaskId());
            task.setPriorSiblingTaskId(previousTask.getId());
        } else if (column > previousTask.getLevel().intValue()) {
            task.setParentTaskId(previousTask.getId());
            task.setPriorSiblingTaskId(Long.valueOf(0));
        } else {
            Task taskPrev = (Task) levelToTaskMap.get(task.getLevel());
            task.setParentTaskId(taskPrev.getParentTaskId());
            task.setPriorSiblingTaskId(taskPrev.getId());
        }
    }

    private static int getBonsaiIntField(String string) {
        try {
            return Integer.parseInt(getBonsaiStringField(string));
        } catch (Exception e) {
            return 0;
        }
    }

    private static long getBonsaiLongField(String string) {
        try {
            return Long.parseLong(getBonsaiStringField(string));
        } catch (Exception e) {
            return 0;
        }
    }

    private static String getBonsaiStringField(String string) {
        return string.replaceAll(Globals.QUOTE, "");
    }

    public static String getTempFilePath() {
        return Environment.getDataDirectory().getAbsolutePath() + APPLICATION_FILES_DIRECTORY + EXPORT_TEMP_FILE_NAME;
    }

    public static void writeTaskToBonsaiCsvFile(List<Task> tasks, String dateFormat, boolean isTaskIdInContact) {
        try {
            CSVWriter csvWriter = new CSVWriter(new BufferedWriter(new FileWriter(getTempFilePath())));
            for (Task task : tasks) {
                String[] csvFields = new String[15];
                csvFields[0] = task.getName();
                csvFields[1] = "0";
                csvFields[2] = "1.0";
                csvFields[3] = (task.getLevel().intValue() - 1) + "";
                csvFields[4] = task.isCompleted().booleanValue() ? "100" : "0";
                csvFields[5] = "2";
                csvFields[6] = "0";
                csvFields[7] = getFormattedBonsaiDate(task.getCreationDate().longValue(), dateFormat);
                csvFields[8] = "";
                csvFields[9] = getFormattedBonsaiDate(task.getDueDate().longValue(), dateFormat);
                csvFields[10] = getFormattedBonsaiDate(task.getLastContentModifiedDate().longValue(), dateFormat);
                csvFields[11] = "Unfiled";
                csvFields[12] = "UNKNOWN05";
                if (isTaskIdInContact) {
                    csvFields[13] = task.getId() + "";
                }
                csvFields[14] = task.getNote();
                csvWriter.writeNext(csvFields);
                csvWriter.flush();
            }
            csvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getFormattedBonsaiDate(long dateMillis, String dateFormat) {
        return dateMillis == 0 ? "" : DateUtil.format(new Date(dateMillis), dateFormat);
    }
}
