package com.irt.tasks.app.dataaccess.adapter;

import android.database.Cursor;
import android.database.SQLException;
import com.irt.tasks.app.framework.business.IrtTasksBo;

public interface DBAdapter {
    void close();

    boolean delete(long j);

    Cursor get(long j) throws SQLException;

    Cursor getAll();

    long insert(IrtTasksBo irtTasksBo);

    DBAdapter open() throws SQLException;

    boolean update(IrtTasksBo irtTasksBo);
}
