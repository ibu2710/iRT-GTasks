package com.irt.tasks.app.framework.business;

import android.app.Application;
import android.content.Intent;
import android.content.res.Configuration;
import com.google.android.gms.drive.DriveFile;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.ErrorReporter;
import com.irt.tasks.app.framework.config.ConfigManager;
import com.irt.tasks.ui.preferences.TasksPreferences;
import com.irt.tasks.ui.receiver.AlarmSetterHelper;
import com.irt.tasks.ui.sync.DropboxSyncHelper;

public class TasksApplication extends Application {
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void onCreate() {
        super.onCreate();
        ConfigManager config = ConfigManager.getInstance(getApplicationContext());
        initErrorReporter();
        AppUtil.checkForProVersion();
        AlarmSetterHelper.resetAllAlarms(config.getContext());
        config.setWifiConnected(Boolean.valueOf(AppUtil.isWifiConnected()));
        DropboxSyncHelper.backendRequestSyncWithDelay(1);
        checkForNewRelease(config);
    }

    private void checkForNewRelease(ConfigManager config) {
        if (!AppUtil.getVersionName().equals(config.getApplicationVersion())) {
            Intent intent = new Intent(this, TasksPreferences.class);
            intent.addFlags(DriveFile.MODE_READ_ONLY);
            startActivity(intent);
        }
    }

    private void initErrorReporter() {
        new ErrorReporter().Init(this);
    }

    public void onLowMemory() {
        super.onLowMemory();
    }

    public void onTerminate() {
        super.onTerminate();
    }
}
