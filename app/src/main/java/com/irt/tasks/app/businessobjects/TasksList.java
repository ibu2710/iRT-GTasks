package com.irt.tasks.app.businessobjects;

import com.irt.tasks.app.framework.business.IrtTasksBo;

public class TasksList implements IrtTasksBo {
    public static final String ACCOUNT = "account";
    public static final String ACCOUNT_ID = "accountId";
    public static final String ACTIVE_TASKS_LIST = "activeTasksList";
    public static final String AUTO_SYNC = "autoSync";
    public static final String COLOR_TASKS = "colorTaks";
    public static final String DELETED = "deleted";
    public static final String EXCLUDE_FROM_DELETE = "excludeFromDelete";
    public static final String FONT_BACKGROUND_COLOR = "fontBgColor";
    public static final String FONT_COLOR = "fontColor";
    public static final String FONT_STYLE = "fontStyle";
    public static final String GTASKS_LIST_ID = "gtasksListId";
    public static final String GTASKS_LIST_NAME = "gtasksListName";
    public static final String HIGHLIGHT = "highlight";
    public static final String ID = "id";
    public static final String IN_TRASH_BIN = "inTrashBin";
    public static final String LAST_MODIFIED = "lastModified";
    public static final String NAME = "name";
    public static final String NEW_TASK_POSITION = "newTaskPosition";
    public static final String NOTE = "note";
    public static final String SECURE = "secure";
    public static final String SHOW_DUE_DATE = "showDueDate";
    public static final String SHOW_NOTE = "showNote";
    public static final String SORT_POSITION = "sortPosition";
    public static final String TASK_LAST_MODIFIED = "taskLastModified";
    public static final String TOTALS = "totals";
    public static final String VISIBLE = "visible";
    private Account account;
    private Long accountId;
    private Boolean activeTasksList = Boolean.valueOf(false);
    private Boolean autoSync = Boolean.valueOf(true);
    private Boolean colorTasks = Boolean.valueOf(true);
    private Boolean deleted = Boolean.valueOf(false);
    private Boolean excludeFromDelete = Boolean.valueOf(false);
    private Integer fontBgColor = Integer.valueOf(0);
    private Integer fontColor = Integer.valueOf(0);
    private Integer fontStyle = Integer.valueOf(0);
    private String gtasksListId;
    private String gtasksListName;
    private Boolean highlight = Boolean.valueOf(false);
    private Long id;
    private Boolean inTrashBin = Boolean.valueOf(false);
    private Long lastModified = Long.valueOf(0);
    private String name;
    private Integer newTaskPosition = Integer.valueOf(0);
    private String note;
    private Boolean secure = Boolean.valueOf(false);
    private Boolean showDueDate = Boolean.valueOf(true);
    private Boolean showNote = Boolean.valueOf(true);
    private Integer sortPosition = Integer.valueOf(0);
    private Long taskLastModified = Long.valueOf(0);
    private String totals;
    private Boolean visible = Boolean.valueOf(true);

    public Boolean isExcludeFromDelete() {
        return this.excludeFromDelete;
    }

    public void setExcludeFromDelete(Boolean excludeFromDelete) {
        this.excludeFromDelete = excludeFromDelete;
    }

    public Boolean isColorTasks() {
        return this.colorTasks;
    }

    public void setColorTasks(Boolean colorTasks) {
        this.colorTasks = colorTasks;
    }

    public Boolean isHighlight() {
        return this.highlight;
    }

    public void setHighlight(Boolean highlight) {
        this.highlight = highlight;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getSortPosition() {
        return this.sortPosition;
    }

    public void setSortPosition(Integer sortPosition) {
        this.sortPosition = sortPosition;
    }

    public String getGtasksListId() {
        return this.gtasksListId;
    }

    public void setGtasksListId(String gtasksListId) {
        this.gtasksListId = gtasksListId;
    }

    public Long getLastModified() {
        return this.lastModified;
    }

    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

    public String getGtasksListName() {
        return this.gtasksListName;
    }

    public void setGtasksListName(String gtasksListName) {
        this.gtasksListName = gtasksListName;
    }

    public Long getAccountId() {
        return this.accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Integer getFontStyle() {
        return this.fontStyle;
    }

    public void setFontStyle(Integer fontStyle) {
        this.fontStyle = fontStyle;
    }

    public Integer getFontColor() {
        return this.fontColor;
    }

    public void setFontColor(Integer fontColor) {
        this.fontColor = fontColor;
    }

    public Integer getFontBgColor() {
        return this.fontBgColor;
    }

    public void setFontBgColor(Integer fontBgColor) {
        this.fontBgColor = fontBgColor;
    }

    public String toString() {
        return "name = [" + this.name + "], id = [" + this.id + "], gtasksListId = [" + this.gtasksListId + "]";
    }

    public Boolean isInTrashBin() {
        return this.inTrashBin;
    }

    public void setInTrashBin(Boolean inTrashBin) {
        this.inTrashBin = inTrashBin;
    }

    public Account getAccount() {
        return this.account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Boolean isAutoSync() {
        return this.autoSync;
    }

    public void setAutoSync(Boolean autoSync) {
        this.autoSync = autoSync;
    }

    public Boolean isVisible() {
        return this.visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Long getTaskLastModified() {
        return this.taskLastModified;
    }

    public void setTaskLastModified(Long taskLastModified) {
        this.taskLastModified = taskLastModified;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTotals() {
        return this.totals;
    }

    public void setTotals(String totals) {
        this.totals = totals;
    }

    public Boolean istActiveTasksList() {
        return this.activeTasksList;
    }

    public void setActiveTasksList(Boolean activeTasksList) {
        this.activeTasksList = activeTasksList;
    }

    public Boolean isShowNote() {
        return this.showNote;
    }

    public void setShowNote(Boolean showNote) {
        this.showNote = showNote;
    }

    public Boolean isShowDueDate() {
        return this.showDueDate;
    }

    public void setShowDueDate(Boolean showDueDate) {
        this.showDueDate = showDueDate;
    }

    public Integer getNewTaskPosition() {
        return this.newTaskPosition;
    }

    public void setNewTaskPosition(Integer newTaskPosition) {
        this.newTaskPosition = newTaskPosition;
    }

    public Boolean isSecure() {
        return this.secure;
    }

    public void setSecure(Boolean secure) {
        this.secure = secure;
    }
}
