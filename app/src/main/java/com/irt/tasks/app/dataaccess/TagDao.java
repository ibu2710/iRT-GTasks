package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.Tag;
import java.util.List;

public interface TagDao {
    void delete(Tag tag);

    List<Tag> getAll();

    boolean isNameUsed(String str, Long l);

    void saveOrUpdate(Tag tag);
}
