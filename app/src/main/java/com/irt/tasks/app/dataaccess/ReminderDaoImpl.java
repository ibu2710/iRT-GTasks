package com.irt.tasks.app.dataaccess;

import android.database.Cursor;
import com.irt.tasks.app.businessobjects.Reminder;
import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.dataaccess.adapter.ReminderDBAdapter;
import com.irt.tasks.app.dataaccess.adapter.ReminderDBAdapterImpl;
import com.irt.tasks.app.framework.config.ConfigManager;
import java.util.ArrayList;
import java.util.List;

public class ReminderDaoImpl implements ReminderDao {
    ConfigManager config;

    public List<Reminder> getAll() {
        ReminderDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAll();
        List<Reminder> tasks = buildReminders(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasks;
    }

    public List<Reminder> getAllByTasksListId(Long tasksListId) {
        ReminderDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.getAllByTasksListId(tasksListId);
        List<Reminder> tasks = buildReminders(cursor);
        cursor.close();
        closeDbAdapter(dBAdapter);
        return tasks;
    }

    public void saveOrUpdate(Reminder reminder) {
        ReminderDBAdapter dBAdapter = openDbAdapter();
        Cursor cursor = dBAdapter.get(reminder.getTaskId().longValue());
        if (cursor.getCount() == 0) {
            dBAdapter.insert(reminder);
        } else {
            dBAdapter.update(reminder);
        }
        cursor.close();
        closeDbAdapter(dBAdapter);
        AppUtil.setLastApplicationModifiedDate();
    }

    private List<Reminder> buildReminders(Cursor cursor) {
        List<Reminder> reminders = new ArrayList();
        while (cursor.moveToNext()) {
            reminders.add(buildReminder(cursor));
        }
        return reminders;
    }

    private Reminder buildReminder(Cursor cursor) {
        Long taskId = Long.valueOf(cursor.getLong(0));
        Boolean enabled = AppUtil.isNumberOne(cursor.getInt(1));
        Integer daysEarlier = Integer.valueOf(cursor.getInt(2));
        Long dateTime = Long.valueOf(cursor.getLong(3));
        Boolean repeatFromCompletionDate = AppUtil.isNumberOne(cursor.getInt(4));
        String repeatRule = cursor.getString(5);
        Reminder reminder = new Reminder();
        reminder.setTaskId(taskId);
        reminder.setEnabled(enabled);
        reminder.setDaysEarlier(daysEarlier);
        reminder.setDateTime(dateTime);
        reminder.setRepeatFromCompletionDate(repeatFromCompletionDate);
        reminder.setRepeatRule(repeatRule);
        return reminder;
    }

    private ReminderDBAdapter openDbAdapter() {
        this.config = ConfigManager.getInstance();
        ReminderDBAdapter dBAdapter = new ReminderDBAdapterImpl(this.config.getContext());
        dBAdapter.open();
        return dBAdapter;
    }

    private void closeDbAdapter(ReminderDBAdapter dBAdapter) {
        dBAdapter.close();
    }
}
