package com.irt.tasks.app.businessobjects;

import com.irt.tasks.app.common.AppUtil;
import com.irt.tasks.app.common.Constants.FILTER_PARAMETER_TYPES;
import com.irt.tasks.app.common.StringUtil;
import com.irt.tasks.app.framework.business.IrtTasksBo;
import java.util.Comparator;
import java.util.List;

public class Task implements IrtTasksBo {
    public static final String AUTO_SNOOZE_DURATION = "autoSnoozeDuration";
    public static final String COMPLETED = "completed";
    public static final String COMPLETION_DATE = "completionDate";
    public static final String CONSIDERED_AS_FLOAT = "consideredAsFloat";
    public static final String CREATION_DATE = "creationDate";
    public static final String CUT = "cut";
    public static final String DELETED = "deleted";
    public static final String DUE_DATE = "dueDate";
    public static final String EXCLUDE_FROM_DELETE = "excludeFromDelete";
    public static final String EXPANDED = "expanded";
    public static final String EXTRA_EMAILS = "extraEmails";
    public static final String FONT_BACKGROUND_COLOR = "fontBgColor";
    public static final String FONT_COLOR = "fontColor";
    public static final String FONT_STYLE = "fontStyle";
    public static final String GTASKS_LIST_ID = "gtasksListId";
    public static final String GTASK_ID = "gtaskId";
    public static final String GTASK_NEED_UPDATE = "gtaskNeedUpdate";
    public static final String HIGHLIGHT = "highlight";
    public static final String ICON_NAME = "iconName";
    public static final String INDENTION = "indention";
    public static final String IN_TRASH_BIN = "inTrashBin";
    public static final String IS_FLOAT = "isFloat";
    public static final String LAST_CONTENT_MODIFIED_DATE = "lastContentModifiedDate";
    public static final String LAST_MODIFIED = "lastModified";
    public static final String LEVEL = "level";
    public static final String MARK_AS_TODAY = "markAsToday";
    public static final String MOVED = "moved";
    public static final String NAME = "name";
    public static final String NOTE = "note";
    public static final String NOTIFY_BY_EMAIL = "notifyByEmail";
    public static final String NOTIFY_EXTRA_EMAILS = "notifyExtraEmails";
    public static final String PARENT = "parent";
    public static final String PARENT_GTASK_ID = "parentGtaskId";
    public static final String PARENT_TASK_ID = "parentTaskId";
    public static final String POSITION = "position";
    public static final String PRIORITY = "priority";
    public static final String PRIOR_SIBLING_GTASK_ID = "priorSiblingGTaskId";
    public static final String PRIOR_SIBLING_TASK_ID = "priorSiblingTaskId";
    public static final String PROJECT_ID = "projectId";
    public static final String REMINDER_DATE_TIME = "reminderDateTime";
    public static final String REMINDER_DAYS_EARLIER = "reminderDaysEarlier";
    public static final String REMINDER_ENABLED = "reminderEnabled";
    public static final String REMINDER_REPEAT_FROM_COMPLETION_DATE = "reminderRepeatFromCompletionDate";
    public static final String REMINDER_REPEAT_RULE = "reminderRepeatRule";
    public static final String ROW_ID = "id";
    public static final String SELECTED = "selected";
    public static final String SHOW_NOTE = "showNote";
    public static final String SNOOZE_DATE_TIME = "snoozeDateTime";
    public static final String STARRED = "starred";
    public static final String STARTER_PARENT_TASK = "isStarterParentTask";
    public static final String STATUS = "status";
    public static final String SYNC_ID = "syncId";
    public static final String SYNC_PARENT_ID = "syncParentId";
    public static final String SYNC_PRIOR_SIBLING_ID = "syncPriorSiblingId";
    public static final String SYNC_TIME = "syncTime";
    public static final String TASKSLIST_AS_TASK = "tasksListAsTask";
    public static final String TASKS_LIST = "tasksList";
    public static final String TASKS_LIST_ID = "tasksListId";
    public static final String TASKS_LIST_NAME = "tasksListName";
    public static final String TASK_NEED_UPDATE = "taskNeedUpdate";
    public static final String TASK_NUMBER = "taskNumber";
    public static final String TASK_PATH = "taskPath";
    public static final String WEB_TASK = "isWebTask";
    private boolean anySelected = false;
    private Integer autoSnoozeDuration = Integer.valueOf(1);
    private Boolean completed = Boolean.valueOf(false);
    private Long completionDate = Long.valueOf(0);
    private String contextName;
    private List<ContextTask> contextTasks;
    private Long creationDate = Long.valueOf(0);
    private Boolean deleted = Boolean.valueOf(false);
    private Long dueDate = Long.valueOf(0);
    private Boolean excludeFromDelete = Boolean.valueOf(false);
    private String extraEmails;
    private FILTER_PARAMETER_TYPES filterParameterType;
    private Integer fontBgColor = Integer.valueOf(0);
    private Integer fontColor = Integer.valueOf(0);
    private Integer fontStyle = Integer.valueOf(0);
    private String gtaskId;
    private boolean gtaskNeedUpdate;
    private String gtasksListId;
    private Boolean highlight = Boolean.valueOf(false);
    private String iconName;
    private Long id;
    private Boolean inTrashBin = Boolean.valueOf(false);
    private String indention;
    private boolean isAndOperation = false;
    private boolean isConsideredAsFloat = false;
    private boolean isCut;
    private boolean isExpanded;
    private Boolean isFloat = Boolean.valueOf(false);
    private boolean isMarkAsToday = false;
    private boolean isSelected;
    private boolean isStarterParentTask = false;
    private boolean isWebTask = false;
    private Long lastContentModifiedDate = Long.valueOf(0);
    private Long lastModified = Long.valueOf(0);
    private Integer level = Integer.valueOf(0);
    private Boolean moved = Boolean.valueOf(false);
    private String name;
    private boolean noneSelected = false;
    private String note;
    private Boolean notifyByEmail = Boolean.valueOf(false);
    private Boolean notifyExtraEmails = Boolean.valueOf(false);
    private boolean parent;
    private String parentGtaskId;
    private Long parentTaskId = Long.valueOf(0);
    private String priorSiblingGtaskId;
    private Long priorSiblingTaskId = Long.valueOf(0);
    private Integer priority = Integer.valueOf(1);
    private Long projectId = Long.valueOf(0);
    private Long reminderDateTime = Long.valueOf(0);
    private Integer reminderDaysEarlier = Integer.valueOf(0);
    private Boolean reminderEnabled = Boolean.valueOf(false);
    private Boolean reminderRepeatFromCompletionDate = Boolean.valueOf(false);
    private String reminderRepeatRule;
    private Boolean showNote = Boolean.valueOf(false);
    private Long snoozeDateTime = Long.valueOf(0);
    private Boolean starred = Boolean.valueOf(false);
    private Integer status = Integer.valueOf(0);
    private Long syncId;
    private Long syncParentId;
    private Long syncPriorSiblingId;
    private Long syncTime = Long.valueOf(0);
    private List<TagTask> tagTasks;
    private boolean taskNeedUpdate;
    private Integer taskNumber;
    private String taskPath;
    private TasksList tasksList;
    private boolean tasksListAsTask;
    private Long tasksListId = Long.valueOf(0);
    private String tasksListName;

    public static class FloatComparator implements Comparator<Task> {
        public int compare(Task task1, Task task2) {
            boolean isFloat1 = task1.isFloat().booleanValue();
            boolean isFloat2 = task2.isFloat().booleanValue();
            if (!isFloat1 && isFloat2) {
                return -1;
            }
            if (!isFloat1 || isFloat2) {
                return 0;
            }
            return 1;
        }
    }

    public void setDecoratorsForGoogleFromTask(Task task) {
        setHighlight(task.isHighlight());
        setFontStyle(task.getFontStyle());
        setFontColor(task.getFontColor());
        setFontBgColor(task.getFontBgColor());
        setShowNote(task.isShowNote());
        setCreationDate(task.getCreationDate());
        setPriority(task.getPriority());
        setReminderEnabled(task.isReminderEnabled());
        setReminderDaysEarlier(task.getReminderDaysEarlier());
        setReminderDateTime(task.getReminderDateTime());
        setReminderRepeatFromCompletionDate(task.isReminderRepeatFromCompletionDate());
        setReminderRepeatRule(task.getReminderRepeatRule());
        setNotifyByEmail(task.isNotifyByEmail());
        setNotifyExtraEmails(task.isNotifyExtraEmails());
        setExtraEmails(task.getExtraEmails());
        setIsFloat(task.isFloat());
        setExcludeFromDelete(task.isExcludeFromDelete());
        setIconName(task.getIconName());
        setSnoozeDateTime(task.getSnoozeDateTime());
        setAutoSnoozeDuration(task.getAutoSnoozeDuration());
        setCompletionDate(task.getCompletionDate());
        setStarred(task.isStarred());
        setProjectId(task.getProjectId());
        setStatus(task.getStatus());
        if (isReallyModified(task)) {
            setLastContentModifiedDate(this.lastModified);
        } else {
            setLastContentModifiedDate(task.getLastContentModifiedDate());
        }
    }

    public void setDecoratorsExtra(Task task) {
        setTasksListId(task.getTasksListId());
        setGtaskId(task.getGtaskId());
        setDueDate(task.getDueDate());
    }

    public boolean isTaskModified(Task task) {
        if (isReallyModified(task)) {
            return true;
        }
        boolean isTheSame = true && getTasksListId().equals(task.getTasksListId());
        if (isTheSame && this.highlight.equals(task.isHighlight())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.fontStyle.equals(task.getFontStyle())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.fontColor.equals(task.getFontColor())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.fontBgColor.equals(task.getFontBgColor())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.showNote.equals(task.isShowNote())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.priority.equals(task.getPriority())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.reminderEnabled.equals(task.isReminderEnabled())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.reminderDaysEarlier.equals(task.getReminderDaysEarlier())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.reminderDateTime.equals(task.getReminderDateTime())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.reminderRepeatFromCompletionDate.equals(task.isReminderRepeatFromCompletionDate())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && isStringObjectNotEqual(this.reminderRepeatRule, task.getReminderRepeatRule())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.notifyByEmail.equals(task.isNotifyByEmail())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.notifyExtraEmails.equals(task.isNotifyExtraEmails())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && isStringObjectNotEqual(this.extraEmails, task.getExtraEmails())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.isFloat.equals(task.isFloat())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.excludeFromDelete.equals(task.isExcludeFromDelete())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && isStringObjectNotEqual(this.iconName, task.getIconName())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.completionDate.equals(task.getCompletionDate())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.snoozeDateTime.equals(task.getSnoozeDateTime())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.status.equals(task.getStatus())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame && this.autoSnoozeDuration.equals(task.getAutoSnoozeDuration())) {
            isTheSame = true;
        } else {
            isTheSame = false;
        }
        if (isTheSame) {
            return false;
        }
        return true;
    }

    private boolean isStringObjectNotEqual(String str1, String str2) {
        if (StringUtil.isNullOrEmpty(str1) && !StringUtil.isNullOrEmpty(str2)) {
            return true;
        }
        if (StringUtil.isNullOrEmpty(str2) && !StringUtil.isNullOrEmpty(str1)) {
            return true;
        }
        if (StringUtil.isNullOrEmpty(str1) || StringUtil.isNullOrEmpty(str2) || str1.equals(str2)) {
            return false;
        }
        return true;
    }

    public boolean isRecurringTask() {
        return !StringUtil.isNullOrEmpty(this.reminderRepeatRule);
    }

    private boolean isReallyModified(Task task) {
        if (StringUtil.isNullOrEmpty(this.name) && !StringUtil.isNullOrEmpty(task.getName())) {
            return true;
        }
        if (StringUtil.isNullOrEmpty(task.getName()) && !StringUtil.isNullOrEmpty(this.name)) {
            return true;
        }
        if (StringUtil.isNullOrEmpty(this.name) || StringUtil.isNullOrEmpty(task.getName())) {
            if (!this.completed.equals(task.isCompleted()) || !this.dueDate.equals(task.getDueDate())) {
                return true;
            }
            if (StringUtil.isNullOrEmpty(this.note) && !StringUtil.isNullOrEmpty(task.getNote())) {
                return true;
            }
            if (StringUtil.isNullOrEmpty(task.getNote()) && !StringUtil.isNullOrEmpty(this.note)) {
                return true;
            }
            if (!(StringUtil.isNullOrEmpty(this.note) || StringUtil.isNullOrEmpty(task.getNote()) || this.note.equals(task.getNote()))) {
                return true;
            }
        } else if (!this.name.equals(task.getName())) {
            return true;
        }
        return false;
    }

    public Integer getPriority() {
        return this.priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getFontStyle() {
        return this.fontStyle;
    }

    public void setFontStyle(Integer fontStyle) {
        this.fontStyle = fontStyle;
    }

    public Integer getFontColor() {
        return this.fontColor;
    }

    public void setFontColor(Integer fontColor) {
        this.fontColor = fontColor;
    }

    public Integer getFontBgColor() {
        return this.fontBgColor;
    }

    public void setFontBgColor(Integer fontBgColor) {
        this.fontBgColor = fontBgColor;
    }

    public Boolean isHighlight() {
        return this.highlight;
    }

    public void setHighlight(Boolean highlight) {
        this.highlight = highlight;
    }

    public Boolean isMoved() {
        return this.moved;
    }

    public void setMoved(Boolean moved) {
        this.moved = moved;
    }

    public Boolean isInTrashBin() {
        return this.inTrashBin;
    }

    public void setInTrashBin(Boolean inTrashBin) {
        this.lastContentModifiedDate = Long.valueOf(AppUtil.getTodayDateTimeMilisecond());
        this.inTrashBin = inTrashBin;
    }

    public String getGtasksListId() {
        return this.gtasksListId;
    }

    public void setGtasksListId(String gtasksListId) {
        this.gtasksListId = gtasksListId;
    }

    public String getParentGtaskId() {
        return this.parentGtaskId;
    }

    public void setParentGtaskId(String parentGtaskId) {
        this.parentGtaskId = parentGtaskId;
    }

    public String getPriorSiblingGtaskId() {
        return this.priorSiblingGtaskId;
    }

    public void setPriorSiblingGtaskId(String priorSiblingGtaskId) {
        this.priorSiblingGtaskId = priorSiblingGtaskId;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGtaskId() {
        return this.gtaskId;
    }

    public void setGtaskId(String gtaskId) {
        this.gtaskId = gtaskId;
    }

    public Long getTasksListId() {
        return this.tasksListId;
    }

    public void setTasksListId(Long tasksListId) {
        this.tasksListId = tasksListId;
    }

    public Long getParentTaskId() {
        return this.parentTaskId;
    }

    public void setParentTaskId(Long parentTaskId) {
        this.parentTaskId = Long.valueOf(parentTaskId == null ? 0 : parentTaskId.longValue());
    }

    public Long getPriorSiblingTaskId() {
        return this.priorSiblingTaskId;
    }

    public void setPriorSiblingTaskId(Long priorSiblingTaskId) {
        long j;
        if (priorSiblingTaskId == null) {
            j = 0;
        } else {
            j = priorSiblingTaskId.longValue();
        }
        this.priorSiblingTaskId = Long.valueOf(j);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getDueDate() {
        return this.dueDate;
    }

    public void setDueDate(Long dueDate) {
        this.dueDate = dueDate;
    }

    public Long getLastModified() {
        return this.lastModified;
    }

    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

    public Boolean isDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.lastContentModifiedDate = Long.valueOf(AppUtil.getTodayDateTimeMilisecond());
        this.deleted = deleted;
    }

    public Boolean isCompleted() {
        return this.completed;
    }

    public void setCompleted(Boolean completed) {
        if (completed.booleanValue()) {
            this.snoozeDateTime = Long.valueOf(0);
        }
        this.completed = completed;
    }

    public boolean isGtaskNeedUpdate() {
        return this.gtaskNeedUpdate;
    }

    public void setGtaskNeedUpdate(boolean gtaskNeedUpdate) {
        this.gtaskNeedUpdate = gtaskNeedUpdate;
    }

    public boolean isTaskNeedUpdate() {
        return this.taskNeedUpdate;
    }

    public void setTaskNeedUpdate(boolean taskNeedUpdate) {
        this.taskNeedUpdate = taskNeedUpdate;
    }

    public Integer getLevel() {
        return this.level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getIndention() {
        return this.indention;
    }

    public void setIndention(String indention) {
        this.indention = indention;
    }

    public boolean isTasksListAsTask() {
        return this.tasksListAsTask;
    }

    public void setTasksListAsTask(boolean tasksListAsTask) {
        this.tasksListAsTask = tasksListAsTask;
    }

    public boolean isParent() {
        return this.parent;
    }

    public void setParent(boolean parent) {
        this.parent = parent;
    }

    public boolean isExpanded() {
        return this.isExpanded;
    }

    public void setExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
    }

    public void setBothParentTaskAndGtaskId(Task task) {
        this.parentTaskId = task.getParentTaskId();
        this.parentGtaskId = task.getParentGtaskId();
    }

    public void setBothPriorSiblingTaskFromTaskId(Task task) {
        this.priorSiblingTaskId = task.getId();
        this.priorSiblingGtaskId = task.getGtaskId();
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public Integer getTaskNumber() {
        return this.taskNumber;
    }

    public void setTaskNumber(Integer taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTasksListName() {
        return this.tasksListName;
    }

    public void setTasksListName(String tasksListName) {
        this.tasksListName = tasksListName;
    }

    public String getTaskPath() {
        return this.taskPath;
    }

    public void setTaskPath(String taskPath) {
        this.taskPath = taskPath;
    }

    public boolean isCut() {
        return this.isCut;
    }

    public void setCut(boolean isCut) {
        this.isCut = isCut;
    }

    public Long getSyncId() {
        return this.syncId;
    }

    public void setSyncId(Long syncId) {
        this.syncId = syncId;
    }

    public Long getSyncParentId() {
        return this.syncParentId;
    }

    public void setSyncParentId(Long syncParentId) {
        this.syncParentId = syncParentId;
    }

    public Long getSyncPriorSiblingId() {
        return this.syncPriorSiblingId;
    }

    public void setSyncPriorSiblingId(Long syncPriorSiblingId) {
        this.syncPriorSiblingId = syncPriorSiblingId;
    }

    public boolean isWebTask() {
        return this.isWebTask;
    }

    public void setWebTask(boolean isWebTask) {
        this.isWebTask = isWebTask;
    }

    public boolean isStarterParentTask() {
        return this.isStarterParentTask;
    }

    public void setStarterParentTask(boolean isStarterParentTask) {
        this.isStarterParentTask = isStarterParentTask;
    }

    public Long getSyncTime() {
        return this.syncTime;
    }

    public void setSyncTime(Long syncTime) {
        this.syncTime = syncTime;
    }

    public Boolean isShowNote() {
        return this.showNote;
    }

    public void setShowNote(Boolean showNote) {
        this.showNote = showNote;
    }

    public TasksList getTasksList() {
        return this.tasksList;
    }

    public void setTasksList(TasksList tasksList) {
        this.tasksList = tasksList;
    }

    public Long getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    public Long getLastContentModifiedDate() {
        return this.lastContentModifiedDate;
    }

    public void setLastContentModifiedDate(Long lastContentModifiedDate) {
        this.lastContentModifiedDate = lastContentModifiedDate;
    }

    public boolean isMarkAsToday() {
        return this.isMarkAsToday;
    }

    public void setMarkAsToday(boolean isMarkAsToday) {
        this.isMarkAsToday = isMarkAsToday;
    }

    public Boolean isReminderEnabled() {
        return this.reminderEnabled;
    }

    public void setReminderEnabled(Boolean reminderEnabled) {
        this.reminderEnabled = reminderEnabled;
    }

    public Integer getReminderDaysEarlier() {
        return this.reminderDaysEarlier;
    }

    public void setReminderDaysEarlier(Integer reminderDaysEarlier) {
        this.reminderDaysEarlier = reminderDaysEarlier;
    }

    public Long getReminderDateTime() {
        return this.reminderDateTime;
    }

    public void setReminderDateTime(Long reminderDateTime) {
        this.reminderDateTime = reminderDateTime;
    }

    public Boolean isReminderRepeatFromCompletionDate() {
        return this.reminderRepeatFromCompletionDate;
    }

    public void setReminderRepeatFromCompletionDate(Boolean reminderRepeatFromCompletionDate) {
        this.reminderRepeatFromCompletionDate = reminderRepeatFromCompletionDate;
    }

    public String getReminderRepeatRule() {
        return this.reminderRepeatRule;
    }

    public void setReminderRepeatRule(String reminderRepeatRule) {
        this.reminderRepeatRule = reminderRepeatRule;
    }

    public Boolean isNotifyByEmail() {
        return this.notifyByEmail;
    }

    public void setNotifyByEmail(Boolean notifyByEmail) {
        this.notifyByEmail = notifyByEmail;
    }

    public Boolean isNotifyExtraEmails() {
        return this.notifyExtraEmails;
    }

    public void setNotifyExtraEmails(Boolean notifyExtraEmails) {
        this.notifyExtraEmails = notifyExtraEmails;
    }

    public String getExtraEmails() {
        return this.extraEmails;
    }

    public void setExtraEmails(String extraEmails) {
        this.extraEmails = extraEmails;
    }

    public Boolean isFloat() {
        return this.isFloat;
    }

    public void setIsFloat(Boolean isFloat) {
        this.isFloat = isFloat;
    }

    public boolean isConsideredAsFloat() {
        return this.isConsideredAsFloat;
    }

    public void setConsideredAsFloat(boolean isConsideredAsFloat) {
        this.isConsideredAsFloat = isConsideredAsFloat;
    }

    public Boolean isExcludeFromDelete() {
        return this.excludeFromDelete;
    }

    public void setExcludeFromDelete(Boolean excludeFromDelete) {
        this.excludeFromDelete = excludeFromDelete;
    }

    public String getIconName() {
        return this.iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public Long getCompletionDate() {
        return this.completionDate;
    }

    public void setCompletionDate(Long completionDate) {
        this.completionDate = completionDate;
    }

    public Long getSnoozeDateTime() {
        return this.snoozeDateTime;
    }

    public void setSnoozeDateTime(Long snoozeDateTime) {
        if (this.completed.booleanValue()) {
            snoozeDateTime = Long.valueOf(0);
        }
        this.snoozeDateTime = snoozeDateTime;
    }

    public Integer getAutoSnoozeDuration() {
        return this.autoSnoozeDuration;
    }

    public void setAutoSnoozeDuration(Integer autoSnoozeDuration) {
        this.autoSnoozeDuration = autoSnoozeDuration;
    }

    public Boolean isStarred() {
        return this.starred;
    }

    public void setStarred(Boolean starred) {
        this.starred = starred;
    }

    public List<ContextTask> getContextTasks() {
        return this.contextTasks;
    }

    public void setContextTasks(List<ContextTask> contextTasks) {
        this.contextTasks = contextTasks;
    }

    public Long getProjectId() {
        return this.projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public boolean isAnySelected() {
        return this.anySelected;
    }

    public void setAnySelected(boolean anySelected) {
        this.anySelected = anySelected;
    }

    public boolean isNoneSelected() {
        return this.noneSelected;
    }

    public void setNoneSelected(boolean noneSelected) {
        this.noneSelected = noneSelected;
    }

    public FILTER_PARAMETER_TYPES getFilterParameterType() {
        return this.filterParameterType;
    }

    public void setFilterParameterType(FILTER_PARAMETER_TYPES filterParameterType) {
        this.filterParameterType = filterParameterType;
    }

    public String getContextName() {
        return this.contextName;
    }

    public void setContextName(String contextName) {
        this.contextName = contextName;
    }

    public boolean isAndOperation() {
        return this.isAndOperation;
    }

    public void setAndOperation(boolean isAndOperation) {
        this.isAndOperation = isAndOperation;
    }

    public List<TagTask> getTagTasks() {
        return this.tagTasks;
    }

    public void setTagTasks(List<TagTask> tagTasks) {
        this.tagTasks = tagTasks;
    }

    public String toString() {
        return "[" + this.name + "][" + this.id + "][" + this.parentTaskId + "][" + this.priorSiblingTaskId + "]";
    }
}
