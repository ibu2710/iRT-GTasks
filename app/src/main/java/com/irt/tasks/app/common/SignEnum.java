package com.irt.tasks.app.common;

public final class SignEnum {
    public static final SignEnum ALLOW_NEG = new SignEnum("Negative", 1);
    public static final SignEnum ALLOW_NEGPOS = new SignEnum("NegativeAndPositive", 5);
    public static final SignEnum ALLOW_NEGZER = new SignEnum("NegativeAndZero", 3);
    public static final SignEnum ALLOW_NEGZERPOS = new SignEnum("NegativeAndZeroAndPositive", 7);
    public static final SignEnum ALLOW_POS = new SignEnum("Positive", 4);
    public static final SignEnum ALLOW_ZER = new SignEnum("Zero", 2);
    public static final SignEnum ALLOW_ZERPOS = new SignEnum("ZeroAndPositive", 6);
    private String name;
    private final int value;

    private SignEnum(String newName, int newValue) {
        this.value = newValue;
        this.name = newName;
    }

    public String toString() {
        return "Name=" + this.name + ",Value=" + this.value;
    }

    public String getName() {
        return this.name;
    }

    public int getValue() {
        return this.value;
    }

    public boolean hasSetting(SignEnum compare) {
        Assert.notNull(compare, "compare");
        return (this.value & compare.getValue()) != 0;
    }
}
