package com.irt.tasks.app.framework.business;

import com.irt.tasks.app.businessobjects.Account;

public class ArchitectureContext {
    private static ThreadLocal<Account> accountContext = new ThreadLocal();
    private static ThreadLocal<Object> localContext = new ThreadLocal();
    private static ThreadLocal<Object> localContextExtra = new ThreadLocal();

    public static Account getAccount() {
        return (Account) accountContext.get();
    }

    public static void setAccount(Account account) {
        accountContext.set(account);
    }

    public static void setObject(Object obj) {
        localContext.set(obj);
    }

    public static Object getObject() {
        return localContext.get();
    }

    public static void setObjectExtra(Object obj) {
        localContextExtra.set(obj);
    }

    public static Object getObjectExta() {
        return localContextExtra.get();
    }
}
