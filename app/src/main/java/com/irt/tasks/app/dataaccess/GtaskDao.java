package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.Task;
import com.irt.tasks.app.businessobjects.TasksList;
import java.util.List;

public interface GtaskDao {

    public enum SubBatchType {
        DELETE,
        INSERT,
        UPDATE
    }

    void batchUpdateTasks(List<Task> list, SubBatchType subBatchType);

    List<Task> getAllTasksByTasksListId(String str);

    void moveTaskFromListToList(Task task, TasksList tasksList, TasksList tasksList2, Task task2);

    void moveTaskWithinSameList(Task task);
}
