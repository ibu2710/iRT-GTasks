package com.irt.tasks.app.dataaccess.adapter;

import android.database.Cursor;

public interface AccountDBAdapter extends DBAdapter {
    Cursor getAllAutoSync();

    Cursor getAllVisible(boolean z);
}
