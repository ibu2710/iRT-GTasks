package com.irt.tasks.app.dataaccess;

import com.irt.tasks.app.businessobjects.Email;

public interface EmailDao {
    Email get();

    void saveOrUpdate(Email email);
}
