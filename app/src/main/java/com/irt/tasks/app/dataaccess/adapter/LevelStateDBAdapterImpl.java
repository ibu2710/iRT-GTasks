package com.irt.tasks.app.dataaccess.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.irt.tasks.app.businessobjects.LevelState;
import com.irt.tasks.app.framework.business.IrtTasksBo;

public class LevelStateDBAdapterImpl implements LevelStateDBAdapter {
    public static final String DATABASE_CREATE = "create table level_state (tasks_list_id integer not null, zoom_task_id_str text null, level integer default 0 not null, position integer default 0 not null); ";
    public static final String DATABASE_TABLE = "level_state";
    public static final String INDEX_ON_TASK_LIST_ID = "CREATE INDEX idx_level_state_tasks_list_id ON level_state (tasks_list_id) ";
    public static final String LEVEL = "level";
    public static final String POSITION = "position";
    public static final String TASKS_LIST_ID = "tasks_list_id";
    public static final String ZOOM_TASK_STR_ID = "zoom_task_id_str";
    private DatabaseHelper DBHelper;
    private final Context context;
    private SQLiteDatabase db;
    private static final String[] ALL_COLUMNS = new String[]{"tasks_list_id", ZOOM_TASK_STR_ID, "level", "position"};

    public LevelStateDBAdapterImpl(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(this.context);
    }

    public LevelStateDBAdapter open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.DBHelper.close();
    }

    public boolean delete(long rowId) {
        return this.db.delete(DATABASE_TABLE, new StringBuilder().append("tasks_list_id=").append(rowId).toString(), null) > 0;
    }

    public Cursor get(long rowId) throws SQLException {
        Cursor mCursor = this.db.query(true, DATABASE_TABLE, ALL_COLUMNS, "tasks_list_id=" + rowId, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor getAll() {
        return this.db.query(DATABASE_TABLE, ALL_COLUMNS, null, null, null, null, null);
    }

    public long insert(IrtTasksBo businessObject) {
        return this.db.insert(DATABASE_TABLE, null, populateBusinessObjectContentValues((LevelState) businessObject));
    }

    public boolean update(IrtTasksBo businessObject) {
        LevelState levelState = (LevelState) businessObject;
        return this.db.update(DATABASE_TABLE, populateBusinessObjectContentValues(levelState), new StringBuilder().append("tasks_list_id=").append(levelState.getTasksListId()).toString(), null) > 0;
    }

    private ContentValues populateBusinessObjectContentValues(LevelState levelState) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("tasks_list_id", levelState.getTasksListId());
        contentValues.put(ZOOM_TASK_STR_ID, levelState.getZoomTaskIdStr());
        contentValues.put("level", levelState.getLevel());
        contentValues.put("position", levelState.getPosition());
        return contentValues;
    }
}
